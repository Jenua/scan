/**
 * Created by Melnykova on 12/13/2016.
 */
$(function () {
    $('.main-input').focus();
    //off the history of the input field as valueless
    $(".main-input").attr("autocomplete","off");
    var userID = $('body').find('#userID').val();
    //permissions value
        superUser = (userID == 2);

    /*
     Form validation
     */
    $('#wForm').validator().on('submit', function (e) {
        if (e.isDefaultPrevented()) {
            bootstrapMess('warning','Please, fill required data correct.');
        } else {
            //hide error messages if was
            $('.popup').fadeOut(1000);
            e.preventDefault();
            var form = $('#wForm');
            var url = $(form).attr("action");
            var formData = {};

            formData = $(form).serialize();
            $.ajax({
                url: url,
                method: 'POST',
                data: formData,
                beforeSend: function () {
                    displayModal();
                    displayLoading();
                },
                complete: function (request) {
                    hideLoading();
                    hideModal();
                    var res = request.responseText;
                    if(!(~res.indexOf('data '))){
                        bootstrapMess('danger', res + ' Saving failed.' );

                    } else {
                       // bootstrapMess('success', res);
                        $(form).find("#print_pdf").trigger('click');
                    }

                },
                error: function (request, error) {
                    console.log("Ajax request error: " + request.responseText);
                }
            });
        }
    });

    /**
     * User accounts window
     */

    var  usersDialog, scannedBy;

    //inventory dialog
    usersDialog = $("#module_accounts").dialog({
        //Select your name to personalize of scanning process.
        title: "Select your name to start scanning",
        autoOpen: false,
        height: 'auto',
        width: 775,
        modal: true,
        position: { my: "center top", at: "center center-15%", of: $( ".module-container" ) },
            //prevent to close the dialog
            closeOnEscape: (superUser),
            open: function(event, ui) {
                if(!superUser)
                    $(".ui-dialog-titlebar-close", ui.dialog | ui).hide();
            }
    });

    //OFF the UserList popup by default for DreamLineAdmin
    if(!superUser)
        usersDialog.dialog("open");

    $("#module_accounts_dialog").button().on("click", function () {
        usersDialog.dialog("open");
    });
    $("#module_accounts_dialog").removeClass('ui-button');
    //inventory settings form
    usersDialog.find('button').on("click", function (event) {

        event.preventDefault();
        var formData = {};
        formData = $(this);
        scannedBy = formData[0].value;
        $('#scannedByUser').val(scannedBy).change();
        $('#userAcc').text( formData[0].name ).change();
        usersDialog.dialog("close");
        $('.main-input').focus();
    });
    /*
     Scanning form validation
     */
    var dataWindow = $(".jqGridScannedOrders"),
        scannedStr = '',
        flag = 0,
        url = $('#sForm').attr("action"),
        comment = $('#scanned').val();

    /* LOADING Parse Barcode add fields for the scanning system     */

    var addParams = {},
    module = $('#scannedSystem').val();
    /**
     * Error effects, animation
     * @type {*}
     */
    var audio = $('#scanForm').children('audio'),
    audioElement = audio.get(0);

    // Do action until
    $('.module-container').on('keypress click',function() {
        audioElement.pause();
        if (audioElement.currentTime > 0){
            stopEffect();
            //if($('.popup').css('display') == 'block')
            $('.popup').effect('blind', 800);
            audioElement.currentTime = 0;
        }
    });

    $('#sForm').validator().on('submit', function (e) {
        if (e.isDefaultPrevented() || $('#scannedByUser').val() == 0) {

            bootstrapMess('danger', 'Please enter data before submitting.');
            //Error sound+highlight effect
            audioElement.play();
            runEffect('highlight');//puff

            return false;
        }
        else {
            e.preventDefault();
            e.stopPropagation();

            var form = this,
                comment = $('#comment').val();
                scannedBy = $('#scannedByUser').val();
            if (scannedBy == 0) {
                if (module == 'shipping') {
                    scannedBy = userID;
                } else {
                    alert('You can\'t scan without selection of User Mode.');
                    usersDialog.dialog("open");
                    return false;
                }
            }
            addParams.PO = $('#PO').val();
            if (flag == 1) {
                flag++;
                bootstrapMess('warning', 'The order: ' + addParams.PO + ' has been canceled!');
                return false;
            }
            scannedStr = (module != 'loading') ? "Scanned code <b>" + addParams.PO + " </b> saved." :
                //"Pallet ID: <u>"+addParams.PLT+"</u> Truck ID: <u>"+addParams.TRU+ "</u> PO Number: <u>"+addParams.PO+"</u> saved.";
            "Scanned PRO number <b>" + addParams.PO + " </b> saved.";
            var msuccess = scannedStr +
                "" +
                " <br> Expand the collapsed 'Scanned today' table to see changes.";

            $.ajax({
                url: url,
                method: 'POST',
                data: {
                    action: 'save',
                    module: module,
                    PO: addParams.PO,
                    comment: comment,
                    scannedBy: scannedBy
                },

                beforeSend: function () {
                    displayModal();
                    displayLoading();
                },
                complete: function (request) {
                    hideLoading();
                    hideModal();
                    var res = request.responseText;

                    if (~res.indexOf('data saved')) {
                        bootstrapMess('success', scannedStr);
                    } else {
                        bootstrapMess('warning', res + '');
                    }

                    if (dataWindow.getGridParam("gridstate") == "visible")
                        $(".ui-jqgrid-titlebar-close", dataWindow[0].grid.cDiv).trigger("click");
                    //$(".jqGridScannedOrders").setGridParam('hiddengrid','hidden').trigger();
                    form.reset();
                },
                error: function (request, error) {
                    console.log("Ajax request error: " + request.responseText);
                }
            });

        }
            return false;  // Stop form submit
        });


});

// Set adaptive width
$(window).on("resize", function () {
    var $grid = $('[class^="jqGrid"]'),
        newWidth = $grid.closest(".ui-jqgrid").parent().width();
    $grid.jqGrid("setGridWidth", newWidth, true);
    // Setter for dialog window
    $( "#module_accounts" ).dialog( "option", "width", newWidth );
    if (newWidth < 479)
        $("#usersBoxes button").css( { width: '100%' });

});
// Set adaptive UserList popup
// $(window).on("resize", function () {
//     var $container = $('.module-container'),
//         newWidth = $container.closest("div").parent().width() + 30;
//     console.log($container.closest("div").parent().width());
//     // Setter for dialog window
//     $( "#module_accounts" ).dialog( "option", "width", newWidth );
// });