/**
 * Created by Yeuvheniia Melnykova on 2/16/2017 for DataWindow.
 * Using free jqGrid plugin
 * https://free-jqgrid.github.io/
 */
$(function() {
    var
        checkboxTemplate = {
            formatter: 'checkbox', edittype: 'checkbox', type: 'select',
            editoptions: {value: "1:0"}
        },
        gridData,
        content,
        scannedBy,
        scannedSystem,
        handlerUrl = "handler.php";

        scannedSystem = $('#scannedSystem').val();
        scannedBy = $('#scannedByUser').val();



var dataWindow = $('.jqGridScannedOrders'),
    editingRowId,
    commonColNames = ["ID","POnumber","Timestamp","Comment","Name","Edit"],
     orderColum = (scannedSystem == 'shipping')? 'ShipDate' : 'DateCreated',
     commonColModel = [{name: 'ID', index: 'ID', key: true, hidden: true},
         {name: 'POnumber', index: 'POnumber', width: 120},
         {
             name: orderColum,
             index: orderColum,
             width: 100,
             formatter: 'date',
             formatoptions: {srcformat: 'ISO8601Long', newformat: 'm-d-Y h:i:s'},
             align:'center',
             hidden: false
         },
         {name: 'comment', index: 'comment', width: 120, formatter: nullFormatter,
             editable: true}
         ,
         { name: 'UserName', index: 'UserName', width: 120, editable: false, sortable: false }
         ,
         {
             label: "Edit",
             name: "edit",
             width: 40, align: "center",
             formatter: "actions",formatoptions: { keys: true, delbutton: false },
             sortable: false
         }];

//loading additional fields

    if ( scannedSystem == 'loading' ) {
        commonColNames.unshift(//"TruckID","PalletID",
            "PROnumber" );

        commonColModel.unshift(
            {name: 'PROnumber', index: 'PROnumber', width: 120}
            //{name: 'TruckID', index: 'TruckID', width: 120},
            //{name: 'PalletID', index: 'PalletID', width: 120}
            );
    }
    // DataWindow editing params

    //Scanned : loading, packing, packing, end-of-day dataWindow

    dataWindow.jqGrid({
        datatype: "json",//stop loading data with the loading page
        url: handlerUrl,
        height: 'auto',
        postData: {
            action: 'scanned_today',
            module: scannedSystem
        },
        caption: 'Scanned today',
        colNames: commonColNames,
        colModel: commonColModel,
        rowNum: 6,
        rowList: [6,30,100,1000],
        width: 'auto',
        pager: 'pagernav',
        sortname: orderColum,
        sortorder: 'DESC',
        rownumbers: true,
        viewrecords: true,
        shrinkToFit: true,
        autowidth: true,
        editurl:  function () {
            return handlerUrl+'?scannedBy='+ $('#scannedByUser').val();
        },
        ondblClickRow: function(rowid){

           $(this).jqGrid('editGridRow', rowid, {
               //dialog window settings
               width: 333, left: 0, top: -130, dataheight: '50%',
               closeOnEscape: true } );
        },
        hiddengrid: false, // default collapse grid
        autoload:false,
        loadError : function(xhr,st,err) {
            err ="Type: " + st + "; Response: "+ xhr.status + " " + xhr.statusText;
            content = '<div class="alert alert-dismissable alert-danger">'
                + '<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>';
            content += '<strong> DataWindow error!  </strong> ' + err + ' </div>';
            $('.content').html(content);
        }
    });

    dataWindow.jqGrid('navGrid','#pagernav',{
        edit: false,
        add: false,
        del: false,
        search: false,
        refresh: true,refreshtitle:"Show all records", refreshtext:"All", refreshstate: 'current',
        view: false,
        position: "left",
        cloneToTop: false
    });
    //filter by owner user
    dataWindow.jqGrid('navButtonAdd', "#pagernav", {
        caption: "Owner", title: "Filter the data scanned by me", buttonicon: "ui-icon-person",
        onClickButton: function () {
            var scannedBy = $('#scannedByUser').val();
            //var mypostData = $('.jqGridScannedOrders').jqGrid("getGridParam", "postData");
            $(".jqGridScannedOrders")
                .jqGrid('setGridParam',{
                    datatype:'json',
                    postData:{ filters: 'byUser', userFilter: scannedBy }
                }).trigger("reloadGrid");
        }
    });

    //reload grid after expanding
    $(".ui-jqgrid-titlebar-close").click(function() {
        var isHidden = dataWindow.getGridParam("gridstate");
           /* loggedUser = $('#userID').val(); */
        if(isHidden == "hidden")
            dataWindow.trigger('reloadGrid');
           /* dataWindow.jqGrid('setGridParam',{
                datatype:'json',
                postData:{ userFilter: loggedUser }
            }).trigger("reloadGrid");*/
    });
    //end of scanned js

    //RITS dataWindow
    var ritsDW = $("#jqGrid");
    ritsDW.jqGrid({
        url: handlerUrl,
        datatype: 'json',
        postData:{ action: 'data_window', module: scannedSystem },
        mtype:'POST',
        height: 'auto',
        autowidth: true,
        caption: 'The list of the today printed/modified labels',
        colNames: ['Inventory ID', 'SKU', 'LOT', 'Create', 'Label printed', 'Actions', 'Is deleted'],
        colModel: [
            {
                name: 'id',//set in lowcase that multiedit use request name in this format
                key: true, index: 'id', width: 80, hidden: false,
                searchoptions: {sopt: ['cn']}
            },
            {
                name: 'SKU', index: 'SKU', width: 200, stype: 'text',
                //edit
                editable: true,
                edittype: "text",
                editrules: {
                    custom: true, custom_func: validateSKU
                },
                search:true, searchoptions:{ sopt: ['cn']}
            },
            {
                name: 'LOT', index: 'LOT', width: 100, stype: 'text',
                editable: true,
                edittype: "text",
                editrules: {
                    custom: true, custom_func: validateLot
                },
                search:true, searchoptions:{ sopt: ['cn']}
            }
            , {
                name: 'ENTERED_DATE', index: 'ENTERED_DATE', width: 80, stype: 'text',
                formatter: 'date', formatoptions: {srcformat: 'ISO8601Long', newformat: 'y-m-d'},
                search: false
                //  ,searchoptions:{dataInit:datePick, attr:{title:'Select Date'}, sopt: ['gt','lt','bw']}
            }
            , {
                name: 'LABEL_PRINT_DATE', index: 'LABEL_PRINT_DATE', width: 80, stype: 'text',
                formatter: 'date', formatoptions: {srcformat: 'ISO8601Long', newformat: 'y-m-d'},
                search: false
            },
            {
                label: "Actions",
                name: "actions",
                width: 60,
                formatter: "actions",
                formatoptions: {
                    keys: true,
                    editOptions: {},
                    addOptions: {},
                    delOptions: {}
                },
                search:false,
                sortable: false
            },
            {name: 'MARK', index: 'MARK', width: 80, align: 'center', sortable: true, editable: false,
                 formatter: 'checkbox', formatoptions: { disabled : true},
                 type: 'select',
                search:true, searchoptions:{ sopt: ['cn']}
            }
        ],
        rowNum: -1,
       // width: 'auto',
        pager: '#pagernav',
        //hiddengrid: true,// default collapse grid
        //Loading... and key:true for id
        // jsonReader: {
        //     repeatitems: false
        // },
        gridview: true, //you should always add this option to your grids to improve performance.
        sortname: 'ID',
        sortorder: 'DESC',
        rownumbers: true,
        viewrecords: true,
        //loadonce: true,//reloadGrid after Save dosen't work if true
        //onSelectRow: editRow, // the javascript function to call on row click. will ues to to put the row in edit mode
        editurl: handlerUrl,
        rowattr: function (rd) {
            if (rd.MARK == "1") {
                //$("#jqGrid").editRow('#'+rd.ID, false);
                return {"class": "deletedAltRowClass"};
            }
        },
        multiselect: true
    });

    ritsDW.navGrid('#pagernav',
        // the buttons to appear on the toolbar of the grid
        {
            edit: true,
            add: false,
            del: true,
            search: true,
            refresh: true,
            view: false,
            position: "left",
            cloneToTop: false
        }
        //options for the Edit Dialog
        , {
            editCaption: "The Edit Dialog",
            //template: template,
            errorTextFormat: function (data) {
                return 'Error: ' + data.responseText
            }
        }
        , {
            errorTextFormat: function (data) {
                console.log(data.responseText);
                return 'Error: ' + data.responseText
            }
        }
    );


});

function editSuccessful() {
    console.log("success");
}

function editFailed() {
    console.log("fail");
}

function validateSKU(value, column) {
    if (/^[A-Z0-9-]{10,25}$/.test(value))
        return [true, ""];
    else
        return [false, "Please enter a correct " + column + " value"];
}

function validateLot(value, column) {

    if (/^[{A-Z}0-9-]{6,10}$/.test(value))
        return [true, ""];
    else
        return [false, "Please enter a correct " + column + " value"];
}
function nullFormatter(cellvalue, options, rowObject) {

    return (cellvalue === null || cellvalue === 'null' || cellvalue === 'NULL') ? '' : cellvalue;
}
