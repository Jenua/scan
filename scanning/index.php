<?php require_once '../Include/init.php'; ?>
<?php require_once '../Include/header.php'; ?>
<?php require_once '../Include/navigation.php'; ?>
<?php
global $user,
       $us_url_root;

//Session::flash('module');
if (Input::get('module'))
    Session::put('module', Input::get('module'));
else
    Session::put('module', '');
//if( Input::get('module') == '' )    Session::delete('module');
?>

<div class="container">
    <?php if ((!$user->isLoggedIn())){
        Redirect::to('../Include/login.php');
    } else {
    Redirect::to();
    ?>

    <div class="row">
    <div class="col-xs-12 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2">
        <?php
        if (Session::exists('module') && in_array(Session::get('module'), $user_modules)) {
        $module = Session::get('module');
        $module_title = module_title($module);
        ?>
        <?php
        if ($module != 'rits' && $module != 'full_loading') {
        ?>
        <div class="<?= $module ?> module-container row">
            <div id="module-header" class="col-md-12">
                <div id="date_block" class="text-right">Date: <span
                            class="xx-large"><?php echo date('m-d-y'); ?></span></div>
                <h1><?= $module_title ?> </h1>
            </div>

            <div class="col-md-12" id="scanForm">
                <form method="post" action="handler.php" class="form-vertical" id="sForm" data-toggle="validator">
                    <fieldset>
                        <div class="form-group">
                            <div class="col-md-12 text-center">
                                <div id="userAcc"></div>
                                <input type="hidden" id="scannedByUser" name="scannedByUser"
                                       value="<?= ($module=='shipping' or $user->data()->id==2)? $user->data()->id : 0 ?>">
                            </div>
                            <label class="col-md-12 control-label" for="PO">

                                <?= ($module == 'loading') ? 'PRO ' : 'PO '; ?>
                                Number: </label>
                            <div class="col-md-12" id="message">
                                <div class="popup">
                                    <div class="content"></div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <input class="form-control input-lg main-input" type="text" id="PO" name="poNumber"
                                       required></div>
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-lg btn-primary btn-block">Submit</button>
                            </div>
                            <input type="hidden" id="scannedSystem" name="scannedSystem" value="<?= $module ?>">

                            <input type="hidden" id="comment" name="comment">
                        </div>
                    </fieldset>
                </form>
                <audio id="xyz" src="../Include/snd/Siren_Noise.mp3" preload="auto"></audio>
            </div>

            <div class="col-md-12">
                <table class="jqGrid" id="jqGridScanedOrders"></table>
                <div class="pagernavScannedOrders" id="pagernav"></div>
            </div>
            <?php
            } else if (file_exists($module . "_form.php")) {
                //rits module
                include ($module . '_form.php');
            }
            ?>
            <input type="hidden" id="userID" name="userID" value="<?= $user->data()->id ?>">
        </div> <!-- module-container end of -->
            <?php
            } else { ?>

                <h2 class="text-center">Inventory systems</h2>
                <form id="sform" name="inventorySelect" method="post" action="index.php">
                    <ul id="systems">
                        <?php
                        $inv_url = $us_url_root;
                        $master_modules = Config::get('master_modules');
                        $inv_url .= "?module=";
                        foreach ($master_modules as $module_name) {
                            if (in_array($module_name, $user_modules)) {
                                ?>
                                <li id="<?= $module_name ?>">
                                    <a id="<?= $module_name . "_link" ?>"
                                       href="<?= $inv_url . $module_name ?>"><?= module_title($module_name) ?></a>
                                </li>
                            <?php } else { ?>
                                <li id="<?= $module_name ?>" class="disabled">
                                    <a href=""><?= module_title($module_name) ?></a>
                                </li>
                            <?php }

                        } ?>
                    </ul>
                </form>

            <?php } ?>

            <div class="darkBack"></div>
            <div class="page-loading"><img alt="loading..." src="../new_order/Include/img/294.GIF"></div>

            <?php
            }
            ?>
    </div>
        <div class="col-lg-2 col-md-1"></div>
    </div>
<?php include_once '../Include/footer.php' ?>
