<?php
$path = $_SERVER['DOCUMENT_ROOT'];
require_once($path.'/db_connect/connect.php');

	$returnValue = false;
		
	$term = trim(strip_tags($_GET['term']));
	$term = str_replace(array("'", "`"), "''", $term);
		
	$conn = Database::getInstance()->dbc;
	$res = getValues($conn, $term);
	
	if(isset($res) && !empty($res))
	{
		$array = array();
		foreach($res as $key => $value)
		{
			$array[]= $value['QB_SKU'];
		}
		$returnValue = $array;
	}
	$returnValue = json_encode($returnValue);
	$conn = null;
	print_r($returnValue);

function getValues($conn, $term)
{
	$query = "SELECT TOP(100) [QB_SKU] FROM [".DATABASE."].[dbo].[DL_valid_SKU] WHERE [QB_SKU] LIKE  '%".$term."%' GROUP BY [QB_SKU] ORDER BY [QB_SKU]";
	//print_r($query);
	try{
		$result = $conn->prepare($query);
		$result->execute();
		$result = $result->fetchAll(PDO::FETCH_ASSOC); 
		return $result;
	}
	catch(PDOException $e)
	{
		return false;
	}
}
?>