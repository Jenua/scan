<?php
$path = $_SERVER['DOCUMENT_ROOT'];
require_once($path.'/db_connect/connect.php');

	$returnValue = false;
		
	$term = trim(strip_tags($_GET['term']));
	$term = str_replace(array("'", "`"), "''", $term);
		
	$conn = Database::getInstance()->dbc;
	$res = getValues($conn, $term);
	
	if(isset($res) && !empty($res))
	{
		$array = array();
		foreach($res as $key => $value)
		{
			$array[]= $value['CustomerRef_FullName'];
		}
		$returnValue = $array;
	}
	$returnValue = json_encode($returnValue);
	$conn = null;
	print_r($returnValue);


function getValues($conn, $term)
{
	$query = "SELECT TOP(100) * FROM (	
	SELECT 
	[shiptrack].[CustomerRef_FullName]
	FROM [".DATABASE."].[dbo].[shiptrack]
	WHERE [shiptrack].[CustomerRef_FullName] LIKE '%".$term."%' UNION ALL
	SELECT 
	[invoice].[CustomerRef_FullName]
	FROM [".DATABASE31."].[dbo].[invoice]
	WHERE [invoice].[CustomerRef_FullName] LIKE '%".$term."%' UNION ALL
	SELECT 
	[salesorder].[CustomerRef_FullName]
	FROM [".DATABASE31."].[dbo].[salesorder]
	WHERE [salesorder].[CustomerRef_FullName] LIKE '%".$term."%' UNION ALL
	SELECT 
	[manually_shiptrack].[CustomerRef_FullName]
	FROM [".DATABASE."].[dbo].[manually_shiptrack]
	WHERE [manually_shiptrack].[CustomerRef_FullName] LIKE '%".$term."%') A
	GROUP BY [CustomerRef_FullName]
	ORDER BY [CustomerRef_FullName]";
	//print_r($query);
	try{
		$result = $conn->prepare($query);
		$result->execute();
		$result = $result->fetchAll(PDO::FETCH_ASSOC); 
		return $result;
	}
	catch(PDOException $e)
	{
		return false;
	}
}
?>