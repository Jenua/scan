<?php
$path = $_SERVER['DOCUMENT_ROOT'];
require_once($path.'/db_connect/connect.php');

	$returnValue = false;
		
	$term = trim(strip_tags($_GET['term']));
	$term = str_replace(array("'", "`"), "''", $term);
		
	$conn = Database::getInstance()->dbc;
	$res = getValues($conn, $term);
	
	if(isset($res) && !empty($res))
	{
		$array = array();
		foreach($res as $key => $value)
		{
			$array[]= $value['ShipAddress_Addr1'];
		}
		$returnValue = $array;
	}
	$returnValue = json_encode($returnValue);
	$conn = null;
	print_r($returnValue);


function getValues($conn, $term)
{
	$query = "SELECT TOP(100) [ShipAddress_Addr1] FROM [".DATABASE."].[dbo].[invoice] WHERE [ShipAddress_Addr1] LIKE '%".$term."%' GROUP BY [ShipAddress_Addr1] ORDER BY [ShipAddress_Addr1]";
	//print_r($query);
	try{
		$result = $conn->prepare($query);
		$result->execute();
		$result = $result->fetchAll(PDO::FETCH_ASSOC); 
		return $result;
	}
	catch(PDOException $e)
	{
		return false;
	}
}
?>