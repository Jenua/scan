<?php
$path = $_SERVER['DOCUMENT_ROOT'];
require_once($path.'/db_connect/connect.php');

function get_uid()
{
	$uid = uniqid('manually_', true);
	return $uid;
}

function addquote($str)
{
	$str = str_replace("'", "`", $str);
	return $str;
}

function prepare_array()
{
	$data = Array();
		foreach($_POST['product'] as $keyP => $product)
		{
			$data['product'][] = $product;
			foreach($_POST['item'] as $item)
			{
				if($item['SKU'] == $product['SKU'])
				{
					$item['iWeight'].= ' lbs';
					$data['product'][$keyP]['item'][] = $item;
				}
			}
		}
		
		$data['CustomerFullName'] = $_POST['CustomerFullName'];
		$data['Addr1'] = $_POST['Addr1'];
		$data['Addr2'] = $_POST['Addr2'];
		$data['Addr3'] = $_POST['Addr3'];
		$data['ShipToCity'] = $_POST['ShipToCity'];
		$data['ShipToState'] = $_POST['ShipToState'];
		$data['ShipToPostalCode'] = $_POST['ShipToPostalCode'];
		$data['ShipToCountry'] = $_POST['ShipToCountry'];
		$data['PONumber'] = $_POST['PONumber'];
		$data['PhoneNumber'] = $_POST['PhoneNumber'];
		$data['ShipMetdod'] = $_POST['ShipMetdod'];
		if(isset($_POST['force_carrier']) && !empty($_POST['force_carrier']))
		{
			$data['force_carrier'] = 1;
		} else
		{
			$data['force_carrier'] = 0;
		}
		$data['Memo'] = $_POST['Memo'];
		$data['User'] = $_POST['User'];
	if (!empty($data)) return $data; else die('<br>Can not prepare form data for processing.<br>');
}

function add_keys($data)
{
	$data['TxnID'] = get_uid();
	$data['RefNumber'] = get_uid();
	foreach($data['product'] as $keyP => $product)
	{
		$data['product'][$keyP]['IDKEY'] = $data['TxnID'];
		$data['product'][$keyP]['TxnLineID'] = get_uid();
		foreach($product['item'] as $keyI => $item)
		{
			$data['product'][$keyP]['item'][$keyI]['IDKEY'] = $data['product'][$keyP]['IDKEY'];
			$data['product'][$keyP]['item'][$keyI]['GroupIDKEY'] = $data['product'][$keyP]['TxnLineID'];
			$data['product'][$keyP]['item'][$keyI]['TxnLineID'] = get_uid();
		}
	}
	if (!empty($data)) return $data; else die('<br>Can not add unique keys.<br>');
}

function generateShiptrackQuery($data, $user)
{
	$query = "INSERT INTO [dbo].[manually_shiptrack]
           ([TxnID]
           ,[TimeCreated]
           ,[TimeModified]
           ,[CustomerRef_FullName]
           ,[RefNumber]
           ,[ShipAddress_Addr1]
           ,[ShipAddress_Addr2]
           ,[ShipAddress_Addr3]
           ,[ShipAddress_City]
           ,[ShipAddress_State]
           ,[ShipAddress_PostalCode]
           ,[ShipAddress_Country]
           ,[PONumber]
           ,[FOB]
           ,[ShipMethodRef_FullName]
           ,[IsManuallyClosed]
           ,[IsFullyInvoiced]
           ,[Memo]
		   ,[CustomField10]
		   ,[force_carrier])
     VALUES
           ('".addquote($data['TxnID'])."'
           ,GETDATE()
           ,GETDATE()
		   ,'".addquote($data['CustomerFullName'])."'
		   ,'".addquote($data['RefNumber'])."'
		   ,'".addquote($data['Addr1'])."'
		   ,'".addquote($data['Addr2'])."'
		   ,'".addquote($data['Addr3'])."'
		   ,'".addquote($data['ShipToCity'])."'
		   ,'".addquote($data['ShipToState'])."'
		   ,'".addquote($data['ShipToPostalCode'])."'
		   ,'".addquote($data['ShipToCountry'])."'
		   ,'".addquote($data['PONumber'])."'
		   ,'".addquote($data['PhoneNumber'])."'
		   ,'".addquote($data['ShipMetdod'])."'
		   ,0
		   ,0
		   ,'".addquote($data['Memo'])."'
		   ,'".addquote($user)."'
		   ,'".addquote($data['force_carrier'])."'
          )";
	if (!empty($query)) return $query; else die('<br>Can not generate shiptrack query.<br>');
}

function generateGroupdetailQuery($data)
{
	$query = "INSERT INTO [dbo].[manually_groupdetail]
           ([IDKEY]
           ,[TxnLineID]
           ,[ItemGroupRef_FullName]
           ,[Prod_desc]
           ,[Quantity])
     VALUES
           ('".addquote($data['IDKEY'])."'
           ,'".addquote($data['TxnLineID'])."'
           ,'".addquote($data['SKU'])."'
           ,'".addquote($data['pDescription'])."'
           ,'".addquote($data['pQuantity'])."')";
	if (!empty($query)) return $query; else die('<br>Can not generate groupdetail query.<br>');
}

function generateLinedetailQuery($data)
{
	$query = "INSERT INTO [dbo].[manually_linedetail]
           ([IDKEY]
           ,[TxnLineID]
           ,[CustomField8]
           ,[CustomField9]
           ,[ItemRef_FullName]
           ,[quantity]
           ,[GroupIDKEY])
     VALUES
           ('".addquote($data['IDKEY'])."'
           ,'".addquote($data['TxnLineID'])."'
           ,'".addquote($data['iWeight'])."'
           ,'".addquote($data['iWeight'])."'
           ,'".addquote($data['iDescription'])."'
           ,'".addquote($data['iQuantity'])."'
           ,'".addquote($data['GroupIDKEY'])."')";
	if (!empty($query)) return $query; else die('<br>Can not generate linedetail query.<br>');
}

function getConnection()
{
	$conn = Database::getInstance()->dbc;
	return $conn;
}

function beginTransaction($conn)
{
	try
	{
		$result = $conn->beginTransaction();
		return $result;
	}catch (Exception $e) {
		die("<br>".$e."<br>");
		return false;
	}
}

function commitTransaction($conn)
{
	try
	{
		$result = $conn->commit();
		return $result;
	}catch (Exception $e) {
		die("<br>".$e."<br>");
		return false;
	}
}

function rollbackTransaction($conn)
{
	try
	{
		$result = $conn->rollback();
		return $result;
	}catch (Exception $e) {
		die("<br>".$e."<br>");
		return false;
	}
}

function runQuery($conn, $query)
{
	try
	{
		//echo "<br>".$query."<br>";
		$stmt = $conn->prepare($query);
		$result = $stmt->execute();
		$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}catch (Exception $e) {
		die("<br>".$e."<br>");
		return false;
	}
}

function runQuery_empty_result($conn, $query)
{
	try
	{
		//echo "<br>".$query."<br>";
		$stmt = $conn->prepare($query);
		$result = $stmt->execute();
		return $result;
	}catch (Exception $e) {
		die("<br>".$e."<br>");
		return false;
	}
}
?>