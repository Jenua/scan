<?php
// This is form handler to create new order in shipping module system manually.
require_once('functions.php');
$path = $_SERVER['DOCUMENT_ROOT'];
require_once($path.'/db_connect/connect.php');

		/*print_r('<pre>');
		print_r($_POST);
		print_r('</pre>');
		die('stop');*/
		
if (isset($_POST) && !empty($_POST)) //check if data posted
	{
		$data = prepare_array();
		$data = add_keys($data);
		
		/*print_r('<pre>');
		print_r($data);
		print_r('</pre>');
		die('stop');*/
		
		$ShiptrackQuery = generateShiptrackQuery($data, $data['User']);
		$GroupdetailQueries = Array();
		$LinedetailQueries = Array();
		foreach($data['product'] as $product)
		{
			$GroupdetailQueries[]= generateGroupdetailQuery($product);
			foreach($product['item'] as $item)
			{
				$LinedetailQueries[]= generateLinedetailQuery($item);
			}
		}
		if(empty($ShiptrackQuery)
			|| empty($GroupdetailQueries)
			|| empty($LinedetailQueries)
			|| count($GroupdetailQueries) > count($LinedetailQueries)
		) die('<br>Error! Wrong number of queries generated.<br>');
		else
		{
			$all_queries = Array($ShiptrackQuery);
			$all_queries = array_merge($all_queries, $GroupdetailQueries);
			$all_queries = array_merge($all_queries, $LinedetailQueries);
			
			$conn = Database::getInstance()->dbc;
			
			/*print_r('<pre>');
			print_r($all_queries);
			print_r('</pre>');
			die('stop');*/
			
			$result = beginTransaction($conn);
			if ($result)
			{
				foreach ($all_queries as $query)
				{
					$result = runQuery_empty_result($conn, $query);
					if (!$result)
					{
						rollbackTransaction($conn);
						die('Error. Can not run query: '.$query.'');
					}
				}
				$result = commitTransaction($conn);
				if (!$result)
				{
					rollbackTransaction($conn);
					die('Error. Can not commit transaction.');
				} else 
				{
					echo 'New order was successfully saved. PO: ';
					echo "<a target='_blank' href='/shipping_module/order_status_history.php?po=".urlencode($data['PONumber'])."&id=".urlencode($data['RefNumber'])."'>".$data['PONumber']."</a>";
				}
			} else die('Error. Can not begin transaction.');
		}
		$conn = null;
	} else die("<br>Can not get form data.<br>");
	
function main($array)
{
	//$conn = getConnection(); //connect to DB
	
}
?>