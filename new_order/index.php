<?php
$header_name = "Create new order";
require_once( "../shipping_module/header.php" );
require_once('functions.php');
?>
  <link rel="stylesheet" href="Include/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="Include/bootstrap/css/bootstrap-theme.min.css">
  <link href="Include/jquery-ui-1.11.4.custom/themes/black-tie/jquery-ui.css" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="Include/style/style.css">

  <script src="Include/bootstrap/js/bootstrap.min.js"></script>
  <script src="Include/bootstrap-validator-master/js/validator.js"></script>
  <script type="text/javascript" src="Include/jquery-ui-1.11.4.custom/jquery-ui.min.js"></script>
  <script>
  $(document).ready(function() {
  //$('#orderForm').validator();
  //item
  itemIndex = 0;
        $('#orderForm').on('click', '.addButton', function() {
            itemIndex++;
            var $template = $('#itemTemplate'),
                $clone    = $template
                                .clone()
                                .removeClass('hide')
                                .removeAttr('id')
                                .attr('data-item-index', itemIndex)
                                .insertBefore($template);

            $clone
				.find('[name="SKUTempItem"]').attr('name', 'item[' + itemIndex + '][SKU]').end()
                .find('[name="iWeightTemp"]').attr('name', 'item[' + itemIndex + '][iWeight]').end()
                .find('[name="iDescriptionTemp"]').attr('name', 'item[' + itemIndex + '][iDescription]').autocomplete({
					source: "phpFunctions/autocomplete/iDescription.php",
					minLength: 1
				}).end()
                .find('[name="iQuantityTemp"]').attr('name', 'item[' + itemIndex + '][iQuantity]').end();

        }).on('click', '.removeButton', function() {
            var $row  = $(this).parents('.group'),
                index = $row.attr('data-item-index');
				console.log('remove');

            $row.remove();
        });

  //product
  productIndex = 0;
        $('#orderForm').on('click', '.addButtonProduct', function() {

			productIndex++;

			var $template2 = $('#productTemplate'),
                $clone2    = $template2
                                .clone()
                                .removeClass('hide')
                                .removeAttr('id')
                                .attr('data-product-index', productIndex)
                                .insertBefore($template2);

            $clone2
                .find('[name="SKUTemp"]').attr('name', 'product[' + productIndex + '][SKU]').change(function() { updateProductSKUSelect() }).autocomplete({
					source: "phpFunctions/autocomplete/sku.php",
					minLength: 1,
					change: function () {
						updateProductSKUSelect();
					}
				}).end()
                .find('[name="pDescriptionTemp"]').attr('name', 'product[' + productIndex + '][pDescription]').autocomplete({
					source: "phpFunctions/autocomplete/pDescription.php",
					minLength: 1
				}).end()
                .find('[name="pQuantityTemp"]').attr('name', 'product[' + productIndex + '][pQuantity]').end();


        }).on('click', '.removeButtonProduct', function() {
            var $row  = $(this).parents('.group'),
                index = $row.attr('data-product-index');
				console.log('remove');

            $row.remove();
			updateProductSKUSelect();
        });
	
	//item/product relation
	$( '.productSKU' ).autocomplete({
					change: function () {
						updateProductSKUSelect();
					}
				});

	//Submit event with prevalidation
	$('#orderForm').validator().on('submit', function (e) {
	  if (e.isDefaultPrevented()) {
		alert('Form is not valid.');
	  } else {
		event.preventDefault();
		var form = $('#orderForm');
		var url = $(form).attr("action");
		var formData = {};
		/*$(form).find("input[name]").each(function (index, node) {
			formData[node.name] = node.value;
		});*/
		formData = $(form).serialize();
		$.ajax({
			url: url,
			type: 'POST',
			data: formData,
			beforeSend : function(){ displayModal(); displayLoading(); },
			complete: function(data){
				//console.log(data.responseText);
				//alert(data.responseText);
				hideLoading();
				displayInPopup(data.responseText);
				runShipsingle();
			},
			error: function (request, status, error) {
				console.log("Ajax request error: "+request.responseText);
			}
		});
	  }
	});
	
	$(function() {
		$("#CustomerFullName").autocomplete({
			source: "phpFunctions/autocomplete/dealer.php",
			minLength: 1
		});
		$("#ShipMetdod").autocomplete({
			source: "phpFunctions/autocomplete/carrier.php",
			minLength: 1
		});
		$(".pDescription").autocomplete({
			source: "phpFunctions/autocomplete/pDescription.php",
			minLength: 1
		});
		$(".iDescription").autocomplete({
			source: "phpFunctions/autocomplete/iDescription.php",
			minLength: 1
		});
		$("#ShipToCity").autocomplete({
			source: "phpFunctions/autocomplete/city.php",
			minLength: 1
		});
		$("#ShipToPersonName").autocomplete({
			source: "phpFunctions/autocomplete/addr1.php",
			minLength: 1
		});
		$(".productSKU").autocomplete({
			source: "phpFunctions/autocomplete/sku.php",
			minLength: 1
		});
	});
});

function updateProductSKUSelect()
{
	console.log('updateProductSKUSelect');
	var options = new Array();
	$( '.productSKU' ).each(function() {
		if ($(this).val())
		{
			//options = options + '<option value="'+$(this).val()+'">'+$(this).val()+'</option>';
			options.push($(this).val());
		}
	});
	if (options)
	{
		$('.itemSKU')
		.find('option')
		.remove()
		.end();
		
		$.each(options, function (i, option) {
			$('.itemSKU').append($('<option>', { 
				value: option,
				text : option 
			}));
		});
	} else
	{
		$('.itemSKU')
		.find('option')
		.remove()
		.end();
	}
}

function runShipsingle()
{
	console.log('shipsingle started.');
	$.ajax({
		url: '/shipsingle/shipsingle.php',
		type: 'POST',
		complete: function(data){
			console.log(data.responseText);
			console.log('shipsingle finished.');
		},
		error: function (request, status, error) {
			console.log("Ajax request error: "+request.responseText);
		}
	});
}

function displayLoading()
{
	$('.loading').fadeIn(100);
}

function hideLoading()
{
	$('.loading').fadeOut(100);
}

function displayModal()
{
	$('.darkBack').fadeIn(100);
}

function hideModal()
{
	$('.darkBack').fadeOut(100);
}

function displayInPopup(content)
{
	$('.popup').fadeIn(100);
	$('.content').html(content);
}

function hidePopup()
{
	$('.popup').fadeOut(100);
	hideModal();
	$('.content').html('');
}
  </script>
</head>
<body>
<?php
if ($auth->isAuth()) {
?>
    <?php require_once($path.'/Include/header_section.php');?>
<div class="container">
<?php 
	$user = $auth->getName();
	echo "<div style='position: absolute; left: 50%; margin-left: -160px; top: 0px; width: 500px; height: 20px;'>Hello, " . $user .". <a href='?is_exit=1'><button class='loginsubmit'>Exit</button></a></div>";
	?>
	<form data-toggle="validator" role="form" id="orderForm" action="handler.php" method="POST">
	
	<div class="group">
		<h2>Products</h2>
		
		<div class="form-group">
			<label for="SKU">Product SKU</label>
			<input type="text" class="form-control productSKU" name="product[0][SKU]" placeholder="GSHDR-244657210-06" required>
			<span class="help-block with-errors">SKU code of the product</span>
		</div>
		
		<div class="form-group">
			<label for="pDescription">Product Description</label>
			<textarea class="form-control pDescription" rows="1" name="product[0][pDescription]" placeholder="DreamLine Unidoor Plus 46-1/2 to 47 in. W x 72 in. H Hinged Shower Door, Oil Rubbed Bronze" required></textarea>
			<span class="help-block with-errors">Text description of the product</span>
		</div>
		
		<div class="form-group">
			<label for="pQuantity">Product Quantity</label>
			<input type="number" class="form-control" name="product[0][pQuantity]" placeholder="2" min="1" max="999" required>
			<span class="help-block with-errors">Number from 1 to 999</span>
		</div>
		
		<button type="button" class="btn btn-primary addButtonProduct">+Add Product</button>
	</div>
	
	<div class="group hide" id="productTemplate">
		<div class="form-group">
			<label for="SKU">Product SKU</label>
			<input type="text" class="form-control productSKU" name="SKUTemp" placeholder="GSHDR-244657210-06" required>
			<span class="help-block with-errors">SKU code of the product</span>
		</div>
		
		<div class="form-group">
			<label for="pDescription">Product Description</label>
			<textarea class="form-control pDescription" rows="1" name="pDescriptionTemp" placeholder="DreamLine Unidoor Plus 46-1/2 to 47 in. W x 72 in. H Hinged Shower Door, Oil Rubbed Bronze" required></textarea>
			<span class="help-block with-errors">Text description of the product</span>
		</div>
		
		<div class="form-group">
			<label for="pQuantity">Product Quantity</label>
			<input type="number" class="form-control" name="pQuantityTemp" placeholder="2" min="1" max="999" required>
			<span class="help-block with-errors">Number from 1 to 999</span>
		</div>
		
		<button type="button" class="btn btn-warning removeButtonProduct">-Remove Product</button>
	</div>
	
	<div class="group">
		<h2>Items</h2>
		
		<div class="form-group">
			<label for="itemSKU">Product SKU</label>
			<select name="item[0][SKU]" class="form-control itemSKU">
			</select>
			<span class="help-block with-errors">Select the product to which this item belongs</span>
		</div>
		
		<div class="form-group">
			<label>Item Weight</label>
			<input type="number" class="form-control" name="item[0][iWeight]" placeholder="60" min="0" max="999" required>
			<span class="help-block with-errors">Number from 0 to 999</span>
		</div>
		
		<div class="form-group">
			<label>Item Description</label>
			<textarea class="form-control iDescription" name="item[0][iDescription]" rows="1" placeholder="SHOWER-DOORS:SHOWER-DOOR-GLASS:SHDR-GL2001-247210" required></textarea>
			<span class="help-block with-errors">Text description of the product</span>
		</div>
		
		<div class="form-group">
			<label>Item Quantity</label>
			<input type="number" class="form-control" name="item[0][iQuantity]" placeholder="2" min="1" max="999" required>
			<span class="help-block with-errors">Number from 1 to 999</span>
		</div>
		
		<button type="button" class="btn btn-primary addButton">+Add Item</button>
	</div>
	
	<div class="group hide" id="itemTemplate">
		<div class="form-group">
			<label for="itemSKU">Product SKU</label>
			<select name="SKUTempItem" class="form-control itemSKU">
			</select>
			<span class="help-block with-errors">Select the product to which this item belongs</span>
		</div>
	
		<div class="form-group">
			<label>Item Weight</label>
			<input type="number" class="form-control" name="iWeightTemp" placeholder="60" min="0" max="999" required>
			<span class="help-block with-errors">Number from 0 to 999</span>
		</div>
		
		<div class="form-group">
			<label>Item Description</label>
			<textarea class="form-control iDescription" name="iDescriptionTemp" rows="1" placeholder="SHOWER-DOORS:SHOWER-DOOR-GLASS:SHDR-GL2001-247210" required></textarea>
			<span class="help-block with-errors">Text description of the product</span>
		</div>
		
		<div class="form-group">
			<label>Item Quantity</label>
			<input type="number" class="form-control" name="iQuantityTemp" placeholder="2" min="1" max="999" required>
			<span class="help-block with-errors">Number from 1 to 999</span>
		</div>
		
		<button type="button" class="btn btn-warning removeButton">-Remove Item</button>
	</div>
	
	<div class="group">
		<h2>Order Data</h2>
		
		<div class="form-group">
			<label for="CustomerFullName">Customer Full Name</label>
			<input type="text" class="form-control" id="CustomerFullName" name="CustomerFullName" placeholder="Lowe`s CUS:1605" data-minlength="3" required>
			<span class="help-block with-errors">Dealer or customer full name</span>
		</div>
		
		<div class="form-group">
			<label for="ShipToPersonName">Address 1</label>
			<input type="text" class="form-control" id="ShipToPersonName" name="Addr1" placeholder="Amazon.com.dedc, LLC" data-minlength="3"  required>
			<span class="help-block with-errors">Address field 1</span>
		</div>
		
		<div class="form-group">
			<label for="ShipToStoreName">Address 2</label>
			<input type="text" class="form-control" id="ShipToStoreName" name="Addr2" placeholder="500 McCarthy Dr." data-minlength="3" required>
			<span class="help-block with-errors">Address field 2</span>
		</div>
		
		<div class="form-group">
			<label for="ShipToAddress">Address 3</label>
			<input type="text" class="form-control" id="ShipToAddress" name="Addr3" placeholder="Fairview Business Park">
			<span class="help-block with-errors">Address field 3</span>
		</div>
		
		<div class="form-group">
			<label for="ShipToCity">Ship To City</label>
			<input type="text" class="form-control" id="ShipToCity" name="ShipToCity" placeholder="Kalispell" data-minlength="3" required>
			<span class="help-block with-errors">City name</span>
		</div>
		
		<div class="form-group">
			<label for="ShipToState">Ship To State</label>
			<input type="text" class="form-control" id="ShipToState" size="2" maxlength="2" style="text-transform:uppercase" name="ShipToState" placeholder="AL" data-minlength="2" required>
			<span class="help-block with-errors">Two letters state code</span>
		</div>
		
		<div class="form-group">
			<label for="ShipToPostalCode">Ship To Postal Code</label>
			<input type="text" class="form-control" id="ShipToPostalCode" name="ShipToPostalCode" placeholder="42906" pattern="^(\d{5}(-\d{4})?|[A-CEGHJ-NPRSTVXY]\d[A-CEGHJ-NPRSTV-Z] ?\d[A-CEGHJ-NPRSTV-Z]\d)$" required>
			<span class="help-block with-errors">USA or Canada postal code</span>
		</div>
		
		<div class="form-group">
			<label for="ShipToCountry">Ship To Country</label>
			<select id="ShipToCountry" name="ShipToCountry" class="form-control">
				<option value="US">USA</option>
				<option value="CA">Canada</option>
			</select>
			<span class="help-block with-errors">Choose country from the list</span>
		</div>
		
		<div class="form-group">
			<label for="PONumber">PO Number</label>
			<input type="text" class="form-control" id="PONumber" name="PONumber" placeholder="MCOM31464701" data-minlength="3" required>
			<span class="help-block with-errors">Purchase order number</span>
		</div>
		
		<div class="form-group">
			<label for="PhoneNumber">Phone Number</label>
			<input type="text" class="form-control" id="PhoneNumber" size="10" maxlength="10" name="PhoneNumber" placeholder="7358555785" data-minlength="10" required>
			<span class="help-block with-errors">10 digits phone number</span>
		</div>
		
		<div class="form-group">
			<label for="ShipMetdod">Ship Method</label>
			<input type="text" class="form-control" id="ShipMetdod" name="ShipMetdod" placeholder="ESTES EX - PROC" data-minlength="2" required>
			<span class="help-block with-errors">Proposed name of the carrier</span>
			<label for="force_carrier"><input type="checkbox" name="force_carrier" id="force_carrier" value="force"> Force Ship Method as a carrier for this order</label><br>
		</div>
		
		<div class="form-group">
			<label for="Memo">Memo</label>
			<input type="text" class="form-control" id="Memo" name="Memo" placeholder="06690079, John Smith, , MCC, ,  , , W398428856, LTL CURBSIDE" data-minlength="3" required>
			<span class="help-block with-errors">Comma separated values as in the dB</span>
		</div>
		
		<input type="hidden" name="User" value="<?php echo $user; ?>">
		
	</div>
	<button type="submit" class="btn btn-success">Submit</button>
	<button type="button" onClick="location.reload();" class="btn btn-default">Reset</button><br><br>
	</form><br>
</div>
<?php
}
?>
<div class="darkBack"></div>
<div class="loading"><img src="Include/img/294.gif" width="128px" height="128px"></div>
<div class="popup">
	<div class="content">
	</div>
	<center><button class="okButton" onclick="hidePopup();">OK</button></center>
</div>
<?php require_once($path.'/Include/footer_section.php');?>