<?php
$request = array(
 "order_data" => array(
  "TxnID" => "1095073-1454397784", //1095073-1454397784
  "RefNumber" => "3474815",
  "PONumber" => "166146072",
  "ship_method" => "R&L",
  "weight" => '1270', // OPTIONAL if isset -- use, if not -- calculate 1270
  "boxes" => 12, 
  "pallets" => 1, 
  "freight_class" => '756',
  "liftgate" => true,
  "residential" => false,
  "shipping_ground" => false // OPTIONAL if isset -- use, if not -- calculate
 ),
 
 "ship_from_data" => array(
  "name_from" => "Bathauthority",
  "address_from" => "75 Hawk Rd",
  "city_from" => "Warminster",
  "state_from" => "PA",//"PA",
  "zip_from" => "18974",//"18974",
  "country_from" => "US"
 ),
 
 "ship_to_data" => array(
  "dealer" => "Lowe's CUS:1033",
  "customer" => "Valerie Urchell",
  "address_to" => "164 New County Road",
  "city_to" => "Williamsburg",
  "state_to" => "CA",
  "zip_to" => "89014",
  "country_to" => "US",
  "phone_to" => "6306212092"
 ),
 
 "product_data" => array(
  "0" => array(
   "SKU" => "SHDR-61607610-07",
   "quantity" => 1,
   "item_data" => array(
    "0" => array(
     "item_SKU" => "SHDR-61607610-08",
     "weight" => 60,
     "quantity" => 1
    ),
    "1" => array(
     "item_SKU" => "SHDR-61607610-08",
     "weight" => 70,
     "quantity" => 1
    ),
    "2" => array(
     "item_SKU" => "SHDR-61607610-08",
     "weight" => 30,
     "quantity" => 2
    )
   )
  ),
  "1" => array(
   "SKU" => "SHDR-61607610-08",
   "quantity" => '3',
   "item_data" => array(
    "0" => array(
     "item_SKU" => "SHDR-61607610-08",
     "weight" => 50,
     "quantity" => 1
    )
   )
  )
 ),
 
 "rate_data" => array(
  "carriers" => array(
   //*
   "0" => array(
    "name" => "FEDEX-ECONOMYs"
   ),
   
   "1" => array(
    "name" => "UPS"
   ),
   
   "2" => array(
    "name" => "RL"
   ),
   //*/
   "3" => array(
    "name" => "FEDEX-PRIORITY"
   )
   /*//*/
  ),
 ),
 "timestamp" => "",
 "messages" => ['code' => '199999']
);              

$newShipsingleUrl = 'http://shipsingle.local/api/rate';
$data_string = json_encode($request); 
$s = curl_init(); 
curl_setopt($s, CURLOPT_URL, $newShipsingleUrl); 
curl_setopt($s, CURLOPT_CUSTOMREQUEST, "GET");                                                                  
curl_setopt($s, CURLOPT_POSTFIELDS, $data_string);
curl_setopt($s, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($s, CURLOPT_CONNECTTIMEOUT, 15);
curl_setopt($s, CURLOPT_TIMEOUT, 15);
curl_setopt($s, CURLOPT_HTTPHEADER, array(                                                                          
 'Content-Type: application/json',                                                                                
 'Content-Length: ' . strlen($data_string))                                                                       
);
$result = curl_exec($s);
$error  = curl_error($s);

curl_close($s);

$r = json_decode( $result );


/*echo "<pre>".print_r($r,true)."</pre>";
echo "<br />";
echo $error;*/
echo "<pre>".print_r($result,true)."</pre>";
echo "<br />";
echo "<pre>".print_r($r,true)."</pre>";
echo "<br />";
/*echo "<pre>".print_r($request,true)."</pre>";
echo "<br />";*/
echo $error;