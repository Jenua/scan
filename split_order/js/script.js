$(document).ready(function() {
	orderIndex = 1;
        $('#orderForm').on('click', '.addButton', function() {
            var $template = $('#orderTemplate'),
                $clone    = $template
                                .clone()
                                .removeClass('hide')
                                .removeAttr('id')
                                .attr('data-item-index', orderIndex)
                                .insertBefore($template)
								.addClass('addedGroup');

            $clone
                .find('.quantity').each(function(i, n){
					$(n).attr('name', 'order[' + orderIndex + '][' + i + '][quantity]').removeClass('quantity');
				});
			$clone
                .find('.TxnLineID').each(function(i, n){
					$(n).attr('name', 'order[' + orderIndex + '][' + i + '][TxnLineID]').removeClass('TxnLineID');
				});
			$clone
                .find('.order_head').text('Splitted Order '+orderIndex);

            orderIndex++;
        }).on('click', '.removeButton', function() {
            var $row  = $(this).parents('.group'),
                index = $row.attr('data-item-index');
				console.log('remove');
            $row.remove();
        });
		 $('#orderForm').on('click', '.resetButton', function() {
			 location.reload();
		});
		
		//Submit event with prevalidation
	$('#orderForm').validator().on('submit', function (e) {
	  if (e.isDefaultPrevented()) {
		alert('Form is not valid.');
	  } else {
		event.preventDefault();
		var form = $('#orderForm');
		var url = $(form).attr("action");
		var formData = {};
		/*$(form).find("input[name]").each(function (index, node) {
			formData[node.name] = node.value;
		});*/
		formData = $(form).serialize();
		$.ajax({
			url: url,
			type: 'POST',
			data: formData,
			beforeSend : function(){ displayModal(); displayLoading(); },
			complete: function(data){
				//console.log(data.responseText);
				//alert(data.responseText);
				hideLoading();
				displayInPopup(data.responseText);
				runShipsingle();
			},
			error: function (request, status, error) {
				console.log("Ajax request error: "+request.responseText);
			}
		});
	  }
	});
});

function change_total(obj)
{
	var undef = $(obj).val();
	if (undef == "" || undef == null) $(obj).val(0);
	
	var input_class = $(obj).attr('class').split(' ')[1];
	
	var input_sum = 0;
	
	$('.'+input_class).each(function(){
		input_sum += parseInt($(this).val());
	});
	
	var original_quantity = parseInt($('#original_'+input_class).val());
	var total_quantity_left = original_quantity - input_sum;
	$('#'+input_class).val(total_quantity_left);
	
	$('.'+input_class).each(function(){
		var max_param = parseInt($(this).val()) + total_quantity_left;
		$(this).attr({'max' : max_param});
	});
	$('#orderForm').validator('validate');
	/*console.log(input_class);
	console.log(input_sum);
	console.log(original_quantity);
	console.log(total_quantity_left);*/
}

/*function submitForm(form){
    var url = $(form).attr("action");
    var formData = {};
    $(form).find("input[name]").each(function (index, node) {
        formData[node.name] = node.value;
    });
	$.ajax({
		url: url,
		type: 'POST',
		data: formData,
		beforeSend : function(){ displayModal(); displayLoading(); },
		complete: function(data){
			//console.log(data.responseText);
			//alert(data.responseText);
			hideLoading();
			displayInPopup(data.responseText);
			runShipsingle();
		},
		error: function (request, status, error) {
			console.log("Ajax request error: "+request.responseText);
		}
	});
}*/

function runShipsingle()
{
	console.log('shipsingle started.');
	$.ajax({
		url: '/shipsingle/shipsingle.php',
		type: 'POST',
		complete: function(data){
			console.log(data.responseText);
			console.log('shipsingle finished.');
		},
		error: function (request, status, error) {
			console.log("Ajax request error: "+request.responseText);
		}
	});
}

function displayLoading()
{
	$('.loading').fadeIn(100);
}

function hideLoading()
{
	$('.loading').fadeOut(100);
}

function displayModal()
{
	$('.darkBack').fadeIn(100);
}

function hideModal()
{
	$('.darkBack').fadeOut(100);
}

function displayInPopup(content)
{
	$('.popup').fadeIn(100);
	$('.content').html(content);
}

function hidePopup()
{
	$('.popup').fadeOut(100);
	hideModal();
	$('.content').html('');
}