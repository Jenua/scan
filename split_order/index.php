<?php
$header_name = "Split Order";
require_once( "../shipping_module/header.php" );
require_once('functions.php');

if ($auth->isAuth()) {

if (isset($_GET['id']) && !empty($_GET['id']))
{
	$po = $_GET['id'];
	$conn = Database::getInstance()->dbc;
	$query = "SELECT [groupdetail].*, [shiptrack].[RefNumber], [shiptrack].[PONumber]
  FROM [dbo].[groupdetail]
  LEFT JOIN [shiptrack] ON [groupdetail].IDKEY = [shiptrack].TxnID
  WHERE [shiptrack].[RefNumber] = '".$po."'";
	$results = runQuery($conn, $query);
	if (!isset($results) || empty($results)) die('<br>Can not get data on this ID: '.$po.'<br>');
	foreach ($results as $key => $result)
	{
		if (!isset($result['Quantity']))
		{
			$results[$key]['Quantity'] = 1;
		}
			
		if (!isset($result['Prod_desc']) || 
		!isset($result['TxnLineID'])) die('<br>Order data is not full.<br>Please check missing data.<br>');
	}
	
	$results = exclude_splitted_before($conn, $po, $results);
	$conn = null;
	
	
	/*print_r("<pre>");
	print_r($results);
	print_r("</pre>");
	die();*/
} else die('<br>Can not get ID.<br>');
?>

  <title><?=$header_name?></title>
  <link rel="stylesheet" href="Include/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="Include/bootstrap/css/bootstrap-theme.min.css">


  <script src="Include/bootstrap/js/bootstrap.min.js"></script>

  <script src="js/script.js"></script>
  <link rel="stylesheet" href="Include/style/style.css">
  <style>
	.bold_red_text
	{
		font-weight: bold;
		color: red;
	}
  </style>
</head>
<body>
<?php require_once($path.'/Include/header_section.php'); ?>
<div class="container">
	<?php 
	$user = $auth->getName();
	echo "<div style='position: absolute; left: 50%; margin-left: -160px; top: 0px; width: 500px; height: 20px;'>Hello, " . $user .". <a href='?is_exit=1'><button class='loginsubmit'>Exit</button></a></div>";
	?>
	<form data-toggle="validator" role="form" id="orderForm" action="handler.php?po=<?php echo $results[0]['RefNumber']; ?>" method="POST">
	
	<div class="group">
	<h4>Original Order</h4>
	<table data-toggle='table' class='table table-bordered display table-striped'>
		<thead>
			<tr class='info'>
				<th>Product SKU</th>
				<th>Product name</th>
				<th>Quantity</th>
				<th>Quantity Left To Distribute</th>
			</tr>
		</thead>
		<tbody>
			<?php
			$i = 0;
			foreach ($results as $result)
			{
				echo 
				"
				<tr>
					<td>".$result['ItemGroupRef_FullName']."</td>
					<td>".$result['Prod_desc']."</td>
					<td name='original_order[".$i."][Quantity]'>
						<div class='form-group'>
							<input type='number' class='form-control' id='original_order".$i."' value='".$result['Quantity']."' disabled>
						</div>
					</td>
					<td name='original_order[".$i."][Quantity]'>
						<div class='form-group'>
							<input type='number' class='form-control' id='order".$i."' value='".$result['Quantity']."' disabled>
						</div>
					</td>
				</tr>
				";
				$i++;
			}
			?>
		</tbody>
	</table>
	
	<button type="button" class="btn btn-primary addButton">+Add Order</button>
	</div>
	
	<div class="group hide" id="orderTemplate">
	<br>
	<h4 class="order_head"></h4>
	<table data-toggle='table' class='table table-bordered display table-striped'>
		<thead>
			<tr class='info'>
				<th>Product SKU</th>
				<th>Product name</th>
				<th>Quantity</th>
			</tr>
		</thead>
		<tbody>
			<?php
			$i = 0;
			foreach ($results as $result)
			{
				echo 
				"
				<tr>
					<td>".$result['ItemGroupRef_FullName']."</td>
					<td>".$result['Prod_desc']."</td>
					<td>
						<div class='form-group'>
							<input onchange='change_total(this)' type='number' class='form-control quantity order".$i."' value='0' min='0' max='".$result['Quantity']."'>
							<input type='hidden' class='TxnLineID' value='".$result['TxnLineID']."'>
							<input type='hidden' name='UserName' value='".$user."'>
						</div>
					</td>
				</tr>
				";
				$i++;
			}
			?>
		</tbody>
	</table>
	
	<button type="button" class="btn btn-warning removeButton">-Remove Order</button>
	</div>
	
	<br>
	<button type="submit" class="btn btn-success">Submit</button>
	<button type="button" class="btn btn-default resetButton">Reset</button><br><br>
	</form><br>
</div>
<?php
}
?>
<div class="darkBack"></div>
<div class="loading"><img src="Include/img/294.gif" width="128px" height="128px"></div>
<div class="popup">
	<div class="content">
	</div>
	<center><button class="okButton" onclick="hidePopup();">OK</button></center>
</div>
</body>
</html>