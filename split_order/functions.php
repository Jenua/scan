<?php
$path = $_SERVER['DOCUMENT_ROOT'];
require_once($path.'/db_connect/connect.php');

function exclude_splitted_before($conn, $id, $datas)
{
	$query = "SELECT 
	[manually_groupdetail].[ItemGroupRef_FullName],
	sum(cast([manually_groupdetail].[Quantity] AS INT)) AS quantity_sum
	FROM [manually_groupdetail]
  LEFT JOIN [manually_shiptrack] ON [manually_groupdetail].IDKEY = [manually_shiptrack].TxnID
  WHERE [manually_shiptrack].[RefNumber] like '".$id."_split_%'
  GROUP BY [manually_groupdetail].[ItemGroupRef_FullName]";
	$results = runQuery($conn, $query);
	if ($results)
	{
		foreach ($datas as $key => $data)
		{
			foreach ($results as $result)
			{
				if (empty($datas[$key]['ItemGroupRef_FullName'])) die('<br>Error. Empty ItemGroupRef_FullName.<br>');
				if ($data['ItemGroupRef_FullName'] == $result['ItemGroupRef_FullName'])
				{
					$datas[$key]['Quantity']-= $result['quantity_sum'];
					if ($datas[$key]['Quantity'] < 0) die('<br>Error. Negative amount of products.<br>');
				}
			}
		}
	}
	/*print_r("<pre>");
	print_r($datas);
	print_r($results);
	print_r("</pre>");
	die();*/
	return $datas;
}

function remove_zero_rows($data)
{
	foreach ($data as $key => $value)
	{
		$notZero = false;
		foreach ($value as $key1 => $value1)
		{
			if ($value1['quantity'] == 0) unset($data[$key][$key1]); else $notZero = true;
		}
		if (!$notZero) unset($data[$key]);
	}
	$data = array_values($data);
	return $data;
}

function get_uid()
{
	$uid = uniqid('split_', true);
	return $uid;
}

function get_urn($rn)
{
	$urn = uniqid($rn.'_split_', true);
	return $urn;
}

function get_upo($conn, $po, $key, $upos)
{
	$query = "SELECT [PONumber]
	  FROM [dbo].[manually_shiptrack]
	  WHERE [PONumber] LIKE '".$po."_split_%'";
	$results = runQuery($conn, $query);
	if (!empty($upos))
	{
		foreach ($upos as $key => $upo)
		{
			$upo = explode('_split_', $upo);
			$upo = $upo[1];
			$upos[$key] = $upo;
		}
		$max_index = max($upos);
	}else{
		$max_index = 0;
	}
	if (!empty($results))
	{
		foreach ($results as $result)
		{
			$indexes = explode('_split_', $result['PONumber']);
			$index = $indexes[1];
			if ($index > $max_index) $max_index = $index;
		}
	}
	++$max_index;
	/*print_r("<pre>");
	print_r($results);
	print_r($max_index);
	print_r("</pre>");
	die();*/
	//$upo = $po.'_split_'.$key;
	//$upo = uniqid($po.'_split_', true);
	$upo = $po.'_split_'.$max_index;
	return $upo;
}

function generate_log_queries($POnumber, $RefNumber, $Divided_into_POnumber, $Divided_into_RefNumber, $userName)
{
	$query = "INSERT INTO [dbo].[Orders_divided]
           ([POnumber]
           ,[RefNumber]
           ,[Divided_into_POnumber]
           ,[Divided_into_RefNumber]
           ,[DivideDate]
           ,[userName])
     VALUES
           ('".$POnumber."'
           ,'".$RefNumber."'
           ,'".$Divided_into_POnumber."'
           ,'".$Divided_into_RefNumber."'
           ,GETDATE()
           ,'".$userName."')";
	return $query;
}

function addquote($str)
{
	$str = str_replace("'", "`", $str);
	return $str;
}

function generate_shiptrack_queries($conn, $data, $shiptrack, $user)
{
	$queries = array();
	$log_queries = array();
	$TxnIDs = array();
	$upos = array();
	$urns = array();
	foreach ($data as $key => $value)
	{
		$TxnID = get_uid();
		$upo = get_upo($conn, $shiptrack['PONumber'], $key, $upos);
		//$urn = get_upo($conn, $shiptrack['RefNumber'], $key, $upos);
		$urn = get_urn($shiptrack['RefNumber']);
		$queries[]= "INSERT INTO [dbo].[".MANUALLY_SHIPTRACK_TABLE."]
			   ([TxnID]
			   ,[TimeCreated]
			   ,[TimeModified]
			   ,[EditSequence]
			   ,[TxnNumber]
			   ,[CustomerRef_ListID]
			   ,[CustomerRef_FullName]
			   ,[ClassRef_ListID]
			   ,[ClassRef_FullName]
			   ,[TemplateRef_ListID]
			   ,[TemplateRef_FullName]
			   ,[TxnDate]
			   ,[RefNumber]
			   ,[BillAddress_Addr1]
			   ,[BillAddress_Addr2]
			   ,[BillAddress_Addr3]
			   ,[BillAddress_Addr4]
			   ,[BillAddress_Addr5]
			   ,[BillAddress_City]
			   ,[BillAddress_State]
			   ,[BillAddress_PostalCode]
			   ,[BillAddress_Country]
			   ,[BillAddress_Note]
			   ,[ShipAddress_Addr1]
			   ,[ShipAddress_Addr2]
			   ,[ShipAddress_Addr3]
			   ,[ShipAddress_Addr4]
			   ,[ShipAddress_Addr5]
			   ,[ShipAddress_City]
			   ,[ShipAddress_State]
			   ,[ShipAddress_PostalCode]
			   ,[ShipAddress_Country]
			   ,[ShipAddress_Note]
			   ,[PONumber]
			   ,[TermsRef_ListID]
			   ,[TermsRef_FullName]
			   ,[DueDate]
			   ,[SalesRepRef_ListID]
			   ,[SalesRepRef_FullName]
			   ,[FOB]
			   ,[ShipDate]
			   ,[ShipMethodRef_ListID]
			   ,[ShipMethodRef_FullName]
			   ,[Subtotal]
			   ,[ItemSalesTaxRef_ListID]
			   ,[ItemSalesTaxRef_FullName]
			   ,[SalesTaxPercentage]
			   ,[SalesTaxTotal]
			   ,[TotalAmount]
			   ,[IsManuallyClosed]
			   ,[IsFullyInvoiced]
			   ,[Memo]
			   ,[CustomerMsgRef_ListID]
			   ,[CustomerMsgRef_FullName]
			   ,[IsToBePrinted]
			   ,[IsToBeEmailed]
			   ,[CustomerSalesTaxCodeRef_ListID]
			   ,[CustomerSalesTaxCodeRef_FullName]
			   ,[Other]
			   ,[LinkedTxn]
			   ,[CustomField1]
			   ,[CustomField2]
			   ,[CustomField3]
			   ,[CustomField4]
			   ,[CustomField5]
			   ,[CustomField6]
			   ,[CustomField7]
			   ,[CustomField8]
			   ,[CustomField9]
			   ,[CustomField10]
			   ,[Status])
		 VALUES
			   ('".addquote($TxnID)."'
			   ,GETDATE()
			   ,GETDATE()
			   ,'".addquote($shiptrack['EditSequence'])."'
			   ,".$shiptrack['TxnNumber']."
			   ,'".addquote($shiptrack['CustomerRef_ListID'])."'
			   ,'".addquote($shiptrack['CustomerRef_FullName'])."'
			   ,'".addquote($shiptrack['ClassRef_ListID'])."'
			   ,'".addquote($shiptrack['ClassRef_FullName'])."'
			   ,'".addquote($shiptrack['TemplateRef_ListID'])."'
			   ,'".addquote($shiptrack['TemplateRef_FullName'])."'
			   ,'".addquote($shiptrack['TxnDate'])."'
			   ,'".$urn."'
			   ,'".addquote($shiptrack['BillAddress_Addr1'])."'
			   ,'".addquote($shiptrack['BillAddress_Addr2'])."'
			   ,'".addquote($shiptrack['BillAddress_Addr3'])."'
			   ,'".addquote($shiptrack['BillAddress_Addr4'])."'
			   ,'".addquote($shiptrack['BillAddress_Addr5'])."'
			   ,'".addquote($shiptrack['BillAddress_City'])."'
			   ,'".addquote($shiptrack['BillAddress_State'])."'
			   ,'".addquote($shiptrack['BillAddress_PostalCode'])."'
			   ,'".addquote($shiptrack['BillAddress_Country'])."'
			   ,'".addquote($shiptrack['BillAddress_Note'])."'
			   ,'".addquote($shiptrack['ShipAddress_Addr1'])."'
			   ,'".addquote($shiptrack['ShipAddress_Addr2'])."'
			   ,'".addquote($shiptrack['ShipAddress_Addr3'])."'
			   ,'".addquote($shiptrack['ShipAddress_Addr4'])."'
			   ,'".addquote($shiptrack['ShipAddress_Addr5'])."'
			   ,'".addquote($shiptrack['ShipAddress_City'])."'
			   ,'".addquote($shiptrack['ShipAddress_State'])."'
			   ,'".addquote($shiptrack['ShipAddress_PostalCode'])."'
			   ,'".addquote($shiptrack['ShipAddress_Country'])."'
			   ,'".addquote($shiptrack['ShipAddress_Note'])."'
			   ,'".$upo."'
			   ,'".addquote($shiptrack['TermsRef_ListID'])."'
			   ,'".addquote($shiptrack['TermsRef_FullName'])."'
			   ,'".addquote($shiptrack['DueDate'])."'
			   ,'".addquote($shiptrack['SalesRepRef_ListID'])."'
			   ,'".addquote($shiptrack['SalesRepRef_FullName'])."'
			   ,'".addquote($shiptrack['FOB'])."'
			   ,'".addquote($shiptrack['ShipDate'])."'
			   ,'".addquote($shiptrack['ShipMethodRef_ListID'])."'
			   ,'".addquote($shiptrack['ShipMethodRef_FullName'])."'
			   ,".$shiptrack['Subtotal']."
			   ,'".addquote($shiptrack['ItemSalesTaxRef_ListID'])."'
			   ,'".addquote($shiptrack['ItemSalesTaxRef_FullName'])."'
			   ,'".addquote($shiptrack['SalesTaxPercentage'])."'
			   ,".$shiptrack['SalesTaxTotal']."
			   ,".$shiptrack['TotalAmount']."
			   ,".$shiptrack['IsManuallyClosed']."
			   ,".$shiptrack['IsFullyInvoiced']."
			   ,'".addquote($shiptrack['Memo'])."'
			   ,'".addquote($shiptrack['CustomerMsgRef_ListID'])."'
			   ,'".addquote($shiptrack['CustomerMsgRef_FullName'])."'
			   ,".$shiptrack['IsToBePrinted']."
			   ,".$shiptrack['IsToBeEmailed']."
			   ,'".addquote($shiptrack['CustomerSalesTaxCodeRef_ListID'])."'
			   ,'".addquote($shiptrack['CustomerSalesTaxCodeRef_FullName'])."'
			   ,'".addquote($shiptrack['Other'])."'
			   ,'".addquote($shiptrack['LinkedTxn'])."'
			   ,'".addquote($shiptrack['CustomField1'])."'
			   ,'".addquote($shiptrack['CustomField2'])."'
			   ,'".addquote($shiptrack['CustomField3'])."'
			   ,'".addquote($shiptrack['CustomField4'])."'
			   ,'".addquote($shiptrack['CustomField5'])."'
			   ,'".addquote($shiptrack['CustomField6'])."'
			   ,'".addquote($shiptrack['CustomField7'])."'
			   ,'".addquote($shiptrack['CustomField8'])."'
			   ,'".addquote($shiptrack['CustomField9'])."'
			   ,'".addquote($user)."'
			   ,'".addquote($shiptrack['Status'])."')";
		$TxnIDs[]=$TxnID;
		$upos[]= $upo;
		$urns[]= $urn;
		$log_query = generate_log_queries($shiptrack['PONumber'], $shiptrack['RefNumber'], $upo, $urn, $user);
		$log_queries[]= $log_query;
	}
	if (!empty($queries))
	{
		return array('queries' => $queries, 'TxnIDs' => $TxnIDs, 'upos' => $upos, 'urns' => $urns, 'log_queries' => $log_queries);
	} else {
		die('<br>Error. Can not generate shiptrack queries.<br>');
	}
}

function generate_groupdetail_queries($data, $groupdetail, $TxnIDs)
{
	$queries = array();
	$TxnLineIDs = array();
	$TxnLineIDs_old_and_new = array();
	foreach ($TxnIDs as $key0 => $value0)
	{
		foreach ($groupdetail as $key1 => $value1)
		{
			if (isset($data[$key0][$key1]['quantity']))
			{
				$TxnLineID = get_uid();
				$TxnLineIDs[]= $TxnLineID;
				$queries[]= "INSERT INTO [dbo].[".MANUALLY_GROUPDETAIL_TABLE."]
					   ([IDKEY]
					   ,[TxnLineID]
					   ,[ItemGroupRef_FullName]
					   ,[Prod_desc]
					   ,[Quantity])
				 VALUES
					   ('".$TxnIDs[$key0]."'
					   ,'".$TxnLineID."'
					   ,'".$value1['ItemGroupRef_FullName']."'
					   ,'".$value1['Prod_desc']."'
					   ,'".$data[$key0][$key1]['quantity']."')";
				$TxnLineIDs_old_and_new[$value0][$value1['TxnLineID']]= $TxnLineID ;
			}
		}
	}
	if (!empty($queries))
	{
		return array('queries' => $queries, 'TxnLineIDs' => $TxnLineIDs, 'TxnLineIDs_old_and_new' => $TxnLineIDs_old_and_new);
	} else {
		die('<br>Error. Can not generate groupdetail queries.<br>');
	}
}

function generate_linedetail_queries($data, $linedetail, $groupdetail, $TxnIDs, $TxnLineIDs, $TxnLineIDs_old_and_new)
{
	$queries = array();
	foreach ($TxnIDs as $TxnID_key => $TxnID_value)
	{
		foreach ($linedetail as $linedetail_key => $linedetail_value)
		{
			$ngq = get_new_group_quantity($data, $TxnID_key, $linedetail_value['GroupIDKEY']);
			if ($ngq)
			{
				$GroupIDKEY = $TxnLineIDs_old_and_new[$TxnID_value][$linedetail_value['GroupIDKEY']];
				$quantity = get_new_quantity($linedetail_value['GroupIDKEY'], $linedetail_value['quantity'], $groupdetail, $ngq);
				$TxnLineID = get_uid();
				$query = "INSERT INTO [dbo].[".MANUALLY_LINEDETAIL_TABLE."]
					   ([IDKEY]
					   ,[TxnLineID]
					   ,[CustomField8]
					   ,[CustomField9]
					   ,[ItemRef_FullName]
					   ,[Rate]
					   ,[quantity]
					   ,[GroupIDKEY])
				 VALUES
					   ('".$TxnID_value."'
					   ,'".$TxnLineID."'
					   ,'".$linedetail_value['CustomField8']."'
					   ,'".$linedetail_value['CustomField9']."'
					   ,'".$linedetail_value['ItemRef_FullName']."'
					   ,'".$linedetail_value['Rate']."'
					   ,'".$quantity."'
					   ,'".$GroupIDKEY."')";
				$queries[]=$query;
			}
		}
	}
	if (!empty($queries))
	{
		return $queries;
	} else {
		die('<br>Error. Can not generate linedetail queries.<br>');
	}
}

function get_new_quantity($GroupIDKEY, $olq, $groupdetail, $ngq)
{
	$ogq = get_original_group_quantity($groupdetail, $GroupIDKEY);
	$new_quantity = false;
	$multiplier = $olq/$ogq;
	$new_quantity = $ngq*$multiplier;
	if (!$new_quantity) die('<br>Error. Can not get new line quantity.<br>'); else 
	return $new_quantity;
}

function get_original_group_quantity($groupdetail, $GroupIDKEY)
{
	$original_group_quantity = false;
	foreach ($groupdetail as $groupdetail_key => $groupdetail_value)
	{
		if ($GroupIDKEY == $groupdetail_value['TxnLineID']) $original_group_quantity = $groupdetail_value['Quantity'];
	}
	if (!$original_group_quantity) die('<br>Error. Can not get original group quantity.<br>'); else return $original_group_quantity;
}

function get_new_group_quantity($data, $TxnID_key, $GroupIDKEY)
{
	$new_group_quantity = false;
	foreach ($data[$TxnID_key] as $key => $row)
	{
		if ($row['TxnLineID'] == $GroupIDKEY) $new_group_quantity = $row['quantity'];
	}
	return $new_group_quantity;
}

function get_shiptrack($conn, $po)
{
	$query = "SELECT [shiptrack].*
  FROM [dbo].[shiptrack]
  WHERE [shiptrack].[RefNumber] = '".$po."'";
	$result = runQuery($conn, $query);
	if ($result)
	{
		return $result;
	} else
	{
		die('<br>Error. Can not get shiptrack data.<br>');
	}
}

function get_linedetail($conn, $po)
{
	$query = "SELECT [linedetail].*
  FROM [dbo].[linedetail]
  LEFT JOIN [groupdetail] ON [linedetail].GroupIDKEY = [groupdetail].TxnLineID
  LEFT JOIN [shiptrack] ON [groupdetail].IDKEY = [shiptrack].TxnID
  WHERE [shiptrack].[RefNumber] = '".$po."'";
	$result = runQuery($conn, $query);
	if ($result)
	{
		return $result;
	} else
	{
		die('<br>Error. Can not get linedetail data.<br>');
	}
}

function get_groupdetail($conn, $po)
{
	$query = "SELECT [groupdetail].*
  FROM [dbo].[groupdetail]
  LEFT JOIN [shiptrack] ON [groupdetail].IDKEY = [shiptrack].TxnID
  WHERE [shiptrack].[RefNumber] =  '".$po."'";
	$result = runQuery($conn, $query);
	if ($result)
	{
		foreach ($result as $key => $res)
		{
			if (!isset($res['Quantity']))
			{
				$result[$key]['Quantity'] = 1;
			}
		}
		return $result;
	} else
	{
		die('<br>Error. Can not get groupdetail data.<br>');
	}
}

function getConnection()
{
	$conn = Database::getInstance()->dbc;
	return $conn;
}

function beginTransaction($conn)
{
	try
	{
		$result = $conn->beginTransaction();
		return $result;
	}catch (Exception $e) {
		die("<br>".$e."<br>");
		return false;
	}
}

function commitTransaction($conn)
{
	try
	{
		$result = $conn->commit();
		return $result;
	}catch (Exception $e) {
		die("<br>".$e."<br>");
		return false;
	}
}

function rollbackTransaction($conn)
{
	try
	{
		$result = $conn->rollback();
		return $result;
	}catch (Exception $e) {
		die("<br>".$e."<br>");
		return false;
	}
}

function runQuery($conn, $query)
{
	try
	{
		//echo "<br>".$query."<br>";
		$stmt = $conn->prepare($query);
		$result = $stmt->execute();
		$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}catch (Exception $e) {
		die("<br>".$e."<br>");
		return false;
	}
}

function runQuery_empty_result($conn, $query)
{
	try
	{
		//echo "<br>".$query."<br>";
		$stmt = $conn->prepare($query);
		$result = $stmt->execute();
		return $result;
	}catch (Exception $e) {
		die("<br>".$e."<br>");
		return false;
	}
}

function display_new_orders($conn, $upos)
{
	foreach ($upos as $upo)
	{
		echo "<br>";
		echo "PO: <b>".$upo."</b>";
		echo "<br>Linedetail";
		$query = "SELECT [manually_linedetail].*
		  FROM [dbo].[manually_linedetail]
		  LEFT JOIN [manually_groupdetail] ON [manually_linedetail].GroupIDKEY = [manually_groupdetail].TxnLineID
		  LEFT JOIN [manually_shiptrack] ON [manually_groupdetail].IDKEY = [manually_shiptrack].TxnID
		  WHERE [manually_shiptrack].[PONUMBER] = '".$upo."'";
		echo "<table data-toggle='table' class='table table-bordered display table-striped'>";
		$results = runQuery($conn, $query);
		echo "<tr>";
		foreach ($results[0] as $key => $field)
		{
			echo "<th>".$key."</th>";
		}
		echo "</tr>";
		foreach ($results as $result)
		{
			echo "<tr>";
			foreach ($result as $field)
			{
				echo "<td>".$field."</td>";
			}
			echo "</tr>";
		}
		echo "</table>";
		echo "<br>";
		echo "<br>Groupdetail";
		$query = "SELECT [manually_groupdetail].*
		  FROM [dbo].[manually_groupdetail]
		  LEFT JOIN [manually_shiptrack] ON [manually_groupdetail].IDKEY = [manually_shiptrack].TxnID
		  WHERE [manually_shiptrack].[PONUMBER] = '".$upo."'";
		echo "<table data-toggle='table' class='table table-bordered display table-striped'>";
		$results = runQuery($conn, $query);
		echo "<tr>";
		foreach ($results[0] as $key => $field)
		{
			echo "<th>".$key."</th>";
		}
		echo "</tr>";
		foreach ($results as $result)
		{
			echo "<tr>";
			foreach ($result as $field)
			{
				echo "<td>".$field."</td>";
			}
			echo "</tr>";
		}
		echo "</table>";
		echo "<br>";
		echo "<br>Shiptrack";
		$query = "SELECT [manually_shiptrack].*
		  FROM [dbo].[manually_shiptrack]
		  WHERE [manually_shiptrack].[PONUMBER] = '".$upo."'";
		echo "<table data-toggle='table' class='table table-bordered display table-striped'>";
		$results = runQuery($conn, $query);
		echo "<tr>";
		foreach ($results[0] as $key => $field)
		{
			echo "<th>".$key."</th>";
		}
		echo "</tr>";
		foreach ($results as $result)
		{
			echo "<tr>";
			foreach ($result as $field)
			{
				echo "<td>".$field."</td>";
			}
			echo "</tr>";
		}
		echo "</table>";
		echo "<br>";
	}
}

function display_new_orders2($conn, $upos)
{
	$resultString = '<br>List of new PO numbers:<br>';
	foreach ($upos as $upo)
	{
		$resultString.= '<br><a target="_blank" href="/shipping_module/order_status_history.php?po='.$upo.'">'.$upo.'</a><br>';
	}
	echo $resultString;
}
?>