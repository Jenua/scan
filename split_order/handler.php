<?php
//print_r($_POST);
// This is form handler to create new orders in shipping module system.
require_once('functions.php');
$path = $_SERVER['DOCUMENT_ROOT'];
require_once($path.'/db_connect/connect.php');

if (!isset($_GET['po']) || empty($_GET['po']))
{
	die('Error. Can not get PO.');
} else {
	if (!isset($_POST['order']) || empty($_POST['order']))
	{
		die('Error. Can not get form data.');
	} else {
		if (!isset($_POST['UserName']) || empty($_POST['UserName']))
		{
			die('Error. Can not get user name.');
		} else {
			$user = $_POST['UserName'];
			$po = $_GET['po'];
			$data = $_POST['order'];
			$data = array_values($data);
			$sum_quantity = 0;
			foreach ($data as $rows)
			{
				foreach ($rows as $row)
				{
					$sum_quantity+= $row['quantity'];
				}
			}
			if ($sum_quantity == 0) die('Error. At least one product quantity should be more than 0.'); else
			{
				$data = remove_zero_rows($data);
			}
		}
	}
}

$conn = Database::getInstance()->dbc;

$shiptrack = get_shiptrack($conn, $po);
$linedetail = get_linedetail($conn, $po);
$groupdetail = get_groupdetail($conn, $po);

$all_queries = array();

$shiptrack_queries = generate_shiptrack_queries($conn, $data, $shiptrack[0], $user);
$queries = $shiptrack_queries['queries'];
$all_queries = array_merge($all_queries, $queries);
$TxnIDs = $shiptrack_queries['TxnIDs'];
$upos = $shiptrack_queries['upos'];
//$urns = $shiptrack_queries['urns'];
$log_queries = $shiptrack_queries['log_queries'];

$groupdetail_queries = generate_groupdetail_queries($data, $groupdetail, $TxnIDs);
$queries = $groupdetail_queries['queries'];
$all_queries = array_merge($all_queries, $queries);
$TxnLineIDs = $groupdetail_queries['TxnLineIDs'];
$TxnLineIDs_old_and_new = $groupdetail_queries['TxnLineIDs_old_and_new'];

$linedetail_queries = generate_linedetail_queries($data, $linedetail, $groupdetail, $TxnIDs, $TxnLineIDs, $TxnLineIDs_old_and_new);
$all_queries = array_merge($all_queries, $linedetail_queries);

$all_queries = array_merge($all_queries, $log_queries);
/*
print_r("<pre>");
print_r($all_queries);
print_r("</pre>");
die();
*/
$result = beginTransaction($conn);
if ($result)
{
	foreach ($all_queries as $query)
	{
		//print_r($query);
		$result = runQuery_empty_result($conn, $query);
		if (!$result)
		{
			rollbackTransaction($conn);
			die('Error. Can not run query: '.$query.'');
		}
	}
	$result = commitTransaction($conn);
	if (!$result)
	{
		rollbackTransaction($conn);
		die('Error. Can not commit transaction.');
	} else 
	{
		echo 'New orders was successfully saved.';
		if (isset($upos) && !empty($upos))
		{
			display_new_orders2($conn, $upos);
			//echo "<a href='/shipsingle/shipsingle.php'><button>Rate new orders</button></a>";
		} else die("Can not get new PO numbers to display results.");
	}
} else die('Error. Can not begin transaction.');
$conn = null;
?>