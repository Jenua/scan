<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Split Order Results</title>
  <link rel="stylesheet" href="Include/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="Include/bootstrap/css/bootstrap-theme.min.css">
  <script src="Include/jQuery/jquery-1.11.3.min.js"></script>
  <script src="Include/bootstrap/js/bootstrap.min.js"></script>
</head>
<body>
<?php
require_once('functions.php');
$conn = getConnection();
echo '<br>New orders was successfully saved.<br>';
echo '<br>New orders will appear in Shipping Module later.<br>';
$upos = $_GET;
if (isset($upos) && !empty($upos))
{
	display_new_orders2($conn, $upos);
} else die("<br>Can not get new PO numbers to display results.<br>");
?>
</body>
</html>