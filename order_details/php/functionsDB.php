<?php
$path = $_SERVER['DOCUMENT_ROOT'];
require_once($path.'/db_connect/connect.php');
//Connection to DB
function con()
{
	$conn = Database::getInstance()->dbc;
	return $conn;
}

//Execute input query
function execQuery($connect, $query)
{
	try{
		$result = $connect->prepare($query);
		$result->execute();
		//print_r($result);
	}
	catch(PDOException $e){
		die('<br>Can not execute query! '.$e->getMessage().'<br>');
	}
	return $result;
}

//Get all data from DB
function getData($conn, $query)
{
		$result = $conn->prepare($query);
		$result->execute();
		$result = $result->fetchAll(PDO::FETCH_ASSOC);
		if (!isset($result) || empty($result)) die('There is no data for this request in DB.');
		return $result;
}

//Get all queries from DB
function getQueries($conn)
{
		$query = "SELECT 
		[id_odq]
		,[query_name]
		,[query_description]
		,[query_text]
			FROM [dbo].[order_details_queries]";
		$result = $conn->prepare($query);
		$result->execute();
		$result = $result->fetchAll(PDO::FETCH_ASSOC);
		if (!isset($result) || empty($result)) die('Can not get queries from DB.');
		return $result;
}

//Get query by ID from DB
function getQuery($conn, $id)
{
		$query = "SELECT 
		[id_odq]
		,[query_name]
		,[query_description]
		,[query_text]
			FROM [dbo].[order_details_queries]
			WHERE [id_odq] = ".$id;
		$result = $conn->prepare($query);
		$result->execute();
		$result = $result->fetch(PDO::FETCH_ASSOC);
		if (!isset($result) || empty($result)) die('Can not get queries from DB.');
		return $result;
}

//update query
function saveQuery($conn, $id_odq, $query_name, $query_description, $query_text)
{
	$query_text = str_replace("'", "''", $query_text);
	try{
		$query = "UPDATE [dbo].[order_details_queries] SET [query_name] = '".$query_name."', [query_description] = '".$query_description."', [query_text] = '".$query_text."' WHERE [id_odq] = ".$id_odq;
		execQuery($conn, $query);
		return 'Query saved';
	}catch (Exception $e) {
		die("Can not save query to database!");
	}
}

//delete query
function removeQuery($conn, $id_odq)
{
	try{
		$query = "DELETE FROM [dbo].[order_details_queries] WHERE [id_odq] = ".$id_odq;
		execQuery($conn, $query);
		return 'Query removed';
	}catch (Exception $e) {
		die("Can not remove query from database!");
	}
}

//insert query
function createQuery($conn, $query_name, $query_description, $query_text)
{
	$query_text = str_replace("'", "''", $query_text);
	try{
		$query = "INSERT INTO [dbo].[order_details_queries]
           ([query_name]
           ,[query_description]
           ,[query_text])
     VALUES ('".$query_name."', '".$query_description."', '".$query_text."')";
		execQuery($conn, $query);
		return 'Query created';
	}catch (Exception $e) {
		die("Can not create query in database!");
	}
}
?>