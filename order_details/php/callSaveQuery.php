<?php
require_once('functionsDB.php');
$path = $_SERVER['DOCUMENT_ROOT'];
require_once($path.'/db_connect/connect.php');

if (isset($_REQUEST['id_odq']) && isset($_REQUEST['query_name']) && isset($_REQUEST['query_description']) && isset($_REQUEST['query_text']))
{
	$id_odq = $_REQUEST['id_odq'];
	$query_name = json_decode($_REQUEST['query_name']);
	$query_description = json_decode($_REQUEST['query_description']);
	$query_text = json_decode($_REQUEST['query_text']);
	if (!empty($query_name) && !empty($query_description) && !empty($query_text))
	{
		$conn = Database::getInstance()->dbc;
		$result = saveQuery($conn, $id_odq, $query_name, $query_description, $query_text);
		echo $result;
	} else die('No data to save query');
} else die('No data to save query');
?>