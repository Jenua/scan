<?php
require_once('functionsDB.php');
$path = $_SERVER['DOCUMENT_ROOT'];
require_once($path.'/db_connect/connect.php');

if (isset($_REQUEST['id']) && !empty($_REQUEST['id']))
{
	$conn = Database::getInstance()->dbc;
	$query = getQuery($conn, $_REQUEST['id']);
	if(isset($query) && !empty($query))
	{
		echo json_encode($query);
	} else die('Can not get query!');
} else die('No data to get query!');
?>