<?php
require_once('functionsDB.php');
$path = $_SERVER['DOCUMENT_ROOT'];
require_once($path.'/db_connect/connect.php');

if (isset($_REQUEST['id']) && !empty($_REQUEST['id']))
{
	$conn = Database::getInstance()->dbc;
	$result = removeQuery($conn, $_REQUEST['id']);
	echo $result;
} else die('No data to remove query');
?>