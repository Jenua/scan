<?php
require_once('functionsDB.php');
$path = $_SERVER['DOCUMENT_ROOT'];
require_once($path.'/db_connect/connect.php');

if (isset($_REQUEST['query_name']) && isset($_REQUEST['query_description']) && isset($_REQUEST['query_text']))
{
	$query_name = json_decode($_REQUEST['query_name']);
	$query_description = json_decode($_REQUEST['query_description']);
	$query_text = json_decode($_REQUEST['query_text']);
	if (!empty($query_name) && !empty($query_description) && !empty($query_text))
	{
		$conn = Database::getInstance()->dbc;
		$result = createQuery($conn, $query_name, $query_description, $query_text);
		echo $result;
	} else die('No data to create query');
} else die('No data to create query');
?>