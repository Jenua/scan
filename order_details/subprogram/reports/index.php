<?php
session_start();
$path = $_SERVER['DOCUMENT_ROOT'];
require_once($path.'/db_connect/connect.php');
require_once($path.'/auth/auth.php');

$auth = new AuthClass();
 
if (isset($_POST["login"]) && isset($_POST["password"])) {
    if (!$auth->auth($_POST["login"], $_POST["password"])) {
        echo "<script>alert('Wrong login or password!')</script>";
    }
}
 
if (isset($_GET["is_exit"])) {
    if ($_GET["is_exit"] == 1) {
        $auth->out();
        header("Location: ?is_exit=0");
    }
}

$helloString = '';
if ($auth->isAuth()) { 
			$helloString =  "Hello, " . $auth->getLogin() .". <a href='?is_exit=1'>Exit</a>";
	
	require_once('functionsDB.php');
	
	$conn = Database::getInstance()->dbc;
	$results = getQueries($conn);
	$conn = null;
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Shipping Module Reports</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<script type="text/javascript" charset="utf8" src="../../Include/jQuery/jquery-1.11.3.min.js"></script>
		<link rel="stylesheet" type="text/css" href="../../Include/codemirror/codemirror-5.3/lib/main.css">
		<script type="text/javascript" src="js/script.js"></script>
	</head>

	<body>
		<div id="headerContainer">
			<div class="logo"><img src="../../../Include/img/logo.png"></div>
			<?php echo $helloString; ?>
			<div class="product">
				<i>Shipping Module Reports</i>
				<strong style="color: black;"></strong>
			</div>
		</div>
		<div id="queryContainer">
			<div class="grid">
				<?php
				foreach ($results as $result)
				{
					echo '
					<div class="container">
						<a href="display_data.php?id='.$result['id_odq'].'">
							<div class="description">
								<p>'.$result['query_description'].'</p>
							</div>
							<div class="outer">
								<div class="middle">
									<div class="inner">
										<p>'.$result['query_name'].'</p>
									</div>
								</div>
							</div>
						</a>
					</div>';
				}
				?>
				</div>
				<br><br>
				<div class="grid">
					<div class="container program" style="cursor: pointer;" onclick="var win = window.open('/shipping_module/duplicate_status.php', '_blank'); win.focus();">
							<div class="description">
								<p>A list of orders that have duplicated status</p>
							</div>
							<div class="outer">
								<div class="middle">
									<div class="inner">
										<p>Duplicated Status</p>
									</div>
								</div>
							</div>
					</div>
					<div class="container program" style="cursor: pointer;" onclick="var win = window.open('../wave_picking_list/index.php', '_blank'); win.focus();">
						<div class="description">
							<p>Total quantity of items in groups for multiple orders</p>
						</div>
						<div class="outer">
							<div class="middle">
								<div class="inner">
									<p>Wave Picking List</p>
								</div>
							</div>
						</div>
					</div>
					<div class="container program" style="cursor: pointer;" onclick="var win = window.open('../sla2/index.php', '_blank'); win.focus();">
						<div class="description">
							<p> </p>
						</div>
						<div class="outer">
							<div class="middle">
								<div class="inner">
									<p>SLA Report for Active orders</p>
								</div>
							</div>
						</div>
					</div>
					<div class="container program" style="cursor: pointer;" onclick="var win = window.open('../dcReport/index.php', '_blank'); win.focus();">
						<div class="description">
							<p> </p>
						</div>
						<div class="outer">
							<div class="middle">
								<div class="inner">
									<p>DC Reports</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			<br><br>
		</div>
	</body>
</html>
<?php
} else {
?>
<div style="position: relative; top: 110px; width: 100%;">
<div style="
position: relative;
width: 330px;
height: 160px;
left: 50%;
margin-left: -160px;
padding: 10px;
background: #367FBB;
">
<form method="post" action="">
<fieldset>
    <p><label class="field" for="login">Login:</label><input class="logininput" type="text" name="login"
    value="<?php echo (isset($_POST["login"])) ? $_POST["login"] : null; ?>" /></p>
    <p><label class="field" for="password">Password:</label><input class="logininput" type="password" name="password" value="" /></p>
	<p><label class="field" for="password"> </label><input class="loginsubmit" type="submit" value="login" /></p>
</fieldset>
</form>
</div>
</div>
<?php 
}