<?php
$path = $_SERVER['DOCUMENT_ROOT'];
require_once($path.'/db_connect/connect.php');
//Connection to DB
function con()
{
	$conn = Database::getInstance()->dbc;
	return $conn;
}

//Execute input query
function execQuery($connect, $query)
{
	try{
		$result = $connect->prepare($query);
		$result->execute();
		//print_r($result);
	}
	catch(PDOException $e){
		die('<br>Can not execute query! '.$e->getMessage().'<br>');
	}
	return $result;
}

//Get all data from DB
function getData($conn, $query)
{
		$result = $conn->prepare($query);
		$result->execute();
		$result = $result->fetchAll(PDO::FETCH_ASSOC);
		if (!isset($result) || empty($result)) die('There is no data for this request in DB.');
		return $result;
}

//Get all queries from DB
function getQueries($conn)
{
		$query = "EXEC [shipping_module_get_reports]";
		$result = $conn->prepare($query);
		$result->execute();
		$result = $result->fetchAll(PDO::FETCH_ASSOC);
		if (!isset($result) || empty($result)) die('Can not get queries from DB.');
		return $result;
}

//Get query by ID from DB
function getQuery($conn, $id)
{
		$query = "SELECT 
		[id_odq]
		,[query_name]
		,[query_description]
		,[query_text]
			FROM [dbo].[order_details_queries]
			WHERE [id_odq] = ".$id;
		$result = $conn->prepare($query);
		$result->execute();
		$result = $result->fetch(PDO::FETCH_ASSOC);
		if (!isset($result) || empty($result)) die('Can not get queries from DB.');
		return $result;
}
?>