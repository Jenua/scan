var mime = 'text/x-sql';
var editor;
var id;

function getQuery(id_odq)
{
	$.ajax({
		url: 'php/callGetQuery.php',
		type: 'POST',
		data: {
			id: id_odq
		},
		complete: function(data){
			//console.log(data.responseText);
			
			try
			{
				var query = JSON.parse(data.responseText);
				$('#editPopup').fadeIn(0);
				//var textArea = document.getElementById('editTextArea');
				//textArea.value = query['query_text'];
				var editTextAreaName = document.getElementById('editTextAreaName');
				var editTextAreaDescription = document.getElementById('editTextAreaDescription');
				
				editTextAreaName.value = query['query_name'];
				editTextAreaDescription.value = query['query_description'];
				
				editor.setValue("");
				editor.clearHistory();
				editor.setValue(query['query_text']);
			}
			catch(e)
			{
				console.log('Error: '+e)
			}
		},
		error: function (request, status, error) {
			console.log("Ajax request error: "+request.responseText);
		}
	});
}