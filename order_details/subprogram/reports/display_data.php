<?php

$path = $_SERVER['DOCUMENT_ROOT'];
require_once('functionsDB.php');
require_once($path.'/db_connect/connect.php');
$conn = Database::getInstance()->dbc;
$backFlag = false;

if (isset($_GET["query_text"]) && !empty($_GET["query_text"])
	&& isset($_GET["query_name"]) && !empty($_GET["query_name"]))
{
	$queryText = $_GET["query_text"];
	$queryName = $_GET["query_name"];
} else if (isset($_GET["id"]) && !empty($_GET["id"]))
{
	$backFlag = true;
	$query = getQuery($conn, $_GET["id"]);
	if($query) 
	{
		$queryText = $query['query_text'];
		$queryName = $query['query_name'];
	}
} else die('<br>Query ID is empty!<br>');

	if(isset($queryText) && !empty($queryText))
	{
		$results = getData($conn, $queryText);
		$conn = null;
	} else die('<br>Can not get query from DB!<br>');
?>
<!DOCTYPE html>
<html>
	<head>
	<meta charset="UTF-8">
	<title>Order Details</title>
	<!-- DataTables CSS -->
	<link rel="stylesheet" type="text/css" href="../../Include/DataTables/media/css/jquery.dataTables.css">
	<link rel="stylesheet" type="text/css" href="../../Include/codemirror/codemirror-5.3/lib/main.css">
	<!-- jQuery -->
	<script type="text/javascript" charset="utf8" src="../../Include/jQuery/jquery-1.11.3.min.js"></script>
	<!-- DataTables -->
	<script type="text/javascript" charset="utf8" src="../../Include/DataTables/media/js/jquery.dataTables.js"></script>
	
	<script>
	function getParameterByName(name) {
		name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
		var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
			results = regex.exec(location.search);
		return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
	}
	
	// Update a key-value pair in the URL query parameters
	function updateUrlParameter(uri, key, value) {
		// remove the hash part before operating on the uri
		var i = uri.indexOf('#');
		var hash = i === -1 ? ''  : uri.substr(i);
			 uri = i === -1 ? uri : uri.substr(0, i);

		var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
		var separator = uri.indexOf('?') !== -1 ? "&" : "?";
		if (uri.match(re)) {
			uri = uri.replace(re, '$1' + key + "=" + value + '$2');
		} else {
			uri = uri + separator + key + "=" + value;
		}
		return uri + hash;  // finally append the hash as well
	}
	
		$(document).ready(function() {
			var table = $('#example').DataTable({
				aLengthMenu: [
					[25, 50, 100, 200, -1],
					[25, 50, 100, 200, "All"]
				],
				"order": [[ 1, "desc" ]],
				iDisplayLength: -1
			} );
			var search = getParameterByName('search');
			if (search)
			{
				table.search( search ).draw();
			}
			
			$('input[type=search]').on('keypress input propertychange', function() {
				uri = location.search;
				key = 'search';
				value = $(this).val();
				new_url = updateUrlParameter(uri, key, value);
				window.history.pushState("object or string", "Title", new_url);
			})
		} );
	</script>

	<!--<script>
		$(document).ready(function() {
			$('#example').fixedHeaderTable();
		} );
	</script>-->
	<style>
	/*.fixed{
		top:0;
		position:fixed;
		width:auto;
		display:none;
		border:none;
	}*/
	.wrap {
		padding-bottom: 30px;
		padding-top: 70px;
	}
	#headerContainer {
		top: 0;
		height: 60px;
		position: fixed;
		width: 100%;
		z-index: 100;
		box-sizing: border-box;
		background: #eaf1f7;
		opacity: 1;
	}
	.table-holder {
		height: 60px;
		margin-bottom: 30px;
	}
		#backHref {
			position: absolute;
			right: 1%;
			transition: 0.3s;
			top: 10%;
		}
		#backHref:hover {
			transform: scale(1.1);
		}
	</style>
	</head>

	<body>
		<div id="headerContainer">
			<div class="logo"><img src="../../../Include/img/logo.png" style="margin-left: 10px;"></div>
			<div class="product">
				<i>Shipping Module Reports</i>
				<?php
				if ($backFlag) echo '<a id="backHref" href="index.php">
					<img width="25px" height="25px" src="../../Images/back.png" alt="Back to query selection" title="Back to query selection">
				</a>';
				?>
			</div>
			<center><div style="font-size: 18px; font-weight: bold; color: black; margin: 10px;"><?php echo $queryName; ?></div></center>
		</div>
		<div class="table-holder" ></div>

		<table id="example" class="display" cellspacing="0" width="100%">
			<thead>
				<tr>
					<?php
					foreach ($results[0] as $key => $value)
					{
						echo '<th>'.$key.'</th>';
					}
					?>
				</tr>
<!-- 				<tfoot>
					<?php
					foreach ($results[0] as $key => $value)
					{
						echo '<th>'.$key.'</th>';
					}
					?>
				</tfoot> -->
			</thead>
			<tbody>
				<?php
				foreach ($results as $result)
				{
					echo '<tr>';
					foreach ($result as $key => $value)
					{
						if (empty($value) && $value != '0') echo '<td><b style="color: red;">Missing</b></td>'; else echo '<td>'.$value.'</td>';
					}
					echo '</tr>';
				}
				?>
			</tbody>
		</table>
	</body>

</html>