<?php

$path = $_SERVER['DOCUMENT_ROOT'];
require_once('functionsDB.php');
require_once($path.'/db_connect/connect.php');
$conn = Database::getInstance()->dbc;

$queryName = "SLA Report for Active orders";
$query = "      SELECT ISNULL(data.[Dealer], 'Others') as Dealers
                        , sum(case when [GTS] <= ISNULL(data.[plh], dflt.[Processing_low_hours]) then 1 else 0 end)
                        + sum(case when [GTS] > ISNULL(data.[plh], dflt.[Processing_low_hours]) and [GTS] <= ISNULL( data.[pmh], dflt.[Processing_med_hours] ) then 1 else 0 end)
                        + sum(case when [GTS] > ISNULL(data.[pmh], dflt.[Processing_med_hours]) and [GTS] <= ISNULL( data.[phh], dflt.[Processing_high_hours] ) then 1 else 0 end)
                        + sum(case when [GTS] > ISNULL(data.[phh], dflt.[Processing_high_hours]) then 1 else 0 end) as [Processing delay: Total]

                        --, sum(case when [GTS] <= ISNULL(data.[plh], dflt.[Processing_low_hours]) then 1 else 0 end) as [Processing delay: Low]
                        , sum(case when [GTS] > ISNULL(data.[plh], dflt.[Processing_low_hours]) and [GTS] <= ISNULL( data.[pmh], dflt.[Processing_med_hours] ) then 1 else 0 end) as [Processing delay: Medium]
                        , sum(case when [GTS] > ISNULL(data.[pmh], dflt.[Processing_med_hours]) and [GTS] <= ISNULL( data.[phh], dflt.[Processing_high_hours] ) then 1 else 0 end) as [Processing delay: High]
                        , sum(case when [GTS] > ISNULL(data.[phh], dflt.[Processing_high_hours]) then 1 else 0 end) as [Processing delay: Expired]
                FROM (
                        SELECT a.[PONumber]
                                 , c.[Dealer]
                                 , a.[CARRIERNAME]
                                 , case
                                         when [GenerateTimeStamp] is not null then (DateDiff(s, [TimeCreated], [GenerateTimeStamp])/3600)
                                         else (DateDiff(s, [TimeCreated], SYSDATETIME())/3600)
                                   end as GTS
                                 , case
                                         when [ShipDate] is not null then (DateDiff(s, [TimeCreated], [ShipDate])/3600)
                                         else (DateDiff(s, [TimeCreated], SYSDATETIME())/3600)
                                   end as SD
                                 , c.[Processing_low_hours] as [plh]
                                 , c.[Processing_med_hours] as [pmh]
                                 , c.[Processing_high_hours] as [phh]
                                 , c.[GR_low_hours] as [glh]
                                 , c.[GR_med_hours] as [gmh]
                                 , c.[GR_high_hours] as [ghh]
                                 , c.[LTL_low_hours] as [llh]
                                 , c.[LTL_med_hours] as [lmh]
                                 , c.[LTL_high_hours] as [lhh]	 
                         FROM (
                                 SELECT [shiptrack].[RefNumber]
                                          , [shiptrack].[TimeCreated]
                                          , [ShipTrackWithLabel].[DEALER]
                                          , [ShipTrackWithLabel].[PONUMBER]
                                          , [ShipTrackWithLabel].[CARRIERNAME]
                                          , cast(ShipTrackWithLabel.GenerateTimeStamp as smalldatetime) as GenerateTimeStamp
                                   FROM [orders].[dbo].[shiptrack]
                         INNER JOIN [orders].[dbo].[ShipTrackWithLabel] ON [shiptrack].RefNumber=[ShipTrackWithLabel].ID
                          LEFT JOIN ( SELECT [Orders_combined].[RefNumber]
                                                           , [Combined_into_RefNumber]
                                                        FROM [orders].[dbo].[ShipTrackWithLabel]
                                          INNER JOIN [orders].[dbo].[Orders_combined] ON [Orders_combined].[RefNumber] = [ShipTrackWithLabel].[ID]
                                                   WHERE EXISTS (SELECT [ID]
                                                                                   FROM [orders].[dbo].[ShipTrackWithLabel]
                                                                                  WHERE [ID] = [Orders_combined].[Combined_into_RefNumber])
                                                ) as comb ON ShipTrackWithLabel.[ID] = comb.RefNumber
                        UNION
                                 SELECT [manually_shiptrack].[RefNumber]
                                          , [manually_shiptrack].[TimeCreated]
                                          , [ShipTrackWithLabel].[DEALER]
                                          , [ShipTrackWithLabel].[PONUMBER]
                                          , [ShipTrackWithLabel].[CARRIERNAME]
                                          , cast(ShipTrackWithLabel.GenerateTimeStamp as smalldatetime) as GenerateTimeStamp
                                   FROM [orders].[dbo].[manually_shiptrack]
                         INNER JOIN [orders].[dbo].[ShipTrackWithLabel] ON [manually_shiptrack].[RefNumber]=[ShipTrackWithLabel].[ID]
                                  WHERE [ShipTrackWithLabel].[PONUMBER] IN (SELECT [orders].[dbo].[Orders_divided].[Divided_into_POnumber]
                                                                                                                          FROM [orders].[dbo].[shiptrack]
                                                                                                                INNER JOIN [orders].[dbo].[Orders_divided] ON [Orders_divided].[POnumber] = [shiptrack].[PONumber])
                                         OR [ShipTrackWithLabel].[PONUMBER] IN (SELECT [orders].[dbo].[Orders_combined].[Combined_into_POnumber]
                                                                                                                          FROM [orders].[dbo].[shiptrack]
                                                                                                                INNER JOIN [orders].[dbo].[Orders_combined] ON [Orders_combined].[RefNumber] = [shiptrack].[RefNumber])
                                         OR ([ShipTrackWithLabel].[ID] LIKE 'manually_%' AND ShipDate IS NULL OR CONVERT(DATE, ShipDate, 110) > GETDATE() - 5)
                                 ) a
           LEFT JOIN (SELECT [salesorder].[PONumber]
                                           , max([Orders_shipped].ShipDate) as ShipDate
                                           , max([Orders_picked].[DateCreated]) as PickDate
                                           , max([Orders_printed].[DateCreated]) as ScannedDate
                                        FROM [orders31].[dbo].[salesorder]
                           LEFT JOIN [orders31].[dbo].[Orders_picked] ON [salesorder].[PONumber] = [Orders_picked].[PONumber]
                           LEFT JOIN [orders31].[dbo].[Orders_printed] ON [salesorder].[PONumber] = [Orders_printed].[PONumber]
                           LEFT JOIN [orders31].[dbo].[Orders_shipped] ON [salesorder].[PONumber] = [Orders_shipped].[PONumber]
                                GROUP BY [salesorder].[PONumber]
                                 ) b ON a.[PONUMBER] = b.[PONumber]
           LEFT JOIN (SELECT * 
                                        FROM [orders].[dbo].[SLA]
                                 ) c ON a.[DEALER] like c.[Dealer_cut]
                         ) data
   LEFT JOIN [orders].[dbo].[SLA] as dflt ON [Dealer_cut] = 'o65GVdUNBf'
        GROUP BY data.Dealer
        ORDER BY data.Dealer";

$results = getData($query);
if(!empty($results))
{
?>
<html>
	<head>
	<meta charset="UTF-8">
	<title>Order Details</title>
	<!-- DataTables CSS -->
	<link rel="stylesheet" type="text/css" href="../../Include/DataTables/media/css/jquery.dataTables.css">
	<link rel="stylesheet" type="text/css" href="../../Include/codemirror/codemirror-5.3/lib/main.css">
	<!-- jQuery -->
	<script type="text/javascript" charset="utf8" src="../../Include/jQuery/jquery-1.11.3.min.js"></script>
	<!-- DataTables -->
	<script type="text/javascript" charset="utf8" src="../../Include/DataTables/media/js/jquery.dataTables.js"></script>
	
	<script>
	function getParameterByName(name) {
		name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
		var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
			results = regex.exec(location.search);
		return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
	}
	
	// Update a key-value pair in the URL query parameters
	function updateUrlParameter(uri, key, value) {
		// remove the hash part before operating on the uri
		var i = uri.indexOf('#');
		var hash = i === -1 ? ''  : uri.substr(i);
			 uri = i === -1 ? uri : uri.substr(0, i);

		var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
		var separator = uri.indexOf('?') !== -1 ? "&" : "?";
		if (uri.match(re)) {
			uri = uri.replace(re, '$1' + key + "=" + value + '$2');
		} else {
			uri = uri + separator + key + "=" + value;
		}
		return uri + hash;  // finally append the hash as well
	}
	
		$(document).ready(function() {
			var table = $('#example').DataTable({
				aLengthMenu: [
					[25, 50, 100, 200, -1],
					[25, 50, 100, 200, "All"]
				],
				"order": [[ 1, "desc" ]],
				iDisplayLength: -1
			} );
			var search = getParameterByName('search');
			if (search)
			{
				table.search( search ).draw();
			}
			
			$('input[type=search]').on('keypress input propertychange', function() {
				uri = location.search;
				key = 'search';
				value = $(this).val();
				new_url = updateUrlParameter(uri, key, value);
				window.history.pushState("object or string", "Title", new_url);
			})
		} );
	</script>

	<!--<script>
		$(document).ready(function() {
			$('#example').fixedHeaderTable();
		} );
	</script>-->
	<style>
	/*.fixed{
		top:0;
		position:fixed;
		width:auto;
		display:none;
		border:none;
	}*/
	.wrap {
		padding-bottom: 30px;
		padding-top: 70px;
	}
	#headerContainer {
		top: 0;
		height: 60px;
		position: fixed;
		width: 100%;
		z-index: 100;
		box-sizing: border-box;
		background: #eaf1f7;
		opacity: 1;
	}
	.table-holder {
		height: 60px;
		margin-bottom: 30px;
	}
        #backHref {
                position: absolute;
                right: 1%;
                transition: 0.3s;
                top: 10%;
        }
        #backHref:hover {
                transform: scale(1.1);
        }
        table.dataTable.display tbody tr.odd td.prM {
            background-color: #FFB;
        }    
        table.dataTable.display tbody tr.odd td.prH {
            background-color: #FBB;
        }
        table.dataTable.display tbody tr.odd td.prE {
            background-color: #AAA;
            font-weight: bold;
        }
        table.dataTable.display tbody tr.even td.prM {
            background-color: #FFD;
        }    
        table.dataTable.display tbody tr.even td.prH {
            background-color: #FDD;
        }
        table.dataTable.display tbody tr.even td.prE {
            background-color: #CCC;
            font-weight: bold;
        }
        
        table.dataTable.display tbody tr.odd td.prM.sorting_1,
        table.dataTable.display tbody tr.odd td.prM.sorting_2,
        table.dataTable.display tbody tr.odd td.prM.sorting_3 {
            background-color: #FFA;
        }    
        table.dataTable.display tbody tr.odd td.prH.sorting_1,
        table.dataTable.display tbody tr.odd td.prH.sorting_2,
        table.dataTable.display tbody tr.odd td.prH.sorting_3 {
            background-color: #FAA;
        }
        table.dataTable.display tbody tr.odd td.prE.sorting_1,
        table.dataTable.display tbody tr.odd td.prE.sorting_2,
        table.dataTable.display tbody tr.odd td.prE.sorting_3 {
            background-color: #999;
            font-weight: bold;
        }
        table.dataTable.display tbody tr.even td.prM.sorting_1,
        table.dataTable.display tbody tr.even td.prM.sorting_2,
        table.dataTable.display tbody tr.even td.prM.sorting_3 {
            background-color: #FFC;
        }    
        table.dataTable.display tbody tr.even td.prH.sorting_1,
        table.dataTable.display tbody tr.even td.prH.sorting_2,
        table.dataTable.display tbody tr.even td.prH.sorting_3 {
            background-color: #FCC;
        }
        table.dataTable.display tbody tr.even td.prE.sorting_1,
        table.dataTable.display tbody tr.even td.prE.sorting_2,
        table.dataTable.display tbody tr.even td.prE.sorting_3 {
            background-color: #BBB;
            font-weight: bold;
        }

        table.dataTable.display tbody tr:hover td.prM.sorting_1,
        table.dataTable.display tbody tr:hover td.prM.sorting_2,
        table.dataTable.display tbody tr:hover td.prM.sorting_3,
        table.dataTable.display tbody tr:hover td.prM {
            background-color: #FFFFA0;
        }
        table.dataTable.display tbody tr:hover td.prH.sorting_1,
        table.dataTable.display tbody tr:hover td.prH.sorting_2,
        table.dataTable.display tbody tr:hover td.prH.sorting_3,        
        table.dataTable.display tbody tr:hover td.prH {
            background-color: #FFA0A0;
        }
        table.dataTable.display tbody tr:hover td.prE.sorting_1,
        table.dataTable.display tbody tr:hover td.prE.sorting_2,
        table.dataTable.display tbody tr:hover td.prE.sorting_3,        
        table.dataTable.display tbody tr:hover td.prE {
            background-color: #909090;
            font-weight: bold;
        }
                
	</style>
	</head>

	<body>
		<div id="headerContainer">
			<div class="logo"><img src="../../../Include/img/logo.png" style="margin-left: 10px;"></div>
			<div class="product">
				<i>Custom </i><strong style="color: black; font-size: 24px;">Report</strong><i> Builder</i>
				<?php
				if ( isset($backFlag) && $backFlag ) echo '<a id="backHref" href="/order_details/">
					<img width="25px" height="25px" src="/order_details/Images/back.png" alt="Back to query selection" title="Back to query selection">
				</a>';
				?>
			</div>
			<center><div style="font-size: 18px; font-weight: bold; color: black; margin: 10px;"><?php echo $queryName; ?></div></center>
		</div>
		<div class="table-holder" ></div>

		<table id="example" class="display" cellspacing="0" width="100%">
			<thead>
				<tr>
					<?php
					foreach ($results[0] as $key => $value)
					{
                                                echo '<th>'.$key.'</th>';
					}
					?>
				</tr>
<!-- 				<tfoot>
					<?php
					foreach ($results[0] as $key => $value)
					{
						echo '<th>'.$key.'</th>';
					}
					?>
				</tfoot> -->
			</thead>
			<tbody>
				<?php
				foreach ($results as $result)
				{
					echo '<tr>';
					foreach ($result as $key => $value)
					{
                                            $color_class = "";
                                            switch( $key ) {
                                               case "Processing delay: Medium": $color_class = "prM"; break;
                                               case "Processing delay: High": $color_class = "prH"; break;
                                               case "Processing delay: Expired": $color_class = "prE"; break;
                                            }                                            
                                            if (empty($value) && $value != '0') echo '<td class="'.$color_class.'"><b style="color: red;">Missing</b></td>'; else echo '<td class="'.$color_class.'">'.$value.'</td>';
					}
					echo '</tr>';
				}
				?>
			</tbody>
		</table>
	</body>

</html>    
    
<?php
}
