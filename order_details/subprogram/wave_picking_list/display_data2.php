<?php

$path = $_SERVER['DOCUMENT_ROOT'];
require_once('functionsDB.php');
require_once($path.'/db_connect/connect.php');
$conn = Database::getInstance()->dbc;

		$query_main = "SELECT [itemgroup].[Name] as [group_sku]
	   ,case
			when [itemgrouplinedetail].[ItemRef_FullName] like '%:%' then RIGHT([itemgrouplinedetail].[ItemRef_FullName], (CHARINDEX(':',REVERSE([itemgrouplinedetail].[ItemRef_FullName]),0))-1)
			else [itemgrouplinedetail].[ItemRef_FullName]
		end as [item_sku]
      ,[itemgrouplinedetail].[Quantity] as [qty]
  FROM [".DATABASE31."].[dbo].[itemgroup]
  LEFT JOIN [".DATABASE31."].[dbo].[itemgrouplinedetail] ON [itemgroup].[ListID] = [itemgrouplinedetail].[IDKEY]
  WHERE
		[itemgrouplinedetail].ItemRef_FullName not like '%Price-Adjustment%' 
		and [itemgrouplinedetail].ItemRef_FullName not like '%Freight%'
		and [itemgrouplinedetail].ItemRef_FullName not like '%warranty%' 
		and [itemgrouplinedetail].ItemRef_FullName is not NULL
		and [itemgrouplinedetail].ItemRef_FullName not like '%Subtotal%'
		and [itemgrouplinedetail].ItemRef_FullName not like '%IDSC-10%'
		and [itemgrouplinedetail].ItemRef_FullName not like '%DISCOUNT%'
		and [itemgrouplinedetail].ItemRef_FullName not like '%SPECIAL ORDER4%'
		and [itemgrouplinedetail].ItemRef_FullName not like '%Manual%'
  ORDER BY [itemgroup].[Name]";
		
		
		$result_main = getData($query_main);
		if(!empty($result_main))
		{
			$csv = 'download.php?file='.array2csv($result_main, 'items_in_group');
			$table = "<button class='btn btn-success' style='float: left;' onclick='OpenInNewTab(".'"'.$csv.'"'.")'>CSV Report</button><table border='1'>
						<thead>
							<tr>";
			foreach($result_main[0] as $key => $value)
			{
				$table.= "<th>".$key."</th>";
			}
			$table.= "</tr>
					</thead>
					<tbody>";
			$color = true;
			$prev_sku = '';
			foreach($result_main as $row)
			{
				if($row['group_sku'] != $prev_sku && $color)
				{
					$color = false;
				} else if($row['group_sku'] != $prev_sku && !$color)
				{
					$color = true;
				}
				if($color) $style='#f4f4f4'; else $style='#fff';
				$table.= "<tr style='background-color: ".$style." !important;'>";
				foreach($row as $field)
				{
					$table.= "<td>".$field."</td>";
				}
				$table.= "</tr>";
				$prev_sku = $row['group_sku'];
			}
			$table.= "</tbody>
				</table>";
			print_r($table);
		}
		
function array2csv($array, $name)
{
   if (count($array) == 0) {
     return null;
   }
   $file_name = "csv/".$name.".csv";
   $df = fopen($file_name, 'w');
   fputcsv($df, array_keys(reset($array)));
   foreach ($array as $row) {
      fputcsv($df, $row);
   }
   fclose($df);
   $file_name2 = $name.".csv";
   return $file_name2;
}