$(document).ready(function() {
	var dialog = $('#dialog').dialog({
        autoOpen: false,
        height: 'auto',
        width: 'auto',
        resizable: false,
        autoResize: true,
        modal: true,
	title: '',
        overlay: { backgroundColor: "#000", opacity: 0.5 },
    });
    dialog.dialog('close');
	
    //Submit event
    $('#po_form').on('submit', function (e) {
        event.preventDefault();
        var form = $('#po_form');
        var url = $(form).attr("action");
        
        var tovalidate = new Array();
        
        $( 'textarea', this ).each(function(){
            var tagstr = $(this).tagEditor('getTags')[0].tags;
            var tagarr = tagstr.toString().split(',');
			tovalidate = tagarr;
        });
        
        $.ajax({
            url: url,
            type: 'POST',
            data: {po_numbers: JSON.stringify( tovalidate )},
            complete: function(data){
                //console.log(data.responseText);
				dialog.dialog('option', 'title', 'Wave Picking List');
				$('#content').html(data.responseText);
				dialog.dialog('open');
            },
            error: function (request, status, error) {
                console.log("Ajax request error: "+request.responseText);
            }
        });
    });
	
	$('#items_in_group').click(function() {
		dialog.dialog('option', 'title', 'Loading...');
		$('#content').html("<img src='Include/gif/294.GIF' height='150px' width='150px'>");
		dialog.dialog('open');
		$.ajax({
			url: 'display_data2.php',
			type: 'POST',
			complete: function(data){
				dialog.dialog('close');
				dialog.dialog('option', 'title', 'Items In Group');
				$('#content').html(data.responseText);
				dialog.dialog('open');
			},
			error: function (request, status, error) {
				dialog.dialog('close');
				console.log("Ajax request error: "+request.responseText);
			}
		});
	});
});

function OpenInNewTab(url) {
  var win = window.open(url, '_blank');
  win.focus();
}