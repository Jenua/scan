<?php

$path = $_SERVER['DOCUMENT_ROOT'];
require_once('functionsDB.php');
require_once($path.'/db_connect/connect.php');
$conn = Database::getInstance()->dbc;

if(isset($_POST['po_numbers']) && !empty($_POST['po_numbers']))
{
	$arr = json_decode($_POST['po_numbers']);
	$query_po = "SELECT [PONumber] FROM [dbo].[shiptrack] WHERE";
	foreach($arr as $key => $value)
	{
		if($key == 0)
		{
			$query_po.= " [PONumber] = '".$value."'";
		} else
		{
			$query_po.= " OR [PONumber] = '".$value."'";
		}
	}
	$query_po.=" UNION ALL SELECT [PONumber] FROM [dbo].[manually_shiptrack] WHERE";
	foreach($arr as $key => $value)
	{
		if($key == 0)
		{
			$query_po.= " [PONumber] = '".$value."'";
		} else
		{
			$query_po.= " OR [PONumber] = '".$value."'";
		}
	}
	$query_po.=" GROUP BY [PONumber]";
	//print_r($query_po);
	$result = getData($query_po);
	if(count($result) == count($arr))
	{
		$query_main = "SELECT distinct * FROM
(
	SELECT
	case
	when [ItemRef_FullName] like '%:%' then RIGHT([ItemRef_FullName], (CHARINDEX(':',REVERSE([ItemRef_FullName]),0))-1)
	else [ItemRef_FullName]
	end as [ITEM_SKU],
	sum([quantity]) over  (partition by [ItemRef_FullName]) as [TOTAL_QUANTITY]
	FROM
	(
		SELECT
			[linedetail].[ItemRef_FullName]
			,CAST([linedetail].[quantity] AS INT) as [quantity]
		FROM [dbo].[linedetail]
		LEFT JOIN [groupdetail] ON [groupdetail].[TxnLineID] = [linedetail].[GroupIDKEY]
		LEFT JOIN [shiptrack] ON [groupdetail].[IDKEY] = [shiptrack].[TxnID]
		WHERE
		linedetail.ItemRef_FullName not like '%Price-Adjustment%' 
		and linedetail.ItemRef_FullName not like '%Freight%'
		and linedetail.ItemRef_FullName not like '%warranty%' 
		and linedetail.ItemRef_FullName is not NULL
		and linedetail.ItemRef_FullName not like '%Subtotal%'
		and linedetail.ItemRef_FullName not like '%IDSC-10%'
		and linedetail.ItemRef_FullName not like '%DISCOUNT%'
		and linedetail.ItemRef_FullName not like '%SPECIAL ORDER4%'
		and linedetail.ItemRef_FullName not like '%Manual%' 
		and [shiptrack].[PONumber] IN (";
		foreach($arr as $key => $value)
		{
			if($key == 0 && count($arr) > 1)
			{
				$query_main.= "'".$value."', ";
			} else if(count($arr) == $key+1)
			{
				$query_main.= "'".$value."'";
			} else
			{
				$query_main.= "'".$value."', ";
			}
		}
		$query_main.=")
		UNION ALL
		SELECT
			[manually_linedetail].[ItemRef_FullName]
			,CAST([manually_linedetail].[quantity] AS INT) as [quantity]
		FROM [dbo].[manually_linedetail]
		LEFT JOIN [manually_groupdetail] ON [manually_groupdetail].[TxnLineID] = [manually_linedetail].[GroupIDKEY]
		LEFT JOIN [manually_shiptrack] ON [manually_groupdetail].[IDKEY] = [manually_shiptrack].[TxnID]
		WHERE
		[manually_linedetail].ItemRef_FullName not like '%Price-Adjustment%' 
		and [manually_linedetail].ItemRef_FullName not like '%Freight%'
		and [manually_linedetail].ItemRef_FullName not like '%warranty%' 
		and [manually_linedetail].ItemRef_FullName is not NULL
		and [manually_linedetail].ItemRef_FullName not like '%Subtotal%'
		and [manually_linedetail].ItemRef_FullName not like '%IDSC-10%'
		and [manually_linedetail].ItemRef_FullName not like '%DISCOUNT%'
		and [manually_linedetail].ItemRef_FullName not like '%SPECIAL ORDER4%'
		and [manually_linedetail].ItemRef_FullName not like '%Manual%' 
		and [manually_shiptrack].[PONumber] IN (";
		foreach($arr as $key => $value)
		{
			if($key == 0 && count($arr) > 1)
			{
				$query_main.= "'".$value."', ";
			} else if(count($arr) == $key+1)
			{
				$query_main.= "'".$value."'";
			} else
			{
				$query_main.= "'".$value."', ";
			}
		}
		$query_main.= ")
			) a
		) b
		ORDER BY [ITEM_SKU]";
		
		/*print_r('<pre>');
		print_r($query_main);
		print_r('</pre>');
		die();*/
		
		$result_main = getData($query_main);
		if(!empty($result_main))
		{
			$csv = 'download.php?file='.array2csv($result_main, 'wave_picking_list');
			$table = "<button class='btn btn-success' style='float: left;' onclick='OpenInNewTab(".'"'.$csv.'"'.")'>CSV Report</button><table border='1'>
						<thead>
							<tr>";
			foreach($result_main[0] as $key => $value)
			{
				$table.= "<th>".$key."</th>";
			}
			$table.= "</tr>
					</thead>
					<tbody>";
			/*$color = true;
			$prev_sku = '';*/
			foreach($result_main as $row)
			{
				/*if($row['GROUP_SKU'] != $prev_sku && $color)
				{
					$color = false;
				} else if($row['GROUP_SKU'] != $prev_sku && !$color)
				{
					$color = true;
				}
				if($color) $style='#f4f4f4'; else $style='#fff';*/
				$table.= "<tr>";
				foreach($row as $field)
				{
					$table.= "<td>".$field."</td>";
				}
				$table.= "</tr>";
				/*$prev_sku = $row['GROUP_SKU'];*/
			}
			$table.= "</tbody>
				</table>";
			print_r($table);
		}
	}else{
		$result2 = array();
		foreach($result as $value)
		{
			$result2[]= $value['PONumber'];
		}
		
		$diff = array_diff($arr, $result2);
		//print_r($diff);
		
		if(!array_filter($diff))
		{
			echo 'Please type valid PO numbers.';
		}else
		{
			$message = 'Can not find data for PO numbers:<br>';
			foreach($diff as $lost_po)
			{
				$message.= $lost_po.'<br>';
			}
			echo $message;
		}
	}
}

function array2csv($array, $name)
{
   if (count($array) == 0) {
     return null;
   }
   $file_name = "csv/".$name.".csv";
   $df = fopen($file_name, 'w');
   fputcsv($df, array_keys(reset($array)));
   foreach ($array as $row) {
      fputcsv($df, $row);
   }
   fclose($df);
   $file_name2 = $name.".csv";
   return $file_name2;
}