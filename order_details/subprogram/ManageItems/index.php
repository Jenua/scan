<?php
session_start();
$path = $_SERVER['DOCUMENT_ROOT'];
require_once($path.'/db_connect/connect.php');
require_once($path.'/auth/auth.php');

$auth = new AuthClass();
 
if (isset($_POST["login"]) && isset($_POST["password"])) { //Если логин и пароль были отправлены
    if (!$auth->auth($_POST["login"], $_POST["password"])) { //Если логин и пароль введен не правильно
        echo "<script>alert('Wrong login or password!')</script>";
    }
}
 
if (isset($_GET["is_exit"])) { //Если нажата кнопка выхода
    if ($_GET["is_exit"] == 1) {
        $auth->out(); //Выходим
        header("Location: ?is_exit=0"); //Редирект после выхода
    }
}

$helloString = '';
if ($auth->isAuth()) { // Если пользователь авторизован, приветствуем:  
			$helloString =  "Hello, " . $auth->getLogin() .". <a href='?is_exit=1'>Exit</a>"; //Показываем кнопку выхода
			
require_once('config.DB.php');
require_once('../../php/functionsDB.php');
?>
<!DOCTYPE HTML>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>Manage SKU Shipment Method</title>
	<link rel="stylesheet" type="text/css" href="../../Include/DataTables/media/css/jquery.dataTables.css">
	<link rel="stylesheet" href="../../Include/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="../../Include/bootstrap/css/bootstrap-theme.min.css">
	<script src="../../Include/jQuery/jquery-1.11.3.min.js"></script>
	<script type="text/javascript" charset="utf8" src="../../Include/DataTables/media/js/jquery.dataTables.js"></script>
	<script src="../../Include/bootstrap/js/bootstrap.min.js"></script>
	<script>
		$(document).ready(function() {
			$('#example').DataTable({
    aLengthMenu: [
        [25, 50, 100, 200, -1],
        [25, 50, 100, 200, "All"]
    ],
	"order": [[ 2, "desc" ]],
    iDisplayLength: -1
		} );
		} );
	</script>
	<style>
		#backHref {
			position: absolute;
			right: 1%;
			transition: 0.3s;
			top: 10%;
		}
		#backHref:hover {
			transform: scale(1.1);
		}
	#headerContainer {
	/*position: absolute;
	top: 0px;
	left: 0px;
	width: 90%;*/
	height: 50px;
	padding: 5px 20px;
	/*padding-bottom: 5px;*/
	/*padding-left: 10%;*/
	/*background: rgb(130, 167, 22);*/
	color: #367FBB;
	background: rgba(54, 127, 187, 0.1);
	-webkit-box-shadow: 0 1px 5px rgba(0, 0, 0, 0.25);
	-moz-box-shadow: 0 1px 5px rgba(0, 0, 0, 0.25);
	box-shadow: 0 1px 5px rgba(0, 0, 0, 0.25);
}
.product {
	float: right;
	margin-top: 10px;
	position: relative;
	padding-right: 40px;
}
#backHref img {
	margin-left: 10px;
}
#backHref img:hover {
	transform: scale(1.05); 
}
.product i {
	font-weight: bold;
}
.logo {
	width: 150px;
	float: left;
}
img {
	max-width: 100%;
	vertical-align: top;
}
	</style>
</head>
<body>
<div id="headerContainer">
			<div class="logo"><img src="../../Images/DreamLineLogo_Color_final-01.png"></div>
			<div class="product">
				<i>Manage SKU Shipment Method</i>
				<a id="backHref" href="/order_details/">
					<img width="25px" height="25px" src="/order_details/Images/back.png" alt="Back to Custom Report Builder" title="Back to Custom Report Builder">
				</a>
			</div>
		</div>
<?php
$conn = Database::getInstance()->dbc;
$query = "SELECT * FROM DL_valid_SKU ORDER BY [ISACTIVE] DESC";
$data = getData($conn, $query);
/*
print_r('<pre>');
print_r($data);
print_r('</pre>');
*/

	echo "
	<br><a href='newItem.php'><button class='btn btn-default'>Add new item</button></a><br><br>
	<table data-toggle='table' class='table table-bordered display' id='example'>
		<thead>
			<tr class='info'>
				<!--<th>ID</th>-->
				<th>SKU</th>
				<th>QB_SKU</th>
				<th>ISACTIVE</th>
				<!--<th>DISC_YEAR</th>-->
				<!--<th>UPC</th>-->
				<!--<th>HD_SKU</th>-->
				<!--<th>O.CO_SKU</th>-->
				<!--<th>USE_846</th>-->
				<!--<th>AmazonDS_SKU</th>-->
				<th>SHIP_METHOD</th>
				<!--<th>MSRP</th>-->
				<!--<th>Note</th>-->
				<!--<th>lowes_SKU</th>-->
				<!--<th>HD_Special</th>-->
				<!--<th>Sears Product ID</th>-->
				<!--<th>Sears CommerceHub Merchant SKU</th>-->
				<!--<th>Manufacturer Part Number</th>-->
				<th>EDIT</th>
			</tr>
		</thead>
		<tbody>";
	foreach ($data as $row)
	{
		$postData = json_encode($row);
		//$postData = htmlentities($row);
		echo "
			<tr>
				<!--<td>".$row['ID']."</td>-->
				<td>".$row['SKU']."</td>
				<td>".$row['QB_SKU']."</td>
				<td>".$row['ISACTIVE']."</td>
				<!--<td>".$row['DISC_YEAR']."</td>-->
				<!--<td>".$row['UPC']."</td>-->
				<!--<td>".$row['HD_SKU']."</td>-->
				<!--<td>".$row['O.CO_SKU']."</td>-->
				<!--<td>".$row['USE_846']."</td>-->
				<!--<td>".$row['AmazonDS_SKU']."</td>-->
				<td>".$row['SHIP_METHOD']."</td>
				<!--<td>".$row['MSRP']."</td>-->
				<!--<td>".$row['Note']."</td>-->
				<!--<td>".$row['lowes_SKU']."</td>-->
				<!--<td>".$row['HD_Special']."</td>-->
				<!--<td>".$row['Sears Product ID']."</td>-->
				<!--<td>".$row['Sears CommerceHub Merchant SKU']."</td>-->
				<!--<td>".$row['Manufacturer Part Number']."</td>-->
				<td>
					<form action='edit.php' method='POST'>
						<input type='hidden' name='data' value='".$postData."'>
						<button type='submit'>EDIT</button>
					</form>
			</tr>
		";
	}
	echo "</tbody>
	</table><br><br><br>";
?>
</body>
</html>
<?php
} else { //Если не авторизован, показываем форму ввода логина и пароля
?>
<div style="position: relative; top: 110px; width: 100%;">
<div style="
position: relative;
width: 330px;
height: 160px;
left: 50%;
margin-left: -160px;
padding: 10px;
background: #367FBB;
">
<form method="post" action="">
<fieldset>
    <p><label class="field" for="login">Login:</label><input class="logininput" type="text" name="login"
    value="<?php echo (isset($_POST["login"])) ? $_POST["login"] : null; // Заполняем поле по умолчанию ?>" /></p>
    <p><label class="field" for="password">Password:</label><input class="logininput" type="password" name="password" value="" /></p>
	<p><label class="field" for="password"> </label><input class="loginsubmit" type="submit" value="login" /></p>
</fieldset>
</form>
</div>
</div>
<?php 
}