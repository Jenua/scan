<?php
if (isset($_POST['data']) && !empty($_POST['data']))
{
	$data = $_POST['data'];
	$data = json_decode($data, true);
	/*print_r('<pre>');
	print_r($data);
	print_r('</pre>');*/
}
?>
<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Edit Item</title>
  <link rel="stylesheet" href="../../Include/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="../../Include/bootstrap/css/bootstrap-theme.min.css">
  <script src="../../Include/jQuery/jquery-1.11.3.min.js"></script>
  <script src="../../Include/bootstrap/js/bootstrap.min.js"></script>
	<style>
		#backHref {
			position: absolute;
			right: 1%;
			transition: 0.3s;
			top: 10%;
		}
		#backHref:hover {
			transform: scale(1.1);
		}
	#headerContainer {
	/*position: absolute;
	top: 0px;
	left: 0px;
	width: 90%;*/
	height: 50px;
	padding: 5px 20px;
	/*padding-bottom: 5px;*/
	/*padding-left: 10%;*/
	/*background: rgb(130, 167, 22);*/
	color: #367FBB;
	background: rgba(54, 127, 187, 0.1);
	-webkit-box-shadow: 0 1px 5px rgba(0, 0, 0, 0.25);
	-moz-box-shadow: 0 1px 5px rgba(0, 0, 0, 0.25);
	box-shadow: 0 1px 5px rgba(0, 0, 0, 0.25);
}
.product {
	float: right;
	margin-top: 10px;
	position: relative;
	padding-right: 40px;
}
#backHref img {
	margin-left: 10px;
}
#backHref img:hover {
	transform: scale(1.05); 
}
.product i {
	font-weight: bold;
}
.logo {
	width: 150px;
	float: left;
}
img {
	max-width: 100%;
	vertical-align: top;
}
	</style>
</head>
<body>
<div id="headerContainer">
			<div class="logo"><img src="../../Images/DreamLineLogo_Color_final-01.png"></div>
			<div class="product">
				<i>Manage SKU Shipment Method</i>
				<a id="backHref" href="/order_details/subprogram/ManageItems/index.php">
					<img width="25px" height="25px" src="/order_details/Images/back.png" alt="Back to Manage SKU Shipment Method" title="Back to Manage SKU Shipment Method">
				</a>
			</div>
		</div>
<div class="container">
	<!--<br><a href="../ManageItems/"><button class="btn btn-default">< Back</button></a>-->
	<?php
		//echo '<a href="handleRemoveItem.php?sku='.$data['SKU'].'"><button class="btn btn-danger">Remove this item</button></a>';
	?>
	<form role="form" id="EditItemForm" action="handleEditItem.php" method="POST">
	
	<div class="group">
		<h2>Edit Item</h2>
		
		<div class="form-group">
			<label for="sku">SKU</label>
			<input type="text" class="form-control" id="sku" name="sku" value="<?php echo $data['SKU']; ?>">
		</div>
		
		<div class="form-group">
			<label for="qb_sku">QB_SKU</label>
			<input type="text" class="form-control" id="qb_sku" name="qb_sku" value="<?php echo $data['QB_SKU']; ?>">
		</div>
		
		<div class="form-group">
			<label class="checkbox-inline">
			<?php
				if ($data['ISACTIVE'] == 'TRUE      ') $checkedISACTIVE = 'checked'; else $checkedISACTIVE = '';
				echo '<input type="checkbox" name="isactive" value="TRUE" '.$checkedISACTIVE.'>';
			?>
			ISACTIVE</label>
		</div>
		
		<div class="form-group">
			<label for="disc_year">DISC_YEAR</label>
			<input type="text" class="form-control" id="disc_year" name="disc_year" value="<?php echo $data['DISC_YEAR']; ?>">
		</div>
		
		<div class="form-group">
			<label for="upc">UPC</label>
			<input type="text" class="form-control" id="upc" name="upc" value="<?php echo $data['UPC']; ?>">
		</div>
		
		<div class="form-group">
			<label for="hd_sku">HD_SKU</label>
			<input type="text" class="form-control" id="hd_sku" name="hd_sku" value="<?php echo $data['HD_SKU']; ?>">
		</div>
		
		<div class="form-group">
			<label for="ocosku">O.CO_SKU</label>
			<input type="text" class="form-control" id="ocosku" name="ocosku" value="<?php echo $data['O.CO_SKU']; ?>">
		</div>
		
		<div class="form-group">
			<label class="checkbox-inline">
			<?php
				if ($data['USE_846'] == 'Y') $checkeduse_846 = 'checked'; else $checkeduse_846 = '';
				echo '<input type="checkbox" name="use_846" value="Y" '.$checkeduse_846.'>';
			?>
			USE_846</label>
		</div>
		
		<div class="form-group">
			<label for="amazonds_sku">AmazonDS_SKU</label>
			<input type="text" class="form-control" id="amazonds_sku" name="amazonds_sku" value="<?php echo $data['AmazonDS_SKU']; ?>">
		</div>
		
		<div class="form-group">
			<label for="ship_method">SHIP_METHOD</label>
			<select id="ship_method" name="ship_method" class="form-control">
				<?php
				$selected = '';
				$selectedGR = '';
				$selectedGRIfNot1 = '';
				$selectedLTL = '';
				
				switch ($data['SHIP_METHOD'])
				{
				case "GR":
				case "GROUND":
					$selectedGR = 'selected';
					break;
				case "GR: if not exceed 1":
				case "Ground":
					$selectedGRIfNot1 = 'selected';
					break;
				case "LTL":
					$selectedLTL = 'selected';
					break;
				default:
					$selected = 'selected';
					break;
				}
				echo 
				'<option value="" '.$selected.'></option>
				<option value="GR" '.$selectedGR.'>GR</option>
				<option value="GR: if not exceed 1" '.$selectedGRIfNot1.'>GR: if not exceed 1</option>
				<option value="LTL" '.$selectedLTL.'>LTL</option>';
				?>
			</select>
		</div>
		
		<div class="form-group">
			<label for="msrp">MSRP</label>
			<input type="text" class="form-control" id="msrp" name="msrp" value="<?php echo $data['MSRP']; ?>">
		</div>
		
		<div class="form-group">
			<label for="note">Note</label>
			<input type="text" class="form-control" id="note" name="note" value="<?php echo $data['Note']; ?>">
		</div>
		
		<div class="form-group">
			<label for="lowes_sku">lowes_SKU</label>
			<input type="text" class="form-control" id="lowes_sku" name="lowes_sku" value="<?php echo $data['lowes_SKU']; ?>">
		</div>
		
		<div class="form-group">
			<label for="hd_special">HD_Special</label>
			<input type="text" class="form-control" id="hd_special" name="hd_special" value="<?php echo $data['HD_Special']; ?>">
		</div>
		
		<div class="form-group">
			<label for="searsproductid">Sears Product ID</label>
			<input type="text" class="form-control" id="searsproductid" name="searsproductid" value="<?php echo $data['Sears Product ID']; ?>">
		</div>
		
		<div class="form-group">
			<label for="scmsku">Sears CommerceHub Merchant SKU</label>
			<input type="text" class="form-control" id="scmsku" name="scmsku" value="<?php echo $data['Sears CommerceHub Merchant SKU']; ?>">
		</div>
		
		<div class="form-group">
			<label for="mpn">Manufacturer Part Number</label>
			<input type="text" class="form-control" id="mpn" name="mpn" value="<?php echo $data['Manufacturer Part Number']; ?>">
		</div>
		<input type="hidden" name="sku_ref" value="<?php echo $data['SKU']; ?>">
	</div>
	
	<button type="submit" class="btn btn-success">Save changes</button>
	</form><br><br>
</div>
</body>
</html>