<?php
require_once('../../php/functionsDB.php');
$path = $_SERVER['DOCUMENT_ROOT'];
require_once($path.'/db_connect/connect.php');

if (isset($_POST) && !empty($_POST))
{
	$data = $_POST;
	$data = validateDataUpdate($data);
	
	/*print_r('<pre>');
	print_r($data);
	print_r('</pre>');*/
	$query = "INSERT INTO [dbo].[DL_valid_SKU]
           ([SKU]
           ,[QB_SKU]
           ,[ISACTIVE]
           ,[DISC_YEAR]
           ,[UPC]
           ,[HD_SKU]
           ,[O.CO_SKU]
           ,[USE_846]
           ,[AmazonDS_SKU]
           ,[SHIP_METHOD]";
		   if (!empty($data['msrp'])) $query.= ",[MSRP]";
     $query.="
           ,[Note]
           ,[lowes_SKU]
           ,[HD_Special]
           ,[Sears Product ID]
           ,[Sears CommerceHub Merchant SKU]
           ,[Manufacturer Part Number])
     VALUES
           ('".$data['sku']."'
           ,'".$data['qb_sku']."'
           ,'".$data['isactive']."'
           ,'".$data['disc_year']."'
           ,'".$data['upc']."'
           ,'".$data['hd_sku']."'
           ,'".$data['ocosku']."'
           ,'".$data['use_846']."'
           ,'".$data['amazonds_sku']."'
           ,'".$data['ship_method']."'";
		    if (!empty($data['msrp'])) $query.= ",".$data['msrp'];
      $query.="
           ,'".$data['note']."'
           ,'".$data['lowes_sku']."'
           ,'".$data['hd_special']."'
           ,'".$data['searsproductid']."'
           ,'".$data['scmsku']."'
           ,'".$data['mpn']."')";

	$conn = Database::getInstance()->dbc;
	$result = execQuery($conn, $query);
	if ($result)
	{
		echo "<center>Data created successfully.<br><a href='index.php'>Back</a></center>";
	} else
	{
		echo "<center>Error creating data.<br><a href='index.php'>Back</a></center>";
	}
}

function validateDataUpdate($data)
{
	foreach($data as $key => $value)
	{
		$data[$key] = htmlspecialchars($value, ENT_QUOTES);
	}
	if (!isset($data['isactive'])) $data['isactive'] = 'FALSE';
	if (!isset($data['use_846'])) $data['use_846'] = 'N';
	return $data;
}
?>