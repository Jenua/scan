<?php
require_once('../../php/functionsDB.php');
$path = $_SERVER['DOCUMENT_ROOT'];
require_once($path.'/db_connect/connect.php');

if (isset($_POST) && !empty($_POST))
{
	$data = $_POST;
	$data = validateDataUpdate($data);
	
	/*print_r('<pre>');
	print_r($data);
	print_r('</pre>');*/
	
	$query = "UPDATE [dbo].[DL_valid_SKU]
   SET [SKU] = '".$data['sku']."'
      ,[QB_SKU] = '".$data['qb_sku']."'
      ,[ISACTIVE] = '".$data['isactive']."'
      ,[DISC_YEAR] = '".$data['disc_year']."'
      ,[UPC] = '".$data['upc']."'
      ,[HD_SKU] = '".$data['hd_sku']."'
      ,[O.CO_SKU] = '".$data['ocosku']."'
      ,[USE_846] = '".$data['use_846']."'
      ,[AmazonDS_SKU] = '".$data['amazonds_sku']."'
      ,[SHIP_METHOD] = '".$data['ship_method']."'";
	  if (!empty($data['msrp'])) $query.= ",[MSRP] = ".$data['msrp'];
      $query.="
      ,[Note] = '".$data['note']."'
      ,[lowes_SKU] = '".$data['lowes_sku']."'
      ,[HD_Special] = '".$data['hd_special']."'
      ,[Sears Product ID] = '".$data['searsproductid']."'
      ,[Sears CommerceHub Merchant SKU] = '".$data['scmsku']."'
      ,[Manufacturer Part Number] = '".$data['mpn']."'
 WHERE [SKU] = '".$data['sku_ref']."'";

	$conn = Database::getInstance()->dbc;
	$result = execQuery($conn, $query);
	if ($result)
	{
		echo "<center>Data updated successfully.<br><a href='index.php'>Back</a></center>";
	} else
	{
		echo "<center>Error updating data.<br><a href='index.php'>Back</a></center>";
	}
}

function validateDataUpdate($data)
{
	foreach($data as $key => $value)
	{
		$data[$key] = htmlspecialchars($value, ENT_QUOTES);
	}
	if (!isset($data['isactive'])) $data['isactive'] = 'FALSE';
	if (!isset($data['use_846'])) $data['use_846'] = 'N';
	return $data;
}
?>