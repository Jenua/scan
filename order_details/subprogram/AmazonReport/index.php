<?php
ini_set("memory_limit","512M");
ini_set('max_execution_time', 1200);
$path = $_SERVER['DOCUMENT_ROOT'];
require_once($path.'/order_details/php/functionsDB.php');
require_once($path.'/db_connect/connect.php');
$conn = Database::getInstance()->dbc;
$backFlag = true;

	$queryText = "SELECT
     [PONumber],
     [weight],
     CEILING(cast([weight] as float)/2500) as pallet,
     [RefNumber],
     [ItemRef_FullName],
     [volume],
     [ItemGroupRef_FullName],
     [quantity]
FROM
(
SELECT
     [PONumber],
     sum(weight) over  (partition by [RefNumber])  +70 as [weight],
     [RefNumber],
     [ItemRef_FullName],
     sum([volume]) over  (partition by [RefNumber]) as [volume],
     [ItemGroupRef_FullName],
     [quantity]
FROM
(
SELECT
     [salesorder].[PONumber],
     [salesorder].[RefNumber],
     case when [sold].[Quantity] is NULL
         then
1*cast(replace(Replace(replace(replace([sold].[CustomField9],'lbs', ''),
'lb', ''), ' lbs', ''),' lb','') as int)
          else
[sold].[Quantity]*cast(replace(Replace(replace(replace([sold].[CustomField9],'lbs',
''), 'lb', ''), ' lbs', ''),' lb','') as int)
     end as weight,
     [sold].[ItemRef_FullName],
     cast([shipping_measurements].[Length] as
float)*cast([shipping_measurements].[Width] as
float)*cast([shipping_measurements].[Height] as float) as [volume],
     [salesorderlinegroupdetail].[ItemGroupRef_FullName],
     [sold].[quantity]
   FROM [".DATABASE31."].[dbo].[salesorder]
INNER JOIN [".DATABASE31."].[dbo].[salesorderlinegroupdetail]
ON [salesorder].[TxnID] = [salesorderlinegroupdetail].[IDKEY]
INNER JOIN
(
SELECT * FROM [".DATABASE31."].[dbo].[salesorderlinedetail]
WHERE [salesorderlinedetail].[CustomField9] != 'lbs'
and [salesorderlinedetail].ItemRef_FullName not like '%Price-Adjustment%'
and [salesorderlinedetail].ItemRef_FullName not like '%Freight%'
and [salesorderlinedetail].ItemRef_FullName not like '%warranty%'
and [salesorderlinedetail].ItemRef_FullName is not NULL
and [salesorderlinedetail].ItemRef_FullName not like '%Subtotal%'
and [salesorderlinedetail].ItemRef_FullName not like '%IDSC-10%'
and [salesorderlinedetail].ItemRef_FullName not like '%DISCOUNT%'
and [salesorderlinedetail].ItemRef_FullName not like '%SPECIAL ORDER4%'
and [salesorderlinedetail].ItemRef_FullName not like '%Manual%'
and [salesorderlinedetail].ItemRef_FullName like '%:%'
and [salesorderlinedetail].[Quantity] is not null
)[sold]
ON [salesorderlinegroupdetail].[TxnLineID] = [sold].[GroupIDKEY]
INNER JOIN [".DATABASE."].[dbo].[shipping_measurements]
ON [shipping_measurements].[SKU] =
[salesorderlinegroupdetail].[ItemGroupRef_FullName]/*REVERSE(LEFT(REVERSE([sold].[ItemRef_FullName]),
CHARINDEX(':', REVERSE([sold].[ItemRef_FullName])) - 1))*/
WHERE [salesorder].[CustomerRef_FullName] LIKE 'Amazon'
) a
) b
ORDER BY [RefNumber]";
	$queryName = "Amazon orders report";
	
	if(isset($queryText) && !empty($queryText))
	{
		$results = getData($conn, $queryText);
		$conn = null;
	} else die('<br>Can not get query from DB!<br>');
	$results = preapre_array($results);
	foreach($results as $key=>$result)
	{
		$results[$key] = boxes($result);
	}
	
	$results2 = array();
	foreach($results as $key=>$result)
	{
		$results2[] = array(
		'po' => $result[0]['PONumber'], 
		'pallet' => (integer)$result[0]['pallet'],
		'boxes' => $result[0]['boxes'],
		'total volume' => (integer)$result[0]['volume'], 
		'total weight' => $result[0]['weight']
		);
		unset($results[$key]);
	}
	unset($results);
	
	$fp = fopen('amazon_report.csv', 'w');

	foreach ($results2 as $fields) {
		fputcsv($fp, $fields);
	}

	fclose($fp);
	
	echo '<a href="/order_details/subprogram/AmazonReport/amazon_report.csv" download>Download amazon_report.csv</a>';

function preapre_array($results)
{
	$sorted_array = array();
	$ref = $results[0]['RefNumber'];
	$i=0;
	foreach($results as $result)
	{
		if($result['RefNumber'] != $ref) $i++;
		$sorted_array[$i][]=$result;
	}
	return $sorted_array;
}
	
function boxes($arr)
{
	try{
		$c = count($arr);
		for($i=0; $i<$c; $i++)
		{
			$row = $arr[$i];
			$PONumber = $row['PONumber'];
			$boxes = 0;
			foreach ($arr as $row2)
			{
				if ($row2['PONumber'] == $PONumber)
				{
					if (($row2['ItemGroupRef_FullName']=='GSHST-01-TK') || 
					($row2['ItemGroupRef_FullName']=='GSHST-01-PL'))
					{
						if((integer)$row2['quantity']<=5)
						{
							$qua = 1;
						} else 
						{
							$qua = ceil((integer)$row2['quantity'] / 5);
						}
						$boxes += $qua;
					}else 
					{
						$qua = (integer)$row2['quantity'];
						$boxes += $qua;
					}
				}
			}
			$row['boxes'] = $boxes;
			$arr[$i] = $row;
		}
		return $arr;
	}catch (Exception $e) {
		error_msg($e->getMessage());
		send_error();
	}
}
?>