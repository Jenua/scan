<?php
ini_set("memory_limit","512M");
ini_set('max_execution_time', 3600);
$path = $_SERVER['DOCUMENT_ROOT'];
require_once($path.'/db_connect/connect.php');

echo "<br>Script started<br>";

$inputFileName = 'po_status.csv';
$outputFileName = 'latest_status_report.csv';

echo "<br>Input file: ".$inputFileName."<br>";
echo "<br>Output file: ".$outputFileName."<br>";

$pos = parseCsvFile($inputFileName);

if(!empty($pos)) 
{
    echo "<br>Input file parsed<br>";
} else
{
    echo "<br>Can not open input file<br>";
    die();
}

clear_file($outputFileName);

echo "<br>Output file cleared<br>";

/*print_r('<pre>');
print_r($pos);
print_r('</pre>');
die();*/

foreach($pos as $po)
{
    $status = get_latest_status($po[0]);
    if($status)
    {
        $status['po'] = $po[0];
        save_to_csv($status);
    } else
    {
        $status = ['no status', '', '', '', '', $po[0]];
        save_to_csv($status);
    }
    /*print_r('<pre>');
    print_r($status);
    print_r('</pre>');
    die();*/
}

echo '<a href="/order_details/subprogram/latest_status/'.$outputFileName.'" download>Download '.$outputFileName.'</a>';

function clear_file($outputFileName)
{
    $fp = fopen($outputFileName, 'w');
    fclose($fp);
}

function save_to_csv($status)
{
    $fp = fopen('latest_status_report.csv', 'a');

    fputcsv($fp, $status);

    fclose($fp);
}

function get_latest_status($po)
{
    $conn = Database::getInstance()->dbc;
    $query = "EXEC [dbo].[Latest_status] @PO = ".$conn->quote($po);
    $result = $conn->prepare($query);
    $result->execute();
    $result = $result->fetch(PDO::FETCH_ASSOC); 
    
    if(!empty($result))
    {
        return $result;
    } else
    {
        return false;
    }
}

function parseCsvFile($inputFileName)
{
	$csvData = file_get_contents($inputFileName);
	$lines = explode("\r\n", $csvData);
	$array = array();
	foreach ($lines as $line) {
		$array[] = str_getcsv($line, ',');
	}
	return $array;
}