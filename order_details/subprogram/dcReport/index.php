<?php
session_start();
$path = $_SERVER['DOCUMENT_ROOT'];
require_once($path.'/db_connect/connect.php');
require_once($path.'/auth/auth.php');

$auth = new AuthClass();
 
if (isset($_POST["login"]) && isset($_POST["password"])) {
    if (!$auth->auth($_POST["login"], $_POST["password"])) {
        echo "<script>alert('Wrong login or password!')</script>";
    }
}
 
if (isset($_GET["is_exit"])) {
    if ($_GET["is_exit"] == 1) {
        $auth->out();
        header("Location: ?is_exit=0");
    }
}

$helloString = '';
if ($auth->isAuth()) { 
			$helloString =  "Hello, " . $auth->getLogin() .". <a href='?is_exit=1'>Exit</a>";
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>DC Report System</title>
		<link rel="stylesheet" href="Include/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="Include/bootstrap/css/bootstrap-theme.min.css">
		<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
		<link rel="stylesheet" href="Include/jquery-tageditor/jquery.tag-editor.css">
		<script src="Include/jQuery/jquery-1.11.3.min.js"></script>
		<script src="Include/bootstrap/js/bootstrap.min.js"></script>
		<script src="Include/jquery-ui-1.11.4.custom/jquery-ui.js"></script>
		  
		<script src="Include/jquery-ui-1.11.4.custom/jquery-ui.js"></script>
		<script src="Include/jquery-tageditor/jquery.caret.min.js"></script>
		<script src="Include/jquery-tageditor/jquery.tag-editor.js"></script>
		<script src="js/script.js"></script>
		<style>
		table td {
			border: 1px solid #ddd;
			border-width: 0 1px 1px 0;
		}
		table th {
			text-align: left;
			background: #d9edf7;
			border-bottom: 2px solid #ddd;
		}
		table th, table td {
			padding:  7px 15px 8px;
		}
		table {
			border: 1px solid #ddd;
			width: 100%;
		}
		#headerContainer {
			height: 50px;
			padding: 5px 20px;
			color: #367FBB;
			background: rgba(54, 127, 187, 0.1);
			-webkit-box-shadow: 0 1px 5px rgba(0, 0, 0, 0.25);
			-moz-box-shadow: 0 1px 5px rgba(0, 0, 0, 0.25);
			box-shadow: 0 1px 5px rgba(0, 0, 0, 0.25);
            text-align: center;
		}
		.logo {
			width: 150px;
			float: left;
		}
		.product {
			float: right;
			margin-top: 10px;
			position: relative;
			padding-right: 40px;
		}
		.product i {
			font-weight: bold;
		}
		img {
			max-width: 100%;
			vertical-align: top;
		}
		.tag-editor {
			min-height: 50px;
		}
		</style>
	</head>
	<body>
		<div id="headerContainer">
			<div class="logo"><img src="../../../Include/img/logo.png"></div>
			<?php echo $helloString; ?>
			<div class="product">
				<i>DC Report System</i>
				<strong style="color: black;"></strong>
			</div>
		</div>
		<div class="container">
			<br><h4>Type PO numbers please:</h4>
			<form role="form" id="po_form" action="display_data.php" method="POST">
				
			<div style="padding: 10px;">
			<textarea id='po_numbers'>
			</textarea><br/>
			<script>$(document).ready(function(){ $('#po_numbers').tagEditor({removeDuplicates: false}); })</script>
			<button type="submit" style="float: left;" class="btn btn-success">Show Report</button>			
			</div>       
			</form>
			<br/><br/>
		</div>
		<div class="container rescont" id="resultcontainer"></div>
		
		<div id="dialog">
			<div id="content"></div>
		</div>
	</body>
</html>
<?php
} else {
?>
<div style="position: relative; top: 110px; width: 100%;">
<div style="
position: relative;
width: 330px;
height: 160px;
left: 50%;
margin-left: -160px;
padding: 10px;
background: #367FBB;
">
<form method="post" action="">
<fieldset>
    <p><label class="field" for="login">Login:</label><input class="logininput" type="text" name="login"
    value="<?php echo (isset($_POST["login"])) ? $_POST["login"] : null; ?>" /></p>
    <p><label class="field" for="password">Password:</label><input class="logininput" type="password" name="password" value="" /></p>
	<p><label class="field" for="password"> </label><input class="loginsubmit" type="submit" value="login" /></p>
</fieldset>
</form>
</div>
</div>
<?php 
}