$(document).ready(function() {
	var dialog = $('#dialog').dialog({
        autoOpen: false,
        height: 'auto',
        width: 'auto',
        resizable: false,
        autoResize: true,
        modal: true,
	title: '',
        overlay: { backgroundColor: "#000", opacity: 0.5 },
    });
    dialog.dialog('close');
	
    //Submit event
    $('#po_form').on('submit', function (e) {
        e.preventDefault();
        var form = $('#po_form');
        var url = $(form).attr("action");
        
        var tovalidate = new Array();
        
        $( 'textarea', this ).each(function(){
            if( $(this).val() ) {
                var tagstr = $(this).tagEditor('getTags')[0].tags;
                var tagarr = tagstr.toString().split(',');
		tovalidate = tagarr;
            }
        });
        
        $.ajax({
            url: url,
            type: 'POST',
            data: {po_numbers: JSON.stringify( tovalidate )},
            complete: function(data){
                //console.log(data.responseText);
                /*
                dialog.dialog('option', 'title', 'DC Orders Report');
                $('#content').html();
                dialog.dialog('open');
                //*/
                $('#resultcontainer').html(data.responseText);
            },
            error: function (request, status, error) {
                console.log("Ajax request error: "+request.responseText);
            }
        });
    });
});

function OpenInNewTab(url) {
  var win = window.open(url, '_blank');
  win.focus();
}