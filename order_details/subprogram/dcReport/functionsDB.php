<?php
$path = $_SERVER['DOCUMENT_ROOT'];
require_once($path.'/db_connect/connect.php');

//Execute input query
function execQuery($query)
{
	try{
		$conn = Database::getInstance()->dbc;
		$result = $conn->prepare($query);
		$result->execute();
		//print_r($result);
	}
	catch(PDOException $e){
		die('<br>Can not execute query! '.$e->getMessage().'<br>');
	}
	return $result;
}

//Get all data from DB
function getData($query)
{
	try{
		$conn = Database::getInstance()->dbc;
		$result = $conn->prepare($query);
		$result->execute();
		$result = $result->fetchAll(PDO::FETCH_ASSOC);
	}
	catch(PDOException $e){
		die('<br>Can not execute query! '.$e->getMessage().'<br>');
	}
	return $result;
}
?>