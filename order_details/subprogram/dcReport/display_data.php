<?php

$path = $_SERVER['DOCUMENT_ROOT'];
require_once('functionsDB.php');
require_once($path.'/db_connect/connect.php');
$conn = Database::getInstance()->dbc;

if(isset($_POST['po_numbers']) && !empty($_POST['po_numbers'])) {
	$arr = json_decode($_POST['po_numbers']);
	
	if( $arr ) {
		$arrStr = '';
		foreach( $arr as $po ) {
			if( $arrStr != '' ) {
				$arrStr .= ', ';
			}
			$arrStr .= $conn->quote($po);
		}
		$query1 = "	SET NOCOUNT ON
					BEGIN

					DECLARE @Weights TABLE
					(
						[GroupIDKEY] varchar(255),
						[OneQtyWeight] int
					)
					;

					INSERT INTO @Weights (
						GroupIDKEY,
						OneQtyWeight
						)
					SELECT
						distinct [linedetail].[GroupIDKEY]
						,sum(CAST(dbo.udf_GetNumeric([linedetail].[CustomField9]) AS INT)) over  (partition by [linedetail].[GroupIDKEY])
					FROM [dbo].[linedetail]
					INNER JOIN [dbo].[groupdetail] ON [groupdetail].[TxnLineID] = [linedetail].[GroupIDKEY]
					INNER JOIN [dbo].[shiptrack] ON [shiptrack].[TxnID] = [groupdetail].[IDKEY]
					WHERE [shiptrack].[PONumber] IN ( ".$arrStr." )
					AND [linedetail].[CustomField9] is not null
					AND [linedetail].[CustomField9] != '0'
					;
					END
					;

					SELECT
						  [PONumber]
						, [Truck]
						, SUM(CAST([Quantity] AS int)) AS [Total Items]
						, SUM([TotalWeight]) AS [Total Weight]
					FROM
					(
						SELECT
							  st.[PONumber]
							, tp.[TRUCK]
							, pp.[Quantity]
							, (wt.[OneQtyWeight] * pp.[Quantity]) AS [TotalWeight]	
						FROM [groupdetail] AS gr
						LEFT JOIN [shiptrack] AS st ON st.[TxnId] = gr.[IDKEY]
						LEFT JOIN @Weights AS wt ON wt.[GroupIDKEY] = gr.[TxnLineID]
						LEFT JOIN [DL_valid_SKU] AS dl ON dl.[QB_SKU] = gr.[ItemGroupRef_FullName] AND dl.[QB_SKU] like '%'+dl.[SKU] AND (dl.[Menards_SKU] IS NULL OR dl.[QB_SKU] != dl.[Menards_SKU])
						LEFT JOIN [pallet_preload] AS pp ON pp.[UPC] = dl.[UPC] AND pp.[PONumber] = st.[PONumber]
						LEFT JOIN [truck_preload] AS tp ON pp.[PALLET] = tp.[PALLET] AND tp.[PONumber] = st.[PONumber]
						WHERE st.[PONumber] IN ( ".$arrStr." )
						AND pp.[PALLET] IS NOT NULL
						AND tp.[TRUCK] IS NOT NULL
					) AS a
					GROUP BY [PONumber], [Truck]
					ORDER BY [PONumber], [Truck]
					;";
		

		$query2 = "	SELECT
						  st.[PONumber]
						, dl.[SKU]
						, dl.[UPC]
						, SUM(CAST(pp.[Quantity] AS int)) AS [QTY]
						, tp.[Truck]
					FROM [groupdetail] AS gr
					LEFT JOIN [shiptrack] AS st ON st.[TxnId] = gr.[IDKEY]
					LEFT JOIN [DL_valid_SKU] AS dl ON gr.[ItemGroupRef_FullName] = dl.[QB_SKU] AND dl.[QB_SKU] LIKE '%'+dl.[SKU] AND (dl.[Menards_SKU] IS NULL OR dl.[QB_SKU] != dl.[Menards_SKU])
					LEFT JOIN [pallet_preload] AS pp ON pp.[UPC] = dl.[UPC] AND pp.[PONUMBER] = st.[PONumber]
					LEFT JOIN [truck_preload] AS tp ON tp.[PALLET] = pp.[PALLET] AND tp.[PONumber] = pp.[PONumber]

					WHERE st.[PONumber] IN ( ".$arrStr." )
				      AND gr.[Quantity] IS NOT NULL
					  AND pp.[Quantity] IS NOT NULL
					  AND tp.[Truck] IS NOT NULL
					GROUP BY st.[PONumber], tp.[Truck], dl.[SKU], dl.[UPC]
					ORDER BY [PONumber], [Truck], [QTY], [SKU]
					;";
		
		$query3 = "	SELECT
						  [PONumber]
						, [SKU]
						, [UPC]
						, CONCAT( '<b style=\"color:red;\">', [QuantityLeft], '</b> / ', [Quantity] ) as [Quantity Left / Total]
					FROM (
						SELECT
							  st.[PONumber]
							, dl.[SKU]
							, dl.[UPC]
							, gr.[Quantity] AS [Quantity]							
							, gr.[Quantity] - SUM(ISNULL((CASE WHEN [Truck] IS NULL THEN 0 ELSE CAST(pp.[Quantity] AS int) END), 0)) AS [QuantityLeft]
						FROM [groupdetail] AS gr
						LEFT JOIN [shiptrack] AS st ON st.[TxnId] = gr.[IDKEY]
						LEFT JOIN [DL_valid_SKU] AS dl ON dl.[QB_SKU] = gr.[ItemGroupRef_FullName] AND dl.[QB_SKU] like '%'+dl.[SKU] AND (dl.[Menards_SKU] IS NULL OR dl.[QB_SKU] != dl.[Menards_SKU])
						LEFT JOIN [pallet_preload] AS pp ON pp.[UPC] = dl.[UPC] AND pp.[PONumber] = st.[PONumber]
						LEFT JOIN [truck_preload] AS tp ON tp.[PALLET] = pp.[PALLET] AND tp.[PONumber] = st.[PONumber]
						WHERE st.[PONumber] IN ( ".$arrStr." )
							AND gr.[Quantity] IS NOT NULL	
						GROUP BY st.[PONumber], dl.[SKU], dl.[UPC], gr.[Quantity]
					) AS a
					WHERE [QuantityLeft] > 0";
		
		$results1 = getData($query1);
		$results2 = getData($query2);
		$results3 = getData($query3);
		
		?>
			<ul class="nav nav-tabs">
				<li class="active"><a data-toggle="tab" href="#st">Shipped Products</a></li>
				<li><a data-toggle="tab" href="#nst">Not shipped Products</a></li>
			</ul>
			<div class="tab-content">
                <div id="st" class="tab-pane fade in active">
					<h1>Truck weights</h1>
					<?php if( $results1 ) { ?>
					<table id="example" class="display" cellspacing="0" width="100%">
						<thead>
							<tr>
								<?php
								foreach ($results1[0] as $key => $value)
								{
									echo '<th>'.$key.'</th>';
								}
								?>
							</tr>
						</thead>
						<tbody>
							<?php
							foreach ($results1 as $result)
							{
								echo '<tr>';
								foreach ($result as $key => $value)
								{
									if (empty($value) && $value != '0') echo '<td><b style="color: red;">Missing</b></td>'; else echo '<td>'.$value.'</td>';
								}
								echo '</tr>';
							}
							?>
						</tbody>
					</table>
					<?php } else { ?>
						<table id="example" class="display" cellspacing="0" width="100%">
							<tr>
								<th>No trucks found</th>
							</tr>
						</table>
					<?php } ?>
					<h1>Products in Trucks</h1>
					<?php if( $results2 ) { ?>					
					<table id="example" class="display" cellspacing="0" width="100%">
						<thead>
							<tr>
								<?php
								foreach ($results2[0] as $key => $value)
								{
									echo '<th>'.$key.'</th>';
								}
								?>
							</tr>
						</thead>
						<tbody>
							<?php
							foreach ($results2 as $result)
							{
								echo '<tr>';
								foreach ($result as $key => $value)
								{
									if (empty($value) && $value != '0') echo '<td><b style="color: red;">Missing</b></td>'; else echo '<td>'.$value.'</td>';
								}
								echo '</tr>';
							}
							?>
						</tbody>
					</table>
					<?php } else { ?>
						<table id="example" class="display" cellspacing="0" width="100%">
							<tr>
								<th>No trucks found</th>
							</tr>
						</table>
					<?php } ?>					
				</div>
				<div id="nst" class="tab-pane fade in">
					<h1>Not shipped products</h1>
					<?php if( $results3 ) { ?>
					<table id="example" class="display" cellspacing="0" width="100%">
						<thead>
							<tr>
								<?php
								foreach ($results3[0] as $key => $value)
								{
									echo '<th>'.$key.'</th>';
								}
								?>
							</tr>
						</thead>
						<tbody>
							<?php
							foreach ($results3 as $result)
							{
								echo '<tr>';
								foreach ($result as $key => $value)
								{
									if (empty($value) && $value != '0') echo '<td><b style="color: red;">Missing</b></td>'; else echo '<td>'.$value.'</td>';
								}
								echo '</tr>';
							}
							?>
						</tbody>
					</table>
					<?php } else { ?>
						<table id="example" class="display" cellspacing="0" width="100%">
							<tr>
								<th>All products shipped</th>
							</tr>
						</table>
					<?php } ?>					
				</div>
			</div>
			<br/><br/>
		<?php
	} else {
		?>
			<table id="example" class="display" cellspacing="0" width="100%">
				<tr>
					<th>Please, enter PO Number</th>
				</tr>
			</table>
		<?php
	}
}

function array2csv($array, $name)
{
   if (count($array) == 0) {
     return null;
   }
   $file_name = "csv/".$name.".csv";
   $df = fopen($file_name, 'w');
   fputcsv($df, array_keys(reset($array)));
   foreach ($array as $row) {
      fputcsv($df, $row);
   }
   fclose($df);
   $file_name2 = $name.".csv";
   return $file_name2;
}