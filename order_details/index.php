<?php
session_start();
$path = $_SERVER['DOCUMENT_ROOT'];
require_once($path.'/db_connect/connect.php');
require_once($path.'/auth/auth.php');

$auth = new AuthClass();
 
if (isset($_POST["login"]) && isset($_POST["password"])) {
    if (!$auth->auth($_POST["login"], $_POST["password"])) {
        echo "<script>alert('Wrong login or password!')</script>";
    }
}
 
if (isset($_GET["is_exit"])) {
    if ($_GET["is_exit"] == 1) {
        $auth->out();
        header("Location: ?is_exit=0");
    }
}

$helloString = '';
if ($auth->isAuth()) { 
			$helloString =  "Hello, " . $auth->getLogin() .". <a href='?is_exit=1'>Exit</a>";
	
	require_once('php/functionsDB.php');
	
	$conn = Database::getInstance()->dbc;
	$results = getQueries($conn);
	$conn = null;
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Order Details</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- jQuery -->
		<script type="text/javascript" charset="utf8" src="Include/jQuery/jquery-1.11.3.min.js"></script>
		<script src="Include/codemirror/codemirror-5.3/lib/codemirror.js"></script>
		<script src="Include/codemirror/codemirror-5.3/mode/sql/sql.js"></script>
		<link rel="stylesheet" type="text/css" href="Include/codemirror/codemirror-5.3/lib/codemirror.css">
		<link rel="stylesheet" type="text/css" href="Include/codemirror/codemirror-5.3/lib/main.css">
		<script type="text/javascript" src="js/script.js"></script>
	</head>

	<body>
		<div id="headerContainer">
			<div class="logo"><img src="Images/DreamLineLogo_Color_final-01.png"></div>
			<?php echo $helloString; ?>
			<div class="product">
				<i>Custom </i><strong style="color: black; font-size: 24px;">Report</strong><i> Builder</i>
				<strong style="color: black;"></strong>
			</div>
		</div>
		<div id="darkBackground">
			<div id="editPopup">
				<div id="controlArea">
					<div id="editControls">
						<span><strong>Editor</strong></span>
						<button onclick="cancelEdit()" class="editButton">
							Exit
						</button>
						<button onclick="removeEdit()" class="editButton">
							Remove
						</button>
						<button onclick="saveQuery()" class="editButton">
							Save
						</button>
					</div>
				</div>
				<div id="editAreaQuery">
					<textarea id="editTextArea">
					</textarea>
				</div>
				<div id="editAreaName">
					<b>QUERY NAME:</b><br>
					<textarea id="editTextAreaName">
					</textarea>
				</div>
				<div id="editAreaDescription">
					<b>QUERY DESCRIPTION:</b><br>
					<textarea id="editTextAreaDescription">
					</textarea>
				</div>
			</div>
		</div>
		<div id="queryContainer">
			<!--<a style="display: inline-block; color: #367fbb; margin: 15px 0 0 10px" href="http://ordering_process.com/order_details/index-new.php" target="_blank">Orders</a>-->
			<div class="grid">
				<?php
				foreach ($results as $result)
				{
					echo '
					<div class="container">
						<img class="editIcon" src="/order_details/Images/editIcon.png" onclick="editQuery('.$result['id_odq'].')">
						<a href="/order_details/display_data.php?id='.$result['id_odq'].'">
							<div class="description">
								<p>'.$result['query_description'].'</p>
							</div>
							<div class="outer">
								<div class="middle">
									<div class="inner">
										<p>'.$result['query_name'].'</p>
									</div>
								</div>
							</div>
						</a>
					</div>';
				}
				echo '
					<div class="container" style="cursor: pointer;" onclick="createQuery()">
							<div class="description">
								<p>Create new query</p>
							</div>
							<div class="outer">
								<div class="middle">
									<div class="inner">
										<img width="100px" height="100px" src="/order_details/Images/createQueryIcon.png">
									</div>
								</div>
							</div>
					</div>';
				?>
				</div>
				<br><br>
				<div class="grid">
					<div class="container program" style="cursor: pointer;" onclick="window.location.href = 'http://www.bathauthority.com/ssl/app/Storelocator/csvFile/';">
							<div class="description">
								<p>This is a script to update dealers data in the Storelocator database on bathauthority.com</p>
							</div>
							<div class="outer">
								<div class="middle">
									<div class="inner">
										<p>Update Storelocator Data (bathauthority)</p>
									</div>
								</div>
							</div>
					</div>
					<div class="container program" style="cursor: pointer;" onclick="window.location.href = 'http://dreamline.com/Storelocator/csvFile/';">
							<div class="description">
								<p>This is a script to update dealers data in the Storelocator database on dreamline.com</p>
							</div>
							<div class="outer">
								<div class="middle">
									<div class="inner">
										<p>Update Storelocator Data (dreamline)</p>
									</div>
								</div>
							</div>
					</div>
					<!--<div class="container program" style="cursor: pointer;" onclick="window.location.href = '/order_details/subprogram/';">
							<div class="description">
								<p>Cross-search enables you to display related data from two queries at the same time. Just click on any value in the table and the second table will display all the data that contain these value.</p>
							</div>
							<div class="outer">
								<div class="middle">
									<div class="inner">
										<p>Master - Detail Report</p>
									</div>
								</div>
							</div>
					</div>-->
					<div class="container program" style="cursor: pointer;" onclick="window.location.href = '/general_settings/';">
							<div class="description">
								<p>Web interface to update settings for shipsingle and other scripts.</p>
							</div>
							<div class="outer">
								<div class="middle">
									<div class="inner">
										<p>General Settings</p>
									</div>
								</div>
							</div>
					</div>
					<!--<div class="container program" style="cursor: pointer;" onclick="window.location.href = '/order_details/subprogram/ManageItems/index.php';">
							<div class="description">
								<p>Web interface to manage SKU shipment method and other item info.</p>
							</div>
							<div class="outer">
								<div class="middle">
									<div class="inner">
										<p>Manage SKU Shipment Method</p>
									</div>
								</div>
							</div>
					</div>-->
					<div class="container program" style="cursor: pointer;" onclick="window.location.href = '/order_details/subprogram/AmazonReport/';">
							<div class="description">
								<p>Generate CSV report from salesorder table (po#,qty pallet,qty boxes,total volume ( cubic feet ),total weight)</p>
							</div>
							<div class="outer">
								<div class="middle">
									<div class="inner">
										<p>Amazon Report</p>
									</div>
								</div>
							</div>
					</div>
					<div class="container program" style="cursor: pointer;" onclick="window.location.href = '/order_details/subprogram/sla2/';">
						<div class="description">
							<p> </p>
						</div>
						<div class="outer">
							<div class="middle">
								<div class="inner">
									<p>SLA Report for Active orders</p>
								</div>
							</div>
						</div>
					</div>
					<div class="container program" style="cursor: pointer;" onclick="window.location.href = '/order_details/subprogram/dcReport/';">
						<div class="description">
							<p> </p>
						</div>
						<div class="outer">
							<div class="middle">
								<div class="inner">
									<p>DC Reports</p>
								</div>
							</div>
						</div>
					</div>
			</div>
			<br><br>
		</div>
	</body>
</html>
<?php
} else { //Если не авторизован, показываем форму ввода логина и пароля
?>
<div style="position: relative; top: 110px; width: 100%;">
<div style="
position: relative;
width: 330px;
height: 160px;
left: 50%;
margin-left: -160px;
padding: 10px;
background: #367FBB;
">
<form method="post" action="">
<fieldset>
    <p><label class="field" for="login">Login:</label><input class="logininput" type="text" name="login"
    value="<?php echo (isset($_POST["login"])) ? $_POST["login"] : null; // Заполняем поле по умолчанию ?>" /></p>
    <p><label class="field" for="password">Password:</label><input class="logininput" type="password" name="password" value="" /></p>
	<p><label class="field" for="password"> </label><input class="loginsubmit" type="submit" value="login" /></p>
</fieldset>
</form>
</div>
</div>
<?php 
}