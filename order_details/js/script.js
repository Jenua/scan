var mime = 'text/x-sql';
var editor;
var id;

$( document ).ready(function() {
	var textArea = document.getElementById('editTextArea');
    editor = CodeMirror.fromTextArea(textArea, {
					lineNumbers: true,
					mode: mime,
					indentWithTabs: true,
					smartIndent: true,
					lineNumbers: true,
					matchBrackets : true,
					autofocus: true
	});
});

function createQuery()
{
	id = 'new';
	$('#darkBackground').fadeIn(0);
	$('#editPopup').fadeIn(0);
}
				
function editQuery(id_odq)
{
	id = id_odq;
	$('#darkBackground').fadeIn(0);
	getQuery(id_odq);
}

function cancelEdit()
{
	editor.setValue("");
	editor.clearHistory();
	id = '';
	$('#darkBackground').fadeOut(0);
	window.location.replace("/order_details/");
}

function getQuery(id_odq)
{
	$.ajax({
		url: 'php/callGetQuery.php',
		type: 'POST',
		data: {
			id: id_odq
		},
		complete: function(data){
			//console.log(data.responseText);
			
			try
			{
				var query = JSON.parse(data.responseText);
				$('#editPopup').fadeIn(0);
				//var textArea = document.getElementById('editTextArea');
				//textArea.value = query['query_text'];
				var editTextAreaName = document.getElementById('editTextAreaName');
				var editTextAreaDescription = document.getElementById('editTextAreaDescription');
				
				editTextAreaName.value = query['query_name'];
				editTextAreaDescription.value = query['query_description'];
				
				editor.setValue("");
				editor.clearHistory();
				editor.setValue(query['query_text']);
			}
			catch(e)
			{
				console.log('Error: '+e)
			}
		},
		error: function (request, status, error) {
			console.log("Ajax request error: "+request.responseText);
		}
	});
}

function saveQuery()
{
	var textAreaValue = editor.getValue();
	var editTextAreaName = document.getElementById('editTextAreaName').value;
	var editTextAreaDescription = document.getElementById('editTextAreaDescription').value;

		textAreaValue = JSON.stringify(textAreaValue);
		editTextAreaName = JSON.stringify(editTextAreaName);
		editTextAreaDescription = JSON.stringify(editTextAreaDescription);

		if (id !='new')
		{
			$.ajax({
				url: 'php/callSaveQuery.php',
				type: 'POST',
				data: {
					id_odq: id,
					query_name: editTextAreaName,
					query_description: editTextAreaDescription,
					query_text: textAreaValue
				},
				complete: function(data){
					alert(data.responseText);
				},
				error: function (request, status, error) {
					console.log("Ajax request error: "+request.responseText);
				}
			});
		} else if (id =='new')
		{
			$.ajax({
				url: 'php/callCreateQuery.php',
				type: 'POST',
				data: {
					query_name: editTextAreaName,
					query_description: editTextAreaDescription,
					query_text: textAreaValue
				},
				complete: function(data){
					alert(data.responseText);
				},
				error: function (request, status, error) {
					console.log("Ajax request error: "+request.responseText);
				}
			});
		}
}

function removeEdit()
{
	if (id !='new')
	{
		$.ajax({
			url: 'php/callRemoveQuery.php',
			type: 'POST',
			data: {
				id: id
			},
			complete: function(data){
				alert(data.responseText);
			},
			error: function (request, status, error) {
				console.log("Ajax request error: "+request.responseText);
			}
		});
	} else alert('Nothing to remove');
}