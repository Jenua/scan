<?php
/**
 *
 * User: Yeuvheniia Melnykova
 * Date: 5/12/2017
 * Old chat https://dreamline.com/content/warranty-and-care
 * New chat: https://my.boldchat.com/aid/364514729093008891/account_setup_wizard/_trial_setup_step_3_Chat_Client.jsp?tcid=&piid=10010111.2

 */
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- To ensure proper rendering and touch zooming for all devices -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>BoldChat</title>
</head>
<body>
<h1>Bold Chat test page</h1>
<!-- BoldChat Visitor Monitor HTML v5.00 (Website=My Website,Ruleset=My Invite Ruleset,Floating Chat=- None - -->
<script type="text/javascript">
    window._bcvma = window._bcvma || [];
    _bcvma.push(["setAccountID", "364514729093008891"]);
    _bcvma.push(["setParameter", "WebsiteID", "362485196460332085"]);
    _bcvma.push(["setParameter", "InvitationID", "362485196793284481"]);
    _bcvma.push(["pageViewed", document.location.href, document.referrer]);
    var bcLoad = function(){
        if(window.bcLoaded) return; window.bcLoaded = true;
        var vms = document.createElement("script"); vms.type = "text/javascript"; vms.async = true;
        vms.src = ('https:'==document.location.protocol?'https://':'http://') + "vmss.boldchat.com/aid/364514729093008891/bc.vms4/vms.js";
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(vms, s);
    };
    if(window.pageViewer && pageViewer.load) pageViewer.load();
    else if(document.readyState=="complete") bcLoad();
    else if(window.addEventListener) window.addEventListener('load', bcLoad, false);
    else window.attachEvent('onload', bcLoad);
</script>
<noscript>
    <a href="http://www.boldchat.com" title="Live Chat Software" target="_blank"><img alt="Live Chat Software" src="https://vms.boldchat.com/aid/179697076101781581/bc.vmi?&wdid=179518113798925128" border="0" width="1" height="1" /></a>
</noscript>
<!-- /BoldChat Visitor Monitor HTML v5.00 -->
</body>
</html>