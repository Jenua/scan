var palletCounter = 0;
var palletArray = [];
var dialog = null;

class PP_Product {
    constructor( name, weight, quantity, obj ) {
        this.name = name;
        this.weight = weight * 1;
        this.quantityMax = quantity * 1;
        this.quantity = quantity * 1;
        this.dom = obj;
    }
    checkNewQuantity( value ) {
        if( (value >= 0 && this.quantity+value <= this.quantityMax) || (value <= 0 && this.quantity+value >= 0) ) {
            return true;
        }
        return false;
    }
    modifyQuantity( value ) {
        if( this.checkNewQuantity(value) ) {
            this.quantity += value;
            return true;
        }
        return false;
    }
    updateQuantity() {
        this.dom.html(this.quantity);
    }
    updateClasses() {
        if( !this.checkNewQuantity( -1 ) ) {
            this.dom.parent().parent().addClass('tr-zeroleft');
        } else {
            this.dom.parent().parent().removeClass('tr-zeroleft');
        }
    }
}

class PP_Pallet {
    constructor( name, limit, arr ) {
        this.name = name;
        this.limit = limit * 1;
        this.weight = 0;
        this.dom = arr;
    }
    checkNewWeight( value ) {
        if( (value >= 0 && this.weight+value <= this.limit) || (value <= 0 && this.weight-value >= 0) ) {
            return true;
        }
        return false;
    }
    modifyWeight( value ) {
        if( this.checkNewWeight(value) ) {
            this.weight += value;
            return true;
        }
        return false;
    }
    updateWeight() {
        let suffix = ''; // ' lbs';
		let obj = this;
        $(this.dom).each(function(){
			$(this).html(obj.weight+suffix);
		});
    }
    findCellsByPallet( cells ) {
        var arr = [];
        var name = this.name;
        $.each(cells, function(i,e){
            if( e.palletName == name ) {
                arr.push(e);
            }
        });
        return arr;
    }
}

class PP_Cell {
    constructor( product, pallet, min, max, current, obj ) {
        this.min = min;
        this.max = max;
        this.current = current>max ? max : (current<min ? min : current);
        this.palletName = pallet;
        this.productName = product;
        this.dom = obj;

        this.pallet = {};
        this.product = {};
        this.cells = {};
    }
    setProduct( prds ) {
        this.product = prds[this.productName];
    }
    setPallet( plts ) {
        this.pallet = plts[this.palletName];
    }
    setCells( cls ) {
        this.cells = cls;
    }
    getCellsByPalletName() {
        var arr = [];
        var obj = this;
        $.each(this.cells, function(i,e){
            if( e.palletName == obj.palletName ) {
                arr.push(e);
            }
        });
        return arr;
    }
    getCellsByProductName() {
        var arr = [];
        var obj = this;
        $.each(this.cells, function(i,e){
            if( e.productName == obj.productName ) {
                arr.push(e);
            }
        });
        return arr;
    }
    updateMax() {
        // RESERVED IF NEEDED
        /*
        if( this.product ) {
            this.max = this.current + this.product.quantity;
        } else {
            this.max = this.current;
        }
        this.dom.attr('max', this.max);
        //*/
    }
    updateClasses() {
        //-- NON ZERO Check --
        if( this.current > 0 ) {
            this.dom.addClass('notzero');
        } else {
            this.dom.removeClass('notzero');
        }

        //-- PALLET column check --
        $(this.getCellsByPalletName()).each(function(i,e){
            if( !e.pallet.checkNewWeight(e.product.weight) ) {
                e.dom.addClass('overweight-warning');
            } else {
                e.dom.removeClass('overweight-warning');
            }
        });

        //-- PRODUCT row check --
        this.product.updateClasses();
    }
    updateValues() {
        this.updateMax();
        this.product.updateQuantity();
        this.pallet.updateWeight();
        this.updateClasses();
    }
    update() {
        let newValue = this.dom.val() * 1;
        let delta    = newValue-this.current;
        let prodRes  = this.product.checkNewQuantity( delta * -1 );
        let palRes   = this.pallet.checkNewWeight( delta * this.product.weight );
        if( !prodRes || !palRes ) {
            this.dom.val(this.current);
            return false;
        }
        prodRes = this.product.modifyQuantity( delta * -1 );
        palRes  = this.pallet.modifyWeight( delta * this.product.weight );
        if( !prodRes || !palRes ) {
            console.log( 'UNEXPECTED ERROR' );
            return false;
        }
        this.current = (this.current*1) + (delta*1);
        this.updateValues();
        return true;
    }
    setupActions() {
        var obj = this;
        this.dom.on('keypress', function(e){ return false; });
        this.dom.on('input', function(e){
            obj.update();
        });
    }
}

function createPallet(){
    dialog = $('#dialog').dialog({
        autoOpen: false,
        height: 'auto',
        width: 'auto',
        resizable: false,
        autoResize: true,
        modal: true,
        title: 'Add Pallet',
        overlay: {
            backgroundColor: "#000",
            opacity: 0.5
        },
    });
    dialog.dialog('close');
    dialog.dialog('option', 'title', 'Add Pallet');
    var newPalletName = 'P' + (palletCounter+1);
    $('#content').html( "Pallet ID:<br><input id='truck_name' value='' placeholder='"+newPalletName+"'><br><br><button style='cursor: pointer;' type='button' onclick='savePallet();'>Add Pallet</button>");

    dialog.dialog('open');
}
function savePallet(){
    dialog.dialog('close');
    var newPalletName = $('#truck_name').val().trim();
    var regex = /^[a-z0-9\-_]{2,32}$/i;
    if( newPalletName == '' || regex.test(newPalletName) ) {
        addPallet( newPalletName );
    } else {
        dialog.dialog('close');
        dialog.dialog('option', 'title', 'Validation Error');
        $('#content').html("<b style='color:red;'>Error:</b> Invalid pallet name<br/>Pallet name should be 2-32 characters long and should contain only:<ul><li>Letters ( <b>A-z</b> )</li><li>Numbers ( <b>0-9</b> )</li><li>Dash ( <b>-</b> )</li><li>Underscore ( <b>_</b> )</li></ul>");
        dialog.dialog('open');
    }
}
function addPallet( customName, maxWeight ){
    customName = typeof customName !== 'undefined' ? customName : '';
    maxWeight  = typeof maxWeight  !== 'undefined' ? maxWeight  : 2500;
    var palletPlaceholder = 'P';
    palletCounter++;
    var palletName = customName ? customName : (palletPlaceholder+palletCounter);
    var isExist = false;
    $('.pallet').each(function(){
        var pallet = $(this).attr('data-pallet');
        if( pallet.toLowerCase() == palletName.toLowerCase() && palletName != '' ) {
            isExist = true;
        }
    });
    $( palletArray ).each(function(){
        if( palletName != '' && this.toString().toLowerCase() == palletName.toLowerCase() ) {
            isExist = true;
        }
    });

    if( isExist ) {
        alert('Please type unique Pallet ID!');
        palletCounter--;
    } else {
        $('tr.js-product-head th.js-append-after').after("<th class='danger pallet_col'><span class='truck_name'>"+palletName+"</span><br /><span class=\"pallet\" data-pallet=\""+palletName+"\" data-max-weight=\""+maxWeight+"\">0</span> lbs</th>");
        $('div.js-append-after').after("<div class='table-header-col danger pallet_col'><div><span class='inner'><span class='truck_name'>"+palletName+"</span><br />" +
            "<span class=\"pallet-div\" data-pallet=\""+palletName+"\" data-max-weight=\""+maxWeight+"\">0</span> lbs" +
                "<div class='togglePart'><span class=\"toggle-bg\">\n" +
                    "<span class='textLeft'>L</span>" +
                    "<span class='textRight'>S</span>" +
                    " <input type=\"radio\" name=\"checker["+palletName+"]\" value=\"short\">\n" +
                    " <input type=\"radio\" name=\"checker["+palletName+"]\" value=\"long\" checked>\n" +
                    "<span class=\"switch\"></span>\n" +
                "</span></div>" +
            "</span>" +
            "<div class=\"palletMenu\">\n" +
            "    <i class=\"fa fa-sort-desc\" aria-hidden=\"true\"></i>\n" +
            "    <div class=\"blockList\">\n" +
            "        <span class=\"clearAll js-pallet-clear\" data-pallet=\""+palletName+"\"><i class=\"fa fa-ban\" aria-hidden=\"true\"></i> Clear all</span>\n" +
            "    </div>\n" +
            "</div>" +
            "</div></div>");
        if (!$('.table-header').hasClass('bgPallets')){
            $('.table-header').addClass('bgPallets');
        };
        $('tr.js-product-row').each(function(){
            var product = $(this).attr('data-product');
            var isReadOnly = $(this).hasClass('tr-done');
            var qtyLeft = $(".product[data-product='"+product+"']").attr('data-left');
            $('.js-append-after', this).after("<td class='success pallet_col'><div class='form-group'><input "+(isReadOnly?'readonly':'')+" type=\"number\" data-pallet=\""+palletName+"\" data-product=\""+product+"\" class=\"form-control preload-cell\" min=\"0\" value=\"0\" name=\"product["+product+"]["+palletName+"][quantity]\" /></div></td>");
        });
        //*
        $( '.pallet' ).each(function(){
            let name  = $(this).attr('data-pallet');
            let limit = $(this).attr('data-max-weight');
            if( !(name in Pallets) ) {
                Pallets[name] = new PP_Pallet( name, limit, [$(this)] );
            }
        });
		$( '.pallet-div' ).each(function(){
            let name  = $(this).attr('data-pallet');
            let limit = $(this).attr('data-max-weight');
            if( name in Pallets ) {
                Pallets[name].dom.push( $(this) );
            }
		});
        $( '.preload-cell' ).each(function(){
            let pallet  = $(this).attr('data-pallet');
            let product = $(this).attr('data-product');
            let min     = $(this).attr('min');
            let max     = $(this).attr('max');
            let current = 0;//$(this).val();
            let newName = product+'-'+pallet;
            if( !(newName in Cells) ) {
                var cell = new PP_Cell( product, pallet, min, max, current, $(this) );
                cell.setProduct( Products );
                cell.setPallet( Pallets );
                cell.setCells( Cells );
                cell.update();
                cell.setupActions();
                Cells[newName] = cell;
                cell = null;
            }
        });
        $( '.js-pallet-clear' ).each(function(){
            $(this).on('click', function(e){
                let name  = $(this).attr('data-pallet');
                if( name in Pallets ) {
                    $(Pallets[name].findCellsByPallet( Cells )).each(function(){
                        this.dom.val(0);
                        this.update();
                    });
                }
            });
        });   
        //*/
    }
    tableHeaderFix();
}
function removePallets(po, u)
{
    if( confirm('This action will delete all pallets, trucks and documents for this order. Continue?') ) {
        $.ajax({
            url: 'removePallets.php?po='+po+'&u='+u,
            type: 'POST',
            complete: function(data){
                alert(data.responseText);
                palletNum = 1;
                location.reload();
            },
            error: function (request, status, error) {
                console.log("Ajax request error: "+request.responseText);
            }
        });
    }
}
function OpenInNewTab(url) {
    var win = window.open(url, '_blank');
    win.focus();
}
function truckPreloadGo(po, type){
    var url = '/truck_preload/index.php?t='+type+'&po='+po;
    OpenInNewTab(url);
}
function printLabelsGo(po, type){
    var url = '/shipping_module/hddc_print_labels.php?t='+type+'&po='+po;
    OpenInNewTab(url);
}
function changeHistoryGo(po){
        var url = 'history.php?po='+po;
        OpenInNewTab(url);
}

var Products = {};
var Pallets = {};
var Cells = {};

/*Pallet Preload Fixing Header -- start --*/
( function( $ ) {
    $(document).ready(function(){
        $('#palletForm .wrap').scroll(function(){
            var pageX = $(this).scrollLeft();
            $('.table-header .innerBg').css('margin-left','-' + pageX + 'px');
        });
    });
} )( jQuery );

$(window).on('load resize', tableHeaderFix);

$(document).ready(function(){
    //*
    var dialog = $('#dialog').dialog({
        autoOpen: false,
        height: 'auto',
        width: 'auto',
        resizable: false,
        autoResize: true,
        modal: true,
        title: '',
        overlay: {
            backgroundColor: "#000",
            opacity: 0.5
        },
    });
    dialog.dialog('close');
    $('#palletForm').on('submit', function(e){
        e.preventDefault();
        dialog.dialog('option', 'title', 'Saving data...');
        $('#content').html("<p>Saving data to DB...</p><img src='Include/gif/294.GIF' height='150px' width='150px'>");
        dialog.dialog('open');

        var url = $(this).attr("action");
        var formData = {};
        formData = $(this).serializeForm();
        var formDataStr = JSON.stringify( formData );
        
        $.ajax({
            url: url,
            type: 'POST',
            data: { productStr: formDataStr },
            complete: function(data){
                console.log(data.responseText);
                result = data.responseText;
                dialog.dialog('close');

                dialog.dialog('option', 'title', 'Result');
                $('#content').html(result);
                dialog.dialog('open');
            },
            error: function (request, status, error) {
                console.log("Ajax request error: "+request.responseText);
            }
        });
    });

    $( '.product' ).each(function(){
        let name   = $(this).attr('data-product');
        let maxqty = $(this).attr('data-max-qty');
        let weight = $(this).attr('data-weight');
        Products[name] = new PP_Product( name, weight, maxqty, $(this) );
    });
    $( '.pallet' ).each(function(){
        let name  = $(this).attr('data-pallet');
        let limit = $(this).attr('data-max-weight');
        Pallets[name] = new PP_Pallet( name, limit, [$(this)] );
    });
	$( '.pallet-div' ).each(function(){
		let name  = $(this).attr('data-pallet');
		let limit = $(this).attr('data-max-weight');
		if( name in Pallets ) {
			Pallets[name].dom.push( $(this) );
		}
	});
    $( '.preload-cell' ).each(function(){
        let pallet  = $(this).attr('data-pallet');
        let product = $(this).attr('data-product');
        let min     = $(this).attr('min');
        let max     = $(this).attr('max');
        let current = 0;//$(this).val();

        var cell = new PP_Cell( product, pallet, min, max, current, $(this) );
        cell.setProduct( Products );
        cell.setPallet( Pallets );
        cell.setCells( Cells );
        cell.update();
        cell.setupActions();
        Cells[product+'-'+pallet] = cell;
        cell = null;
    });
	$( '.js-pallet-clear' ).each(function(){
		$(this).on('click', function(e){
			let name  = $(this).attr('data-pallet');
			if( name in Pallets ) {
				$(Pallets[name].findCellsByPallet( Cells )).each(function(){
					this.dom.val(0);
					this.update();
				});
			}
		});
	});      

    /*
    $(".pallet").each(function(){
        var pallet = $(this).attr('data-pallet');
        calculateWeightByPallet( pallet );
    });
    //*/
});

function tableHeaderFix(){
    var cols = [];
    $('#truck_table tbody tr td').each(function () {
        cols.push($(this).width());
    });

    var i = 0,
        colWidth = 0,
        colPadding = 0,
        widthSum = 0,
        widthDouble = 0;

    colPadding = parseInt($('#truck_table tbody tr td').css('padding-left')) + parseInt($('#truck_table tbody tr td').css('padding-right')) + 2;

    if (!$('.table-header').hasClass('bgPallets')){
        $('.table-header').addClass('bgPallets');
    };

    $('.table-header .table-header-col').each(function(){
        colWidth = parseInt(cols[i]) + colPadding;
        if (i == 3){
            colWidth = colWidth + (parseInt(cols[i+1]) + colPadding);
            i++;
        }
        widthSum = widthSum + colWidth;
        $(this).css("width", `${colWidth}px`);
        i++;
    });

    if($('#truck_table thead th').hasClass('pallet_col')){
        $('.table-header .innerBg').css({'background-color' : '#f2dede', 'width' : `${widthSum}px`});
    }
}
/*Pallet Preload Fixing Header -- end --*/



