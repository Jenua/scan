<?php

if(isset($_REQUEST['po']) && !empty($_REQUEST['po']) && isset($_REQUEST['product']) && !empty($_REQUEST['product']) && isset($_REQUEST['skus']) && !empty($_REQUEST['skus']))
{
	$array = form_csv_array($_REQUEST['product'], $_REQUEST['skus']);
	/*print_r("<pre>");
	print_r($array);
	print_r("</pre>");
	die();*/
	$file_name = array2csv($array, $_REQUEST['po']);
	echo $file_name;
}

function form_csv_array($array, $skus)
{
	$result = array();
	foreach($array as $key => $value)
	{
		$row = array();
		$row['UPC'] = $key;
		$row['SKU'] = $skus[$row['UPC']];
		foreach($value as $key2 => $value2)
		{
			$row['Truck '.$key2]= $value2['quantity'];
		}
		$result[]= $row;
	}
	return $result;
}

function array2csv($array, $po)
{
   if (count($array) == 0) {
     return null;
   }
   $file_name = "csv/".$po."_truck_preload_report.csv";
   $df = fopen($file_name, 'w');
   fputcsv($df, array_keys(reset($array)));
   foreach ($array as $row) {
      fputcsv($df, $row);
   }
   fclose($df);
   $file_name2 = $po."_truck_preload_report.csv";
   return $file_name2;
}
?>