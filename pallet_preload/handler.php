<?php
/*print_r("<pre>");
print_r($_REQUEST);
print_r("</pre>");
die();*/
ini_set('max_execution_time', 1200);
$path = $_SERVER['DOCUMENT_ROOT'];
require_once($path.'/db_connect/connect.php');
//SELECT * from paller_preload WHERE PONUMBER = '8668301'
;


if(isset($_REQUEST['po']) && !empty($_REQUEST['po']) && isset($_REQUEST['u']) && !empty($_REQUEST['u']) && isset($_REQUEST['ref']) && !empty($_REQUEST['ref']) && isset($_REQUEST['productStr']) && !empty($_REQUEST['productStr']))
//if(true)
{
	
        
	$po = $_REQUEST['po'];
	$ref = $_REQUEST['ref'];
	$user = $_REQUEST['u'];
	
	$productsStr = $_REQUEST['productStr'];
		
	$products = json_decode( $productsStr, true );	

    $checkers = empty($products['checker']) ? null : $products['checker'];
	$products = empty($products['product']) ? null : $products['product'];
	$dataBefore = getDataArr($po);
    
	if( $products ) {
		$queries = generateQueries( $products, $checkers, $po, $ref );

		/*
		print_r("<pre>");
		print_r($queries);
		print_r("</pre>");
		die();
		//*/
		
		foreach( $queries as $query ) {
			$result = runQuery_empty_result($query);
			if( $result ) {
				// DO NOthing
			} else {
				die('Error. Can not run queries: <br><pre>'.$query.'</pre>');
			}
		}
		echo 'Pallet preload was successfully saved.';
		
	} else {
		die('Nothing to save.');
	}
	
	$dataAfter = getDataArr($po);
	$dataDiff  = compareDataArr( $dataBefore, $dataAfter );	
	if( !empty($dataDiff) && $dataDiff != '[]' ) {
		// 1 = PALLET Changelog | 2 = TRUCK Changelog
		saveDiff( $user, $po, 1, $dataDiff );
	}
} else
{
	echo "Can not get data.";
}

function saveDiff( $user, $po, $type, $diff ) {
	$conn = Database::getInstance()->dbc;	
	$query = "	INSERT INTO [dc_log] (
					  [user]
					, [po]
					, [type]
					, [diff]
					, [date])
				VALUES (
					  ".$conn->quote( $user )."
					, ".$conn->quote( $po )."
					, ".$conn->quote( $type )."
					, ".$conn->quote( $diff )."
					, GETDATE()
				);";
	runQuery_empty_result($query);
}

function compareDataArr( $before, $after ) {
	$diff = [];
	foreach( $before as $SKU => $B ){
		if( !empty($after[$SKU]) ) {
			$A = $after[$SKU];
		} else {
			$A = [];
		}
		$diff_before = array_diff_assoc( $B, $A );
		$diff_after  = array_diff_assoc( $A, $B );
		foreach( $diff_before as $dBk => $dB ) {
			if( $dBk ) {
				$diff[$SKU][$dBk]['before'] = $dB;
				$diff[$SKU][$dBk]['after'] = 0;
			}
		}
		foreach( $diff_after as $dAk => $dA ) {
			if( $dAk ) {
				$diff[$SKU][$dAk]['after'] = $dA;
				if( empty($diff[$SKU][$dAk]['before']) ) {
					$diff[$SKU][$dAk]['before'] = 0;	
				}
			}
		}
	}
	return json_encode($diff);
}

function getDataArr( $po ) {
	$query = "	SELECT 
					  gr.[ItemGroupRef_FullName] AS GroupSKU
					, pp.[Pallet]
					, pp.[Quantity]
				FROM [groupdetail] AS gr
				LEFT JOIN [shiptrack] AS st ON st.[TxnId] = gr.[IDKEY]
				LEFT JOIN [DL_valid_SKU] AS dl ON dl.[QB_SKU] = gr.[ItemGroupRef_Fullname] AND dl.[QB_SKU] like '%'+dl.[SKU] AND (dl.[Menards_SKU] IS NULL OR dl.[QB_SKU] != dl.[Menards_SKU])
				LEFT JOIN [pallet_preload] AS pp ON pp.[UPC] = dl.[UPC] and pp.[PONumber] = st.[PONumber]
				WHERE st.[PONumber] = '".addquote($po)."'
				--AND pp.[Pallet] IS NOT NULL
				--AND pp.[Quantity] IS NOT NULL
				ORDER BY [GroupSKU]
				;";
	//die( $query );
	$result = runQuery($query);
	
	$resArr = [];
	foreach( $result as $res ) {
		$GSKU = $res['GroupSKU'];
		$PLT  = $res['Pallet']; 
		
		if( empty($resArr[$GSKU]) ) {
			$resArr[$GSKU] = [];
		}
		if( empty($resArr[$GSKU][$PLT]) ) {
			$resArr[$GSKU][$PLT] = [];
		}
		$resArr[$GSKU][$PLT] = $res['Quantity'];
	}
	
	return $resArr;
}

function getSize( $check, $index ) {
    $size = empty($check[$index]) ? 'long' : $check[$index];
    switch( $size ) {
    case 'long':
        return 'long'; break;
    case 'short':
        return 'short'; break;
    default: 
        return 'long';
    }    
}

function generateQueries( $products, $checkers, $po, $ref ) {
	/*
	foreach($products as $key => $data)
	{
		$queries_temp = generate_query($key, $data, $po, $ref);
		$queries.= $queries_temp;
	}
	//*/
	$delete_queries = '';
	$insert_queries = '';
	
	foreach($products as $upc => $data) {
		foreach($data as $truck => $row)
		{
			if(!isset($row['quantity']) || $row['quantity'] == '') {
				$row['quantity'] = '0';
			}
				
			if ($row['quantity'] != '0') {
				// smth useful
				$insert_queries.= " 
				IF EXISTS (
					SELECT *
					FROM [dbo].[pallet_preload]
					WHERE [REFNUMBER] = '".addquote($ref)."'
					AND [UPC] = '".addquote($upc)."'
					AND [PALLET] = '".addquote($truck)."'
				)
				BEGIN
					UPDATE [dbo].[pallet_preload]
					SET  [PONUMBER] = '".addquote($po)."'
						,[REFNUMBER] = '".addquote($ref)."'
						,[UPC] = '".addquote($upc)."'
						,[QUANTITY] = '".addquote($row['quantity'])."'
						,[PALLET] = '".addquote($truck)."'
						,[DISTRIBUTE_DATE] = GETDATE()
                                                ,[PALLET_SIZE] = '".addquote(getSize($checkers, $truck))."'
					WHERE [REFNUMBER] = '".addquote($ref)."' AND [UPC] = '".addquote($upc)."' AND [PALLET] = '".addquote($truck)."'
				END
				ELSE BEGIN
					INSERT INTO [dbo].[pallet_preload]
						([PONUMBER]
						,[REFNUMBER]
						,[UPC]
						,[QUANTITY]
						,[PALLET]
						,[DISTRIBUTE_DATE]
                                                ,[PALLET_SIZE])
					VALUES
						('".addquote($po)."'
						,'".addquote($ref)."'
						,'".addquote($upc)."'
						,'".addquote($row['quantity'])."'
						,'".addquote($truck)."'
						,GETDATE()
                                                ,'".addquote(getSize($checkers, $truck))."' )
				END; ";


			} else {
				if( $delete_queries == '' ) {
					$delete_queries .= "SELECT [ID] FROM [dbo].[pallet_preload] WHERE ";
				} else {
					$delete_queries .= " OR ";
				}
				$delete_queries .= "( [REFNUMBER] = '".addquote($ref)."' AND [UPC] = '".addquote($upc)."' AND [PALLET] = '".addquote($truck)."' )";
			}	
			
		}
	}
	$results = [];
	
	
	$result  = "BEGIN TRY
				BEGIN TRAN ";	
	
	if( $delete_queries != '' ) {
		$result .= "DELETE FROM [dbo].[pallet_preload] WHERE [ID] IN ( ".$delete_queries." ) ";
	}
	$result .= "COMMIT TRAN
				END TRY
				BEGIN CATCH
					ROLLBACK TRAN
				END CATCH";
	
	$results['del'] = $result;
	
	$result  = "BEGIN TRY
				BEGIN TRAN ";
	
	$result .= $insert_queries;
	
	$result .= "COMMIT TRAN
				END TRY
				BEGIN CATCH
					ROLLBACK TRAN
				END CATCH";
	
	$results['add'] = $result;
	
	
	//die( "<pre>".print_r($results,true)."</pre>" );
	return $results;	
}


function generate_query($upc, $data, $po, $ref)
{
	$queries = '';
	foreach($data as $truck => $row)
	{
		if ($row['quantity'] != '0')
		{
			$queries.= "
			if exists (SELECT * FROM [dbo].[pallet_preload] with (updlock,serializable) where [REFNUMBER] = '".addquote($ref)."' AND [UPC] = '".addquote($upc)."' AND [PALLET] = '".addquote($truck)."')
			begin
			   UPDATE [dbo].[pallet_preload]
			   SET [PONUMBER] = '".addquote($po)."'
				  ,[REFNUMBER] = '".addquote($ref)."'
				  ,[UPC] = '".addquote($upc)."'
				  ,[QUANTITY] = '".addquote($row['quantity'])."'
				  ,[PALLET] = '".addquote($truck)."'
				  ,[DISTRIBUTE_DATE] = GETDATE()
			   WHERE [REFNUMBER] = '".addquote($ref)."' AND [UPC] = '".addquote($upc)."' AND [PALLET] = '".addquote($truck)."'
			end
			else
			begin
			   INSERT INTO [dbo].[pallet_preload]
			   ([PONUMBER]
			   ,[REFNUMBER]
			   ,[UPC]
			   ,[QUANTITY]
			   ,[PALLET]
			   ,[DISTRIBUTE_DATE])
		 VALUES
			   ('".addquote($po)."'
			   ,'".addquote($ref)."'
			   ,'".addquote($upc)."'
			   ,'".addquote($row['quantity'])."'
			   ,'".addquote($truck)."'
			   ,GETDATE())
			end;";
		} else
		{
			$queries.= "
			   DELETE FROM [dbo].[pallet_preload]
			   WHERE [REFNUMBER] = '".addquote($ref)."' AND [UPC] = '".addquote($upc)."' AND [PALLET] = '".addquote($truck)."';";
		}
		
	}
	return $queries;
}

function beginTransaction()
{
	try
	{
		$conn = Database::getInstance()->dbc;
		$result = $conn->beginTransaction();
		return $result;
	}catch (Exception $e) {
		die("<br>".$e."<br>");
		return false;
	}
}

function commitTransaction()
{
	try
	{
		$conn = Database::getInstance()->dbc;
		$result = $conn->commit();
		return $result;
	}catch (Exception $e) {
		die("<br>".$e."<br>");
		return false;
	}
}

function rollbackTransaction()
{
	try
	{
		$conn = Database::getInstance()->dbc;
		$result = $conn->rollback();
		return $result;
	}catch (Exception $e) {
		die("<br>".$e."<br>");
		return false;
	}
}

function runQuery_empty_result($query)
{
	try
	{
		$conn = Database::getInstance()->dbc;
		$stmt = $conn->prepare($query);
		$result = $stmt->execute();
		return $result;
	}catch (Exception $e) {
		die("<br>".$e->getMessage()."<br><br>".$query);
		return false;
	}
}
function runQuery($query)
{
	try
	{
		$conn = Database::getInstance()->dbc;
		return exec_query($conn, $query);
	}catch (Exception $e) {
		die("<br>".$e->getMessage()."<br><br>".$query);
		return false;
	}
}
function exec_query($conn, $query) {
	try {
		$result = $conn->prepare($query);
		$result->execute();
		return $result;
	}
	catch(PDOException $e) {
		//print_r($e->getMessage());
	}
}
function addquote($str)
{
	$str = str_replace("'", "`", $str);
	return $str;
}
?>