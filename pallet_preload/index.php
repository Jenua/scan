<?php
session_start();
//order status history
$path = $_SERVER['DOCUMENT_ROOT'];
require_once($path.'/db_connect/connect.php');
require_once($path.'/auth/auth.php');
require_once($path.'/pallet_preload/Include/Twig/Autoloader.php');

function exec_query($conn, $query) {
	try {
		$result = $conn->prepare($query);
		$result->execute();
		return $result;
	}
	catch(PDOException $e) {
		print_r($e->getMessage());
	}
}
function parseItemFullName( $name ) {
    return end( explode(":", $name) );
}

function palletSorter( $a, $b ) {
	$tmp = strlen($b['Pallet']) - strlen($a['Pallet']);
	if( $tmp == 0 ) {
		return strcmp(strtolower($b['Pallet']),strtolower($a['Pallet']));
	}
	return $tmp;
}

function parsePalletResults( $results ){
	$pallets = [];
	$products = [];	
	$RefNumber = null;
	$hiddenPallets = [];
	
	foreach( $results as $result ) {
		$pallet = $result['Pallet'];
		$truck  = $result['Truck'];
		$UPC    = $result['UPC'];
		
		if( !empty($pallet) ) {
			
			if( empty($truck) ) {
				if( !isset($pallets[$pallet]) ) {
					$pallets[ $pallet ] = [ 'Pallet' => $pallet, 'MaxWeight' => 2500, 'Weight' => 0, 'Size' => $result['PalletSize'] ];
				}
				$pallets[ $pallet ]['Weight'] += $result['PalletQuantity'] * $result['ProductWeight'];
			} else {
				if( !isset($hiddenPallets[$pallet]) ) {
					$hiddenPallets[ $pallet ] = 1;
				}
			}
			
		}
		uasort($pallets, 'palletSorter');
		if( empty($RefNumber) ) { $RefNumber = $result['RefNumber']; }
		if( !empty($UPC) ) {
			if( !isset($products[$UPC]) ) {
				$products[$UPC] = [
					'UPC'           => $result['UPC'],
					'SKU'           => $result['SKU'],
					'Descr'         => $result['Descr'],
					'ProductWeight' => $result['ProductWeight'],
					'Quantity'      => $result['Quantity'],
					'QuantityLeft'  => $result['Quantity'],
					'ShipMethod'    => $result['SHIP_METHOD'],
					'Supply'        => $result['Supply'],
					'Done'          => $result['Done'],
					'Pallets'       => [],
				];
			}
			$products[$UPC]['Pallets'][$pallet] = $result['PalletQuantity'];
			$products[$UPC]['QuantityLeft'] -= $result['PalletQuantity'];
			
			if( !empty( $hiddenPallets[$pallet] ) ) { // HIDDEN PALLET FIX
				$products[$UPC]['Quantity'] -= $result['PalletQuantity'];
			}
		}
	}
	
	return [
		'RefNumber' => $RefNumber,
		'pallets'   => $pallets,
		'products'  => $products,
		'hiddenPal' => $hiddenPallets,
	];
}

function getDealerType( $conn, $type ) {
	
	$query   = "SELECT * FROM [dc_type] WHERE id = ".$conn->quote( $type );
	$results = exec_query($conn, $query);
	$dealer  = $results->fetchAll(PDO::FETCH_ASSOC);

	return $dealer;
}

Twig_Autoloader::register();
$loader = new Twig_Loader_Filesystem('templates/Pages');
$twig = new Twig_Environment($loader, array(
	'cache'       => 'compilation_cache',
	'auto_reload' => true
));

$auth = new AuthClass();
if( isset($_POST['login']) && isset($_POST["password"]) ) {
	if( !$auth->auth($_POST["login"], $_POST["password"]) ) {		
		$loginName = $_POST["login"] ? $_POST["login"] : '';
		$wrongCredentials = $twig->render('loginPage.html', array('login' => $loginName, 'errmessage' => 'Error: wrong login or password'));
		die($wrongCredentials);		
	}
}
if( isset($_GET["is_exit"]) ) {
    if ($_GET["is_exit"] == 1) {
        $auth->out();
        header("Location: ?is_exit=0");
    }
}
if( !$auth->isAuth() ) {	
	$noAuth = $twig->render('loginPage.html', array('login' => '', 'errmessage' => ''));
	die($noAuth);
}
if ( empty($_GET['po']) )
{
	$noPO = $twig->render('fatalErorr.html', array('error' => 'Error: No PONumber'));
	die($noPO);
}

//die( "<pre>".print_r( $auth->getId(), true )."</pre>" );

$po = $_GET['po'];
$po = trim($po);

// DB Connect
$conn = Database::getInstance()->dbc;

$type = empty($_REQUEST['t']) ? '1' : $_REQUEST['t'];
$dealerType = getDealerType( $conn, $type );

$query="SELECT
			 [PONumber]
			,[RefNumber]
			,[UPC]
			,[SKU]
			,[Prod_desc] as [Descr]
			,[product_weight] as [OrderWeight]
			,[qty_one_product_weight] as [ProductWeight]
			,[Quantity]
			,[distributed_qty] as [PalletQuantity]
			,[Quantity]-[distributed_qty] as [Quantity_left]
			,[PALLET] as [Pallet]
			,[PALLET_SIZE] as [PalletSize]
			,[TRUCK] as [Truck]
			,[SHIP_METHOD]
			,[Date] as [Supply]
			,ISNULL([status], 0) as [Done]
		FROM
		(
			SELECT 
					[shiptrack].[PONumber]
				,[shiptrack].[RefNumber]
				,[groupdetail].[ItemGroupRef_FullName] as [SKU]
				,[groupdetail].[Prod_desc]
				,CAST([groupdetail].[Quantity]  as int) as [Quantity]
				,case
					when [pallet_preload].[QUANTITY] is null then 0
					else CAST([pallet_preload].[QUANTITY]  as int) 
				end as [distributed_qty]
				,[DL_valid_SKU].[UPC]
				,[pallet_preload].[PALLET]
				,ISNULL([pallet_preload].[PALLET_SIZE], 'long') AS [PALLET_SIZE]
				,[truck_preload].[TRUCK]
				,pw.[product_weight]
				,pw.[qty_one_product_weight]
				,[DL_valid_SKU].[SHIP_METHOD]
				,[hddc_checker].[status]
				,[inventory_allocation_per_group].[Date]
			FROM [dbo].[groupdetail]
			LEFT JOIN [shiptrack] ON [groupdetail].[IDKEY] = [shiptrack].[TxnID]
			LEFT JOIN [dbo].[shipping_measurements] ON [shipping_measurements].[SKU] = [groupdetail].[ItemGroupRef_FullName]
			LEFT JOIN [dbo].[DL_valid_SKU] ON [groupdetail].[ItemGroupRef_FullName] = [DL_valid_SKU].[QB_SKU] AND [DL_valid_SKU].[UPC] IS NOT NULL AND ISNULL([DL_valid_SKU].[QB_SKU], '') != ISNULL([DL_valid_SKU].[Menards_SKU], '')
			LEFT JOIN [hddc_checker] ON [hddc_checker].[TxnLineID] = [groupdetail].[TxnLineID] AND [hddc_checker].[PONumber] = [shiptrack].[PONumber]			
			LEFT JOIN [dbo].[pallet_preload] ON [DL_valid_SKU].[UPC] = [pallet_preload].[UPC] AND [shiptrack].[PONUMBER] = [pallet_preload].[PONUMBER] AND [shiptrack].[REFNUMBER] = [pallet_preload].[REFNUMBER]
			LEFT JOIN [dbo].[truck_preload] ON [truck_preload].[PONUMBER] = [pallet_preload].[PONUMBER] AND [truck_preload].[REFNUMBER] = [pallet_preload].[REFNUMBER] AND [truck_preload].[PALLET] = [pallet_preload].[PALLET]
			LEFT JOIN [dbo].[inventory_allocation_per_group] ON [inventory_allocation_per_group].[ItemGroupRef_FullName] = [groupdetail].[ItemGroupRef_FullName]
                        AND [shiptrack].[PONumber] = [inventory_allocation_per_group].[PONumber]
			LEFT JOIN
			(
				SELECT
						distinct [linedetail].[GroupIDKEY]
					,sum(CAST(dbo.udf_GetNumeric([linedetail].[CustomField9]) AS INT)*CAST([linedetail].[quantity] AS INT)) over  (partition by [linedetail].[GroupIDKEY]) AS [product_weight]
					,sum(CAST(dbo.udf_GetNumeric([linedetail].[CustomField9]) AS INT)) over  (partition by [linedetail].[GroupIDKEY]) AS [qty_one_product_weight]
				FROM [dbo].[linedetail]
				INNER JOIN [dbo].[groupdetail] ON [groupdetail].[TxnLineID] = [linedetail].[GroupIDKEY]
				INNER JOIN [dbo].[shiptrack] ON [shiptrack].[TxnID] = [groupdetail].[IDKEY]
				WHERE [shiptrack].[PONumber] = ".$conn->quote($po)."
				AND
				(
						[linedetail].[ItemRef_FullName] not like '%Price-Adjustment%' 
					and [linedetail].[ItemRef_FullName] not like '%Freight%'
					and [linedetail].[ItemRef_FullName] not like '%warranty%' 
					and [linedetail].[ItemRef_FullName] is not NULL
					and [linedetail].[ItemRef_FullName] not like '%Subtotal%'
					and [linedetail].[ItemRef_FullName] not like '%IDSC-10%'
					and [linedetail].[ItemRef_FullName] not like '%DISCOUNT%'
					and [linedetail].[ItemRef_FullName] not like '%SPECIAL ORDER4%'
					and [linedetail].[ItemRef_FullName] not like '%Manual%'
					and [linedetail].[CustomField9] is not null
					and [linedetail].[CustomField9] != '0'
				)
			) pw ON [groupdetail].[TxnLineID] = pw.[GroupIDKEY]
			WHERE [shiptrack].[PONumber] = ".$conn->quote($po)."	
		) a
		WHERE [Quantity] > 0
		ORDER BY SUBSTRING([SKU],1,3), [SHIP_METHOD], [ProductWeight] DESC;";
$results = exec_query($conn, $query);
if ( empty($results) )
{
	$noPO = $twig->render('fatalErorr.html', array('error' => 'Error: Can not get data on this PO: '.$po));
	die($noPO);
}
$results = parsePalletResults( $results->fetchAll(PDO::FETCH_ASSOC) );
$otherPallets = array_unique( array_keys($results['hiddenPal']) );

$palletPreload = $twig->render('palletPreload.html', array(	'PONumber'    => $po,
															'RefNumber'   => $results['RefNumber'],
															'pallets'     => $results['pallets'],
															'products'    => $results['products'],
															'hidden'      => count( $results['hiddenPal'] ),
															'hidden_json' => json_encode($otherPallets),
															'username'    => $auth->getName(),
															'userid'      => $auth->getId(),
															'dealer'      => $dealerType
															));
die($palletPreload);
