<?php
session_start();
//order status history
$path = $_SERVER['DOCUMENT_ROOT'];
require_once($path.'/db_connect/connect.php');
require_once($path.'/auth/auth.php');
require_once($path.'/pallet_preload/Include/Twig/Autoloader.php');

function exec_query($conn, $query) {
	try {
		$result = $conn->prepare($query);
		$result->execute();
		return $result;
	}
	catch(PDOException $e) {
		print_r($e->getMessage());
	}
}
function parseItemFullName( $name ) {
    return end( explode(":", $name) );
}
function getDealerType( $conn, $type ) {	
	$query   = "SELECT * FROM [dc_type] WHERE id = ".$conn->quote( $type );
	$results = exec_query($conn, $query);
	$dealer  = $results->fetchAll(PDO::FETCH_ASSOC);
	return $dealer;
}
function parseLogResults( $src ){
	$res = [];
	foreach( $src as $row ) {
		$type = empty($row['type']) ? 1 : $row['type'];
		$user = empty($row['name']) ? 'Unknown' : $row['name'];
		$diff = empty($row['diff']) ? [] : json_decode($row['diff'], true);
				
		$dateObj = DateTime::createFromFormat('Y-m-d H:i:s.u', $row['date']);
		$date = $dateObj->format('Y-m-d H:i:s');
		
		$res[$type][] = [
			'user' => $user,
			'date' => $date,
			'diff' => $diff,
		];
	}
	return $res;
}


Twig_Autoloader::register();
$loader = new Twig_Loader_Filesystem('templates/Pages');
$twig = new Twig_Environment($loader, array(
	'cache'       => 'compilation_cache',
	'auto_reload' => true
));

$auth = new AuthClass();
if( isset($_POST['login']) && isset($_POST["password"]) ) {
	if( !$auth->auth($_POST["login"], $_POST["password"]) ) {		
		$loginName = $_POST["login"] ? $_POST["login"] : '';
		$wrongCredentials = $twig->render('loginPage.html', array('login' => $loginName, 'errmessage' => 'Error: wrong login or password'));
		die($wrongCredentials);		
	}
}
if( isset($_GET["is_exit"]) ) {
    if ($_GET["is_exit"] == 1) {
        $auth->out();
        header("Location: ?is_exit=0");
    }
}
if( !$auth->isAuth() ) {	
	$noAuth = $twig->render('loginPage.html', array('login' => '', 'errmessage' => ''));
	die($noAuth);
}
if ( empty($_GET['po']) )
{
	$noPO = $twig->render('fatalErorr.html', array('error' => 'Error: No PONumber'));
	die($noPO);
}

$po = $_GET['po'];
$po = trim($po);

$conn = Database::getInstance()->dbc;

$query = "	SELECT
				  u.[name]
				, l.[type]
				, l.[diff]
				, l.[date]	
			FROM [dc_log] AS l
			LEFT JOIN [User] AS u ON l.[user] = u.[id]
			WHERE l.[po] = ".$conn->quote($po)." 
			AND l.[date] > DATEADD(MONTH, -1, GETDATE())
			ORDER BY l.[type], l.[date] DESC, u.[name]
			;";
$results = exec_query($conn, $query);

$logArray = [];
if( !empty($results) ) {
	$logArray = parseLogResults( $results->fetchAll(PDO::FETCH_ASSOC) );	
} 
if( empty($logArray[1]) ) { $logArray[1] = []; }
if( empty($logArray[2]) ) { $logArray[2] = []; }

$preloadHistory = $twig->render('preloadHistory.html', array(	'PONumber'    => $po,
																'palletLog'   => $logArray[1],
																'truckLog'    => $logArray[2],
																//'products'    => $results['products'],
																//'hidden'      => count( $results['hiddenPal'] ),
																//'hidden_json' => json_encode($otherPallets),
																//'username'    => $auth->getName(),
																//'userid'      => $auth->getId(),
																//'dealer'      => $dealerType
															));
die($preloadHistory);
