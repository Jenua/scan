<?php
$path = $_SERVER['DOCUMENT_ROOT'];
if( file_exists($path.'/db_connect/config.DB.test.php') ) {
	require_once($path.'/db_connect/config.DB.test.php');
} else {
	require_once($path.'/db_connect/config.DB.php');
}


class Database
{
    public $dbc;
    private static $instance;
    private $_query, $_error = false, $_results, $_resultsArray, $_count = 0, $_lastId, $_queryCount=0;

    private function __construct()
    {
		try{
			$this -> dbc = new PDO("sqlsrv:server = ".SERVER."; Database = ".DATABASE, USER, PASSWORD/*, array(PDO::ATTR_PERSISTENT => true)*/);
			$this -> dbc -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		} catch(PDOException $e) {
			die('Can not connect to database.');
		}
    }

    public static function getInstance()
    {
        if (!isset(self::$instance))
        {
            $object = __CLASS__;
            self::$instance = new $object;
        }
        return self::$instance;
    }

    public function query($sql, $params = array()){
        $this->_queryCount++;
        $this->_error = false;
        if ($this->_query = $this->dbc->prepare($sql)) {
            $x = 1;
            if (count($params)) {
                foreach ($params as $param) {
                    $this->_query->bindValue($x, $param);
                    $x++;
                }
            }

            if ($this->_query->execute()) {
                //error prevent SQLSTATE[IMSSP]: The active result for the update/insert queries contains no fields
                $q = strpos($this->_query->queryString,"UPDATE")===0 ||
                        strpos($this->_query->queryString,"INSERT")===0 ||
                        strpos($this->_query->queryString,"DELETE")===0? false: true;
                if($q){
                    $this->_results = $this->_query->fetchALL(PDO::FETCH_OBJ);
                    $this->_resultsArray = json_decode(json_encode($this->_results),true);
                }
                $this->_count = $this->_query->rowCount();
                $this->_lastId = $this->dbc->lastInsertId();
            } else{
                $this->_error = true;
            }
        }
        return $this;
    }
    public function findAll($table){
        return $this->action('SELECT *',$table);
    }

    public function findById($id,$table){
        return $this->action('SELECT *',$table,array('id','=',$id));
    }

    public function action($action, $table, $where = array(), $order = array()){
        $sql = "{$action} FROM [{$table}]";
        $value = '';
        if (count($where) === 3) {
            $operators = array('=', '>', '<', '>=', '<=');

            $field = $where[0];
            $operator = $where[1];
            $value = $where[2];

            if(in_array($operator, $operators)){
                $sql .= " WHERE [{$field}] {$operator} ?";
            }
        }
        if( !empty($order) )
            $sql .= " ORDER BY ".$order[0] ." ". $order[1];
        if (!$this->query($sql, array($value))->error()) {
            return $this;
        }
        return false;
    }

    public function get($table, $where, $order = array('ID','ASC') ){
        return $this->action('SELECT *', $table, $where, $order);
    }

    public function delete($table, $where){
        return $this->action('DELETE', $table, $where);
    }

    public function deleteById($table,$id){
        return $this->action('DELETE',$table,array('id','=',$id));
    }

    public function insert($table, $fields = array()){
        $keys = array_keys($fields);
        $values = null;
        $x = 1;

        foreach ($fields as $field) {
            $values .= "?";
            if ($x < count($fields)) {
                $values .= ', ';
            }
            $x++;
        }

        $sql = "INSERT INTO [{$table}] ([". implode('],[', $keys)."]) VALUES ({$values})";

        if (!$this->query($sql, $fields)->error()) {
            return true;
        }
        return false;
    }
    public function update($table, $id, $fields){
        $set = '';
        $x = 1;

        foreach ($fields as $name => $value) {
            $set .= "[{$name}] = ?";
            if ($x < count($fields)) {
                $set .= ', ';
            }
            $x++;
        }

        $sql = "UPDATE [{$table}] SET {$set} WHERE id = {$id}";

        if (!$this->query($sql, $fields)->error()) {
            return true;
        }
        return false;
    }

    public function results($assoc = false){
        if($assoc) return $this->_resultsArray;
        return $this->_results;
    }

    public function first(){
        if (isset($this->results()[0])) return  $this->results()[0];
    }

    public function count(){
        return $this->_count;
    }

    public function error(){
        return $this->_error;
    }

    public function lastId(){
        return $this->_lastId;
    }

    public function getQueryCount(){
        return $this->_queryCount;
    }

}
