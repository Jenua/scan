<?php

//DB and Mail server settings
//144.2.49.15,5001 , 23.24.8.30,5001
//define("SERVER", "tcp:23.24.8.30,5001");
define("SERVER", "tcp:192.168.0.105,1433");
define("USER", "orders");
define("PASSWORD", 'adminnimda');
define("DATABASE", "orders");
//define("DATABASE", "Orders_Test");
define("DATABASE31", "orders31");

//DB tables settings FOR PROD ONLY!!!
define("SHIPTRACK_TABLE", "shiptrack");
define("MANUALLY_SHIPTRACK_TABLE", "manually_shiptrack");
define("SHIPTRACKWITHLABEL_TABLE", "ShipTrackWithLabel");
define("SHIPTRACKWITHLABEL_REMOVED_ORDERS_TABLE", "ShipTrackWithLabel_removed_orders");
define("ORDERS_SHIPPED_TABLE", "Orders_shipped");
define("ORDERS_COMBINED", "Orders_combined");
define("ORDERS_PACKED_TABLE", "Orders_packed");
define("ORDERS_PRINTED_TABLE", "Orders_printed");
define("ORDERS_SCAN_HISTORY", "Orders_Scan_History");
define("ORDERS_INVENTORY", "Inventory_activity");
define("SCAN_TREE", "Scanned_Tree");
define("SCAN_HISTORY", "Scan_History");
define("ITEMINVENTORY", "iteminventory");
define("GROUPDETAIL_TABLE", "groupdetail");
define("MANUALLY_GROUPDETAIL_TABLE", "manually_groupdetail");
define("LINEDETAIL_TABLE", "linedetail");
define("MANUALLY_LINEDETAIL_TABLE", "manually_linedetail");
define("GENERAL_SETTINGS", "general_settings");
define("ORDERS_PICKED_TABLE", "Orders_picked");
define("ORDERS_LOADED_TABLE", "Orders_loaded");
define("ORDERS_LOADED_FULL_TABLE", "Orders_loaded_full");
define("SHIPSINGLE_URL", "/shipsingle/shipsingle.php");
define("UPCCODE_TABLE", "UPCcode");
define("CARRIER_PROFILE", "Carrier_Profile");
define("ZIP_CODE_FOR_NY", "zip_code_for_ny");
define("PALLETRATES", "PalletRates");
define("LEFTINRANGE", "LeftInRange");
define("DL_VALID_SKU", "DL_valid_SKU");
define("LOG", "log");
define("USER_TABLE", "User");
define("SHIPSINGLE_TIME", "shipsingleTime");
define("ITEMGROUP", "itemgroup");
define("SHIPPING_MEASUREMENTS", "shipping_measurements");
define("ALLCOSTS", "AllCosts");
