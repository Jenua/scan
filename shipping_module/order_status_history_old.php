<?php
//order status history
$path = $_SERVER['DOCUMENT_ROOT'];
require_once($path.'/db_connect/connect.php');
function get_icon($status)
{
	$icon = 'Include/pictures/empty.png';
	switch ($status)
		{
		case "Created":
			$icon = 'Include/pictures/CREATED.png';
			break;
		case "Modified":
			$icon = 'Include/pictures/MODIFIED.png';
			break;
		case "Calculated":
			$icon = 'Include/pictures/CALCULATED.png';
			break;
		case "Generated":
			$icon = 'Include/pictures/PRINTED.png';
			break;
		case "Reprinted":
			$icon = 'Include/pictures/REPRINTED.png';
			break;
		case "Scanned":
			$icon = 'Include/pictures/SCANNED.png';
			break;
		case "Picked":
			$icon = 'Include/pictures/PICKED.png';
			break;
		case "Shipped":
			$icon = 'Include/pictures/SHIPPED.png';
			break;
		case "Removed":
			$icon = 'Include/pictures/REMOVED.png';
			break;
		case "Divided":
			$icon = 'Include/pictures/DIVIDED.png';
			break;
		case "Combined":
			$icon = 'Include/pictures/COMBINED.png';
			break;
		}
	return $icon;
}
if (isset($_GET['po']) && !empty($_GET['po']))
{
	$po = $_GET['po'];
	$conn = Database::getInstance()->dbc;
	$query = "EXECUTE shipping_module_status_history @PO = '".$po."'";
	//print_r($query);
	$result = exec_query($conn, $query);
	$result = $result->fetchAll();
?>
<!doctype html>

<html lang="en">
<head>
  <meta charset="utf-8">
	<title>Order Status History</title>
	<link rel="stylesheet" href="Include/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="Include/bootstrap/css/bootstrap-theme.min.css">
	<script src="js/jquery-1.10.2.js"></script>
	<script src="Include/bootstrap/js/bootstrap.min.js"></script>
		<style>
		#backHref {
			position: absolute;
			right: 1%;
			transition: 0.3s;
			top: 10%;
		}
		#backHref:hover {
			transform: scale(1.1);
		}
	#headerContainer {
	/*position: absolute;
	top: 0px;
	left: 0px;
	width: 90%;*/
	height: 50px;
	padding: 5px 20px;
	/*padding-bottom: 5px;*/
	/*padding-left: 10%;*/
	/*background: rgb(130, 167, 22);*/
	color: #367FBB;
	background: rgba(54, 127, 187, 0.1);
	-webkit-box-shadow: 0 1px 5px rgba(0, 0, 0, 0.25);
	-moz-box-shadow: 0 1px 5px rgba(0, 0, 0, 0.25);
	box-shadow: 0 1px 5px rgba(0, 0, 0, 0.25);
}
.product {
	float: right;
	margin-top: 10px;
	position: relative;
	padding-right: 40px;
}
#backHref img {
	margin-left: 10px;
}
#backHref img:hover {
	transform: scale(1.05); 
}
.product i {
	font-weight: bold;
}
.logo {
	width: 150px;
	float: left;
}
img {
	max-width: 100%;
	vertical-align: top;
}

.panel-title{
	text-align: center;
}
	</style>
</head>

<body>
		<div id="headerContainer">
			<div class="logo"><img src="Include/pictures/DreamLineLogo_Color_final-01.png"></div>
			<div class="product">
				<i>Order Status History. PO: <?php echo $po; ?></i>
				<!--<a id="backHref" href="/shipping_module/">
					<img width="25px" height="25px" src="Include/pictures/back.png" alt="Back to Shipping Module" title="Back to Shipping Module">
				</a>-->
			</div>
		</div>
<h3>Status</h3>
<?php
if ($result)
	{
?>
<table data-toggle='table' class='table table-bordered display table-striped'>
<thead>
<tr class='info'>
	<th>Action</th>
	<!--<th>Icon</th>-->
	<th>Date</th>
	<th>Time</th>
	<th>Reason</th>
	<th>Note</th>
	<th>User</th>
</tr>
</thead>
<tbody>
<?php
if($result && !empty($result))
{
	foreach ($result as $res)
	{
		$ts = strtotime($res['OperationDateTime']);
		if(isset($res['Operation']) && !empty($res['Operation']) && isset($res['OperationDateTime']) && !empty($res['OperationDateTime']))
		{
			$res['OperationDate'] = date("m-d-Y", $ts);
			$res['OperationTime'] = date("H:i:s", $ts);
		
			$icon = get_icon($res['Operation']);
			echo "
				<tr>
					<td><img src='".$icon."' width='32px' height='44px' style='margin-right: 50px;'>".$res['Operation']."</td>
					<!--<td><img src='".$icon."' width='32px' height='44px'></td>-->
					<td>".$res['OperationDate']."</td>
					<td>".$res['OperationTime']."</td>
					<td>".$res['Reason']."</td>
					<td>".$res['Note']."</td>
					<td>".$res['userName']."</td>
				</tr>";
		}
	}
} else
{
	echo "
			<tr>
				<td>N/A</td>
				<td>N/A</td>
				<td>N/A</td>
				<td>N/A</td>
				<td>N/A</td>
				<td>N/A</td>
			</tr>";
}
?>
</tbody>
</table>
<?php
} else echo "There is no status information about this order.";
?>
<div class="bs-example">
    <div class="panel-group" id="accordion">
		<br><br>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">Order data</a>
                </h4>
            </div>
            <div id="collapseOne" class="panel-collapse collapse in">
                    <?php
$query_shiptrack = "SELECT 
		shiptrack.[CustomerRef_FullName] as Customer
      ,shiptrack.[RefNumber]
	  ,shiptrack.[TimeCreated]
      ,shiptrack.[ShipAddress_Addr1] as Addr1
      ,shiptrack.[ShipAddress_Addr2] as Addr2
      ,shiptrack.[ShipAddress_Addr3] as Addr3
      ,shiptrack.[ShipAddress_City] as City
      ,shiptrack.[ShipAddress_State] as State
      ,shiptrack.[ShipAddress_PostalCode] as Zip
      ,shiptrack.[ShipAddress_Country] as Country
      ,shiptrack.[FOB] as Phone
      ,shiptrack.[ShipMethodRef_FullName] as Ship_method
	  ,[salesorder].[ShipMethodRef_FullName] as [Actual Ship_method]
	  ,[salesorder].[TimeModified] as [Actual Modified Date]
	  ,[salesorder].[ShipDate] as [Actual ShipDate]
      ,shiptrack.[IsManuallyClosed]
      ,shiptrack.[IsFullyInvoiced]
      ,shiptrack.[Memo] 
	  ,[invoice].[other] as [Tracking #]
	  FROM [".DATABASE."].[dbo].[shiptrack]
	  LEFT JOIN [".DATABASE31."].[dbo].[invoice] ON shiptrack.[PONumber] = [invoice].[PONumber]
	  LEFT JOIN [".DATABASE31."].[dbo].[salesorder] ON shiptrack.[PONumber] = [salesorder].[PONumber]
	  WHERE shiptrack.[PONumber] = '".$po."'
	  UNION ALL
	  SELECT 
		manually_shiptrack.[CustomerRef_FullName] as Customer
      ,manually_shiptrack.[RefNumber]
	  ,manually_shiptrack.[TimeCreated]
      ,manually_shiptrack.[ShipAddress_Addr1] as Addr1
      ,manually_shiptrack.[ShipAddress_Addr2] as Addr2
      ,manually_shiptrack.[ShipAddress_Addr3] as Addr3
      ,manually_shiptrack.[ShipAddress_City] as City
      ,manually_shiptrack.[ShipAddress_State] as State
      ,manually_shiptrack.[ShipAddress_PostalCode] as Zip
      ,manually_shiptrack.[ShipAddress_Country] as Country
      ,manually_shiptrack.[FOB] as Phone
      ,manually_shiptrack.[ShipMethodRef_FullName] as Ship_method
	  ,[salesorder].[ShipMethodRef_FullName] as [Actual Ship_method]
	  ,[salesorder].[TimeModified] as [Actual Modified Date]
	  ,[salesorder].[ShipDate] as [Actual ShipDate]
      ,manually_shiptrack.[IsManuallyClosed]
      ,manually_shiptrack.[IsFullyInvoiced]
      ,manually_shiptrack.[Memo]
	  ,[invoice].[other] as [Tracking #] 
	  FROM [".DATABASE."].[dbo].[manually_shiptrack]
	  LEFT JOIN [".DATABASE31."].[dbo].[invoice] ON manually_shiptrack.[PONumber] = [invoice].[PONumber]
	  LEFT JOIN [".DATABASE31."].[dbo].[salesorder] ON manually_shiptrack.[PONumber] = [salesorder].[PONumber]
	  WHERE manually_shiptrack.[PONumber] = '".$po."'";
/*
print_r('<pre>');
print_r($query_shiptrack);
print_r('</pre>');
*/	  
	  $query_invoice = "SELECT 
		[CustomerRef_FullName] as Customer
      ,[RefNumber]
	  ,[TimeCreated]
      ,[ShipAddress_Addr1] as Addr1
      ,[ShipAddress_Addr2] as Addr2
      ,[ShipAddress_Addr3] as Addr3
      ,[ShipAddress_City] as City
      ,[ShipAddress_State] as State
      ,[ShipAddress_PostalCode] as Zip
      ,[ShipAddress_Country] as Country
      ,[FOB] as Phone
      ,[ShipMethodRef_FullName] as Ship_method
      ,'N/A' as [IsManuallyClosed]
      ,'N/A' as [IsFullyInvoiced]
      ,[Memo]
	  ,[other] as [Tracking #]
	  FROM [".DATABASE31."].[dbo].[invoice] WHERE [PONumber] = '".$po."'";
$result_shiptrack = exec_query($conn, $query_shiptrack);
$result_shiptrack = $result_shiptrack->fetchAll(PDO::FETCH_ASSOC);
$info = "";

if (isset($_GET['source']) && !empty($_GET['source']) && $_GET['source'] == 'orders31') $result_shiptrack = false;

if (!$result_shiptrack)
{
	$result_invoice = exec_query($conn, $query_invoice);
	$result_invoice = $result_invoice->fetchAll(PDO::FETCH_ASSOC);
	if ($result_invoice) 
	{
		$result_shiptrack = $result_invoice;
		$info = "This data is from [".DATABASE31."] database*";
	}
}

if ($result_shiptrack)
{
?>
		<table data-toggle='table' class='table table-bordered display'>
			<thead>
				<tr class='info'>
					<?php
					foreach ($result_shiptrack[0] as $key => $value)
					{
						echo '<th>'.$key.'</th>';
					}
					?>
				</tr>
			</thead>
			<tbody>
				<?php
				foreach ($result_shiptrack as $key1 => $result)
				{
					echo '<tr>';
					foreach ($result as $key => $value)
					{
						if (empty($value) && $value != '0') echo '<td></td>'; else echo '<td>'.$value.'</td>';
					}
					echo '</tr>';
					if(count($result_shiptrack) > 1 && $key1 != count($result_shiptrack)-1)
					{
						echo '<tr class="info">';
						foreach ($result_shiptrack[0] as $key2 => $value1)
						{
							echo '<th>'.$key2.'</th>';
						}
						echo '</tr>';
					}
				}
				?>
			</tbody>
		</table>
				<?php
} else echo "There is no information about this order.";
		?>
            </div>
        </div>
		<?php display_info($info); ?>
		<div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">Calculated order data (shiptrackwithlabel table)</a>
                </h4>
            </div>
            <div id="collapseTwo" class="panel-collapse collapse in">
                    <?php
$query_shiptrackwithlabel = "SELECT [ID] as RefNumber
      ,[CARRIERNAME]
      ,[QUOTENUMBER]
      ,[DEALER]
      ,[FROMZIP]
      ,[TOZIP]
      ,[TRANSITTIME]
      ,[COST]
      ,[WEIGHT]
      ,[LIFTGATE]
      ,[PRO]
      ,[BOL] as [INVOICE]
      ,[SERIAL_REFERENCE]
      ,[RESIDENTIAL]
      ,[delivery_notification] FROM ".SHIPTRACKWITHLABEL_TABLE." WHERE [PONUMBER] = '".$po."'";
$result_shiptrackwithlabel = exec_query($conn, $query_shiptrackwithlabel);
$result_shiptrackwithlabel = $result_shiptrackwithlabel->fetchAll(PDO::FETCH_ASSOC);
if ($result_shiptrackwithlabel)
{
?>
		<table data-toggle='table' class='table table-bordered display'>
			<thead>
				<tr class='info'>
					<?php
					foreach ($result_shiptrackwithlabel[0] as $key => $value)
					{
						echo '<th>'.$key.'</th>';
					}
					?>
				</tr>
<!-- 				<tfoot>
					<?php
					foreach ($result_shiptrackwithlabel[0] as $key => $value)
					{
						echo '<th>'.$key.'</th>';
					}
					?>
				</tfoot> -->
			</thead>
			<tbody>
				<?php
				foreach ($result_shiptrackwithlabel as $key1 => $result)
				{
					echo '<tr>';
					foreach ($result as $key => $value)
					{
						if (empty($value) && $value != '0') echo '<td></td>'; else echo '<td>'.$value.'</td>';
					}
					echo '</tr>';
					if(count($result_shiptrackwithlabel) > 1 && $key1 != count($result_shiptrackwithlabel)-1)
					{
						echo '<tr class="info">';
						foreach ($result_shiptrackwithlabel[0] as $key2 => $value1)
						{
							echo '<th>'.$key2.'</th>';
						}
						echo '</tr>';
					}
				}
				?>
			</tbody>
		</table>
		<?php
	} else echo "There is no calculated information about this order.";
?>
            </div>
        </div>
		<br><br>
		<div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">Products in order + shipping method</a>
                </h4>
            </div>
            <div id="collapseThree" class="panel-collapse collapse in">
                    <?php
$query_groupdetail_DL= "SELECT 
				[shiptrack].[RefNumber]
			  ,[groupdetail].[IDKEY]
			  ,[groupdetail].[TxnLineID]
			  ,[groupdetail].[ItemGroupRef_FullName]
			  ,[groupdetail].[Prod_desc]
			  ,[groupdetail].[Quantity]
			,[DL_valid_SKU].[SHIP_METHOD]
		FROM [dbo].[groupdetail]
		LEFT JOIN [shiptrack] ON [groupdetail].[IDKEY] = [shiptrack].[TxnID]
		LEFT JOIN [DL_valid_SKU] ON [groupdetail].[ItemGroupRef_FullName] = [DL_valid_SKU].[QB_SKU]
		WHERE [shiptrack].[PONumber] = '".$po."'
		UNION ALL
		SELECT 
				[manually_shiptrack].[RefNumber]
			  ,[manually_groupdetail].[IDKEY]
			  ,[manually_groupdetail].[TxnLineID]
			  ,[manually_groupdetail].[ItemGroupRef_FullName]
			  ,[manually_groupdetail].[Prod_desc]
			  ,[manually_groupdetail].[Quantity]
			,[DL_valid_SKU].[SHIP_METHOD]
		FROM [dbo].[manually_groupdetail]
		LEFT JOIN [manually_shiptrack] ON [manually_groupdetail].[IDKEY] = [manually_shiptrack].[TxnID]
		LEFT JOIN [DL_valid_SKU] ON [manually_groupdetail].[ItemGroupRef_FullName] = [DL_valid_SKU].[QB_SKU]
		WHERE [manually_shiptrack].[PONumber] = '".$po."'
		ORDER BY [RefNumber]";

	  $query_invoicelinegroupdetail_DL= "SELECT
				[invoice].[RefNumber]
			  ,[invoicelinegroupdetail].[IDKEY]
			  ,[invoicelinegroupdetail].[TxnLineID]
			  ,[invoicelinegroupdetail].[ItemGroupRef_FullName]
			  ,[invoicelinegroupdetail].[Desc]
			  ,[invoicelinegroupdetail].[Quantity]
			,[DL_valid_SKU].[SHIP_METHOD]
		FROM [".DATABASE31."].[dbo].[invoicelinegroupdetail]
		LEFT JOIN [".DATABASE31."].[dbo].[invoice] ON [invoicelinegroupdetail].[IDKEY] = [invoice].[TxnID]
		LEFT JOIN [".DATABASE."].[dbo].[DL_valid_SKU] ON [invoicelinegroupdetail].[ItemGroupRef_FullName] = [DL_valid_SKU].[QB_SKU]
		WHERE [invoice].[PONumber] = '".$po."'
		ORDER BY [RefNumber]";
		
$result_groupdetail_DL = exec_query($conn, $query_groupdetail_DL);
$result_groupdetail_DL = $result_groupdetail_DL->fetchAll(PDO::FETCH_ASSOC);
$info = "";

if (isset($_GET['source']) && !empty($_GET['source']) && $_GET['source'] == 'orders31') $result_groupdetail_DL = false;

if (!$result_groupdetail_DL)
{
	$result_invoicelinegroupdetail_DL= exec_query($conn, $query_invoicelinegroupdetail_DL);
	$result_invoicelinegroupdetail_DL = $result_invoicelinegroupdetail_DL->fetchAll(PDO::FETCH_ASSOC);
	if ($result_invoicelinegroupdetail_DL)
	{
		$result_groupdetail_DL = $result_invoicelinegroupdetail_DL;
		$info = "This data is from [".DATABASE31."] database*";
	}
}

if ($result_groupdetail_DL)
{
?>
		<table data-toggle='table' class='table table-bordered display'>
			<thead>
				<tr class='info'>
					<?php
					foreach ($result_groupdetail_DL[0] as $key => $value)
					{
						echo '<th>'.$key.'</th>';
					}
					?>
				</tr>
<!-- 				<tfoot>
					<?php
					foreach ($result_groupdetail_DL[0] as $key => $value)
					{
						echo '<th>'.$key.'</th>';
					}
					?>
				</tfoot> -->
			</thead>
			<tbody>
				<?php
				$ref = $result_groupdetail_DL[0]['RefNumber'];
				foreach ($result_groupdetail_DL as $key1 => $result)
				{
					if($result['RefNumber'] != $ref)
					{
						echo '<tr class="info">';
						foreach ($result_groupdetail_DL[0] as $key2 => $value2)
						{
							echo '<th>'.$key2.'</th>';
						}
						echo '</tr>';
					}
					echo '<tr>';
					foreach ($result as $key => $value)
					{
						if (empty($value) && $value != '0') echo '<td></td>'; else echo '<td>'.$value.'</td>';
					}
					echo '</tr>';
					$ref = $result['RefNumber'];
				}
				?>
			</tbody>
		</table>
						<?php
} else echo "There is no information about products in this order.";
		?>
            </div>
        </div>
		<?php display_info($info); ?>
		<div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour">Items in order</a>
                </h4>
            </div>
            <div id="collapseFour" class="panel-collapse collapse in">
                    <?php
$query_linedetail= "SELECT
				[shiptrack].[RefNumber]
			  ,[linedetail].[IDKEY]
			  ,[linedetail].[TxnLineID]
			  ,[linedetail].[CustomField9] as [ItemWeight]
			  ,[linedetail].[ItemRef_FullName]
			  ,[linedetail].[Rate]
			  ,[linedetail].[quantity]
			  ,[linedetail].[GroupIDKEY]
		FROM [dbo].[linedetail]
		LEFT JOIN [groupdetail] ON [groupdetail].[TxnLineID] = [linedetail].[GroupIDKEY]
		LEFT JOIN [shiptrack] ON [groupdetail].[IDKEY] = [shiptrack].[TxnID]
		WHERE [shiptrack].[PONumber] = '".$po."'
		and
						 (
						  [linedetail].ItemRef_FullName not like '%Price-Adjustment%' 
						  and [linedetail].ItemRef_FullName not like '%Freight%'
						  and [linedetail].ItemRef_FullName not like '%warranty%' 
						  and [linedetail].ItemRef_FullName is not NULL
						  and [linedetail].ItemRef_FullName not like '%Subtotal%'
						  and [linedetail].ItemRef_FullName not like '%IDSC-10%'
						  and [linedetail].ItemRef_FullName not like '%DISCOUNT%'
						  and [linedetail].ItemRef_FullName not like '%SPECIAL ORDER4%'
						  and [linedetail].ItemRef_FullName not like '%Manual%'
						  )
		UNION ALL
		SELECT 
				[manually_shiptrack].[RefNumber]
			  ,[manually_linedetail].[IDKEY]
			  ,[manually_linedetail].[TxnLineID]
			  ,[manually_linedetail].[CustomField9] as [ItemWeight]
			  ,[manually_linedetail].[ItemRef_FullName]
			  ,[manually_linedetail].[Rate]
			  ,[manually_linedetail].[quantity]
			  ,[manually_linedetail].[GroupIDKEY]
		FROM [dbo].[manually_linedetail]
		LEFT JOIN [manually_groupdetail] ON [manually_groupdetail].[TxnLineID] = [manually_linedetail].[GroupIDKEY]
		LEFT JOIN [manually_shiptrack] ON [manually_groupdetail].[IDKEY] = [manually_shiptrack].[TxnID]
		WHERE [manually_shiptrack].[PONumber] = '".$po."'
		and
						 (
						  [manually_linedetail].ItemRef_FullName not like '%Price-Adjustment%' 
						  and [manually_linedetail].ItemRef_FullName not like '%Freight%'
						  and [manually_linedetail].ItemRef_FullName not like '%warranty%' 
						  and [manually_linedetail].ItemRef_FullName is not NULL
						  and [manually_linedetail].ItemRef_FullName not like '%Subtotal%'
						  and [manually_linedetail].ItemRef_FullName not like '%IDSC-10%'
						  and [manually_linedetail].ItemRef_FullName not like '%DISCOUNT%'
						  and [manually_linedetail].ItemRef_FullName not like '%SPECIAL ORDER4%'
						  and [manually_linedetail].ItemRef_FullName not like '%Manual%'
						  )
						  ORDER BY [RefNumber]";

$query_invoicelinedetail= "SELECT 
				[invoice].[RefNumber]
			  ,[invoicelinedetail].[IDKEY]
			  ,[invoicelinedetail].[TxnLineID]
			  ,[invoicelinedetail].[CustomField9] as [ItemWeight]
			  ,[invoicelinedetail].[ItemRef_FullName]
			  ,[invoicelinedetail].[Rate]
			  ,[invoicelinedetail].[quantity]
			  ,[invoicelinedetail].[GroupIDKEY]
		FROM [".DATABASE31."].[dbo].[invoicelinedetail]
		LEFT JOIN [".DATABASE31."].[dbo].[invoicelinegroupdetail] ON [invoicelinegroupdetail].[TxnLineID] = [invoicelinedetail].[GroupIDKEY]
		LEFT JOIN [".DATABASE31."].[dbo].[invoice] ON [invoicelinegroupdetail].[IDKEY] = [invoice].[TxnID]
		WHERE [invoice].[PONumber] = '".$po."'
		and
						 (
						  [invoicelinedetail].ItemRef_FullName not like '%Price-Adjustment%' 
						  and [invoicelinedetail].ItemRef_FullName not like '%Freight%'
						  and [invoicelinedetail].ItemRef_FullName not like '%warranty%' 
						  and [invoicelinedetail].ItemRef_FullName is not NULL
						  and [invoicelinedetail].ItemRef_FullName not like '%Subtotal%'
						  and [invoicelinedetail].ItemRef_FullName not like '%IDSC-10%'
						  and [invoicelinedetail].ItemRef_FullName not like '%DISCOUNT%'
						  and [invoicelinedetail].ItemRef_FullName not like '%SPECIAL ORDER4%'
						  and [invoicelinedetail].ItemRef_FullName not like '%Manual%'
						  )
						  ORDER BY [RefNumber]";
$result_linedetail = exec_query($conn, $query_linedetail);
$result_linedetail = $result_linedetail->fetchAll(PDO::FETCH_ASSOC);
$info = "";

if (isset($_GET['source']) && !empty($_GET['source']) && $_GET['source'] == 'orders31') $result_linedetail = false;

if (!$result_linedetail)
{
	$result_invoicelinedetail= exec_query($conn, $query_invoicelinedetail);
	$result_invoicelinedetail = $result_invoicelinedetail->fetchAll(PDO::FETCH_ASSOC);
	if ($result_invoicelinedetail) 
	{
		$result_linedetail = $result_invoicelinedetail;
		$info = "This data is from [".DATABASE31."] database*";
	}
}

if ($result_linedetail)
{
?>
		<table data-toggle='table' class='table table-bordered display'>
			<thead>
				<tr class='info'>
					<?php
					foreach ($result_linedetail[0] as $key => $value)
					{
						echo '<th>'.$key.'</th>';
					}
					?>
				</tr>
<!-- 				<tfoot>
					<?php
					foreach ($result_linedetail[0] as $key => $value)
					{
						echo '<th>'.$key.'</th>';
					}
					?>
				</tfoot> -->
			</thead>
			<tbody>
				<?php
				$ref = $result_linedetail[0]['RefNumber'];
				foreach ($result_linedetail as $key1 => $result)
				{
					if($result['RefNumber'] != $ref)
					{
						echo '<tr class="info">';
						foreach ($result_linedetail[0] as $key2 => $value2)
						{
							echo '<th>'.$key2.'</th>';
						}
						echo '</tr>';
					}
					echo '<tr>';
					foreach ($result as $key => $value)
					{
						if (empty($value) && $value != '0') echo '<td></td>'; else echo '<td>'.$value.'</td>';
					}
					echo '</tr>';
					$ref = $result['RefNumber'];
				}
				?>
			</tbody>
		</table>
						<?php
} else echo "There is no information about items in this order.";
		?>
            </div>
        </div>
		<?php display_info($info); ?>
    </div>
</div>
<br>
<br>
<br>
<?php
display_info("* We are looking for the order information in multiple databases simultaneously. 
If you see a message 'the data is from [".DATABASE31."] database*' under any of the tables, it means that this order is not active now. 
This order either was already fully processed or still not in the system.");
?>
<br>
</body>
</html>
<?php
}
$conn = null;

function display_info($info)
{
	echo "<p style='font-size: 10px; margin-top: 10px; margin-bottom: 30px; margin-left: 30px; font-weight: bold; font-style: italic; color: grey;'>".$info."</p>";
}

function exec_query($conn, $query)
{
	try{
		$result = $conn->prepare($query);
		$result->execute();
		return $result;
	}
	catch(PDOException $e)
	{
		print_r($e->getMessage());
	}
}
?>