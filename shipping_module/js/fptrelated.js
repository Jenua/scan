var user = '';

function editButton(e) {
    e.stopPropagation();
    var Btn = $('em', this);
    var Row = $(this).parent().parent();
    var id = Row.attr('data-id');

    if( Btn.hasClass('fa-floppy-o') ) {            
        Btn.removeClass('fa-floppy-o')
        Btn.addClass('fa-pencil');
        // SAVE            
        var Input = $('input', Row);
        var oldValue = Input.attr('data-value').trim();
        var newValue = Input.val().trim();
        var isSubitem = Row.hasClass('js-subitem');

        if( newValue != oldValue ) {
            $.post( "phpFunctions/relatedProducts.php",
                {
                    product:id,
                    type: (isSubitem?'related':'product'),
                    oldVal:oldValue,
                    newVal:newValue,
                    user:user
                },
                function(data){
                    var result = JSON.parse(data);
                    if( result.error ) {
                        
                        swal('Error!', result.error, 'error');
                        
                        $('div.name', Row).html(oldValue);
                    } else {
                        $('div.name', Row).html(newValue);
                        if( !isSubitem ) {
                            $('[data-id='+oldValue+']').each(function(){
                               $(this).attr('data-id', newValue);
                            });
                        }
                    }
                }
            );
        } else {
            $('div.name', Row).html(oldValue);
        }
    } else if( Btn.hasClass('fa-pencil') ) {
        Btn.removeClass('fa-pencil')
        Btn.addClass('fa-floppy-o');
        // EDIT
        var data = $('div.name', Row).html();
        $('div.name', Row).html( '<input class="form-control" onclick="return false;" type=text name=edit value="'+data+'" data-value="'+data+'">' );
        $('input', Row).click(function(e){e.stopPropagation();});
    }    
}
function addItem() {
    $('table.fptrelated tbody').prepend('<tr data-id="" class="js-mainitem">'
                                        +   '<td class="bg-primary mainitem">'
                                        +       '<div class="name"><input class="form-control" onclick="return false;" type=text name=edit value="" data-value=""></div>'
                                        +   '</td>'
                                        +   '<td class="bg-primary" align="right">'
                                        +       '<a class="btn btn-success btn-add2"><em class="fa fa-plus"></em></a> '
                                        +       '<a class="btn btn-default btn-edit"><em class="fa fa-floppy-o"></em></a> '
                                        +       '<a class="btn btn-danger btn-delete"><em class="fa fa-trash"></em></a>'
                                        +   '</td>'
                                        +'</tr>');
    var Row = $('table.fptrelated tbody').first();
    $('.btn-add2', Row).click(addSubitem);
    $('.btn-edit', Row).click(editButton);
    $('.btn-delete', Row).click(deleteItem);
}
function addSubitem(e) {
    e.stopPropagation();
    var Row = $(this).parent().parent();
    var id = Row.attr('data-id');
    $('.js-subitem[data-id='+id+']').fadeIn();

    Row.after('<tr class="js-subitem" data-id="'+id+'">'
            +   '<td>'
            +       '<div class="name"><input class="form-control" onclick="return false;" type=text name=edit value="" data-value=""></div>'
            +   '</td>'
            +   '<td align="right">'
            +       '<a class="btn btn-default btn-edit"><em class="fa fa-floppy-o"></em></a> '
            +       '<a class="btn btn-danger btn-delete"><em class="fa fa-trash"></em></a>'
            +   '</td>'
            +'</tr>');
    $('.btn-edit', Row.next()).click(editButton);
    $('.btn-delete', Row.next()).click(deleteItem);
}
function deleteItem(e) {
    e.stopPropagation();
    var Row = $(this).parent().parent();
    var id = Row.attr('data-id');
    var DataBlock = $('.name', Row);

    var data = DataBlock.has('input').length
             ? $('input', DataBlock).attr('data-value')
             : DataBlock.html();

    var isSubitem = Row.hasClass('js-subitem');

    var message1 = isSubitem?"related":"";
    var message2 = isSubitem?"":"and all related products ";

    swal({
        title: "Are you sure?",
        text: "This "+message1+" product "+message2+"will be removed from FPT Related table.",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete!",
        showLoaderOnConfirm: true,
        preConfirm: function(){
            return new Promise(function (resolve, reject) {
                if( data ) {
                    $.post( "phpFunctions/relatedProducts.php",
                        {
                            product:id,
                            type: 'delete',                            
                            oldVal:data,
                            user:user                            
                        },
                        function(data){
                            var result = JSON.parse(data);
                            if( result.error ) {
                                reject( result.error );
                            } else {
                                resolve();
                            }
                        }
                    );
                } else {
                    resolve();
                }
            });                
        }        
    }).then(function(){
        //=== resolve
        if(isSubitem) {
            Row.remove();
        } else {
            $('[data-id='+id+']').remove();
        }
        swal("Deleted!", "Product has been deleted from FPT Related table.", "success");
        //=== resolve
    }, function(dismiss, err){
        //=== reject
        if( dismiss == 'ajax' ) {
            swal("Error!", err, "error"); 
        }
        
        //=== reject
    });
}
function restoreItem(e) {
	e.stopPropagation();
	var Row = $(this).parent().parent();
	var id = Row.attr('data-id');
	var data = id;
	
    swal({
        title: "Are you sure?",
        text: "\"Incompatible products\" status will be restored for this Order",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, restore!",
        showLoaderOnConfirm: true,
        preConfirm: function(){
            return new Promise(function (resolve, reject) {
                if( data ) {
                    $.post( "phpFunctions/relatedProducts.php",
                        {
                            product:id,
                            type: 'restore',
                            oldVal:data,
                            user:user
                        },
                        function(data){
                            var result = JSON.parse(data);
                            if( result.error ) {
                                reject( result.error );
                            } else {
                                resolve();
                            }
                        }
                    );
                } else {
                    resolve();
                }
            });                
        }        
    }).then(function(){
        //=== resolve
        Row.remove();
        swal("Restored!", "Order status has been restored.", "success");
        //=== resolve
    }, function(dismiss, err){
        //=== reject
        if( dismiss == 'ajax' ) {
            swal("Error!", err, "error"); 
        }
        
        //=== reject
    });	
}

function importData(e) {
    swal({
        title: 'CSV Import',
        type: 'info',
        html: '<form class="js-import" enctype="multipart/form-data" action="" method="POST">'
            +	'<div class="row align-top import-radio">'
            +		'<div class="col-sm-6 text-left">'
            +			'<label><h3><input type="radio" name="replace" value="0" checked> Update<br/><small>Only new information will be added</small></h3></label>'
            +		'</div>'
            +		'<div class="col-sm-6 text-left">'
            +			'<label><h3><input type="radio" name="replace" value="1"> Replace<br/><small>All information will be replaced with imported</small></h3></label>'
            +		'</div>'
            +	'</div>'
            +	'<div class="row align-top import-radio">'
            +		'<div class="col-sm-6 col-sm-offset-3">'
            +                   '<input type="file" name="fpt_file">'
            +                   '<input type="hidden" name="user" value="'+user+'">'
            +		'</div>'
            +	'</div>'
            + '</form>',
        showCloseButton: true,
        showCancelButton: true,
        confirmButtonText: 'Upload',
        cancelButtonText: 'Cancel',
        showLoaderOnConfirm: true,
        preConfirm: function(){
            return new Promise(function(resolve, reject){
                var Form = $("form.js-import");
                Form.on('submit', function(e){
                    e.stopPropagation();
                    e.preventDefault();
                    $.ajax({
                        url: "phpFunctions/fptRelatedImport.php",
                        type: 'POST',
                        //dataType: 'json',
                        data: new FormData(this),
                        cache: false,
                        processData: false,
                        contentType:false,                        
                        success: function(data){
                            var result = JSON.parse(data);
                            if( result.error ) {
                                console.log( result.error );
                                reject( result.error );
                            } else {
                                resolve();
                            }
                        }
                    });
                });
                Form.submit();
            });
        }
      
    }).then(function(){
        //SUBMIT
        swal("Imorted!", "New Data imported", "success");
    });    
}

$(document).ready(function () {
    $('.js-subitem').hide();
    $('.js-mainitem').click(function(){
        var id = $(this).attr('data-id');
        $('.js-subitem[data-id='+id+']').fadeToggle(250);
    });    
    $('.btn-add').click(addItem);
    $('.btn-add2').click(addSubitem);
    $('.btn-edit').click(editButton);
    $('.btn-delete').click(deleteItem);
	$('.btn-restore').click(restoreItem);
    //
    $('.btn-import').click(importData);
    $('.btn-export').click(function(){
        
        var url = 'phpFunctions/fptRelatedExport.php'
        window.open( url, '_blank' );
    });
	$('.btn-exclusions').click(function(){
        
        var url = 'fptexclusions.php'
        window.open( url, '_blank' );
    });
});
