var state = false;
function alertLog( LogFlag, message ){
    if(LogFlag) {
        console.log( message );        
    } else {
        alert( message );
    }
}


function connectQZ( auto ){
	if(printer_label_global && printer_label_global != 'false' && printer_bol_global && printer_bol_global != 'false') {
            launchQZ( auto );            
        } else alertLog( auto, 'You must set the printer name first in "Printer settings".');
}
function connectRitsQZ( auto ){
	if(rits_printer != 'false') {
            launchQZ( auto );
        } else alertLog( auto, 'You must set the printer name first in "Printer settings".');
}
	
	function launchQZ( auto ) {
	if (!qz.websocket.isActive()) {
		window.location.assign("qz:launch");
		//Retry 1 times, pausing 1 second between each attempt
		startConnection( auto, { retries: 2, delay: 2 });
	}
}

function startConnection(auto, config) {
	if (!qz.websocket.isActive()) {
		updateState('Waiting');
		
		qz.security.setCertificatePromise(function(resolve, reject) {
			$.ajax("/shipping_module/qz/digital-certificate.txt?r=1").then(resolve, reject);
		});        
		qz.security.setSignaturePromise(function(toSign) {
		   return function(resolve, reject) {
			  $.ajax("/shipping_module/qz/sign.php?request=" + toSign).then(resolve, reject);
		   };
		});
		
		qz.websocket.connect(config).then(function() {
			updateState('Active');
	alertLog(auto, 'Connected');
			findVersion();
		}).catch(function(e) {if (state != 'Active') alertLog( auto, 'Can not connect. Make sure you installed QZ Tray and try again.'); });
	} else {
		alertLog(auto, 'An active connection already exists.');
	}
}
	
function printOn(printerName, pathToFile, type, format)
{
	//console.log(printerName);
	//console.log(pathToFile);
	if( !type ) {
		type = 'pdf';
	}
	if( type == 'html' ) {
		var data = [
			{ 
				type: type, 
				format: 'plain',
				data: pathToFile 
			}
		];
	} else {
		var data = [
			{ 
				type: type,
				data: pathToFile 
			}
		];
	}
	var config = qz.configs.create(printerName);
	qz.print(config, data).catch(function(e) { alert('Error. Can not print document.'); });
}

function printOnHtml(printerName, pathToFile)
{
	var config = qz.configs.create(printerName);
	var data = [
		{
			type: 'html',
			format: 'file',
			data: pathToFile
		   // options: { pageWidth: 4, pageHeight: 6 }
		}
	];
	qz.print(config, data).catch(function(e) { alert('Error. Can not print document.'); });
}
	
function printOnPdf(printerName, pathToFile, copies) {
    var config = qz.configs.create(printerName, {
        copies: copies
          //  ,orientation: 'landscape'
        ,size: {width: 4, height: 6.25}, units: 'in'
        ,colorType: 'grayscale'
        ,interpolation: "nearest-neighbor"
    });
    var data = [
        {
            type: 'pdf',
            data: pathToFile
        }
    ];
    qz.print(config, data).catch(function (e) {
        alert('Error. Can not print document.');
    });
}

function updateState(text) {
	console.log(text);
	state = text;
}