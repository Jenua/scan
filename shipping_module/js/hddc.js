'use strict';

var printer_bol_global = false;
var printer_label_global = false;
var printer_barcode_global = false;
var user_id = false;
var dealer_id = false;

function switchButton( opnID, prnID, path, printer ) {
	if( path ) {
		$(opnID).removeClass('disabled');
		$(opnID).attr('href', path + '?r='+Math.floor(Math.random() * 32768) );
		
		if(qz.websocket.isActive()) {
			$(prnID).removeClass('disabled');
			$(prnID).click(() => {
				console.log( printer+' '+path );
				printOn(printer, path, 'html');				
			});
		}
		return true;		
	} else {
		$(opnID).addClass('disabled');
		$(opnID).attr('href', '' );
		
		$(prnID).addClass('disabled');
		$(prnID).click(() => {
			return false;
		});		
		return false;
	}
}

function processCheckDocs( checkArray ) {
	Object.keys(checkArray.pallets).forEach((plt,i,a) => {
		let data = checkArray.pallets[plt];
		for( let doc in data ) {
			let opnID = "."+doc+"-open[data-btn-file='"+plt+"']";
			let prnID = "."+doc+"-print[data-btn-file='"+plt+"']";
			let printer = '';
			switch( doc ) {
				case 'flyers':
					printer = printer_label_global;
					break;
				case 'barcodes':
					printer = printer_barcode_global;
					break;
				case 'picking':
				case 'packing':
				case 'carton':
				default:
					printer = printer_bol_global;
			}
			switchButton( opnID, prnID, data[doc], printer );
		}		
	});
	
	Object.keys(checkArray.trucks).forEach((trk,i,a) => {
		let data = checkArray.trucks[trk];
		for( let doc in data ) {
			let opnID = "."+doc+"-open[data-btn-file='"+trk+"']";
			let prnID = "."+doc+"-print[data-btn-file='"+trk+"']";
			switchButton( opnID, prnID, data[doc], printer_bol_global );
		}
	});
	
	Object.keys(checkArray.products).forEach((prd,i,a) => {
		let data = checkArray.products[prd];
		let active = false;
		for( let doc in data ) {
			let opnID = "."+doc+"-open[data-btn-file='"+prd+"']";
			let prnID = "."+doc+"-print[data-btn-file='"+prd+"']";
			let printer = '';
			switch( doc ) {
				case 'flyers':
					printer = printer_label_global;
					break;
				case 'barcodes':
					printer = printer_barcode_global;
					break;
				case 'picking':
				case 'packing':
				case 'carton':
				default:
					printer = printer_bol_global;
			}			
			if( switchButton(opnID, prnID, data[doc], printer) ) {
				active = true;
			}
		}
		let grpID = ".icon-list-btn[data-file='"+prd+"']";
		if( active ) {			
			$(grpID).addClass('active');
		} else {
			$(grpID).removeClass('active');
		}
	});
}

function checkDocuments( PONumber, dealerId ) {
	$.post( "phpFunctions/functionsHomeDepotDCcheckDocs.php"
        , { po:PONumber, dealer:dealerId }
        , function( data ) {
			var result = $.parseJSON(data); 
			if( result ) {                
				processCheckDocs( result );
            }
        }
    )
}

function generateAllDocuments( PONumber, dealerId ) {
    var btnId = event.target.id;
    $('#'+btnId).addClass('blue-loader');
    $.post( "phpFunctions/functionsHomeDepotDCpallets.php"
        , { po:PONumber, dealerId:dealerId }
        , function( /*data*/ ) {
            $('#'+btnId).removeClass( 'blue-loader' );
            checkDocuments(PONumber, dealerId);
        }        
    );	
}

function generateSingleDocument( PONumber, dealerId, GroupSKU ) {
    var btnId = event.target.id;
    $('#'+btnId).addClass('blue-loader');
    $.post( "phpFunctions/functionsHomeDepotDCpallets.php"
        , { po:PONumber, dealerId:dealerId, gsku:GroupSKU }
        , function( /*data*/ ) {
            $('#'+btnId).removeClass( 'blue-loader' );
            checkDocuments(PONumber, dealerId);
        }        
    );
}

$(document).ready(function () {
    $('.clickable').click(function(e){
        var el = e.target.className,
            popup = $('.product-table').find('.printButtonsPopup');
        
		if(el.indexOf('icon-list-btn') < 0 && !popup.is(":visible")){
            $(this).toggleClass('active-row');
            $(this).next('.sub-items').toggle();
        } else if (el.indexOf('icon-list-btn') >=0 && !popup.is(":visible")){
            $(this).find('.printButtonsPopup').fadeIn(300);
        } else if (el.indexOf('icon-list-btn') >=0){
			$('.printButtonsPopup').hide();
			$(this).find('.printButtonsPopup').fadeIn(400);
        }
    });

    $('.done-check').click(function(e){
        e.stopPropagation();       
        var txn   = $(this).attr('data-txn');
        var po    = $('#genallbtn').attr('data-ponumber');
        var check = $(this).is(':checked') ? 1 : 0;

        $.post( "phpFunctions/functionsHomeDepotDCDoneCheck.php"
            , { po:po, txnlineid:txn, check:check }
            , function( data ) {
                if( data == '1' ) {
                    if( check ) {
                        $("tr.txn-line[data-txn='"+txn+"']").addClass('tr_done');
                    } else {
                        $("tr.txn-line[data-txn='"+txn+"']").removeClass('tr_done');
                    }
                } else alert( data );
            }
        );
    });    
    $('.disc-check').click(function(e){
        e.stopPropagation();       
        var txn   = $(this).attr('data-txn');
        var po    = $('#genallbtn').attr('data-ponumber');
        var check = $(this).is(':checked') ? 1 : 0;

        $.post( "phpFunctions/functionsHomeDepotDCDoneCheck.php"
            , { po:po, txnlineid:txn, check:check, type:'disc' }
            , function( data ) {
                if( data == '1' ) {
                    if( check ) {
                        $("tr.txn-line[data-txn='"+txn+"']").addClass('tr_disc');
                    } else {
                        $("tr.txn-line[data-txn='"+txn+"']").removeClass('tr_disc');
                    }
                } else alert( data );
            }
        );
    });
    if( user_id ) {
        $.ajax({
            url: "phpFunctions/getPrinters.php",
            type: 'POST',
            data: {
                    id: user_id
            },
            complete: function(data){
                var res = JSON.parse(data.responseText);
                printer_bol_global     = res[0]['printer_bol'];
                printer_label_global   = res[0]['printer_hddc1'];
                printer_barcode_global = res[0]['printer_hddc2'];
                connectQZ(true);
                
                var PONumber = $('#genallbtn').attr('data-ponumber');
                if( PONumber ) {
                    checkDocuments( PONumber, dealer_id ); // dealer_id defined in template 
                }                
            }
        });
    }
    var PONumber = $('#genallbtn').attr('data-ponumber');
    if( PONumber ) {
        checkDocuments( PONumber, dealer_id ); // dealer_id defined in template
    }
});

$(document).on('click', (function (e){
	var div = $(".printButtonsPopup"),
        icon = $(".icon-list-btn");
    if (!icon.is(e.target) && (!div.is(e.target) && div.has(e.target).length === 0)) {
        div.hide();
    }
}));


