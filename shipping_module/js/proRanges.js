function updateProRangeCEVA()
{
	var dialog = $('#dialog').dialog({
        autoOpen: false,
        height: 'auto',
        width: 'auto',
        resizable: false,
        autoResize: true,
        modal: true,
		title: 'Update PRO# Range for CEVA',
        overlay: { backgroundColor: "#000", opacity: 0.5 },
    });
	dialog.dialog('close');
	
	$.ajax({
		url: "phpFunctions/proRanges/get_ceva_range.php",
		type: 'POST',
		complete: function(data){
			console.log(data.responseText);
			var range = JSON.parse(data.responseText);;
			$('#content').html( "<p>Current range is: "+range+"</p><p>Please enter new range</p>"
								+ "<table cellspacing=0 cellpadding=0>"
								+	"<tr style='color: grey'>"
								+		"<td align='center'>MIN</td>"
								+		"<td align='center'>&nbsp;-&nbsp;</td>"
								+		"<td align='center'>MAX</td>"
								+	"</tr>"
								+	"<tr>"
								+		"<td><input type='text' id='range_min_ceva' size=\"8\" data-inputmask=\"'mask': '99999999'\"></td>"
								+		"<td>&nbsp;-&nbsp;</td>"
								+		"<td><input type='text' id='range_max_ceva' size=\"8\" data-inputmask=\"'mask': '99999999'\"></td>"
								+	"</tr>"
								+ "</table>"
								+ "<br><br><button style='cursor: pointer;' id='save_range_ceva'>Save</button>");
			dialog.dialog('open');
			$("#content :input").inputmask();

			$('#save_range_ceva').click(function() {
				var min = $('#range_min_ceva').val();
				var max = $('#range_max_ceva').val();
				min = $.trim(min);
				max = $.trim(max);
				if (max > min && $.isNumeric( max ) && $.isNumeric( min ))
				{
					$.ajax({
						url: "phpFunctions/proRanges/set_ceva_range.php",
						type: 'POST',
						data: {
								min: min,
								max: max
							},
						complete: function(data){
							alert(data.responseText);
							dialog.dialog('close');
						},
						error: function (request, status, error) {
							console.log("Ajax request error: "+request.responseText);
							dialog.dialog('close');
						}
					});
				} else alert('MAX should be more than MIN and both should be numeric!');
			});
		},
		error: function (request, status, error) {
			console.log("Ajax request error: "+request.responseText);
			dialog.dialog('close');
		}
	});
}

function updateProRangeODFL()
{
	var dialog = $('#dialog').dialog({
        autoOpen: false,
        height: 'auto',
        width: 'auto',
        resizable: false,
        autoResize: true,
        modal: true,
		title: 'Update PRO# Range for ODFL',
        overlay: { backgroundColor: "#000", opacity: 0.5 },
    });
	dialog.dialog('close');
	
	$.ajax({
		url: "phpFunctions/proRanges/get_odfl_range.php",
		type: 'POST',
		complete: function(data){
			console.log(data.responseText);
			var range = JSON.parse(data.responseText);;
			$('#content').html( "<p>Current range is: "+range+"</p><p>Please enter new range</p>"
								+ "<table cellspacing=0 cellpadding=0>"
								+	"<tr style='color: grey'>"
								+		"<td align='center'>MIN</td>"
								+		"<td align='center'>&nbsp;-&nbsp;</td>"
								+		"<td align='center'>MAX</td>"
								+	"</tr>"
								+	"<tr>"
								+		"<td><input type='text' id='range_min_odfl' size=\"11\" data-inputmask=\"'mask': '99999999999'\"></td>"
								+		"<td>&nbsp;-&nbsp;</td>"
								+		"<td><input type='text' id='range_max_odfl' size=\"11\" data-inputmask=\"'mask': '99999999999'\"></td>"
								+	"</tr>"
								+ "</table>"
								+ "<br><br><button style='cursor: pointer;' id='save_range_odfl'>Save</button>");
			dialog.dialog('open');
			$("#content :input").inputmask();

			$('#save_range_odfl').click(function() {
				var min = $('#range_min_odfl').val();
				var max = $('#range_max_odfl').val();
				min = $.trim(min);
				max = $.trim(max);
				if (max > min && $.isNumeric( max ) && $.isNumeric( min ))
				{
					$.ajax({
						url: "phpFunctions/proRanges/set_odfl_range.php",
						type: 'POST',
						data: {
								min: min,
								max: max
							},
						complete: function(data){
							alert(data.responseText);
							dialog.dialog('close');
						},
						error: function (request, status, error) {
							console.log("Ajax request error: "+request.responseText);
							dialog.dialog('close');
						}
					});
				} else alert('MAX should be more than MIN and both should be numeric!');
			});
		},
		error: function (request, status, error) {
			console.log("Ajax request error: "+request.responseText);
			dialog.dialog('close');
		}
	});
}

function updateProRangeYRC()
{
	var dialog = $('#dialog').dialog({
        autoOpen: false,
        height: 'auto',
        width: 'auto',
        resizable: false,
        autoResize: true,
        modal: true,
		title: 'Update PRO# Range for YRC',
        overlay: { backgroundColor: "#000", opacity: 0.5 },
    });
	dialog.dialog('close');
	
	$.ajax({
		url: "phpFunctions/proRanges/get_yrc_range.php",
		type: 'POST',
		complete: function(data){
			console.log(data.responseText);
			var range = JSON.parse(data.responseText);;
			$('#content').html( "<p>Current range is: "+range+"</p><p>Please enter new range</p>"
								+ "<table cellspacing=0 cellpadding=0>"
								+	"<tr style='color: grey'>"
								+		"<td align='center'>Prefix</td>"
								+		"<td align='center'>-</td>"
								+		"<td align='center'>MIN</td>"
								+		"<td align='center'>&nbsp;-&nbsp;</td>"
								+		"<td align='center'>&nbsp;</td>"
								+		"<td align='center'>MAX</td>"
								+	"</tr>"
								+	"<tr>"
								+		"<td align='right'><input type='text' id='range_bh_yrc' size=\"3\" data-inputmask=\"'mask': '999'\" onchange=\"$('#range_bh_copy_yrc').html( $(this).val() )\"> </td>"
								+		"<td>-</td>"
								+		"<td><input type='text' id='range_min_yrc' size=\"6\" data-inputmask=\"'mask': '999999'\"></td>"
								+		"<td>&nbsp;-&nbsp;</td>"
								+		"<td><font id='range_bh_copy_yrc'>___</font>-</td>"
								+		"<td><input type='text' id='range_max_yrc' size=\"6\" data-inputmask=\"'mask': '999999'\"></td>"
								+	"</tr>"
								+ "</table>"
								+ "<br><br><button style='cursor: pointer;' id='save_range_yrc'>Save</button>");
			dialog.dialog('open');
			$("#content :input").inputmask();

			$('#save_range_yrc').click(function() {
				var bh = $('#range_bh_yrc').val();
				var min = $('#range_min_yrc').val();
				var max = $('#range_max_yrc').val();
				bh = $.trim(bh);
				min = $.trim(min);
				max = $.trim(max);
				if (max > min && $.isNumeric( max ) && $.isNumeric( min ) && $.isNumeric( bh ))
				{
					$.ajax({
						url: "phpFunctions/proRanges/set_yrc_range.php",
						type: 'POST',
						data: {
								bh: bh,
								min: min,
								max: max
							},
						complete: function(data){
							alert(data.responseText);
							dialog.dialog('close');
						},
						error: function (request, status, error) {
							console.log("Ajax request error: "+request.responseText);
							dialog.dialog('close');
						}
					});
				} else alert('MAX should be more than MIN and both should be numeric!');
			});
		},
		error: function (request, status, error) {
			console.log("Ajax request error: "+request.responseText);
			dialog.dialog('close');
		}
	});
}

function updateProRangeNEWENGL()
{
	var dialog = $('#dialog').dialog({
        autoOpen: false,
        height: 'auto',
        width: 'auto',
        resizable: false,
        autoResize: true,
        modal: true,
		title: 'Update PRO# Range for NEW ENGL',
        overlay: { backgroundColor: "#000", opacity: 0.5 },
    });
	dialog.dialog('close');
	
	$.ajax({
		url: "phpFunctions/proRanges/get_newengl_range.php",
		type: 'POST',
		complete: function(data){
			console.log(data.responseText);
			var range = JSON.parse(data.responseText);;
			$('#content').html( "<p>Current range is: "+range+"</p><p>Please enter new range</p>"
								+ "<table cellspacing=0 cellpadding=0>"
								+	"<tr style='color: grey'>"
								+		"<td align='center'>MIN</td>"
								+		"<td align='center'>&nbsp;-&nbsp;</td>"
								+		"<td align='center'>MAX</td>"
								+	"</tr>"
								+	"<tr>"
								+		"<td><input type='text' id='range_min_newengl' size=\"8\" data-inputmask=\"'mask': '99999999'\"></td>"
								+		"<td>&nbsp;-&nbsp;</td>"
								+		"<td><input type='text' id='range_max_newengl' size=\"8\" data-inputmask=\"'mask': '99999999'\"></td>"
								+	"</tr>"
								+ "</table>"
								+ "<br><br><button style='cursor: pointer;' id='save_range_newengl'>Save</button>");
			dialog.dialog('open');
			$("#content :input").inputmask();

			$('#save_range_newengl').click(function() {
				var min = $('#range_min_newengl').val();
				var max = $('#range_max_newengl').val();
				min = $.trim(min);
				max = $.trim(max);
				if (max > min && $.isNumeric( max ) && $.isNumeric( min ))
				{
					$.ajax({
						url: "phpFunctions/proRanges/set_newengl_range.php",
						type: 'POST',
						data: {
								min: min,
								max: max
							},
						complete: function(data){
							alert(data.responseText);
							dialog.dialog('close');
						},
						error: function (request, status, error) {
							console.log("Ajax request error: "+request.responseText);
							dialog.dialog('close');
						}
					});
				} else alert('MAX should be more than MIN and both should be numeric!');
			});
		},
		error: function (request, status, error) {
			console.log("Ajax request error: "+request.responseText);
			dialog.dialog('close');
		}
	});
}

function updateProRangeESTES()
{
	var dialog = $('#dialog').dialog({
        autoOpen: false,
        height: 'auto',
        width: 'auto',
        resizable: false,
        autoResize: true,
        modal: true,
		title: 'Update PRO# Range for ESTES',
        overlay: { backgroundColor: "#000", opacity: 0.5 },
    });
	dialog.dialog('close');
	
	$.ajax({
		url: "phpFunctions/proRanges/get_estes_range.php",
		type: 'POST',
		complete: function(data){
			console.log(data.responseText);
			var range = JSON.parse(data.responseText);;
			$('#content').html( "<p>Current range is: "+range+"</p><p>Please enter new range</p>"
								+ "<table cellspacing=0 cellpadding=0>"
								+	"<tr style='color: grey'>"
								+		"<td align='center'>First digit</td>"
								+		"<td align='center'>MIN</td>"
								+		"<td align='center'>&nbsp;-&nbsp;</td>"
								+		"<td align='center'>&nbsp;</td>"
								+		"<td align='center'>MAX</td>"
								+	"</tr>"
								+	"<tr>"
								+		"<td align='right'><input type='text' id='range_fd_estes' size=\"1\" data-inputmask=\"'mask': '9'\" onchange=\"$('#range_fd_copy_estes').html( $(this).val() )\"> </td>"
								+		"<td><input type='text' id='range_min_estes' size=\"9\" data-inputmask=\"'mask': '999999999'\"></td>"
								+		"<td>&nbsp;-&nbsp;</td>"
								+		"<td><font id='range_fd_copy_estes'>_</font> </td>"
								+		"<td><input type='text' id='range_max_estes' size=\"9\" data-inputmask=\"'mask': '999999999'\"></td>"
								+	"</tr>"
								+ "</table>"
								+ "<br><br><button style='cursor: pointer;' id='save_range_estes'>Save</button>");
			dialog.dialog('open');
			$("#content :input").inputmask();

			$('#save_range_estes').click(function() {
				var min = $('#range_min_estes').val();
				var max = $('#range_max_estes').val();
				var fd = $('#range_fd_estes').val();
				min = $.trim(min);
				max = $.trim(max);
				fd = $.trim(fd);
				if (max > min && $.isNumeric( max ) && $.isNumeric( min ) && $.isNumeric( fd ))
				{
					$.ajax({
						url: "phpFunctions/proRanges/set_estes_range.php",
						type: 'POST',
						data: {
								min: min,
								max: max,
								fd: fd
							},
						complete: function(data){
							alert(data.responseText);
							dialog.dialog('close');
						},
						error: function (request, status, error) {
							console.log("Ajax request error: "+request.responseText);
							dialog.dialog('close');
						}
					});
				} else alert('MAX should be more than MIN and both should be numeric!');
			});
		},
		error: function (request, status, error) {
			console.log("Ajax request error: "+request.responseText);
			dialog.dialog('close');
		}
	});
}

function updateProRangeRL()
{
	var dialog = $('#dialog').dialog({
        autoOpen: false,
        height: 'auto',
        width: 'auto',
        resizable: false,
        autoResize: true,
        modal: true,
		title: 'Update PRO# Range for RL',
        overlay: { backgroundColor: "#000", opacity: 0.5 },
    });
	dialog.dialog('close');
	
	$.ajax({
		url: "phpFunctions/proRanges/get_rl_range.php",
		type: 'POST',
		complete: function(data){
			console.log(data.responseText);
			var range = JSON.parse(data.responseText);;
			$('#content').html( "<p>Current range is: "+range+"</p><p>Please enter new range</p>"
								+ "<table cellspacing=0 cellpadding=0>"
								+	"<tr style='color: grey'>"
								+		"<td align='center'>MIN</td>"
								+		"<td align='center'>&nbsp;-&nbsp;</td>"
								+		"<td align='center'>MAX</td>"
								+	"</tr>"
								+	"<tr>"
								+		"<td><input type='text' id='range_min_rl' size=\"8\" data-inputmask=\"'mask': '99999999'\"></td>"
								+		"<td>&nbsp;-&nbsp;</td>"
								+		"<td><input type='text' id='range_max_rl' size=\"8\" data-inputmask=\"'mask': '99999999'\"></td>"
								+	"</tr>"
								+ "</table>"
								+ "<br><br><button style='cursor: pointer;' id='save_range_rl'>Save</button>");
			dialog.dialog('open');
			$("#content :input").inputmask();

			$('#save_range_rl').click(function() {
				var min = $('#range_min_rl').val();
				var max = $('#range_max_rl').val();
				min = $.trim(min);
				max = $.trim(max);
				if (max > min && $.isNumeric( max ) && $.isNumeric( min ))
				{
					$.ajax({
						url: "phpFunctions/proRanges/set_rl_range.php",
						type: 'POST',
						data: {
								min: min,
								max: max
							},
						complete: function(data){
							alert(data.responseText);
							dialog.dialog('close');
						},
						error: function (request, status, error) {
							console.log("Ajax request error: "+request.responseText);
							dialog.dialog('close');
						}
					});
				} else alert('MAX should be more than MIN and both should be numeric!');
			});
		},
		error: function (request, status, error) {
			console.log("Ajax request error: "+request.responseText);
			dialog.dialog('close');
		}
	});
}

function updateProRangeUPS()
{
	var dialog = $('#dialog').dialog({
        autoOpen: false,
        height: 'auto',
        width: 'auto',
        resizable: false,
        autoResize: true,
        modal: true,
		title: 'Update PRO# Range for UPS',
        overlay: { backgroundColor: "#000", opacity: 0.5 },
    });
	dialog.dialog('close');
	
	$.ajax({
		url: "phpFunctions/proRanges/get_ups_range.php",
		type: 'POST',
		complete: function(data){
			console.log(data.responseText);
			var range = JSON.parse(data.responseText);;
			$('#content').html( "<p>Current range is: "+range+"</p><p>Please enter new range</p>"
								+ "<table cellspacing=0 cellpadding=0>"
								+	"<tr style='color: grey'>"
								+		"<td align='center'>MIN</td>"
								+		"<td align='center'>&nbsp;-&nbsp;</td>"
								+		"<td align='center'>MAX</td>"
								+	"</tr>"
								+	"<tr>"
								+		"<td><input type='text' id='range_min_ups' size=\"8\" data-inputmask=\"'mask': '99999999'\"></td>"
								+		"<td>&nbsp;-&nbsp;</td>"
								+		"<td><input type='text' id='range_max_ups' size=\"8\" data-inputmask=\"'mask': '99999999'\"></td>"
								+	"</tr>"
								+ "</table>"
								+ "<br><br><button style='cursor: pointer;' id='save_range_ups'>Save</button>");
			dialog.dialog('open');
			$("#content :input").inputmask();

			$('#save_range_ups').click(function() {
				var min = $('#range_min_ups').val();
				var max = $('#range_max_ups').val();
				min = $.trim(min);
				max = $.trim(max);
				if (max > min && $.isNumeric( max ) && $.isNumeric( min ))
				{
					$.ajax({
						url: "phpFunctions/proRanges/set_ups_range.php",
						type: 'POST',
						data: {
								min: min,
								max: max
							},
						complete: function(data){
							alert(data.responseText);
							dialog.dialog('close');
						},
						error: function (request, status, error) {
							console.log("Ajax request error: "+request.responseText);
							dialog.dialog('close');
						}
					});
				} else alert('MAX should be more than MIN and both should be numeric!');
			});
		},
		error: function (request, status, error) {
			console.log("Ajax request error: "+request.responseText);
			dialog.dialog('close');
		}
	});
}

function updateProRangeFEDEX()
{
	var dialog = $('#dialog').dialog({
        autoOpen: false,
        height: 'auto',
        width: 'auto',
        resizable: false,
        autoResize: true,
        modal: true,
		title: 'Update PRO# Range for FEDEX',
        overlay: { backgroundColor: "#000", opacity: 0.5 },
    });
	dialog.dialog('close');
	
	$.ajax({
		url: "phpFunctions/proRanges/get_fedex_range.php",
		type: 'POST',
		complete: function(data){
			console.log(data.responseText);
			var range = JSON.parse(data.responseText);;
			$('#content').html( "<p>Current range is: "+range+"</p><p>Please enter new range</p>"
								+ "<table cellspacing=0 cellpadding=0>"
								+	"<tr style='color: grey'>"
								+		"<td align='center'>MIN</td>"
								+		"<td align='center'>&nbsp;-&nbsp;</td>"
								+		"<td align='center'>MAX</td>"
								+	"</tr>"
								+	"<tr>"
								+		"<td><input type='text' id='range_min_fedex' size=\"9\" data-inputmask=\"'mask': '999999999'\"></td>"
								+		"<td>&nbsp;-&nbsp;</td>"
								+		"<td><input type='text' id='range_max_fedex' size=\"9\" data-inputmask=\"'mask': '999999999'\"></td>"
								+	"</tr>"
								+ "</table>"
								+ "<br><br><button style='cursor: pointer;' id='save_range_fedex'>Save</button>");
			dialog.dialog('open');
			$("#content :input").inputmask();

			$('#save_range_fedex').click(function() {
				var min = $('#range_min_fedex').val();
				var max = $('#range_max_fedex').val();
				min = $.trim(min);
				max = $.trim(max);
				if (max > min && $.isNumeric( max ) && $.isNumeric( min ))
				{
					$.ajax({
						url: "phpFunctions/proRanges/set_fedex_range.php",
						type: 'POST',
						data: {
								min: min,
								max: max
							},
						complete: function(data){
							alert(data.responseText);
							dialog.dialog('close');
						},
						error: function (request, status, error) {
							console.log("Ajax request error: "+request.responseText);
							dialog.dialog('close');
						}
					});
				} else alert('MAX should be more than MIN and both should be numeric!');
			});
		},
		error: function (request, status, error) {
			console.log("Ajax request error: "+request.responseText);
			dialog.dialog('close');
		}
	});
}

function updateProRangePYLE()
{
	var dialog = $('#dialog').dialog({
		autoOpen: false,
		height: 'auto',
		width: 'auto',
		resizable: false,
		autoResize: true,
		modal: true,
		title: 'Update PRO# Range for PYLE',
		overlay: { backgroundColor: "#000", opacity: 0.5 },
	});
	dialog.dialog('close');
	
	$.ajax({
		url: "phpFunctions/proRanges/get_pyle_range.php",
		type: 'POST',
		complete: function(data){
			console.log(data.responseText);
			var range = JSON.parse(data.responseText);;
			$('#content').html( "<p>Current range is: "+range+"</p><p>Please enter new range</p>"
								+ "<table cellspacing=0 cellpadding=0>"
								+	"<tr style='color: grey'>"
								+		"<td align='center'>MIN</td>"
								+		"<td align='center'>&nbsp;-&nbsp;</td>"
								+		"<td align='center'>MAX</td>"
								+	"</tr>"
								+	"<tr>"
								+		"<td><input type='text' id='range_min_fedex' size=\"8\" data-inputmask=\"'mask': '99999999'\"></td>"
								+		"<td>&nbsp;-&nbsp;</td>"
								+		"<td><input type='text' id='range_max_fedex' size=\"8\" data-inputmask=\"'mask': '99999999'\"></td>"
								+	"</tr>"
								+ "</table>"
								+ "<br><br><button style='cursor: pointer;' id='save_range_fedex'>Save</button>");
			dialog.dialog('open');
			$("#content :input").inputmask();

			$('#save_range_fedex').click(function() {
				var min = $('#range_min_fedex').val();
				var max = $('#range_max_fedex').val();
				min = $.trim(min);
				max = $.trim(max);
				if (max > min && $.isNumeric( max ) && $.isNumeric( min ))
				{
					$.ajax({
						url: "phpFunctions/proRanges/set_pyle_range.php",
						type: 'POST',
						data: {
								min: min,
								max: max
							},
						complete: function(data){
							alert(data.responseText);
							dialog.dialog('close');
						},
						error: function (request, status, error) {
							console.log("Ajax request error: "+request.responseText);
							dialog.dialog('close');
						}
					});
				} else alert('MAX should be more than MIN and both should be numeric!');
			});
		},
		error: function (request, status, error) {
			console.log("Ajax request error: "+request.responseText);
			dialog.dialog('close');
		}
	});
}


function updateProRangeABF()
{
	var dialog = $('#dialog').dialog({
        autoOpen: false,
        height: 'auto',
        width: 'auto',
        resizable: false,
        autoResize: true,
        modal: true,
		title: 'Update PRO# Range for ABF',
        overlay: { backgroundColor: "#000", opacity: 0.5 },
    });
	dialog.dialog('close');
	
	$.ajax({
		url: "phpFunctions/proRanges/get_abf_range.php",
		type: 'POST',
		complete: function(data){
			console.log(data.responseText);
			var range = JSON.parse(data.responseText);;
			$('#content').html( "<p>Current range is: "+range+"</p><p>Please enter new range</p>"
								+ "<table cellspacing=0 cellpadding=0>"
								+	"<tr style='color: grey'>"
								+		"<td align='center'>First digit</td>"
								+		"<td align='center'>MIN</td>"
								+		"<td align='center'>&nbsp;-&nbsp;</td>"
								+		"<td align='center'>&nbsp;</td>"
								+		"<td align='center'>MAX</td>"
								+	"</tr>"
								+	"<tr>"
								+		"<td align='right'><input type='text' id='range_fd_abf' size=\"1\" data-inputmask=\"'mask': '9'\" onchange=\"$('#range_fd_copy_abf').html( $(this).val() )\"> </td>"
								+		"<td><input type='text' id='range_min_abf' size=\"8\" data-inputmask=\"'mask': '99999999'\"></td>"
								+		"<td>&nbsp;-&nbsp;</td>"
								+		"<td><font id='range_fd_copy_abf'>_</font> </td>"
								+		"<td><input type='text' id='range_max_abf' size=\"8\" data-inputmask=\"'mask': '99999999'\"></td>"
								+	"</tr>"
								+ "</table>"
								+ "<br><br><button style='cursor: pointer;' id='save_range_abf'>Save</button>");
			dialog.dialog('open');
			$("#content :input").inputmask();

			$('#save_range_abf').click(function() {
				var min = $('#range_min_abf').val();
				var max = $('#range_max_abf').val();
				var fd = $('#range_fd_abf').val();
				min = $.trim(min);
				max = $.trim(max);
				fd = $.trim(fd);
				if (max > min && $.isNumeric( max ) && $.isNumeric( min ) && $.isNumeric( fd ))
				{
					$.ajax({
						url: "phpFunctions/proRanges/set_abf_range.php",
						type: 'POST',
						data: {
								min: min,
								max: max,
								fd: fd
							},
						complete: function(data){
							alert(data.responseText);
							dialog.dialog('close');
						},
						error: function (request, status, error) {
							console.log("Ajax request error: "+request.responseText);
							dialog.dialog('close');
						}
					});
				} else alert('MAX should be more than MIN and both should be numeric!');
			});
		},
		error: function (request, status, error) {
			console.log("Ajax request error: "+request.responseText);
			dialog.dialog('close');
		}
	});
}