console.log('log started');
$(document).ready(function() {
	$(function() {
		$("#PO").autocomplete({
			source: "phpFunctions/autocomplete/po.php",
			minLength: 1
		});
		$("#Customer").autocomplete({
			source: "phpFunctions/autocomplete/customer.php",
			minLength: 1
		});
		$("#Phone").autocomplete({
			source: "phpFunctions/autocomplete/phone.php",
			minLength: 1
		});
		$("#Address").autocomplete({
			source: "phpFunctions/autocomplete/address.php",
			minLength: 1
		});
		$("#Zip").autocomplete({
			source: "phpFunctions/autocomplete/zip.php",
			minLength: 1
		});
		$("#Dealer").autocomplete({
			source: "phpFunctions/autocomplete/dealer.php",
			minLength: 1
		});
		$("#Carrier").autocomplete({
			source: "phpFunctions/autocomplete/carrier.php",
			minLength: 1
		});
		$("#Tracking").autocomplete({
			source: "phpFunctions/autocomplete/tracking.php",
			minLength: 1
		});
		$("#qb_invoice").autocomplete({
			source: "phpFunctions/autocomplete/qb_invoice.php",
			minLength: 1
		});
		$("#sm_invoice").autocomplete({
			source: "phpFunctions/autocomplete/sm_invoice.php",
			minLength: 1
		});
		$("#Memo").autocomplete({
			source: "phpFunctions/autocomplete/memo.php",
			minLength: 1
		});
	});
	
	var dialog = $('#dialog').dialog({
        autoOpen: false,
        height: 'auto',
        width: 'auto',
        resizable: false,
        autoResize: true,
        modal: true,
        overlay: { backgroundColor: "#000", opacity: 0.5 },
    });
	dialog.dialog('close');
	$('#searchForm').on('submit', function (e) {
		event.preventDefault();
		var form = $('#searchForm');
		var url = $(form).attr("action");
		var formData = {};
		formData = $(form).serialize();
		$.ajax({
			url: url,
			type: 'POST',
			data: formData,
			complete: function(data){
				console.log(data.responseText);
				responses = JSON.parse(data.responseText);
				if (responses)
				{
					var table = "<table data-toggle='table' class='table table-bordered display table-striped'><thead><tr class='info'><th>PO</th><th>Customer</th><th>Phone</th><th>Address</th><th>Zip</th><th>Dealer</th><th>Carrier</th><th>PRO#</th><th>Tracking#</th><th>QB Invoice#</th><th>SM Invoice#</th><th>Memo</th><th>Creation Time</th></tr></thead><tbody>";
					for (i = 0; i<responses.length; i++)
					{
						if(!responses[i]['PONumber']) responses[i]['PONumber'] = 'N/A';
						if(!responses[i]['ShipAddress_Addr1']) responses[i]['ShipAddress_Addr1'] = 'N/A';
						if(!responses[i]['FOB']) responses[i]['FOB'] = 'N/A';
						if(!responses[i]['ShipAddress_Addr2']) responses[i]['ShipAddress_Addr2'] = 'N/A';
						if(!responses[i]['ShipAddress_PostalCode']) responses[i]['ShipAddress_PostalCode'] = 'N/A';
						if(!responses[i]['CustomerRef_FullName']) responses[i]['CustomerRef_FullName'] = 'N/A';
						if(!responses[i]['CARRIERNAME']) responses[i]['CARRIERNAME'] = 'N/A';
						if(!responses[i]['PRO#']) responses[i]['PRO#'] = 'N/A';
						if(!responses[i]['Tracking#']) responses[i]['Tracking#'] = 'N/A';
						if(!responses[i]['RefNumber']) responses[i]['RefNumber'] = 'N/A';
						if(!responses[i]['TimeCreated']) responses[i]['TimeCreated'] = 'N/A';
						table+="<tr>";
						if(responses[i]['PONumber'] != 'N/A') table+="<td><a target='_blank' href='/shipping_module/order_status_history.php?po="+responses[i]['PONumber']+(responses[i]['RefNumber']=='N/A'?'':('&ref='+responses[i]['RefNumber']))+"'>"+responses[i]['PONumber']+"</a></td>"; else table+="<td>N/A</td>";
						table+="<td>"+responses[i]['ShipAddress_Addr1']+"</td><td>"+responses[i]['FOB']+"</td><td>"+responses[i]['ShipAddress_Addr2']+"</td><td>"+responses[i]['ShipAddress_PostalCode']+"</td><td>"+responses[i]['CustomerRef_FullName']+"</td><td>"+responses[i]['CARRIERNAME']+"</td><td>"+responses[i]['PRO#']+"</td><td>"+responses[i]['Tracking#']+"</td><td>"+responses[i]['QB Invoice#']+"</td><td>"+responses[i]['SM Invoice#']+"</td><td>"+responses[i]['Memo']+"</td><td>"+responses[i]['TimeCreated']+"</td></tr>";
					}
					table+= "</tbody></table>";
					$('#content').html(table);
					dialog.dialog('open');
				} else
				{
					$('#content').html('No results');
					dialog.dialog('open');
				}
			},
			error: function (request, status, error) {
				console.log("Ajax request error: "+request.responseText);
			}
		});
	});
});