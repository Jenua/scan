console.log('log started');

var printer_bol_global = false;
var printer_label_global = false;

var labelOk;
var bolOk;

var poTempVal;
var idTempVal;
var objTempVal;
var fedexModeTempVal;
var carrierTempVal;

var liftTempVal;
var resiTempVal;
var delivery_notificationTempVal;

var obj_remove_order;

$(document).ready(function () {
	$('#RESIDENTIAL').click(function () {
		if (!$(this).is(":checked")) {
			$('#delivery_notification').prop('checked', false);
			$('#LIFTGATE').prop('checked', false);
		} else
		{
			$('#delivery_notification').prop('checked', true);

			if (liftTempVal == '1')
			{
				$('#LIFTGATE').prop('checked', true);
			} else
			{
				$('#LIFTGATE').prop('checked', false);
			}
		}
	});


	$('#LIFTGATE').click(function () {
		if ($('#LIFTGATE').is(":checked")) {
			if (!$('#RESIDENTIAL').is(":checked"))
			{
				var box1 = confirm("Are you sure? LIFTGATE is only applicable for RESIDENTIAL.");
				if (box1 != true)
				{
					$('#LIFTGATE').prop('checked', false);
				}
			}
		}
	});

	$('#delivery_notification').click(function () {
		if ($('#delivery_notification').is(":checked")) {
			if (!$('#RESIDENTIAL').is(":checked"))
			{
				var box2 = confirm("Are you sure? DELIVERY NOTIFICATION is only applicable for RESIDENTIAL.");
				if (box2 != true)
				{
					$('#delivery_notification').prop('checked', false);
				}
			}
		}
	});
	updateSplit();
});

function updateSplit()
{
	console.log('updating splitted...');
	$.ajax({
		url: 'phpFunctions/getProdNumber.php',
		type: 'POST',
		complete: function (data) {
			//console.log(data.responseText);
			responses = JSON.parse(data.responseText);
			for (i = 0; i < responses.length; i++)
			{
				buttonId = responses[i]['id'];
				buttonText = responses[i]['text1'] + ' of ' + responses[i]['text2'];
				idSPLIT = '#SPLIT' + buttonId;
				n = idSPLIT.lastIndexOf(".");
				if (n != -1)
				{
					idSPLIT = idSPLIT.substring(0, n) + "\\" + idSPLIT.substring(n);
				}
				$(idSPLIT).text(buttonText);
			}
			console.log('splitted updated');
		},
		error: function (request, status, error) {
			console.log("Ajax request error: " + request.responseText);
		}
	});
}

function refreshThepage()
{
	window.location.href = 'index.php';
}

function removeCancel()
{
	$('#removeBackground').fadeOut(100);
	$('#removePO').val('');
	$("#reasonNote").val('');
}

function statusCancel()
{
	$('#statusBackground').fadeOut(100);
	$('#statusPO').val('');
}

function reprintCancel()
{
	$('#reprintBackground').fadeOut(100);
	$('#reprintPO').val('');
	$("#reasonNotereprint").val('');
}

function goReprint()
{
	$('#reprintBackground').fadeIn(100);
	setFocusToId('#reprintPO');
	/*var manualPoEntry = $('#manualPoEntry').val();
	 $('#reprintPO').val(manualPoEntry);*/
}

function reprintOrder(userName)
{

	var po = $('#reprintPO').val();
	var reason = $("#reasonSelectreprint option:selected").val();
	var reasonNote = $("#reasonNotereprint").val();

	$.ajax({
		url: 'phpFunctions/functionsFindPDF.php',
		type: 'POST',
		data: {
			po: po
		},
		beforeSend: function () {
			loadingBill();
			loading();
		},
		complete: function (data) {
			console.log(data.responseText);
			productCodeText = data.responseText;
			productCode = JSON.parse(productCodeText);
			if (Object.prototype.toString.call(productCode) === "[object Object]") {

				var path = productCode.path;
				var path2 = productCode.path2;
				var path3 = productCode.pathFlyer;

				console.log('Label to reprint: ' + path);
				console.log('BOL to reprint: ' + path2);
				console.log('Flyer to reprint: ' + path3);

				generateSuccessButtonLabel(path);
				generateSuccessButtonBill(path2);
				generateSuccessButtonFlyer(path3);

				if (path != 'false')
				{
					generateSuccessButtonLabel(path);
				} else
				{
					displayErrors("#errors", "<b><p style='font-size: 12pt; color: red;'>Label:</p></b><b><p style='font-size: 10pt; color: black;'>There is no PDF for this PO.</p></b>");
					generateFailButtonLabel();
					showErPopup();
				}

				if (path2 != 'false')
				{
					generateSuccessButtonBill(path2);
				} else
				{
					displayErrors("#errorsBill", "<b><p style='font-size: 12pt; color: red;'>Bill of Lading:</p></b><b><p style='font-size: 10pt; color: black;'>There is no PDF for this PO.</p></b>");
					generateFailButtonBill();
					showErPopup();
				}

				if (path3 != 'false')
				{
					generateSuccessButtonFlyer(path3);
				} else
				{
					displayErrors("#errorsFlyer", "<b><p style='font-size: 12pt; color: red;'>Flyer:</p></b><b><p style='font-size: 10pt; color: black;'>There is no PDF for this PO.</p></b>");
					generateFailButtonFlyer();
					showErPopup();
				}

				if (path != 'false' && path2 != 'false')
				{
					$.ajax({
						url: 'phpFunctions/logOrderReprinted.php',
						type: 'POST',
						data: {
							po: po,
							reason: reason,
							reasonNote: reasonNote,
							userName: userName
						},
						complete: function (data) {
							console.log(data.responseText);
						},
						error: function (request, status, error) {
							console.log("Ajax request error: " + request.responseText);
						}
					});
				}

			} else
			{
				generateFailButtonLabel();
				generateFailButtonBill();
				generateFailButtonFlyer();
				showErPopup();
				displayErrors("#errorsBill", "<b><p style='font-size: 12pt; color: red;'>Bill of Lading:</p></b><b><p style='font-size: 10pt; color: black;'>Error. Please enter valid PO Number and try again.</p></b>");
				displayErrors("#errorsFlyer", "<b><p style='font-size: 12pt; color: red;'>Flyer:</p></b><b><p style='font-size: 10pt; color: black;'>Error. Please enter valid PO Number and try again.</p></b>");
				displayErrors("#errors", "<b><p style='font-size: 12pt; color: red;'>Label:</p></b><b><p style='font-size: 10pt; color: black;'>Error. Please enter valid PO Number and try again.</p></b>");
			}
		},
		error: function (request, status, error) {
			console.log("Ajax request error: " + request.responseText);
		}
	});
	reprintCancel();
}

function writeToLog(po, login, lift, resi, delivery_notification)
{
	console.log(liftTempVal);
	console.log(resiTempVal);
	if (lift != liftTempVal || resi != resiTempVal || delivery_notification != delivery_notificationTempVal)
	{
		var message = 'user "' + login + '" changed values: ';
		if (lift != liftTempVal)
			message = message + ' lift: "' + liftTempVal + '" to "' + lift + '";';
		if (resi != resiTempVal)
			message = message + ' resi: "' + resiTempVal + '" to "' + resi + '";';
		if (delivery_notification != delivery_notificationTempVal)
			message = message + ' dl: "' + delivery_notificationTempVal + '" to "' + delivery_notification + '";';
		console.log(message);
		$.ajax({
			url: 'phpFunctions/functionsInsertDeliveryNotififcation.php',
			type: 'POST',
			data: {
				po: po,
				message: message
			},
			complete: function (data) {
				console.log(data.responseText);
			},
			error: function (request, status, error) {
				console.log("Ajax request error: " + request.responseText);
			}
		});
	}
}

function liftResi(po, id, obj, fedexMode, carrier, ShipAddress_Country, ShipAddress_State, ShipAddress_City, ShipAddress_PostalCode, ShipAddress_Addr1, ShipAddress_Addr2, ShipAddress_Addr3, FOB, display_popup, login, dealer, asn)
{
	$('#buttonDirect').html(" ");
	$('#buttonBillDirect').html(" ");
	$('#button').html(" ");
        $('#buttonFlyer').html(" ");
	$('#buttonBill').html(" ");
	$('#buttonManifest').html(" ");

	if (dealer == 'Ferguson' && !asn)
	{
		asnPopup(po, id, obj, fedexMode, carrier, ShipAddress_Country, ShipAddress_State, ShipAddress_City, ShipAddress_PostalCode, ShipAddress_Addr1, ShipAddress_Addr2, ShipAddress_Addr3, FOB, display_popup, login, dealer, asn);
	} else
	{
		console.log('looking for warnings...');
		$.ajax({
			url: 'phpFunctions/getWarnings.php',
			type: 'POST',
			data: {
				po: po,
				id: id
			},
			beforeSend: function () {
				$('#resiLiftLoader').fadeIn(100);
			},
			complete: function (data) {
				console.log(data.responseText);
				responses = JSON.parse(data.responseText);
				var warnings = '';
				if (responses)
				{
					for (i = 0; i < responses.length; i++)
					{
						warnings += '\n' + responses[i] + '\n';
					}
					console.log('warnings detected');
					if (confirm('ATTENTION!!!\n ' + warnings)) {
						liftResi2(po, id, obj, fedexMode, carrier, ShipAddress_Country, ShipAddress_State, ShipAddress_City, ShipAddress_PostalCode, ShipAddress_Addr1, ShipAddress_Addr2, ShipAddress_Addr3, FOB, display_popup, login);
						$('#resiLiftLoader').fadeOut(100);
					} else {
						$('#resiLiftLoader').fadeOut(100);
					}
				} else
				{
					console.log('no warnings');
					liftResi2(po, id, obj, fedexMode, carrier, ShipAddress_Country, ShipAddress_State, ShipAddress_City, ShipAddress_PostalCode, ShipAddress_Addr1, ShipAddress_Addr2, ShipAddress_Addr3, FOB, display_popup, login);
					$('#resiLiftLoader').fadeOut(100);
				}
			},
			error: function (request, status, error) {
				console.log("Ajax request error: " + request.responseText);
			}
		});
	}
}

function liftResi2(po, id, obj, fedexMode, carrier, ShipAddress_Country, ShipAddress_State, ShipAddress_City, ShipAddress_PostalCode, ShipAddress_Addr1, ShipAddress_Addr2, ShipAddress_Addr3, FOB, display_popup, login)
{
	/*console.log(po);
	 console.log(id);
	 console.log(obj);
	 console.log(fedexMode);
	 console.log(carrier);
	 console.log(ShipAddress_Country);
	 console.log(ShipAddress_State);
	 console.log(ShipAddress_City);
	 console.log(ShipAddress_PostalCode);
	 console.log(ShipAddress_Addr1);
	 console.log(ShipAddress_Addr2);
	 console.log(ShipAddress_Addr3);
	 console.log(FOB);
	 console.log(display_popup);
	 console.log(login);*/

	$.ajax({
		url: 'phpFunctions/is_divided.php',
		type: 'POST',
		data: {
			id: id
		},
		complete: function (data) {
			var isDivided1 = data.responseText;
			console.log(isDivided1);
			isDivided = JSON.parse(isDivided1);
			if (isDivided == 'no')
			{
				console.log('Order was not divided');
				if (display_popup == 'True')
				{
					//var fullAddr = '<br><table border="1" cellpadding="3"><tr><td bgcolor="silver" align="center" colspan="6"><FONT size="3" COLOR="black"><b>CONSIGNEE (to)</b></FONT></td></tr><tr><td colspan="3">Consignee:<br>Address:<br>City/State/Zip:<br>Phone#:<br></td><td colspan="3"><b>'+ShipAddress_Addr1+'<br>'+ShipAddress_Addr2+', '+ShipAddress_Addr3+'<br>'+ShipAddress_City+', '+ShipAddress_State+', '+ShipAddress_PostalCode+'<br>'+FOB+'</b></td></tr></table>';
					var fullAddr = 'Shipping Address :<br><br><b>' + ShipAddress_Addr1 + '<br>' + ShipAddress_Addr2 + ', ' + ShipAddress_Addr3 + '<br>' + ShipAddress_City + ', ' + ShipAddress_State + ', ' + ShipAddress_PostalCode + '<br>' + FOB + '</b>';
					//console.log(fullAddr);
					$("#messageRLCLabel").empty();
					$("#messageRLCLabel").append(fullAddr);
					$.ajax({
						url: 'phpFunctions/functionsGetResiLift.php',
						type: 'POST',
						data: {
							id: id
						},
						beforeSend: function () {
							$('#resiLiftLoader').fadeIn(100);
						},
						complete: function (data) {
							console.log('---------------------------------------------------------------------------------');
							console.log('data: ' + data.responseText);
							var getLiftResi = JSON.parse(data.responseText);
							if (Object.prototype.toString.call(getLiftResi) === "[object Object]") {
								var lift = getLiftResi.lift;
								var resi = getLiftResi.resi;
								//var delivery_notification = getLiftResi.delivery_notification;

								if (resi == 1)
								{
									$('#RESIDENTIAL').prop('checked', true);
									//$('#TheCheckBox').bootstrapSwitch('state', true);
									resiTempVal = '1';
									$('#resiIconOnPopup').html("<img src='Include/pictures/residentialLogo.png' width='16px' height='16px' alt='RESIDENTIAL'>");
									if (lift == 1)
									{
										$('#LIFTGATE').prop('checked', true);
										liftTempVal = '1';
										$('#liftIconOnPopup').html("<img src='Include/pictures/liftgate-icon.png' width='16px' height='16px' alt='LIFTGATE'>");
									} else
									{
										$('#LIFTGATE').prop('checked', false);
										liftTempVal = '0';
										$('#liftIconOnPopup').html("");
									}
									$('#delivery_notification').prop('checked', true);
									delivery_notificationTempVal = '1';
								} else
								{
									$('#RESIDENTIAL').prop('checked', false);
									//$('#TheCheckBox').bootstrapSwitch('state', false);
									resiTempVal = '0';
									$('#resiIconOnPopup').html("");
									$('#delivery_notification').prop('checked', false);
									delivery_notificationTempVal = '0';
									$('#LIFTGATE').prop('checked', false);
									liftTempVal = '0';
									if (lift == 1)
									{
										$('#liftIconOnPopup').html("<img src='Include/pictures/liftgate-icon.png' width='16px' height='16px' alt='LIFTGATE'>");
									} else
									{
										$('#liftIconOnPopup').html("");
									}
								}

								/*$('#TheCheckBox').on('switchChange.bootstrapSwitch', function(event, state) {
								 if($(this).bootstrapSwitch('state') == true) {
								 $('#delivery_notification').prop('checked', true);
								 if (lift == 1)
								 {
								 $('#LIFTGATE').prop('checked', true);
								 } else
								 {
								 $('#LIFTGATE').prop('checked', false);
								 }
								 } else
								 {
								 $('#delivery_notification').prop('checked', false);
								 $('#LIFTGATE').prop('checked', false);
								 }
								 });*/

								//clear hidden fields
								poTempVal = '';
								idTempVal = '';
								objTempVal = '';
								fedexModeTempVal = '';
								carrierTempVal = '';

								//set hidden fields
								poTempVal = po;
								idTempVal = id;
								objTempVal = obj;
								fedexModeTempVal = fedexMode;
								carrierTempVal = carrier;

								$('#resiLiftBackground').fadeIn(100);
								$('#resiLiftLoader').fadeOut(100);

							} else
							{
								$('#resiLiftLoader').fadeOut(100);
								console.log('Something went wrong. ' + data.responseText);
								alert('Something went wrong. Please refresh the page and try again. ' + data.responseText);
							}
						},
						error: function (request, status, error) {
							$('#resiLiftLoader').fadeOut(100);
							console.log("Ajax request error: " + request.responseText);
						}
					});
				} else {
					updateResiLift(po, id, obj, fedexMode, carrier, login, '0', '0', '0');
				}
			} else if (isDivided == 'divided')
			{
				console.log('order was divided!');
				$.ajax({
					url: 'phpFunctions/getDivided.php',
					type: 'POST',
					data: {
						id: id
					},
					complete: function (data) {
						console.log('data: ' + data.responseText);
						var getDivided = data.responseText;
						getDivided = JSON.parse(getDivided);
						var tableString = '<table><tr><th>Search for PO:</th></tr>';
						for (i = 0; i < getDivided.length; i++)
						{
							tableString += "<tr><td>" + focusOnSplit(getDivided[i]) + "</td></tr>";
						}
						tableString += '</table>';
						displayErrors("#errorsGlobal", "<b><p style='font-size: 12pt; color: red;'>Common problems:</p></b><b><p style='font-size: 10pt; color: black;'>This order was divided into new orders. It was disabled for printing. Please process new orders instead of this. Here is a list of PO numbers of new orders, related to current:</p></b>" + tableString);
						displayErrors("#errors", " ");
						displayErrors("#errorsBill", " ");
						displayErrors("#errorsManifest", " ");
						generateFailButtonBill();
						generateFailButtonLabel();
						showErPopup();
					},
					error: function (request, status, error) {
						console.log("Ajax request error: " + request.responseText);
					}
				});
			} else if (isDivided == 'combined')
			{
				console.log('order was combined!');
				$.ajax({
					url: 'phpFunctions/getCombined.php',
					type: 'POST',
					data: {
						id: id
					},
					complete: function (data) {
						console.log('data: ' + data.responseText);
						var getDivided = data.responseText;
						getDivided = JSON.parse(getDivided);
						var tableString = '<table><tr><th>Search for original PO:</th><th>Search for combined PO:</th></tr>';
						for (i = 0; i < getDivided.length; i++)
						{
							tableString += "<tr><td>" + focusOnCombined(getDivided[i][0]) + "</td><td>" + focusOnCombined(getDivided[i][1]) + "</td></tr>";
						}
						tableString += '</table>';
						displayErrors("#errorsGlobal", "<b><p style='font-size: 12pt; color: red;'>Common problems:</p></b><b><p style='font-size: 10pt; color: black;'>This order was combined with another order. It was disabled for printing. Please process new order instead of this. Here is a list of combined PO numbers:</p></b>" + tableString);
						displayErrors("#errors", " ");
						displayErrors("#errorsBill", " ");
						displayErrors("#errorsManifest", " ");
						generateFailButtonBill();
						generateFailButtonLabel();
						showErPopup();
					},
					error: function (request, status, error) {
						console.log("Ajax request error: " + request.responseText);
					}
				});
			} else
			{
				console.log('Can not check combined order.');
				displayErrors("#errorsGlobal", "<b><p style='font-size: 12pt; color: red;'>Common problems:</p></b><b><p style='font-size: 10pt; color: black;'>Error: " + isDivided + "</p></b>");
				generateFailButtonBill();
				generateFailButtonLabel();
				showErPopup();
			}
		},
		error: function (request, status, error) {
			console.log("Ajax request error: " + request.responseText);
		}
	});
	updateSplit();
}

function liftResi3(po, id, obj, fedexMode, carrier, ShipAddress_Country, ShipAddress_State, ShipAddress_City, ShipAddress_PostalCode, ShipAddress_Addr1, ShipAddress_Addr2, ShipAddress_Addr3, FOB, display_popup, login, Amount, soSubtotal)
{
	$('#buttonDirect').html(" ");
	$('#buttonBillDirect').html(" ");
	$('#button').html(" ");
        $('#buttonFlyer').html(" ");
	$('#buttonBill').html(" ");
	$('#buttonManifest').html(" ");

	Amount = Math.round(Amount * 1000) / 1000;
	soSubtotal = Math.round(soSubtotal * 1000) / 1000;

	if (Amount != soSubtotal) {
		if (!confirm("Warning: Amount not equals soSubtotal for this order\nDo you want to generate documents?")) {
			return false;
		}
	}

	console.log('looking for warnings...');
	$.ajax({
		url: 'phpFunctions/getWarnings.php',
		type: 'POST',
		data: {
			po: po,
			id: id
		},
		beforeSend: function () {
			$('#resiLiftLoader').fadeIn(100);
		},
		complete: function (data) {
			console.log(data.responseText);
			responses = JSON.parse(data.responseText);
			var warnings = '';
			if (responses)
			{
				for (i = 0; i < responses.length; i++)
				{
					warnings += '\n' + responses[i] + '\n';
				}
				console.log('warnings detected');
				if (confirm('ATTENTION!!!\n ' + warnings)) {
					liftResi2(po, id, obj, fedexMode, carrier, ShipAddress_Country, ShipAddress_State, ShipAddress_City, ShipAddress_PostalCode, ShipAddress_Addr1, ShipAddress_Addr2, ShipAddress_Addr3, FOB, display_popup, login);
					$('#resiLiftLoader').fadeOut(100);
				} else {
					$('#resiLiftLoader').fadeOut(100);
				}
			} else
			{
				console.log('no warnings');
				liftResi2(po, id, obj, fedexMode, carrier, ShipAddress_Country, ShipAddress_State, ShipAddress_City, ShipAddress_PostalCode, ShipAddress_Addr1, ShipAddress_Addr2, ShipAddress_Addr3, FOB, display_popup, login);
				$('#resiLiftLoader').fadeOut(100);
			}
		},
		error: function (request, status, error) {
			console.log("Ajax request error: " + request.responseText);
		}
	});
}


function focusOnSplit(po)
{
	var po_split = po.split('_split_');
	var real_po = po_split[0] + '_split_';
	var focus_button = "<button style='cursor:pointer;' onclick='searchSplit(" + '"' + real_po + '"' + ")'>" + po + "</button>";
	return focus_button;
}

function focusOnCombined(po)
{
	var focus_button = "<button style='cursor:pointer;' onclick='searchSplit(" + '"' + po + '"' + ")'>" + po + "</button>";
	return focus_button;
}

function searchSplit(real_po)
{
	//console.log(real_po);
	table.search(real_po).draw();
	hideErrors();
}

function goResiLift(login) {
	console.log('user: ' + login);
	goResiLiftCancel();
	var resi = $('#RESIDENTIAL').is(":checked");
	var lift = $('#LIFTGATE').is(":checked");
	var delivery_notification = $('#delivery_notification').is(":checked");

	var po = poTempVal;
	var id = idTempVal;
	var obj = objTempVal;
	var fedexMode = fedexModeTempVal;
	var carrier = carrierTempVal;

	if (resi)
	{
		resi = '1';
	} else
	{
		resi = '0';
	}
	if (lift)
	{
		lift = '1';
	} else
	{
		lift = '0';
	}
	if (delivery_notification)
	{
		delivery_notification = '1';
	} else
	{
		delivery_notification = '0';
	}
	updateResiLift(po, id, obj, fedexMode, carrier, login, lift, resi, delivery_notification);
}

function updateResiLift(po, id, obj, fedexMode, carrier, login, lift, resi, delivery_notification) {

	$.ajax({
		url: 'phpFunctions/functionsUpdateResiLift.php',
		type: 'POST',
		data: {
			id: id,
			lift: lift,
			resi: resi,
			delivery_notification: delivery_notification
		},
		complete: function (data) {
			//console.log(data.responseText);
			var liftResiSaved = JSON.parse(data.responseText);
			if (liftResiSaved == 'lift, resi, delivery_notification saved')
			{
				console.log('lift, resi, delivery_notification saved');

				writeToLog(po, login, lift, resi, delivery_notification);

				goFirst(po, id, obj, fedexMode, carrier);
				//clear hidden fields
				poTempVal = '';
				idTempVal = '';
				objTempVal = '';
				fedexModeTempVal = '';
				carrierTempVal = '';
			} else
			{
				console.log('Something went wrong. ' + data.responseText);
				alert('Something went wrong. Please refresh the page and try again. ' + data.responseText);
			}
		},
		error: function (request, status, error) {
			console.log("Ajax request error: " + request.responseText);
		}
	});
}

function goFirst(po, id, obj, fedexMode, carrier)
{
	console.log('mode: ' + fedexMode);
	console.log('carrier: ' + carrier);

	if (carrier == 'FEDEX' && fedexMode == 'electronic')
	{
		goFedex(po, id, obj, carrier);
	} else if (carrier == 'FEDEX' && fedexMode == 'mixed')
	{
		$.ajax({
			url: 'phpFunctions/functionsIsProAssigned.php',
			type: 'POST',
			data: {
				value: id
			},
			complete: function (data) {
				console.log(data.responseText);
				var fedexGetPro = data.responseText;
				getProductCodeFedexMixed(po, id, obj, carrier, fedexGetPro);
			},
			error: function (request, status, error) {
				console.log("Ajax request error: " + request.responseText);
			}
		});
	} else
	{
		go(po, id, obj, carrier);
	}
}

function runShipsingleNow(obj)
{
	$(obj).prop("disabled", true);
	obj.style.backgroundColor = "silver";
	$(obj).html('Loading...');
	$(obj).css('cursor', 'default');
	$.ajax({
		url: '/shipsingle/shipsingle.php',
		type: 'POST',
		complete: function (data) {
			console.log(data.responseText);
			if (data.responseText == 'Please try again later')
			{
				$(obj).prop("disabled", false);
				obj.style.backgroundColor = "green";
				$(obj).html('Process orders');
				$(obj).css('cursor', 'pointer');
				alert('The program is already running at the moment. Please try again later.');
			} else
			{
				$(obj).prop("disabled", false);
				obj.style.backgroundColor = "green";
				$(obj).html('Process orders');
				$(obj).css('cursor', 'pointer');
				alert('The data update is completed. Please refresh the page to see new orders.');
			}
		},
		error: function (request, status, error) {
			console.log("Ajax request error: " + request.responseText);
			$(obj).prop("disabled", false);
			obj.style.backgroundColor = "green";
			$(obj).html('Process orders');
			$(obj).css('cursor', 'pointer');
			alert("Ajax request error: " + request.responseText);
		}
	});
}

function go(po, id, obj, carrier)
{
	labelOk = false;
	bolOk = false;
	paintbuttons(obj, 'genbutton');
	var idPRO = '#PRO' + id;
	var n = idPRO.lastIndexOf(".");
	if (n != -1)
	{
		var idPRO = idPRO.substring(0, n) + "\\" + idPRO.substring(n);
	}
	var idBOL = '#BOL' + id;
	var n = idBOL.lastIndexOf(".");
	if (n != -1)
	{
		var idBOL = idBOL.substring(0, n) + "\\" + idBOL.substring(n);
	}
	var pro = $(idPRO).val();
	var bol = $(idBOL).val();

	displayErrors("#errorsGlobal", " ");

	if (!validate(po))
	{
		console.log(po);
		console.log(pro);
		console.log(bol);
		generateFailButtonLabel();
		displayErrors("#errors", "<b><p style='font-size: 12pt; color: red;'>Label:</p></b><b><p style='font-size: 10pt; color: black;'>Purchase Order number needed to generate Label!</p></b>");
		showErPopup();
	} else
	{
		getProductCode(po, id, pro, bol, carrier);
	}

	if (!validate(po))
	{
		console.log(po);
		console.log(pro);
		console.log(bol);
		generateFailButtonBill();
		displayErrors("#errorsBill", "<b><p style='font-size: 12pt; color: red;'>Bill of Lading:</p></b><b><p style='font-size: 10pt; color: black;'>Purchase Order number needed to generate Bill of Lading!</p></b>");
		showErPopup();
	} else if (validateIsSet(pro) && validateIsSet(bol))
	{
		$.ajax({
			url: 'phpFunctions/verifyPRONumberUniqueness.php',
			type: 'POST',
			data: {
				id: id,
				pro: pro
			},
			beforeSend: function () {
				loadingBill(); /*loading()*/
			},
			complete: function (data) {
				var proUsed = data.responseText;
				console.log(proUsed);
				proUsed = JSON.parse(proUsed);
				if (proUsed == 'unused')
				{
					console.log('Unused PRO#');
					getProductCodeBill(po, id, pro, bol, carrier);
				} else if (proUsed == 'used')
				{
					console.log('Used PRO#');
					displayErrors("#errorsGlobal", "<b><p style='font-size: 12pt; color: red;'>Common problems:</p></b><b><p style='font-size: 10pt; color: black;'>This PRO# was already used! Please enter another PRO#.</p></b>");
					generateFailButtonBill();
					showErPopup();
					//generateFailButtonLabel();
				} else
				{
					console.log('Can not check PRO# (used/unused)');
					displayErrors("#errorsGlobal", "<b><p style='font-size: 12pt; color: red;'>Common problems:</p></b><b><p style='font-size: 10pt; color: black;'>Error: " + proUsed + "</p></b>");
					generateFailButtonBill();
					showErPopup();
					//generateFailButtonLabel();
				}
			},
			error: function (request, status, error) {
				console.log("Ajax request error: " + request.responseText);
			}
		});
	} else if (!validateIsSet(pro) || !validateIsSet(bol))
	{
		getProductCodeBill(po, id, pro, bol, carrier);
	}
}

function goFedex(po, id, obj, carrier)
{
	labelOk = false;
	bolOk = false;
	paintbuttons(obj, 'genbutton');
	var idPRO = '#PRO' + id;
	var idBOL = '#BOL' + id;
	var pro = $(idPRO).val();
	var bol = $(idBOL).val();

	displayErrors("#errorsGlobal", " ");


	if (!validateIsSet(bol) || !validate(po))
	{
		generateFailButtonBill();
		displayErrors("#errorsBill", "<b><p style='font-size: 12pt; color: red;'>Bill of Lading:</p></b><b><p style='font-size: 10pt; color: black;'>INVOICE# and Purchase Order number data needed to generate Fedex Bill of Lading!</p></b>");
		generateFailButtonLabel();
		displayErrors("#errors", "<b><p style='font-size: 12pt; color: red;'>Label:</p></b><b><p style='font-size: 10pt; color: black;'>INVOICE# and Purchase Order number needed to generate Fedex Label!</p></b>");
		showErPopup();
	} else
	{
		getProductCodeFedex(po, id, pro, bol, carrier);
	}
}

function goManifest()
{
	$('#dateBackground').fadeIn(100);
	$('#dateMessageBox').toggleClass('animated bounceIn');
	//getManifest();
}

function goStartManifest()
{
	$('#buttonDirect').html(" ");
	$('#buttonBillDirect').html(" ");
	$('#button').html(" ");
	$('#buttonBill').html(" ");
	$('#buttonManifest').html(" ");
	var currentDate = $("#datepicker").datepicker("getDate");
	day = currentDate.getDate();
	month = currentDate.getMonth() + 1;
	year = currentDate.getFullYear();
	stringCurrentDate = year + '-' + month + '-' + day;
	getManifest(stringCurrentDate);
	goManifestCancel();
}

function goManifestCancel()
{
	$('#dateBackground').fadeOut(100);
	$('#dateMessageBox').removeClass();
}

function goResiLiftCancel()
{
	$('#resiLiftBackground').fadeOut(100);
}

function lock(po, object)
{
	var objectClass = $(object).attr('class');
	var inputId = '#PRO' + po;
	var n = inputId.lastIndexOf(".");
	if (n > -1)
	{
		inputId = inputId.substring(0, n) + "\\" + inputId.substring(n);
	}

	//console.log(inputId);
	if (objectClass == 'unlockedInput')
	{
		$(object).html("<img width='21px' height='21px' src='Include/pictures/lock.png'>");
		$(inputId).prop('disabled', true);
		$(object).removeClass('unlockedInput');
		$(object).toggleClass('lockedInput');
	} else if (objectClass == 'lockedInput')
	{
		$(object).html("<img width='21px' height='21px' src='Include/pictures/unlock.png'>");
		$(inputId).prop('disabled', false);
		$(object).removeClass('lockedInput');
		$(object).toggleClass('unlockedInput');
	}
}

function updateTOZIP(id)
{
	//window.open('phpFunctions/update_missing.php?id='+id);
	$('#MissingBackground').fadeIn(100);
	$('#MissingMessageBox').toggleClass('animated bounceIn');
	$('#ID').val(id);
}

function updateTOZIPcancel()
{
	$('#MissingBackground').fadeOut(100);
	$('#MissingMessageBox').removeClass();
}

function updateTOZIPhandle()
{
	$('#MissingBackground').fadeOut(100);
	$('#MissingMessageBox').removeClass();
	var id = $('#ID').val();
	var tozip = $('#TOZIP').val();
	console.log(id);
	console.log(tozip);
	$.ajax({
		url: 'phpFunctions/handlerUpdate_missing.php',
		type: 'POST',
		data: {
			id: id,
			value: tozip
		},
		complete: function (data) {
			console.log(data.responseText);
		},
		error: function (request, status, error) {
			console.log("Ajax request error: " + request.responseText);
		}
	});
}

function refreshOrder(id, po, obj)
{
	obj_remove_order = obj;
	$('#removeBackground').fadeIn(100);
	$('#removePO').val(po);
	setFocusToId('#removePO');
	$("#removePO").select();
}

function removeOrder(userName)
{
	var po = $('#removePO').val();
	var reason = $("#reasonSelect option:selected").val();
	var reasonNote = $("#reasonNote").val();

	$(obj_remove_order).parents('tr').fadeOut(500, function () {
		table.row($(obj_remove_order).parents('tr')).remove();
	});


	$.ajax({
		url: 'phpFunctions/refreshOrder.php',
		type: 'POST',
		data: {
			po: po,
			reason: reason,
			reasonNote: reasonNote,
			userName: userName
		},
		complete: function (data) {
			console.log(data.responseText);
			removeCancel();
			run_shipsingle_po(po);
		},
		error: function (request, status, error) {
			console.log("Ajax request error: " + request.responseText);
			removeCancel();
		}
	});
	removeCancel();
}

function run_shipsingle_po(po) {
	if (!po)
		po = '';
	console.log('shipsingle started');
	$.ajax({
		url: '/shipsingle/shipsingle.php',
		type: 'GET',
		data: {
			po: po
		},
		complete: function (data) {
			//console.log(data.responseText);
			console.log('shipsingle finished');
		},
		error: function (request, status, error) {
			console.log("Ajax request error: " + request.responseText);
		}
	});
}

/*function goBill(id, obj)
 {
 paintbuttons(obj, 'genbuttonBill');
 var idPRO = '#PRO'+id;
 var idBOL = '#BOL'+id;
 var pro = $(idPRO).val();
 var bol = $(idBOL).val();
 
 if (!validateIsSet(pro) || !validateIsSet(bol))
 {
 generateFailButtonBill();
 //displayErrors("#errorsBill", "<b><p style='font-size: 12pt; color: red;'>Bill of Lading:</p></b><b><p style='font-size: 10pt; color: black;'>PRO# and INVOICE# data needed to generate Bill of Lading!</p></b><button style='font-family:Arial, Helvetica, sans-serif; font-size:11px; width: 121px; height: 30px; color: #ffffff; background: red; border:0; cursor: pointer; margin-top: 20px;' onclick="+'"'+"getProductCodeBill('"+id+"', ' ', ' '); hideErrors();"+'"'+">Generate with empty fields</button>");
 } else
 {
 getProductCodeBill(id, pro, bol);
 }
 }*/

function validateIsSet(value)
{
	if (value == null || value == "" || value == " ")
		return false;
	else
		return true;
}

function paintbuttons(obj, selector)
{
	elements = document.getElementsByClassName(selector);
	for (var i = 0; i < elements.length; i++) {
		if (elements[i].getAttribute('class') == "genbutton done")
		{
			elements[i].style.backgroundColor = "#F9A63F";
		} else
		{
			elements[i].style.backgroundColor = "#367FBB";
		}
	}
	if (selector == "genbutton")
		(obj).style.backgroundColor = "#82A716";
	//if (selector == "genbuttonBill") (obj).style.backgroundColor="#F9A63F";
}

function loading()
{
	$('#buttonManifest').html(" ");
	$('#errorsManifest').html(" ");
	$('#errors').html(" ");
	$('#button').html("<img src='Include/gif/greenLoad.gif' width='50' height='50' alt='loading...'>");
        $('#buttonFlyer').html("<img src='Include/gif/greenLoad.gif' width='50' height='50' alt='loading...'>");
}

function loadingBill()
{
	$('#buttonManifest').html(" ");
	$('#errorsManifest').html(" ");
	$('#errorsBill').html(" ");
	$('#buttonBill').html("<img src='Include/gif/orangeLoad.gif' width='50' height='50' alt='loading...'>");
}

function loadingManifest()
{
	$('#buttonManifest').html(" ");
	$('#errorsManifest').html(" ");
	$('#buttonManifest').html("<img src='Include/gif/turquoiseLoad.gif' width='50' height='50' alt='loading...'>");
}

function generate_unique_string()
{
	var timestamp = new Date().getUTCMilliseconds();
	return timestamp;
}

function generateSuccessButtonManifest(path)
{
	var time = generate_unique_string();
	$('#buttonManifest').html("<button style='font-family:Arial, Helvetica, sans-serif; font-size:11px; width: 100px; height: 50px; color: #ffffff; background: #049A9C; border:0; cursor: pointer;' onclick=" + '"' + "window.open('newTab.php?value=" + path + "&time=" + time + "')" + '"' + ">Manifest</button>").fadeIn(500);
}

function generateFailButtonManifest()
{
	$('#buttonManifest').html("<button style='font-family:Arial, Helvetica, sans-serif; font-size:11px; width: 100px; height: 50px; color: #ffffff; background: silver; border:0; cursor: pointer;' onclick='showErPopup()'>Manifest N/A</button>").fadeIn(500);
}

function generateErrorButtonManifest()
{
	$('#buttonManifest').html("<button style='font-family:Arial, Helvetica, sans-serif; font-size:11px; width: 100px; height: 50px; color: #ffffff; background: red; border:0; cursor: pointer;' onclick='showErPopup()'>Manifest Error!</button>").fadeIn(500);
}

function generateSuccessButtonLabel(path)
{
	var time = generate_unique_string();
	$('#button').html("<button style='font-family:Arial, Helvetica, sans-serif; font-size:11px; width: 100px; height: 50px; color: #ffffff; background: #82A716; border:0; cursor: pointer;' onclick=" + '"' + "window.open('newTab.php?value=" + path/*+"&printer="+printer_label_global*/ + "&time=" + time + "')" + '"' + ">Label</button>").fadeIn(500);
	if (qz.websocket.isActive()) {
		var pathDirect = decodeURIComponent(path);
		$('#buttonDirect').html("<button style='font-family:Arial, Helvetica, sans-serif; font-size:11px; width: 100px; height: 20px; color: #ffffff; background: #82A716; border:0; cursor: pointer;'  onclick='printOn(" + '"' + printer_label_global + '"' + ", " + '"' + pathDirect + '"' + ");'>Print Label</button>").fadeIn(500);
	}
}

function generateSuccessButtonFlyer(path)
{
	var time = generate_unique_string();
	$('#buttonFlyer').html("<button style='font-family:Arial, Helvetica, sans-serif; font-size:11px; width: 100px; height: 50px; color: #ffffff; background: #82A716; border:0; cursor: pointer;' onclick=" + '"' + "window.open('newTab.php?value=" + path/*+"&printer="+printer_label_global*/ + "&time=" + time + "')" + '"' + ">Flyer</button>").fadeIn(500);
	if (qz.websocket.isActive()) {
		var pathDirect = decodeURIComponent(path);
		$('#buttonFlyerDirect').html("<button style='font-family:Arial, Helvetica, sans-serif; font-size:11px; width: 100px; height: 20px; color: #ffffff; background: #82A716; border:0; cursor: pointer;'  onclick='printOn(" + '"' + printer_label_global + '"' + ", " + '"' + pathDirect + '"' + ");'>Print Label</button>").fadeIn(500);
	}
}

function generateFailButtonLabel()
{
	$('#button').html("<button style='font-family:Arial, Helvetica, sans-serif; font-size:11px; width: 100px; height: 50px; color: #ffffff; background: silver; border:0; cursor: pointer;' onclick='showErPopup()'>Label N/A</button>").fadeIn(500);
}

function generateFailButtonFlyer()
{
	$('#buttonFlyer').html("<button style='font-family:Arial, Helvetica, sans-serif; font-size:11px; width: 100px; height: 50px; color: #ffffff; background: silver; border:0; cursor: pointer;' onclick='showErPopup()'>Flyer N/A</button>").fadeIn(500);
}

function generateErrorButtonLabel()
{
	$('#button').html("<button style='font-family:Arial, Helvetica, sans-serif; font-size:11px; width: 100px; height: 50px; color: #ffffff; background: red; border:0; cursor: pointer;' onclick='showErPopup()'>Label error!</button>").fadeIn(500);
}

function generateErrorButtonFlyer()
{
	$('#buttonFlyer').html("<button style='font-family:Arial, Helvetica, sans-serif; font-size:11px; width: 100px; height: 50px; color: #ffffff; background: red; border:0; cursor: pointer;' onclick='showErPopup()'>Flyer error!</button>").fadeIn(500);
}

function generateSuccessButtonBill(path)
{
	var time = generate_unique_string();
	$('#buttonBill').html("<button style='font-family:Arial, Helvetica, sans-serif; font-size:11px; width: 100px; height: 50px; color: #ffffff; background: #F9A63F; border:0; cursor: pointer;' onclick=" + '"' + "window.open('newTab.php?value=" + path/*+"&printer="+printer_bol_global*/ + "&time=" + time + "')" + '"' + ">Bill of Lading</button>").fadeIn(500);
	if (qz.websocket.isActive()) {
		var pathDirect = decodeURIComponent(path);
		$('#buttonBillDirect').html("<button style='font-family:Arial, Helvetica, sans-serif; font-size:11px; width: 100px; height: 20px; color: #ffffff; background: #F9A63F; border:0; cursor: pointer;'  onclick='printOn(" + '"' + printer_bol_global + '"' + ", " + '"' + pathDirect + '"' + ");'>Print BOL</button>").fadeIn(500);
	}
}

function generateFailButtonBill()
{
	$('#buttonBill').html("<button style='font-family:Arial, Helvetica, sans-serif; font-size:11px; width: 100px; height: 50px; color: #ffffff; background: silver; border:0; cursor: pointer;' onclick='showErPopup()'>Bill of Lading N/A</button>").fadeIn(500);
}

function generateErrorButtonBill()
{
	$('#buttonBill').html("<button style='font-family:Arial, Helvetica, sans-serif; font-size:11px; width: 100px; height: 50px; color: #ffffff; background: red; border:0; cursor: pointer;' onclick='showErPopup()'>Bill of Lading Error!</button>").fadeIn(500);
}

function displayErrors(selector, content)
{
	//hideErrors();
	if (content)
	{
		$(selector).html(content);
	}
}

function showErPopup()
{
	$("#errorsInvisDiv").fadeIn(500);
}

function hideErrors()
{
	$("#errorsInvisDiv").fadeOut(500);
}

function getProductCode(value, id, pro, bol, carrier)
{
	var productCode;
	$.ajax({
		url: 'phpFunctions/functions.php',
		type: 'POST',
		data: {
			value: value,
			id: id,
			pro: pro,
			bol: bol
		},
		beforeSend: function () {
			loading()
		},
		complete: function (data) {
			$('#button').css('display', 'none');
                        $('#buttonFlyer').css('display', 'none');
			//console.log(data.responseText);
			productCodeText = data.responseText;
			console.log(productCodeText);

			try
			{
				productCode = JSON.parse(productCodeText);
				if (Object.prototype.toString.call(productCode) === "[object Object]") {
					var path = productCode.path;
					if (path != 'false')
					{
						generateSuccessButtonLabel(path);
						labelOk = true;
						if ((pro == 'test' || pro == 'TEST') && (bol == 'test' || bol == 'TEST'))
							console.log('TESTING MODE');
						else
							saveTimeStamp(id, value);
						saveProBol(value, id, pro, bol, carrier);
					} else
					{
						generateFailButtonLabel();
						showErPopup();
					}
                                        
                                        var pathFlyer = productCode.pathFlyer;
					if (pathFlyer != 'false')
					{
						generateSuccessButtonFlyer(pathFlyer);
						labelOk = true;
						if ((pro == 'test' || pro == 'TEST') && (bol == 'test' || bol == 'TEST'))
							console.log('TESTING MODE');
						else
							saveTimeStamp(id, value);
						saveProBol(value, id, pro, bol, carrier);
					} else
					{
						generateFailButtonFlyer();
						if (productCode.er || productCode.DataEr) 
                                                {
                                                    showErPopup();
                                                } else
                                                {
                                                    productCode.er = "<br>Flyer is not available for this order.<br>";
                                                }
					}
					displayErrors("#errors", productCode.er + productCode.DataEr);
				} else
				{
					generateErrorButtonLabel();
					displayErrors("#errors", "<pre>" + productCodeText + "</pre>");
				}
			} catch (e)
			{
				generateErrorButtonLabel();
				displayErrors("#errors", "<pre>" + productCodeText + "</pre>");

			}
		},
		error: function (request, status, error) {
			console.log("Ajax request error: " + request.responseText);
			generateErrorButtonLabel();
			displayErrors("#errors", "<b><p style='font-size: 12pt; color: red;'>Label:</p></b><b><p style='font-size: 10pt; color: black;'>Ajax request error: " + request.responseText + "</p></b>");
		}
	});
}

function getProductCodeBill(value, id, pro, bol, carrier)
{
	var productCode;
	$.ajax({
		url: 'phpFunctions/functionsBill.php',
		type: 'POST',
		data: {
			value: value,
			id: id,
			pro: pro,
			bol: bol
		},
		beforeSend: function () {
			loadingBill()
		},
		complete: function (data) {
			$('#buttonBill').css('display', 'none');
			//console.log(data.responseText);
			productCodeText = data.responseText;
			console.log(productCodeText);

			try
			{
				productCode = JSON.parse(productCodeText);
				if (Object.prototype.toString.call(productCode) === "[object Object]") {
					var path = productCode.path;
					if (path != 'false')
					{
						generateSuccessButtonBill(path);
						bolOk = true;
						if ((pro == 'test' || pro == 'TEST') && (bol == 'test' || bol == 'TEST'))
							console.log('TESTING MODE');
						else
							saveTimeStamp(id, value);
						saveProBol(value, id, pro, bol, carrier);
					} else
					{
						generateFailButtonBill();
						showErPopup();
					}
					displayErrors("#errorsBill", productCode.er + productCode.DataEr);
				} else
				{
					generateErrorButtonBill();
					displayErrors("#errorsBill", "<pre>" + productCodeText + "</pre>");
				}
			} catch (e)
			{
				generateErrorButtonBill();
				displayErrors("#errorsBill", "<pre>" + productCodeText + "</pre>");

			}
		},
		error: function (request, status, error) {
			console.log("Ajax request error: " + request.responseText);
			generateErrorButtonBill();
			displayErrors("#errorsBill", "<b><p style='font-size: 12pt; color: red;'>Bill of Lading:</p></b><b><p style='font-size: 10pt; color: black;'>Ajax request error: " + request.responseText + "</p></b>");
		}
	});
}

function getManifest(currentDate)
{
	var productCode;
	$.ajax({
		url: 'phpFunctions/functionsManifest.php',
		type: 'POST',
		data: {
			currentDate: currentDate,
		},
		beforeSend: function () {
			loadingManifest()
		},
		complete: function (data) {
			$('#buttonManifest').css('display', 'none');
			productCodeText = data.responseText;
			console.log(productCodeText);

			try
			{
				productCode = JSON.parse(productCodeText);
				if (Object.prototype.toString.call(productCode) === "[object Object]") {
					var path = productCode.path;

					console.log(path);

					if (path != 'false')
					{
						generateSuccessButtonManifest(path);
					} else
					{
						generateFailButtonManifest();
						showErPopup();
					}
					displayErrors("#errorsManifest", productCode.er + productCode.DataEr);
				} else
				{
					generateErrorButtonManifest();
					displayErrors("#errorsManifest", "<pre>" + productCodeText + "</pre>");
				}
			} catch (e)
			{
				generateErrorButtonManifest();
				displayErrors("#errorsManifest", "<pre>" + productCodeText + "</pre>");

			}
		},
		error: function (request, status, error) {
			console.log("Ajax request error: " + request.responseText);
			generateErrorButtonManifest();
			displayErrors("#errorsManifest", "<b><p style='font-size: 12pt; color: red;'>Manifest:</p></b><b><p style='font-size: 10pt; color: black;'>Ajax request error: " + request.responseText + "</p></b>");
		}
	});
}

function getProductCodeFedex(value, id, pro, bol, carrier)
{
	var productCode;
	$.ajax({
		url: 'phpFunctions/functionsFedex.php',
		type: 'POST',
		data: {
			value: value,
			pro: pro,
			bol: bol
		},
		beforeSend: function () {
			loadingBill();
			loading();
		},
		complete: function (data) {
			$('#buttonBill').css('display', 'none');
			$('#button').css('display', 'none');
			productCodeText = data.responseText;
			console.log(productCodeText);

			try
			{
				productCode = JSON.parse(productCodeText);
				if (Object.prototype.toString.call(productCode) === "[object Object]") {

					var proReturned = productCode.pro;
					var path = productCode.path;
					var path2 = productCode.path2;

					console.log('PRO# from Fedex: ' + proReturned);
					console.log('BOL from Fedex: ' + path);
					console.log('Label from Fedex: ' + path2);

					if (proReturned != 'false')
					{
						if (path != 'false')
						{
							generateSuccessButtonBill(path);
							bolOk = true;
							saveProBol(value, id, proReturned, bol, carrier);
						} else
						{
							generateFailButtonBill();
							showErPopup();
						}

						if (path2 != 'false')
						{
							generateSuccessButtonLabel(path2);
							labelOk = true;
							saveProBol(value, id, proReturned, bol, carrier);
						} else
						{
							generateFailButtonLabel();
							showErPopup();
						}
						if (path != 'false' && path2 != 'false')
						{
							if ((pro == 'test' || pro == 'TEST') && (bol == 'test' || bol == 'TEST'))
								console.log('TESTING MODE');
							else
								saveTimeStamp(id, value);
						}
					} else
					{
						generateFailButtonLabel();
						generateFailButtonBill();
						showErPopup();
						displayErrors("#errorsBill", "<b><p style='font-size: 12pt; color: red;'>Bill of Lading:</p></b><b><p style='font-size: 10pt; color: black;'>No PRO# returned from Fedex.</p></b>");
						displayErrors("#errors", "<b><p style='font-size: 12pt; color: red;'>Label:</p></b><b><p style='font-size: 10pt; color: black;'No PRO# returned from Fedex.</p></b>");
					}

					displayErrors("#errorsBill", productCode.er + productCode.DataEr);
					displayErrors("#errors", productCode.er + productCode.DataEr);
				} else
				{
					generateErrorButtonBill();
					generateErrorButtonLabel();
					displayErrors("#errorsBill", "<pre>" + productCodeText + "</pre>");
					displayErrors("#errors", "<pre>" + productCodeText + "</pre>");
				}
			} catch (e)
			{
				generateErrorButtonBill();
				generateErrorButtonLabel();
				displayErrors("#errorsBill", "<pre>" + productCodeText + "</pre>");
				displayErrors("#errors", "<pre>" + productCodeText + "</pre>");
			}
		},
		error: function (request, status, error) {
			console.log("Ajax request error: " + request.responseText);
			generateErrorButtonBill();
			generateErrorButtonLabel();
			displayErrors("#errorsBill", "<b><p style='font-size: 12pt; color: red;'>Bill of Lading:</p></b><b><p style='font-size: 10pt; color: black;'>Ajax request error: " + request.responseText + "</p></b>");
			displayErrors("#errors", "<b><p style='font-size: 12pt; color: red;'>Label:</p></b><b><p style='font-size: 10pt; color: black;'>Ajax request error: " + request.responseText + "</p></b>");
		}
	});
}

function getProductCodeFedexMixed(value, id, obj, carrier, fedexGetPro)
{
	console.log('fedexGetPro: ' + fedexGetPro);
	labelOk = false;
	bolOk = false;
	paintbuttons(obj, 'genbutton');
	var idPRO = '#PRO' + id;
	var idBOL = '#BOL' + id;
	var pro = $(idPRO).val();
	var bol = $(idBOL).val();
	if (!validateIsSet(bol) || !validate(value))
	{
		generateFailButtonBill();
		displayErrors("#errorsBill", "<b><p style='font-size: 12pt; color: red;'>Bill of Lading:</p></b><b><p style='font-size: 10pt; color: black;'>INVOICE# and Purchase Order number data needed to generate Fedex Bill of Lading!</p></b>");
		generateFailButtonLabel();
		displayErrors("#errors", "<b><p style='font-size: 12pt; color: red;'>Label:</p></b><b><p style='font-size: 10pt; color: black;'>INVOICE# and Purchase Order number needed to generate Fedex Label!</p></b>");
		showErPopup();
	} else
	{
		var productCode;
		if (fedexGetPro == 'getPRO')
		{
			$.ajax({
				url: 'phpFunctions/functionsFedex.php',
				type: 'POST',
				data: {
					value: value,
					pro: pro,
					bol: bol,
					fedexGetPro: fedexGetPro
				},
				beforeSend: function () {
					loadingBill();
					loading();
				},
				complete: function (data) {
					productCodeText = data.responseText;
					console.log(productCodeText);

					try
					{
						productCode = JSON.parse(productCodeText);
						if (Object.prototype.toString.call(productCode) === "[object Object]") {

							var proReturned = productCode.pro;
							var path = productCode.path;
							var path2 = productCode.path2;

							console.log('PRO# from Fedex: ' + proReturned);
							console.log('BOL from Fedex: ' + path);
							console.log('Label from Fedex: ' + path2);

							if (proReturned != 'false')
							{
								$.ajax({
									url: 'phpFunctions/verifyPRONumberUniqueness.php',
									type: 'POST',
									data: {
										id: id,
										pro: proReturned
									},
									beforeSend: function () {
										loadingBill();
										loading()
									},
									complete: function (data) {
										var proUsed = data.responseText;
										proUsed = JSON.parse(proUsed);
										if (proUsed == 'unused')
										{
											$.ajax({
												url: 'phpFunctions/functionsTryToSavePro.php',
												type: 'POST',
												data: {
													value: id,
													pro: proReturned,
													carrier: carrier
												},
												complete: function (data) {
													console.log(data.responseText);
													if (data.responseText == 'CARRIERNAME_PRO IS OK')
													{
														console.log('Unused PRO#');
														getProductCode(value, id, proReturned, bol, carrier);
														getProductCodeBill(value, id, proReturned, bol, carrier);
														$(idPRO).val(proReturned);
													} else
													{
														console.log('Used PRO#');
														displayErrors("#errorsGlobal", "<b><p style='font-size: 12pt; color: red;'>Common problems:</p></b><b><p style='font-size: 10pt; color: black;'>Fedex returned PRO# that was already used! Please try to generate PDF later. You will get another PRO#.</p></b>");
														generateFailButtonBill();
														generateFailButtonLabel();
														showErPopup();
													}
												},
												error: function (request, status, error) {
													console.log("Ajax request error: " + request.responseText);
												}
											});

										} else if (proUsed == 'used')
										{
											console.log('Used PRO#');
											displayErrors("#errorsGlobal", "<b><p style='font-size: 12pt; color: red;'>Common problems:</p></b><b><p style='font-size: 10pt; color: black;'>This PRO# was already used! Please enter another PRO#.</p></b>");
											generateFailButtonBill();
											generateFailButtonLabel();
											showErPopup();
										} else
										{
											console.log('Can not check PRO# (used/unused)');
											displayErrors("#errorsGlobal", "<b><p style='font-size: 12pt; color: red;'>Common problems:</p></b><b><p style='font-size: 10pt; color: black;'>Error: " + proUsed + "</p></b>");
											generateFailButtonBill();
											generateFailButtonLabel();
											showErPopup();
										}
									},
									error: function (request, status, error) {
										console.log("Ajax request error: " + request.responseText);
									}
								});
							} else
							{
								generateFailButtonLabel();
								generateFailButtonBill();
								showErPopup();
								displayErrors("#errorsBill", "<b><p style='font-size: 12pt; color: red;'>Bill of Lading:</p></b><b><p style='font-size: 10pt; color: black;'>No PRO# returned from Fedex.</p></b>");
								displayErrors("#errors", "<b><p style='font-size: 12pt; color: red;'>Label:</p></b><b><p style='font-size: 10pt; color: black;'No PRO# returned from Fedex.</p></b>");
							}

							displayErrors("#errorsBill", productCode.er + productCode.DataEr);
							displayErrors("#errors", productCode.er + productCode.DataEr);
						} else
						{
							generateErrorButtonBill();
							generateErrorButtonLabel();
							displayErrors("#errorsBill", "<pre>" + productCodeText + "</pre>");
							displayErrors("#errors", "<pre>" + productCodeText + "</pre>");
						}
					} catch (e)
					{
						generateErrorButtonBill();
						generateErrorButtonLabel();
						displayErrors("#errorsBill", "<pre>" + productCodeText + "</pre>");
						displayErrors("#errors", "<pre>" + productCodeText + "</pre>");
					}
				},
				error: function (request, status, error) {
					console.log("Ajax request error: " + request.responseText);
					generateErrorButtonBill();
					generateErrorButtonLabel();
					displayErrors("#errorsBill", "<b><p style='font-size: 12pt; color: red;'>Bill of Lading:</p></b><b><p style='font-size: 10pt; color: black;'>Ajax request error: " + request.responseText + "</p></b>");
					displayErrors("#errors", "<b><p style='font-size: 12pt; color: red;'>Label:</p></b><b><p style='font-size: 10pt; color: black;'>Ajax request error: " + request.responseText + "</p></b>");
				}
			});
		} else if (fedexGetPro == 'noPRO')
		{
			console.log('PRO# is already assigned.');
			getProductCode(value, id, pro, bol, carrier);
			getProductCodeBill(value, id, pro, bol, carrier);
		} else
		{
			console.log('Dont know should i assign PRO# or no.');
			generateErrorButtonBill();
			generateErrorButtonLabel();
			displayErrors("#errorsGlobal", "<b><p style='font-size: 12pt; color: red;'>Common problems:</p></b><b><p style='font-size: 10pt; color: black;'>Can not get variable 'fedexGetPro'.</p></b>");
		}
	}
}

function saveTimeStamp(value, po)
{
	var userName = $('#userNameHidden').val();
	if (labelOk == true && bolOk == true)
	{
		$.ajax({
			url: 'phpFunctions/functionsSaveTimeStamp.php',
			type: 'POST',
			data: {
				value: value
			},
			complete: function (data) {
				console.log(data.responseText);
			},
			error: function (request, status, error) {
				console.log("Ajax request error: " + request.responseText);
			}
		});
		$.ajax({
			url: 'phpFunctions/logOrderRegenerated.php',
			type: 'POST',
			data: {
				po: po,
				userName: userName
			},
			complete: function (data) {
				console.log(data.responseText);
			},
			error: function (request, status, error) {
				console.log("Ajax request error: " + request.responseText);
			}
		});
	}
	//console.log('saveTimeStamp turned off!');
}

function saveProBol(value, id, pro, bol, carrier)
{
	if (labelOk == true && bolOk == true)
	{
		if (pro && bol)
		{
			$.ajax({
				url: 'phpFunctions/functionsSaveProBol.php',
				type: 'POST',
				data: {
					value: id,
					pro: pro,
					bol: bol,
					carrier: carrier
				},
				complete: function (data) {
					console.log(data.responseText);
				},
				error: function (request, status, error) {
					console.log("Ajax request error: " + request.responseText);
				}
			});
		} else
			console.log('PRO# or INVOICE# is empty and will not be saved.');
	}
	//console.log('saveProBol turned off!');
}

function validate(value)
{
	//var pattern=/^[a-zA-Z0-9- _/]+$/;
	//if (value.length<3 || value.length>35 || pattern.test(value) === false) return false; else return true;
	if ((typeof value != 'undefined') && (value != '') && (value != null))
		return true;
	else
		return false;
}

function goStatus()
{
	$('#statusBackground').fadeIn(100);
	setFocusToId('#statusPO');
}

function get_status_history()
{
	var po = $('#statusPO').val();
	var win = window.open('order_status_history.php?po=' + po, '_blank');
	win.focus();
	statusCancel();
}

function runMissingOrders()
{
	var win = window.open('/order_details/display_data.php?id=16', '_blank');
	win.focus();
}

function showRemoveReasonNote()
{
	var lastValue = $('#reasonSelect option:last-child').val();
	var selectedValue = $("#reasonSelect option:selected").text();
	if (lastValue == selectedValue)
	{
		$('#removeLabel').fadeIn(50);
		$('#reasonNote').fadeIn(50);
		setFocusToId('#reasonNote');
	} else
	{
		$('#removeLabel').fadeOut(50);
		$('#reasonNote').fadeOut(50);
	}
}

function showReprintReasonNote()
{
	var lastValue = $('#reasonSelectreprint option:last-child').val();
	var selectedValue = $("#reasonSelectreprint option:selected").text();
	if (lastValue == selectedValue)
	{
		$('#reprintLabel').fadeIn(50);
		$('#reasonNotereprint').fadeIn(50);
		setFocusToId('#reasonNotereprint');
	} else
	{
		$('#reprintLabel').fadeOut(50);
		$('#reasonNotereprint').fadeOut(50);
	}
}

function setFocusToId(id) {
	setTimeout(function () {
		$(id).focus();
	}, 100);
}

function showChangePass()
{
	$('#changepassBackground').fadeIn(100);
}

function changepassCancel()
{
	$('#changepassBackground').fadeOut(100);
	$('#changepassOldPass').val('');
	$('#changepassNewPass').val('');
}

function changePassword(id)
{
	var oldPass = $('#changepassOldPass').val();
	var newPass = $('#changepassNewPass').val();
	var message = '';
	$.ajax({
		url: 'phpFunctions/changePassword.php',
		type: 'POST',
		data: {
			id: id,
			oldPass: oldPass,
			newPass: newPass
		},
		complete: function (data) {
			console.log(data.responseText);
			var result = data.responseText;
			result = JSON.parse(result);
			//console.log(result);
			if (result == "pass_updated")
			{
				alert('Your password updated successfully. You will be redirected to login page.');
				location.reload();
			} else
			{
				message += 'Can not update password.';
				if (result == "wrong_pass")
					message += ' Please type your current password correct.';
				if (result == "no_data")
					message += ' Please fill all fields.';
				if (result == "no_auth")
					message += ' Please re-login and try again.';
				alert(message);
			}
		},
		error: function (request, status, error) {
			console.log("Ajax request error: " + request.responseText);
		}
	});
}

function runUnhandledorders()
{
	var win = window.open('/order_details/display_data.php?id=22', '_blank');
	win.focus();
}

function pallets_popup(id)
{
	$('#palletsID').val(id);
	$('#palletsBackground').fadeIn(100);
}

function palletsCancel()
{
	$('#palletsBackground').fadeOut(100);
	$('#pallets').val('');
	$('#palletsID').val('');
}

function updatePallets()
{
	var id = $('#palletsID').val();
	var pallets = $('#pallets').val();
	$.ajax({
		url: 'phpFunctions/updatePallets.php',
		type: 'POST',
		data: {
			id: id,
			pallets: pallets
		},
		complete: function (data) {
			//console.log(data.responseText);
			var result = data.responseText;
			//console.log(result);
			result1 = JSON.parse(result);
			if (result1 == "pallets_updated")
			{
				console.log('Pallets updated.');
				console.log('#PALLETS' + id);
				$('#PALLETS' + id).text(pallets);
				palletsCancel();
			} else
			{
				message = 'Can not update pallets. Error:';
				message += result;
				palletsCancel();
				alert(message);
			}
		},
		error: function (request, status, error) {
			console.log("Ajax request error: " + request.responseText);
		}
	});
}

function runDuplicate_status()
{
	var win = window.open('duplicate_status.php', '_blank');
	win.focus();
}

function printers_popup(id)
{
	var dialog = $('#dialog').dialog({
		autoOpen: false,
		height: 'auto',
		width: 'auto',
		resizable: false,
		autoResize: true,
		modal: true,
		title: 'Printer settings',
		overlay: {backgroundColor: "#000", opacity: 0.5},
	});
	dialog.dialog('close');

	$.ajax({
		url: "phpFunctions/getPrinters.php",
		type: 'POST',
		data: {
			id: id
		},
		complete: function (data) {
			//console.log(data.responseText);
			responses = JSON.parse(data.responseText);

			$('#content').html("<p>Please enter printer name:</p><input type='text' id='printer_label_popup' name='printer_label_popup' value='" + responses[0].printer_label + "'> Printer for Label<br><br><input type='text' id='printer_bol_popup' name='printer_bol_popup' value='" + responses[0].printer_bol + "'> Printer for BOL<br><br>_____________________________<br><br><input type='text' id='printer_hddc1_popup' name='printer_hddc1_popup' value='" + responses[0].printer_hddc1 + "'> Printer for HD DC Flyer<br><br><input type='text' id='printer_hddc2_popup' name='printer_hddc2_popup' value='" + responses[0].printer_hddc2 + "'> Printer for HD DC Barcodes<br><br><button id='save_printers'>Save settings and logout</button>");

			dialog.dialog('open');

			$('#save_printers').click(function () {
				var printer_label = $('#printer_label_popup').val();
				var printer_bol = $('#printer_bol_popup').val();
				var printer_hddc1 = $('#printer_hddc1_popup').val();
				var printer_hddc2 = $('#printer_hddc2_popup').val();
				$.ajax({
					url: "phpFunctions/savePrinters.php",
					type: 'POST',
					data: {
						id: id,
						printer_label: printer_label,
						printer_bol: printer_bol,
						printer_hddc1: printer_hddc1,
						printer_hddc2: printer_hddc2
					},
					complete: function (data) {
						console.log(data.responseText);
						dialog.dialog('close');
						logout();
					},
					error: function (request, status, error) {
						console.log("Ajax request error: " + request.responseText);
						dialog.dialog('close');
					}
				});
			});
		},
		error: function (request, status, error) {
			console.log("Ajax request error: " + request.responseText);
		}
	});
}

function assignPrinters()
{
	printer_label_global = $('#printer_label').val();
	printer_bol_global = $('#printer_bol').val();
	console.log('Printers assigned.');
}

function logout()
{
	window.location.href = 'index.php?is_exit=1';
}

function homeDepotDC()
{
	var dialog = $('#dialog').dialog({
		autoOpen: false,
		height: 'auto',
		width: 'auto',
		resizable: false,
		autoResize: true,
		modal: true,
		title: 'Generate Label',
		overlay: {backgroundColor: "#000", opacity: 0.5},
	});
	dialog.dialog('close');


	$('#content').html("<p>Please enter PO:</p><input type='text' id='home_depot_dc' name='home_depot_dc'><br><br><button style='cursor: pointer;' id='print_home_depot_dc_label'>Display products</button>");
	$('#print_home_depot_dc_label').click(function () {
		var po = $('#home_depot_dc').val();
		po = encodeURIComponent(po);
		dialog.dialog('close');
		window.open('/shipping_module/print_products_hddc.php?po=' + po, '_blank');
	});
	dialog.dialog('open');

}

function reprintOrders(po)
{
	var dialog = $('#dialog').dialog({
		autoOpen: false,
		height: 'auto',
		width: 'auto',
		resizable: false,
		autoResize: true,
		modal: true,
		title: 'Reprint PDF',
		overlay: {backgroundColor: "#000", opacity: 0.5},
	});
	dialog.dialog('close');
	$.ajax({
		url: 'phpFunctions/functionsFindPDFs.php',
		type: 'POST',
		data: {
			po: po
		},
		complete: function (data) {
			console.log(data.responseText);
			productCodeText = data.responseText;
			productCode = JSON.parse(productCodeText);
			if (Object.prototype.toString.call(productCode) === "[object Array]") {

				var content = '';
				for (i = 0; i < productCode.length; i++)
				{
					path = decodeURIComponent(productCode[i].path);
					type = productCode[i].type;
					if (type == 'bol')
					{
						content += "<br><a href='" + path + "' target='_blank'>Bill Of Lading</a><br>";
					} else if (type == 'label')
					{
						content += "<br><a href='" + path + "' target='_blank'>Label</a><br>";
					}
                                         else if (type == 'flyer')
					{
						content += "<br><a href='" + path + "' target='_blank'>Flyer</a><br>";
					}
				}
				$('#content').html(content);
				dialog.dialog('open');
			} else
			{
				alert('Error. ' + data.responseText);
			}
		},
		error: function (request, status, error) {
			console.log("Ajax request error: " + request.responseText);
		}
	});
}

function asnPopup(po, id, obj, fedexMode, carrier, ShipAddress_Country, ShipAddress_State, ShipAddress_City, ShipAddress_PostalCode, ShipAddress_Addr1, ShipAddress_Addr2, ShipAddress_Addr3, FOB, display_popup, login, dealer, asn)
{
	if (!asn)
	{
		getASNDB(po, id, obj, fedexMode, carrier, ShipAddress_Country, ShipAddress_State, ShipAddress_City, ShipAddress_PostalCode, ShipAddress_Addr1, ShipAddress_Addr2, ShipAddress_Addr3, FOB, display_popup, login, dealer, asn);
	} else
	{
		var dialog = $('#dialog').dialog({
			autoOpen: false,
			height: 'auto',
			width: 'auto',
			resizable: false,
			autoResize: true,
			modal: true,
			title: 'Type ASN number.',
			overlay: {backgroundColor: "#000", opacity: 0.5},
		});
		if (asn === true)
			asn_value = '';
		else
			asn_value = asn;
		dialog.dialog('close');
		$('#content').html("<p>Please enter ASN:</p><input type='text' id='asn' name='asn' value='" + asn_value + "'><br><br><button style='cursor: pointer;' id='asn_button'>OK</button>");
		$('#asn_button').on('click', function () {
			if ($('#asn').val())
			{
				dialog.dialog('close');
				asn = $('#asn').val();
				$.ajax({
					url: 'phpFunctions/saveAsn.php',
					type: 'POST',
					data: {
						asn: asn,
						id: id,
						po: po
					},
					complete: function (data) {
						console.log(data.responseText);
						liftResi(po, id, obj, fedexMode, carrier, ShipAddress_Country, ShipAddress_State, ShipAddress_City, ShipAddress_PostalCode, ShipAddress_Addr1, ShipAddress_Addr2, ShipAddress_Addr3, FOB, display_popup, login, dealer, asn);
					},
					error: function (request, status, error) {
						console.log("Ajax request error: " + request.responseText);
					}
				});
			} else
				alert('Please enter ASN number.');
		});
		dialog.dialog('open');
	}
}

function getASNDB(po, id, obj, fedexMode, carrier, ShipAddress_Country, ShipAddress_State, ShipAddress_City, ShipAddress_PostalCode, ShipAddress_Addr1, ShipAddress_Addr2, ShipAddress_Addr3, FOB, display_popup, login, dealer, asn)
{
	$.ajax({
		url: 'phpFunctions/getAsn.php',
		type: 'POST',
		data: {
			id: id,
			po: po
		},
		complete: function (data) {
			console.log(data.responseText);
			productCodeText = data.responseText;
			productCode = JSON.parse(productCodeText);
			if (Object.prototype.toString.call(productCode) === "[object Object]") {
				asn = productCode.ASN;
			} else
			{
				asn = true;
			}
			asnPopup(po, id, obj, fedexMode, carrier, ShipAddress_Country, ShipAddress_State, ShipAddress_City, ShipAddress_PostalCode, ShipAddress_Addr1, ShipAddress_Addr2, ShipAddress_Addr3, FOB, display_popup, login, dealer, asn);
		},
		error: function (request, status, error) {
			console.log("Ajax request error: " + request.responseText);
			alert('Error. Can not get ASN from DB.');
		}
	});
}