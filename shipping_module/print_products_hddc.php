<?php
set_time_limit(1200);
session_start();
//order status history
require('phpFunctions/functionsExtr.php');
$path = $_SERVER['DOCUMENT_ROOT'];
require_once($path.'/db_connect/connect.php');
require_once($path.'/auth/auth.php');
 
$auth = new AuthClass();
 
if (isset($_POST["login"]) && isset($_POST["password"])) {
    if (!$auth->auth($_POST["login"], $_POST["password"])) {
        echo "<script>alert('Wrong login or password!')</script>";
    }
}
 
if (isset($_GET["is_exit"])) {
    if ($_GET["is_exit"] == 1) {
        $auth->out();
        header("Location: ?is_exit=0");
    }
}

if ($auth->isAuth()) {
if (isset($_GET['po']) && !empty($_GET['po']))
{
	$po = $_GET['po'];
	$po = trim($po);
	$conn = Database::getInstance()->dbc;
?>
<!doctype html>

<html lang="en">
<head>
  <meta charset="utf-8">
	<title>Print Labels</title>
	<link rel="stylesheet" href="Include/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="Include/bootstrap/css/bootstrap-theme.min.css">
	<script src="js/jquery-1.10.2.js"></script>
	<script src="Include/bootstrap/js/bootstrap.min.js"></script>
        <script>
            var printer_bol_global = false;
            var printer_label_global = false;
            var printer_barcode_global = false;
            var user_id = '<?php echo $_SESSION["id"] ?>';
            $(document).ready(function() {
                $.ajax({
                    url: "phpFunctions/getPrinters.php",
                    type: 'POST',
                    data: {
                            id: user_id
                    },
                    complete: function(data){
                        var res = JSON.parse(data.responseText);
                        printer_bol_global     = res[0]['printer_bol'];
                        printer_label_global   = res[0]['printer_hddc1'];
                        printer_barcode_global = res[0]['printer_hddc2'];
                    }
                });
            });
            
        </script>
        <script type="text/javascript" src="Include/qz/js/dependencies/rsvp-3.1.0.min.js"></script>
	<script type="text/javascript" src="Include/qz/js/dependencies/sha-256.min.js"></script>
	<script type="text/javascript" src="Include/qz/js/qz-tray.js"></script>
	<script type="text/javascript" src="js/print.js"></script>
        
	<style>
		#backHref {
			position: absolute;
			right: 1%;
			transition: 0.3s;
			top: 10%;
		}
		#backHref:hover {
			transform: scale(1.1);
		}
		#headerContainer {
			/*position: absolute;
			top: 0px;
			left: 0px;
			width: 90%;*/
			height: 50px;
			padding: 5px 20px;
			/*padding-bottom: 5px;*/
			/*padding-left: 10%;*/
			/*background: rgb(130, 167, 22);*/
			color: #367FBB;
			background: rgba(54, 127, 187, 0.1);
			-webkit-box-shadow: 0 1px 5px rgba(0, 0, 0, 0.25);
			-moz-box-shadow: 0 1px 5px rgba(0, 0, 0, 0.25);
			box-shadow: 0 1px 5px rgba(0, 0, 0, 0.25);
		}
		.product {
			float: right;
			margin-top: 10px;
			position: relative;
			padding-right: 40px;
		}
		#backHref img {
			margin-left: 10px;
		}
		#backHref img:hover {
			transform: scale(1.05); 
		}
		.product i {
			font-weight: bold;
		}
		.logo {
			width: 160px;
			float: left;
		}
		img {
			max-width: 100%;
			vertical-align: top;
		}

		.panel-title{
			text-align: center;
		}
                tr.tr_done {
                    background: #dfe;
                }
		td .order-supply{
			background: url('Include/pictures/supply.png');
			width: 32px;
			height: 32px;
			display: block;
		}
                td .order-supply2{
                            background: url('Include/pictures/negative.png');
                            width: 32px;
                            height: 32px;
                            display: block;
                        }
		</style>
		<script>
			var po_txn_arr = new Array();

			function generateAllDocs( po ) {
				if(confirm("This action will generate and replace existing files with new Labesl and Barcodes. Are you sure?")) {
					$( "#genallbtn" ).html( '<span style="display:inline-block; width:171px;"><img style="vertical-align: middle;" src="Include/gif/tiny_loading.GIF"/></span>' );
					$.post( "phpFunctions/functionsHomeDepotDCall.php"
						, { po: po }
						, function( data ) {
							if(data.indexOf('ERROR')+1) {
								alert(data);
							} else {
								for( n in po_txn_arr ) {
									checkPdf( '', po_txn_arr[n][0], po_txn_arr[n][1] );
								}
							}
							$( "#genallbtn" ).html( "Generate All Documents" );
						}
					);
				}
			}
			function generatePdf( type, po, tl ) {
				var po2 = po.replace( /\./g, '\\.' );
				if( type == 'label' || type == 'barcode' ) {
					if( $("#open_"+type+"_"+po2+"_"+tl).is(":visible") == false ) {
						var tmp = $( "#gen_"+type+"_"+po2+"_"+tl+" button" ).html();
						$( "#gen_"+type+"_"+po2+"_"+tl+" button" ).html( '<span style="display:inline-block; width:'+(type=='label'?'96':'117')+'px;"><img style="vertical-align: middle;" src="Include/gif/tiny_loading.GIF"/></span>' );

						$.post( "phpFunctions/functionsHomeDepotDC"+type+"s.php"
							, { po: po, txnlineid: tl }
							, function( data ) {
								if(data.indexOf('ERROR')+1) {
									alert(data);
								} else {
									checkPdf( type, po, tl );
								}
								$( "#gen_"+type+"_"+po2+"_"+tl+" button" ).html( tmp );
							}
						);
					} else if(confirm("This action will create new QR and Barcodes. Are you shure to do this?")) {
						var tmp = $( "#gen_"+type+"_"+po2+"_"+tl+" button" ).html();
						$( "#gen_"+type+"_"+po2+"_"+tl+" button" ).html( '<span style="display:inline-block; width:'+(type=='label'?'96':'117')+'px;"><img style="vertical-align: middle;" src="Include/gif/tiny_loading.GIF"/></span>' );

						$.post( "phpFunctions/functionsHomeDepotDC"+type+"s.php"
							, { po: po, txnlineid: tl }
							, function( data ) {
								if(data.indexOf('ERROR')+1) {
									alert(data);
								} else {
									checkPdf( type, po, tl );                                                                            
								}
								$( "#gen_"+type+"_"+po2+"_"+tl+" button" ).html( tmp );
							}
						);
					}
					
				} else if( type == 'bol' ) {
					if( $("#open_"+type+"_"+po2).is(":visible") == false ) {
						var tmp = $( "#gen_"+type+"_"+po2+" button" ).html();
						$( "#gen_"+type+"_"+po2+" button" ).html( '<span style="display:inline-block; width:93px;"><img style="vertical-align: middle;" src="Include/gif/tiny_loading.GIF"/></span>' );

						$.post( "phpFunctions/functionsHomeDepotDC"+type+".php"
							, { po: po }
							, function( data ) {
								if(data.indexOf('ERROR')+1) {
									alert(data);
								} else {
									checkPdf( type, po, tl );
								}
								$( "#gen_"+type+"_"+po2+" button" ).html( tmp );
							}
						);

					} else if(confirm("This action will create new BOL. Are you shure to do this?")) {
						var tmp = $( "#gen_"+type+"_"+po2+" button" ).html();
						$( "#gen_"+type+"_"+po2+" button" ).html( '<span style="display:inline-block; width:93px;"><img style="vertical-align: middle;" src="Include/gif/tiny_loading.GIF"/></span>' );

						$.post( "phpFunctions/functionsHomeDepotDC"+type+".php"
							, { po: po }
							, function( data ) {
								if(data.indexOf('ERROR')+1) {
									alert(data);
								} else {
									checkPdf( type, po, tl );
								}
								$( "#gen_"+type+"_"+po2+" button" ).html( tmp );
							}
						);
					}
				}
			}
			//merge conflict test
			function checkPdf( type, po, tl ) {
				var po2 = po.replace( /\./g, '\\.' );
					if( type == 'bol' ) {
						$.post( "phpFunctions/functionsHomeDepotDCpdfchecker.php"
							, { po: po, type: type }
							, function( data ) {								
								var result = $.parseJSON(data);
								if( result.bol ) {
									$( "#open_bol_"+po2 ).show();
									$( "#open_bol_"+po2 ).attr('href', result.bol);
									$( "#print_bol_"+po2 ).show();
									$( "#print_bol_"+po2 ).click(function(){
										if(qz.websocket.isActive()) {
												printOn(printer_bol_global, result.bol);
										} else { alert('Printers are not connected.'); }
									});    
								}
							}
						);
					} else {
						$.post( "phpFunctions/functionsHomeDepotDCpdfchecker.php"
							, { po: po, txnlineid: tl }
							, function( data ) {
								var result = $.parseJSON(data);
								if( result.label ) {
									$( "#open_label_"+po2+"_"+tl ).show();
									$( "#open_label_"+po2+"_"+tl ).attr('href', result.label);
									$( "#print_label_"+po2+"_"+tl ).show();
									$( "#print_label_"+po2+"_"+tl ).click(function(){
										if(qz.websocket.isActive()) {
												printOn(printer_label_global, result.label);
										} else { alert('Printers are not connected.'); }
									});
								} else {
									$( "#open_label_"+po2+"_"+tl ).hide();
									$( "#open_label_"+po2+"_"+tl ).attr('href', '#');

									$( "#print_label_"+po2+"_"+tl ).hide();
								}
								if( result.barcode ) {
									$( "#open_barcode_"+po2+"_"+tl ).show();
									$( "#open_barcode_"+po2+"_"+tl ).attr('href', result.barcode);

									$( "#print_barcode_"+po2+"_"+tl ).show();
									$( "#print_barcode_"+po2+"_"+tl ).click(function(){
										if(qz.websocket.isActive()) {
												printOn(printer_barcode_global, result.label);
										} else { alert('Printers are not connected.'); }
									});
								} else {
									$( "#open_barcode_"+po2+"_"+tl ).hide();
									$( "#open_barcode_"+po2+"_"+tl ).attr('href', '#');

									$( "#print_barcode_"+po2+"_"+tl ).hide();
								}
							}
						);    
					}
			}
			function checkDone( obj, po, tl ) {
				var isChecked = 0;
				var po2 = po.replace( /\./g, '\\.' );				
				if( $(obj).is(':checked') ) isChecked = 1;


				$.post( "phpFunctions/functionsHomeDepotDCDoneCheck.php"
					  , { po: po, txnlineid: tl, check: isChecked }
					  , function( data ) {
						  if( data == '1' ) {
							  if( isChecked ) $('tr.line_'+po2+'_'+tl).addClass('tr_done')
							  else $('tr.line_'+po2+'_'+tl).removeClass('tr_done')
						  } else alert( data );
					  }
				);
			}
		</script>
</head>

<body>
		<div id="headerContainer">
			<div class="logo"><img src="../Include/img/logo.png"></div>
			<div class="product">
				<i>Print Labels. PO: <?php echo $po; ?></i>
			</div>
		</div>
		<br>
		<br>
		<a href="../pallet_preload/index.php?po=<?php echo $po; ?>" target="_blank"><button>Pallet Preload</button></a>
		<!--<a href="../truck_load/index.php?po=<?php echo $po; ?>" target="_blank"><button>Truck Load</button></a>-->
		<a onclick="connectQZ();"><button>Connect Printers</button></a>                
		<a onclick="generateAllDocs( '<?php echo $po; ?>' )"><button id="genallbtn">Generate All Documents</button></a>
                
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                
                <a id="<?php echo 'gen_bol_'.$po ?>"   onclick="generatePdf( 'bol', '<?php echo $po; ?>', '' )"><button id="genbol">Generate BOL</button></a>
                <a id="<?php echo 'open_bol_'.$po ?>"  href="#" target="_blank" style="display:none;"><button id="openbol">Open BOL</button></a>
                <a id="<?php echo 'print_bol_'.$po ?>" style="display:none;"><button id="printbol">Print BOL</button></a>
		<script>
                        $(document).ready(function() {
                                checkPdf( 'bol', '<?php echo $po; ?>', '' );
                        });
                </script>
		<br>
		<br>
		<h3>Products in order</h3>

                    <?php
$run_sp = "execute [".DATABASE31."].[dbo].[hd_dc_sync] @PO = '".$po."'";
$query_groupdetail_DL= "
SELECT * FROM
(
        SELECT 
             [groupdetail].[TxnLineID]
            ,[groupdetail].[ItemGroupRef_FullName]
            ,[groupdetail].[Prod_desc]
            ,[groupdetail].[Quantity]
            ,[DL_valid_SKU].[SHIP_METHOD]
			,[inventory_allocation_per_group].[Date] as [Supply]
			,ISNULL([hddc_checker].[status], 0) as [Done]
	FROM [dbo].[groupdetail]
	LEFT JOIN [shiptrack] ON [groupdetail].[IDKEY] = [shiptrack].[TxnID]
	LEFT JOIN [DL_valid_SKU] ON [groupdetail].[ItemGroupRef_FullName] = [DL_valid_SKU].[QB_SKU]
    LEFT JOIN [hddc_checker] ON [hddc_checker].[TxnLineID] = [groupdetail].[TxnLineID] AND [hddc_checker].[PONumber] = [shiptrack].[PONumber]
	LEFT JOIN [dbo].[inventory_allocation_per_group] ON [inventory_allocation_per_group].[ItemGroupRef_FullName] = [groupdetail].[ItemGroupRef_FullName]
        AND [shiptrack].[PONumber] = [inventory_allocation_per_group].[PONumber]
	WHERE [shiptrack].[PONumber] = '".$po."'
) a
GROUP BY [TxnLineID], [ItemGroupRef_FullName], [Prod_desc], [Quantity], [SHIP_METHOD], [Supply], [Done];";
		
$res_sp = exec_query($conn, $run_sp);
$result_groupdetail_DL = exec_query($conn, $query_groupdetail_DL);
$result_groupdetail_DL = $result_groupdetail_DL->fetchAll(PDO::FETCH_ASSOC);

if ($result_groupdetail_DL)
{
?>
		<table data-toggle='table' class='table table-bordered display'>
			<thead>
				<tr class='info'>
					<?php
					foreach ($result_groupdetail_DL[0] as $key => $value)
					{
						echo '<th>'.$key.'</th>';
					}
					echo '<th>Generate Label</th>';
					echo '<th>Generate Barcode</th>';
					/*echo '<th>Generate Picking/Packing List</th>';*/
					?>
				</tr>
<!-- 				<tfoot>
					<?php
					foreach ($result_groupdetail_DL[0] as $key => $value)
					{
						echo '<th>'.$key.'</th>';
					}
					?>
				</tfoot> -->
			</thead>
			<tbody>
				<?php
				foreach ($result_groupdetail_DL as $key1 => $result)
				{
					echo '<tr class="line_'.$po.'_'.$result['TxnLineID'].' '.($result['Done']==1?'tr_done':'').'" >';
					foreach ($result as $key => $value)
					{
						if( $key == 'Done' ) {

							echo '<td><input onchange="checkDone( this, \''.$po.'\', \''.$result['TxnLineID'].'\' )" type="checkbox" '.($value==1?'checked':'').'></td>';

						} else if( $key == 'Supply' ) {

							if( $value ) {
								echo '<td><i class="order-supply2" title="Not enough items for this product! Calculated date: '.$value.'"></i></td>';
							} else {
								echo '<td><i></i></td>';
							}

						} else {

							if (empty($value) && $value != '0') echo '<td></td>'; else echo '<td>'.$value.'</td>';

						}
                                            
						
					}
					
					?>
					<td width="184">
						<div>
							<a class="hddc_label_gen" id="<?php echo 'gen_label_'.$po.'_'.$result['TxnLineID']; ?>" onclick="generatePdf( 'label', '<?php echo $po; ?>', '<?php echo $result['TxnLineID']; ?>' )"><button>Generate Flyer</button></a>
							<a class="hddc_label_opn" id="<?php echo 'open_label_'.$po.'_'.$result['TxnLineID']; ?>" href="#" target="_blank" style="display:none;"><button>Open</button></a>
							<a class="hddc_label_prn" id="<?php echo 'print_label_'.$po.'_'.$result['TxnLineID']; ?>" style="display:none;"><button>Print</button></a>
						</div>
					</td>
					<td width="205">
						<div>
							<a class="hddc_barcode_gen" id="<?php echo 'gen_barcode_'.$po.'_'.$result['TxnLineID']; ?>" onclick="generatePdf( 'barcode', '<?php echo $po; ?>', '<?php echo $result['TxnLineID']; ?>' )"><button>Generate Barcode</button></a>
							<a class="hddc_barcode_opn" id="<?php echo 'open_barcode_'.$po.'_'.$result['TxnLineID']; ?>" href="#" target="_blank" style="display:none;"><button>Open</button></a>
							<a class="hddc_barcode_prn" id="<?php echo 'print_barcode_'.$po.'_'.$result['TxnLineID']; ?>" style="display:none;"><button>Print</button></a>
						</div>
					</td>
					<script>
						$(document).ready(function() {
							checkPdf( '', '<?php echo $po; ?>', '<?php echo $result['TxnLineID']; ?>' );
							po_txn_arr.push( ['<?php echo $po; ?>', '<?php echo $result['TxnLineID']; ?>'] );
						});
					</script>
					<?php

							/*echo '<td><a href="phpFunctions/functionsHomeDepotDCpickinglist.php?po='.$po.'&txnlineid='.$result['TxnLineID'].'" target="_blank"><button>Generate Picking/Packing List</button></a></td>';*/
					echo '</tr>';
				}
				?>
			</tbody>
		</table>
						<?php
} else echo "There is no information about products in this order.";
		?>
 
</body>
</html>
<?php
}
$conn = null;
} else {
?>
<div style="position: relative; top: 110px; width: 100%;">
<div style="
position: relative;
width: 330px;
height: 160px;
left: 50%;
margin-left: -160px;
padding: 10px;
background: #367FBB;
">
<form method="post" action="">
<fieldset>
    <p><label class="field" for="login">Login:</label><input class="logininput" type="text" name="login"
    value="<?php echo (isset($_POST["login"])) ? $_POST["login"] : null; ?>" /></p>
    <p><label class="field" for="password">Password:</label><input class="logininput" type="password" name="password" value="" /></p>
	<p><label class="field" for="password"> </label><input class="loginsubmit" type="submit" value="login" /></p>
</fieldset>
</form>
</div>
<div style="bottom: 2px; width: 100%; text-align:center; margin: 10px 0 0 0;"><p>Copyright © 2015 Dreamline</p></div>
</div>
<?php 
$conn = null;
}


function exec_query($conn, $query)
{
	try{
		$result = $conn->prepare($query);
		$result->execute();
		return $result;
	}
	catch(PDOException $e)
	{
		print_r($e->getMessage());
	}
}
?>