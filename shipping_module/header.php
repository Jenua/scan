<?php
session_start();
$path = $_SERVER['DOCUMENT_ROOT'];
require_once($path.'/auth/auth.php');
$auth = new AuthClass();

$page_url = getRequestURL();
$page_delimeter = "?";
if( stripos($page_url, '?') !== false ) {
        $page_delimeter = "&";
}

if (isset($_GET["is_exit"])) {
    if ($_GET["is_exit"] == 1) {
        $auth->out();        
        if( stripos($page_url, 'is_exit') !== false ) {
            $search = array( '?is_exit=0', '?is_exit=1', '&is_exit=0', '&is_exit=1' );
            $page_url = str_ireplace($search, '', $page_url);
        }          
        header("Location: ".$page_url);
    }
}

if( !isset( $header_name ) ) {
    $header_name = "Shipping Portal";
}

if ( !$auth->isAuth() ) { ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title><?php echo $header_name; ?> </title>
<link rel="stylesheet" type="text/css" href="/Include/css/style.css" media="all"/>
</head>
<body>
    <div style="position: relative; top: 110px; width: 100%;">
    <div style="position: relative; width: 330px; height: 160px; left: 50%; margin-left: -160px; padding: 10px; background: #367FBB;">
    <form method="post" action="">
    <fieldset>
        <p><label class="field" for="login">Login:</label><input class="logininput" type="text" name="login"
        value="<?php echo (isset($_POST["login"])) ? $_POST["login"] : null; ?>" /></p>
        <p><label class="field" for="password">Password:</label><input class="logininput" type="password" name="password" value="" /></p>
            <p><label class="field" for="password"> </label><input class="loginsubmit" type="submit" value="login" /></p>
    </fieldset>
    </form>
    </div>
    <div style="bottom: 2px; width: 100%; text-align:center; margin: 10px 0 0 0;"><p>Copyright &copy; <?php echo date('Y');?> Dreamline</p></div>
    </div>
    <?php
    if (isset($_POST["login"]) && isset($_POST["password"])) {
        if (!$auth->auth($_POST["login"], $_POST["password"])) {
            echo "<script>alert('Wrong login or password!')</script>";
        }
        else
            echo "<script>window.location.href = '" . getRequestURL() . "'</script>";
    } ?>
<?php
$conn = null;
die();?>
</body>
</html>
<?php
}
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <script type="text/javascript" src="/shipping_module/js/jquery.js"></script>
    <script type="text/javascript" src="/shipping_module/js/jquery-ui.min-1.11.4.js"></script>
    <link rel="stylesheet" href="/shipping_module/Include/css/style.css" type="text/css" />

    <link rel="stylesheet" type="text/css" href="/shipping_module/Include/css/header.css" media="all"/>
    <script type="text/javascript" src="/shipping_module/js/header.js"></script>
    <script src="/shipping_module/Include/bootstrap-validator-master/js/validator.js"></script>