<?php
date_default_timezone_set('America/New_York');  //timezone
$path = $_SERVER['DOCUMENT_ROOT'];  //get root path
echo '<br><h1>Cleaning Script</h1><br>';

//SETTINGS
$settings = [
    [
        'name' => 'barcodes',
        'directory' => $path.'/shipping_module/Images/',
        'limit' => 50000   //Images folder limit
    ],
    [
        'name' => 'labels',
        'directory' => $path.'/shipping_module/Pdf/',
        'limit' => 50000   //Pdf folder limit
    ],
    [
        'name' => 'bols',
        'directory' => $path.'/shipping_module/PdfBill/',
        'limit' => 50000   //PdfBill folder limit
    ],
    [
        'name' => 'invoices',
        'directory' => $path.'/shipping_module/PdfInvoice/',
        'limit' => 50000   //PdfInvoice folder limit
    ]
];

$log_file_path = $path.'/shipping_module/log/log.txt';  //set log file path
//SETTINGS

//Main sequence
    display_date_message('Cleaning script started.', $log_file_path);
    
    foreach ($settings as $setting)
    {
        $removed_files_counter = delete_files($setting['directory'], $setting['limit']);
        display_date_message($removed_files_counter.' '.$setting['name'].' removed. Limit: '.$setting['limit'].'. Path: '.$setting['directory'].'.', $log_file_path);
    }

   display_date_message('Cleaning script finished.', $log_file_path);
//Main sequence



//Functions
function delete_files($directory, $anof)    //Remove from $directory folder oldest files which exceed the limit $anof
{
    $removed_files_counter = 0;
    //$anof - allowable number of files
    // Grab all files from the desired folder
    $files = @glob( $directory.'*.*' );

    // Sort files by modified time, latest to earliest
    // Use SORT_ASC in place of SORT_DESC for earliest to latest
    array_multisort(
    @array_map( 'filemtime', $files ),
    SORT_NUMERIC,
    SORT_ASC,
    $files
    );
    $cf = count($files);
    if($cf > $anof)
    {
        $ftdn = $cf - $anof;
        for ($i = 0; $i <= $ftdn; $i++)
        {
            @unlink($files[$i]);
            $removed_files_counter++;
        }
    }
    return $removed_files_counter;
}

function logToFile($log_file_path, $message)
{
	$fd = fopen($log_file_path, "a");
	fwrite($fd, $message . "\n");
	fclose($fd);
}

function display_date_message($message, $log_file_path)
{
    $date = date('l jS \of F Y h:i:s A');
    echo '<br>'.$date.' <strong>Message: '.$message.'</strong>';
    logToFile($log_file_path, $date.' Message: '.$message);
}
//Functions