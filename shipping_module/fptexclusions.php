<?php
set_time_limit(120);
session_start();
//order status history
require('phpFunctions/functionsExtr.php');
$path = $_SERVER['DOCUMENT_ROOT'];
require_once($path.'/db_connect/connect.php');
require_once($path.'/auth/auth.php');
require_once($path.'/shipping_module/Include/Twig/Autoloader.php');

function exec_query($conn, $query) {
	try {
		$result = $conn->prepare($query);
		$result->execute();
		return $result;
	}
	catch(PDOException $e) {
		print_r($e->getMessage());
	}
}
function parseItemFullName( $name ) {
    return end( explode(":", $name) );
}




Twig_Autoloader::register();
$loader = new Twig_Loader_Filesystem('templates/Pages');
$twig = new Twig_Environment($loader, array(
	'cache'       => 'compilation_cache',
	'auto_reload' => true
));

$auth = new AuthClass();
if( isset($_POST['login']) && isset($_POST["password"]) ) {
	if( !$auth->auth($_POST["login"], $_POST["password"]) ) {		
		$loginName = $_POST["login"] ? $_POST["login"] : '';
		$wrongCredentials = $twig->render('loginPage.html', array('login' => $loginName, 'errmessage' => 'Error: wrong login or password'));
		die($wrongCredentials);		
	}
}
if( isset($_GET["is_exit"]) ) {
    if ($_GET["is_exit"] == 1) {
        $auth->out();
        header("Location: ?is_exit=0");
    }
}
if( !$auth->isAuth() ) {	
	$noAuth = $twig->render('loginPage.html', array('login' => '', 'errmessage' => ''));
	die($noAuth);
}
// DB Connect
$conn = Database::getInstance()->dbc;
$getRelatedQuery = "SELECT
						os.ID,
						os.PONumber,
						os.RefNumber
					FROM [orders_statuses] os
					INNER JOIN [shiptrack] AS st ON st.[PONumber] = os.[PONumber] AND st.[RefNumber] = os.[RefNumber]
					WHERE os.[Type] = 1 AND os.[isActive] = 0 ;";
					
$relatedResult = exec_query($conn, $getRelatedQuery);
$relatedArray  = $relatedResult->fetchAll(PDO::FETCH_ASSOC);

$fptRelated = $twig->render('fptExclusions.html', array( 'orders' => $relatedArray ));
die($fptRelated);


