<?php
set_time_limit(120);
session_start();
//order status history
require('phpFunctions/functionsExtr.php');
$path = $_SERVER['DOCUMENT_ROOT'];
require_once($path.'/db_connect/connect.php');
require_once($path.'/auth/auth.php');
require_once($path.'/shipping_module/Include/Twig/Autoloader.php');

function exec_query($conn, $query) {
	try {
		$result = $conn->prepare($query);
		$result->execute();
		return $result;
	}
	catch(PDOException $e) {
		print_r($e->getMessage());
	}
}
function parseItemFullName( $name ) {
    return end( explode(":", $name) );
}
function parsePalletQTY( $palletString ) {
	$palletAssoc = [];
	if( $palletString ) {
		$palletPairs  = explode( ',', $palletString );	
		foreach( $palletPairs as $palletPair ) {
			if( $palletPair ) {
				$palletParsed = explode( '=', $palletPair );
				if( !empty($palletParsed[0]) && !empty($palletParsed[1]) ) {
					$palletAssoc[ $palletParsed[0] ] = $palletParsed[1];
				}
			}
		}
	}
	return $palletAssoc;
}
function getDealerType( $conn, $type ) {
	$query   = "SELECT TOP 1 * FROM [dc_type] WHERE id = ".$conn->quote( $type );
	$results = exec_query($conn, $query);
	$dealer  = $results->fetch(PDO::FETCH_ASSOC);
	return $dealer;
}


Twig_Autoloader::register();
$loader = new Twig_Loader_Filesystem('templates/Pages');
$twig = new Twig_Environment($loader, array(
	'cache'       => 'compilation_cache',
	'auto_reload' => true
));

$auth = new AuthClass();
if( isset($_POST['login']) && isset($_POST["password"]) ) {
	if( !$auth->auth($_POST["login"], $_POST["password"]) ) {		
		$loginName = $_POST["login"] ? $_POST["login"] : '';
		$wrongCredentials = $twig->render('loginPage.html', array('login' => $loginName, 'errmessage' => 'Error: wrong login or password'));
		die($wrongCredentials);		
	}
}
if( isset($_GET["is_exit"]) ) {
    if ($_GET["is_exit"] == 1) {
        $auth->out();
        header("Location: ?is_exit=0");
    }
}
if( !$auth->isAuth() ) {	
	$noAuth = $twig->render('loginPage.html', array('login' => '', 'errmessage' => ''));
	die($noAuth);
}
if ( empty($_GET['po']) )
{
	$noAuth = $twig->render('fatalErorr.html', array('error' => 'Error: No PONumber'));
	die($noAuth);
}

$po = $_GET['po'];
$po = trim($po);

// DB Connect
$conn = Database::getInstance()->dbc;

$type   = empty($_REQUEST['t']) ? '1' : $_REQUEST['t'];
$dealer = getDealerType( $conn, $type );


//GET Pallet Data = PALLET - WEIGHT
if( $dealer['isPallet'] ) {
	$preloadQty    = "	case
							when [pallet_preload].[QUANTITY] is null then 0
							else CAST([pallet_preload].[QUANTITY]  as int) 
						end as [distributed_qty]";
	$preloadPallet = "	,[pallet_preload].[PALLET]";
	$preloadJoin1  = "	LEFT JOIN [dbo].[pallet_preload] ON [DL_valid_SKU].[UPC] = [pallet_preload].[UPC] AND [shiptrack].[PONUMBER] = [pallet_preload].[PONUMBER] AND [shiptrack].[REFNUMBER] = [pallet_preload].[REFNUMBER]";
} else {
	$preloadQty    = "	[groupdetail].[Quantity] AS [distributed_qty]";
	$preloadPallet = "	,'Order' AS [PALLET]";
	$preloadJoin1  = "";
}

$palletQuery = "SET NOCOUNT ON
				BEGIN

				DECLARE @Weights TABLE
				(
				   [GroupIDKEY] varchar(255),
				   [OneQtyWeight] int
				)
				;

				INSERT INTO @Weights (
					GroupIDKEY,
					OneQtyWeight
					)
				SELECT
					 distinct [linedetail].[GroupIDKEY]
					,sum(CAST(dbo.udf_GetNumeric([linedetail].[CustomField9]) AS INT)) over  (partition by [linedetail].[GroupIDKEY])
				FROM [dbo].[linedetail]
				INNER JOIN [dbo].[groupdetail] ON [groupdetail].[TxnLineID] = [linedetail].[GroupIDKEY]
				INNER JOIN [dbo].[shiptrack] ON [shiptrack].[TxnID] = [groupdetail].[IDKEY]
				WHERE [shiptrack].[PONumber] = ".$conn->quote($po)."
				AND [linedetail].[CustomField9] is not null
				AND [linedetail].[CustomField9] != '0'
				;
				END
				;

				SELECT
					[PALLET] as [Pallet]
					,SUM([OneQtyWeight]*[distributed_qty]) as [Weight]	
				FROM
				(
					SELECT 
						".$preloadQty."
						".$preloadPallet."
						,pw.[OneQtyWeight]
					FROM [dbo].[groupdetail]
					LEFT JOIN [shiptrack] ON [groupdetail].[IDKEY] = [shiptrack].[TxnID]
					LEFT JOIN [dbo].[shipping_measurements] ON [shipping_measurements].[SKU] = [groupdetail].[ItemGroupRef_FullName]
					LEFT JOIN [dbo].[DL_valid_SKU] ON [groupdetail].[ItemGroupRef_FullName] = [DL_valid_SKU].[QB_SKU]
					".$preloadJoin1."
					LEFT JOIN @Weights AS pw ON [groupdetail].[TxnLineID] = pw.[GroupIDKEY]
					WHERE [shiptrack].[PONumber] = ".$conn->quote($po)."
					AND [DL_valid_SKU].[UPC] IS NOT NULL
				) a
				WHERE [PALLET] IS NOT NULL
				GROUP BY [PALLET]
				ORDER BY LEN([PALLET]), [PALLET]
				;";
$palletResult = exec_query($conn, $palletQuery);
$palletArray  = $palletResult->fetchAll(PDO::FETCH_ASSOC);
//GET Truck Data
if( $dealer['isPallet'] ) {
	$truckQuery  = "SELECT
						[Truck]
						,COUNT([PALLET]) as [Pallets]
					FROM [truck_preload] WHERE [PONumber] = ".$conn->quote($po)."
					GROUP BY [Truck]
					ORDER BY [Truck];";
	$truckResult = exec_query($conn, $truckQuery);
	$truckArray  = $truckResult->fetchAll(PDO::FETCH_ASSOC);
} else {
	$truckArray = array( array("Truck" => "Order", "Pallets" => 1) );
}
//GET Items Data
$itemsQuery =  "SELECT
					 [linedetail].[TxnLineID]
					,[linedetail].[GroupIDKEY]
					,[linedetail].[ItemRef_FullName] as [FullName]
					,[linedetail].[Quantity]	
				FROM [linedetail]
				JOIN [groupdetail] ON [linedetail].[GroupIDKEY] = [groupdetail].[TxnLineId]
				JOIN [shiptrack] ON [groupdetail].[IDKEY] = [shiptrack].[TxnID]
				WHERE [shiptrack].[PONumber] = ".$conn->quote($po)."
				AND	(
							[linedetail].[ItemRef_FullName] not like '%Price-Adjustment%' 
						AND [linedetail].[ItemRef_FullName] not like '%Freight%'
						AND [linedetail].[ItemRef_FullName] not like '%warranty%' 
						AND [linedetail].[ItemRef_FullName] is not NULL
						AND [linedetail].[ItemRef_FullName] not like '%Subtotal%'
						AND [linedetail].[ItemRef_FullName] not like '%IDSC-10%'
						AND [linedetail].[ItemRef_FullName] not like '%DISCOUNT%'
						AND [linedetail].[ItemRef_FullName] not like '%SPECIAL ORDER4%'
						AND [linedetail].[ItemRef_FullName] not like '%Manual%'
					)
				;";
$itemsResult = exec_query($conn, $itemsQuery);
$itemsArray  = $itemsResult->fetchAll(PDO::FETCH_ASSOC);
$tempArray   = [];
foreach( $itemsArray as $item ) {
	if( !isset($tempArray[$item['GroupIDKEY']]) ) {
		$tempArray[$item['GroupIDKEY']] = [];
	}
	$item['FullName'] = parseItemFullName( $item['FullName'] );
	$tempArray[$item['GroupIDKEY']][] = $item;
}
$itemsArray = $tempArray;
unset( $tempArray );


if( $dealer['isPallet'] ) {
	$preloadJoin2  = "	LEFT JOIN [pallet_preload] ON [pallet_preload].[UPC] = [DL_valid_SKU].[UPC] AND [pallet_preload].[PONUMBER] = [shiptrack].[PONumber]";
	$preloadSelect = "	,ISNULL(SUM(CAST([pallet_preload].[QUANTITY] AS INT)) OVER (partition by [pallet_preload].[UPC]), 0) AS [Preload]
						,[Pallet_Distribution]";
	$preloadApply  = "	CROSS APPLY
						(

							SELECT PALLET + '=' + QUANTITY + ','
							FROM [pallet_preload] as internal	
							WHERE [internal].[UPC] = [pallet_preload].[UPC]
							AND [PONumber] = ".$conn->quote($po)."
							FOR XML PATH('')
						) pre_trimmed (Pallet_Distribution)";	
} else {
	$preloadJoin2  = "";
	$preloadSelect = "	,ISNULL([groupdetail].[Quantity], 0) AS [Preload]
						,'Order' AS  [Pallet_Distribution]";
	$preloadApply  = "";
}
//GET Product Data
$productQuery = "
				SELECT * FROM
				(
					SELECT 
						 [groupdetail].[TxnLineID]
						,[groupdetail].[ItemGroupRef_FullName] as [GroupSKU]
						,[groupdetail].[Prod_desc]
						,ISNULL([groupdetail].[Quantity], 0) as [Quantity]
						,[DL_valid_SKU].[SHIP_METHOD]
						,ISNULL([hddc_checker].[status], 0) as [Done]
						,ISNULL([hddc_checker].[discontinued], 0) as [Discontinued]
						".$preloadSelect."
						,[inventory_allocation_per_group].[Date] as [Supply]
						,CASE
							WHEN [groupdetail].[ItemGroupRef_FullName] LIKE 'G[DE0-9]%'
							THEN 1
							ELSE 0
						END AS [PartsOrder]
					FROM [dbo].[groupdetail]
					LEFT JOIN [shiptrack] ON [groupdetail].[IDKEY] = [shiptrack].[TxnID]
					LEFT JOIN [DL_valid_SKU] ON [groupdetail].[ItemGroupRef_FullName] = [DL_valid_SKU].[QB_SKU] AND [DL_valid_SKU].[UPC] IS NOT NULL
					LEFT JOIN [hddc_checker] ON [hddc_checker].[TxnLineID] = [groupdetail].[TxnLineID] AND [hddc_checker].[PONumber] = [shiptrack].[PONumber]
					".$preloadJoin2."
					LEFT JOIN [inventory_allocation_per_group] ON [inventory_allocation_per_group].[ItemGroupRef_FullName] = [groupdetail].[ItemGroupRef_FullName]
															  AND [shiptrack].[PONumber] = [inventory_allocation_per_group].[PONumber]
					".$preloadApply."
					WHERE [shiptrack].[PONumber] = ".$conn->quote($po)."
				) a
				WHERE [Quantity] > 0
				GROUP BY [TxnLineID], [GroupSKU], [Prod_desc], [Quantity], [SHIP_METHOD], [Supply], [Done], [Discontinued], [Preload], [Pallet_Distribution], [PartsOrder]
				ORDER BY [PartsOrder], [SHIP_METHOD];";
				
$productResult = exec_query($conn, $productQuery);
$productArray  = $productResult->fetchAll(PDO::FETCH_ASSOC);
for( $i=0; $i<count($productArray); $i++ ) {
	$productArray[$i]['Pallet_Distribution'] = parsePalletQTY( $productArray[$i]['Pallet_Distribution'] );
}

//die( "<pre>".print_r($productArray,true)."</pre>" );

$printLabels = $twig->render('printLabels.html', array(	'PONumber' => $po,
														'user_id'  => $_SESSION["id"],
														'pallets'  => $palletArray,
														'trucks'   => $truckArray,
														'products' => $productArray,
														'items'    => $itemsArray,
														'dealer'   => $dealer
													));
die($printLabels);