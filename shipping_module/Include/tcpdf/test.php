<?php
require_once('tcpdf.php');

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

$pdf->SetPrintHeader(false);
$pdf->SetPrintFooter(false);
$pdf->SetMargins(5, 5);
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
$pdf->SetAutoPageBreak(TRUE, 1);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}


// set font
$pdf->SetFont('helvetica', 'B', 20);

// add a page
$pdf->AddPage('P', array(102, 153), false, false);
$pdf->SetFont('helvetica', '', 11);

// Table with rowspans and THEAD
$tbl = <<<EOD
<table style="border-bottom: 2px solid #000000;" cellpadding="6">
	<tr>
		<td width="30%"><font size="12">FROM:</font></td>
		<td width="70%">
			<font size="12">Dreamline<br>
			75 Hawk rd<br>
			Warminster, PA 18947</font>
		</td>
	</tr>
</table><br>

<table cellpadding="6">
	<tr>
		<td width="20%"><font size="12">TO:</font></td>
		<td width="80%">
			<font size="12">FERGUSON ENTERPRISES INC #369<br>
			4109 POPLAR SPRINGS ROAD, SUITE 100<br>
			ELKIN, NC 28621<br><br><br></font>
		</td>
	</tr>
</table><br>

<table style="border-bottom: 2px solid #000000; border-top: 2px solid #000000;" cellpadding="6">
	<tr>
		<td width="50%" style="border-right: 2px solid #000000;">
			Carrier name:<br>
			UPS GROUND SERVICE<br>
		</td>
		<td width="50%" style="border-left: 2px solid #000000;">
			
		</td>
	</tr>
</table><br>

<table style="border-bottom: 2px solid #000000; border-top: 2px solid #000000;" cellpadding="6">
	<tr>
		<td width="35%">PO Number:</td>
		<td width="65%">
			PO #<br>
		</td>
	</tr>
</table><br>

<table cellpadding="6">
	<tr>
		<td align="center"><br><br><img width="200px" src="../../Images/code_128111111.png"></td>
	</tr>
	<tr>
		<td align="center"><br>BOL Number: 111111</td>
	</tr>
</table>
EOD;

$pdf->writeHTML($tbl, true, false, false, false, '');

//Close and output PDF document
$pdf->Output('example_048.pdf', 'F');

?>