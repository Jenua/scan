<?php
$classFile = 'BCGpdf417.barcode2d.php';
$className = 'BCGpdf417';
$baseClassFile = 'BCGBarcode2D.php';
$codeVersion = '5.1.0';

function customSetup($barcode, $get) {
    if (isset($get['column'])) {
        $barcode->setColumn(intval($get['column']));
    }
    if (isset($get['errorlevel'])) {
        $barcode->setErrorLevel(intval($get['errorlevel']));
    }
    if (isset($get['quietzone'])) {
        $barcode->setQuietZone($get['quietzone'] === '1' ? true : false);
    }
    if (isset($get['compact'])) {
        $barcode->setCompact($get['compact'] === '1' ? true : false);
    }
}
?>