<?php
$header_name = "Advanced Search";
require_once( "../shipping_module/header.php" );?>
<?php
if ($auth->isAuth()) {
?>

        <title><?php echo $header_name ?></title>
	<link rel="stylesheet" href="Include/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="Include/bootstrap/css/bootstrap-theme.min.css">
        <link href="Include/jquery-ui-1.11.4.custom/themes/black-tie/jquery-ui.css" rel="stylesheet" type="text/css" />
        <link href="Include/css/bootstrap-datepicker3.standalone.min.css" rel="stylesheet" type="text/css" />        

        <script src="Include/bootstrap/js/bootstrap.min.js"></script>        
        <script type="text/javascript" src="Include/jquery-ui-1.11.4.custom/jquery-ui.min.js"></script>
        <script type="text/javascript" src="js/bootstrap-datepicker.min.js"></script>
        <script type="text/javascript" src="js/search.js"></script>
		<style>
			.ui-dialog-titlebar-close { 
				outline: none; border: none;
			}
			:link { color: #0000EE; }
			:visited { color: #551A8B; }
			.ui-autocomplete {
				max-height: 600px;
				overflow-y: auto;
				overflow-x: hidden;
			}
			#content
			{
				max-height: 600px;
				overflow-y: auto;
				overflow-x: hidden;
			}

          </style>
    </head>
    <body>
    <?php require_once($path.'/Include/header_section.php');?>
		<div class="container">
			<form id="searchForm" role="form" action="phpFunctions/search.php" method="POST">
				<div class="group">
					<h2>Advanced Search</h2>
					
					<div class="form-group">
						<label for="PO">PO</label>
						<input type="text" class="form-control" id="PO" name="PO" placeholder="10880923">
					</div>
					<div class="form-group">
						<label for="Customer">Customer</label>
						<input type="text" class="form-control" id="Customer" name="Customer" placeholder="Alvin Aubinoe">
					</div>
					<div class="form-group">
						<label for="Phone">Phone</label>
						<input type="text" class="form-control" id="Phone" name="Phone" placeholder="2012135390">
					</div>
					<div class="form-group">
						<label for="Address">Address</label>
						<input type="text" class="form-control" id="Address" name="Address" placeholder="1504 US Highway  22">
					</div>
					<div class="form-group">
						<label for="Zip">Zip</label>
						<input type="text" class="form-control" id="Zip" name="Zip" placeholder="30512">
					</div>
					<div class="form-group">
						<label for="Dealer">Dealer</label>
						<input type="text" class="form-control" id="Dealer" name="Dealer" placeholder="FaucetDirect">
					</div>
					<div class="form-group">
						<label for="Carrier">Carrier</label>
						<input type="text" class="form-control" id="Carrier" name="Carrier" placeholder="ESTES EX - PROC">
					</div>
					<div class="form-group">
						<label for="Tracking">Tracking# / PRO#</label>
						<input type="text" class="form-control" id="Tracking" name="Tracking" placeholder="48675673-8">
					</div>
					<div class="form-group">
						<label for="Tracking">SM Invoice#</label>
						<input type="text" class="form-control" id="sm_invoice" name="sm_invoice" placeholder="15466249">
					</div>
					<div class="form-group">
						<label for="Tracking">QB Invoice#</label>
						<input type="text" class="form-control" id="qb_invoice" name="qb_invoice" placeholder="15466249">
					</div>
					<div class="form-group">
						<label for="Memo">Memo</label>
						<input type="text" class="form-control" id="Memo" name="Memo" placeholder="8449397, HOME DEPOT U.S.A., INC. D/B/A YOUR OTHER WAREHOUSE, , , , , TAG PO 59502912,  , , 59502912">
					</div>
                                        <div class="row">
                                            <div class="col-xs-3">
                                                <label for="Date1">Creation Date From</label>
                                                <input type="text" class="form-control" id="Date1" name="Date1" readonly="readonly" placeholder="">
                                            </div>                                            
                                            <div class="col-xs-3">
                                                <label for="Date1">Creation Date To</label>
                                                <input type="text" class="form-control" id="Date2" name="Date2" readonly="readonly" placeholder="">
                                            </div>
                                        </div>
                                        
				</div>
                                <br />
                                <br />
				<button type="submit" class="btn btn-success">Search</button>
				<button type="button" onClick="location.reload();" class="btn btn-default">Reset</button>
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
			</form>
		</div>
		<div id="dialog" title="TOP 100 search results">
			<div id="content"></div>
		</div>
                
                <script>
                    $("#Date1").datepicker();
                    $("#Date2").datepicker();
                </script>
                
	<?php
}
?>
<?php require_once($path.'/Include/footer_section.php'); ?>
