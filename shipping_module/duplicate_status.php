<?php
//session_start();
$path = $_SERVER['DOCUMENT_ROOT'];
require_once($path.'/db_connect/connect.php');
$conn = Database::getInstance()->dbc;
$query = "EXECUTE [".DATABASE31."].[dbo].[Orders_shipped_twice]";
$result = exec_query($conn, $query);
$Orders_shipped_twice = $result->fetchAll();
$query = "EXECUTE [".DATABASE31."].[dbo].[Orders_scanned_twice]";
$result = exec_query($conn, $query);
$Orders_scanned_twice = $result->fetchAll();
$query = "EXECUTE [".DATABASE31."].[dbo].[Orders_picked_twice]";
$result = exec_query($conn, $query);
$Orders_picked_twice = $result->fetchAll();
?>
<!doctype html>

<html lang="en">
<head>
  <meta charset="utf-8">
	<title>Duplicated Status Report</title>
	<link rel="stylesheet" href="Include/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="Include/bootstrap/css/bootstrap-theme.min.css">
	<script src="js/jquery-1.10.2.js"></script>
	<!--<script src="js/duplicate_status.js"></script>-->
	<script src="Include/bootstrap/js/bootstrap.min.js"></script>
	<style>
		#backHref {
			position: absolute;
			right: 1%;
			transition: 0.3s;
			top: 10%;
		}
		#backHref:hover {
			transform: scale(1.1);
		}
	#headerContainer {
	/*position: absolute;
	top: 0px;
	left: 0px;
	width: 90%;*/
	height: 50px;
	padding: 5px 20px;
	/*padding-bottom: 5px;*/
	/*padding-left: 10%;*/
	/*background: rgb(130, 167, 22);*/
	color: #367FBB;
	background: rgba(54, 127, 187, 0.1);
	-webkit-box-shadow: 0 1px 5px rgba(0, 0, 0, 0.25);
	-moz-box-shadow: 0 1px 5px rgba(0, 0, 0, 0.25);
	box-shadow: 0 1px 5px rgba(0, 0, 0, 0.25);
}
.product {
	float: right;
	margin-top: 10px;
	position: relative;
	padding-right: 40px;
}
#backHref img {
	margin-left: 10px;
}
#backHref img:hover {
	transform: scale(1.05); 
}
.product i {
	font-weight: bold;
}
.logo {
	width: 150px;
	float: left;
}
img {
	max-width: 100%;
	vertical-align: top;
}
	</style>
</head>

<body>
		<div id="headerContainer">
			<div class="logo"><img src="../Include/img/logo.png"></div>
			<div class="product">
				<i>Duplicated Status Report</i>
			</div>
		</div>

		
<br><h3><img src='Include/pictures/SCANNED.png' width='32px' height='44px' style='margin-right: 50px; margin-left: 50px;'><b>Scanned</b> more than once</h3><br>
<table data-toggle='table' class='table table-bordered display table-striped'>
<thead>
<tr class='info'>
	<th>Action</th>
	<th>PO</th>
	<th>ID</th>
	<th>Date</th>
	<th>Time</th>
	<!--<th>Investigation</th>
	<th>Fixing</th>-->
</tr>
</thead>
<tbody>
<?php
if($Orders_scanned_twice && !empty($Orders_scanned_twice))
{
	$prevPO = $Orders_scanned_twice[0]['ID'];
	foreach ($Orders_scanned_twice as $key => $res)
	{
		if ($prevPO != $res['ID'])
		{
			echo "
			<tr style='background-color: #D9EDF7 !important;'>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<!--<td></td>
				<td></td>-->
			</tr>";
			$ts = strtotime($res['date']);
			$res['OperationDate'] = date("m-d-Y", $ts);
			$res['OperationTime'] = date("H:i:s", $ts);
			echo "
				<tr>
					<td>".$res['action']."</td>
					<td><a target='_blank' href='/shipping_module/order_status_history.php?po=".urlencode($res['POnumber'])."&id=".urlencode($res['ID'])."'>".$res['POnumber']."</a></td>
					<td>".$res['ID']."</td>
					<td>".$res['OperationDate']."</td>
					<td>".$res['OperationTime']."</td>";
			/*if (!empty($res['investigated_user'])) echo "<td>Under investigation by user: ".$res['investigated_user']."</td>"; else
			{
					echo "<td><button onclick='inv(".'"'.$res['ID'].'"'.", ".'"'.$res['POnumber'].'"'.", ".'"'.$user_name.'"'.")' class='inv".$res['ID']."'>I will investigate it</button></td>";
			}
			echo "<td><button onclick='fix(".'"'.$res['ID'].'"'.", ".'"'.$res['POnumber'].'"'.", ".'"'.$user_name.'"'.")' class='fix".$res['ID']."'>I fixed this problem</button></td>";*/
			echo "</tr>";
			
		}
		else
		{
			$ts = strtotime($res['date']);
			$res['OperationDate'] = date("m-d-Y", $ts);
			$res['OperationTime'] = date("H:i:s", $ts);
			echo "
				<tr>
					<td>".$res['action']."</td>
					<td><a target='_blank' href='/shipping_module/order_status_history.php?po=".urlencode($res['POnumber'])."&id=".urlencode($res['ID'])."'>".$res['POnumber']."</a></td>
					<td>".$res['ID']."</td>
					<td>".$res['OperationDate']."</td>
					<td>".$res['OperationTime']."</td>";
			/*if (!empty($res['investigated_user'])) echo "<td>Under investigation by user: ".$res['investigated_user']."</td>"; else
			{
					echo "<td><button onclick='inv(".'"'.$res['ID'].'"'.", ".'"'.$res['POnumber'].'"'.", ".'"'.$user_name.'"'.")' class='inv".$res['ID']."'>I will investigate it</button></td>";
			}
			echo "<td><button onclick='fix(".'"'.$res['ID'].'"'.", ".'"'.$res['POnumber'].'"'.", ".'"'.$user_name.'"'.")' class='fix".$res['ID']."'>I fixed this problem</button></td>";*/
			echo "</tr>";
		}
		$prevPO = $res['ID'];
	}
} else
{
	echo "
			<tr>
				<td>N/A</td>
				<td>N/A</td>
				<td>N/A</td>
				<td>N/A</td>
				<td>N/A</td>
				<!--<td>N/A</td>
				<td>N/A</td>-->
			</tr>";
}
?>
</tbody>
</table>
		

<br><h3><img src='Include/pictures/PICKED.png' width='32px' height='44px' style='margin-right: 50px; margin-left: 50px;'><b>Picked</b> more than once</h3><br>
<table data-toggle='table' class='table table-bordered display table-striped'>
<thead>
<tr class='info'>
	<th>Action</th>
	<th>PO</th>
	<th>ID</th>
	<th>Date</th>
	<th>Time</th>
	<!--<th>Investigation</th>
	<th>Fixing</th>-->
</tr>
</thead>
<tbody>
<?php
if($Orders_picked_twice && !empty($Orders_picked_twice))
{
	$prevPO = $Orders_picked_twice[0]['ID'];
	foreach ($Orders_picked_twice as $key => $res)
	{
		if ($prevPO != $res['ID'])
		{
			echo "
			<tr style='background-color: #D9EDF7 !important;'>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<!--<td></td>
				<td></td>-->
			</tr>";
			$ts = strtotime($res['date']);
			$res['OperationDate'] = date("m-d-Y", $ts);
			$res['OperationTime'] = date("H:i:s", $ts);
			echo "
				<tr>
					<td>".$res['action']."</td>
					<td><a target='_blank' href='/shipping_module/order_status_history.php?po=".urlencode($res['POnumber'])."&id=".urlencode($res['ID'])."'>".$res['POnumber']."</a></td>
					<td>".$res['ID']."</td>
					<td>".$res['OperationDate']."</td>
					<td>".$res['OperationTime']."</td>";
			/*if (!empty($res['investigated_user'])) echo "<td>Under investigation by user: ".$res['investigated_user']."</td>"; else
			{
					echo "<td><button onclick='inv(".'"'.$res['ID'].'"'.", ".'"'.$res['POnumber'].'"'.", ".'"'.$user_name.'"'.")' class='inv".$res['ID']."'>I will investigate it</button></td>";
			}
			echo "<td><button onclick='fix(".'"'.$res['ID'].'"'.", ".'"'.$res['POnumber'].'"'.", ".'"'.$user_name.'"'.")' class='fix".$res['ID']."'>I fixed this problem</button></td>";*/
			echo "</tr>";
		}
		else
		{
			$ts = strtotime($res['date']);
			$res['OperationDate'] = date("m-d-Y", $ts);
			$res['OperationTime'] = date("H:i:s", $ts);
			echo "
				<tr>
					<td>".$res['action']."</td>
					<td><a target='_blank' href='/shipping_module/order_status_history.php?po=".urlencode($res['POnumber'])."&id=".urlencode($res['ID'])."'>".$res['POnumber']."</a></td>
					<td>".$res['ID']."</td>
					<td>".$res['OperationDate']."</td>
					<td>".$res['OperationTime']."</td>";
			/*if (!empty($res['investigated_user'])) echo "<td>Under investigation by user: ".$res['investigated_user']."</td>"; else
			{
					echo "<td><button onclick='inv(".'"'.$res['ID'].'"'.", ".'"'.$res['POnumber'].'"'.", ".'"'.$user_name.'"'.")' class='inv".$res['ID']."'>I will investigate it</button></td>";
			}
			echo "<td><button onclick='fix(".'"'.$res['ID'].'"'.", ".'"'.$res['POnumber'].'"'.", ".'"'.$user_name.'"'.")' class='fix".$res['ID']."'>I fixed this problem</button></td>";*/
			echo "</tr>";
		}
		$prevPO = $res['ID'];
	}
} else
{
	echo "
			<tr>
				<td>N/A</td>
				<td>N/A</td>
				<td>N/A</td>
				<td>N/A</td>
				<td>N/A</td>
				<!--<td>N/A</td>
				<td>N/A</td>-->
			</tr>";
}
?>
</tbody>
</table>
		

<br><h3><img src='Include/pictures/SHIPPED.png' width='32px' height='44px' style='margin-right: 50px; margin-left: 50px;'><b>Shipped</b> more than once</h3><br>
<table data-toggle='table' class='table table-bordered display table-striped'>
<thead>
<tr class='info'>
	<th>Action</th>
	<th>PO</th>
	<th>ID</th>
	<th>Date</th>
	<th>Time</th>
	<!--<th>Investigation</th>
	<th>Fixing</th>-->
</tr>
</thead>
<tbody>
<?php
if($Orders_shipped_twice && !empty($Orders_shipped_twice))
{
	$prevPO = $Orders_shipped_twice[0]['ID'];
	foreach ($Orders_shipped_twice as $key => $res)
	{
		if ($prevPO != $res['ID'])
		{
			echo "
			<tr style='background-color: #D9EDF7 !important;'>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<!--<td></td>
				<td></td>-->
			</tr>";
			$ts = strtotime($res['date']);
			$res['OperationDate'] = date("m-d-Y", $ts);
			$res['OperationTime'] = date("H:i:s", $ts);
			echo "
				<tr>
					<td>".$res['action']."</td>
					<td><a target='_blank' href='/shipping_module/order_status_history.php?po=".urlencode($res['POnumber'])."&id=".urlencode($res['ID'])."'>".$res['POnumber']."</a></td>
					<td>".$res['ID']."</td>
					<td>".$res['OperationDate']."</td>
					<td>".$res['OperationTime']."</td>";
			/*if (!empty($res['investigated_user'])) echo "<td>Under investigation by user: ".$res['investigated_user']."</td>"; else
			{
					echo "<td><button onclick='inv(".'"'.$res['ID'].'"'.", ".'"'.$res['POnumber'].'"'.", ".'"'.$user_name.'"'.")' class='inv".$res['ID']."'>I will investigate it</button></td>";
			}
			echo "<td><button onclick='fix(".'"'.$res['ID'].'"'.", ".'"'.$res['POnumber'].'"'.", ".'"'.$user_name.'"'.")' class='fix".$res['ID']."'>I fixed this problem</button></td>";*/
			echo "</tr>";
		}
		else
		{
			$ts = strtotime($res['date']);
			$res['OperationDate'] = date("m-d-Y", $ts);
			$res['OperationTime'] = date("H:i:s", $ts);
			echo "
				<tr>
					<td>".$res['action']."</td>
					<td><a target='_blank' href='/shipping_module/order_status_history.php?po=".urlencode($res['POnumber'])."&id=".urlencode($res['ID'])."'>".$res['POnumber']."</a></td>
					<td>".$res['ID']."</td>
					<td>".$res['OperationDate']."</td>
					<td>".$res['OperationTime']."</td>";
			/*if (!empty($res['investigated_user'])) echo "<td>Under investigation by user: ".$res['investigated_user']."</td>"; else
			{
					echo "<td><button onclick='inv(".'"'.$res['ID'].'"'.", ".'"'.$res['POnumber'].'"'.", ".'"'.$user_name.'"'.")' class='inv".$res['ID']."'>I will investigate it</button></td>";
			}
			echo "<td><button onclick='fix(".'"'.$res['ID'].'"'.", ".'"'.$res['POnumber'].'"'.", ".'"'.$user_name.'"'.")' class='fix".$res['ID']."'>I fixed this problem</button></td>";*/
			echo "</tr>";
		}
		$prevPO = $res['ID'];
	}
} else
{
	echo "
			<tr>
				<td>N/A</td>
				<td>N/A</td>
				<td>N/A</td>
				<td>N/A</td>
				<td>N/A</td>
				<!--<td>N/A</td>
				<td>N/A</td>-->
			</tr>";
}
?>
</tbody>
</table>
</body>
</html>
<?php
$conn = null;
/*} else header("Location: /shipping_module/index.php?is_exit=0");*/

function exec_query($conn, $query)
{
	try{
		$result = $conn->prepare($query);
		$result->execute();
		return $result;
	}
	catch(PDOException $e)
	{
		print_r($e->getMessage());
	}
}

function getConnection()
{
	try{
		$conn = new PDO("sqlsrv:server = ".SERVER."; Database = ".DATABASE, USER, PASSWORD);
		$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); }
		catch(PDOException $e)
		{
			die($e->getMessage());
		}
	return $conn;
}
?>