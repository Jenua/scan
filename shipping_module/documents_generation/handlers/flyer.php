<?php
//Include files
//Root path
$path = $_SERVER['DOCUMENT_ROOT'];

//Barcode library classes
require_once($path.'/shipping_module/Include/barcode/class/BCGFontFile.php');
require_once($path.'/shipping_module/Include/barcode/class/BCGColor.php');
require_once($path.'/shipping_module/Include/barcode/class/BCGDrawing.php');
require_once($path.'/shipping_module/Include/barcode/class/BCGcode39.barcode.php');
//DB connection class
require_once($path.'/db_connect/connect.php');
//Twig template engine
require_once($path.'/shipping_module/Include/Twig/Autoloader.php');

//Include error codes with description
require_once($path.'/analyzer/messages/messages.php');

//Include settings
require_once($path.'/general_settings/files/settings.php');

//Main logic
//Check request values
$request_data = get_array_from_json();

// Get values from request
$po = $request_data['po'];
$ref = $request_data['ref'];
$combined = $request_data['combined'];

if(!empty($request_data['pro_barcode']))
{
    $pro_barcode = $request_data['pro_barcode'];
} else
{
    $pro_barcode = false;
}

if(!empty($request_data['pro']))
{
    $pro = $request_data['pro'];
} else
{
    $pro = false;
}

if(!empty($request_data['carrier']))
{
    $carrier = $request_data['carrier'];
} else
{
    $carrier = false;
}

if(!empty($request_data['carrier_logo']))
{
    $carrier_logo = $request_data['carrier_logo'];
} else
{
    $carrier_logo = false;
}

if(!empty($request_data['print_carrier_info']))
{
    $print_carrier_info = $request_data['print_carrier_info'];
} else
{
    $print_carrier_info = false;
}

//Get DB data for this order
if(!empty($request_data['order_data']))
{
    $order_data = $request_data['order_data'];
} else
{
    $order_data = getOrderData($po, $ref);
    //debug($order_data);
}

//Check if order data is not empty
if (empty($order_data))
{
    add_message('10000', 'file: lowes.php');
    finish_script();
}

//Filter order data using master carton logic
$order_data = filterMasterCartonsAndBoxes($order_data, $combined);

//Count boxes
$order_data = boxes($order_data);

foreach($order_data as $key=>$row)
{
    //Generate barcodes
    $order_data[$key]['code39_upc_barcode'] = generate_code39_barcode($row['UPC']);
    
    //Assign pro barcode returned from bill of lading generator
    $order_data[$key]['pro_barcode'] = $pro_barcode;
    $order_data[$key]['pro'] = $pro;
    $order_data[$key]['carrier'] = $carrier;
    $order_data[$key]['carrier_logo'] = $carrier_logo;
    $order_data[$key]['print_carrier_info'] = $print_carrier_info;
    
    if($combined == 'true')
    {
        $order_data[$key]['page_multiplier'] = 2;
    } else
    {
        $order_data[$key]['page_multiplier'] = 1;
    }
}

//Initialize Twig
Twig_Autoloader::register();
$loader = new Twig_Loader_Filesystem('../templates');
$twig = new Twig_Environment($loader, array(
    'cache'       => 'compilation_cache',
    'auto_reload' => true
));

//Generate file and save it to disk, and save file name to array
$html = $twig->render('lowes.html', array('data' => $order_data, 'path' => $path));
file_put_contents($path.'/shipping_module/html_documents/label/'.$order_data[0]['PONumber'].'_label.html', $html);
$file_generated = $path.'/shipping_module/html_documents/label/'.$order_data[0]['PONumber'].'_label.html';

if (file_exists($file_generated))
{
    $request_data['label_path'] = $file_generated;
    //return order data for reuse
    $request_data['order_data'] = $order_data;
    success_finish_script($request_data);
} else
{
    add_message('11049', $file_generated);
    finish_script();
}

function getOrderData($po, $ref)
{
    $conn = Database::getInstance()->dbc;
    $query = "SELECT
      ld.[quantity] as [ld_qty]
      ,ld.[ItemRef_FullName]
      ,gd.[ItemGroupRef_FullName] as [gd_sku]
      ,gd.[Prod_desc]
      ,gd.[Quantity] as [gd_qty]
      ,gd.[TxnLineID] as [gd_TxnLineID]
      ,st.[CustomerRef_FullName]
      ,st.[RefNumber]
      ,st.[ShipAddress_Addr1]
      ,st.[ShipAddress_Addr2]
      ,st.[ShipAddress_City]
      ,st.[ShipAddress_State]
      ,st.[ShipAddress_PostalCode]
      ,st.[ShipAddress_Country]
      ,st.[PONumber]
      ,[dbo].FormatPhoneNumber([dbo].udf_GetNumeric(st.[FOB])) as [FOB]
      ,dl.[SKU]
      ,dl.[UPC]
      ,dl.[Box_Count]
      ,CASE
            WHEN gd.[ItemGroupRef_FullName] LIKE 'DLT%'
                    THEN 1
            WHEN gd.[ItemGroupRef_FullName] IN (SELECT [SKU] FROM [dbo].[master_carton] WHERE [DEALER] like 'Lowes')
                    THEN 1
            ELSE 0
       END AS [master_carton_lowes]
       ,CASE
            WHEN gd.[ItemGroupRef_FullName] LIKE 'DLT%'
                    THEN 1
            ELSE 0
       END AS [master_carton]
  FROM [dbo].[linedetail] ld
  INNER JOIN [dbo].[groupdetail] gd
	ON ld.[GroupIDKEY] = gd.[TxnLineID]
  INNER JOIN [dbo].[shiptrack] st
	ON gd.[IDKEY] = st.[TxnID]
  LEFT JOIN [dbo].[DL_valid_SKU] dl
	ON dl.[QB_SKU] = gd.[ItemGroupRef_FullName]
  WHERE
  (
	st.[PONumber] = ".$conn->quote($po)."
	AND st.[RefNumber] = ".$conn->quote($ref)."
  ) AND
  ld.[ItemRef_FullName] NOT LIKE '%Price-Adjustment%' 
  AND ld.[ItemRef_FullName] NOT LIKE '%Freight%'
  AND ld.[ItemRef_FullName] NOT LIKE '%warranty%'
  AND ld.[ItemRef_FullName] NOT LIKE '%Subtotal%'
  AND ld.[ItemRef_FullName] NOT LIKE '%IDSC-10%'
  AND ld.[ItemRef_FullName] NOT LIKE '%DISCOUNT%'
  AND ld.[ItemRef_FullName] NOT LIKE '%SPECIAL ORDER%'
  AND ld.[ItemRef_FullName] NOT LIKE '%Manual%'
  AND ld.[quantity] IS NOT NULL
  AND ld.[quantity] != '0'
  AND gd.[quantity] IS NOT NULL
  AND gd.[quantity] != '0'
  UNION
  SELECT
      m_ld.[quantity] as [ld_qty]
      ,m_ld.[ItemRef_FullName]
      ,m_gd.[ItemGroupRef_FullName] as [gd_sku]
      ,m_gd.[Prod_desc]
      ,m_gd.[Quantity] as [gd_qty]
      ,m_gd.[TxnLineID] as [gd_TxnLineID]
      ,m_st.[CustomerRef_FullName]
      ,m_st.[RefNumber]
      ,m_st.[ShipAddress_Addr1]
      ,m_st.[ShipAddress_Addr2]
      ,m_st.[ShipAddress_City]
      ,m_st.[ShipAddress_State]
      ,m_st.[ShipAddress_PostalCode]
      ,m_st.[ShipAddress_Country]
      ,m_st.[PONumber]
      ,[dbo].FormatPhoneNumber([dbo].udf_GetNumeric(m_st.[FOB])) as [FOB]
      ,dl.[SKU]
      ,dl.[UPC]
      ,dl.[Box_Count]
      ,CASE
            WHEN m_gd.[ItemGroupRef_FullName] LIKE 'DLT%'
                    THEN 1
            WHEN m_gd.[ItemGroupRef_FullName] IN (SELECT [SKU] FROM [dbo].[master_carton] WHERE [DEALER] like 'Lowes')
                    THEN 1
            ELSE 0
       END AS [master_carton_lowes]
       ,CASE
            WHEN m_gd.[ItemGroupRef_FullName] LIKE 'DLT%'
                    THEN 1
            ELSE 0
       END AS [master_carton]
  FROM [dbo].[manually_linedetail] m_ld
  INNER JOIN [dbo].[manually_groupdetail] m_gd
	ON m_ld.[GroupIDKEY] = m_gd.[TxnLineID]
  INNER JOIN [dbo].[manually_shiptrack] m_st
	ON m_gd.[IDKEY] = m_st.[TxnID]
  LEFT JOIN [dbo].[DL_valid_SKU] dl
	ON dl.[QB_SKU] = m_gd.[ItemGroupRef_FullName]
  WHERE
  (
	m_st.[PONumber] = ".$conn->quote($po)."
	AND m_st.[RefNumber] = ".$conn->quote($ref)."
  ) AND
  m_ld.[ItemRef_FullName] NOT LIKE '%Price-Adjustment%' 
  AND m_ld.[ItemRef_FullName] NOT LIKE '%Freight%'
  AND m_ld.[ItemRef_FullName] NOT LIKE '%warranty%'
  AND m_ld.[ItemRef_FullName] NOT LIKE '%Subtotal%'
  AND m_ld.[ItemRef_FullName] NOT LIKE '%IDSC-10%'
  AND m_ld.[ItemRef_FullName] NOT LIKE '%DISCOUNT%'
  AND m_ld.[ItemRef_FullName] NOT LIKE '%SPECIAL ORDER%'
  AND m_ld.[ItemRef_FullName] NOT LIKE '%Manual%'
  AND m_ld.[quantity] IS NOT NULL
  AND m_ld.[quantity] != '0'
  AND m_gd.[quantity] IS NOT NULL
  AND m_gd.[quantity] != '0'";
    
    $result = $conn->prepare($query);
    $result->execute();
    $result = $result->fetchAll(PDO::FETCH_ASSOC);
    if(!empty($result))
    {
        return $result;
    } else
    {
        return false;
    }
}

function add_message($error, $reference)
{
    global $messages, $message_type, $rule_type, $field_type;
    $messages[]= [
            'code' => $error,
            'type' => $message_type[substr($error, 0, 1)]['type'],
            'rule' => $rule_type[substr($error, 1, 2)]['rule'],
            'field' => $field_type[substr($error, 3, 2)]['field'],
            'reference' => $reference
        ];
}

function finish_script()
{
    global $messages;
    $response = [];
    $response['messages'] = $messages;
    $json_messages = json_encode($response);
    print_r($json_messages);
    die();
}

function success_finish_script($result)
{
    global $messages;
    $response = [];
    $response['data'] = $result;
    $response['messages'] = $messages;
    $json_response = json_encode($response);
    print_r($json_response);
    die();
}

//Is substring in string
function is_in_str($str,$substr)
{
	$str1 = strtolower($str);
	$substr1 = strtolower($substr);
	if (!empty($str1) && !empty($substr1))
	{
		$result = strpos($str1, $substr1);
	} else 
	{
		$result = false;
	}
	
	if ($result === false) return false; else return true;
}

function filterMasterCartonsAndBoxes($array, $combined) {
    global $settings;   //get access to settings
    
    if($combined == 'true') //use different values for combined and regular orders
    {
        $field_to_use = 'master_carton_lowes';
    } else
    {
        $field_to_use = 'master_carton';
    }
    
    $arary_master_carton = [];
    $array_filtered_txn = [];
    
    foreach($array as $key => $value)
    {
        //change items qty of items packed few into one box based on settings
        foreach($settings['label_generator']['items_to_boxes_allocation'] as $items_to_boxes_allocation)
        {
            if(in_array($value['gd_sku'], $items_to_boxes_allocation['items']))
            {
                $array[$key]['ld_qty'] = ceil($value['ld_qty'] / $items_to_boxes_allocation['items_in_box']);
                $value['ld_qty'] = $array[$key]['ld_qty'];
            }
        }
        
        //change items qty of products packed into master cartons
        if($value[$field_to_use] == '1')
        {
            if(!in_array($value['gd_TxnLineID'], $array_filtered_txn))
            {
                $array_filtered_txn[]=$value['gd_TxnLineID'];
                $value['ld_qty'] = $value['gd_qty']*$value['Box_Count'];
                $arary_master_carton[]=$value;
                foreach($array as $key1=>$row)
                {
                    if($value['gd_TxnLineID'] == $row['gd_TxnLineID']) unset($array[$key1]);
                }
            }
        }
    }
    
    if(!empty($arary_master_carton))
    {
        return $arary_master_carton;
    } else
    {
        return $array;
    }
}

//Count boxes for each PO number
function boxes($arr)
{
	try{
		$c = count($arr);
		for($i=0; $i<$c; $i++)
		{
			$row = $arr[$i];
			$PONumber = $row['PONumber'];
			$boxes = 0;
			foreach ($arr as $row2)
			{
				if ($row2['PONumber'] == $PONumber)
				{
					if (($row2['gd_sku']=='GSHST-01-TK') || 
					($row2['gd_sku']=='GSHST-01-PL'))
					{
						if((integer)$row2['ld_qty']<=5)
						{
							$qua = 1;
						} else 
						{
							$qua = ceil((integer)$row2['ld_qty'] / 5);
						}
						$boxes += $qua;
					}else 
					{
						$qua = (integer)$row2['ld_qty'];
						$boxes += $qua;
					}
				}
			}
			$row['boxes'] = $boxes;
			$arr[$i] = $row;
		}
		
		return $arr;
	}catch (Exception $e) {
            add_message('40048', 'file: lowes.php, error: '.$e->getMessage());
            finish_script();
	}
}

//Generate Code-39 barcode from given value
function generate_code39_barcode($value)
{
    global $path;
    $uniqid = uniqid();
    try{
	// Loading Font
	$font = new BCGFontFile($path.'/shipping_module/Include/barcode/font/Arial.ttf', 18);
	// The arguments are R, G, B for color.
	$color_black = new BCGColor(0, 0, 0);
	$color_white = new BCGColor(255, 255, 255); 

	$code = new BCGcode39();
	$code->setScale(2); // Resolution
	$code->setThickness(30); // Thickness
	$code->setForegroundColor($color_black); // Color of bars
	$code->setBackgroundColor($color_white); // Color of spaces
	$code->setFont($font); // Font (or 0)
	$code->parse($value); // Text
	$code->ClearLabels();
	$drawing = new BCGDrawing('', $color_white);
	$drawing->setBarcode($code);
	$drawing->draw();
        ob_start();
            $drawing->finish(BCGDrawing::IMG_FORMAT_PNG);
            $result = base64_encode( ob_get_contents() );
        ob_end_clean();
	unset($color_black);
	unset($color_white);
	return $result;
    } catch (Exception $e){
        add_message('40047', 'file: lowes.php, value: '.$value.', error: '.$e->getMessage());
        finish_script();
    }
}

//Get input data from POST or GET request in JSON format
function get_array_from_json()
{
    $response = false;
    try{
        if ($_SERVER['REQUEST_METHOD'] === 'GET')
        {
            $object = json_decode(urldecode($_REQUEST['data']));
            $response = (array)$object;
        } else if ($_SERVER['REQUEST_METHOD'] === 'POST')
        {
            $object = json_decode(urldecode($_REQUEST['data']));
            $response = (array)$object;
        } else
        {
            add_message('40340', 'request type = '.$_SERVER['REQUEST_METHOD']);
            finish_script();
        }
    } catch (Exception $ex) {
        add_message('40941', 'json data = '.urldecode($_REQUEST['data']));
        finish_script();
    }
    
    if ($response)
    {
        return $response;
    } else
    {
        add_message('40039', $_REQUEST);
        finish_script();
    }
}

//Call this func to display debug message
function debug($value)
{
    print_r('<pre>');
    print_r($value);
    print_r('</pre>');
}