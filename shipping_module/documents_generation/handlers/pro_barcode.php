<?php
//Include files
//Root path
$path = $_SERVER['DOCUMENT_ROOT'];

//Barcode library classes
require_once($path.'/shipping_module/Include/barcode/class/BCGFontFile.php');
require_once($path.'/shipping_module/Include/barcode/class/BCGColor.php');
require_once($path.'/shipping_module/Include/barcode/class/BCGDrawing.php');
require_once($path.'/shipping_module/Include/barcode/class/BCGcode39.barcode.php');

//Include error codes with description
require_once($path.'/analyzer/messages/messages.php');

//Main logic
//Check request values
$request_data = get_array_from_json();

if(!empty($request_data['pro']))
{
    $pro = $request_data['pro'];
} else
{
    $pro = false;
}

if(!empty($request_data['carrier']))
{
    $carrier = $request_data['carrier'];
} else
{
    $carrier = false;
}

if(!$pro || !$carrier)
{
    $request_data['print_carrier_info'] = false;
    success_finish_script($request_data);
} else
{
    $request_data['print_carrier_info'] = true;
    $request_data['pro_barcode'] = generate_code39_barcode($pro);
    $request_data['carrier_logo'] = getCarrierLogo($carrier);
    success_finish_script($request_data);
}

function add_message($error, $reference)
{
    global $messages, $message_type, $rule_type, $field_type;
    $messages[]= [
            'code' => $error,
            'type' => $message_type[substr($error, 0, 1)]['type'],
            'rule' => $rule_type[substr($error, 1, 2)]['rule'],
            'field' => $field_type[substr($error, 3, 2)]['field'],
            'reference' => $reference
        ];
}

function finish_script()
{
    global $messages;
    $response = [];
    $response['messages'] = $messages;
    $json_messages = json_encode($response);
    print_r($json_messages);
    die();
}

function success_finish_script($result)
{
    global $messages;
    $response = [];
    $response['data'] = $result;
    $response['messages'] = $messages;
    $json_response = json_encode($response);
    print_r($json_response);
    die();
}

//Is substring in string
function is_in_str($str,$substr)
{
	$str1 = strtolower($str);
	$substr1 = strtolower($substr);
	if (!empty($str1) && !empty($substr1))
	{
		$result = strpos($str1, $substr1);
	} else 
	{
		$result = false;
	}
	
	if ($result === false) return false; else return true;
}

//Generate Code-39 barcode from given value
function generate_code39_barcode($value)
{
    global $path;
    $uniqid = uniqid();
    try{
	// Loading Font
	$font = new BCGFontFile($path.'/shipping_module/Include/barcode/font/Arial.ttf', 18);
	// The arguments are R, G, B for color.
	$color_black = new BCGColor(0, 0, 0);
	$color_white = new BCGColor(255, 255, 255); 

	$code = new BCGcode39();
	$code->setScale(2); // Resolution
	$code->setThickness(30); // Thickness
	$code->setForegroundColor($color_black); // Color of bars
	$code->setBackgroundColor($color_white); // Color of spaces
	$code->setFont($font); // Font (or 0)
	$code->parse($value); // Text
	$code->ClearLabels();
	$drawing = new BCGDrawing('', $color_white);
	$drawing->setBarcode($code);
	$drawing->draw();
        ob_start();
            $drawing->finish(BCGDrawing::IMG_FORMAT_PNG);
            $result = base64_encode( ob_get_contents() );
        ob_end_clean();
	unset($color_black);
	unset($color_white);
	return $result;
    } catch (Exception $e){
        add_message('40047', 'file: pro_barcode.php, value: '.$value.', error: '.$e->getMessage());
        finish_script();
    }
}

//Get input data from POST or GET request in JSON format
function get_array_from_json()
{
    $response = false;
    try{
        if ($_SERVER['REQUEST_METHOD'] === 'GET')
        {
            $object = json_decode(urldecode($_REQUEST['data']));
            $response = (array)$object;
        } else if ($_SERVER['REQUEST_METHOD'] === 'POST')
        {
            $object = json_decode(urldecode($_REQUEST['data']));
            $response = (array)$object;
        } else
        {
            add_message('40340', 'request type = '.$_SERVER['REQUEST_METHOD']);
            finish_script();
        }
    } catch (Exception $ex) {
        add_message('40941', 'json data = '.urldecode($_REQUEST['data']));
        finish_script();
    }
    
    if ($response)
    {
        return $response;
    } else
    {
        add_message('40039', $_REQUEST);
        finish_script();
    }
}

//get carrier logo
function getCarrierLogo($carrier)
{
    global $path;
    $carrier = extrCarrier($carrier);
    switch ($carrier)
    {
        case "FEDEX":
                $src = $path.'/shipping_module/Include/pictures/fedex.png';
        break;
        case "RL":
                $src = $path.'/shipping_module/Include/pictures/rl.png';
        break;
        case "YRC":
                $src = $path.'/shipping_module/Include/pictures/yrc.png';
        break;
        case "ESTES":
                $src = $path.'/shipping_module/Include/pictures/estes.png';
        break;
        case "ABF":
                $src = $path.'/shipping_module/Include/pictures/abf.png';
        break;
        case "CEVA":
                $src = $path.'/shipping_module/Include/pictures/ceva.png';
        break;
        case "EMPIRE":
                $src = $path.'/shipping_module/Include/pictures/empire.PNG';
        break;
        case "NEW ENGL":
                $src = $path.'/shipping_module/Include/pictures/newengl.png';
        break;
        case "ODFL":
                $src = $path.'/shipping_module/Include/pictures/odfl.png';
        break;
        case "SEKO":
                $src = $path.'/shipping_module/Include/pictures/seko.png';
        break;
        case "UPS":
                $src = $path.'/shipping_module/Include/pictures/ups.png';
        break;
        case "YRC":
                $src = $path.'/shipping_module/Include/pictures/yrc.png';
        break;
        default:
                $src = $path.'/shipping_module/Include/pictures/noImage.png';
        break;
    }
    $data = file_get_contents($src);
    $base64 = base64_encode($data);
    return $base64;
}

//Extract carrier name from value
function extrCarrier($value)
{
	$result = false;
		if (is_in_str($value, "FEDEX") != false)
		{
			$result = "FEDEX";
		} else if (is_in_str($value, "RL") != false)
		{
			$result = "RL";
		} else if (is_in_str($value, "CEVA") != false)
		{
			$result = "CEVA";
		} else if (is_in_str($value, "YELLOW") != false || is_in_str($value, "YRC") != false)
		{
			$result = "YRC";
		} else if (is_in_str($value, "ESTES") != false)
		{
			$result = "ESTES";
		} else if (is_in_str($value, "NEW ENGL") != false)
		{
			$result = "NEW ENGL";
		} else if (is_in_str($value, "ODFL") != false)
		{
			$result = "ODFL";
		} else if (is_in_str($value, "SEKO") != false)
		{
			$result = "SEKO";
		} else if (is_in_str($value, "UPS") != false)
		{
			$result = "UPS";
		} else if (is_in_str($value, "EMPIRE") != false)
		{
			$result = "EMPIRE";
		} else if (is_in_str($value, "RoadRunner") != false)
		{
			$result = "Roadrunner";
		} else if (is_in_str($value, "CONWAY") != false)
		{
			$result = "CONWAY";
		} else if (is_in_str($value, "Zenith") != false)
		{
			$result = "ZEFL";
		} else if (is_in_str($value, "Watkins") != false)
		{
			$result = "WKSH";
		} else if (is_in_str($value, "HomeDirect") != false)
		{
			$result = "BVLC";
		} else if (is_in_str($value, "Non-Stop") != false)
		{
			$result = "NDLV";
		} else if (is_in_str($value, "Pyle") != false)
		{
			$result = "PYLE";
		} else if (is_in_str($value, "Averitt") != false)
		{
			$result = "AVRT";
		} else if (is_in_str($value, "Lakeville") != false)
		{
			$result = "LKVL";
		} else if (is_in_str($value, "ABF") != false)
		{
			$result = "ABF";
		} else if (is_in_str($value, "Land Air") != false)
		{
			$result = "LAXV";
		} else if (is_in_str($value, "Northwest") != false)
		{
			$result = "NFXR";
		} else if (is_in_str($value, "Southeastern Freight") != false)
		{
			$result = "SEFL";
		} else if (is_in_str($value, "Styline") != false)
		{
			$result = "STYH";
		} else if (is_in_str($value, "SunBelt") != false)
		{
			$result = "SBFE";
		} else if (is_in_str($value, "USF Holland") != false)
		{
			$result = "HMES";
		} else if (is_in_str($value, "USF Reddaway") != false)
		{
			$result = "RETL";
		} else if (is_in_str($value, "Wilson") != false)
		{
			$result = "WTVA";
		} else if (is_in_str($value, "Wiseway") != false)
		{
			$result = "WWMF";
		} else if (is_in_str($value, "NSD") != false)
		{
			$result = "NSD";
		} else $result = $value;
	return $result;
}


//Call this func to display debug message
function debug($value)
{
    print_r('<pre>');
    print_r($value);
    print_r('</pre>');
}