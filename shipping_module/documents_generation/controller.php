<?php
//PHP configuration
ini_set('max_execution_time', 7200);
ini_set("memory_limit", "2000M");
//Include files
//Root path
$path = $_SERVER['DOCUMENT_ROOT'];

//DB connection class
require_once($path.'/db_connect/connect.php');

//Include error codes with description
require_once($path.'/analyzer/messages/messages.php');

//Main logic
//Check request values
$request_data = get_array_from_json();

//basic validation of important valuses
if(empty($request_data['po']))
{
    add_message('10044', 'file: controller.php');
    finish_script();
}
if(empty($request_data['ref']))
{
    add_message('10045', 'file: controller.php');
    finish_script();
}
if(empty($request_data['combined']))
{
    add_message('10046', 'file: controller.php');
    finish_script();
}
if(empty($request_data['dealer']))
{
    add_message('10014', 'file: controller.php');
    finish_script();
}

//identify dealer
$dealerShortName = getDealerShortName($request_data['dealer']);

//run pro number barcode generation script
if  (
        empty($request_data['print_carrier_info'])
        || empty($request_data['pro_barcode'])
        || empty($request_data['carrier_logo'])
    )
{
    $response = curl_request('/shipping_module/documents_generation/handlers/pro_barcode.php', $request_data);
    $request_data = merge_data($request_data, $response);
}
//run label generation script based on dealer name
switch ($dealerShortName)
{
    case "Lowe's":
        $response = curl_request('/shipping_module/documents_generation/handlers/lowes_label.php', $request_data);
        $request_data = merge_data($request_data, $response);
        success_finish_script($request_data);
	break;
    case "Amazon":
	$response = curl_request('/shipping_module/documents_generation/handlers/amazon.php', $request_data);
        $request_data = merge_data($request_data, $response);
        success_finish_script($request_data);
        break;
    case "Amazon.Canada":
	$response = curl_request('/shipping_module/documents_generation/handlers/amazon_canada.php', $request_data);
        $request_data = merge_data($request_data, $response);
        success_finish_script($request_data);
        break;
    case "AmazonDS":
        $response = curl_request('/shipping_module/documents_generation/handlers/amazon_ds.php', $request_data);
        $request_data = merge_data($request_data, $response);
        success_finish_script($request_data);
        break;
    case "Sears":
	$response = curl_request('/shipping_module/documents_generation/handlers/sears.php', $request_data);
        $request_data = merge_data($request_data, $response);
        success_finish_script($request_data);
        break;
    case "C & R":
	$response = curl_request('/shipping_module/documents_generation/handlers/cnr.php', $request_data);
        $request_data = merge_data($request_data, $response);
        success_finish_script($request_data);
        break;
    case "MENARDS":
	$response = curl_request('/shipping_module/documents_generation/handlers/menards.php', $request_data);
        $request_data = merge_data($request_data, $response);
        success_finish_script($request_data);
        break;
    case "Wayfair":
	$response = curl_request('/shipping_module/documents_generation/handlers/wayfair.php', $request_data);
        $request_data = merge_data($request_data, $response);
        success_finish_script($request_data);
        break;
    case "Ferguson":
        $response = curl_request('/shipping_module/documents_generation/handlers/ferguson.php', $request_data);
        $request_data = merge_data($request_data, $response);
        success_finish_script($request_data);
        break;
    default:
	$response = curl_request('/shipping_module/documents_generation/handlers/homedepot.php', $request_data);
        $request_data = merge_data($request_data, $response);
        success_finish_script($request_data);
        break;
}

function merge_data($request_data, $response)
{   
    global $messages;
    
    $request_data = ($request_data);
    $response = json_decode($response, true);
    
    if(!empty($response['data']))
    {
        $request_data = $response['data'];
    }
    if(!empty($response['messages']))
    {
        $messages = array_merge($messages, $response['messages']);
    }
    
    return $request_data;
}


function add_message($error, $reference)
{
    global $messages, $message_type, $rule_type, $field_type;
    $messages[]= [
            'code' => $error,
            'type' => $message_type[substr($error, 0, 1)]['type'],
            'rule' => $rule_type[substr($error, 1, 2)]['rule'],
            'field' => $field_type[substr($error, 3, 2)]['field'],
            'reference' => $reference
        ];
}

function finish_script()
{
    global $messages;
    $response = [];
    $response['messages'] = $messages;
    $json_messages = json_encode($response);
    print_r($json_messages);
    die();
}

function success_finish_script($result)
{
    global $messages;
    $response = ['data' => $result, 'messages' => $messages];
    
    debug($response);
    
    $json_response = json_encode($response);
    print_r($json_response);
    die();
}

function get_array_from_json()
{
    $response = false;
    try{
        if ($_SERVER['REQUEST_METHOD'] === 'GET')
        {
            $object = json_decode(urldecode($_REQUEST['data']), true);
            $response = (array)$object;
        } else if ($_SERVER['REQUEST_METHOD'] === 'POST')
        {
            $object = json_decode(urldecode($_REQUEST['data']), true);
            $response = (array)$object;
        } else
        {
            add_message('40340', 'request type = '.$_SERVER['REQUEST_METHOD']);
            finish_script();
        }
    } catch (Exception $ex) {
        add_message('40941', 'json data = '.urldecode($_REQUEST['data']));
        finish_script();
    }
    
    if ($response)
    {
        return $response;
    } else
    {
        add_message('40039', $_REQUEST);
        finish_script();
    }
}

function getDealerShortName($value)
{
		if (is_in_str($value, "Lowe's") != false || is_in_str($value, "Lowe`s") != false)
		{
			$result = "Lowe's";
		} else if (is_in_str($value, "Amazon") != false && $value != "AmazonDS" && $value != "Amazon.Canada" && $value != "AmazonDS Canada")
		{
			$result = "Amazon";
		} else if (is_in_str($value, "AmazonDS") != false)
		{
			$result = "AmazonDS";
		} else if (is_in_str($value, "Amazon.Canada") != false)
		{
			$result = "Amazon.Canada";
		} else if (is_in_str($value, "The Home Depot") != false)
		{
			$result = "Depot";
		} else if (is_in_str($value, "C & R") != false)
		{
			$result = "C & R";
		} else if (is_in_str($value, "MENARDS") != false)
		{
			$result = "MENARDS";
		} else if (is_in_str($value, "HomeClick") != false)
		{
			$result = "HomeClick";
		} else if (is_in_str($value, "Sears") != false)
		{
			$result = "Sears";
		} else if (is_in_str($value, "Your other Warehouse") != false)
		{
			$result = "YOW";
		} else if (is_in_str($value, "Wayfair") != false)
		{
			$result = "Wayfair";
		} else if (is_in_str($value, "Ferguson") != false)
		{
			$result = "Ferguson";
		} else $result = $value;
	return $result;
}

function curl_request($url, $request_data)
{
    $json_array = urlencode(json_encode($request_data));
    
    $actual_link = "http://$_SERVER[HTTP_HOST]";
    $myCurl = curl_init();
    curl_setopt($myCurl, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($myCurl, CURLOPT_HEADER, FALSE);
    curl_setopt($myCurl, CURLOPT_FOLLOWLOCATION, TRUE);
    curl_setopt($myCurl, CURLOPT_MAXREDIRS, 10);
    curl_setopt($myCurl, CURLOPT_ENCODING, "");
    curl_setopt($myCurl, CURLOPT_USERAGENT, "test");
    curl_setopt($myCurl, CURLOPT_AUTOREFERER, TRUE);
    curl_setopt($myCurl, CURLOPT_CONNECTTIMEOUT, 120);
    curl_setopt($myCurl, CURLOPT_TIMEOUT, 120);

    curl_setopt($myCurl, CURLOPT_URL, $actual_link.$url);
    curl_setopt($myCurl, CURLOPT_POST, TRUE);
    curl_setopt($myCurl, CURLOPT_POSTFIELDS, http_build_query(array('data' => $json_array)));

    $response = curl_exec($myCurl);
    curl_close($myCurl);
    
    //debug($response);
    
    return $response;
}

function isJson($string) {
 json_decode($string);
 return (json_last_error() == JSON_ERROR_NONE);
}

//Is substring in string
function is_in_str($str,$substr)
{
	$result = strpos ($str, $substr);
	if ($result === FALSE) return false; else return true;
}

//Call this func to display debug message
function debug($value)
{
    print_r('<pre>');
    print_r($value);
    print_r('</pre>');
}