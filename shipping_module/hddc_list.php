<?php
set_time_limit(120);
session_start();
//order status history
require('phpFunctions/functionsExtr.php');
$path = $_SERVER['DOCUMENT_ROOT'];
require_once($path.'/db_connect/connect.php');
require_once($path.'/auth/auth.php');
require_once($path.'/shipping_module/Include/Twig/Autoloader.php');

function exec_query($conn, $query) {
	try {
		$result = $conn->prepare($query);
		$result->execute();
		return $result;
	}
	catch(PDOException $e) {
		print_r($e->getMessage());
	}
}
function parseItemFullName( $name ) {
    return end( explode(":", $name) );
}
function parsePalletQTY( $palletString ) {
	$palletAssoc = [];
	if( $palletString ) {
		$palletPairs  = explode( ',', $palletString );	
		foreach( $palletPairs as $palletPair ) {
			if( $palletPair ) {
				$palletParsed = explode( '=', $palletPair );
				if( !empty($palletParsed[0]) && !empty($palletParsed[1]) ) {
					$palletAssoc[ $palletParsed[0] ] = $palletParsed[1];
				}
			}
		}
	}
	return $palletAssoc;
}



Twig_Autoloader::register();
$loader = new Twig_Loader_Filesystem('templates/Pages');
$twig = new Twig_Environment($loader, array(
	'cache'       => 'compilation_cache',
	'auto_reload' => true
));

$auth = new AuthClass();
if( isset($_POST['login']) && isset($_POST["password"]) ) {
	if( !$auth->auth($_POST["login"], $_POST["password"]) ) {		
		$loginName = $_POST["login"] ? $_POST["login"] : '';
		$wrongCredentials = $twig->render('loginPage.html', array('login' => $loginName, 'errmessage' => 'Error: wrong login or password'));
		die($wrongCredentials);		
	}
}
if( isset($_GET["is_exit"]) ) {
    if ($_GET["is_exit"] == 1) {
        $auth->out();
        header("Location: ?is_exit=0");
    }
}
if( !$auth->isAuth() ) {	
	$noAuth = $twig->render('loginPage.html', array('login' => '', 'errmessage' => ''));
	die($noAuth);
}

// DB Connect
$conn = Database::getInstance()->dbc;

//GET HDDC orders
$productQuery1 = "SELECT
					st.[PONumber],
					st.[RefNumber],
					st.[TimeCreated]
				FROM [".DATABASE31."].[dbo].[salesorder] AS so
				INNER JOIN [shiptrack] AS st ON st.[PONumber] = so.[PONumber] AND st.[RefNumber] = so.[RefNumber]
				WHERE so.[SalesRepRef_FullName] = 'HDDC'
				ORDER BY CAST( st.[TimeCreated] as DATETIME)
				;";

$productQuery2 = "SELECT
					[PONumber],
					[RefNumber],
					[TimeCreated]
				FROM [shiptrack]
				WHERE [CustomerRef_FullName] LIKE 'Amazon'
				ORDER BY CAST( [TimeCreated] as DATETIME)
				;";

;
$productResult1 = exec_query($conn, $productQuery1);
$productResult2 = exec_query($conn, $productQuery2);
$productArray1  = $productResult1->fetchAll(PDO::FETCH_ASSOC);
$productArray2  = $productResult2->fetchAll(PDO::FETCH_ASSOC);

$printLabels = $twig->render('hddcList.html', array(	'products1' => $productArray1,
														'products2' => $productArray2 ));
die($printLabels);