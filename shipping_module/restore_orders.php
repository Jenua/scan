<?php
$header_name = "Restore Orders";
$path = $_SERVER['DOCUMENT_ROOT'];
require_once($path.'/shipping_module/header.php');
require_once($path.'/db_connect/connect.php');
$conn = Database::getInstance()->dbc;
$query="SELECT [po], [user], [date] FROM [dbo].[restore_order]";
$result = $conn->prepare($query);
$result->execute();
$result = $result->fetchAll(PDO::FETCH_ASSOC);
$pos = [];
foreach($result as $po)
{
    $pos[]=$po['po'];
}

$user = 'unknown';
if(!empty($result['0']['user']))
{
    $user = $result['0']['user'];
}

$date = 'unknown';
if(!empty($result['0']['date']))
{
    $date = $result['0']['date'];
}
?>
		<title>Restore Orders</title>
		<link rel="stylesheet" href="Include/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="Include/bootstrap/css/bootstrap-theme.min.css">
		<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
		<link rel="stylesheet" href="Include/jquery-tageditor/jquery.tag-editor.css">
		<script src="Include/bootstrap/js/bootstrap.min.js"></script>
		<script src="Include/jquery-tageditor/jquery.caret.min.js"></script>
		<script src="Include/jquery-tageditor/jquery.tag-editor.js"></script>
		<style>
		.tag-editor {
			min-height: 50px;
		}
		</style>
	</head>
	<body>
            <?php require_once($path.'/Include/header_section.php');?>
            <?php
            if($auth->getName() == 'Stanislav Stetsenko' || $auth->getName() == 'DreamLineAdmin')
            {
            ?>
		<div class="container">
			<br><h4>List of PO numbers to temporary restore (last edited by user <?php echo $user; ?> on date <?php echo $date; ?>):</h4>
			<form role="form" id="po_form" action="phpFunctions/save_orders_to_restore.php" method="POST">

                            <div style="padding: 10px;">
                                <textarea id='po_numbers'></textarea><br><br>
                                <script>
                                    $(document).ready(function(){
                                        $('#po_numbers').tagEditor({
                                            removeDuplicates: false,
                                            initialTags: <?php echo json_encode($pos); ?>,
                                        });
                                        
                                        //Submit event
                                        $('#po_form').on('submit', function (e) {
                                            event.preventDefault();
                                            var form = $('#po_form');
                                            var url = $(form).attr("action");

                                            var tovalidate = new Array();

                                            $( 'textarea', this ).each(function(){
                                                var tagstr = $(this).tagEditor('getTags')[0].tags;
                                                var tagarr = tagstr.toString().split(',');
                                                            tovalidate = tagarr;
                                            });

                                            $.ajax({
                                                url: url,
                                                type: 'POST',
                                                data: {
                                                    po_numbers: JSON.stringify(tovalidate),
                                                    user: <?php echo "'".$auth->getName()."'"; ?>
                                                },
                                                complete: function(data){
                                                    console.log(data.responseText);
                                                    alert(data.responseText);
                                                },
                                                error: function (request, status, error) {
                                                    console.log("Ajax request error: "+request.responseText);
                                                }
                                            });
                                        });
                                    })
                                </script>
                                <button type="submit" style="float: left;" class="btn btn-success">Save</button>
                            </div>       
			</form>
		</div>
            <?php
            } else
            {
                echo $auth->getName().", you do not have access to this page.";
            }
            ?>
	</body>
</html>