<?php
require('functionsDB.php');
$path = $_SERVER['DOCUMENT_ROOT'];
require_once($path.'/db_connect/connect.php');
date_default_timezone_set('America/New_York');

$connect = Database::getInstance()->dbc;

//Start if have PO number
if (isset($_REQUEST['value']) && !empty($_REQUEST['value']))
{
	updateTimeStamp($connect, $_REQUEST['value']);
}

$connect = null;

//write GenerateTimeStamp to ShipTrackWithLabel
function updateTimeStamp($connect, $Po)
{
	try{
		$query = "UPDATE [dbo].[".SHIPTRACKWITHLABEL_TABLE."] SET [GenerateTimeStamp] = GETDATE() WHERE [ID] = '".$Po."'";
		$result = execQuery($connect, $query);
		if ($result) echo 'GenerateTimeStamp is saved in DB.';
	}catch (Exception $e) {
		die("Can not save GenerateTimeStamp to database!");
	}
}
?>