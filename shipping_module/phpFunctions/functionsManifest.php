<?php
//Include files
require('../Include/tcpdf/tcpdf.php');
require('functionsPDFManifest.php');
require('functionsDB.php');
require('functionsExtr.php');

$path = $_SERVER['DOCUMENT_ROOT'];
require_once($path.'/db_connect/connect.php');

//Global variables
$error_messages = array();//Array to save all errors
$data_error_messages = array();//Array to save all errors in input data
$connect = Database::getInstance()->dbc;
$pdf_pathManifest = false;//path to pdf. False while not created

if (isset($_REQUEST['currentDate']) && !empty($_REQUEST['currentDate']))
	{
		main($_REQUEST['currentDate']);
		$connect = null;
	}

//Main function to execute functions kit depending on store name
function main($currentDate)
{
	global $error_messages;
	global $data_error_messages;
	global $connect;
	global $pdf_pathManifest;
	/*print_r($currentDate);
	die();*/
	//$currentDate = new DateTime($currentDate);
	
	$arrays = getSearsTodayOrders($connect, $currentDate);
	foreach ($arrays as $array)
	{
		$poNumbers[] = $array['PONumber'];
	}
	$poNumbers = array_unique($poNumbers);
	$outerArray = Array();
	foreach ($poNumbers as $poNumber)
	{
		$innerArray = Array();
		foreach ($arrays as $key => $array)
		{
			if($poNumber == $array['PONumber'])
			{
				$innerArray[] = $array;
			}
		}
		$outerArray[] = $innerArray;
	}
	$arrays = $outerArray;
	unset($outerArray);
	foreach ($arrays as $key => $array)
	{
		$array = boxes($array);
		$array = dublic($array);
		$array = extrPc($array);
		$array = getUPC($connect, $array);
		$array = getValidSKU($connect, $array);
		$arrays[$key] = $array;
	}
	
	$pdf_pathManifest = searsManifest($arrays);
	/*print_r($pdf_pathManifest);
	
	print_r('<pre>');
	print_r($arrays);
	print_r('</pre>');*/
	return send_error();
}

//Sort array
function ab($a,$b)
{
	return ($a['boxNumber']-$b['boxNumber']);
}

//Fills an array with error messages
function error_msg($msg)
{
	global $error_messages;
	$error_messages[] = $msg;
}

//Fills an array with errors in input data
function data_error_msg($type, $id, $msg)
{
	global $data_error_messages;
	$row = array('type' => $type, 'id' => $id, 'msg' => $msg);
	$data_error_messages[] = $row;
}

//Generate list of error messages
function formErrors($error_messages)
{
	if (isset($error_messages) && !empty($error_messages))
	{
		$mes_text = "<b><p style='font-size: 12pt; color: red;'>Manifest:</p></b>";
		foreach($error_messages as $message){
			$mes_text.= "<b><p style='font-size: 10pt; color: black;'>".$message."</p></b>";
		}
		return $mes_text;
	} else return false;
}

//Generate table with errors in input data
function formDataErrors($data_error_messages)
{
	if (isset($data_error_messages) && !empty($data_error_messages))
	{
		$mes_text = "<b><p style='font-size: 12pt; color: red;'>Manifest. Detailed problems:</p></b><p><table border='1' style='font-size: 12pt; color: black;'>
		<tr>
			<th>type</th>
			<th>data</th>
			<th>problem</th>
		</tr>";
		foreach($data_error_messages as $message){
			$mes_text.= "<tr>
			<td>".$message['type']."</td>
			<td>".$message['id']."</td>
			<td>".$message['msg']."</td>
			</tr>";
		}
		$mes_text.= "</table></p>";
		return $mes_text;
	} else return false;
}

//Returns array to ajax query with errors and pathes to pdf files and terminates script
function send_error()
{
	global $error_messages;
	global $data_error_messages;
	global $pdf_pathManifest;
	try
	{
		$formErrors = (string)formErrors($error_messages);
		$formDataErrors = (string)formDataErrors($data_error_messages);
		if ($pdf_pathManifest && isset($pdf_pathManifest) && !empty($pdf_pathManifest))
		{
			$html = (string)$pdf_pathManifest[0];
		} else $html = 'false';
		$result = array(
		'path' => $html,
		'er' => $formErrors,
		'DataEr' => $formDataErrors
		);
		$result = json_encode($result);
		print_r($result);
	} catch (Exception $e){
		echo $e->getMessage();
	}
	die();
}
?>