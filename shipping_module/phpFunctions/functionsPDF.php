<?php
define("globalWidth", 210);
define("globalHeight", 336);
define("globalMargins", 11);
define("globalLineWidth", 0.8);
define("globalMainFont", 20);
define("globalSmallFont", 14);
define("globalBigFont", 100);
define("globalCarrierFont", 40);
define("globalRegularCellHeight", 14);
define("globalSmallCellHeight", 8);
define("globalBigCellHeight", 40);
define("globalBottomContentY", 266);

// Functions to generate PDF depending on store name (look function name)
class PDF_Rotate extends FPDF
{
var $angle=0;

function Rotate($angle,$x=-1,$y=-1)
{
    if($x==-1)
        $x=$this->x;
    if($y==-1)
        $y=$this->y;
    if($this->angle!=0)
        $this->_out('Q');
    $this->angle=$angle;
    if($angle!=0)
    {
        $angle*=M_PI/180;
        $c=cos($angle);
        $s=sin($angle);
        $cx=$x*$this->k;
        $cy=($this->h-$y)*$this->k;
        $this->_out(sprintf('q %.5F %.5F %.5F %.5F %.2F %.2F cm 1 0 0 1 %.2F %.2F cm',$c,$s,-$s,$c,$cx,$cy,-$cx,-$cy));
    }
}
}

class PDF extends PDF_Rotate
{
function RotatedText($x,$y,$txt,$angle)
{
    //Text rotated around its origin
    $this->Rotate($angle,$x,$y);
    $this->Text($x,$y,$txt);
    $this->Rotate(0);
}

function RotatedImage($file,$x,$y,$w,$h,$angle)
{
    //Image rotated around its upper-left corner
    $this->Rotate($angle,$x,$y);
    $this->Image($file,$x,$y,$w,$h);
    $this->Rotate(0);
}


function _endpage()
{
    if($this->angle!=0)
    {
        $this->angle=0;
        $this->_out('Q');
    }
    parent::_endpage();
}
}

//Generate pdf files for each PO number with separate pages for each box number with barcodes and info
function lowesPdf($arr, $pro10, $barcodeBill, $originCarrierName, $ProductsWarnings)
{
	try{
			if ($barcodeBill != 'none')
			{
				$barcodeBill = $barcodeBill.'.png';
			}
			$PONumbers = array();
			$result = array();
			
			foreach ($arr as $row)
			{
				$PONumbers[] = $row['PONumber'];
			}
			$c = count($PONumbers);
			
			for ($i=0; $i<$c; $i++)
			{
				$pdf_path = "../Pdf/".repPo($PONumbers[$i]).".pdf";
				$pdf_path2 = "Pdf/".repPo($PONumbers[$i]).".pdf";
				
				$pdf = new PDF('P','mm',array(globalWidth, globalHeight));
				$pdf->SetAutoPageBreak(true, 1);
				$pdf->SetMargins(5,5,5);
				$pdf->SetLineWidth(globalLineWidth);
				
				foreach ($arr as $row)
				{
				if ($row['quantity_in_box'] == 1) $row['quantity_in_box'] = 0;
				for ($qib = 0; $qib <= $row['quantity_in_box']; $qib++)
				{
					if ($row['PONumber'] == $PONumbers[$i])
					{
						$logo = '../Include/logo/logo.png';
						$UPC = 'UPC CODE: '.$row['UPC'];
						$Path = $row['Path'].'.png';
						//$PONumber = 'PO Number: '.checkPO($row['PONumber'], $row['RefNumber'], $row['original_po']);
                                                $PONumber = checkPO($row['PONumber'], $row['RefNumber'], $row['original_po']);
						$ProductCode = 'SKU: '.$row['ProductCode'];
						
						$boxes = (integer)$row['boxes'];
						$boxNumber = (integer)$row['boxNumber'];
						if ($boxes > 1)
						{
							$text = "This box is part of a group.";
							$text1 = "This is box:";
							$BoxCarton = $boxNumber.' of '.$boxes;
						} else
						{
							$text = "This is a single box.";
							$text1 = "This is box:";
							$BoxCarton = $boxNumber.' of '.$boxes;
						}
						
						//$ShipAddress_Addr1 =  'Customer: '.$row['ShipAddress_Addr1'];
                                                $ShipAddress_Addr1 =  $row['ShipAddress_Addr1'];
						$contact = 'For questions or concerns regarding this order';
						$contact1 = 'Please contact us at 866-731-8378';
						
						//$pdf->AddPage();
						$pdf->AddPage('P', array(102, 153), false, false);
						/* Update PDF to 4" x 6" */
						$localMainFont = 10;
						$localBigFont = 52;
						$localSmallFont = 8;
						$localCarrierFont = 7;
						$localRegularCellHeight = 5;
						$localBigCellHeight = 22;
						$localSmallCellHeight = 5;
						$localBottomContentY = 120;
						
						$pdf->SetFont('Arial','B',$localSmallFont);
								
								//UPC Barcode
								//$pdf->Cell(190,20,"UPC CODE",0,1,'L');
								$pdf->Image($Path, NULL, NULL, -110);
								
								//UPC Code
								$pdf->Cell(82,$localRegularCellHeight,$UPC,0,1,'C');
								
								//SKU Code
								$pdf->Cell(82,$localRegularCellHeight,$ProductCode,0,1,'L');
								
								
								
								//Customer
                                                                $pdf->Ln();
                                                                $pdf->SetFont('Arial','B',$localSmallFont);
                                                                $pdf->Cell(82,6,'Customer:',0,1,'L');
                                                                $pdf->SetFont('Arial','B',18);
								$pdf->Cell(82,8,$ShipAddress_Addr1,0,1,'L');
                                                                
                                                                //PO #
                                                                $pdf->SetFont('Arial','B',$localSmallFont);
                                                                $pdf->Cell(82,6,'PO Number:',0,1,'L');
                                                                $pdf->SetFont('Arial','B',32);
								$pdf->Cell(82,12,$PONumber,0,1,'C');
								
								//text
								$pdf->SetFont('Arial','',$localMainFont);
								$pdf->Cell(82,$localSmallCellHeight,$text,0,1,'C');
								$pdf->Cell(82,$localSmallCellHeight,$text1,0,1,'C');
								//Box Number / Carton Number
								$pdf->SetFont('Arial','B',$localBigFont);
								$pdf->Cell(82,18,$BoxCarton,0,1,'C');
								
								//line
								$pdf->SetFont('Arial','B',$localSmallFont);
								$pdf->Cell(82,2,'_________________________________________________',0,1,'C');
								
								//Dreamline Logo
                                $pdf->Cell(12);
                                $actual_position_y = $pdf->GetY();
                                $pdf->Image($logo, 30, $actual_position_y+3, 40);
                                $pdf->SetXY(0, $actual_position_y+15);
								
								//contact
								/*$pdf->SetFont('Arial','',$localMainFont);
								$pdf->Cell(82,$localSmallCellHeight,$contact,0,1,'C');*/
								$pdf->SetFont('Arial','B',$localMainFont);
								$pdf->Cell(82,$localSmallCellHeight,$contact1,0,1,'C');
								if ($pro10 != 'none' && $barcodeBill != 'none')
								{
									$pdf->Line(11, $localBottomContentY, 92, $localBottomContentY);
									$pdf->SetXY(15, $localBottomContentY);
									$pdf->Ln();
									$pdf->SetFont('Arial','B',$localMainFont);
									$actual_position_y = $pdf->GetY();
									$pdf->SetXY(34, $actual_position_y);
									$pdf->Cell(58, 20, " ", 1, 1, 'C');
									$pdf->SetXY(36, $actual_position_y+1);
									$pdf->Cell(58,$localSmallCellHeight,"PRO# ".$pro10,0,1,'L');
									$pdf->SetXY(37, $actual_position_y+7);
									$pdf->Image($barcodeBill, NULL, NULL, -165);
									if (isCarrierNameCorrect($originCarrierName))
									{
										$carrierLogo = getCarrierLogo($originCarrierName);
										$pdf->Image($carrierLogo, 11, $localBottomContentY+4, 21, 14, 'PNG');
										$pdf->SetFont('Arial','B',$localCarrierFont);
										$pdf->SetXY(10, $localBottomContentY+20);
										$pdf->Cell(92,$localSmallCellHeight,$originCarrierName,0,1,'L');
									}
									
								} else
								{
									if (isCarrierNameCorrect($originCarrierName))
									{
										$pdf->Line(11, $localBottomContentY, 92, $localBottomContentY);
										$carrierLogo = getCarrierLogo($originCarrierName);
										$pdf->Image($carrierLogo, 11, $localBottomContentY+4, 21, 14, 'PNG');
										
										$pdf->SetFont('Arial','',$localMainFont);
										$pdf->SetXY(32, $localBottomContentY+10);
										$pdf->Cell(92,$localSmallCellHeight,$originCarrierName,0,1,'L');
									}
								}
					}
					}
				}
				
				if ($pro10 != 'none' && $barcodeBill != 'none')
				{
					$pagesNum = count($arr) + 2;
					for($pn = 0; $pn < $pagesNum; $pn++)
					{
						//$pdf->AddPage();
						$pdf->AddPage('P', array(102, 153), false, false);
						/* Update PDF to 4" x 6" */
						$localMainFont = 11;
						$localBigFont = 52;
						$localSmallFont = 8;
						$localRegularCellHeight = 6;
						$localBigCellHeight = 22;
						$localSmallCellHeight = 5;
						$localBottomContentY = 120;
						
						
						$pdf->SetFont('Arial','B',$localSmallFont);

						if (extrCarrier($originCarrierName) == "RL")
						{
							$pdf->SetXY(24, 40);
							$pdf->Cell(58,$localSmallCellHeight, $arr[0]['ShipAddress_Addr1'],0,1,'L');
							$actual_position_y = $pdf->GetY();
							$pdf->SetXY(24, $actual_position_y);
							$pdf->Cell(58,$localSmallCellHeight, $arr[0]['ShipAddress_Addr2'].", - ",0,1,'L');
							$actual_position_y = $pdf->GetY();
							$pdf->SetXY(24, $actual_position_y);
							$pdf->Cell(58,$localSmallCellHeight, $arr[0]['ShipAddress_City'].", ".$arr[0]['ShipAddress_State'].", ".$arr[0]['ShipAddress_PostalCode'],0,1,'L');
							$actual_position_y = $pdf->GetY();
							$pdf->SetXY(24, $actual_position_y);
							$pdf->Cell(58,$localSmallCellHeight, $arr[0]['FOB'],0,1,'L');
						}
						
						$pdf->SetFont('Arial','B',$localMainFont);						
						//$actual_position_y = $pdf->GetY();
						$actual_position_y = 62;
						$pdf->SetXY(23, $actual_position_y);
						$pdf->Cell(58, 20, " ", 1, 1, 'C');
						$pdf->SetXY(25, $actual_position_y+1);
						$pdf->Cell(58,$localSmallCellHeight,"PRO# ".$pro10,0,1,'L');
						$pdf->SetXY(26, $actual_position_y+7);
						$pdf->Image($barcodeBill, NULL, NULL, -165);
						
						if (isCarrierNameCorrect($originCarrierName))
						{
							$carrierLogo = getCarrierLogo($originCarrierName);
							$pdf->Image($carrierLogo, 24, 84, 0, 17, 'PNG');
									
							$pdf->SetFont('Arial','B',$localSmallFont);
							$pdf->SetXY(24, 103);
							$pdf->Cell(82,$localSmallCellHeight,$originCarrierName,0,1,'L');
						}
					}
				}
				
				if(!empty($ProductsWarnings))
				{
					//$pdf->AddPage();
					$pdf->AddPage('P', array(102, 153), false, false);
					$localSmallFont = 8;					
					$localRegularCellHeight = 5;
					
					$pdf->SetFont('Arial','B',$localSmallFont);
					foreach($ProductsWarnings as $ProductsWarning)
					{
						$pdf->Cell(82,$localRegularCellHeight,$ProductsWarning,0,1,'L');
					}
				}
				
				$pdf->Output($pdf_path);
				$result[] = $pdf_path2;
			}
			if (!empty($result))
			{
				return $result;
			} else
			{
				error_msg("Can not generate any pdf!");
				send_error();
			}
	}catch (Exception $e) {
		error_msg($e->getMessage());
		send_error();
	}
}

//Generate pdf files for each PO number with separate pages for each box number with barcodes and info
function amazonPdf($arr, $pro, $bol, $pro10, $barcodeBill, $originCarrierName, $ProductsWarnings)
{
	try{
			if ($barcodeBill != 'none')
			{
				$barcodeBill = $barcodeBill.'.png';
			}
			
			$carrierLogo = getCarrierLogo($originCarrierName);
			
			$PONumbers = array();
			$result = array();
			
			foreach ($arr as $row)
			{
				$PONumbers[] = $row['PONumber'];
			}
			$c = count($PONumbers);
			
			for ($i=0; $i<$c; $i++)
			{
				$pdf_path = "../Pdf/".repPo($PONumbers[$i]).".pdf";
				$pdf_path2 = "Pdf/".repPo($PONumbers[$i]).".pdf";
				
				$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
				$pdf->SetPrintHeader(false);
				$pdf->SetPrintFooter(false);
				$pdf->SetMargins(11, 11);
				$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

				foreach ($arr as $row)
				{
				if ($row['quantity_in_box'] == 1) $row['quantity_in_box'] = 0;
				for ($qib = 0; $qib <= $row['quantity_in_box']; $qib++)
				{
					if ($row['PONumber'] == $PONumbers[$i])
					{
						$logo = '../Include/logo/logo.png';
						$UPC = 'UPC: '.$row['UPC'];
						$Path = $row['Path'].'.png';
						$PONumber = 'PO Number: '.checkPO($row['PONumber'], $row['RefNumber'], $row['original_po']);
						$ProductCode = 'SKU: '.$row['ProductCode'];
						
						$boxes = (integer)$row['boxes'];
						$boxNumber = (integer)$row['boxNumber'];
						if ($boxes > 1)
						{
							$text = "This box is part of a group.";
							$text1 = "This is box:";
							$BoxCarton = $boxNumber.' of '.$boxes;
						} else
						{
							$text = "This is a single box.";
							$text1 = "This is box:";
							$BoxCarton = $boxNumber.' of '.$boxes;
						}
						$contact = 'For questions or concerns regarding this order';
						$contact1 = 'Please contact us at 866-731-8378';
						
						//$pdf->AddPage();
						$pdf->AddPage('P', array(102, 153), false, false);
						
						$pdf->SetFont('helvetica', '', 7); // 670
						$tbl = '
								<table border="1" width="280" cellpadding="5">
								 <tr>
								  <td width="33%">
									<b>SHIP FROM:</b><br>
									DreamLine<br>
									75 Hawk Rd.<br>
									Warminster, PA<br>
									18974
								  </td>
								  <td width="67%">
									<b>SHIP TO:</b><br>';
									if (isset($row['ShipAddress_Addr1']) && !empty($row['ShipAddress_Addr1'])) $tbl.=$row['ShipAddress_Addr1'].'<br>';
									if (isset($row['ShipAddress_Addr2']) && !empty($row['ShipAddress_Addr2'])) $tbl.=$row['ShipAddress_Addr2'].'<br>';
									if (isset($row['ShipAddress_Addr3']) && !empty($row['ShipAddress_Addr3'])) $tbl.=$row['ShipAddress_Addr3'].'<br>';
									if (isset($row['ShipAddress_Addr4']) && !empty($row['ShipAddress_Addr4'])) $tbl.=$row['ShipAddress_Addr4'].'<br>';
									if (isset($row['ShipAddress_Addr5']) && !empty($row['ShipAddress_Addr5'])) $tbl.=$row['ShipAddress_Addr5'].'<br>';
									if (isset($row['ShipAddress_City']) && !empty($row['ShipAddress_City'])) $tbl.=$row['ShipAddress_City'].', ';
									if (isset($row['ShipAddress_State']) && !empty($row['ShipAddress_State'])) $tbl.=$row['ShipAddress_State'].'<br>';
									if (isset($row['ShipAddress_PostalCode']) && !empty($row['ShipAddress_PostalCode'])) $tbl.=$row['ShipAddress_PostalCode'].'<br>';
								 $tbl.='</td>
								 </tr>
								 <tr>';
								 if ($pro10 != 'none' && $barcodeBill != 'none' && (PrintCarrierForAmazonMode == '0' || PrintCarrierForAmazonMode == '2'))
								{
									$tbl.='
									<td width="50%" align="center">
										<br>
										<img src="'.$carrierLogo.'" height="70px">
										<br>
										<br>
										<b>PRO# '.$pro10.'</b><br><br>
										<img src="'.$barcodeBill.'" width="120"><br>
									</td>';
								} else
								{
									$tbl.='
									<td width="50%">
										<br>
										<br>
										<br>
										<br>
										<br>
										<br>
									</td>';
								}
								 $tbl.='
								  <td width="50%">
									<br>';
								 if (PrintCarrierForAmazonMode == '0' || PrintCarrierForAmazonMode == '2')
								{
									$tbl.='<font size="9">'.$originCarrierName.'</font><br>';
								} else
								{
									$tbl.='<font size="9"></font><br>';
								}
									$tbl.='PRO#: '.strtoupper($pro).'<br>
									BOL#: '.strtoupper($bol).'<br>
								  </td>
								 </tr>
								 <tr nobr="true">
								  <td colspan="2">
									<table cellpadding="8">
										<tr>
											<td width="67%">
												<b>PURCHASE ORDER(S): '.checkPO($row['PONumber'], $row['RefNumber'], $row['original_po']).'<br>
												UPC: '.$row['UPC'].'<br>
												QTY: '.$row['quantity'].'<br>
												CARTON#: '.$BoxCarton.'</b>
											</td>
											<td width="33%">
												
											</td>
										</tr>
									</table>
								  </td>
								 </tr>
								 <tr nobr="true">
								  <td colspan="2" align="center">
									<p>Barcode Packing Slip</p>
									<img src="'.$Path.'" width="260">
								  </td>
								 </tr>
								 
								</table>
								<b>'.$ProductCode.'</b>
								<br><br><br>
								 <table cellpadding="10">
										<tr>
											<td width="87%" align="center"><font size="16px"> CARTON#: '.$BoxCarton.'</font>
											</td></tr></table>
								';
								
								$pdf->writeHTML($tbl, true, false, false, false, '');								
					}
					}
				}
				foreach ($arr as $row)
				{
					if ($pro10 != 'none' && $barcodeBill != 'none' && (PrintCarrierForAmazonMode == '0' || PrintCarrierForAmazonMode == '2'))
					{
						//$pdf->AddPage();
						$pdf->AddPage('P', array(102, 153), false, false);
						$pdf->SetFont('helvetica', '', 8);
						$tbl='
						<table width="280">
							<tr>
								<td width="15%"></td>
								<td width="70%" style="font-size: 6px;">
									<br>
									<br>
									<br>
									<br>
									<br>
									<br>
									<br>
									<br>
									<br>
									<br>
									<br>
									<br>
									<br>';
						if (extrCarrier($originCarrierName) == "RL")
						{
							$tbl.='
									'.$arr[0]['ShipAddress_Addr1'].'<br>
									'.$arr[0]['ShipAddress_Addr2'].", - ".'<br>
									'.$arr[0]['ShipAddress_City'].", ".$arr[0]['ShipAddress_State'].", ".$arr[0]['ShipAddress_PostalCode'].'<br>
									'.$arr[0]['FOB'].'<br>';
						} else
						{
							$tbl.='<br>
									<br>';
						}
								$tbl.='</td>
								<td width="15%"></td>
							</tr>
							<tr>
								<td width="15%"></td>
								<td width="70%">
									<table border="1" width="100%">
										<tr>
											<td align="center">
												<b>PRO# '.$pro10.'</b><br><br>
												<img src="'.$barcodeBill.'" width="170"><br>
											</td>
										</tr>
									</table>
								</td>
								<td width="15%"></td>
							</tr>
							<tr>
								<td width="15%"></td>
								<td width="70%">
									<br>
									<br>
									<br>
									<img src="'.$carrierLogo.'" height="70px">
								</td>
								<td width="15%"></td>
							</tr>
						</table>';
						$pdf->writeHTML($tbl, true, false, false, false, '');
						
						
						
					}
				}
				if(!empty($ProductsWarnings))
						{
							//$pdf->AddPage();
							$pdf->AddPage('P', array(102, 153), false, false);
							$pdf->SetFont('helvetica', '', 8);
							$tbl='';
							foreach($ProductsWarnings as $ProductsWarning)
							{
								$tbl.='<p>'.$ProductsWarning.'</p>';
							}
							$pdf->writeHTML($tbl, true, false, false, false, '');
						}
				//$pdf->Output($pdf_path);				
				$pdf->Output($pdf_path, 'F');
				$result[] = $pdf_path2;
			}
			if (!empty($result))
			{
				return $result;
			} else
			{
				error_msg("Can not generate any pdf!");
				send_error();
			}
	}catch (Exception $e) {
		error_msg($e->getMessage());
		send_error();
	}
}

//Generate pdf files for each PO number with separate pages for each box number with barcodes and info
function amazonCanadaPdf($arr, $pro, $bol, $pro10, $barcodeBill, $originCarrierName, $ProductsWarnings)
{
	try{
			if ($barcodeBill != 'none')
			{
				$barcodeBill = $barcodeBill.'.png';
			}
			
			$carrierLogo = getCarrierLogo($originCarrierName);
			
			$PONumbers = array();
			$result = array();
			
			foreach ($arr as $row)
			{
				$PONumbers[] = $row['PONumber'];
			}
			$c = count($PONumbers);
			
			for ($i=0; $i<$c; $i++)
			{
				$pdf_path = "../Pdf/".repPo($PONumbers[$i]).".pdf";
				$pdf_path2 = "Pdf/".repPo($PONumbers[$i]).".pdf";
				
				$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
				$pdf->SetPrintHeader(false);
				$pdf->SetPrintFooter(false);
				$pdf->SetMargins(11, 11);
				$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

				foreach ($arr as $row)
				{
				if ($row['quantity_in_box'] == 1) $row['quantity_in_box'] = 0;
				for ($qib = 0; $qib <= $row['quantity_in_box']; $qib++)
				{
					if ($row['PONumber'] == $PONumbers[$i])
					{
						$logo = '../Include/logo/logo.png';
						$UPC = 'UPC: '.$row['UPC'];
						$Path = $row['Path'].'.png';
						$PONumber = 'PO Number: '.checkPO($row['PONumber'], $row['RefNumber'], $row['original_po']);
						$ProductCode = 'SKU: '.$row['ProductCode'];
						
						$boxes = (integer)$row['boxes'];
						$boxNumber = (integer)$row['boxNumber'];
						if ($boxes > 1)
						{
							$text = "This box is part of a group.";
							$text1 = "This is box:";
							$BoxCarton = $boxNumber.' of '.$boxes;
						} else
						{
							$text = "This is a single box.";
							$text1 = "This is box:";
							$BoxCarton = $boxNumber.' of '.$boxes;
						}
						$contact = 'For questions or concerns regarding this order';
						$contact1 = 'Please contact us at 866-731-8378';
						
						//$pdf->AddPage();
						$pdf->AddPage('P', array(102, 153), false, false);
						$pdf->SetFont('helvetica', '', 7);
						$tbl = '
								<table border="1" width="280" cellpadding="5">
								 <tr>
								  <td width="33%">
									<b>SHIP FROM:</b><br>
									DreamLine<br>
									75 Hawk Rd.<br>
									Warminster, PA<br>
									18974
								  </td>
								  <td width="67%">
									<b>SHIP TO:</b><br>';
									if (isset($row['ShipAddress_Addr1']) && !empty($row['ShipAddress_Addr1'])) $tbl.=$row['ShipAddress_Addr1'].'<br>';
									if (isset($row['ShipAddress_Addr2']) && !empty($row['ShipAddress_Addr2'])) $tbl.=$row['ShipAddress_Addr2'].'<br>';
									if (isset($row['ShipAddress_Addr3']) && !empty($row['ShipAddress_Addr3'])) $tbl.=$row['ShipAddress_Addr3'].'<br>';
									if (isset($row['ShipAddress_Addr4']) && !empty($row['ShipAddress_Addr4'])) $tbl.=$row['ShipAddress_Addr4'].'<br>';
									if (isset($row['ShipAddress_Addr5']) && !empty($row['ShipAddress_Addr5'])) $tbl.=$row['ShipAddress_Addr5'].'<br>';
									if (isset($row['ShipAddress_City']) && !empty($row['ShipAddress_City'])) $tbl.=$row['ShipAddress_City'].', ';
									if (isset($row['ShipAddress_State']) && !empty($row['ShipAddress_State'])) $tbl.=$row['ShipAddress_State'].'<br>';
									if (isset($row['ShipAddress_PostalCode']) && !empty($row['ShipAddress_PostalCode'])) $tbl.=$row['ShipAddress_PostalCode'].'<br>';
								 $tbl.='</td>
								 </tr>
								 <tr>';
								 if ($pro10 != 'none' && $barcodeBill != 'none')
								{
									$tbl.='
									<td width="50%" align="center">
										<br>
										<br>
										<!--<img src="'.$carrierLogo.'" height="70px">-->
										<br>
										<br>
										<br>
										<!--<b>PRO# '.$pro10.'</b><br><br>-->
										<br>
										<br>
										<br>
										<!--<img src="'.$barcodeBill.'" width="120"><br>-->
									</td>';
								} else {
									$tbl.='
									<td width="50%">
										<br>
										<br>
										<br>
										<br>
										<br>
										<br>
									</td>';
								}
								 $tbl.='
								  <td width="50%">
									<br>
									<br>
									<br>
									<br>
									<br>
									<br>
									<br>
									<br>
									<br>
									<br>
									<!--<font size="9">'.$originCarrierName.'</font><br>
									PRO#: '.strtoupper($pro).'<br>
									BOL#: '.strtoupper($bol).'<br>-->
								  </td>
								 </tr>
								 <tr nobr="true">
								  <td colspan="2">
									<table cellpadding="8">
										<tr>
											<td width="67%">
												<b>PURCHASE ORDER(S): '.checkPO($row['PONumber'], $row['RefNumber'], $row['original_po']).'<br>
												UPC: '.$row['UPC'].'<br>
												QTY: '.$row['quantity'].'<br>
												CARTON#: '.$BoxCarton.'</b>
											</td>
											<td width="33%">
												
											</td>
										</tr>
									</table>
								  </td>
								 </tr>
								 <tr nobr="true">
								  <td colspan="2" align="center">
									<p>Barcode Packing Slip</p>
									<img src="'.$Path.'" width="260">
								  </td>
								 </tr>
								 
								</table>
								<b>'.$ProductCode.'</b>
								';
								
								$pdf->writeHTML($tbl, true, false, false, false, '');								
					}
					}
				}
				foreach ($arr as $row)
				{
					if ($pro10 != 'none' && $barcodeBill != 'none')
					{
						//$pdf->AddPage();
						$pdf->AddPage('P', array(102, 153), false, false);
						$pdf->SetFont('helvetica', '', 7);
						$tbl='
						<table width="280">
							<tr>
								<td width="15%"></td>
								<td width="70%" style="font-size: 6px;">
									<br>
									<br>
									<br>
									<br>
									<br>
									<br>
									<br>
									<br>
									<br>
									<br>
									<br>';
						if (extrCarrier($originCarrierName) == "RL")
						{
							$tbl.='
									'.$arr[0]['ShipAddress_Addr1'].'<br>
									'.$arr[0]['ShipAddress_Addr2'].", - ".'<br>
									'.$arr[0]['ShipAddress_City'].", ".$arr[0]['ShipAddress_State'].", ".$arr[0]['ShipAddress_PostalCode'].'<br>
									'.$arr[0]['FOB'].'<br>';
						} else
						{
							$tbl.='<br>
									<br>';
						}
								$tbl.='</td>
								<td width="15%"></td>
							</tr>
							<tr>
								<td width="15%"></td>
								<td width="70%">
									<table border="1" width="100%">
										<tr>
											<td align="center">
												<b>PRO# '.$pro10.'</b><br><br>
												<img src="'.$barcodeBill.'" width="190"><br>
											</td>
										</tr>
									</table>
								</td>
								<td width="15%"></td>
							</tr>
							<tr>
								<td width="15%"></td>
								<td width="70%">
									<br>
									<br>
									<br>
									<img src="'.$carrierLogo.'" height="70px">
								</td>
								<td width="15%"></td>
							</tr>
						</table>';
						$pdf->writeHTML($tbl, true, false, false, false, '');
					}
				}
				if(!empty($ProductsWarnings))
						{
							//$pdf->AddPage();
							$pdf->AddPage('P', array(102, 153), false, false);
							$pdf->SetFont('helvetica', '', 7);
							$tbl='';
							foreach($ProductsWarnings as $ProductsWarning)
							{
								$tbl.='<p>'.$ProductsWarning.'</p>';
							}
							$pdf->writeHTML($tbl, true, false, false, false, '');
						}
				//$pdf->Output($pdf_path);
				$pdf->Output($pdf_path, 'F');
				$result[] = $pdf_path2;
			}
			if (!empty($result))
			{
				return $result;
			} else
			{
				error_msg("Can not generate any pdf!");
				send_error();
			}
	}catch (Exception $e) {
		error_msg($e->getMessage());
		send_error();
	}
}
//Generate pdf files for each PO number with separate pages for each box number with barcodes and info
function amazonDSPdf($arr, $pro10, $barcodeBill, $originCarrierName, $ProductsWarnings)
{
//print_r($arr);
	try{
			if ($barcodeBill != 'none')
			{
				$barcodeBill = $barcodeBill.'.png';
			}
			$PONumbers = array();
			$result = array();
			
			foreach ($arr as $row)
			{
				$PONumbers[] = $row['PONumber'];
			}
			$c = count($PONumbers);
			
			for ($i=0; $i<$c; $i++)
			{
				$pdf_path = "../Pdf/".repPo($PONumbers[$i]).".pdf";
				$pdf_path2 = "Pdf/".repPo($PONumbers[$i]).".pdf";
				
				$pdf = new PDF('P','mm',array(globalWidth, globalHeight));
				$pdf->SetAutoPageBreak(true, 1);
				$pdf->SetMargins(globalMargins,globalMargins,globalMargins);
				$pdf->SetLineWidth(globalLineWidth);
				
				foreach ($arr as $row)
				{
				if ($row['quantity_in_box'] == 1) $row['quantity_in_box'] = 0;
				for ($qib = 0; $qib <= $row['quantity_in_box']; $qib++)
				{
					if ($row['PONumber'] == $PONumbers[$i])
					{
						$logo = '../Include/logo/logo.png';
						$PONumber = 'PO Number: '.checkPO($row['PONumber'], $row['RefNumber'], $row['original_po']);
						$ProductCode = 'SKU: '.$row['ProductCode'];
						
						$boxes = (integer)$row['boxes'];
						$boxNumber = (integer)$row['boxNumber'];
						if ($boxes > 1)
						{
							$text = "This box is part of a group.";
							$text1 = "This is box:";
							$BoxCarton = $boxNumber.' of '.$boxes;
						} else
						{
							$text = "This is a single box.";
							$text1 = "This is box:";
							$BoxCarton = $boxNumber.' of '.$boxes;
						}
						$contact = 'For questions or concerns regarding this order';
						$contact1 = 'Please contact us at 866-731-8378';
						
						$carrier = extrCarrier($row['ShipMethodRef_FullName']);
						//print_r($carrier);
						
						//$pdf->AddPage();
						$pdf->AddPage('P', array(102, 153), false, false);
						/* Update PDF to 4" x 6" */
						$localMainFont = 10;
						$localBigFont = 52;
						$localSmallFont = 8;
						$localCarrierFont = 7;
						$localRegularCellHeight = 5;
						$localBigCellHeight = 22;
						$localSmallCellHeight = 5;
						$localBottomContentY = 120;
						
						
						$pdf->SetFont('Arial','B',$localMainFont);
								
								//SKU Code
								$pdf->Cell(82,$localRegularCellHeight,$ProductCode,0,1,'L');
								//$pdf->Ln();
								//PO #
								$pdf->Cell(82,$localRegularCellHeight,$PONumber,0,1,'L');
								

								//text
								$pdf->SetFont('Arial','',$localMainFont);
								$pdf->Ln();
								$pdf->Cell(82,$localSmallCellHeight,$text,0,1,'C');
								$pdf->Cell(82,$localSmallCellHeight,$text1,0,1,'C');

								//Box Number / Carton Number
								$pdf->SetFont('Arial','B',$localBigFont);
								$pdf->Cell(82,$localBigCellHeight,$BoxCarton,0,1,'C');
								
								$pdf->SetFont('Arial','B',$localSmallFont);
								$pdf->Cell(82,2,'_________________________________________________',0,1,'C');
									
								//Dreamline Logo
                                $pdf->Cell(12);
                                $actual_position_y = $pdf->GetY();
                                $pdf->Image($logo, 30, $actual_position_y+5, 40);
                                $pdf->SetXY(0, $actual_position_y+15);
								
								//contact
								$pdf->SetFont('Arial','',$localMainFont);
								$pdf->Ln(2);
								$pdf->Cell(82,$localSmallCellHeight,$contact,0,1,'C');
								$pdf->SetFont('Arial','B',$localMainFont);
								$pdf->Cell(82,$localSmallCellHeight,$contact1,0,1,'C');
								if ($row['CustomerRef_FullName'] == 'AmazonDS') 
								{
									$pdf->Ln(2);
									$pdf->Cell(82,$localSmallCellHeight,'Amazon DS',0,1,'C');
								}
								// TBD
								if ($pro10 != 'none' && $barcodeBill != 'none' && $carrier == 'CEVA')
								{
									$pdf->Line(11, $localBottomContentY, 92, $localBottomContentY);
									$pdf->SetXY(15, $localBottomContentY);
									$pdf->Ln();
									$pdf->SetFont('Arial','B',$localSmallFont);
									$actual_position_y = $pdf->GetY();
									$pdf->SetXY(34, $actual_position_y);
									$pdf->Cell(58, 20, " ", 1, 1, 'C');
									$pdf->SetXY(36, $actual_position_y+1);
									if ($carrier == 'CEVA') $pdf->Cell(58,$localSmallCellHeight,"CEVA Logistics",0,1,'L'); else $pdf->Cell(58,$localSmallCellHeight,"",0,1,'L');
									$pdf->SetXY(37, $actual_position_y+6);
									$pdf->Image($barcodeBill, NULL, NULL, -165);
									$pdf->SetXY(36, $actual_position_y+15);
									$pdf->Cell(58,$localSmallCellHeight,"PRO# ".$pro10,0,1,'L');	
									
									
									$carrierLogo = getCarrierLogo($originCarrierName);
									$pdf->Image($carrierLogo, 11, $localBottomContentY+4, 21, 14, 'PNG');
									$pdf->SetFont('Arial','B',$localCarrierFont);
									$pdf->SetXY(10, $localBottomContentY+20);
									$pdf->Cell(92,$localSmallCellHeight,$originCarrierName,0,1,'L');
								} else if ($pro10 != 'none' && $barcodeBill != 'none' && $carrier == 'ESTES')
								{
									$pdf->Line(11, $localBottomContentY, 92, $localBottomContentY);
									$pdf->SetXY(15, $localBottomContentY);
									$pdf->Ln();
									$pdf->SetFont('Arial','B',$localSmallFont);
									$actual_position_y = $pdf->GetY();
									$pdf->SetXY(34, $actual_position_y);
									$pdf->Cell(58, 20, " ", 1, 1, 'C');
									$pdf->SetXY(36, $actual_position_y+1);
									if ($carrier == 'ESTES') $pdf->Cell(58,$localSmallCellHeight,$carrier,0,1,'L'); else $pdf->Cell(58,$localSmallCellHeight,"",0,1,'L');
									$pdf->SetXY(37, $actual_position_y+6);
									$pdf->Image($barcodeBill, NULL, NULL, -165);
									$pdf->SetXY(36, $actual_position_y+15);
									$pdf->Cell(58,$localSmallCellHeight,"PRO# ".$pro10,0,1,'L');
									
									
									$carrierLogo = getCarrierLogo($originCarrierName);
									$pdf->Image($carrierLogo, 11, $localBottomContentY+4, 21, 14, 'PNG');
									$pdf->SetFont('Arial','B',$localCarrierFont);
									$pdf->SetXY(10, $localBottomContentY+20);
									$pdf->Cell(92,$localSmallCellHeight,$originCarrierName,0,1,'L');									
								} else if ($pro10 != 'none' && $barcodeBill != 'none' && $carrier == 'ABF')
								{
									$pdf->Line(11, $localBottomContentY, 92, $localBottomContentY);
									$pdf->SetXY(15, $localBottomContentY);
									$pdf->Ln();
									$pdf->SetFont('Arial','B',$localSmallFont);
									$actual_position_y = $pdf->GetY();
									$pdf->SetXY(34, $actual_position_y);
									$pdf->Cell(58, 20, " ", 1, 1, 'C');
									$pdf->SetXY(36, $actual_position_y+1);
									if ($carrier == 'ABF') $pdf->Cell(58,$localSmallCellHeight,$carrier,0,1,'L'); else $pdf->Cell(58,$localSmallCellHeight,"",0,1,'L');
									$pdf->SetXY(37, $actual_position_y+6);
									$pdf->Image($barcodeBill, NULL, NULL, -165);
									$pdf->SetXY(36, $actual_position_y+15);
									$pdf->Cell(58,$localSmallCellHeight,"PRO# ".$pro10,0,1,'L');
									
									
									$carrierLogo = getCarrierLogo($originCarrierName);
									$pdf->Image($carrierLogo, 11, $localBottomContentY+4, 21, 14, 'PNG');
									$pdf->SetFont('Arial','B',$localCarrierFont);
									$pdf->SetXY(10, $localBottomContentY+20);
									$pdf->Cell(92,$localSmallCellHeight,$originCarrierName,0,1,'L');									
								}
								// !TBD
					}
					}
				}
				
				if ($pro10 != 'none' && $barcodeBill != 'none' && $carrier == 'CEVA')
				{
					$pagesNum = count($arr) + 2;
					for($pn = 0; $pn < $pagesNum; $pn++)
					{
						$pdf->AddPage('P', array(102, 153), false, false);
						/* Update PDF to 4" x 6" */
						$localMainFont = 11;
						$localBigFont = 52;
						$localSmallFont = 8;
						$localRegularCellHeight = 6;
						$localBigCellHeight = 22;
						$localSmallCellHeight = 5;
						$localBottomContentY = 120;
						
						
						$pdf->SetFont('Arial','B',$localSmallFont);

						if (extrCarrier($originCarrierName) == "RL")
						{
							$pdf->SetXY(24, 40);
							$pdf->Cell(58,$localSmallCellHeight, $arr[0]['ShipAddress_Addr1'],0,1,'L');
							$actual_position_y = $pdf->GetY();
							$pdf->SetXY(24, $actual_position_y);
							$pdf->Cell(58,$localSmallCellHeight, $arr[0]['ShipAddress_Addr2'].", - ",0,1,'L');
							$actual_position_y = $pdf->GetY();
							$pdf->SetXY(24, $actual_position_y);
							$pdf->Cell(58,$localSmallCellHeight, $arr[0]['ShipAddress_City'].", ".$arr[0]['ShipAddress_State'].", ".$arr[0]['ShipAddress_PostalCode'],0,1,'L');
							$actual_position_y = $pdf->GetY();
							$pdf->SetXY(24, $actual_position_y);
							$pdf->Cell(58,$localSmallCellHeight, $arr[0]['FOB'],0,1,'L');
						}
						
						$pdf->SetFont('Arial','B',$localSmallFont);						
						//$actual_position_y = $pdf->GetY();
						$actual_position_y = 62;
						$pdf->SetXY(23, $actual_position_y);
						$pdf->Cell(58, 20, " ", 1, 1, 'C');
						$pdf->SetXY(25, $actual_position_y+1);
						if ($carrier == 'CEVA') $pdf->Cell(58,$localSmallCellHeight,"CEVA Logistics",0,1,'L'); else $pdf->Cell(58,$localSmallCellHeight,"",0,1,'L');						
						$pdf->SetXY(26, $actual_position_y+6);
						$pdf->Image($barcodeBill, NULL, NULL, -165);
						$pdf->SetXY(25, $actual_position_y+15);
						$pdf->Cell(58,$localSmallCellHeight,"PRO# ".$pro10,0,1,'L');
						
						$carrierLogo = getCarrierLogo($originCarrierName);
						$pdf->Image($carrierLogo, 24, 84, 0, 17, 'PNG');
									
						$pdf->SetFont('Arial','B',$localSmallFont);
						$pdf->SetXY(24, 103);
						$pdf->Cell(82,$localSmallCellHeight,$originCarrierName,0,1,'L');
					}
				} else if ($pro10 != 'none' && $barcodeBill != 'none' && $carrier == 'ESTES')
				{
					$pagesNum = count($arr) + 2;
					for($pn = 0; $pn < $pagesNum; $pn++)
					{
						//$pdf->AddPage();
						$pdf->AddPage('P', array(102, 153), false, false);
						/* Update PDF to 4" x 6" */
						$localMainFont = 11;
						$localBigFont = 52;
						$localSmallFont = 8;
						$localRegularCellHeight = 6;
						$localBigCellHeight = 22;
						$localSmallCellHeight = 5;
						$localBottomContentY = 120;
						
						
						$pdf->SetFont('Arial','B',$localSmallFont);

						if (extrCarrier($originCarrierName) == "RL")
						{
							$pdf->SetXY(24, 40);
							$pdf->Cell(58,$localSmallCellHeight, $arr[0]['ShipAddress_Addr1'],0,1,'L');
							$actual_position_y = $pdf->GetY();
							$pdf->SetXY(24, $actual_position_y);
							$pdf->Cell(58,$localSmallCellHeight, $arr[0]['ShipAddress_Addr2'].", - ",0,1,'L');
							$actual_position_y = $pdf->GetY();
							$pdf->SetXY(24, $actual_position_y);
							$pdf->Cell(58,$localSmallCellHeight, $arr[0]['ShipAddress_City'].", ".$arr[0]['ShipAddress_State'].", ".$arr[0]['ShipAddress_PostalCode'],0,1,'L');
							$actual_position_y = $pdf->GetY();
							$pdf->SetXY(24, $actual_position_y);
							$pdf->Cell(58,$localSmallCellHeight, $arr[0]['FOB'],0,1,'L');
						}
						
						$pdf->SetFont('Arial','B',$localSmallFont);
						//$actual_position_y = $pdf->GetY();
						$actual_position_y = 62;
						$pdf->SetXY(23, $actual_position_y);
						$pdf->Cell(58, 20, " ", 1, 1, 'C');
						$pdf->SetXY(25, $actual_position_y+1);
						if ($carrier == 'ESTES') $pdf->Cell(58,$localSmallCellHeight,$carrier,0,1,'L'); else $pdf->Cell(58,$localSmallCellHeight,"",0,1,'L');						
						$pdf->SetXY(26, $actual_position_y+6);
						$pdf->Image($barcodeBill, NULL, NULL, -165);
						$pdf->SetXY(25, $actual_position_y+15);
						$pdf->Cell(58,$localSmallCellHeight,"PRO# ".$pro10,0,1,'L');
						
						$carrierLogo = getCarrierLogo($originCarrierName);
						$pdf->Image($carrierLogo, 24, 84, 0, 17, 'PNG');
									
						$pdf->SetFont('Arial','B',$localSmallFont);
						$pdf->SetXY(24, 103);
						$pdf->Cell(82,$localSmallCellHeight,$originCarrierName,0,1,'L');
					}
				} else if ($pro10 != 'none' && $barcodeBill != 'none' && $carrier == 'ABF')
				{
					$pagesNum = count($arr) + 2;
					for($pn = 0; $pn < $pagesNum; $pn++)
					{
						//$pdf->AddPage();
						$pdf->AddPage('P', array(102, 153), false, false);
						/* Update PDF to 4" x 6" */
						$localMainFont = 11;
						$localBigFont = 52;
						$localSmallFont = 8;
						$localRegularCellHeight = 6;
						$localBigCellHeight = 22;
						$localSmallCellHeight = 5;
						$localBottomContentY = 120;
						
						
						$pdf->SetFont('Arial','B',$localSmallFont);

						if (extrCarrier($originCarrierName) == "RL")
						{
							$pdf->SetXY(24, 40);
							$pdf->Cell(58,$localSmallCellHeight, $arr[0]['ShipAddress_Addr1'],0,1,'L');
							$actual_position_y = $pdf->GetY();
							$pdf->SetXY(24, $actual_position_y);
							$pdf->Cell(58,$localSmallCellHeight, $arr[0]['ShipAddress_Addr2'].", - ",0,1,'L');
							$actual_position_y = $pdf->GetY();
							$pdf->SetXY(24, $actual_position_y);
							$pdf->Cell(58,$localSmallCellHeight, $arr[0]['ShipAddress_City'].", ".$arr[0]['ShipAddress_State'].", ".$arr[0]['ShipAddress_PostalCode'],0,1,'L');
							$actual_position_y = $pdf->GetY();
							$pdf->SetXY(24, $actual_position_y);
							$pdf->Cell(58,$localSmallCellHeight, $arr[0]['FOB'],0,1,'L');
						}
						
						$pdf->SetFont('Arial','B',$localSmallFont);
						//$actual_position_y = $pdf->GetY();
						$actual_position_y = 62;
						$pdf->SetXY(23, $actual_position_y);
						$pdf->Cell(58, 20, " ", 1, 1, 'C');
						$pdf->SetXY(25, $actual_position_y+1);
						if ($carrier == 'ABF') $pdf->Cell(58,$localSmallCellHeight,$carrier,0,1,'L'); else $pdf->Cell(58,$localSmallCellHeight,"",0,1,'L');						
						$pdf->SetXY(26, $actual_position_y+6);
						$pdf->Image($barcodeBill, NULL, NULL, -165);
						$pdf->SetXY(25, $actual_position_y+15);
						$pdf->Cell(58,$localSmallCellHeight,"PRO# ".$pro10,0,1,'L');
						
						$carrierLogo = getCarrierLogo($originCarrierName);
						$pdf->Image($carrierLogo, 24, 84, 0, 17, 'PNG');
									
						$pdf->SetFont('Arial','B',$localSmallFont);
						$pdf->SetXY(24, 103);
						$pdf->Cell(82,$localSmallCellHeight,$originCarrierName,0,1,'L');
					}
				}
				
				if(!empty($ProductsWarnings))
				{
					$pdf->AddPage('P', array(102, 153), false, false);
					$localSmallFont = 8;					
					$localRegularCellHeight = 5;
					
					$pdf->SetFont('Arial','B',$localSmallFont);
					foreach($ProductsWarnings as $ProductsWarning)
					{
						$pdf->Cell(82,$localRegularCellHeight,$ProductsWarning,0,1,'L');
					}
				}
				
				$pdf->Output($pdf_path);
				$result[] = $pdf_path2;
			}
			if (!empty($result))
			{
				return $result;
			} else
			{
				error_msg("Can not generate any pdf!");
				send_error();
			}
	}catch (Exception $e) {
		error_msg($e->getMessage());
		send_error();
	}
}

//Generate pdf files for each PO number with separate pages for each box number with barcodes and info
function homePdf($arr, $pro10, $barcodeBill, $originCarrierName, $ProductsWarnings)
{
//print_r($arr);
	try{
			if ($barcodeBill != 'none')
			{
				$barcodeBill = $barcodeBill.'.png';
			}
			$PONumbers = array();
			$result = array();
			
			foreach ($arr as $row)
			{
				$PONumbers[] = $row['PONumber'];
			}
			$c = count($PONumbers);
			
			for ($i=0; $i<$c; $i++)
			{
				$pdf_path = "../Pdf/".repPo($PONumbers[$i]).".pdf";
				$pdf_path2 = "Pdf/".repPo($PONumbers[$i]).".pdf";
				
				$pdf = new PDF('P','mm',array(globalWidth, globalHeight));
				$pdf->SetAutoPageBreak(true, 1);
				$pdf->SetMargins(globalMargins,globalMargins,globalMargins);
				$pdf->SetLineWidth(globalLineWidth);
				
				foreach ($arr as $row)
				{
				if ($row['quantity_in_box'] == 1) $row['quantity_in_box'] = 0;
				for ($qib = 0; $qib <= $row['quantity_in_box']; $qib++)
				{
					if ($row['PONumber'] == $PONumbers[$i])
					{
						$logo = '../Include/logo/logo.png';
						$PONumber = 'PO Number: '.checkPO($row['PONumber'], $row['RefNumber'], $row['original_po']);
						$ProductCode = 'SKU: '.$row['ProductCode'];
						
						$boxes = (integer)$row['boxes'];
						$boxNumber = (integer)$row['boxNumber'];
						if ($boxes > 1)
						{
							$text = "This box is part of a group.";
							$text1 = "This is box:";
							$BoxCarton = $boxNumber.' of '.$boxes;
						} else
						{
							$text = "This is a single box.";
							$text1 = "This is box:";
							$BoxCarton = $boxNumber.' of '.$boxes;
						}
						$contact = 'For questions or concerns regarding this order';
						$contact1 = 'Please contact us at 866-731-8378';
						
						//$pdf->AddPage();
						$pdf->AddPage('P', array(102, 153), false, false);
						/* Update PDF to 4" x 6" */
						$localMainFont = 11;
						$localBigFont = 52;
						$localSmallFont = 8;
						$localCarrierFont = 7;
						$localRegularCellHeight = 6;
						$localBigCellHeight = 22;
						$localSmallCellHeight = 5;
						$localBottomContentY = 120;
                                                
						$pdf->SetFont('Arial','B',$localMainFont);
								
								//SKU Code
								$pdf->Cell(82,$localRegularCellHeight,$ProductCode,0,1,'L');
								//$pdf->Ln();
								//PO #
								$pdf->Cell(82,$localRegularCellHeight,$PONumber,0,1,'L');

								//text
								$pdf->SetFont('Arial','',$localMainFont);
								$pdf->Ln();
								$pdf->Cell(82,$localSmallCellHeight,$text,0,1,'C');
								$pdf->Cell(82,$localSmallCellHeight,$text1,0,1,'C');

								//Box Number / Carton Number
								$pdf->SetFont('Arial','B',$localBigFont);
								$pdf->Cell(82,$localBigCellHeight,$BoxCarton,0,1,'C');
								
								$pdf->SetFont('Arial','B',$localSmallFont);
								$pdf->Cell(82,2,'_________________________________________________',0,1,'C');
									
								//Dreamline Logo
                                $pdf->Cell(12);
                                $actual_position_y = $pdf->GetY()+3;
                                $pdf->Image($logo, 30, $actual_position_y, 40);
                                $pdf->SetXY(0, $actual_position_y+15);
								//contact
								$pdf->SetFont('Arial','',$localMainFont);
								$pdf->Ln(2);
								$pdf->Cell(82,$localSmallCellHeight,$contact,0,1,'C');
								$pdf->SetFont('Arial','B',$localMainFont);
								$pdf->Cell(82,$localSmallCellHeight,$contact1,0,1,'C');
                                                                if ($row['CustomerRef_FullName'] == 'AmazonDS') 
								{
									$pdf->Ln(2);
									$pdf->Cell(82,$localSmallCellHeight,'Amazon DS',0,1,'C');
								}
								if ($pro10 != 'none' && $barcodeBill != 'none')
								{
									$pdf->Line(11, $localBottomContentY, 92, $localBottomContentY);
									$pdf->SetXY(15, $localBottomContentY);
									$pdf->Ln();
									$pdf->SetFont('Arial','B',$localMainFont);
									$actual_position_y = $pdf->GetY();
									$pdf->SetXY(34, $actual_position_y);  // 78 => 0.41 => 34 
									$pdf->Cell(58, 20, " ", 1, 1, 'C');
									$pdf->SetXY(36, $actual_position_y+1);
									$pdf->Cell(58,$localSmallCellHeight,"PRO# ".$pro10,0,1,'L');
									$pdf->SetXY(37, $actual_position_y+6);
									$pdf->Image($barcodeBill, NULL, NULL, -165);
									if (isCarrierNameCorrect($originCarrierName))
									{
										$carrierLogo = getCarrierLogo($originCarrierName);
										$pdf->Image($carrierLogo, 11, $localBottomContentY+4, 21, 14, 'PNG');
										$pdf->SetFont('Arial','B',$localCarrierFont);
										$pdf->SetXY(10, $localBottomContentY+20);
										$pdf->Cell(92,$localSmallCellHeight,$originCarrierName,0,1,'L');
									}
								}/* else
								{
									if (isCarrierNameCorrect($originCarrierName))
									{
										$pdf->Line(11, globalBottomContentY, 198, globalBottomContentY);
										$carrierLogo = getCarrierLogo($originCarrierName);
										$pdf->Image($carrierLogo, 11, globalBottomContentY+4, 90, 60, 'PNG');
										
										$pdf->SetFont('Arial','',globalCarrierFont);
										$pdf->SetXY(100, globalBottomContentY+10);
										$pdf->Cell(190,globalSmallCellHeight,$originCarrierName,0,1,'L');
									}
								}*/
					}
					}
				}
				if ($pro10 != 'none' && $barcodeBill != 'none')
				{
					$pagesNum = count($arr) + 2;
					for($pn = 0; $pn < $pagesNum; $pn++)
					{
						//$pdf->AddPage();
						$pdf->AddPage('P', array(102, 153), false, false);
						/* Update PDF to 4" x 6" */
						$localMainFont = 11;
						$localBigFont = 52;
						$localSmallFont = 8;
						$localRegularCellHeight = 6;
						$localBigCellHeight = 22;
						$localSmallCellHeight = 5;
						$localBottomContentY = 120;
                                                
						$pdf->SetFont('Arial','B',$localSmallFont);
                        if (extrCarrier($originCarrierName) == "RL")
						{
							$pdf->SetXY(24, 40);
							$pdf->Cell(58,$localSmallCellHeight, $arr[0]['ShipAddress_Addr1'],0,1,'L');
							$actual_position_y = $pdf->GetY();
							$pdf->SetXY(24, $actual_position_y);
							$pdf->Cell(58,$localSmallCellHeight, $arr[0]['ShipAddress_Addr2'].", - ",0,1,'L');
							$actual_position_y = $pdf->GetY();
							$pdf->SetXY(24, $actual_position_y);
							$pdf->Cell(58,$localSmallCellHeight, $arr[0]['ShipAddress_City'].", ".$arr[0]['ShipAddress_State'].", ".$arr[0]['ShipAddress_PostalCode'],0,1,'L');
							$actual_position_y = $pdf->GetY();
							$pdf->SetXY(24, $actual_position_y);
							$pdf->Cell(58,$localSmallCellHeight, $arr[0]['FOB'],0,1,'L');
						}
 						$pdf->SetFont('Arial','B',$localMainFont);
						//$actual_position_y = $pdf->GetY();
						$actual_position_y = 62; // 120 => 0.4040 => 62
						$pdf->SetXY(23, $actual_position_y);
						$pdf->Cell(58, 20, " ", 1, 1, 'C');
						$pdf->SetXY(25, $actual_position_y+1);
						$pdf->Cell(58,$localSmallCellHeight,"PRO# ".$pro10,0,1,'L');
						$pdf->SetXY(26, $actual_position_y+7);
						$pdf->Image($barcodeBill, NULL, NULL, -165);
						if (isCarrierNameCorrect($originCarrierName))
						{
							$carrierLogo = getCarrierLogo($originCarrierName);
							$pdf->Image($carrierLogo, 24, 84, 0, 17, 'PNG'); // 47x165 -- 0x17
									
							$pdf->SetFont('Arial','B',$localSmallFont);
							$pdf->SetXY(24, 103);
							$pdf->Cell(82,$localSmallCellHeight,$originCarrierName,0,1,'L');
						}
					}
				}
				if(!empty($ProductsWarnings))
				{
					//$pdf->AddPage();
                                        $pdf->AddPage('P', array(102, 153), false, false);
                                        /* Update PDF to 4" x 6" */
                                        $localSmallFont = 8;
                                        $localRegularCellHeight = 6;
					$pdf->SetFont('Arial','B',$localSmallFont);
					foreach($ProductsWarnings as $ProductsWarning)
					{
						$pdf->Cell(82,$localRegularCellHeight,$ProductsWarning,0,1,'L');
					}
				}
				
				$pdf->Output($pdf_path);
				$result[] = $pdf_path2;
			}
			if (!empty($result))
			{
				return $result;
			} else
			{
				error_msg("Can not generate any pdf!");
				send_error();
			}
	}catch (Exception $e) {
		error_msg($e->getMessage());
		send_error();
	}
}

//Generate pdf files for each PO number with separate pages for each box number with barcodes and info
function homeDCPdf_pickinglist($arr)
{
	try{
			$result = array();
				
						$pdf_path = "../Pdf/".repPo($arr[0]['PONumber'])."PL.pdf";
						$pdf_path2 = "Pdf/".repPo($arr[0]['PONumber'])."PL.pdf";
						
						$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
						$pdf->SetPrintHeader(false);
						$pdf->SetPrintFooter(false);
						$pdf->SetMargins(11, 11);
						$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
					
						$pdf->AddPage('P', array(210, 350), false, false);
						$pdf->SetFont('helvetica', '', 8);
				
				$last_id = $arr[0]['TxnLineID'];
				$tbl = '<table border="0" width="670" cellpadding="2" align="center">
						<tr>
							<td colspan="1"><img id="img1" src="../Include/pictures/DreamLineLogo_Color_final-01.png"></td>
							<td colspan="1"><font size="20"><br><strong>Picking List</strong></font></td>
							<td colspan="1"><font size="12"><br><br><strong>PO: '.$arr[0]['PONumber'].'</strong></font></td>
						</tr>
						</table><br>_______________________________________________________________________________________________________________________';
				foreach($arr as $key => $row)
				{
					if (empty($row['Quantity'])) $row['Quantity'] = '1';
					if ($last_id != $row['TxnLineID'] || $key == 0)
					{
						$tbl.= '
						<br>
						<br>
						<br>
						<table nobr="true" border="2" width="670" cellpadding="2" align="center">
						<tr>
							<td colspan="2"><strong>Product SKU</strong></td>
							<td colspan="6"><strong>Description</strong></td>
							<td colspan="1"><strong>QTY</strong></td>
							<td colspan="1"><strong>Picked</strong></td>
						</tr>
						<tr>
							<td colspan="2"><strong>'.$row['ItemGroupRef_FullName'].'</strong></td>
							<td colspan="6"><strong>'.$row['Prod_desc'].'</strong></td>
							<td colspan="1"><strong>'.$row['Quantity'].'</strong></td>
							<td colspan="1"><img id="img1" src="../Include/pictures/checkboxfalse.png" width="12" height="12"></td>
						</tr>
						</table>
						<br>
						<br>
						<table width="670">
						<tr>
							<td width="135"></td>
							<td>
								<table align="center" nobr="true" border="1" width="535" cellpadding="2">
								<tr>
									<td colspan="8">Item Name</td>
									<td colspan="1">QTY</td>
									<td colspan="1">Picked</td>
								</tr>
								<tr>
									<td colspan="8">'.$row['ItemRef_FullName'].'</td>
									<td colspan="1">'.$row['item_quantity'].'</td>
									<td colspan="1"><img id="img1" src="../Include/pictures/checkboxfalse.png" width="12" height="12"></td>
								</tr>
								</table>
							</td>
						</tr>
						</table><br>
						';
					} else{
						$tbl.= '
						<table width="670">
						<tr>
							<td width="135"></td>
							<td>
								<table align="center" nobr="true" border="1" width="535" cellpadding="2">
								<tr>
									<td colspan="8">'.$row['ItemRef_FullName'].'</td>
									<td colspan="1">'.$row['item_quantity'].'</td>
									<td colspan="1"><img id="img1" src="../Include/pictures/checkboxfalse.png" width="12" height="12"></td>
								</tr>
								</table>
							</td>
						</tr>
						</table><br>
						';
					}
					$last_id = $row['TxnLineID'];
				}
				$pdf->writeHTML($tbl, true, false, false, false, '');
				
				/*$pdf->AddPage('P', array(210, 350), false, false);
				$pdf->SetFont('helvetica', '', 8);
				$tbl = '';
				$pdf->writeHTML($tbl, true, false, false, false, '');*/
				
				
				//Packing List
				$pdf->AddPage('P', array(210, 350), false, false);
				$pdf->SetFont('helvetica', '', 8);
				$last_id = $arr[0]['TxnLineID'];
				$tbl = '<table border="0" width="670" cellpadding="2" align="center">
						<tr>
							<td colspan="1"><img id="img1" src="../Include/pictures/DreamLineLogo_Color_final-01.png"></td>
							<td colspan="1"><font size="20"><br><strong>Packing List</strong></font></td>
							<td colspan="1"><font size="12"><br><br><strong>PO: '.$arr[0]['PONumber'].'</strong></font></td>
						</tr>
						</table><br>_______________________________________________________________________________________________________________________';
				foreach($arr as $key => $row)
				{
					if (empty($row['Quantity'])) $row['Quantity'] = '1';
					if ($last_id != $row['TxnLineID'] || $key == 0)
					{
						$tbl.= '
						<br>
						<br>
						<br>
						<table nobr="true" border="2" width="670" cellpadding="2" align="center">
						<tr>
							<td colspan="2"><strong>Product SKU</strong></td>
							<td colspan="6"><strong>Description</strong></td>
							<td colspan="1"><strong>QTY</strong></td>
							<td colspan="1"><strong>Packed</strong></td>
						</tr>
						<tr>
							<td colspan="2"><strong>'.$row['ItemGroupRef_FullName'].'</strong></td>
							<td colspan="6"><strong>'.$row['Prod_desc'].'</strong></td>
							<td colspan="1"><strong>'.$row['Quantity'].'</strong></td>
							<td colspan="1"><img id="img1" src="../Include/pictures/checkboxfalse.png" width="12" height="12"></td>
						</tr>
						</table>
						<br>
						<br>
						<table width="670">
						<tr>
							<td width="135"></td>
							<td>
								<table align="center" nobr="true" border="1" width="535" cellpadding="2">
								<tr>
									<td colspan="8">Item Name</td>
									<td colspan="1">QTY</td>
									<td colspan="1">Packed</td>
								</tr>
								<tr>
									<td colspan="8">'.$row['ItemRef_FullName'].'</td>
									<td colspan="1">'.$row['Quantity']/$row['item_quantity'].'</td>
									<td colspan="1"><img id="img1" src="../Include/pictures/checkboxfalse.png" width="12" height="12"></td>
								</tr>
								</table>
							</td>
						</tr>
						</table><br>
						';
					} else{
						$tbl.= '
						<table width="670">
						<tr>
							<td width="135"></td>
							<td>
								<table align="center" nobr="true" border="1" width="535" cellpadding="2">
								<tr>
									<td colspan="8">'.$row['ItemRef_FullName'].'</td>
									<td colspan="1">'.$row['Quantity']/$row['item_quantity'].'</td>
									<td colspan="1"><img id="img1" src="../Include/pictures/checkboxfalse.png" width="12" height="12"></td>
								</tr>
								</table>
							</td>
						</tr>
						</table><br>
						';
					}
					$last_id = $row['TxnLineID'];
				}
				$pdf->writeHTML($tbl, true, false, false, false, '');
						
				$pdf->Output($pdf_path, 'F');
				$result[] = $pdf_path2;
				
			if (!empty($result))
			{
				return $result;
			} else
			{
				error_msg("Can not generate any pdf!");
				send_error();
			}
	}catch (Exception $e) {
		error_msg($e->getMessage());
		send_error();
	}
}

//Generate pdf files for each PO number with separate pages for each box number with barcodes and info
function homeDCPdf_label($arr, $arr2)
{
	/*print_r('<pre>');
	print_r($arr);
	print_r('</pre>');
	die();*/
	try{
			$result = array();
				
				$packing_list = true;
                                $picking_list = true;
						$pdf_path = "../Pdf/".repPo($arr[0]['PONumber'])."flyer_".$arr2[0]['TxnLineID'].".pdf";
						$pdf_path2 = "Pdf/".repPo($arr[0]['PONumber'])."flyer_".$arr2[0]['TxnLineID'].".pdf";
						$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
						$pdf->SetPrintHeader(false);
						$pdf->SetPrintFooter(false);
						$pdf->SetMargins(5, 5);
						$pdf->SetAutoPageBreak(FALSE, 0);
						$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
				foreach ($arr as $key => $row)
				{
						
					if($picking_list)
					{
					$pdf->AddPage('L', array(102, 153, 'Rotate'=>-90), false, false);
						$pdf->SetFont('helvetica', '', 8);
						/*$pdf->StartTransform();
						$pdf->Rotate(90, 75, 75);*/
				
				$last_id = $arr2[0]['TxnLineID'];
				$tbl = '<table border="0" width="500" cellpadding="2" align="center">
						<tr>
							<td colspan="1"><img width="100" id="img1" src="../Include/pictures/DreamLineLogo_Color_final-01.png"></td>
							<td colspan="1"><font size="10"><strong>Picking List</strong></font></td>
							<td colspan="1"><font size="10"><strong>PO: '.$arr2[0]['PONumber'].'</strong></font></td>
						</tr>
						</table><br>_______________________________________________________________________________';
				foreach($arr2 as $key2 => $row2)
				{
					if (empty($row2['Quantity'])) $row2['Quantity'] = '1';
					if ($last_id != $row2['TxnLineID'] || $key2 == 0)
					{
						$tbl.= '
						<br>
						<table nobr="true" border="2" width="500" cellpadding="1" align="center">
						<tr>
							<td colspan="2"><strong>Product SKU</strong></td>
							<td colspan="6"><strong>Description</strong></td>
							<td colspan="1"><strong>QTY</strong></td>
							<td colspan="1"><strong>Picked</strong></td>
						</tr>
						<tr>
							<td colspan="2"><strong>'.$row2['ItemGroupRef_FullName'].'</strong></td>
							<td colspan="6"><strong>'.$row2['Prod_desc'].'</strong></td>
							<td colspan="1"><strong>'.$row2['Quantity'].'</strong></td>
							<td colspan="1"><img id="img1" src="../Include/pictures/checkboxfalse.png" width="10" height="10"></td>
						</tr>
						</table>
						<br>
						<br>
						<table width="500">
						<tr>
							<td width="25"></td>
							<td>
								<table align="center" nobr="true" border="1" width="475" cellpadding="1">
								<tr>
									<td colspan="8">Item Name</td>
									<td colspan="1">QTY</td>
									<td colspan="1">Picked</td>
								</tr>
								<tr>
									<td colspan="8">'.extrSinglePc($row2['ItemRef_FullName']).'</td>
									<td colspan="1">'.$row2['item_quantity'].'</td>
									<td colspan="1"><img id="img1" src="../Include/pictures/checkboxfalse.png" width="10" height="10"></td>
								</tr>
								</table>
							</td>
						</tr>
						</table><br>
						';
					} else{
						$tbl.= '
						<table width="500">
						<tr>
							<td width="25"></td>
							<td>
								<table align="center" nobr="true" border="1" width="475" cellpadding="1">
								<tr>
									<td colspan="8">'.$row2['ItemRef_FullName'].'</td>
									<td colspan="1">'.$row2['item_quantity'].'</td>
									<td colspan="1"><img id="img1" src="../Include/pictures/checkboxfalse.png" width="10" height="10"></td>
								</tr>
								</table>
							</td>
						</tr>
						</table><br>
						';
					}
					$last_id = $row2['TxnLineID'];
				}
				$pdf->writeHTML($tbl, true, false, false, false, '');
				$picking_list = false;
				}
                                
                                if($packing_list)
					{
					//Packing List
				/*$pdf->AddPage('L', array(102, 51), false, false);
				$pdf->SetFont('helvetica', '', 5);*/
                                
                                $pdf->AddPage('L', array(102, 153, 'Rotate'=>-90), false, false);
				$pdf->SetFont('helvetica', '', 8);
                                
				$last_id = $arr2[0]['TxnLineID'];
				$tbl = '<table border="0" width="500" cellpadding="2" align="center">
						<tr>
							<td colspan="1"><img width="100" id="img1" src="../Include/pictures/DreamLineLogo_Color_final-01.png"></td>
							<td colspan="1"><font size="10"><strong>Packing List</strong></font></td>
							<td colspan="1"><font size="10"><strong>PO: '.$arr2[0]['PONumber'].'</strong></font></td>
						</tr>
						</table><br>_______________________________________________________________________________';
				foreach($arr2 as $key2 => $row2)
				{
					if (empty($row2['Quantity'])) $row2['Quantity'] = '1';
					if ($last_id != $row2['TxnLineID'] || $key2 == 0)
					{
						$tbl.= '
						<br>
						<table nobr="true" border="2" width="500" cellpadding="1" align="center">
						<tr>
							<td colspan="2"><strong>Product SKU</strong></td>
							<td colspan="6"><strong>Description</strong></td>
							<td colspan="1"><strong>QTY</strong></td>
							<td colspan="1"><strong>Packed</strong></td>
						</tr>
						<tr>
							<td colspan="2"><strong>'.$row2['ItemGroupRef_FullName'].'</strong></td>
							<td colspan="6"><strong>'.$row2['Prod_desc'].'</strong></td>
							<td colspan="1"><strong>'.$row2['Quantity'].'</strong></td>
							<td colspan="1"><img id="img1" src="../Include/pictures/checkboxfalse.png" width="10" height="10"></td>
						</tr>
						</table>
						<br>
						<br>
						<table width="500">
						<tr>
							<td width="25"></td>
							<td>
								<table align="center" nobr="true" border="1" width="475" cellpadding="1">
								<tr>
									<td colspan="8">Item Name</td>
									<td colspan="1">QTY</td>
									<td colspan="1">Packed</td>
								</tr>
								<tr>
									<td colspan="8">'.extrSinglePc($row2['ItemRef_FullName']).'</td>
									<td colspan="1">'.$row2['Quantity']/$row2['item_quantity'].'</td>
									<td colspan="1"><img id="img1" src="../Include/pictures/checkboxfalse.png" width="10" height="10"></td>
								</tr>
								</table>
							</td>
						</tr>
						</table>
						';
					} else{
						$tbl.= '
						<table width="500">
						<tr>
							<td width="25"></td>
							<td>
								<table align="center" nobr="true" border="1" width="475" cellpadding="1">
								<tr>
									<td colspan="8">'.$row2['ItemRef_FullName'].'</td>
									<td colspan="1">'.$row2['Quantity']/$row2['item_quantity'].'</td>
									<td colspan="1"><img id="img1" src="../Include/pictures/checkboxfalse.png" width="10" height="10"></td>
								</tr>
								</table>
							</td>
						</tr>
						</table>
						';
					}
					$last_id = $row2['TxnLineID'];
				}
				$pdf->writeHTML($tbl, true, false, false, false, '');
				$packing_list = false;
				}
					
					
					if(!empty($row['collection_name']) && !empty($row['drawing']))
					{
						$pdf->AddPage('P', array(102, 153), false, false);
						$pdf->SetFont('helvetica', '', 5);
						$DLOGO   = "../Include/pictures/drlogo.png";
						$HEADER  = $row['collection_name'];
						$DRAWING = "../drawings/".$row['drawing'];
						$QRCODE  = $row['qr_code'];

						$paramArr = $row['dimensions'];
						if (!empty($paramArr))
						{
							$PARAMS   = getPdfParams( $paramArr );
						} else
						{
							$PARAMS = '<tr><td></td></tr>';
						}
						$CODE     = $row['code'];
						$tbl = '
						<table border="0" width="325" cellpadding="1">
						<tr>
							<td colspan="2" align="center"><img height="40" src="'.$DLOGO.'" /></td>
						</tr>
						<tr>
							<td colspan="2" height="50" align="center"><strong><font size="25">'.$HEADER.'</font></strong></td>
						</tr>
						<tr>
							<td colspan="2" align="center"><img height="200" src="'.$DRAWING.'"><br></td>
						</tr>
						<tr>
							<td width="33%"><img src="'.$QRCODE.'" /></td>
							<td width="67%">
								<br><br><font size="10"><strong>'.$row['ItemGroupRef_FullName'].'</strong></font><br>
								<table cellpadding="0" cellpadding="0">
									'.$PARAMS.'
								</table>
							</td>
						</tr>
						<tr>
							<td width="33%">
								<font size="7"> Scan QR code to obtain<br> product information and<br> register your purchase</font>
							</td>
							<td width="67%">
								<font size="8">Visit <strong>www.DreamLine.com/warranty/</strong> and add <br />your code <b>('.$CODE.')</b></font>
							</td>
						</tr>
						</table>
						<table border="0" width="325" cellpadding="0">
						<tr>
							<td align="center">__________________________________<br><font size="8"><strong>DreamLine.com</strong></font></td>
						</tr>
						</table>
						';

						$pdf->writeHTML($tbl, true, false, false, false, '');
					}
					
						
				}
				$pdf->Output($pdf_path, 'F');
						$result[] = $pdf_path2;
			if (!empty($result))
			{
				saveQrCode($arr);
				return $result;
			} else
			{
				error_msg("Can not generate any pdf!");
				send_error();
			}
	}catch (Exception $e) {
		error_msg($e->getMessage());
		send_error();
	}
}

function oshInvoicePDF( $array1, $array2, $isInternational ) {
    try {
// ===================================================================================
            
            $po = $array1[0]['PONumber'];
            
            $a1 = $array1[0]['Addr1'] ? ($array1[0]['Addr1']."<br />") : "" ;
            $a2 = $array1[0]['Addr2'] ? ($array1[0]['Addr2']."<br />") : "" ;
            $a3 = $array1[0]['Addr3'] ? ($array1[0]['Addr3']."<br />") : "" ;
            $a4 = $array1[0]['City'] ? ($array1[0]['City']) : "";
            $a4 .= $array1[0]['State'] ? ( $a4=="" ? "" : ", " ).$array1[0]['State'] : "";
            $a4 .= $array1[0]['Zip'] ? ( $a4=="" ? "" : " " ).$array1[0]['Zip'] : "";
            $a4 .= "<br />" . ( $a1==""?"<br />":"" ) . ( $a2==""?"<br />":"" ) . ( $a3==""?"<br />":"" );
            	    
            $shipDate = new DateTime( $array1[0]['Actual_Ship_Date'] ? $array1[0]['Actual_Ship_Date'] : ($array1[0]['Ship_Date'] ? $array1[0]['Ship_Date'] : "") );
            $shipMethod = $array1[0]['Actual_Ship_Method'] ? $array1[0]['Actual_Ship_Method'] : $array1[0]['Ship_Method'];            
            $invoiceDate = new DateTime( $array1[0]['Invoice_Date'] );
            
            $pdf_path = "../PdfInvoice/".repPo($po)."_invoice.pdf";
            $pdf_path2 = "PdfInvoice/".repPo($po)."_invoice.pdf";
            $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
            $pdf->SetPrintHeader(false);
            $pdf->SetPrintFooter(false);
            $pdf->SetMargins(11, 11);
            $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
            $pdf->SetAutoPageBreak(TRUE, 1);
            $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

            // set font
            $pdf->SetFont('helvetica', 'B', 20);

            // add a page
            $pdf->AddPage();
            $pdf->SetFont('helvetica', '', 8);

            //
            $DRMLN_LOGO = 'logo.png';

            $no_margins = array(
                0 => array('h' => '', 'n' => 0),
                1 => array('h' => '', 'n' => 0)
            );
            $pdf->setHtmlVSpace(array(    
                'ul'  => $no_margins   
            ));
            $pdf->setListIndentWidth(3);
			
            // Table with rowspans and THEAD
            $tblNI = '
            <table width="670" cellspacing="0" cellpadding="2">
                    <tr>
                            <td width="66%" rowspan="4" align="left"><br /><br /><br /><br /><br /><br /><br /><br />75 Hawk Road<br />Warminster, PA 18974<br />Tel: (866) 731-8378<br />Fax: (866) 227-9245<br />www.BathAuthority.com<br />orders@BathAuthority.com</td>
                            <td width="34%" colspan="2" align="right"><img src="../Include/logo/logo.png"></td>	
                    </tr>
                    <tr>
                            <td width="34%" colspan="2" align="right"><font size="30"><b>Invoice</b></font></td>	
                    </tr>

                    <tr>
                            <td width="17%" align="center" style="border: 1px solid black; background-color:#ccc;"><b>Invoice</b></td>
                            <td width="17%" align="center" style="border: 1px solid black; background-color:#ccc;"><b>Invoice Date</b></td>
                    </tr>
                    <tr>
                            <td width="17%" align="center" style="border: 1px solid black;">'.$array1[0]["RefNumber"].'</td>
                            <td width="17%" align="center" style="border: 1px solid black;">'.($invoiceDate->format('m/d/Y')).'</td>
                    </tr>	
            </table>
            <table width="670" cellspacing="0" cellpadding="4">
                    <tr>
                            <td width="50%" colspan="3" align="left" style="border: 1px solid black; background-color:#ccc;"><b>          Bill To</b></td>
                            <td width="50%" colspan="3" align="left" style="border: 1px solid black; background-color:#ccc;"><b>          Ship To</b></td>	
                    </tr>
                    <tr>
                            <td width="50%" colspan="3" align="left" style="border: 1px solid black;"><b><br /><br />'.$array1[0]["Customer"].'</b></td>
                            <td width="50%" colspan="3" align="left" style="border: 1px solid black;"><b><br />'.$a1.$a2.$a3.$a4.'<br /></b></td>	
                    </tr>
                    <tr>
                            <td width="20%" align="center" style="border: 1px solid black; background-color:#ccc;"><b>P.O. #</b></td>
                            <td width="20%" align="center" style="border: 1px solid black; background-color:#ccc;"><b>Terms</b></td>
                            <td width="10%" align="center" style="border: 1px solid black; background-color:#ccc;"><b>Rep</b></td>

                            <td width="15%" align="center" style="border: 1px solid black; background-color:#ccc;"><b>Shipped</b></td>
                            <td width="15%" align="center" style="border: 1px solid black; background-color:#ccc;"><b>Ship Via</b></td>
                            <td width="20%" align="center" style="border: 1px solid black; background-color:#ccc;"><b>Tracking #</b></td>		
                    </tr>
                    <tr>
                            <td width="20%" align="center" style="border: 1px solid black;">'.$po.'</td>
                            <td width="20%" align="center" style="border: 1px solid black;">'.$array1[0]["Terms"].'</td>
                            <td width="10%" align="center" style="border: 1px solid black;">'.$array1[0]["Rep"].'</td>

                            <td width="15%" align="center" style="border: 1px solid black;">'.($shipDate->format('m/d/Y')).'</td>
                            <td width="15%" align="center" style="border: 1px solid black;">'.$shipMethod.'</td>
                            <td width="20%" align="center" style="border: 1px solid black;">'.$array1[0]["Tracking"].'</td>		
                    </tr>	
            </table>
            <table width="670" cellspacing="0" cellpadding="2">
                    <tr>
                            <td width="5%" align="center" style="border: 1px solid black; background-color:#ccc;"><b>Qty</b></td>
                            <td width="10%" align="center" style="border: 1px solid black; background-color:#ccc;"><b>Comments</b></td>
                            <td width="35%" align="center" style="border: 1px solid black; background-color:#ccc;"><b>Item</b></td>

                            <td width="24%" align="center" style="border: 1px solid black; background-color:#ccc;"><b>Description</b></td>
                            <td width="13%" align="center" style="border: 1px solid black; background-color:#ccc;"><b>Unit Price</b></td>
                            <td width="13%" align="center" style="border: 1px solid black; background-color:#ccc;"><b>Amount</b></td>		
                    </tr>';
			
			$tblI = '
				<table width="670" cellspacing="0" cellpadding="2">
					<tr><td width="100%"><img width="350" src="../Include/logo/logo.png"></td></tr>
					<tr><td width="100%" style="text-align:center"><font size="20"><b>COMMERCIAL INVOICE</b></font><br/></td></tr>
				</table>
				<table width="670" cellspacing="0" cellpadding="2" style="border: 2px solid black;">
					<tr>
						<td width="50%" style="border: 1px solid black; border-bottom: 2px solid black;">
							<table width="96%" cellspacing="0" cellpadding="2" style="border: 1px solid black;">
								<tr>
									<td style="border-bottom: 1px solid black;"><b>&nbsp;&nbsp;BILL TO</b></td>
								</tr>
								<tr>
									<td style="border-bottom: 2px solid black;">The Home Depot Canada<br />1 Concorde Gate<br />Toronto, ON M3C 4H9<br />Canada</td>
								</tr>
								<tr>
									<td style="border-bottom: 1px solid black;"><b>&nbsp;&nbsp;SHIP TO</b></td>
								</tr>
								<tr>
									<td>'.$a1.$a2.$a3.$a4.'</td>
								</tr>
							</table>
						</td>
						<td width="50%" style="border: 1px solid black; border-bottom: 2px solid black;">
							<table width="100%" cellspacing="0" cellpadding="3">
								<tr>
									<td colspan="2" width="100%" style="border-bottom: 2px solid black;"><font size="8">Bath Authority.LLC DBA DreamLine<br />75 Hawk Road<br />Warminster, PA 18974<br />(P) (866) 731-8378<br />(F) (866) 227-9245<br />www.DreamLine.com<br />Orders@DreamLine.com</font></td>
								</tr>
								<tr>
									<td width="50%" style="border: 1px solid black;"><b>DATE</b></td>
									<td width="50%" style="border: 1px solid black;"><b>INVOICE</b></td>
								</tr>
								<tr>
									<td width="50%" style="border: 1px solid black;">'.($invoiceDate->format('m/d/Y')).'</td>
									<td width="50%" style="border: 1px solid black;">'.$array1[0]["RefNumber"].'</td>
								</tr>
								<tr>
									<td width="50%" style="border: 1px solid black;"><b>PO NUMBER</b></td>
									<td width="50%" style="border: 1px solid black;"><b>PRODUCT ORIGIN</b></td>
								</tr>
								<tr>
									<td width="50%" style="border: 1px solid black;">'.$po.'</td>
									<td width="50%" style="border: 1px solid black;">China</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td width="100%" colspan="2">
							<font size="8">
								<br />
								<table width="100%" cellspacing="0" cellpadding="2">
									<tr>
										<td align="center" width="7%"  style="border: 1px solid black;"><b>QTY</b></td>
										<td align="center" width="25%" style="border: 1px solid black;"><b>PRODUCT MODEL #</b></td>
										<td align="center" width="38%" style="border: 1px solid black;"><b>PRODUCT DESCRIPTION</b></td>
										<td align="center" width="15%" style="border: 1px solid black;"><b>UNIT PRICE</b></td>
										<td align="center" width="15%" style="border: 1px solid black;"><b>TOTAL PRICE<sub> IN U.S.$</sub></b></td>
									</tr>';
			
            
            foreach( $array2 as $item ) {

                    if( strripos( $item['Item'], 'Price-Adjustment' ) !== false ) {
                        $itemName = $item['Item'];
                    } else if( strripos( $item['Item'], 'Freight' ) !== false ) {
                        $itemName = $item['Item'];
                    } else if( strripos( $item['Item'], 'warranty' ) !== false ) {
                        $itemName = $item['Item'];
                    } else if( strripos( $item['Item'], 'Subtotal' ) !== false ) {
                        $itemName = $item['Item'];
                    } else if( strripos( $item['Item'], 'IDSC-10' ) !== false ) {
                        $itemName = $item['Item'];
                    } else if( strripos( $item['Item'], 'DISCOUNT' ) !== false ) {
                        $itemName = $item['Item'];
                    } else if( strripos( $item['Item'], 'SPECIAL ORDER' ) !== false ) {
                        $itemName = $item['Item'];
                    } else if( strripos( $item['Item'], 'Manual' ) !== false ) {
                        $itemName = $item['Item'];
                    } else {
                        $itemName = end( split(':',$item['Item']) );
                    }
                    
                    $tblNI .= '<tr>
                            <td width="5%" align="center" style="border-left:  1px solid black;border-right: 1px solid black;">'.$item['Qty'].'</td>
                            <td width="10%" align="left" style="border-left: 1px solid black;border-right: 1px solid black;">'.$item['Comments'].'</td>
                            <td width="35%" align="left" style="border-left: 1px solid black;border-right: 1px solid black;">'.$itemName.'</td>

                            <td width="24%" align="left" style="border-left: 1px solid black;border-right: 1px solid black;">'.$item['Description'].'</td>
                            <td width="13%" align="center" style="border-left: 1px solid black;border-right: 1px solid black;">'.( $item['UnitPrice']?number_format($item['UnitPrice'],2,'.',''):"" ).'</td>
                            <td width="13%" align="center" style="border-left: 1px solid black;border-right: 1px solid black;">'.( $item['Amount']?number_format($item['Amount'],2,'.',''):"" ).'</td>		
                    </tr>';
					
					$tblI .= '<tr>
						<td width="7%"  align="center" style="border-left: 1px solid black;border-right: 1px solid black;">'.$item['Qty'].'</td>						
						<td width="25%" align="center" style="border-left: 1px solid black;border-right: 1px solid black;">'.$itemName.'</td>						
						<td width="38%" align="center" style="border-left: 1px solid black;border-right: 1px solid black;">'.$item['Description'].'</td>
						<td width="15%" align="center" style="border-left: 1px solid black;border-right: 1px solid black;">'.( $item['UnitPrice']?number_format($item['UnitPrice'],2,'.',''):"" ).'</td>
						<td width="15%" align="center" style="border-left: 1px solid black;border-right: 1px solid black;">'.( $item['Amount']?number_format($item['Amount'],2,'.',''):"" ).'</td>		
					</tr>';

            }
            if( $array1[0]['SalesTaxName'] ) {
                $tblNI .= '<tr>
                            <td width="5%" align="center" style="border-left:  1px solid black;border-right: 1px solid black;"> </td>
                            <td width="10%" align="left" style="border-left: 1px solid black;border-right: 1px solid black;"> </td>
                            <td width="35%" align="left" style="border-left: 1px solid black;border-right: 1px solid black;"> </td>

                            <td width="24%" align="left" style="border-left: 1px solid black;border-right: 1px solid black;">'.$array1[0]['SalesTaxName'].' sale, exempt from sales tax</td>
                            <td width="13%" align="center" style="border-left: 1px solid black;border-right: 1px solid black;">'.number_format($array1[0]['SalesTaxPercent'],2,'.','').'%</td>
                            <td width="13%" align="center" style="border-left: 1px solid black;border-right: 1px solid black;">'.number_format($array1[0]['SalesTaxAmount'],2,'.','').'</td>		
                    </tr>';
            }
			
            $tblNI .= '
                <tr>
                        <td width="5%" align="center" style="border:  1px solid black;"> </td>
                        <td width="10%" align="center" style="border: 1px solid black;"> </td>
                        <td width="35%" align="center" style="border: 1px solid black;"> </td>

                        <td width="24%" align="center" style="border: 1px solid black;"> </td>
                        <td width="13%" align="center" style="border: 1px solid black;"> </td>
                        <td width="13%" align="center" style="border: 1px solid black;"> </td>		
                </tr>
                <tr>
                        <td width="5%" align="center" style="border:  1px solid black;"> </td>
                        <td width="10%" align="center" style="border: 1px solid black;"> </td>
                        <td width="35%" align="center" style="border: 1px solid black;"> </td>

                        <td width="24%" align="center" style="border: 1px solid black;"> </td>
                        <td width="13%" align="center" style="border: 1px solid black;"> </td>
                        <td width="13%" align="center" style="border: 1px solid black;"> </td>		
                </tr>
            </table>
            <font size="3"><br /></font>
            <table width="670" cellspacing="0" cellpadding="4">
                    <tr>
                            <td width="100%" align="left" colspan="3" style="border: 1px solid black; background-color:#ccc;"><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;THANK YOU FOR YOUR PURCHASE</b></td>
                    </tr>
                    <tr>
                            <td width="74%" align="left" rowspan="5" style="border: 1px solid black;">For questions concerning invoices or statements, please contact:<br /><br />Phone: 866-731-8378<br />Fax: 866-227-9245<br />Email: accounting@bathauthority.com</td>
                            <td width="13%" align="left"></td>
                            <td width="13%" align="left"></td>	
                    </tr>
                    <tr>
                            <td width="13%" align="left" style="border: 1px solid black; background-color:#ccc;"><b>Sales Tax</b></td>
                            <td width="13%" align="left"></td>	
                    </tr>
                    <tr>
                            <td width="13%" align="left" style="border: 1px solid black; background-color:#ccc;"><b>Total</b></td>
                            <td width="13%" align="right" style="border: 1px solid black;"><b>$'.number_format($array1[0]['Subtotal'],2,'.','').'</b></td>	
                    </tr>
                    <tr>
                            <td width="13%" align="left" style="border: 1px solid black; background-color:#ccc;"><font size="7"><b>Payments/Credits</b></font></td>
                            <td width="13%" align="right" style="border: 1px solid black;"><b>'.( $array1[0]['AppliedAmount']<0 ? '-$'.number_format(abs($array1[0]['AppliedAmount']),2,'.','') : number_format($array1[0]['AppliedAmount'],2,'.','') ).'</b></td>
                    </tr>
                    <tr>
                            <td width="13%" align="left" style="border: 1px solid black; background-color:#ccc;"><b>Balance Due</b></td>
                            <td width="13%" align="right" style="border: 1px solid black;"><b>$'.number_format($array1[0]['BalanceRemaining'],2,'.','').'</b></td>	
                    </tr>
                    <tr>
                            <td width="100%" align="left" colspan="3" style="border: 1px solid black; background-color:#ccc;"><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;RETURN POLICY</b></td>
                    </tr>

                    <tr>
                            <td width="100%" align="left" colspan="3" style="border: 1px solid black;">Your purchase constitutes acceptance of Bath Authority, LLC "Shipping and Return" policies. To receive a credit or replacement the following conditions must be met:<ul>
                                    <li>All returned merchandise must be accompanied by a Bath Authority issued RMA number. An RMA number must be obtained within 30 days from date of product delivery and is valid for 7 days.</li>
                                    <li>There is a minimum twenty-five percent (25%) restocking charge and the cost of all freight charges for all returned goods.</li>
                                    <li>All returned merchandise must be new and in resalable condition - not previously installed.</li>
                                    <li>All items must be in their original packaging. No refund will be given if the product arrives broken or damaged. There can not be any damage to or writing on the packaging of the item(s) being returned.</li>
                                    <li>Packages must be sent back fully insured and via an approved Bath Authority carrier. Bath Authority is not responsible for lost freight or damages caused by the shipper. All claims must be made through the shipping party.</li>
                            </ul>
                            </td>
                    </tr>

            </table>';
			
			$tblI .= '<tr>
						<td width="70%" align="center" style="border-top: 1px solid black;" colspan="3">&nbsp;</td>
						<td width="15%" align="center" style="border: 1px solid black;"><b>SUBTOTAL:</b></td>
						<td width="15%" align="center" style="border: 1px solid black;"><b>$ '.number_format($array1[0]['Subtotal'],2,'.','').'</b></td>
					</tr>
					<tr>
						<td width="70%" align="center" colspan="3">&nbsp;</td>
						<td width="15%" align="center" style="border: 1px solid black;"><b>DISCOUNT:</b></td>
						<td width="15%" align="center" style="border: 1px solid black;"><b>$ 0.00</b></td>
					</tr>
					<tr>
						<td width="70%" align="left" colspan="3">"WE HEREBY CERTIFY THIS INVOICE TO BE TRUE AND CORRECT."</td>
						<td width="15%" align="center" style="border: 1px solid black;"><b>TOTAL:</b></td>
						<td width="15%" align="center" style="border: 1px solid black;"><b>$ '.number_format($array1[0]['Subtotal'],2,'.','').'</b></td>
					</tr>
					<tr>
						<td width="70%" align="left" colspan="3" style="border-bottom: 1px solid black;"><br /><br /><br /><br /></td>
						<td width="30%" align="left" colspan="2" >&nbsp;</td>
					</tr>
				</table>
			</font>
		</td>
	</tr>
	<tr>
		<td width="100%" colspan="2" align="center"><font size="5"><br />THESE COMMODITIES, TECHNOLOGY OR SOFTWARE WERE EXPORTED FROM THE UNITED STATES IN ACCORDANCE WITH THE EXPORT REGULATIONS. DIVERSION CONTRARY TO U.S. LAW PROHIBITED.</font></td>
	</tr>
</table>';

			
			
            $pdf->writeHTML( ($isInternational ? $tblI : $tblNI ), true, false, false, false, '');
            
            if( $array1[0]["TimeCreated"] && !$isInternational ) {
                    $ImageW = 200; //WaterMark Size
                    $ImageH = 100;

                    $pdf->setPage( 1 ); //WaterMark Page    

                    $myPageWidth = $pdf->getPageWidth();
                    $myPageHeight = $pdf->getPageHeight();

                    $myX = floor( ( $myPageWidth / 2 )  - ($ImageW/2) );  //WaterMark Positioning
                    $myY = floor( ( $myPageHeight / 2 ) - ($ImageH/2) );

                    $myX = 65;
                    $myY = 55;

                    $pdf->StartTransform();
                    $pdf->Rotate(30, $myX, $myY);

                    $pdf->SetAlpha(0.45);

                    $datePaid = $array1[0]["TimeCreated"];

                    $imageWidth = 200;		
                    $imageHeight = 100;

                    $im  = imagecreate($imageWidth, $imageHeight);
                    $bgc = imagecolorallocatealpha($im, 255, 255, 255, 127);
                    $tc  = imagecolorallocatealpha($im, 0, 0, 255, 0);
                    imagefilledrectangle($im, 0, 0, $imageWidth, $imageHeight, $bgc);
                    $text = 'PAID';		
                    $font = '../Include/fonts/STENCIL.TTF';
                    imagettftext($im, 63, 0, 5, 63, $tc, $font, $text);
                    if( isset($datePaid) && !empty($datePaid) ) {
                            $paidDate = new DateTime( $datePaid );
                    } else {
                            $paidDate = new DateTime();
                    }		
                    $text = $paidDate->format( 'm/d/Y' );		
                    imagettftext($im, 27, 0, 5, 92, $tc, $font, $text);

                    ob_start();
                    imagepng($im);
                    $imgData = base64_encode(ob_get_clean());
                    imagedestroy($im);

                    $pdf->Image('@'.base64_decode($imgData), $myX, $myY);
                    $pdf->StopTransform();
            }
            
            //Close and output PDF document
            $pdf->Output($pdf_path, 'F');
            $result[] = $pdf_path2;
            if (!empty($result))
            {
                    return $result;
            } else
            {
                    error_msg("Can not generate any pdf!");
                    send_error();
            }
// ====================================================================================        
    } catch (Exception $e) {
        error_msg($e->getMessage());
        send_error();
    }    
}

function homeDCPdf_bol($array)
{
        //arr
	/*print_r('<pre>');
	print_r($arr);
	print_r('</pre>');
	die();*/
	try{
            
            $page = 0;
            $page_counter = 0;
            $pages = array();
            $total_weight = 0;
            $address = $array[0]['Address'];
            $po = $array[0]['PONumber'];
            
            $PAGE_ONE_LIMIT = 12;
            $PAGE_FULL_LIMIT = 29;
            $PAGE_LAST_LIMIT = 20;
            
	    
            $pdf_path = "../Pdf/".repPo($po)."hddc_bol.pdf";
            $pdf_path2 = "Pdf/".repPo($po)."hddc_bol.pdf";
            $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
            $pdf->SetPrintHeader(false);
            $pdf->SetPrintFooter(false);
            $pdf->SetMargins(11, 11);
            $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
            $pdf->SetAutoPageBreak(TRUE, 1);
            $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
            
                    
            
            foreach( $array as $item ) {
				if (!isset($pages[$page])) $pages[$page] = '';
                if( $page_counter == 0 ) {
                    $pages[$page] .= '<table width="670" cellspacing="0" cellpadding="8" style="border: 2px solid #1F4E79;" >
                                        <tr>
                                            <td width="10%" style="background-color: #DEEAF6; border: 1px solid #1F4E79;" align="center">QTY.</td>
                                            <td width="45%" style="background-color: #DEEAF6; border: 1px solid #1F4E79;" align="center">SKU</td>
                                            <td width="45%" style="background-color: #DEEAF6; border: 1px solid #1F4E79;" align="center">OMSID</td>
                                        </tr>';
                }
                
                $pages[$page] .= '<tr>
                                    <td width="10%" style="border-left: 1px solid #1F4E79; border-right: 1px solid #1F4E79;" align="center">'.$item['Quantity'].'</td>
                                    <td width="45%" style="border-left: 1px solid #1F4E79; border-right: 1px solid #1F4E79;" align="center">'.$item['SKU'].'</td>
                                    <td width="45%" style="border-left: 1px solid #1F4E79; border-right: 1px solid #1F4E79;" align="center">'.$item['HD_SKU'].'</td>
                                </tr>';
                $total_weight += $item['FullWeight'];
                $page_counter += 1;
                
                if( ($page == 0 && $page_counter == $PAGE_ONE_LIMIT) || ($page > 0 && $page_counter == $PAGE_FULL_LIMIT) ) {
                    $pages[$page] .= '</table>';
                    $page += 1;
                    $page_counter = 0;
                }
            }
            $pages[$page] .= '</table>';
            if( $page == 0 || ($page > 0 && $page_counter > $PAGE_LAST_LIMIT) ) {
                $page += 1;
                $page_counter = 0;
                $pages[$page] = '';
            }
            
            $pdf->AddPage();
            $pdf->SetFont('helvetica', '', 10);
            
            $data = '<table width="670" cellspacing="4" cellpadding="5">
                        <tr>
                            <td style="border-left: 4px solid #1F4E79;" width="1px"><font size="1pt"> </font></td>
                            <td style="border-left: 4px solid #1F4E79;" width="15px"><font size="1pt"> </font></td>
                            <td width="654px"><font size="22px"><b style="color:#1F4E79;">BATH AUTHORITY LLC<br />75 HAWK ROAD<br />WARMINSTER, PA 18974<br />866-731-8378<br /></b></font><b style="color:#5B9BD5">Date: '.date('m / d / Y').' </b></td>
                        </tr>
                        <tr>
                            <td width="1px"><font size="1pt"> </font></td>
                            <td width="15px"><font size="1pt"> </font></td>
                            <td width="654px"><font style="color:#1F4E79;" size="16px"><br />BILL OF LADING # ________</font></td>
                        </tr>
                        </table>

                        <table width="670" cellspacing="0" cellpadding="8" >
                            <tr>
                                <td style="background-color: #DEEAF6; border-top: 2px solid #1F4E79; border-bottom: 2px solid #1F4E79; border-left:  2px solid #1F4E79; border-right: 1px solid #1F4E79;">Bill To</td>
                                <td style="background-color: #DEEAF6; border-top: 2px solid #1F4E79; border-bottom: 2px solid #1F4E79; border-right: 2px solid #1F4E79; border-left:  1px solid #1F4E79;">Ship To</td>
                            </tr>
                        <tr>
                            <td style="border-bottom: 1px solid #1F4E79; border-left:  2px solid #1F4E79; border-right: 1px solid #1F4E79;">Home Depot Direct c/o Transit Technologies, Inc.<br />360 W Butterfield Rd. Ste 400<br />Elmurst, IL 60126<br /></td>
                            <td style="border-bottom: 1px solid #1F4E79; border-right:  2px solid #1F4E79; border-left: 1px solid #1F4E79;">'.$address.'</td>
                        </tr>
                        <tr>
                            <td style="border-bottom: 1px solid #1F4E79; border-left:  2px solid #1F4E79; border-right: 1px solid #1F4E79;">Shipment ID<br /><br />____________________</td>
                            <td style="border-bottom: 2px solid #1F4E79; border-right:  2px solid #1F4E79; border-left: 1px solid #1F4E79;">
                                <table cellspaceing="0" cellpadding="5">
                                    <tr>
                                        <td width="75%">Freight Terms (FOB Destination)</td>
                                        <td width="25%" align="center">3<sup>rd</sup> Party</td>
                                    </tr>
                                    <tr>
                                        <td width="75%"> </td>
                                        <td width="25%" align="center">
                                            <table cellspacing="0" cellpadding="3" width="100%"><tr><td width="33%"> </td><td width="34%" align="center" border="1"><b>X</b></td><td width="33%"> </td></tr></table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td rowspan="2" style="border-bottom: 2px solid #1F4E79; border-left:  2px solid #1F4E79; border-right: 1px solid #1F4E79;">PO #<br /><br /><font size="14">'.$po.'</font></td>
                            <td style="border-bottom: 2px solid #1F4E79; border-right:  2px solid #1F4E79; border-left: 1px solid #1F4E79;">Commodity Description<br /><br />____________________</td>
                        </tr>
                        <tr>
                            <td style="border-bottom: 2px solid #1F4E79; border-right:  2px solid #1F4E79; border-left: 1px solid #1F4E79;">Skid Total '.ceil( $total_weight / 2500 ).'</td>
                        </tr>
                        </table>
                        <br /><br />';
            $data .= $pages[0];
            $pdf->writeHTML($data, true, false, false, false, '');
            for( $i=1; $i<=$page; $i++ ) {
                $pdf->AddPage();
                $pdf->SetFont('helvetica', '', 10);
                $data = $pages[$i];
                
                if( $i == $page ) {
                    $data .= '<br /><br /><br />
                            <table width="670" cellspacing="3" cellpadding="0">
                                <tr><td>
                                    <table width="100%" cellspacing="0" cellpadding="5">
                                        <tr>
                                            <td border="1"><font size="9">Where the rate is dependent on value, shippers are required to state specifically in writing the agreed or declared value of the property as follows:<br />"The agreed or declared value of the property is specifically stated by the shipper to be not exceeding _______ per ______"</font></td>
                                        </tr>
                                        <tr>
                                            <td border="1"><font size="9"><b>NOTE:</b> Liability limitation for loss or damage on this shipment shall be applicable. See 49 US. C. - 14706(c)(1)(A) and (B).</font></td>
                                        </tr>
                                    </table>
                                </td></tr>
                                <tr><td>
                                    <table width="100%" cellspacing="0" cellpadding="5">
                                        <tr>
                                            <td colspan="2" border="1"><font size="9"><b>RECEIVED</b>, subject to individually determined rates or contracts that have been agreed upon in writing between the carrier and shipper, if applicable, otherwise to the rates, classifications and rules that have been established by the carrier and are available to the shipper, on request, and to all applicable state and federal regulations.</font></td>
                                        </tr>
                                        <tr>
                                            <td border="1"><font size="13">SHIPPER SIGNATURE / DATE</font><br /><font size="8"><br />This is to certify that the above materials are properly classified, described, packaged, marked and labeled and are in proper condition for transportation according to the applicable regulations of the DOT<br /></font><br /><br /><br />___________________ Shipper&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;______ / 2016 Date</td>
                                            <td border="1"><font size="13">CARRIER SIGNATURE / PICK UP DATE</font><br /><font size="8"><br />Carrier acknowledges receipt of packages and required placards. Carrier certifies emergency response information was made available and/or carrier has the U.S. Dot emergency response guidebook or equivalent documentation in the vehicle</font><br /><br /><br />___________________ Driver&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;______ / 2016 Date</td>
                                        </tr>
                                    </table>
                                </td></tr>
                            </table>';
                }                
                $pdf->writeHTML($data, true, false, false, false, '');
            }
            $pdf->Output($pdf_path, 'F');
            $result[] = $pdf_path2;
            if (!empty($result))
            {
                    return $result;
            } else
            {
                    error_msg("Can not generate any pdf!");
                    send_error();
            }
	}catch (Exception $e) {
		error_msg($e->getMessage());
		send_error();
	}
}


//Generate pdf files for each PO number with separate pages for each box number with barcodes and info
function homeDCPdf_upc_sku($arr, $arr2)
{
	try{
			$result = array();
				$packing_list = false;
				$pdf_path = "../Pdf/".repPo($arr[0]['PONumber'])."barcodes_".$arr2[0]['TxnLineID'].".pdf";
				$pdf_path2 = "Pdf/".repPo($arr[0]['PONumber'])."barcodes_".$arr2[0]['TxnLineID'].".pdf";
				$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
				$pdf->SetPrintHeader(false);
				$pdf->SetPrintFooter(false);
				$pdf->SetMargins(5, 5);
				$pdf->SetAutoPageBreak(FALSE, 0);
				$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
				
				foreach ($arr as $key => $row)
				{
					/*if($packing_list)
					{
					//Packing List

                                
                                $pdf->AddPage('L', array(102, 153, 'Rotate'=>-90), false, false);
				$pdf->SetFont('helvetica', '', 8);
                                
				$last_id = $arr2[0]['TxnLineID'];
				$tbl = '<table border="0" width="500" cellpadding="2" align="center">
						<tr>
							<td colspan="1"><img width="100" id="img1" src="../Include/pictures/DreamLineLogo_Color_final-01.png"></td>
							<td colspan="1"><font size="10"><strong>Packing List</strong></font></td>
							<td colspan="1"><font size="10"><strong>PO: '.$arr2[0]['PONumber'].'</strong></font></td>
						</tr>
						</table><br>_______________________________________________________________________________';
				foreach($arr2 as $key2 => $row2)
				{
					if (empty($row2['Quantity'])) $row2['Quantity'] = '1';
					if ($last_id != $row2['TxnLineID'] || $key2 == 0)
					{
						$tbl.= '
						<br>
						<table nobr="true" border="2" width="500" cellpadding="1" align="center">
						<tr>
							<td colspan="2"><strong>Product SKU</strong></td>
							<td colspan="6"><strong>Description</strong></td>
							<td colspan="1"><strong>QTY</strong></td>
							<td colspan="1"><strong>Packed</strong></td>
						</tr>
						<tr>
							<td colspan="2"><strong>'.$row2['ItemGroupRef_FullName'].'</strong></td>
							<td colspan="6"><strong>'.$row2['Prod_desc'].'</strong></td>
							<td colspan="1"><strong>'.$row2['Quantity'].'</strong></td>
							<td colspan="1"><img id="img1" src="../Include/pictures/checkboxfalse.png" width="10" height="10"></td>
						</tr>
						</table>
						<br>
						<br>
						<table width="500">
						<tr>
							<td width="25"></td>
							<td>
								<table align="center" nobr="true" border="1" width="475" cellpadding="1">
								<tr>
									<td colspan="8">Item Name</td>
									<td colspan="1">QTY</td>
									<td colspan="1">Packed</td>
								</tr>
								<tr>
									<td colspan="8">'.extrSinglePc($row2['ItemRef_FullName']).'</td>
									<td colspan="1">'.$row2['Quantity']/$row2['item_quantity'].'</td>
									<td colspan="1"><img id="img1" src="../Include/pictures/checkboxfalse.png" width="10" height="10"></td>
								</tr>
								</table>
							</td>
						</tr>
						</table>
						';
					} else{
						$tbl.= '
						<table width="500">
						<tr>
							<td width="25"></td>
							<td>
								<table align="center" nobr="true" border="1" width="475" cellpadding="1">
								<tr>
									<td colspan="8">'.$row2['ItemRef_FullName'].'</td>
									<td colspan="1">'.$row2['Quantity']/$row2['item_quantity'].'</td>
									<td colspan="1"><img id="img1" src="../Include/pictures/checkboxfalse.png" width="10" height="10"></td>
								</tr>
								</table>
							</td>
						</tr>
						</table>
						';
					}
					$last_id = $row2['TxnLineID'];
				}
				$pdf->writeHTML($tbl, true, false, false, false, '');
				$packing_list = false;
				}*/
					if(!empty($row['collection_name']) && !empty($row['drawing']))
					{
						$pdf->AddPage('L', array(102, 51), false, false);
						$pdf->SetFont('helvetica', '', 12);
						
						$tbl = '
						<table border="0" width="325" cellpadding="0"  align="center">
						<tr>
							<td>DreamLine SKU :</td>
						</tr>
						<tr>
							<td><font size="18"><strong>'.$row['ItemGroupRef_FullName'].'</strong></font></td>
						</tr>
						<tr>
							<td>HD SKU :</td>
						</tr>
						<tr>
							<td><font size="18"><strong>'.$row['SKU'].'</strong></font></td>
						</tr>
						<tr>
							<td>UPC :</td>
						</tr>
						<tr>
							<td><font size="18"><strong>'.$row['UPC'].'</strong></font></td>
						</tr>
						</table>
						';
						$pdf->writeHTML($tbl, true, false, false, false, '');
						
						/*$pdf->AddPage('L', array(210, 130), false, false);
						$pdf->SetFont('helvetica', '', 36);
						$tbl = '
						<table border="0" width="670" cellpadding="20">
							<tr>
								<td align="center">
									<br><strong>SKU BARCODE</strong><br>
									<img src="'.$row['sku_barcode'].'.png'.'"><br>
									<strong>'.$row['SKU'].'</strong>
								</td>
							</tr>
						</table>
						';
						$pdf->writeHTML($tbl, true, false, false, false, '');*/
						
						for ($i=0; $i<2; $i++)
						{
							$pdf->AddPage('L', array(102, 51), false, false);
							$pdf->SetFont('helvetica', '', 36);
							$tbl = '
							<table border="0" width="325" cellpadding="0">
								<tr>
									<td align="center">
										<img width="275" src="'.$row['upc_a_barcode'].'.png'.'">
									</td>
								</tr>
							</table>
							';
							$pdf->writeHTML($tbl, true, false, false, false, '');
						}
						
						$pdf->AddPage('L', array(102, 51), false, false);
						$pdf->SetFont('helvetica', '', 36);
						$tbl = '';
						$pdf->writeHTML($tbl, true, false, false, false, '');
					}
						
				}
				$pdf->Output($pdf_path, 'F');
				$result[] = $pdf_path2;

			if (!empty($result))
			{
				return $result;
			} else
			{
				error_msg("Can not generate any pdf!");
				send_error();
			}
	}catch (Exception $e) {
		error_msg($e->getMessage());
		send_error();
	}
}

//Generate pdf files for each PO number with separate pages for each box number with barcodes and info
function homeDCPdf_upc($arr, $max_pages)
{
	try{
			$result = array();
				$count_printed_pages = 0;
				$pdf_created = false;
				$file_number = 0;
				foreach ($arr as $key => $row)
				{
					if(!$pdf_created)
					{
						$file_number++;
						$pdf_path = "../Pdf/".repPo($arr[0]['PONumber'])."_file".$file_number.".pdf";
						$pdf_path2 = "Pdf/".repPo($arr[0]['PONumber'])."_file".$file_number.".pdf";
						$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
						$pdf->SetPrintHeader(false);
						$pdf->SetPrintFooter(false);
						$pdf->SetMargins(11, 11);
						$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
						$pdf_created = true;
					}
					if ($row['quantity_in_box'] == 1) $row['quantity_in_box'] = 0;
					for ($qib = 0; $qib <= $row['quantity_in_box']; $qib++)
					{
					if(!empty($row['collection_name']) && !empty($row['drawing']))
					{
						for ($i=0; $i<2; $i++)
						{
							$pdf->AddPage('L', array(210, 80), false, false);
							$pdf->SetFont('helvetica', '', 36);
							$tbl = '
							<table border="0" width="670" cellpadding="20">
								<tr>
									<td align="center">
										<img src="'.$row['upc_a_barcode'].'.png'.'"><br>
									</td>
								</tr>
							</table>
							';
							$pdf->writeHTML($tbl, true, false, false, false, '');
						}
						$count_printed_pages++;
					}
					}
					if($count_printed_pages >= $max_pages || $key == count($arr)-1)
					{
						$pdf->Output($pdf_path, 'F');
						$result[] = $pdf_path2;
						unset($pdf);
						$pdf_created = false;
						$count_printed_pages = 0;
					}
				}
				
			if (!empty($result))
			{
				return $result;
			} else
			{
				error_msg("Can not generate any pdf!");
				send_error();
			}
	}catch (Exception $e) {
		error_msg($e->getMessage());
		send_error();
	}
}

//Generate pdf files for each PO number with separate pages for each box number with barcodes and info
/*function homeDCPdf($arr, $pro10, $barcodeBill, $originCarrierName, $ProductsWarnings)
{
	try{
			$result = array();
			
				$pdf_path = "../Pdf/".repPo($arr[0]['PONumber']).".pdf";
				$pdf_path2 = "Pdf/".repPo($arr[0]['PONumber']).".pdf";
				
				$pdf = new PDF('P','mm',array(globalWidth, globalHeight));
				$pdf->SetAutoPageBreak(true, 1);
				$pdf->SetMargins(globalMargins,globalMargins,globalMargins);
				$pdf->SetLineWidth(globalLineWidth);
				
				
				foreach ($arr as $row)
				{
					if ($row['quantity_in_box'] == 1) $row['quantity_in_box'] = 0;
					for ($qib = 0; $qib <= $row['quantity_in_box']; $qib++)
					{
						$pdf->AddPage('L', array('210', '100'));
						$pdf->SetFont('Arial','B',40);
						
						$actual_position_y = 10;
						$pdf->SetXY(50, $actual_position_y);
						$pdf->Cell(107,globalSmallCellHeight,"SKU BARCODE",0,1,'C');
						$pdf->SetXY(65, $actual_position_y+15);
						$pdf->Image($row['sku_barcode'].".png", NULL, NULL, -100);
						$pdf->SetXY(60, $actual_position_y+65);
						$pdf->Cell(90,globalSmallCellHeight,$row['SKU'],0,1,'C');
						
						for ($i=0; $i<2; $i++)
						{
							$pdf->AddPage('L', array('210', '100'));
							$pdf->SetFont('Arial','B',globalMainFont);
							
							$actual_position_y = 30;
							$pdf->SetXY(50, $actual_position_y);
							$pdf->Image($row['upc_a_barcode'].".png", NULL, NULL, -100);
						}
					}
				}
				
				$pdf->Output($pdf_path);
				$result[] = $pdf_path2;
			if (!empty($result))
			{
				return $result;
			} else
			{
				error_msg("Can not generate any pdf!");
				send_error();
			}
	}catch (Exception $e) {
		error_msg($e->getMessage());
		send_error();
	}
}*/

//Generate pdf files for each PO number with separate pages for each box number with barcodes and info
function homePdfNew($arr, $pro10, $barcodeBill, $originCarrierName, $ProductsWarnings)
{
//print_r($arr);
	try{
			if ($barcodeBill != 'none')
			{
				$barcodeBill = $barcodeBill.'.png';
			}
			$PONumbers = array();
			$result = array();
			
			foreach ($arr as $row)
			{
				$PONumbers[] = $row['PONumber'];
			}
			$c = count($PONumbers);
			
			for ($i=0; $i<$c; $i++)
			{
				$pdf_path = "../Pdf/".repPo($PONumbers[$i]).".pdf";
				$pdf_path2 = "Pdf/".repPo($PONumbers[$i]).".pdf";
				
				$pdf = new PDF('L','mm',array(globalWidth, 88));
				$pdf->SetAutoPageBreak(true, 1);
				$pdf->SetMargins(globalMargins,globalMargins,globalMargins);
				$pdf->SetLineWidth(globalLineWidth);
				
				foreach ($arr as $row)
				{
					if ($row['quantity_in_box'] == 1) $row['quantity_in_box'] = 0;
					for ($qib = 0; $qib <= $row['quantity_in_box']; $qib++)
					{
						if ($row['PONumber'] == $PONumbers[$i])
						{
							
							$pdf->AddPage();
							$pdf->SetFont('Arial','B',globalMainFont);
									
									$actual_position_y = 32;
									$pdf->SetXY(65, $actual_position_y);
									$pdf->Image($row['i25'].'.png', NULL, NULL, 0);
						}
					}
				}
				if(!empty($ProductsWarnings))
				{
					$pdf->AddPage();
					$pdf->SetFont('Arial','B',globalSmallFont);
					foreach($ProductsWarnings as $ProductsWarning)
					{
						$pdf->Cell(190,globalRegularCellHeight,$ProductsWarning,0,1,'L');
					}
				}
				
				$pdf->Output($pdf_path);
				$result[] = $pdf_path2;
			}
			if (!empty($result))
			{
				return $result;
			} else
			{
				error_msg("Can not generate any pdf!");
				send_error();
			}
	}catch (Exception $e) {
		error_msg($e->getMessage());
		send_error();
	}
}

//Generate pdf files for each PO number with separate pages for each box number with barcodes and info
function cnrPdf($arr, $pro10, $barcodeBill, $originCarrierName, $ProductsWarnings)
{
	try{
			if ($barcodeBill != 'none')
			{
				$barcodeBill = $barcodeBill.'.png';
			}
			$PONumbers = array();
			$result = array();
			
			foreach ($arr as $row)
			{
				$PONumbers[] = $row['PONumber'];
			}
			$c = count($PONumbers);
			
			for ($i=0; $i<$c; $i++)
			{
				$pdf_path = "../Pdf/".repPo($PONumbers[$i]).".pdf";
				$pdf_path2 = "Pdf/".repPo($PONumbers[$i]).".pdf";
				
				$pdf = new PDF('P','mm',array(globalWidth, globalHeight));
				$pdf->SetAutoPageBreak(true, 1);
				$pdf->SetMargins(globalMargins,globalMargins,globalMargins);
				$pdf->SetLineWidth(globalLineWidth);
				
				foreach ($arr as $row)
				{
				if ($row['quantity_in_box'] == 1) $row['quantity_in_box'] = 0;
				for ($qib = 0; $qib <= $row['quantity_in_box']; $qib++)
				{
					if ($row['PONumber'] == $PONumbers[$i])
					{
						$logo = '../Include/logo/logo.png';
						$PONumber = 'PO Number: '.checkPO($row['PONumber'], $row['RefNumber'], $row['original_po']);
						$ProductCode = 'SKU: '.$row['ProductCode'];
						$boxes = (integer)$row['boxes'];
						$boxNumber = (integer)$row['boxNumber'];
						if ($boxes > 1)
						{
							$text = "This box is part of a group.";
							$text1 = "This is box:";
							$BoxCarton = $boxNumber.' of '.$boxes;
						} else
						{
							$text = "This is a single box.";
							$text1 = "This is box:";
							$BoxCarton = $boxNumber.' of '.$boxes;
						}
						$contact = 'For questions or concerns regarding this order';
						$contact1 = 'Please contact us at 866-731-8378';
						
						//$pdf->AddPage();
						$pdf->AddPage('P', array(102, 153), false, false);
						/* Update PDF to 4" x 6" */
						$localMainFont = 10;
						$localBigFont = 52;
						$localSmallFont = 8;
						$localCarrierFont = 7;
						$localRegularCellHeight = 5;
						$localBigCellHeight = 22;
						$localSmallCellHeight = 5;
						$localBottomContentY = 120;
						
						$pdf->SetFont('Arial','B',$localSmallFont);
						
								//SKU Code
								$pdf->Cell(82,$localRegularCellHeight,$ProductCode,0,1,'L');
								
								//PO #
								$pdf->Cell(82,$localRegularCellHeight,$PONumber,0,1,'L');
								
								//text
								$pdf->SetFont('Arial','',$localMainFont);
								$pdf->Cell(82,$localSmallCellHeight,$text,0,1,'C');
								$pdf->Cell(82,$localSmallCellHeight,$text1,0,1,'C');
								//Box Number / Carton Number
								$pdf->SetFont('Arial','B',$localBigFont);
								$pdf->Cell(82,$localBigCellHeight,$BoxCarton,0,1,'C');
								
								
								//Dreamline Logo
                                $pdf->Cell(12);
                                $actual_position_y = $pdf->GetY();
                                $pdf->Image($logo, 30, $actual_position_y+5, 40);
                                $pdf->SetXY(0, $actual_position_y+15);
								
								//contact
								$pdf->SetFont('Arial','',$localMainFont);
								$pdf->Cell(82,$localSmallCellHeight,$contact,0,1,'C');
								$pdf->SetFont('Arial','B',$localMainFont);
								$pdf->Cell(82,$localSmallCellHeight,$contact1,0,1,'C');
								
								if ($pro10 != 'none' && $barcodeBill != 'none')
								{
									$pdf->Line(11, $localBottomContentY, 92, $localBottomContentY);
									$pdf->SetXY(15, $localBottomContentY);
									$pdf->Ln();
									$pdf->SetFont('Arial','B',$localMainFont);
									$actual_position_y = $pdf->GetY();
									$pdf->SetXY(34, $actual_position_y);
									$pdf->Cell(58, 20, " ", 1, 1, 'C');
									$pdf->SetXY(36, $actual_position_y+1);
									$pdf->Cell(58,$localSmallCellHeight,"PRO# ".$pro10,0,1,'L');
									$pdf->SetXY(37, $actual_position_y+7);
									$pdf->Image($barcodeBill, NULL, NULL, -165);
									if (isCarrierNameCorrect($originCarrierName))
									{
										$carrierLogo = getCarrierLogo($originCarrierName);
										$pdf->Image($carrierLogo, 11, $localBottomContentY+4, 21, 14, 'PNG');
										$pdf->SetFont('Arial','B',$localCarrierFont);
										$pdf->SetXY(10, $localBottomContentY+20);
										$pdf->Cell(92,$localSmallCellHeight,$originCarrierName,0,1,'L');
									}
									
								} else
								{
									if (isCarrierNameCorrect($originCarrierName))
									{
										$pdf->Line(11, $localBottomContentY, 92, $localBottomContentY);
										$carrierLogo = getCarrierLogo($originCarrierName);
										$pdf->Image($carrierLogo, 11, $localBottomContentY+4, 21, 14, 'PNG');
										
										$pdf->SetFont('Arial','',$localMainFont);
										$pdf->SetXY(32, $localBottomContentY+10);
										$pdf->Cell(92,$localSmallCellHeight,$originCarrierName,0,1,'L');
									}
								}
								
					}
					}
				}
				
				if ($pro10 != 'none' && $barcodeBill != 'none')
				{
					$pagesNum = count($arr) + 2;
					for($pn = 0; $pn < $pagesNum; $pn++)
					{
						//$pdf->AddPage();
						$pdf->AddPage('P', array(102, 153), false, false);
						/* Update PDF to 4" x 6" */
						$localMainFont = 11;
						$localBigFont = 52;
						$localSmallFont = 8;
						$localRegularCellHeight = 6;
						$localBigCellHeight = 22;
						$localSmallCellHeight = 5;
						$localBottomContentY = 120;
						$pdf->SetFont('Arial','B',$localSmallFont);
						if (extrCarrier($originCarrierName) == "RL")
						{
							$pdf->SetXY(24, 40);
							$pdf->Cell(58,$localSmallCellHeight, $arr[0]['ShipAddress_Addr1'],0,1,'L');
							$actual_position_y = $pdf->GetY();
							$pdf->SetXY(24, $actual_position_y);
							$pdf->Cell(58,$localSmallCellHeight, $arr[0]['ShipAddress_Addr2'].", - ",0,1,'L');
							$actual_position_y = $pdf->GetY();
							$pdf->SetXY(24, $actual_position_y);
							$pdf->Cell(58,$localSmallCellHeight, $arr[0]['ShipAddress_City'].", ".$arr[0]['ShipAddress_State'].", ".$arr[0]['ShipAddress_PostalCode'],0,1,'L');
							$actual_position_y = $pdf->GetY();
							$pdf->SetXY(24, $actual_position_y);
							$pdf->Cell(58,$localSmallCellHeight, $arr[0]['FOB'],0,1,'L');
						}
						$pdf->SetFont('Arial','B',$localMainFont);						
						//$actual_position_y = $pdf->GetY();
						$actual_position_y = 62;
						$pdf->SetXY(23, $actual_position_y);
						$pdf->Cell(58, 20, " ", 1, 1, 'C');
						$pdf->SetXY(25, $actual_position_y+1);
						$pdf->Cell(58,$localSmallCellHeight,"PRO# ".$pro10,0,1,'L');
						$pdf->SetXY(26, $actual_position_y+7);
						$pdf->Image($barcodeBill, NULL, NULL, -165);
						
						if (isCarrierNameCorrect($originCarrierName))
						{
							$carrierLogo = getCarrierLogo($originCarrierName);
							$pdf->Image($carrierLogo, 24, 84, 0, 17, 'PNG');
									
							$pdf->SetFont('Arial','B',$localSmallFont);
							$pdf->SetXY(24, 103);
							$pdf->Cell(82,$localSmallCellHeight,$originCarrierName,0,1,'L');
						}
					}
				}
				if(!empty($ProductsWarnings))
				{
					//$pdf->AddPage();
					$pdf->AddPage('P', array(102, 153), false, false);
					$localSmallFont = 8;					
					$localRegularCellHeight = 5;
					
					$pdf->SetFont('Arial','B',$localSmallFont);
					foreach($ProductsWarnings as $ProductsWarning)
					{
						$pdf->Cell(82,$localRegularCellHeight,$ProductsWarning,0,1,'L');
					}
				}
				
				$pdf->Output($pdf_path);
				$result[] = $pdf_path2;
			}
			if (!empty($result))
			{
				return $result;
			} else
			{
				error_msg("Can not generate any pdf!");
				send_error();
			}
	}catch (Exception $e) {
		error_msg($e->getMessage());
		send_error();
	}
}

//Generate pdf files for each PO number with separate pages for each box number with barcodes and info
function menardsPdf($arr, $pro10, $barcodeBill, $originCarrierName, $ProductsWarnings)
{
/*print_r("<pre>");
print_r($arr);
print_r("</pre>");*/
	try{
			if ($barcodeBill != 'none')
			{
				$barcodeBill = $barcodeBill.'.png';
			}
			$PONumbers = array();
			$result = array();
			
			foreach ($arr as $row)
			{
				$PONumbers[] = $row['PONumber'];
			}
			$c = count($PONumbers);
			
			for ($i=0; $i<$c; $i++)
			{
				$pdf_path = "../Pdf/".repPo($PONumbers[$i]).".pdf";
				$pdf_path2 = "Pdf/".repPo($PONumbers[$i]).".pdf";
				
				$pdf = new PDF('P','mm',array(globalWidth, globalHeight));
				$pdf->SetAutoPageBreak(true, 1);
				$pdf->SetMargins(globalMargins,globalMargins,globalMargins);
				$pdf->SetLineWidth(globalLineWidth);
				
				foreach ($arr as $row)
				{
				if ($row['quantity_in_box'] == 1) $row['quantity_in_box'] = 0;
				for ($qib = 0; $qib <= $row['quantity_in_box']; $qib++)
				{
					if ($row['PONumber'] == $PONumbers[$i])
					{
						$logo = '../Include/logo/logo.png';
						$PONumber = 'PO Number: '.checkPO($row['PONumber'], $row['RefNumber'], $row['original_po']);
						$ProductCode = 'SKU: '.$row['ProductCode'];
						$boxes = (integer)$row['boxes'];
						$boxNumber = (integer)$row['boxNumber'];
						if ($boxes > 1)
						{
							$text = "This box is part of a group. This is box: ";
							$text1 = "";
							$BoxCarton = $boxNumber.' of '.$boxes;
						} else
						{
							$text = "This package has 1 box";
//							$text1 = "This is box:";
							$BoxCarton = $boxNumber.' of '.$boxes;
						}
						$contact = 'For questions or concerns regarding this order';
						$contact1 = 'Please contact us at 866-731-8378';
						$ShipAddress_Addr1 =  'Customer: '.$row['ShipAddress_Addr1'];
						$Date = 'Order date: '.$row['Date'];
						$Store = 'Store# '.$row['Store'];
						$Descr = 'Product description: '.$row['Descr'];
						
						//$pdf->AddPage();
						$pdf->AddPage('P', array(102, 153), false, false);
						/* Update PDF to 4" x 6" */
						$localMainFont = 10;
						$localBigFont = 52;
						$localSmallFont = 8;
						$localCarrierFont = 7;
						$localRegularCellHeight = 5;
						$localBigCellHeight = 22;
						$localSmallCellHeight = 5;
						$localBottomContentY = 120;
						$pdf->SetFont('Arial','B',$localSmallFont);
								
								//SKU Code
								$pdf->Cell(82,$localSmallCellHeight,$ProductCode,0,1,'L');
								
								//PO #
								$pdf->Cell(82,$localSmallCellHeight,$PONumber,0,1,'L');
								
								$pdf->Ln();
								
								//Order date
								//Store#
								$pdf->Cell(82,$localSmallCellHeight,$Date.'        '.$Store,0,1,'L');
								//$pdf->Ln();
								
								//Descr
//								$pdf->Cell(190,10,$Descr,0,1,'L');
								$pdf->MultiCell(82,$localSmallCellHeight,$Descr,0,'L');
								
								//Customer
								$pdf->SetFont('Arial','B',$localSmallFont);
								$pdf->Cell(82,$localSmallCellHeight,$ShipAddress_Addr1,0,1,'L');
								$pdf->Ln();
								
						//text
								$pdf->SetFont('Arial','',$localMainFont);
								$pdf->Cell(82,$localSmallCellHeight,$text,0,1,'C');
//								$pdf->Cell(190,10,$text1,0,1,'C');
								//Box Number / Carton Number
								$pdf->SetFont('Arial','B',$localBigFont);
								$pdf->Cell(82,$localBigCellHeight,$BoxCarton,0,1,'C');
								
								//line
								$pdf->SetFont('Arial','B',$localSmallFont);
								$pdf->Cell(82,2,'_________________________________________________',0,1,'C');
								//Dreamline Logo
                                $pdf->Cell(12);
                                $actual_position_y = $pdf->GetY()+3;
                                $pdf->Image($logo, 30, $actual_position_y, 40);
                                $pdf->SetXY(0, $actual_position_y+15);
								
								//contact

								$pdf->SetFont('Arial','',$localMainFont);
								$pdf->Ln(1);
								$pdf->Cell(82,$localSmallCellHeight,$contact,0,1,'C');
								$pdf->SetFont('Arial','B',$localMainFont);
								$pdf->Cell(82,$localSmallCellHeight,$contact1,0,1,'C');

								if ($pro10 != 'none' && $barcodeBill != 'none')
								{
									$pdf->Line(11, $localBottomContentY, 92, $localBottomContentY);
									$pdf->SetXY(15, $localBottomContentY);
									$pdf->Ln();
									$pdf->SetFont('Arial','B',$localMainFont);
									$actual_position_y = $pdf->GetY();
									$pdf->SetXY(34, $actual_position_y);
									$pdf->Cell(58, 20, " ", 1, 1, 'C');
									$pdf->SetXY(36, $actual_position_y+1);
									$pdf->Cell(58,$localSmallCellHeight,"PRO# ".$pro10,0,1,'L');
									$pdf->SetXY(37, $actual_position_y+7);
									$pdf->Image($barcodeBill, NULL, NULL, -165);
									if (isCarrierNameCorrect($originCarrierName))
									{
										$carrierLogo = getCarrierLogo($originCarrierName);
										$pdf->Image($carrierLogo, 11, $localBottomContentY+4, 21, 14, 'PNG');
										$pdf->SetFont('Arial','B',$localCarrierFont);
										$pdf->SetXY(10, $localBottomContentY+20);
										$pdf->Cell(92,$localSmallCellHeight,$originCarrierName,0,1,'L');
									}
									
								} else
								{
									if (isCarrierNameCorrect($originCarrierName))
									{
										$pdf->Line(11, $localBottomContentY, 92, $localBottomContentY);
										$carrierLogo = getCarrierLogo($originCarrierName);
										$pdf->Image($carrierLogo, 11, $localBottomContentY+4, 21, 14, 'PNG');
										
										$pdf->SetFont('Arial','',$localMainFont);
										$pdf->SetXY(32, $localBottomContentY+10);
										$pdf->Cell(92,$localSmallCellHeight,$originCarrierName,0,1,'L');
									}
								}
						}
					}
				}
				
				if ($pro10 != 'none' && $barcodeBill != 'none')
				{
					$pagesNum = count($arr) + 2;
					for($pn = 0; $pn < $pagesNum; $pn++)
					{
						//$pdf->AddPage();
						$pdf->AddPage('P', array(102, 153), false, false);
						/* Update PDF to 4" x 6" */
						$localMainFont = 11;
						$localBigFont = 52;
						$localSmallFont = 8;
						$localRegularCellHeight = 6;
						$localBigCellHeight = 22;
						$localSmallCellHeight = 5;
						$localBottomContentY = 120;
						
						
						$pdf->SetFont('Arial','B',$localSmallFont);

						if (extrCarrier($originCarrierName) == "RL")
						{
							$pdf->SetXY(24, 40);
							$pdf->Cell(58,$localSmallCellHeight, $arr[0]['ShipAddress_Addr1'],0,1,'L');
							$actual_position_y = $pdf->GetY();
							$pdf->SetXY(24, $actual_position_y);
							$pdf->Cell(58,$localSmallCellHeight, $arr[0]['ShipAddress_Addr2'].", - ",0,1,'L');
							$actual_position_y = $pdf->GetY();
							$pdf->SetXY(24, $actual_position_y);
							$pdf->Cell(58,$localSmallCellHeight, $arr[0]['ShipAddress_City'].", ".$arr[0]['ShipAddress_State'].", ".$arr[0]['ShipAddress_PostalCode'],0,1,'L');
							$actual_position_y = $pdf->GetY();
							$pdf->SetXY(24, $actual_position_y);
							$pdf->Cell(58,$localSmallCellHeight, $arr[0]['FOB'],0,1,'L');
						}
						
						$pdf->SetFont('Arial','B',$localMainFont);						
						//$actual_position_y = $pdf->GetY();
						$actual_position_y = 62;
						$pdf->SetXY(23, $actual_position_y);
						$pdf->Cell(58, 20, " ", 1, 1, 'C');
						$pdf->SetXY(25, $actual_position_y+1);
						$pdf->Cell(58,$localSmallCellHeight,"PRO# ".$pro10,0,1,'L');
						$pdf->SetXY(26, $actual_position_y+7);
						$pdf->Image($barcodeBill, NULL, NULL, -165);
						
						if (isCarrierNameCorrect($originCarrierName))
						{
							$carrierLogo = getCarrierLogo($originCarrierName);
							$pdf->Image($carrierLogo, 24, 84, 0, 17, 'PNG');
									
							$pdf->SetFont('Arial','B',$localSmallFont);
							$pdf->SetXY(24, 103);
							$pdf->Cell(82,$localSmallCellHeight,$originCarrierName,0,1,'L');
						}
					}
				}
				if(!empty($ProductsWarnings))
				{
					//$pdf->AddPage();
					$pdf->AddPage('P', array(102, 153), false, false);
					$localSmallFont = 8;					
					$localRegularCellHeight = 5;					
					$pdf->SetFont('Arial','B',$localSmallFont);
					foreach($ProductsWarnings as $ProductsWarning)
					{
						$pdf->Cell(82,$localRegularCellHeight,$ProductsWarning,0,1,'L');
					}
				}

				$pdf->Output($pdf_path);
				$result[] = $pdf_path2;
			}
			if (!empty($result))
			{
				return $result;
			} else
			{
				error_msg("Can not generate any pdf!");
				send_error();
			}
	}catch (Exception $e) {
		error_msg($e->getMessage());
		send_error();
	}
}

//Generate pdf files for each PO number with separate pages for each box number with barcodes and info
function searsPdf($arr, $barcodeGs1128, $barcodeUPCA, $barcodeValue, $originCarrierName, $ProductsWarnings)
{
	/*print_r($arr);
	die();*/
	try{
			
			if ($barcodeGs1128 != 'none')
			{
				$barcodeGs1128 = $barcodeGs1128.'.png';
			}
			
			if ($barcodeUPCA != 'none')
			{
				$barcodeUPCA = $barcodeUPCA.'.png';
			}
			
			$carrierLogo = getCarrierLogo($originCarrierName);
			
			$PONumbers = array();
			$result = array();
			
			foreach ($arr as $row)
			{
				$PONumbers[] = $row['PONumber'];
			}
			$c = count($PONumbers);
			
			for ($i=0; $i<$c; $i++)
			{
				$pdf_path = "../Pdf/".repPo($PONumbers[$i]).".pdf";
				$pdf_path2 = "Pdf/".repPo($PONumbers[$i]).".pdf";
				
				$dept = '683';
				$shipToAddress = '';
				$memo = explode(",", $arr[0]['Memo']);
				if (isset($memo[1]) && !empty($memo[1])) $customer = $memo[1]; else $customer = '';
				if (isset($memo[4]) && !empty($memo[4])) $suborder = $memo[4]; else $suborder = '';
				
				if (isset($arr[0]['ShipAddress_Addr1']) && !empty($row['ShipAddress_Addr1'])) $shipToAddress.=$row['ShipAddress_Addr1'].'<br>';
				if (isset($arr[0]['ShipAddress_Addr2']) && !empty($row['ShipAddress_Addr2'])) $shipToAddress.=$row['ShipAddress_Addr2'].'<br>';
				if (isset($arr[0]['ShipAddress_Addr3']) && !empty($row['ShipAddress_Addr3'])) $shipToAddress.=$row['ShipAddress_Addr3'].'<br>';
				if (isset($arr[0]['ShipAddress_Addr4']) && !empty($row['ShipAddress_Addr4'])) $shipToAddress.=$row['ShipAddress_Addr4'].'<br>';
				if (isset($arr[0]['ShipAddress_Addr5']) && !empty($row['ShipAddress_Addr5'])) $shipToAddress.=$row['ShipAddress_Addr5'].'<br>';
				if (isset($arr[0]['ShipAddress_City']) && !empty($row['ShipAddress_City'])) $shipToAddress.=$row['ShipAddress_City'].', ';
				if (isset($arr[0]['ShipAddress_State']) && !empty($row['ShipAddress_State'])) $shipToAddress.=$row['ShipAddress_State'].' ';
				if (isset($arr[0]['ShipAddress_PostalCode']) && !empty($row['ShipAddress_PostalCode'])) $shipToAddress.=$row['ShipAddress_PostalCode'].'<br>';
				//if (isset($arr[0]['FOB']) && !empty($row['FOB'])) $shipToAddress.=$row['FOB'].'<br>';
				
				$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
				$pdf->SetPrintHeader(false);
				$pdf->SetPrintFooter(false);
				$pdf->SetMargins(11, 11);
				$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
				$pdf->SetAutoPageBreak(true, 1);

				foreach ($arr as $row)
				{
				if ($row['quantity_in_box'] == 1) $row['quantity_in_box'] = 0;
				for ($qib = 0; $qib <= $row['quantity_in_box']; $qib++)
				{
					if ($row['PONumber'] == $PONumbers[$i])
					{
						//$pdf->AddPage();
						$pdf->AddPage('P', array(102, 153), false, false);
						$pdf->SetFont('helvetica', '', 8);
						$tbl = '<table border="0" padding="0" width="280" cellpadding="0" cellspacing="0" style="border-bottom: 1px solid red">
				<tr>
					<td>
						<table border="2" padding="0" width="280">
							<tr>
								<td>
									<table padding="0" border="0" width="100%" cellpadding="0" cellspacing="0" style="border-bottom: 1px solid #000000; font-weight: bold;">
										<tr>
											<td colspan="3" style="height: 1px;">
											</td>
										</tr>										
										<tr>
											<td width="2%"></td>
											<td width="13%">FROM</td>
											<td width="85%"></td>
										</tr>
										<tr>
											<td width="2%"></td>
											<td width="13%"></td>
											<td width="85%"><font size="9">Sears.com<br>75 Hawk Road<br>Warminster, PA 18974</font></td>
										</tr>
										<tr>
											<td colspan="3" style="height: 1px;">
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td>
									<table padding="0" border="0" width="100%" cellpadding="0" cellspacing="0" style="border-bottom: 1px solid #000000; font-weight: bold;">
										<tr>
											<td colspan="3" style="height: 1px;">
											</td>
										</tr>
										<tr>
											<td width="2%"></td>
											<td width="13%">TO</td>
											<td width="85%"></td>
										</tr>
										<tr>
											<td width="2%"></td>
											<td width="13%"></td>
											<td width="85%"><font size="9">'.$shipToAddress.'</font></td>
										</tr>
										<tr>
											<td colspan="3" style="height: 1px;">
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td>
									<table padding="0" border="0" width="100%" cellpadding="0" cellspacing="0" style="border-bottom: 1px solid #000000;">
										<tr>
											<td colspan="3" style="height: 1px;">
											</td>
										</tr>
										<tr>
											<td width="2%"></td>
											<td width="28%">SALESCHECK #:</td>
											<td width="70%"></td>
										</tr>
										<tr>
											<td width="2%" ></td>
											<td width="28%"></td>
											<td width="70%" style="font-weight: bold;"><font size="9">'.$suborder.'</font></td>
										</tr>
										<tr>
											<td colspan="3" style="height: 1px;">
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td>
									<table padding="0" border="0" width="100%" cellpadding="0" cellspacing="0" style="border-bottom: 1px solid #000000;">
										<tr>
											<td colspan="5" style="height: 1px;">
											</td>
										</tr>
										<tr>
											<td width="2%"></td>
											<td width="20%" style="font-weight: bold;"><font size="11">DEPT:</font></td>
											<td width="38%" style="font-weight: bold;"><font size="11">'.$dept.'</font></td>
											<td width="38%" style="text-align: right; font-weight: bold;"><font size="11">CUSTOMER</font></td>
											<td width="2%"></td>
										</tr>
										<tr>
											<td width="2%"></td>
											<td width="20%" style="font-weight: bold;"><font size="10">PO:</font></td>
											<td width="38%" style="font-weight: bold;"><font size="10">'.checkPO($row['PONumber'], $row['RefNumber'], $row['original_po']).'</font></td>
											<td width="38%" style="text-align: right; font-weight: bold;"><font size="10">ORDER</font></td>
											<td width="2%"></td>
										</tr>
										<tr>
											<td width="2%"></td>
											<td width="20%" style="font-weight: bold;"><font size="7">CUSTOMER:</font></td>
											<td width="76%" colspan="2" style="font-weight: bold;"><font size="10">'.$customer.'</font></td>
											<td width="2%"></td>
										</tr>
										<tr>
											<td colspan="5" style="height: 1px;">
											</td>
										</tr>										
									</table>
								</td>
							</tr>
							<tr>
								<td width="100%">
									<table width="100%" cellspacing="0" cellpadding="0">
										<tr>
											<td><b>&nbsp;&nbsp;SSCC</b><br /></td>
										</tr>
										<tr>
											<td align="center" style="text-align:center"><img width="230" src="'.$barcodeGs1128.'"><br /></td>
										</tr>
									</table>
								</td>
							</tr>									
						</table>
					</td>
				</tr>
			<table>';
								
						$pdf->writeHTML($tbl, true, false, false, false, '');
						$pdf->AddPage('P', array(102, 153), false, false);
						$pdf->SetFont('helvetica', '', 8);
						$pdf->writeHTML($tbl, true, false, false, false, '');
					}
					}
				}
			
			if ($barcodeUPCA != 'none')
			{
				foreach ($arr as $row)
				{
					if ($row['PONumber'] == $PONumbers[$i])
					{
					
						$boxes = (integer)$row['boxes'];
						$boxNumber = (integer)$row['boxNumber'];
						$BoxCarton = $boxNumber.' OF '.$boxes;
						
						//$pdf->AddPage();
						$pdf->AddPage('L', array(102, 153), false, false);
						$pdf->SetFont('helvetica', '', 18);
						$tbl = '
						<table border="1" width="460" align="center" style="font-weight: bold;">
							<tr>
								<td>
									'.$dept.'
								</td>
								<td colspan="2">
									'.$row['validSKU'].'
								</td>
								<td>
									'.$row['quantity'].'
								</td>
							</tr>
							<tr style="font-size: 16px;">
								<td>
									DEPT
								</td>
								<td colspan="2">
									ITEM/SKU
								</td>
								<td>
									QTY
								</td>
							</tr>
							<tr>
								<td colspan="4" style="font-size: 16px; height: 170px;">
									<p>STORE SCAN</p>
									<img src="'.$barcodeUPCA.'">
								</td>
							</tr>
							<tr>
								<td colspan="4">
									CARTON '.$BoxCarton.'
								</td>
							</tr>
						</table>
								';
								
						$pdf->writeHTML($tbl, true, false, false, false, '');
						$pdf->AddPage();
						$pdf->SetFont('helvetica', '', 18);
						$pdf->writeHTML($tbl, true, false, false, false, '');
					}
				}
				}
				
				foreach ($arr as $row)
				{
						if($row['itemWeight']>70)
						{
							//$pdf->AddPage();
							$pdf->AddPage('L', array(102, 153), false, false);
							$pdf->SetFont('helvetica', '', 15);
							$tbl = '
								<table border="1" width="460">
									<tr>
										<td style="font-weight: bold;" align="center">
										<br>
										<br>
										<br>
										<br>
										<br>
										<br>
										Over 70 lbs.<br>
										<!--Total weight: '.$row['itemWeight'].' lbs.-->
										<br>
										<br>
										<br>
										<br>
										<br>
										</td>
									</tr>
								</table>
							';
						$pdf->writeHTML($tbl, true, false, false, false, '');
						}
				}
				if(!empty($ProductsWarnings))
						{
							//$pdf->AddPage();
							$pdf->AddPage('P', array(102, 153), false, false);
							$pdf->SetFont('helvetica', '', 8);
							$tbl='';
							foreach($ProductsWarnings as $ProductsWarning)
							{
								$tbl.='<p>'.$ProductsWarning.'</p>';
							}
							$pdf->writeHTML($tbl, true, false, false, false, '');
						}
			
				$pdf->Output($pdf_path, 'F');
				$result[] = $pdf_path2;
			}
			if (!empty($result))
			{
				return $result;
			} else
			{
				error_msg("Can not generate any pdf!");
				send_error();
			}
	}catch (Exception $e) {
		error_msg($e->getMessage());
		send_error();
	}
}

//Generate pdf files for each PO number with separate pages for each box number with barcodes and info
function fergusonPdf($arr, $originCarrierName, $ProductsWarnings, $ASNPath, $ASN, $BOLPath, $BOL, $pro10, $barcodeBill)
{
	try{
			if ($barcodeBill != 'none')
			{
				$barcodeBill = $barcodeBill.'.png';
			}
			$carrierLogo = getCarrierLogo($originCarrierName);
			
			$PONumbers = array();
			$result = array();
			
			foreach ($arr as $row)
			{
				$PONumbers[] = $row['PONumber'];
			}
			$c = count($PONumbers);
			
			for ($i=0; $i<$c; $i++)
			{
				$pdf_path = "../Pdf/".repPo($PONumbers[$i]).".pdf";
				$pdf_path2 = "Pdf/".repPo($PONumbers[$i]).".pdf";
				
				$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

				$pdf->SetPrintHeader(false);
				$pdf->SetPrintFooter(false);
				$pdf->SetMargins(5, 5);
				$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
				$pdf->SetAutoPageBreak(TRUE, 1);

				foreach ($arr as $row)
				{
				if ($row['quantity_in_box'] == 1) $row['quantity_in_box'] = 0;
				for ($qib = 0; $qib <= $row['quantity_in_box']; $qib++)
				{
					if ($row['PONumber'] == $PONumbers[$i])
					{
						$pdf->AddPage('P', array(102, 153), false, false);
						$pdf->SetFont('helvetica', '', 11);
						$tbl = '
<table style="border-bottom: 2px solid #000000;" cellpadding="6">
	<tr>
		<td width="30%"><font size="12">FROM:</font></td>
		<td width="70%">
			<font size="12">Dreamline<br>
			75 Hawk rd<br>
			Warminster, PA 18947</font>
		</td>
	</tr>
</table><br>

<table cellpadding="6">
	<tr>
		<td width="20%"><font size="12">TO:</font></td>
		<td width="80%">
			<font size="12">'.$arr[0]['ShipAddress_Addr1'].'<br>
			'.$arr[0]['ShipAddress_Addr2'].'<br>
			'.$arr[0]['ShipAddress_City'].", ".$arr[0]['ShipAddress_State'].", ".$arr[0]['ShipAddress_PostalCode'].'<br><br><br></font>
		</td>
	</tr>
</table><br>

<table style="border-bottom: 2px solid #000000; border-top: 2px solid #000000;" cellpadding="6">
	<tr>
		<td width="50%" style="border-right: 2px solid #000000;">
			Carrier name:<br>
			'.$originCarrierName.'<br>
		</td>
		<td width="50%" style="border-left: 2px solid #000000;">
			<b>PO Number(s):</b><br>
			'.$row['PONumber'].'
		</td>
	</tr>
</table><br>

<table style="border-bottom: 2px solid #000000; border-top: 2px solid #000000;" cellpadding="6">
	<tr>
		<td>ASN#: '.$ASN.'</td>
	</tr>
	<tr>
		<td align="center"><br><br><img width="150px" src="'.$ASNPath.'.png'.'"></td>
	</tr>
</table><br>

<table cellpadding="6">
	<tr>
		<td align="center"><br><br><img width="150px" src="'.$BOLPath.'.png'.'"></td>
	</tr>
	<tr>
		<td align="center"><br>BOL Number: '.$BOL.'</td>
	</tr>
</table>
';
								
								$pdf->writeHTML($tbl, true, false, false, false, '');								
					}
					}
				}
                                
                                
				if(!empty($ProductsWarnings))
						{
							$pdf->AddPage();
							$pdf->SetFont('helvetica', '', 16);
							$tbl='';
							foreach($ProductsWarnings as $ProductsWarning)
							{
								$tbl.='<p>'.$ProductsWarning.'</p>';
							}
							$pdf->writeHTML($tbl, true, false, false, false, '');
						}
                                                
                                //!!!
                                foreach ($arr as $row)
				{
				if ($row['quantity_in_box'] == 1) $row['quantity_in_box'] = 0;
				for ($qib = 0; $qib <= $row['quantity_in_box']; $qib++)
				{
					if ($row['PONumber'] == $PONumbers[$i])
					{
						$logo = '../Include/logo/logo.png';
						$PONumber = 'PO Number: '.checkPO($row['PONumber'], $row['RefNumber'], $row['original_po']);
						$ProductCode = 'SKU: '.$row['ProductCode'];
						
						$boxes = (integer)$row['boxes'];
						$boxNumber = (integer)$row['boxNumber'];
						if ($boxes > 1)
						{
							$text = "This box is part of a group.";
							$text1 = "This is box:";
							$BoxCarton = $boxNumber.' of '.$boxes;
						} else
						{
							$text = "This is a single box.";
							$text1 = "This is box:";
							$BoxCarton = $boxNumber.' of '.$boxes;
						}
						$contact = 'For questions or concerns regarding this order';
						$contact1 = 'Please contact us at 866-731-8378';
						
						//$pdf->AddPage();
						$pdf->AddPage('P', array(102, 153), false, false);
						/* Update PDF to 4" x 6" */
						$localMainFont = 11;
						$localBigFont = 52;
						$localSmallFont = 8;
						$localCarrierFont = 7;
						$localRegularCellHeight = 6;
						$localBigCellHeight = 22;
						$localSmallCellHeight = 5;
						$localBottomContentY = 120;
                                                
						$pdf->SetFont('helvetica','B',$localMainFont);
								
								//SKU Code
								$pdf->Cell(82,$localRegularCellHeight,$ProductCode,0,1,'L');
								//$pdf->Ln();
								//PO #
								$pdf->Cell(82,$localRegularCellHeight,$PONumber,0,1,'L');

								//text
								$pdf->SetFont('helvetica','',$localMainFont);
								$pdf->Ln();
								$pdf->Cell(82,$localSmallCellHeight,$text,0,1,'C');
								$pdf->Cell(82,$localSmallCellHeight,$text1,0,1,'C');

								//Box Number / Carton Number
								$pdf->SetFont('helvetica','B',$localBigFont);
								$pdf->Cell(82,$localBigCellHeight,$BoxCarton,0,1,'C');
								
								$pdf->SetFont('helvetica','B',$localSmallFont);
								$pdf->Cell(82,2,'_________________________________________________',0,1,'C');
									
								//Dreamline Logo
                                $pdf->Cell(12);
                                $actual_position_y = $pdf->GetY();
                                $pdf->Image($logo, 30, $actual_position_y+5, 40);
                                $pdf->SetXY(0, $actual_position_y+15);
                                                                
								//contact
								$pdf->SetFont('helvetica','',$localMainFont);
								$pdf->Ln(2);
								$pdf->Cell(82,$localSmallCellHeight,$contact,0,1,'C');
								$pdf->SetFont('helvetica','B',$localMainFont);
								$pdf->Cell(82,$localSmallCellHeight,$contact1,0,1,'C');
                                                                if ($row['CustomerRef_FullName'] == 'AmazonDS') 
								{
									$pdf->Ln(2);
									$pdf->Cell(82,$localSmallCellHeight,'Amazon DS',0,1,'C');
								}
								if ($pro10 != 'none' && $barcodeBill != 'none')
								{
									$pdf->Line(11, $localBottomContentY, 92, $localBottomContentY);
									$pdf->SetXY(15, $localBottomContentY);
									$pdf->Ln();
									$pdf->SetFont('helvetica','B',$localMainFont);
									$actual_position_y = $pdf->GetY();
									$pdf->SetXY(34, $actual_position_y);  // 78 => 0.41 => 34 
									$pdf->Cell(58, 20, " ", 1, 1, 'C');
									$pdf->SetXY(36, $actual_position_y+1);
									$pdf->Cell(58,$localSmallCellHeight,"PRO# ".$pro10,0,1,'L');
									$pdf->SetXY(37, $actual_position_y+6);
									$pdf->Image($barcodeBill, 37, $actual_position_y+6, 30);
									if (isCarrierNameCorrect($originCarrierName))
									{
										$carrierLogo = getCarrierLogo($originCarrierName);
                                                                                $pdf->Image($carrierLogo, 11, $localBottomContentY+4, 21, 14, 'PNG');
										$pdf->SetFont('helvetica','B',$localCarrierFont);
										$pdf->SetXY(10, $localBottomContentY+20);
										$pdf->Cell(92,$localSmallCellHeight,$originCarrierName,0,1,'L');
									}
								}/* else
								{
									if (isCarrierNameCorrect($originCarrierName))
									{
										$pdf->Line(11, globalBottomContentY, 198, globalBottomContentY);
										$carrierLogo = getCarrierLogo($originCarrierName);
										$pdf->Image($carrierLogo, 11, globalBottomContentY+4, 90, 60, 'PNG');
										
										$pdf->SetFont('Arial','',globalCarrierFont);
										$pdf->SetXY(100, globalBottomContentY+10);
										$pdf->Cell(190,globalSmallCellHeight,$originCarrierName,0,1,'L');
									}
								}*/
					}
					}
				}
				if ($pro10 != 'none' && $barcodeBill != 'none')
				{
					$pagesNum = count($arr) + 2;
					for($pn = 0; $pn < $pagesNum; $pn++)
					{
						//$pdf->AddPage();
						$pdf->AddPage('P', array(102, 153), false, false);
						/* Update PDF to 4" x 6" */
						$localMainFont = 11;
						$localBigFont = 52;
						$localSmallFont = 8;
						$localRegularCellHeight = 6;
						$localBigCellHeight = 22;
						$localSmallCellHeight = 5;
						$localBottomContentY = 120;
                                                
						$pdf->SetFont('helvetica','B',$localSmallFont);
                        if (extrCarrier($originCarrierName) == "RL")
						{
							$pdf->SetXY(24, 40);
							$pdf->Cell(58,$localSmallCellHeight, $arr[0]['ShipAddress_Addr1'],0,1,'L');
							$actual_position_y = $pdf->GetY();
							$pdf->SetXY(24, $actual_position_y);
							$pdf->Cell(58,$localSmallCellHeight, $arr[0]['ShipAddress_Addr2'].", - ",0,1,'L');
							$actual_position_y = $pdf->GetY();
							$pdf->SetXY(24, $actual_position_y);
							$pdf->Cell(58,$localSmallCellHeight, $arr[0]['ShipAddress_City'].", ".$arr[0]['ShipAddress_State'].", ".$arr[0]['ShipAddress_PostalCode'],0,1,'L');
							$actual_position_y = $pdf->GetY();
							$pdf->SetXY(24, $actual_position_y);
							$pdf->Cell(58,$localSmallCellHeight, $arr[0]['FOB'],0,1,'L');
						}
 						$pdf->SetFont('helvetica','B',$localMainFont);
						//$actual_position_y = $pdf->GetY();
						$actual_position_y = 62; // 120 => 0.4040 => 62
						$pdf->SetXY(23, $actual_position_y);
						$pdf->Cell(58, 20, " ", 1, 1, 'C');
						$pdf->SetXY(25, $actual_position_y+1);
						$pdf->Cell(58,$localSmallCellHeight,"PRO# ".$pro10,0,1,'L');
						$pdf->SetXY(26, $actual_position_y+7);
                                                $pdf->Image($barcodeBill, 26, $actual_position_y+7, 30);
						if (isCarrierNameCorrect($originCarrierName))
						{
							$carrierLogo = getCarrierLogo($originCarrierName);
							$pdf->Image($carrierLogo, 24, 84, 0, 17, 'PNG'); // 47x165 -- 0x17
									
							$pdf->SetFont('helvetica','B',$localSmallFont);
							$pdf->SetXY(24, 103);
							$pdf->Cell(82,$localSmallCellHeight,$originCarrierName,0,1,'L');
						}
					}
				}
				if(!empty($ProductsWarnings))
				{
					//$pdf->AddPage();
                                        $pdf->AddPage('P', array(102, 153), false, false);
                                        /* Update PDF to 4" x 6" */
                                        $localSmallFont = 8;
                                        $localRegularCellHeight = 6;
					$pdf->SetFont('helvetica','B',$localSmallFont);
					foreach($ProductsWarnings as $ProductsWarning)
					{
						$pdf->Cell(82,$localRegularCellHeight,$ProductsWarning,0,1,'L');
					}
				}
                                //!!!
			
			
				//$pdf->Output($pdf_path);
				$pdf->Output($pdf_path, 'F');
				$result[] = $pdf_path2;
			}
			if (!empty($result))
			{
				return $result;
			} else
			{
				error_msg("Can not generate any pdf!");
				send_error();
			}
	}catch (Exception $e) {
		error_msg($e->getMessage());
		send_error();
	}
}

function flyersAllDealers($PONumber, $list_of_products)
{
    /*$name = $_SESSION["name"];
    if(!empty($name) && $name == 'Allison Histand')
    {
        return flyersAllDealersLetter($PONumber, $list_of_products);
    } else
    {*/
        return flyersAllDealersSmall($PONumber, $list_of_products);
    /*}*/
}


function flyersAllDealersLetter($PONumber, $list_of_products)
{
	try{
                        $connect = Database::getInstance()->dbc;
                 
                
                
                                                $pdf_path = "../Pdf/".repPo($PONumber)."flyer.pdf";
						$pdf_path2 = "Pdf/".repPo($PONumber)."flyer.pdf";
						$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
						$pdf->SetPrintHeader(false);
						$pdf->SetPrintFooter(false);
						$pdf->SetMargins(5, 5);
						$pdf->SetAutoPageBreak(FALSE, 0);
						$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
                $flyers_array = [];                                
                foreach($list_of_products as $txnlineid_row)
                {
                    $txnlineid = $txnlineid_row['TxnLineID'];
                $array = getProductCodeHomeDepotDCLabel($connect, $PONumber, $txnlineid);
		$qty = $array[0]['Quantity'];
		for($i = 0; $i < $qty; $i++)
		{
			if(!empty($array[0]['collection_name']) && !empty($array[0]['drawing']))
			{
				$array[$i] = $array[0];
				$value = getToken(9);
				$array[$i]['code'] = $value;
				$array[$i]['qr_code'] = gen_qrcode($value);
			} else{
				error_msg("Error. Collection name or drawing is empty for this product.");
			}
		}
		
		foreach($array as $key => $row)
		{
			$array[$key]['SKU'] = remove_G($row['SKU']);
			$array[$key]['ItemGroupRef_FullName'] = remove_G($row['ItemGroupRef_FullName']);
			if(isset($row['dimensions']) && !empty($row['dimensions']))
			{
				$dimensions = explode(",", $row['dimensions']);
				foreach($dimensions as $key2 => $dimension)
				{
					$dimensions[$key2] = trim($dimension);
				}
				$array[$key]['dimensions'] = $dimensions;
			} else
			{
				$array[$key]['dimensions'] = null;
			}
		}
                        
		$arr = $array;		

						
				foreach ($arr as $key => $row)
				{
						
                                    for ($box = 0;$box<$row['Box_Count'];$box++)
                                    {
					
					if(!empty($row['collection_name']) && !empty($row['drawing']))
					{
                                            
						/*$pdf->AddPage('P', array(102, 153), false, false);
						$pdf->SetFont('helvetica', '', 5);*/
                                                
						$DLOGO   = "../Include/pictures/drlogo.png";
						$HEADER  = $row['collection_name'];
						$DRAWING = "../drawings/".$row['drawing'];
                                                if (!file_exists($DRAWING)) {
                                                    $DRAWING = "../drawings/spacer.gif";
                                                }
						$QRCODE  = $row['qr_code'];

						$paramArr = $row['dimensions'];
						if (!empty($paramArr))
						{
							$PARAMS   = getPdfParams( $paramArr );
						} else
						{
							$PARAMS = '<tr><td></td></tr>';
						}
						$CODE     = $row['code'];
						$tbl = '
						<table border="0" width="364" cellpadding="1">
						<tr>
							<td colspan="2" align="center"><img height="40" src="'.$DLOGO.'" /></td>
						</tr>
						<tr>
							<td colspan="2" height="50" align="center"><strong><font size="25">'.$HEADER.'</font></strong></td>
						</tr>
						<tr>
							<td colspan="2" align="center"><img height="186" src="'.$DRAWING.'"><br></td>
						</tr>
						<tr>
							<td width="33%"><img src="'.$QRCODE.'" /></td>
							<td width="67%">
								<br><br><font size="10"><strong>'.$row['ItemGroupRef_FullName'].'</strong></font><br>
								<font size="10">PO: <strong>'.$PONumber.'</strong></font><br>
								<font size="10">Customer: <strong>'.$row['CustomerRef_FullName'].'</strong></font><br>
								<table cellpadding="0" cellpadding="0">
									'.$PARAMS.'
								</table>
							</td>
						</tr>
						<tr>
							<td width="33%">
								<font size="7"> Scan QR code to obtain<br> product information and<br> register your purchase</font>
							</td>
							<td width="67%">
								<font size="8">Visit <strong>www.DreamLine.com/warranty/</strong> and add <br />your code <b>('.$CODE.')</b></font>
							</td>
						</tr>
						</table>
						<table border="0" width="364" cellpadding="0">
						<tr>
							<td align="center">__________________________________<br><font size="8"><strong>DreamLine.com</strong></font></td>
						</tr>
						</table>
						';

						//$pdf->writeHTML($tbl, true, false, false, false, '');
                                                $flyers_array[]= $tbl;
					}
                                    }
						
				}
                }
                
                $number_of_letters = ceil(count($flyers_array) / 4);
                $flyer_index = -1;
                for($i=0; $i<$number_of_letters; $i++)
                {
                    $pdf->AddPage('P', array(215.9, 279.4), false, false);
                    $pdf->SetFont('helvetica', '', 5);
                    $tbl = '<table border="0" align="center" cellpadding="1">
                                <tr>
                                    <td align="center" height="480">'.if_flyer_exists($flyers_array, ++$flyer_index).'</td><td align="center">'.if_flyer_exists($flyers_array, ++$flyer_index).'</td>
                                </tr>
                                <tr>
                                    <td align="center" height="480">'.if_flyer_exists($flyers_array, ++$flyer_index).'</td><td align="center">'.if_flyer_exists($flyers_array, ++$flyer_index).'</td>
                                </tr>
                            </table>';
                    $pdf->writeHTML($tbl, true, false, false, false, '');
                }
				$pdf->Output($pdf_path, 'F');
			if (!empty($pdf_path2))
			{
				saveQrCode($arr);
				return $pdf_path2;
			} else
			{
				error_msg("Can not generate any flyer!");
                                return false;
			}
	}catch (Exception $e) {
		error_msg($e->getMessage());
		send_error();
	}
}

function if_flyer_exists($flyers_array, $flyer_index)
{
    if(isset($flyers_array[$flyer_index]))
        return $flyers_array[$flyer_index];
    else
        return '';
}

function flyersAllDealersSmall($PONumber, $list_of_products)
{
	try{
                        $connect = Database::getInstance()->dbc;
                 
                
                
                                                $pdf_path = "../Pdf/".repPo($PONumber)."flyer.pdf";
						$pdf_path2 = "Pdf/".repPo($PONumber)."flyer.pdf";
						$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
						$pdf->SetPrintHeader(false);
						$pdf->SetPrintFooter(false);
						$pdf->SetMargins(5, 5);
						$pdf->SetAutoPageBreak(FALSE, 0);
						$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
                                               
                foreach($list_of_products as $txnlineid_row)
                {
                    $txnlineid = $txnlineid_row['TxnLineID'];
                $array = getProductCodeHomeDepotDCLabel($connect, $PONumber, $txnlineid);
		$qty = $array[0]['Quantity'];
                if(!empty($qty))
                {
                    for($i = 0; $i < $qty; $i++)
                    {
                            if(!empty($array[0]['collection_name']) && !empty($array[0]['drawing']))
                            {
                                    $array[$i] = $array[0];
                                    $value = getToken(9);
                                    $array[$i]['code'] = $value;
                                    $array[$i]['qr_code'] = gen_qrcode($value);
                            } else{
                                    error_msg("Error. Collection name or drawing is empty for this product.");
                            }
                    }
                } else
                {
                    error_msg('Error! Empty or zero quantity!');
                    send_error();
                }
		
		foreach($array as $key => $row)
		{
			$array[$key]['SKU'] = remove_G($row['SKU']);
			$array[$key]['ItemGroupRef_FullName'] = remove_G($row['ItemGroupRef_FullName']);
			if(isset($row['dimensions']) && !empty($row['dimensions']))
			{
				$dimensions = explode(",", $row['dimensions']);
				foreach($dimensions as $key2 => $dimension)
				{
					$dimensions[$key2] = trim($dimension);
				}
				$array[$key]['dimensions'] = $dimensions;
			} else
			{
				$array[$key]['dimensions'] = null;
			}
		}
                        
		$arr = $array;		

						
				foreach ($arr as $key => $row)
				{
						
                                    for ($box = 0;$box<$row['Box_Count'];$box++)
                                    {
					
					if(!empty($row['collection_name']) && !empty($row['drawing']))
					{
                                            
						$pdf->AddPage('P', array(102, 153), false, false);
						$pdf->SetFont('helvetica', '', 5);
                                                
						$DLOGO   = "../Include/pictures/drlogo.png";
						$HEADER  = $row['collection_name'];
						$DRAWING = "../drawings/".$row['drawing'];
                                                if (!file_exists($DRAWING)) {
                                                    $DRAWING = "../drawings/spacer.gif";
                                                }
						$QRCODE  = $row['qr_code'];

						$paramArr = $row['dimensions'];
						if (!empty($paramArr))
						{
							$PARAMS   = getPdfParams( $paramArr );
						} else
						{
							$PARAMS = '<tr><td></td></tr>';
						}
						$CODE     = $row['code'];
						$tbl = '
						<table border="0" width="325" cellpadding="1">
						<tr>
							<td colspan="2" align="center"><img height="40" src="'.$DLOGO.'" /></td>
						</tr>
						<tr>
							<td colspan="2" height="50" align="center"><strong><font size="25">'.$HEADER.'</font></strong></td>
						</tr>
						<tr>
							<td colspan="2" align="center"><img height="200" src="'.$DRAWING.'"><br></td>
						</tr>
						<tr>
							<td width="33%"><img src="'.$QRCODE.'" /></td>
							<td width="67%">
								<br><br><font size="10"><strong>'.$row['ItemGroupRef_FullName'].'</strong></font><br>
								<font size="10">PO: <strong>'.$PONumber.'</strong></font><br>
								<font size="10">Customer: <strong>'.$row['CustomerRef_FullName'].'</strong></font><br>
								<table cellpadding="0" cellpadding="0">
									'.$PARAMS.'
								</table>
							</td>
						</tr>
						<tr>
							<td width="33%">
								<font size="7"> Scan QR code to obtain<br> product information and<br> register your purchase</font>
							</td>
							<td width="67%">
								<font size="8">Visit <strong>www.DreamLine.com/warranty/</strong> and add <br />your code <b>('.$CODE.')</b></font>
							</td>
						</tr>
						</table>
						<table border="0" width="325" cellpadding="0">
						<tr>
							<td align="center">__________________________________<br><font size="8"><strong>DreamLine.com</strong></font></td>
						</tr>
						</table>
						';

						$pdf->writeHTML($tbl, true, false, false, false, '');
					}
                                    }
						
				}
                }
				$pdf->Output($pdf_path, 'F');
			if (!empty($pdf_path2))
			{
				saveQrCode($arr);
				return $pdf_path2;
			} else
			{
				error_msg("Can not generate any flyer!");
                                return false;
			}
	}catch (Exception $e) {
		error_msg($e->getMessage());
		send_error();
	}
}


//Generate pdf files for each PO number with separate pages for each box number with barcodes and info
function truckPreload($arr)
{
    try{

	$pdf_path = "../Pdf/".repPo($arr[0]['PONUMBER'])."_truck.pdf";
	$pdf_path2 = "Pdf/".repPo($arr[0]['PONUMBER'])."_truck.pdf";
	$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
	$pdf->SetPrintHeader(false);
	$pdf->SetPrintFooter(false);
	$pdf->SetMargins(5, 5);
	$pdf->SetAutoPageBreak(FALSE, 0);
	$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        foreach($arr as $row)
        {
            $pdf->AddPage('L', array(102, 51), false, false);
            $pdf->SetFont('helvetica', '', 14);
            $tbl = '
                <table border="0" cellpadding="5">
                    <tr>
                        <td align="center">
                            PO: <b>'.$row['PONUMBER'].'</b>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <img src="'.$row['barcode'].'">
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            PALLET: <b>'.$row['PALLET'].'</b> TRUCK: <b>'.$row['TRUCK'].'</b>
                        </td>
                    </tr>
                </table>
            ';
            $pdf->writeHTML($tbl, true, false, false, false, '');
        }

        $pdf->Output($pdf_path, 'F');

	return $pdf_path2;
	}catch (Exception $e) {
		error_msg($e->getMessage());
		send_error();
	}
}