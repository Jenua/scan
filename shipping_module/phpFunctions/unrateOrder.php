<?php
require('functionsDB.php');
require('functionsExtr.php');
$path = $_SERVER['DOCUMENT_ROOT'];
require_once($path.'/db_connect/connect.php');
date_default_timezone_set('America/New_York');

$connect = Database::getInstance()->dbc;

//Start if have PO number
if ((isset($_REQUEST['value']) && !empty($_REQUEST['value'])) && (isset($_REQUEST['bol']) && !empty($_REQUEST['bol'])) && (isset($_REQUEST['carrier']) && !empty($_REQUEST['carrier'])))
{
	updateProBol($connect, $_REQUEST['value'], $_REQUEST['bol'], $_REQUEST['carrier']);
} else 
{
	echo "Can not get values.";
	echo $_REQUEST['value'];
	echo $_REQUEST['carrier'];
}

$connect = null;

//write PRO# and BOL# to ShipTrackWithLabel
function updateProBol($connect, $Po, $bol, $carrier)
{
	$extrCarrier = extrCarrier($carrier);
	try{
			$query = "UPDATE [dbo].[".SHIPTRACKWITHLABEL_TABLE."] SET [CARRIERNAME] ='".$carrier."', [QUOTENUMBER] = 'N/A', [TRANSITTIME] = 'N/A', [COST] = '0', [CARRIERNAME_PRO] = NULL, [PRO] = NULL, [BOL] = '".$bol."' WHERE [ID] = '".$Po."'";
			$result = execQuery($connect, $query);
			if ($result) echo 'Order unrated.';
	}catch (Exception $e) {
		print_r("Can not unrate order!");
	}
}
?>