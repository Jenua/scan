<?php
ini_set('max_execution_time', 120);
ini_set('error_reporting', E_ERROR);
require('functionsExtr.php');
$path = $_SERVER['DOCUMENT_ROOT'];
require_once($path.'/db_connect/connect.php');

function exec_query($conn, $query) {
	try {
		$result = $conn->prepare($query);
		$result->execute();
		return $result;
	}
	catch(PDOException $e) {
		print_r($e->getMessage());
	}
}
function returnValue( $type, $value ) {
	$error = [
		$type => $value
	];
	unset( $conn );
	die( json_encode($error) );	
}
function parseCsvFile($fileName, $isHeaderSkip = true)
{
	$csvData = file_get_contents($fileName);	
	$csvData = str_replace("\r\n", "\n", $csvData);	
	
	$lines = explode("\n", $csvData);
	
	$array = array();
	foreach ($lines as $k => $line) {
		if( $line && ($k>0 || !$isHeaderSkip) ){
			$array[] = str_getcsv($line, ',');
		}
	}
	return $array;
}
function debug($data){	
	echo "<pre>".print_r($data,true)."</pre>";
}
function filterEmpty($element) {	
	return( !empty($element) && trim($element) != '' );	
}

function parseContent( $content ) {
	$result = [];
	foreach( $content as $row ) {
		$product = $row[0];
		unset( $row[0] );
		$related = array_filter( $row, "filterEmpty" );
		
		if( empty($result[$product]) ){
			$result[$product] = array_unique($related);
		} else {
			$result[$product] = array_unique(array_merge( $result[$product], $related ));
		}
	}	
	return $result;
}

function updateTable($conn, $content, $user, $truncate = false) {	
	if( $truncate ) {
		$tQuery = "TRUNCATE TABLE [fpt_related];";
		exec_query($conn, $tQuery);
	}
	$inputValues = '';
	$added = 0;
	foreach($content as $product => $row) {
		$product = $conn->quote( preg_replace( "/^(G)/i", '', $product ) );
		foreach( $row as $related ) {
			$related = $conn->quote( preg_replace( "/^(G)/i", '', $related ) );
			if( $inputValues != '' ) {
				$inputValues .= ' UNION ALL ';
			}
			$inputValues .= "SELECT ".$product.", ".$related;
			++$added;
		}
	}
	$message = 'User '.$user.' imported data using '.($truncate?'replace':'update').' mode';
	
	$updateQuery = "DECLARE @input TABLE ( Product nvarchar(128) NULL, Related nvarchar(128) NULL );
					
					INSERT INTO @input ".$inputValues.";
					
					INSERT INTO fpt_related ([Product], [Related])
					SELECT
						i.[Product],
						i.[Related]
					FROM @input as i
					LEFT JOIN [fpt_related] as fpt ON fpt.[Product] = i.[Product] AND fpt.[Related] = i.[Related]
					LEFT JOIN [DL_valid_SKU] as DLp ON DLp.[SKU] = i.[Product] AND DLp.[UPC] IS NOT NULL AND ISNULL(DLp.[QB_SKU], '') != ISNULL(DLp.[Menards_SKU], '')
					LEFT JOIN [DL_valid_SKU] as DLr ON DLr.[SKU] = i.[Related] AND DLr.[UPC] IS NOT NULL AND ISNULL(DLr.[QB_SKU], '') != ISNULL(DLr.[Menards_SKU], '')
					WHERE fpt.[ID] IS NULL
					AND Dlp.[SKU] IS NOT NULL
					AND Dlr.[SKU] IS NOT NULL;
					
					INSERT INTO [log] ([po],[message],[DATE]) VALUES ('FPTRelated', ".$conn->quote($message).", GETDATE());
					";
	
	$updateResult = exec_query( $conn, $updateQuery );
}

// ==========================================================================================================================================

if( empty($_FILES['fpt_file']) ) {	
	returnValue( 'error', 'File uploading error' );
}
if( $_FILES['fpt_file']['error'] ) {
	returnValue( 'error', 'Uploading eror: '.$_FILES['fpt_file']['error'] );
}
if( !empty($_REQUEST['replace']) && $_REQUEST['replace']=='1' ) {
	$replaceMode = 1;	
} else {
	$replaceMode = 0;
}
$user    = empty($_REQUEST['user']) ? false : $_REQUEST['user'];
$content = parseCsvFile($_FILES['fpt_file']['tmp_name'], false);
if( empty($content)  ) {	
	returnValue( 'error', 'Empty file' );	
}
$content = parseContent( $content );

$conn = Database::getInstance()->dbc;
updateTable($conn, $content, $user, $replaceMode);
returnValue( 'result', '1' );
//*/



