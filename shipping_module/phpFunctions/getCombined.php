<?php
$path = $_SERVER['DOCUMENT_ROOT'];
require_once($path.'/db_connect/connect.php');
	if ((isset($_REQUEST['id']) && !empty($_REQUEST['id'])) || (isset($_REQUEST['po']) && !empty($_REQUEST['po'])))
	{
		$returnValue = false;
		$conn = Database::getInstance()->dbc;

		if (isset($_REQUEST['id'])){
		$id = $_REQUEST['id'];
		$res = getDivided($conn, $id);
            if ($res) {
			$resultArray = array();
                foreach ($res as $key => $value) {
                    $resultArray[] = array($value['PONumber'], $value['Combined_into_POnumber']);
			}
			$returnValue = $resultArray;
		}
        }
        else if(isset($_REQUEST['po'])){
            $po = $_REQUEST['po'];
            $res = (isset($_REQUEST['complicatedPo']) && $_REQUEST['complicatedPo'] == true )?
                isCombined($conn,$po) :
                isCombinedInto($conn,$po);
            if (isset($res) && !empty($res))
                $returnValue = $res;
        }
        $returnValue = json_encode($returnValue);
        print_r($returnValue);
        $conn = null;
	} else
	{
		$returnValue = 'Param is missing.';
		$returnValue = json_encode($returnValue);
		print_r($returnValue);
	}

function getDivided($conn, $id)
{
	try{
		$query = "SELECT [Orders_combined].[PONumber], [Orders_combined].[Combined_into_POnumber]
						  FROM [dbo].[Orders_combined]
						  INNER JOIN [dbo].[manually_shiptrack] ON [Orders_combined].[Combined_into_RefNumber] = [manually_shiptrack].[RefNumber]
						  WHERE [Combined_into_RefNumber] IN
(SELECT [Combined_into_RefNumber]
						  FROM [dbo].[Orders_combined]
						  WHERE [Orders_combined].[RefNumber] = '".$id."')
						 ";
		$result = $conn->prepare($query);
		$result->execute();
		$result = $result->fetchAll(PDO::FETCH_ASSOC); 
		return $result;
	}
	catch(PDOException $e)
	{
		return false;
	}
}

/**
 * @param $conn
 * @param $po - part of Combined or Splitted PoNumber
 * @return array(intoPONumber,intoRefNumber,combined type/divided type)
 */
function isCombinedInto($conn,$po)
{
    try {
        $query = "select q.POnumber, q.intoPO, q.intoRef, q.[type]
                      FROM (
                        SELECT [POnumber],[Divided_into_POnumber] as intoPO, [Divided_into_RefNumber] as intoRef, 'divided' as [type]
                          FROM [dbo].[Orders_divided] 
                        UNION
                        SELECT [POnumber],[Combined_into_POnumber] as intoPO, [Combined_into_RefNumber] as intoRef, 'combined' as [type]
                         FROM [dbo].[Orders_combined]
					  ) as q
                      left JOIN [dbo].[manually_shiptrack] ON q.intoRef = [manually_shiptrack].[RefNumber]
						  WHERE q.POnumber = '" . $po . "'";
        $result = $conn->prepare($query);
        $result->execute();
        $result = $result->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    } catch (PDOException $e) {
        return false;
    }
}

/** Define is the PoNumber is Combined from others orders
 * @param $conn
 * @param $po
 * @return bool
 */
function isCombined($conn,$po){
	    try{
	        $query = "SELECT [Combined_into_POnumber]
                                    FROM [dbo].[Orders_combined]
                                    WHERE [Combined_into_POnumber]=?";
            $stmt = $conn->prepare($query);
            $stmt->bindParam(1, $po, PDO::PARAM_STR);
            $stmt->execute();
            $row = $stmt->fetch(PDO::FETCH_ASSOC);

            return $row;
        }
	    catch(PDOException $e){
	        return false;
        }
}
?>