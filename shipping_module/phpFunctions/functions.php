<?php
session_start();
//die('stop');
ini_set('max_execution_time', 7200);
ini_set("memory_limit", "2000M");
//Include files
require_once('../Include/barcode/class/BCGFontFile.php');
require_once('../Include/barcode/class/BCGcode39.barcode.php');
require_once('../Include/barcode/class/BCGcode128.barcode.php');
require_once('../Include/barcode/class/BCGColor.php');
require_once('../Include/barcode/class/BCGDrawing.php');
require_once('../Include/barcode/class/BCGpdf417.barcode2d.php');
require_once('../Include/barcode/class/BCGi25.barcode.php');
require_once('../Include/barcode/class/BCGgs1128.barcode.php');
require_once('../Include/barcode/class/BCGupca.barcode.php');
require_once('../Include/fpdf17/fpdf.php');
require_once('../Include/tcpdf/tcpdf.php');
require_once('functionsPDF.php');
require_once('functionsDB.php');
require_once('functionsBar.php');
require_once('functionsExtr.php');
$path = $_SERVER['DOCUMENT_ROOT'];
require_once($path.'/db_connect/connect.php');

//Global variables
$error_messages = array();//Array to save all errors
$data_error_messages = array();//Array to save all errors in input data
$connect = Database::getInstance()->dbc;
$pdf_patheLabel = false;//path to pdf. False while not created
$pdf_patheFlyer = false;//path to pdf. False while not created

//read settings from DB
$settings = getSettings($connect);

foreach ($settings as $setting)
{
	if ($setting['group_name'] == 'PrintCarrierForAmazon' && $setting['type_name'] == 'PrintCarrierForAmazonMode')
	{
		define("PrintCarrierForAmazonMode", $setting['setting_value']);
	}
}

//Start if have PO number
if (isset($_REQUEST['value']) && !empty($_REQUEST['value']) && isset($_REQUEST['id']) && !empty($_REQUEST['id']))
	{
            /*print_r($_REQUEST);
            die('request');*/
		$pro10 = 'none';
		$barcodeBill = 'none';
		$originCarrierName = 'none';
                if(isset($_REQUEST['combine']) && !empty($_REQUEST['combine'])) 
                {
                    $combine = true;
                }else 
                {
                    $combine = false;
                }
		main($_REQUEST['value'], $_REQUEST['id'], $_REQUEST['pro'], $_REQUEST['bol'], $combine);
		$connect = null;
	}

function assign_pro10_barcodeBill($array, $pro, $bol)
	{
		global $pro10, $barcodeBill, $originCarrierName;
		if (isset($array[0]['CARRIERNAME']) && !empty($array[0]['CARRIERNAME']) && $array[0]['CARRIERNAME'] != null)
		{
			$originCarrierName = $array[0]['CARRIERNAME'];
			$carrierName = extrCarrier($originCarrierName);
			if ($array[0]['CustomerRef_FullName'] != "AmazonDS" && $array[0]['CustomerRef_FullName'] != "Amazon.Canada")
			{
                            switch ($carrierName)
                            {
                                case "FEDEX":
                                        $pro10 = validatePro($pro);
                                        $barcodeBill = gen_imageFedex($pro10);
                                        break;
                                case "RL":
                                        $pro10 = validatePro($pro);
                                        $pro10 = str_replace("-", "", $pro10);
                                        $barcodeBill = gen_imageFedex($pro10);
                                        break;
                                case "ESTES":
                                        $pro10 = validatePro($pro);
                                        $barcodeBill = gen_imageFedex($pro10);
                                        break;
                                case "UPS":
                                        $pro10 = validatePro($pro);
                                        $barcodeBill = gen_imageFedex($pro10);
                                        break;
                                case "YRC":
                                        $pro10 = validatePro($pro);
                                        $barcodeBill = gen_imageFedex($pro10);
                                        break;
                                case "NEW ENGL":
                                        $pro10 = validatePro($pro);
                                        $barcodeBill = gen_imageFedex($pro10);
                                        break;
                                case "ODFL":
                                        $pro10 = validatePro($pro);
                                        $barcodeBill = gen_imageFedex($pro10);
                                        break;
                                case "PYLE":
                                        $pro10 = validatePro($pro);
                                        $barcodeBill = gen_imageFedex($pro10);
                                        break;
                            }
			} else if($array[0]['CustomerRef_FullName'] == "AmazonDS")
			{
				$carrier = extrCarrier($array[0]['ShipMethodRef_FullName']);
				if ($carrier == 'CEVA')
				{
					$pro10 = validatePro($pro);
					$barcodeBill = gen_imageFedex($pro10);
				} else if($carrier == 'ESTES')
				{
					$pro10 = validatePro($pro);
					$barcodeBill = gen_imageFedex($pro10);
				} else if($carrier == 'ABF')
				{
					$pro10 = validatePro($pro);
					$barcodeBill = gen_imageFedex($pro10);
				} else
				{
					$pro10 = 'none';
					$barcodeBill = 'none';
				}
			}else
			{
				$pro10 = 'none';
				$barcodeBill = 'none';
			}
			
		}
	}

//Main function to execute functions kit depending on store name
function main($PONumber, $id, $pro, $bol, $combine)
{
	global $error_messages;
	global $data_error_messages;
	global $connect;
	global $pdf_patheLabel;
        global $pdf_patheFlyer;
	global $pro10, $barcodeBill, $originCarrierName;
	
	/*$pdf_path = "../Pdf/".repPo($PONumber).".pdf";
	if (file_exists($pdf_path))
	{
		$pdf_patheLabel[] = "Pdf/".repPo($PONumber).".pdf";
		send_error();
	}*/
	
	$array = getProductCodeLabels($connect, $PONumber, $id);
	/*print_r($array);
        die();*/
	
	if (isset($array[0]['CARRIERNAME']) && !empty($array[0]['CARRIERNAME']) && $array[0]['CARRIERNAME'] != null)
		{
			$originCarrierName = $array[0]['CARRIERNAME'];
		}
	
	//$array = test1();//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	$storeName = extrStore($array[0]['CustomerRef_FullName']);
	//$ProductsWarnings = getProductsWarnings($connect, $array[0]['RefNumber']);
	$ProductsWarnings = false;
	
	//$storeName = "Sears";//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	//$originCarrierName = "Sears";//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	//$array[0]['CustomerRef_FullName'] = "Sears";//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        
                switch ($storeName)
	{
    case "Lowe's":
		issetProBol($pro, $bol);
		assign_pro10_barcodeBill($array, $pro, $bol);
                //$combine = true; //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                if($combine) 
                {
                    $list_of_products = get_products_for_po_lowes($PONumber);
                    /*print_r($list_of_products);
                    die('lowes');*/
                } else {
                    $list_of_products = get_products_for_po($PONumber);
                    /*print_r($list_of_products);
                    die('not lowes');*/
                }
                
                if(!empty($list_of_products))
                {
                    $pdf_patheFlyer = flyersAllDealers($PONumber, $list_of_products);
                    if ($combine) $array = filterAmazonSKUs($array, $list_of_products);
                }
                /*print_r($array);
                die('lowes');*/
                
                $array = boxes($array);
		$array = dublic($array);
                $array = getUPC($connect, $array);
		$array = gen_images($array);
		usort($array, 'ab');
                if ($combine) $array = array_merge($array, $array);
		$pdf_patheLabel = lowesPdf($array, $pro10, $barcodeBill, $originCarrierName, $ProductsWarnings);
                
		send_error();
        break;
    case "Amazon":
		issetProBol($pro, $bol);
		assign_pro10_barcodeBill($array, $pro, $bol);
                
		if($array[0]['CustomerRef_FullName'] == 'Amazon') 
		{
                    $list_of_products = get_products_for_po_amazon($PONumber);
                    if ($list_of_products) $array = filterAmazonSKUs($array, $list_of_products);
		} else
                {
                    $list_of_products = get_products_for_po($PONumber);
                }
                
		$array = boxes($array);
		$array = dublic($array);
		$array = getUPC($connect, $array);
		$array = gen_imagesBCGpdf417($array);
		usort($array, 'ab');
                if ($combine) $array = array_merge($array, $array);
                
               

		$pdf_patheLabel = amazonPdf($array, $pro, $bol, $pro10, $barcodeBill, $originCarrierName, $ProductsWarnings);
                
                
                if(!empty($list_of_products))
                {
                    $pdf_patheFlyer = flyersAllDealers($PONumber, $list_of_products);
                }
                
		send_error();
        break;
	case "Amazon.Canada":
		issetProBol($pro, $bol);
		assign_pro10_barcodeBill($array, $pro, $bol);
                
                if($array[0]['CustomerRef_FullName'] == 'Amazon.Canada' && $originCarrierName != 'Shipping Ground') 
		{
                    $list_of_products = get_products_for_po_amazon_canada($PONumber);
                    if ($list_of_products) $array = filterAmazonSKUs($array, $list_of_products);
		} else
                {
                    $list_of_products = get_products_for_po($PONumber);
                }
		
		$array = boxes($array);
		$array = dublic($array);
		$array = getUPC($connect, $array);
		$array = gen_imagesBCGpdf417($array);
		usort($array, 'ab');
                if ($combine) $array = array_merge($array, $array);
		//$pdf_patheLabel = amazonCanadaPdf($array, $pro, $bol, $pro10, $barcodeBill, $originCarrierName, $ProductsWarnings);
		$pdf_patheLabel = amazonPdf($array, $pro, $bol, $pro10, $barcodeBill, $originCarrierName, $ProductsWarnings);
                
                if(!empty($list_of_products))
                {
                    $pdf_patheFlyer = flyersAllDealers($PONumber, $list_of_products);
                }
                
		send_error();
        break;
	case "AmazonDS":
		issetProBol($pro, $bol);
		assign_pro10_barcodeBill($array, $pro, $bol);
		
		$array = boxes($array);
		$array = dublic($array);
		usort($array, 'ab');
                if ($combine) $array = array_merge($array, $array);
		//print_r($barcodeBill);
		//print_r($pro10);
		$pdf_patheLabel= amazonDSPdf($array, $pro10, $barcodeBill, $originCarrierName, $ProductsWarnings);
                
                $list_of_products = get_products_for_po($PONumber);
                if(!empty($list_of_products))
                {
                    $pdf_patheFlyer = flyersAllDealers($PONumber, $list_of_products);
                }
                
		send_error();
        break;
	 case "Sears":
		issetProBol($pro, $bol);
	 	assign_pro10_barcodeBill($array, $pro, $bol);
		
		$array = boxes($array);
		$array = dublic($array);
		$array = extrPc($array);
		$array = getUPC($connect, $array);
		if (!isset($array[0]['SERIAL_REFERENCE']) || empty($array[0]['SERIAL_REFERENCE']))
		{
			$SSCCvalue = generateSSCCvalue($connect, false, $PONumber);
		} else
		{
			$SSCCvalue = generateSSCCvalue($connect, $array[0]['SERIAL_REFERENCE'], $PONumber);
		}
		$barcodeValue = '00081532401'.$SSCCvalue;
		$barcodeReadableValue = '(00) 0 81532401 '.$SSCCvalue;
		$barcodeGs1128 = gen_imageBCGgs1128($barcodeValue);
		$array = getValidSKU($connect, $array);
		$ShipAddress_Addr1 = extrShipAddress_Addr1($array[0]['ShipAddress_Addr1']);
                
                //$ShipAddress_Addr1 = 'Mygofer';//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		
		switch ($ShipAddress_Addr1)
		{
		case "Mygofer":
		case "Sears":
			$barcodeUPCA = gen_imageUPCA($array[0]['UPC']);
			break;
		default:
			$barcodeUPCA = 'none';
			break;
		}
		usort($array, 'ab');
                if ($combine) $array = array_merge($array, $array);
		$pdf_patheLabel = searsPdf($array, $barcodeGs1128, $barcodeUPCA, $barcodeReadableValue, $originCarrierName, $ProductsWarnings);
                
                $list_of_products = get_products_for_po($PONumber);
                if(!empty($list_of_products))
                {
                    $pdf_patheFlyer = flyersAllDealers($PONumber, $list_of_products);
                }
                
		send_error();
        break;
	case "C & R":
		issetProBol($pro, $bol);
		assign_pro10_barcodeBill($array, $pro, $bol);
		
                $array = boxes($array);
		$array = dublic($array);
		usort($array, 'ab');
                if ($combine) $array = array_merge($array, $array);
		$pdf_patheLabel = cnrPdf($array, $pro10, $barcodeBill, $originCarrierName, $ProductsWarnings);
                
                $list_of_products = get_products_for_po($PONumber);
                if(!empty($list_of_products))
                {
                    $pdf_patheFlyer = flyersAllDealers($PONumber, $list_of_products);
                }
                
		send_error();
        break;
	case "MENARDS":
		issetProBol($pro, $bol);
		assign_pro10_barcodeBill($array, $pro, $bol);
		
        $array = boxes($array);
		$array = dublic($array);
		$array = extrDate($array);
		$array = extrStoreNum($array);
		usort($array, 'ab');
                if ($combine) $array = array_merge($array, $array);
		$pdf_patheLabel = menardsPdf($array, $pro10, $barcodeBill, $originCarrierName, $ProductsWarnings);
                
                $list_of_products = get_products_for_po($PONumber);
                if(!empty($list_of_products))
                {
                    $pdf_patheFlyer = flyersAllDealers($PONumber, $list_of_products);
                }
                
		send_error();
        break;
	case "Wayfair":
		if (isset($pro) && !empty($pro)) 
		{
			assign_pro10_barcodeBill($array, $pro, $bol);
		} else{
			$pro10 = 'none';
			$barcodeBill = 'none';
		}
		$array = boxes($array);
		$array = dublic($array);
		usort($array, 'ab');
                if ($combine) $array = array_merge($array, $array);
		$pdf_patheLabel= homePdf($array, $pro10, $barcodeBill, $originCarrierName, $ProductsWarnings);
                
                $list_of_products = get_products_for_po($PONumber);
                if(!empty($list_of_products))
                {
                    $pdf_patheFlyer = flyersAllDealers($PONumber, $list_of_products);
                }
                
		send_error();
        break;
	case "Ferguson":
                if (isset($pro) && !empty($pro)) 
		{
			assign_pro10_barcodeBill($array, $pro, $bol);
		} else{
			$pro10 = 'none';
			$barcodeBill = 'none';
		}
		$ASN = $array[0]['ASN'];
		if(!empty($ASN))
		{
			$array = boxes($array);
			$array = dublic($array);
			usort($array, 'ab');
			$ASNPath = gen_imageCode128wayfair($ASN);
			$BOLPath = gen_imageCode128wayfair($bol);
                        if ($combine) $array = array_merge($array, $array);
			$pdf_patheLabel= fergusonPdf($array, $originCarrierName, $ProductsWarnings, $ASNPath, $ASN, $BOLPath, $bol, $pro10, $barcodeBill);
		}else
		{
			error_msg("ASN# needed!");
		}
                
                $list_of_products = get_products_for_po($PONumber);
                if(!empty($list_of_products))
                {
                    $pdf_patheFlyer = flyersAllDealers($PONumber, $list_of_products);
                }
                
		send_error();
        break;
	/*case "Depot":
		issetProBol($pro, $bol);
		$array = boxes($array);
		$array = dublic($array);
		$array = extrPc($array);
		$array = getUPCHD($connect, $array);
		usort($array, 'ab');
		//print_r($array);
		$last = false;
		foreach ($array as $key => $arr)
		{
			if ($last && isset($array[$key-1]) && ($arr['UPC'] == $array[$key-1]['UPC']))
			{
				$array[$key]['i25'] = $last;
			} else{
				$array[$key]['i25'] = gen_imagei25($arr['UPC']);
				$last = $array[$key]['i25'];
			}
		}
		$pdf_patheLabel= homePdfNew($array, $pro10, $barcodeBill, $originCarrierName, $ProductsWarnings);
		send_error();
        break;*/
	default: /*defaulted everyone else to standard home depot layout*/
		issetProBol($pro, $bol);
		assign_pro10_barcodeBill($array, $pro, $bol);
		$array = boxes($array);
		$array = dublic($array);
		usort($array, 'ab');
                if ($combine) $array = array_merge($array, $array);
		$pdf_patheLabel= homePdf($array, $pro10, $barcodeBill, $originCarrierName, $ProductsWarnings);
                
                $list_of_products = get_products_for_po($PONumber);
                if(!empty($list_of_products))
                {
                    $pdf_patheFlyer = flyersAllDealers($PONumber, $list_of_products);
                }
                
		send_error();
        break;
	}
	return send_error();
}

//is set pro and bol
function issetProBol($pro, $bol)
{
	if ((!isset($pro) || !isset($bol)) || (empty($pro) || empty($bol))  || ($pro == ' ' || $bol == ' '))
		{
			error_msg("PRO# and INVOICE# data needed!");
			send_error();
		} else return true;
}

//Sort array
function ab($a,$b)
{
	return ($a['boxNumber']-$b['boxNumber']);
}

//Fills an array with error messages
function error_msg($msg)
{
	global $error_messages;
	$error_messages[] = $msg;
}

//Fills an array with errors in input data
function data_error_msg($type, $id, $msg)
{
	global $data_error_messages;
	$row = array('type' => $type, 'id' => $id, 'msg' => $msg);
	$data_error_messages[] = $row;
}

//Generate list of error messages
function formErrors($error_messages)
{
	if (isset($error_messages) && !empty($error_messages))
	{
		$mes_text = "<b><p style='font-size: 12pt; color: red;'>Label:</p></b>";
		foreach($error_messages as $message){
			$mes_text.= "<b><p style='font-size: 10pt; color: black;'>".$message."</p></b>";
		}
		return $mes_text;
	} else return false;
}

//Generate table with errors in input data
function formDataErrors($data_error_messages)
{
	if (isset($data_error_messages) && !empty($data_error_messages))
	{
		$mes_text = "<b><p style='font-size: 12pt; color: red;'>Label. Detailed problems:</p></b><p><table border='1' style='font-size: 12pt; color: black;'>
		<tr>
			<th>type</th>
			<th>data</th>
			<th>problem</th>
		</tr>";
		foreach($data_error_messages as $message){
			$mes_text.= "<tr>
			<td>".$message['type']."</td>
			<td>".$message['id']."</td>
			<td>".$message['msg']."</td>
			</tr>";
		}
		$mes_text.= "</table></p>";
		return $mes_text;
	} else return false;
}

//Returns array to ajax query with errors and pathes to pdf files and terminates script
function send_error()
{
	global $error_messages;
	global $data_error_messages;
	global $pdf_patheLabel;
        global $pdf_patheFlyer;
	try
	{
		$formErrors = (string)formErrors($error_messages);
		$formDataErrors = (string)formDataErrors($data_error_messages);
		if ($pdf_patheLabel && isset($pdf_patheLabel) && !empty($pdf_patheLabel))
		{
			$html = (string)$pdf_patheLabel[0];
			$html = urlencode($html);
		} else $html = 'false';
                if ($pdf_patheFlyer && isset($pdf_patheFlyer) && !empty($pdf_patheFlyer))
		{
			$htmlFlyer = (string)$pdf_patheFlyer;
			$htmlFlyer = urlencode($htmlFlyer);
		} else $htmlFlyer = 'false';
                $po = urlencode($_REQUEST['value']);
                
		$result = array(
		'path' => $html,
                'pathFlyer' => $htmlFlyer,
		'er' => $formErrors,
		'DataEr' => $formDataErrors,
                'po' => $po
		);
		$result = json_encode($result);
		print_r($result);
	} catch (Exception $e){
		echo $e->getMessage();
	}
	die();
}
?>