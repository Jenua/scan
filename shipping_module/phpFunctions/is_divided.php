<?php

$path = $_SERVER['DOCUMENT_ROOT'];
require_once($path.'/db_connect/connect.php');



	if (isset($_REQUEST['id']) && !empty($_REQUEST['id']))
	{
		$returnValue = 'no';
		$id = $_REQUEST['id'];
		$conn = Database::getInstance()->dbc;
		$res = getDivided($conn, $id);
		$res2 = getCombined($conn, $id);
		if (isset($res) && !empty($res)) 
		{
			$returnValue = 'divided';
		}
		if (isset($res2) && !empty($res2)) 
		{
			$returnValue = 'combined';
		}
		$returnValue = json_encode($returnValue);
		print_r($returnValue);
		$conn = null;
	} else 
	{
		$returnValue = 'Id is missing.';
		$returnValue = json_encode($returnValue);
		print_r($returnValue);
	}


function getDivided($conn, $id)
{
	try{
		$query = "SELECT [manually_shiptrack].[PONumber]
						  FROM [dbo].[Orders_divided]
						  INNER JOIN [manually_shiptrack]
						  ON [Orders_divided].[Divided_into_RefNumber] = [manually_shiptrack].[RefNumber]
						  WHERE [Orders_divided].[RefNumber] = '".$id."'";
		$result = $conn->prepare($query);
		$result->execute();
		$result = $result->fetchAll(PDO::FETCH_ASSOC); 
		return $result;
	}
	catch(PDOException $e)
	{
		return false;
	}
}

function getCombined($conn, $id)
{
	try{
		$query = "SELECT [manually_shiptrack].[PONumber]
						  FROM [dbo].[Orders_combined]
						  INNER JOIN [manually_shiptrack]
						  ON [Orders_combined].[Combined_into_RefNumber] = [manually_shiptrack].[RefNumber]
						  WHERE [Orders_combined].[RefNumber] = '".$id."'";
		$result = $conn->prepare($query);
		$result->execute();
		$result = $result->fetchAll(PDO::FETCH_ASSOC); 
		return $result;
	}
	catch(PDOException $e)
	{
		return false;
	}
}
?>