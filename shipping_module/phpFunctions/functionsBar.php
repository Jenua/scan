<?php
function gen_qrcode($uid)
{
    $uniqid = uniqid();
	try{
		$value = 'http://dreamline.com/warranty/?SRVID='.$uid;
		$path = $_SERVER['DOCUMENT_ROOT'];
		require_once($path.'shipping_module/Include/phpqrcode/qrlib.php'); 
		$path2 = '../Images/qrcode_'.$uid.'_'.$uniqid.'.png';
		if(!findFile($path2))
		{
			QRcode::png($value, $path2, QR_ECLEVEL_L, 6);
		}
		return $path2;
	} catch (Exception $e){
		error_msg($e->getMessage());
		send_error();
	}
}

function findFile($filename)
{
	if (file_exists($filename)) {
     return true;
	} else
	{
		return false;
	}
}
	
//Generate barcodes for each UPC
function gen_images($arr)
{
	try{
		$img_result = array();
		$c = count($arr);
		for ($i=0; $i<$c; $i++)
		{
				$row = $arr[$i];
				$image = gen_image($row['UPC']);
				if (isset($image) && !empty($image))
				{
					$row['Path'] = $image;
					$arr[$i] = $row;
				} else
				{
					unset($arr[$i]);
					data_error_msg("UPC", $row['UPC'], "Can not generate barcode!");
				}
		}
			if (!empty($arr))
		{
			sort($arr);
			return $arr;
		} else
		{
			error_msg("Can not generate any barcodes!");
			send_error();
		}
	} catch (Exception $e){
		error_msg($e->getMessage());
		send_error();
	}
}

//Generate barcodes for each UPC BCGgs1128
function gen_imagesBCGpdf417($arr)
{
	try{
		$img_result = array();
		$c = count($arr);
		for ($i=0; $i<$c; $i++)
		{
				$row = $arr[$i];
				$valueForGen = "AMZN,PO:".$row['PONumber'].",UPC:".$row['UPC'].",QTY:".$row['quantity'];
				//print_r($valueForGen);
				$image = gen_imageBCGpdf417($valueForGen, $row['UPC']);
				if (isset($image) && !empty($image))
				{
					$row['Path'] = $image;
					$arr[$i] = $row;
				} else
				{
					unset($arr[$i]);
					data_error_msg("UPC", $row['UPC'], "Can not generate barcode!");
				}
		}
			if (!empty($arr))
		{
			sort($arr);
			return $arr;
		} else
		{
			error_msg("Can not generate any barcodes!");
			send_error();
		}
	} catch (Exception $e){
		error_msg($e->getMessage());
		send_error();
	}
}

//Generate barcode for input value BCGgs1128
function gen_imageBCGpdf417($value, $UPC)
{
global $error_messages;
$uniqid = uniqid();
	try{
	// The arguments are R, G, B for color.
	$color_black = new BCGColor(0, 0, 0);
	$color_white = new BCGColor(255, 255, 255); 

	$code = new BCGpdf417();
	$code->setScale(2); // Resolution
	$code->setColumn(3);
	$code->setForegroundColor($color_black); // Color of bars
	$code->setBackgroundColor($color_white); // Color of spaces
	$code->setErrorLevel(-1);
	$code->setCompact(false);
	$code->setQuietZone(true);
	$code->parse($value); // Text

	$path = '../Images/code417_'.$uniqid.'_'.$UPC;
	$drawing = new BCGDrawing($path.'.png', $color_white);
	$drawing->setBarcode($code);
	$drawing->draw();

	// Header that says it is an image (remove it if you save the barcode to a file)
	//header('Content-Type: image/png');

	// Draw (or save) the image into PNG format.
	$drawing->finish(BCGDrawing::IMG_FORMAT_PNG);
	unset($color_black);
	unset($color_white);
	return $path;
	} catch (Exception $e){
		error_msg($e->getMessage());
		send_error();
	}
}

//Generate barcode for input value
function gen_image($value)
{
global $error_messages;
$uniqid = uniqid();
	try{
	// Loading Font
	$font = new BCGFontFile('../Include/barcode/font/Arial.ttf', 18);

	// The arguments are R, G, B for color.
	$color_black = new BCGColor(0, 0, 0);
	$color_white = new BCGColor(255, 255, 255); 

	$code = new BCGcode39();
	$code->setScale(2); // Resolution
	$code->setThickness(30); // Thickness
	$code->setForegroundColor($color_black); // Color of bars
	$code->setBackgroundColor($color_white); // Color of spaces
	$code->setFont($font); // Font (or 0)
	//$code->parse('815324013899'); // Text
	$code->parse($value); // Text
	$code->ClearLabels();
	//$drawing = new BCGDrawing('sandeep.png', $color_white);
	$path = '../Images/code39_'.$uniqid.'_'.$value;
	$drawing = new BCGDrawing($path.'.png', $color_white);
	$drawing->setBarcode($code);
	$drawing->draw();

	// Header that says it is an image (remove it if you save the barcode to a file)
	//header('Content-Type: image/png');

	// Draw (or save) the image into PNG format.
	$drawing->finish(BCGDrawing::IMG_FORMAT_PNG);
	unset($color_black);
	unset($color_white);
	return $path;
	} catch (Exception $e){
		error_msg($e->getMessage());
		send_error();
	}
}

//Generate barcode for input value
function gen_imageFedex($value)
{
	global $error_messages;
        $uniqid = uniqid();
	//$numOfDigits = preg_match_all( "/[0-9]/", $value );
		try{
		// Loading Font
		$font = new BCGFontFile('../Include/barcode/font/Arial.ttf', 18);

		// The arguments are R, G, B for color.
		$color_black = new BCGColor(0, 0, 0);
		$color_white = new BCGColor(255, 255, 255); 

		$code = new BCGcode39();
		$code->setScale(2); // Resolution
		$code->setThickness(30); // Thickness
		$code->setForegroundColor($color_black); // Color of bars
		$code->setBackgroundColor($color_white); // Color of spaces
		$code->setFont($font); // Font (or 0)
		//$code->parse('815324013899'); // Text
		$code->parse($value); // Text
		$code->ClearLabels();
		//$drawing = new BCGDrawing('sandeep.png', $color_white);
		$path = '../Images/code39fedex_'.$uniqid.'_'.$value;
		$drawing = new BCGDrawing($path.'.png', $color_white);
		$drawing->setBarcode($code);
		$drawing->draw();

		// Header that says it is an image (remove it if you save the barcode to a file)
		//header('Content-Type: image/png');

		// Draw (or save) the image into PNG format.
		$drawing->finish(BCGDrawing::IMG_FORMAT_PNG);
		unset($color_black);
		unset($color_white);
		return $path;
		} catch (Exception $e){
			error_msg($e->getMessage());
			send_error();
		}
}

//Generate barcode for input value Code128
function gen_imageCode128wayfair($value)
{
	global $error_messages;
	$uniqid = uniqid();
	//$value = '537847';
	
	//$numOfDigits = preg_match_all( "/[0-9]/", $value );
		try{
		$path = '../Images/code128wayfair_'.$uniqid.'_'.$value;
		if(!findFile($path))
		{
			// Loading Font
			$font = new BCGFontFile('../Include/barcode/font/Arial.ttf', 18);

			// The arguments are R, G, B for color.
			$color_black = new BCGColor(0, 0, 0);
			$color_white = new BCGColor(255, 255, 255); 

			$code = new BCGcode128();
			$code->setScale(2); // Resolution
			$code->setThickness(20); // Thickness
			$code->setForegroundColor($color_black); // Color of bars
			$code->setBackgroundColor($color_white); // Color of spaces
			$code->setFont($font); // Font (or 0)
			$code->setStart(NULL);
			$code->setTilde(true);
			$code->parse($value); // Text
			$code->ClearLabels();
			//$drawing = new BCGDrawing('sandeep.png', $color_white);
			$drawing = new BCGDrawing($path.'.png', $color_white);
			$drawing->setBarcode($code);
			$drawing->draw();

			// Header that says it is an image (remove it if you save the barcode to a file)
			//header('Content-Type: image/png');

			// Draw (or save) the image into PNG format.
			$drawing->finish(BCGDrawing::IMG_FORMAT_PNG);
			unset($color_black);
			unset($color_white);
		}
		
		return $path;
		} catch (Exception $e){
			error_msg($e->getMessage());
			send_error();
		}
}

//Generate barcode for input value Code128
function gen_imageCode128($value)
{
	global $error_messages;
        $uniqid = uniqid();
	//$numOfDigits = preg_match_all( "/[0-9]/", $value );
		try{
		$path = '../Images/code128_'.$uniqid.'_'.$value;
		if(!findFile($path))
		{
			// Loading Font
			$font = new BCGFontFile('../Include/barcode/font/Arial.ttf', 18);

			// The arguments are R, G, B for color.
			$color_black = new BCGColor(0, 0, 0);
			$color_white = new BCGColor(255, 255, 255); 

			$code = new BCGcode128();
			$code->setScale(3); // Resolution
			$code->setThickness(60); // Thickness
			$code->setForegroundColor($color_black); // Color of bars
			$code->setBackgroundColor($color_white); // Color of spaces
			$code->setFont($font); // Font (or 0)
			$code->setStart(NULL);
			$code->setTilde(true);
			$code->parse($value); // Text
			$code->ClearLabels();
			//$drawing = new BCGDrawing('sandeep.png', $color_white);
			$drawing = new BCGDrawing($path.'.png', $color_white);
			$drawing->setBarcode($code);
			$drawing->draw();

			// Header that says it is an image (remove it if you save the barcode to a file)
			//header('Content-Type: image/png');

			// Draw (or save) the image into PNG format.
			$drawing->finish(BCGDrawing::IMG_FORMAT_PNG);
			unset($color_black);
			unset($color_white);
		}
		
		return $path;
		} catch (Exception $e){
			error_msg($e->getMessage());
			send_error();
		}
}

//Generate barcode for input value i25
function gen_imagei25($value)
{
	global $error_messages;
        $uniqid = uniqid();
	$numOfDigits = preg_match_all( "/[0-9]/", $value );
		try{
		// Loading Font
		$font = new BCGFontFile('../Include/barcode/font/Arial.ttf', 18);

		// The arguments are R, G, B for color.
		$color_black = new BCGColor(0, 0, 0);
		$color_white = new BCGColor(255, 255, 255); 

		$code = new BCGi25();
		$code->setScale(3); // Resolution
		$code->setThickness(30); // Thickness
		$code->setForegroundColor($color_black); // Color of bars
		$code->setBackgroundColor($color_white); // Color of spaces
		$code->setFont($font); // Font (or 0)
		if ($numOfDigits == 9)
		{
			$code->setChecksum(true);
		} else if ($numOfDigits == 10)
		{
			$code->setChecksum(false);
		} else 
		{
			error_msg('Can not generate i25 barcode. Value should be 9 or 10 digits long.');
			send_error();
		}
		//$code->setStart(NULL);
		//$code->setTilde(true);
		$code->parse($value); // Text
		//$drawing = new BCGDrawing('sandeep.png', $color_white);
		$path = '../Images/codei25_'.$uniqid.'_'.$value;
		$drawing = new BCGDrawing($path.'.png', $color_white);
		$drawing->setBarcode($code);
		$drawing->draw();

		// Header that says it is an image (remove it if you save the barcode to a file)
		//header('Content-Type: image/png');

		// Draw (or save) the image into PNG format.
		$drawing->finish(BCGDrawing::IMG_FORMAT_PNG);
		unset($color_black);
		unset($color_white);
		return $path;
		} catch (Exception $e){
			error_msg($e->getMessage());
			send_error();
		}
}

//Generate barcode for input value BCGgs1128
function gen_imageBCGgs1128($value)
{
	global $error_messages;
        $uniqid = uniqid();
	$numOfDigits = preg_match_all( "/[0-9]/", $value );
		try{
		// Loading Font
		$font = new BCGFontFile('../Include/barcode/font/Arial.ttf', 18);

		// The arguments are R, G, B for color.
		$color_black = new BCGColor(0, 0, 0);
		$color_white = new BCGColor(255, 255, 255); 

		$code = new BCGgs1128();
		$code->setScale(2); // Resolution
		$code->setThickness(60); // Thickness
		$code->setForegroundColor($color_black); // Color of bars
		$code->setBackgroundColor($color_white); // Color of spaces
		$code->setFont($font); // Font (or 0)
		$code->setStrictMode(true);
		//$code->setStart(NULL);
		//$code->setTilde(true);
		$code->parse($value); // Text
		//$code->ClearLabels();
		//$drawing = new BCGDrawing('sandeep.png', $color_white);
		$path = '../Images/codegs1128_'.$uniqid.'_'.$value;
		$drawing = new BCGDrawing($path.'.png', $color_white);
		$drawing->setBarcode($code);
		$drawing->draw();

		// Header that says it is an image (remove it if you save the barcode to a file)
		//header('Content-Type: image/png');

		// Draw (or save) the image into PNG format.
		$drawing->finish(BCGDrawing::IMG_FORMAT_PNG);
		unset($color_black);
		unset($color_white);
		return $path;
		} catch (Exception $e){
			error_msg($e->getMessage());
			send_error();
		}
}

//Generate barcode for input value UPC_A
function gen_imageUPCA($value)
{
	global $error_messages;
        $uniqid = uniqid();
	$numOfDigits = preg_match_all( "/[0-9]/", $value );
		try{
		// Loading Font
		$font = new BCGFontFile('../Include/barcode/font/Arial.ttf', 18);

		// The arguments are R, G, B for color.
		$color_black = new BCGColor(0, 0, 0);
		$color_white = new BCGColor(255, 255, 255); 

		$code = new BCGupca();
		$code->setScale(2); // Resolution
		$code->setThickness(30); // Thickness
		$code->setForegroundColor($color_black); // Color of bars
		$code->setBackgroundColor($color_white); // Color of spaces
		$code->setFont($font); // Font (or 0)
		$code->parse($value); // Text
		$path = '../Images/codeupca_'.$uniqid.'_'.$value;
		$drawing = new BCGDrawing($path.'.png', $color_white);
		$drawing->setBarcode($code);
		$drawing->draw();

		// Header that says it is an image (remove it if you save the barcode to a file)
		//header('Content-Type: image/png');

		// Draw (or save) the image into PNG format.
		$drawing->finish(BCGDrawing::IMG_FORMAT_PNG);
		unset($color_black);
		unset($color_white);
		return $path;
		} catch (Exception $e){
			error_msg($e->getMessage());
			send_error();
		}
}

//Generate barcode for input value UPC_A scale 4
function gen_imageUPCA_scale($value)
{
	global $error_messages;
        $uniqid = uniqid();
	$numOfDigits = preg_match_all( "/[0-9]/", $value );
		try{
		$path = '../Images/codeupcascale_'.$uniqid.'_'.$value;
		if(!findFile($path))
		{
			// Loading Font
			$font = new BCGFontFile('../Include/barcode/font/Arial.ttf', 18);

			// The arguments are R, G, B for color.
			$color_black = new BCGColor(0, 0, 0);
			$color_white = new BCGColor(255, 255, 255); 

			$code = new BCGupca();
			$code->setScale(3); // Resolution
			$code->setThickness(50); // Thickness
			$code->setForegroundColor($color_black); // Color of bars
			$code->setBackgroundColor($color_white); // Color of spaces
			$code->setFont($font); // Font (or 0)
			$code->parse($value); // Text
			$drawing = new BCGDrawing($path.'.png', $color_white);
			$drawing->setBarcode($code);
			$drawing->draw();

			// Header that says it is an image (remove it if you save the barcode to a file)
			//header('Content-Type: image/png');

			// Draw (or save) the image into PNG format.
			$drawing->finish(BCGDrawing::IMG_FORMAT_PNG);
			unset($color_black);
			unset($color_white);
		}
		return $path;
		} catch (Exception $e){
			error_msg($e->getMessage());
			send_error();
		}
}
?>