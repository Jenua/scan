<?php
require('functionsDB.php');
require('functionsExtr.php');
$path = $_SERVER['DOCUMENT_ROOT'];
require_once($path.'/db_connect/connect.php');
date_default_timezone_set('America/New_York');

$connect = Database::getInstance()->dbc;

//Start if have PO number
if ((isset($_REQUEST['value']) && !empty($_REQUEST['value'])) && (isset($_REQUEST['pro']) && !empty($_REQUEST['pro'])) && (isset($_REQUEST['carrier']) && !empty($_REQUEST['carrier'])))
{
	updateProBol($connect, $_REQUEST['value'], $_REQUEST['pro'], $_REQUEST['carrier']);
}

$connect = null;

//write PRO# and BOL# to ShipTrackWithLabel
function updateProBol($connect, $Po, $pro, $carrier)
{
	$extrCarrier = extrCarrier($carrier);
	$CARRIERNAME_PRO = $extrCarrier.'_'.$pro;
	try{
		$query = "UPDATE [dbo].[".SHIPTRACKWITHLABEL_TABLE."] SET [CARRIERNAME_PRO] = '".$CARRIERNAME_PRO."' WHERE [ID] = '".$Po."'";
		$result = execQuery($connect, $query);
		
		if ($result)
		{
			echo 'CARRIERNAME_PRO IS OK';
		} else echo "<br>This PRO# is already in use.<br>";
	}catch (Exception $e) {
		print_r("Can not save PRO#, to database!");
	}
}
?>