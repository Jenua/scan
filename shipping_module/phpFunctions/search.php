<?php
$path = $_SERVER['DOCUMENT_ROOT'];
require_once($path.'/db_connect/connect.php');

	$returnValue = false;
		
	$PO = prepareString($_REQUEST['PO']);
	$Customer = prepareString($_REQUEST['Customer']);
	$Phone = prepareString($_REQUEST['Phone']);
	$Address = prepareString($_REQUEST['Address']);
	$Zip = prepareString($_REQUEST['Zip']);
	$Dealer = prepareString($_REQUEST['Dealer']);
	$Carrier = prepareString($_REQUEST['Carrier']);
	$Tracking = prepareString($_REQUEST['Tracking']);
	$sm_invoice = prepareString($_REQUEST['sm_invoice']);
	$qb_invoice = prepareString($_REQUEST['qb_invoice']);
	$Memo = prepareString($_REQUEST['Memo']);	
	
	$d1 = prepareString($_REQUEST['Date1']);
	$d2 = prepareString($_REQUEST['Date2']);

	if(!is_meaningless($d1) && !is_meaningless($d2)) {
		$dt1 = new DateTime( $d1 );
		$dt2 = new DateTime( $d2 );
		if( $dt1 <= $dt2 ){
			$Date1 = $d1;
			$Date2 = $d2;
		} else {
			$Date1 = $d2;
			$Date2 = $d1;
		}
	} else if(!is_meaningless($d1) && is_meaningless($d2)) {
		$Date1 = $d1;
		$Date2 = $d1;
	} else if(is_meaningless($d1) && !is_meaningless($d2)) {
		$Date1 = $d2;
		$Date2 = $d2;
	} else {
		$Date1 = '';
		$Date2 = '';
	}

	$conn = Database::getInstance()->dbc;
	$query = generateQuery($PO, $Customer, $Phone, $Address, $Zip, $Dealer, $Carrier, $Tracking, $Memo, $Date1, $Date2, $qb_invoice, $sm_invoice);
	
        $res = getOrders($conn, $query);
	
	if(isset($res) && !empty($res))
	{
		$returnValue = $res;
	}
	$returnValue = json_encode($returnValue);
	print_r($returnValue);
	$conn = null;
	
function prepareString($term)
{
	$term = trim(strip_tags($term));
	$term = str_replace(array("'", "`"), "''", $term);
	return $term;
}

function is_meaningless($value)
{
	if(isset($value) && !empty($value) && trim($value) != '')
	{
		return false;
	} else
	{
		return true;
	}
}

function generateQuery($PO, $Customer, $Phone, $Address, $Zip, $Dealer, $Carrier, $Tracking, $Memo, $Date1, $Date2, $qb_invoice, $sm_invoice)
{
	$where1 = false;
	$where2 = false;
	$where3 = false;
	$where4 = false;
	$comma = false;
	$query = "SELECT * FROM
(
SELECT
	[PONumber],
	[ShipAddress_Addr1],
	[FOB],
	[ShipAddress_Addr2],
	[ShipAddress_PostalCode],
	[CustomerRef_FullName],
	[CARRIERNAME],
	[PRO#],
	[SM Invoice#],
	[Tracking#],
	[QB Invoice#],
	[Memo],
	[RefNumber],
	[TimeCreated],
	[table_name]
	,DENSE_RANK() OVER(PARTITION BY [PONumber],
		[ShipAddress_Addr1],
		[FOB],
		[ShipAddress_Addr2],
		[ShipAddress_PostalCode],
		[CustomerRef_FullName],
		[CARRIERNAME],
		[PRO#],
		[SM Invoice#],
		[Tracking#],
		[QB Invoice#],
		[Memo],
		[RefNumber] ORDER BY [table_name]) AS R
	FROM
	(
	SELECT TOP(100) * FROM (	
	SELECT 
	[shiptrack].[PONumber]
	,[shiptrack].[ShipAddress_Addr1]
	,[shiptrack].[FOB]
	,[shiptrack].[ShipAddress_Addr2]
	,[shiptrack].[ShipAddress_PostalCode]
	,[shiptrack].[CustomerRef_FullName]
	,[ShipTrackWithLabel].[CARRIERNAME]
	,[ShipTrackWithLabel].[PRO] as [PRO#]
	,[ShipTrackWithLabel].[BOL] as [SM Invoice#]
	,[invoice].[Other] as [Tracking#]
	,[invoice].[RefNumber] as [QB Invoice#]
	,[shiptrack].[Memo]
	,[ShipTrackWithLabel].[ID] as [RefNumber]
	,[shiptrack].[TimeCreated]
        ,'2' as [table_name]
	FROM [".DATABASE."].[dbo].[shiptrack]
	LEFT JOIN [".DATABASE."].[dbo].[ShipTrackWithLabel] ON [shiptrack].[RefNumber] = [ShipTrackWithLabel].[ID]
	LEFT JOIN [".DATABASE31."].[dbo].[invoice] ON [shiptrack].[PONumber] = [invoice].[PONumber]	";
	if(!is_meaningless($PO) && !$where1)
	{
		$query.="WHERE [shiptrack].[PONumber] LIKE '%".$PO."%'";
		$where1 = true;
	} else if(!is_meaningless($PO) && $where1)
	{
		$query.="AND [shiptrack].[PONumber] LIKE '%".$PO."%'";
	}
	
	if(!is_meaningless($Customer) && !$where1)
	{
		$query.="WHERE [shiptrack].[ShipAddress_Addr1] LIKE '%".$Customer."%'";
		$where1 = true;
	} else if(!is_meaningless($Customer) && $where1)
	{
		$query.="AND [shiptrack].[ShipAddress_Addr1] LIKE '%".$Customer."%'";
	}
	
	if(!is_meaningless($Phone) && !$where1)
	{
		$query.="WHERE [shiptrack].[FOB] LIKE '%".$Phone."%'";
		$where1 = true;
	} else if(!is_meaningless($Phone) && $where1)
	{
		$query.="AND [shiptrack].[FOB] LIKE '%".$Phone."%'";
	}
	
	if(!is_meaningless($Address) && !$where1)
	{
		$query.="WHERE ([shiptrack].[ShipAddress_Addr2] LIKE '%".$Address."%' OR [shiptrack].[ShipAddress_Addr3] LIKE '%".$Address."%')";
		$where1 = true;
	} else if(!is_meaningless($Address) && $where1)
	{
		$query.="AND ([shiptrack].[ShipAddress_Addr2] LIKE '%".$Address."%' OR [shiptrack].[ShipAddress_Addr3] LIKE '%".$Address."%')";
	}
	
	if(!is_meaningless($Zip) && !$where1)
	{
		$query.="WHERE [shiptrack].[ShipAddress_PostalCode] LIKE '%".$Zip."%'";
		$where1 = true;
	} else if(!is_meaningless($Zip) && $where1)
	{
		$query.="AND [shiptrack].[ShipAddress_PostalCode] LIKE '%".$Zip."%'";
	}
	
	if(!is_meaningless($Dealer) && !$where1)
	{
		$query.="WHERE [shiptrack].[CustomerRef_FullName] LIKE '%".$Dealer."%'";
		$where1 = true;
	} else if(!is_meaningless($Dealer) && $where1)
	{
		$query.="AND [shiptrack].[CustomerRef_FullName] LIKE '%".$Dealer."%'";
	}
	
	if(!is_meaningless($Carrier) && !$where1)
	{
		$query.="WHERE ([ShipTrackWithLabel].[CARRIERNAME] LIKE '%".$Carrier."%' OR [shiptrack].[ShipMethodRef_FullName] LIKE '%".$Carrier."%')";
		$where1 = true;
	} else if(!is_meaningless($Carrier) && $where1)
	{
		$query.="AND ([ShipTrackWithLabel].[CARRIERNAME] LIKE '%".$Carrier."%' OR [shiptrack].[ShipMethodRef_FullName] LIKE '%".$Carrier."%')";
	} 
	
	if(!is_meaningless($Tracking) && !$where1)
	{
		$query.="WHERE [ShipTrackWithLabel].[PRO] LIKE '%".$Tracking."%'";
		$where1 = true;
	} else if(!is_meaningless($Tracking) && $where1)
	{
		$query.="AND [ShipTrackWithLabel].[PRO] LIKE '%".$Tracking."%'";
	}
	
	if(!is_meaningless($sm_invoice) && !$where1)
	{
		$query.="WHERE [ShipTrackWithLabel].[BOL] LIKE '%".$sm_invoice."%'";
		$where1 = true;
	} else if(!is_meaningless($sm_invoice) && $where1)
	{
		$query.="AND [ShipTrackWithLabel].[BOL] LIKE '%".$sm_invoice."%'";
	}
	
	if(!is_meaningless($qb_invoice) && !$where1)
	{
		$query.="WHERE [invoice].[RefNumber] LIKE '%".$qb_invoice."%'";
		$where1 = true;
	} else if(!is_meaningless($qb_invoice) && $where1)
	{
		$query.="AND [invoice].[RefNumber] LIKE '%".$qb_invoice."%'";
	}
	
	if(!is_meaningless($Memo) && !$where1)
	{
		$query.="WHERE [shiptrack].[Memo] LIKE '%".$Memo."%'";
		$where1 = true;
	} else if(!is_meaningless($Memo) && $where1)
	{
		$query.="AND [shiptrack].[Memo] LIKE '%".$Memo."%'";
	}
        
	if(!is_meaningless($Date1) && !is_meaningless($Date2) && !$where1)
	{
		$query.="WHERE CONVERT(DATE, [shiptrack].[TimeCreated]) BETWEEN '".$Date1."' AND '".$Date2."'";
		$where1 = true;
	} else if(!is_meaningless($Date1) && !is_meaningless($Date2) && $where1)
	{
		$query.="AND CONVERT(DATE, [shiptrack].[TimeCreated]) BETWEEN '".$Date1."' AND '".$Date2."'";
	}
	
	$query.=" UNION ALL
	SELECT 
	[invoice].[PONumber]
	,[invoice].[ShipAddress_Addr1]
	,[invoice].[FOB]
	,[invoice].[ShipAddress_Addr2]
	,[invoice].[ShipAddress_PostalCode]
	,[invoice].[CustomerRef_FullName]
	,[ShipTrackWithLabel].[CARRIERNAME]
	,[ShipTrackWithLabel].[PRO] as [PRO#]
	,[ShipTrackWithLabel].[BOL] as [SM Invoice#]
	,[invoice].[Other] as [Tracking#]
	,[invoice].[RefNumber] as [QB Invoice#]
	,[invoice].[Memo]
	,[ShipTrackWithLabel].[ID] as [RefNumber]
	,[invoice].[TimeCreated]
        ,'0' as [table_name]
	FROM [".DATABASE31."].[dbo].[invoice]
	LEFT JOIN [".DATABASE."].[dbo].[ShipTrackWithLabel] ON [invoice].[PONumber] = [ShipTrackWithLabel].[PONUMBER] ";
	if(!is_meaningless($PO) && !$where2)
	{
		$query.="WHERE [invoice].[PONumber] LIKE '%".$PO."%'";
		$where2 = true;
	} else if(!is_meaningless($PO) && $where2)
	{
		$query.="AND [invoice].[PONumber] LIKE '%".$PO."%'";
	}
	
	if(!is_meaningless($Customer) && !$where2)
	{
		$query.="WHERE [invoice].[ShipAddress_Addr1] LIKE '%".$Customer."%'";
		$where2 = true;
	} else if(!is_meaningless($Customer) && $where2)
	{
		$query.="AND [invoice].[ShipAddress_Addr1] LIKE '%".$Customer."%'";
	}
	
	if(!is_meaningless($Phone) && !$where2)
	{
		$query.="WHERE [invoice].[FOB] LIKE '%".$Phone."%'";
		$where2 = true;
	} else if(!is_meaningless($Phone) && $where2)
	{
		$query.="AND [invoice].[FOB] LIKE '%".$Phone."%'";
	}
	
	if(!is_meaningless($Address) && !$where2)
	{
		$query.="WHERE ([invoice].[ShipAddress_Addr2] LIKE '%".$Address."%' OR [invoice].[ShipAddress_Addr3] LIKE '%".$Address."%')";
		$where2 = true;
	} else if(!is_meaningless($Address) && $where2)
	{
		$query.="AND ([invoice].[ShipAddress_Addr2] LIKE '%".$Address."%' OR [invoice].[ShipAddress_Addr3] LIKE '%".$Address."%')";
	}
	
	if(!is_meaningless($Zip) && !$where2)
	{
		$query.="WHERE [invoice].[ShipAddress_PostalCode] LIKE '%".$Zip."%'";
		$where2 = true;
	} else if(!is_meaningless($Zip) && $where2)
	{
		$query.="AND [invoice].[ShipAddress_PostalCode] LIKE '%".$Zip."%'";
	}
	
	if(!is_meaningless($Dealer) && !$where2)
	{
		$query.="WHERE [invoice].[CustomerRef_FullName] LIKE '%".$Dealer."%'";
		$where2 = true;
	} else if(!is_meaningless($Dealer) && $where2)
	{
		$query.="AND [invoice].[CustomerRef_FullName] LIKE '%".$Dealer."%'";
	}
	
	if(!is_meaningless($Carrier) && !$where2)
	{
		$query.="WHERE ([ShipTrackWithLabel].[CARRIERNAME] LIKE '%".$Carrier."%' OR [invoice].[ShipMethodRef_FullName] LIKE '%".$Carrier."%')";
		$where2 = true;
	} else if(!is_meaningless($Carrier) && $where2)
	{
		$query.="AND ([ShipTrackWithLabel].[CARRIERNAME] LIKE '%".$Carrier."%' OR [invoice].[ShipMethodRef_FullName] LIKE '%".$Carrier."%')";
	} 
	
	if(!is_meaningless($Tracking) && !$where2)
	{
		$query.="WHERE ([ShipTrackWithLabel].[PRO] LIKE '%".$Tracking."%' OR [invoice].[Other] LIKE '%".$Tracking."%')";
		$where2 = true;
	} else if(!is_meaningless($Tracking) && $where2)
	{
		$query.="AND ([ShipTrackWithLabel].[PRO] LIKE '%".$Tracking."%' OR [invoice].[Other] LIKE '%".$Tracking."%')";
	}
	
	if(!is_meaningless($sm_invoice) && !$where2)
	{
		$query.="WHERE [ShipTrackWithLabel].[BOL] LIKE '%".$sm_invoice."%'";
		$where2 = true;
	} else if(!is_meaningless($sm_invoice) && $where2)
	{
		$query.="AND [ShipTrackWithLabel].[BOL] LIKE '%".$sm_invoice."%'";
	}
	
	if(!is_meaningless($qb_invoice) && !$where2)
	{
		$query.="WHERE [invoice].[RefNumber] LIKE '%".$qb_invoice."%'";
		$where2 = true;
	} else if(!is_meaningless($qb_invoice) && $where2)
	{
		$query.="AND [invoice].[RefNumber] LIKE '%".$qb_invoice."%'";
	}
	
	if(!is_meaningless($Memo) && !$where2)
	{
		$query.="WHERE ([invoice].[Memo] LIKE '%".$Memo."%')";
		$where2 = true;
	} else if(!is_meaningless($Memo) && $where2)
	{
		$query.="AND ([invoice].[Memo] LIKE '%".$Memo."%')";
	}
        
	if(!is_meaningless($Date1) && !is_meaningless($Date2) && !$where2)
	{
		$query.="WHERE CONVERT(DATE, [invoice].[TimeCreated]) BETWEEN '".$Date1."' AND '".$Date2."'";
		$where1 = true;
	} else if(!is_meaningless($Date1) && !is_meaningless($Date2) && $where2)
	{
		$query.="AND CONVERT(DATE, [invoice].[TimeCreated]) BETWEEN '".$Date1."' AND '".$Date2."'";
	}
	
	$query.=" UNION ALL
	SELECT 
	[manually_shiptrack].[PONumber]
	,[manually_shiptrack].[ShipAddress_Addr1]
	,[manually_shiptrack].[FOB]
	,[manually_shiptrack].[ShipAddress_Addr2]
	,[manually_shiptrack].[ShipAddress_PostalCode]
	,[manually_shiptrack].[CustomerRef_FullName]
	,[ShipTrackWithLabel].[CARRIERNAME]
	,[ShipTrackWithLabel].[PRO] as [PRO#]
	,[ShipTrackWithLabel].[BOL] as [SM Invoice#]
	,[invoice].[Other] as [Tracking#]
	,[invoice].[RefNumber] as [QB Invoice#]
	,[manually_shiptrack].[Memo]
	,[ShipTrackWithLabel].[ID] as [RefNumber]
	,[manually_shiptrack].[TimeCreated]
        ,'3' as [table_name]
	FROM [".DATABASE."].[dbo].[manually_shiptrack]
	LEFT JOIN [".DATABASE."].[dbo].[ShipTrackWithLabel] ON [manually_shiptrack].[PONumber] = [ShipTrackWithLabel].[PONUMBER]
	LEFT JOIN [".DATABASE31."].[dbo].[invoice] ON [manually_shiptrack].[PONumber] = [invoice].[PONumber]	";
	if(!is_meaningless($PO) && !$where3)
	{
		$query.="WHERE [manually_shiptrack].[PONumber] LIKE '%".$PO."%'";
		$where3 = true;
	} else if(!is_meaningless($PO) && $where3)
	{
		$query.="AND [manually_shiptrack].[PONumber] LIKE '%".$PO."%'";
	}
	
	if(!is_meaningless($Customer) && !$where3)
	{
		$query.="WHERE [manually_shiptrack].[ShipAddress_Addr1] LIKE '%".$Customer."%'";
		$where3 = true;
	} else if(!is_meaningless($Customer) && $where3)
	{
		$query.="AND [manually_shiptrack].[ShipAddress_Addr1] LIKE '%".$Customer."%'";
	}
	
	if(!is_meaningless($Phone) && !$where3)
	{
		$query.="WHERE [manually_shiptrack].[FOB] LIKE '%".$Phone."%'";
		$where3 = true;
	} else if(!is_meaningless($Phone) && $where3)
	{
		$query.="AND [manually_shiptrack].[FOB] LIKE '%".$Phone."%'";
	}
	
	if(!is_meaningless($Address) && !$where3)
	{
		$query.="WHERE ([manually_shiptrack].[ShipAddress_Addr2] LIKE '%".$Address."%' OR [manually_shiptrack].[ShipAddress_Addr3] LIKE '%".$Address."%')";
		$where3 = true;
	} else if(!is_meaningless($Address) && $where3)
	{
		$query.="AND ([manually_shiptrack].[ShipAddress_Addr2] LIKE '%".$Address."%' OR [manually_shiptrack].[ShipAddress_Addr3] LIKE '%".$Address."%')";
	}
	
	if(!is_meaningless($Zip) && !$where3)
	{
		$query.="WHERE [manually_shiptrack].[ShipAddress_PostalCode] LIKE '%".$Zip."%'";
		$where3 = true;
	} else if(!is_meaningless($Zip) && $where3)
	{
		$query.="AND [manually_shiptrack].[ShipAddress_PostalCode] LIKE '%".$Zip."%'";
	}
	
	if(!is_meaningless($Dealer) && !$where3)
	{
		$query.="WHERE [manually_shiptrack].[CustomerRef_FullName] LIKE '%".$Dealer."%'";
		$where3 = true;
	} else if(!is_meaningless($Dealer) && $where3)
	{
		$query.="AND [manually_shiptrack].[CustomerRef_FullName] LIKE '%".$Dealer."%'";
	}
	
	if(!is_meaningless($Carrier) && !$where3)
	{
		$query.="WHERE ([ShipTrackWithLabel].[CARRIERNAME] LIKE '%".$Carrier."%' OR [manually_shiptrack].[ShipMethodRef_FullName] LIKE '%".$Carrier."%')";
		$where3 = true;
	} else if(!is_meaningless($Carrier) && $where3)
	{
		$query.="AND ([ShipTrackWithLabel].[CARRIERNAME] LIKE '%".$Carrier."%' OR [manually_shiptrack].[ShipMethodRef_FullName] LIKE '%".$Carrier."%')";
	} 
	
	if(!is_meaningless($Tracking) && !$where3)
	{
		$query.="WHERE [ShipTrackWithLabel].[PRO] LIKE '%".$Tracking."%'";
		$where3 = true;
	} else if(!is_meaningless($Tracking) && $where3)
	{
		$query.="AND [ShipTrackWithLabel].[PRO] LIKE '%".$Tracking."%'";
	}
	
	if(!is_meaningless($sm_invoice) && !$where3)
	{
		$query.="WHERE [ShipTrackWithLabel].[BOL] LIKE '%".$sm_invoice."%'";
		$where3 = true;
	} else if(!is_meaningless($sm_invoice) && $where3)
	{
		$query.="AND [ShipTrackWithLabel].[BOL] LIKE '%".$sm_invoice."%'";
	}
	
	if(!is_meaningless($qb_invoice) && !$where3)
	{
		$query.="WHERE [invoice].[RefNumber] LIKE '%".$qb_invoice."%'";
		$where3 = true;
	} else if(!is_meaningless($qb_invoice) && $where3)
	{
		$query.="AND [invoice].[RefNumber] LIKE '%".$qb_invoice."%'";
	}
	
	if(!is_meaningless($Memo) && !$where3)
	{
		$query.="WHERE [manually_shiptrack].[Memo] LIKE '%".$Memo."%'";
		$where3 = true;
	} else if(!is_meaningless($Memo) && $where3)
	{
		$query.="AND [manually_shiptrack].[Memo] LIKE '%".$Memo."%'";
	}
        
	if(!is_meaningless($Date1) && !is_meaningless($Date2) && !$where3)
	{
		$query.="WHERE CONVERT(DATE, [manually_shiptrack].[TimeCreated]) BETWEEN '".$Date1."' AND '".$Date2."'";
		$where1 = true;
	} else if(!is_meaningless($Date1) && !is_meaningless($Date2) && $where3)
	{
		$query.="AND CONVERT(DATE, [manually_shiptrack].[TimeCreated]) BETWEEN '".$Date1."' AND '".$Date2."'";
	}

	$query.=" UNION ALL
	SELECT 
	[salesorder].[PONumber]
	,[salesorder].[ShipAddress_Addr1]
	,[salesorder].[FOB]
	,[salesorder].[ShipAddress_Addr2]
	,[salesorder].[ShipAddress_PostalCode]
	,[salesorder].[CustomerRef_FullName]
	,[ShipTrackWithLabel].[CARRIERNAME]
	,[ShipTrackWithLabel].[PRO] as [PRO#]
	,[ShipTrackWithLabel].[BOL] as [SM Invoice#]
	,[invoice].[Other] as [Tracking#]
	,[invoice].[RefNumber] as [QB Invoice#]
	,[salesorder].[Memo]
	,[ShipTrackWithLabel].[ID] as [RefNumber]
	,[salesorder].[TimeCreated]
        ,'1' as [table_name]
	FROM [".DATABASE31."].[dbo].[salesorder]
	LEFT JOIN [".DATABASE."].[dbo].[ShipTrackWithLabel] ON [salesorder].[PONumber] = [ShipTrackWithLabel].[PONUMBER]
	LEFT JOIN [".DATABASE31."].[dbo].[invoice] ON [salesorder].[PONumber] = [invoice].[PONumber] ";
	if(!is_meaningless($PO) && !$where4)
	{
		$query.="WHERE [salesorder].[PONumber] LIKE '%".$PO."%'";
		$where4 = true;
	} else if(!is_meaningless($PO) && $where4)
	{
		$query.="AND [salesorder].[PONumber] LIKE '%".$PO."%'";
	}
	
	if(!is_meaningless($Customer) && !$where4)
	{
		$query.="WHERE [salesorder].[ShipAddress_Addr1] LIKE '%".$Customer."%'";
		$where4 = true;
	} else if(!is_meaningless($Customer) && $where4)
	{
		$query.="AND [salesorder].[ShipAddress_Addr1] LIKE '%".$Customer."%'";
	}
	
	if(!is_meaningless($Phone) && !$where4)
	{
		$query.="WHERE [salesorder].[FOB] LIKE '%".$Phone."%'";
		$where4 = true;
	} else if(!is_meaningless($Phone) && $where4)
	{
		$query.="AND [salesorder].[FOB] LIKE '%".$Phone."%'";
	}
	
	if(!is_meaningless($Address) && !$where4)
	{
		$query.="WHERE ([salesorder].[ShipAddress_Addr2] LIKE '%".$Address."%' OR [salesorder].[ShipAddress_Addr3] LIKE '%".$Address."%')";
		$where4 = true;
	} else if(!is_meaningless($Address) && $where4)
	{
		$query.="AND ([salesorder].[ShipAddress_Addr2] LIKE '%".$Address."%' OR [salesorder].[ShipAddress_Addr3] LIKE '%".$Address."%')";
	}
	
	if(!is_meaningless($Zip) && !$where4)
	{
		$query.="WHERE [salesorder].[ShipAddress_PostalCode] LIKE '%".$Zip."%'";
		$where4 = true;
	} else if(!is_meaningless($Zip) && $where4)
	{
		$query.="AND [salesorder].[ShipAddress_PostalCode] LIKE '%".$Zip."%'";
	}
	
	if(!is_meaningless($Dealer) && !$where4)
	{
		$query.="WHERE [salesorder].[CustomerRef_FullName] LIKE '%".$Dealer."%'";
		$where4 = true;
	} else if(!is_meaningless($Dealer) && $where4)
	{
		$query.="AND [salesorder].[CustomerRef_FullName] LIKE '%".$Dealer."%'";
	}
	
	if(!is_meaningless($Carrier) && !$where4)
	{
		$query.="WHERE ([ShipTrackWithLabel].[CARRIERNAME] LIKE '%".$Carrier."%' OR [salesorder].[ShipMethodRef_FullName] LIKE '%".$Carrier."%')";
		$where4 = true;
	} else if(!is_meaningless($Carrier) && $where4)
	{
		$query.="AND ([ShipTrackWithLabel].[CARRIERNAME] LIKE '%".$Carrier."%' OR [salesorder].[ShipMethodRef_FullName] LIKE '%".$Carrier."%')";
	} 
	
	if(!is_meaningless($Tracking) && !$where4)
	{
		$query.="WHERE [ShipTrackWithLabel].[PRO] LIKE '%".$Tracking."%'";
		$where4 = true;
	} else if(!is_meaningless($Tracking) && $where4)
	{
		$query.="AND [ShipTrackWithLabel].[PRO] LIKE '%".$Tracking."%'";
	}
	
	if(!is_meaningless($sm_invoice) && !$where4)
	{
		$query.="WHERE [ShipTrackWithLabel].[BOL] LIKE '%".$sm_invoice."%'";
		$where4 = true;
	} else if(!is_meaningless($sm_invoice) && $where4)
	{
		$query.="AND [ShipTrackWithLabel].[BOL] LIKE '%".$sm_invoice."%'";
	}
	
	if(!is_meaningless($qb_invoice) && !$where4)
	{
		$query.="WHERE [invoice].[RefNumber] LIKE '%".$qb_invoice."%'";
		$where4 = true;
	} else if(!is_meaningless($qb_invoice) && $where4)
	{
		$query.="AND [invoice].[RefNumber] LIKE '%".$qb_invoice."%'";
	}
	
	if(!is_meaningless($Memo) && !$where4)
	{
		$query.="WHERE [salesorder].[Memo] LIKE '%".$Memo."%'";
		$where4 = true;
	} else if(!is_meaningless($Memo) && $where4)
	{
		$query.="AND [salesorder].[Memo] LIKE '%".$Memo."%'";
	}
        
	if(!is_meaningless($Date1) && !is_meaningless($Date2) && !$where4)
	{
		$query.="WHERE CONVERT(DATE, [salesorder].[TimeCreated]) BETWEEN '".$Date1."' AND '".$Date2."'";
		$where1 = true;
	} else if(!is_meaningless($Date1) && !is_meaningless($Date2) && $where4)
	{
		$query.="AND CONVERT(DATE, [salesorder].[TimeCreated]) BETWEEN '".$Date1."' AND '".$Date2."'";
	} 
	
	$query.=") A
	GROUP BY [PONumber], [ShipAddress_Addr1], [FOB], [ShipAddress_Addr2], [ShipAddress_PostalCode], [CustomerRef_FullName], [CARRIERNAME], [PRO#], [SM Invoice#], [Tracking#], [QB Invoice#], [Memo], [RefNumber], [TimeCreated], [table_name] 
        ) B
	) C
	WHERE R = 1 
        ";
	
	if(!is_meaningless($PO) && !$comma)
	{
		$query.="ORDER BY [PONumber]";
		$comma = true;
	} else if(!is_meaningless($PO) && $comma)
	{
		$query.=" ,[PONumber]";
	}
	
	if(!is_meaningless($Customer) && !$comma)
	{
		$query.="ORDER BY [ShipAddress_Addr1]";
		$comma = true;
	} else if(!is_meaningless($Customer) && $comma)
	{
		$query.=" ,[ShipAddress_Addr1]";
	}
	
	if(!is_meaningless($Phone) && !$comma)
	{
		$query.="ORDER BY [FOB]";
		$comma = true;
	} else if(!is_meaningless($Phone) && $comma)
	{
		$query.=" ,[FOB]";
	}
	
	if(!is_meaningless($Address) && !$comma)
	{
		$query.="ORDER BY [ShipAddress_Addr2]";
		$comma = true;
	} else if(!is_meaningless($Address) && $comma)
	{
		$query.=" ,[ShipAddress_Addr2]";
	}
	
	if(!is_meaningless($Zip) && !$comma)
	{
		$query.="ORDER BY [ShipAddress_PostalCode]";
		$comma = true;
	} else if(!is_meaningless($Zip) && $comma)
	{
		$query.=" ,[ShipAddress_PostalCode]";
	}
	
	if(!is_meaningless($Dealer) && !$comma)
	{
		$query.="ORDER BY [CustomerRef_FullName]";
		$comma = true;
	} else if(!is_meaningless($Dealer) && $comma)
	{
		$query.=" ,[CustomerRef_FullName]";
	}
	
	if(!is_meaningless($Carrier) && !$comma)
	{
		$query.="ORDER BY [CARRIERNAME]";
		$comma = true;
	} else if(!is_meaningless($Carrier) && $comma)
	{
		$query.=" ,[CARRIERNAME]";
	}
	
	if(!is_meaningless($Tracking) && !$comma)
	{
		$query.="ORDER BY [PRO#], [Tracking#]";
		$comma = true;
	} else if(!is_meaningless($Carrier) && $comma)
	{
		$query.=" ,[PRO#], [Tracking#]";
	}
	
	if(!is_meaningless($Memo) && !$comma)
	{
		$query.="ORDER BY [Memo]";
		$comma = true;
	} else if(!is_meaningless($Carrier) && $comma)
	{
		$query.=" ,[Memo]";
	}

	if(!is_meaningless($Date1) && !is_meaningless($Date2) && !$comma)
	{
		$query.="ORDER BY [TimeCreated]";
		$where1 = true;
	} else if(!is_meaningless($Date1) && !is_meaningless($Date2) && $comma)
	{
		$query.=" ,[TimeCreated]";
	}

	/*print_r('<pre>');
	print_r($query);
	print_r('</pre>');
	die();*/


	return $query;
}

function getOrders($conn, $query)
{
	//print_r($query);
	try{
		$result = $conn->prepare($query);
		$result->execute();
		$result = $result->fetchAll(PDO::FETCH_ASSOC); 
		return $result;
	}
	catch(PDOException $e)
	{
		return false;
	}
}
?>