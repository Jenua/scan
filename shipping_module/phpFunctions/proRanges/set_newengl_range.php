<?php

$path = $_SERVER['DOCUMENT_ROOT'];
require_once($path.'/db_connect/connect.php');
require_once($path.'/shipping_module/phpFunctions/proRanges/functions.php');

if (isset($_REQUEST['min']) && !empty($_REQUEST['min'])
		&& isset($_REQUEST['max']) && !empty($_REQUEST['max'])
	)
	{
		$min = trim($_REQUEST['min']);
		$max = trim($_REQUEST['max']);
		$conn = getConnection();
		
		$query = "UPDATE [dbo].[general_settings]
   SET [setting_value] = '".$min."'
  WHERE [app] = 'shipping_module' 
  AND [group_name] = 'pro_numbers_nemf'
  AND [type_name] = 'min'";
  
		$query1 = "UPDATE [dbo].[general_settings]
   SET [setting_value] = '".$max."'
  WHERE [app] = 'shipping_module' 
  AND [group_name] = 'pro_numbers_nemf'
  AND [type_name] = 'max'";
		
		beginTransaction($conn);
		$res = runQuery_empty_result($conn, $query);
		$res1 = runQuery_empty_result($conn, $query1);
		
		if ($res && $res1) 
		{
			$res2 = commitTransaction($conn);
			if ($res2) $returnValue = 'Range saved.'; else 
			{
				$returnValue = 'Can not save range!';
				rollbackTransaction($conn);
			}
		} else
		{
			rollbackTransaction($conn);
			$returnValue = 'Can not save range!';
		}
		print_r($returnValue);
		$conn = null;
	} else{
		print_r('Can not get values to save.');
	}
?>