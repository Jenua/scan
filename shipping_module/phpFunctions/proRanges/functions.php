<?php
function getConnection()
{
	$conn = Database::getInstance()->dbc;
	return $conn;
}

function beginTransaction($conn)
{
	try
	{
		$result = $conn->beginTransaction();
		return $result;
	}catch (Exception $e) {
		die("<br>".$e."<br>");
		return false;
	}
}

function commitTransaction($conn)
{
	try
	{
		$result = $conn->commit();
		return $result;
	}catch (Exception $e) {
		die("<br>".$e."<br>");
		return false;
	}
}

function rollbackTransaction($conn)
{
	try
	{
		$result = $conn->rollback();
		return $result;
	}catch (Exception $e) {
		die("<br>".$e."<br>");
		return false;
	}
}

function runQuery($conn, $query)
{
	try
	{
		//echo "<br>".$query."<br>";
		$stmt = $conn->prepare($query);
		$result = $stmt->execute();
		$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}catch (Exception $e) {
		die("<br>".$e."<br>");
		return false;
	}
}

function runQuery_empty_result($conn, $query)
{
	try
	{
		//echo "<br>".$query."<br>";
		$stmt = $conn->prepare($query);
		$result = $stmt->execute();
		return $result;
	}catch (Exception $e) {
		die("<br>".$e."<br>");
		return false;
	}
}
?>