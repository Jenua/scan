<?php

$path = $_SERVER['DOCUMENT_ROOT'];
require_once($path.'/db_connect/connect.php');
require_once($path.'/shipping_module/phpFunctions/proRanges/functions.php');

		$returnValue = false;
		$conn = getConnection();
		$query = "SELECT 
	case 
		when  [type_name] = 'min' THEN 'min'
		when  [type_name] = 'max' THEN 'max'
	END as [setting_type]
	,[setting_value]
  FROM [dbo].[general_settings] 
  WHERE [app] = 'shipping_module' 
  AND [group_name] = 'pro_numbers_nemf'
  AND 
  (
	[type_name] = 'min' 
	OR [type_name] = 'max'
  )";
		$res = runQuery($conn, $query);
		if (isset($res) && !empty($res)) 
		{
			$min = false;
			$max = false;
			foreach($res as $row)
			{
				if($row['setting_type'] == 'min') $min = $row['setting_value'];
				if($row['setting_type'] == 'max') $max = $row['setting_value'];
			}
			if($min && $max) $returnValue = $min.' - '.$max;
		}
		$returnValue = json_encode($returnValue);
		print_r($returnValue);
		$conn = null;
?>