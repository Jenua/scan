<?php
$path = $_SERVER['DOCUMENT_ROOT'];
require_once($path.'/db_connect/connect.php');

	$returnValue = false;
		
	$term = trim(strip_tags($_GET['term']));
	$term = str_replace(array("'", "`"), "''", $term);
		
	$conn = Database::getInstance()->dbc;
	$res = getValues($conn, $term);
	
	if(isset($res) && !empty($res))
	{
		$array = array();
		foreach($res as $key => $value)
		{
			$array[]= $value['ShipAddress_PostalCode'];
		}
		$returnValue = $array;
	}
	$returnValue = json_encode($returnValue);
	$conn = null;
	print_r($returnValue);

function getValues($conn, $term)
{
	$query = "SELECT TOP(100) * FROM (	
	SELECT 
	[shiptrack].[ShipAddress_PostalCode]
	FROM [".DATABASE."].[dbo].[shiptrack]
	WHERE [shiptrack].[ShipAddress_PostalCode] LIKE '%".$term."%' UNION ALL
	SELECT 
	[invoice].[ShipAddress_PostalCode]
	FROM [".DATABASE31."].[dbo].[invoice]
	WHERE [invoice].[ShipAddress_PostalCode] LIKE '%".$term."%' UNION ALL
	SELECT 
	[salesorder].[ShipAddress_PostalCode]
	FROM [".DATABASE31."].[dbo].[salesorder]
	WHERE [salesorder].[ShipAddress_PostalCode] LIKE '%".$term."%' UNION ALL
	SELECT 
	[manually_shiptrack].[ShipAddress_PostalCode]
	FROM [".DATABASE."].[dbo].[manually_shiptrack]
	WHERE [manually_shiptrack].[ShipAddress_PostalCode] LIKE '%".$term."%') A
	GROUP BY [ShipAddress_PostalCode]
	ORDER BY [ShipAddress_PostalCode]";
	//print_r($query);
	try{
		$result = $conn->prepare($query);
		$result->execute();
		$result = $result->fetchAll(PDO::FETCH_ASSOC); 
		return $result;
	}
	catch(PDOException $e)
	{
		return false;
	}
}
?>