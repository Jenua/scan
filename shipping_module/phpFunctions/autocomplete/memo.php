<?php
$path = $_SERVER['DOCUMENT_ROOT'];
require_once($path.'/db_connect/connect.php');

	$returnValue = false;
		
	$term = trim(strip_tags($_GET['term']));
	$term = str_replace(array("'", "`"), "''", $term);
		
	$conn = Database::getInstance()->dbc;
	$res = getValues($conn, $term);
	
	if(isset($res) && !empty($res))
	{
		$array = array();
		foreach($res as $key => $value)
		{
			$array[]= $value['Memo'];
		}
		$returnValue = $array;
	}
	$returnValue = json_encode($returnValue);
	$conn = null;
	print_r($returnValue);

function getConnection()
{
	try{
		$conn = new PDO("sqlsrv:server = ".SERVER."; Database = ".DATABASE, USER, PASSWORD);
		$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); }
		catch(PDOException $e)
		{
			die($e->getMessage());
		}
	return $conn;
}

function getValues($conn, $term)
{
	$query = "SELECT TOP(100) * FROM (	
	SELECT 
	[shiptrack].[Memo]
	FROM [".DATABASE."].[dbo].[shiptrack]
	WHERE [shiptrack].[Memo] LIKE '%".$term."%' UNION ALL
	SELECT 
	[invoice].[Memo]
	FROM [".DATABASE31."].[dbo].[invoice]
	WHERE [invoice].[Memo] LIKE '%".$term."%' UNION ALL
	SELECT 
	[salesorder].[Memo]
	FROM [".DATABASE31."].[dbo].[salesorder]
	WHERE [salesorder].[Memo] LIKE '%".$term."%' UNION ALL
	SELECT 
	[manually_shiptrack].[Memo]
	FROM [".DATABASE."].[dbo].[manually_shiptrack]
	WHERE [manually_shiptrack].[Memo] LIKE '%".$term."%') A
	GROUP BY [Memo]
	ORDER BY [Memo]";
	//print_r($query);
	try{
		$result = $conn->prepare($query);
		$result->execute();
		$result = $result->fetchAll(PDO::FETCH_ASSOC); 
		return $result;
	}
	catch(PDOException $e)
	{
		return false;
	}
}
?>