<?php
$path = $_SERVER['DOCUMENT_ROOT'];
require_once($path.'/db_connect/connect.php');

	$returnValue = false;
		
	$term = trim(strip_tags($_GET['term']));
	$term = str_replace(array("'", "`"), "''", $term);
		
	$conn = Database::getInstance()->dbc;
	$res = getValues($conn, $term);
	
	if(isset($res) && !empty($res))
	{
		$array = array();
		foreach($res as $key => $value)
		{
			$array[]= $value['ShipAddress_Addr2'];
		}
		$returnValue = $array;
	}
	$returnValue = json_encode($returnValue);
	$conn = null;
	print_r($returnValue);

function getValues($conn, $term)
{
	$query = "SELECT TOP(100) * FROM (	
	SELECT 
	[shiptrack].[ShipAddress_Addr2]
	FROM [".DATABASE."].[dbo].[shiptrack]
	WHERE [shiptrack].[ShipAddress_Addr2] LIKE '%".$term."%' UNION ALL
	SELECT 
	[invoice].[ShipAddress_Addr2]
	FROM [".DATABASE31."].[dbo].[invoice]
	WHERE [invoice].[ShipAddress_Addr2] LIKE '%".$term."%' UNION ALL
	SELECT 
	[salesorder].[ShipAddress_Addr2]
	FROM [".DATABASE31."].[dbo].[salesorder]
	WHERE [salesorder].[ShipAddress_Addr2] LIKE '%".$term."%' UNION ALL
	SELECT 
	[manually_shiptrack].[ShipAddress_Addr2]
	FROM [".DATABASE."].[dbo].[manually_shiptrack]
	WHERE [manually_shiptrack].[ShipAddress_Addr2] LIKE '%".$term."%' UNION ALL
	SELECT 
	[shiptrack].[ShipAddress_Addr3] as [ShipAddress_Addr2]
	FROM [".DATABASE."].[dbo].[shiptrack]
	WHERE [shiptrack].[ShipAddress_Addr3] LIKE '%".$term."%' UNION ALL
	SELECT 
	[invoice].[ShipAddress_Addr3] as [ShipAddress_Addr2]
	FROM [".DATABASE31."].[dbo].[invoice]
	WHERE [invoice].[ShipAddress_Addr3] LIKE '%".$term."%' UNION ALL
	SELECT 
	[manually_shiptrack].[ShipAddress_Addr3] as [ShipAddress_Addr2]
	FROM [".DATABASE."].[dbo].[manually_shiptrack]
	WHERE [manually_shiptrack].[ShipAddress_Addr3] LIKE '%".$term."%') A
	GROUP BY [ShipAddress_Addr2]
	ORDER BY [ShipAddress_Addr2]";
	//print_r($query);
	try{
		$result = $conn->prepare($query);
		$result->execute();
		$result = $result->fetchAll(PDO::FETCH_ASSOC); 
		return $result;
	}
	catch(PDOException $e)
	{
		return false;
	}
}
?>