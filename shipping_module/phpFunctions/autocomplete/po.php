<?php
$path = $_SERVER['DOCUMENT_ROOT'];
require_once($path.'/db_connect/connect.php');

	$returnValue = false;
		
	$term = trim(strip_tags($_GET['term']));
	$term = str_replace(array("'", "`"), "''", $term);
		
	$conn = Database::getInstance()->dbc;
	$res = getValues($conn, $term);
	
	if(isset($res) && !empty($res))
	{
		$array = array();
		foreach($res as $key => $value)
		{
			$array[]= $value['PONumber'];
		}
		$returnValue = $array;
	}
	$returnValue = json_encode($returnValue);
	$conn = null;
	print_r($returnValue);

function getValues($conn, $term)
{
	$query = "SELECT TOP(100) * FROM (	
	SELECT 
	[shiptrack].[PONumber]
	FROM [".DATABASE."].[dbo].[shiptrack]
	WHERE [shiptrack].[PONumber] LIKE '%".$term."%' UNION ALL
	SELECT 
	[invoice].[PONumber]
	FROM [".DATABASE31."].[dbo].[invoice]
	WHERE [invoice].[PONumber] LIKE '%".$term."%' UNION ALL
	SELECT 
	[salesorder].[PONumber]
	FROM [".DATABASE31."].[dbo].[salesorder]
	WHERE [salesorder].[PONumber] LIKE '%".$term."%' UNION ALL
	SELECT 
	[manually_shiptrack].[PONumber]
	FROM [".DATABASE."].[dbo].[manually_shiptrack]
	WHERE [manually_shiptrack].[PONumber] LIKE '%".$term."%') A
	GROUP BY [PONumber]
	ORDER BY [PONumber]";
	//print_r($query);
	try{
		$result = $conn->prepare($query);
		$result->execute();
		$result = $result->fetchAll(PDO::FETCH_ASSOC); 
		return $result;
	}
	catch(PDOException $e)
	{
		return false;
	}
}
?>