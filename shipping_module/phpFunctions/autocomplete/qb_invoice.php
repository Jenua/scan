<?php
$path = $_SERVER['DOCUMENT_ROOT'];
require_once($path.'/db_connect/connect.php');

	$returnValue = false;
		
	$term = trim(strip_tags($_GET['term']));
	$term = str_replace(array("'", "`"), "''", $term);
		
	$conn = Database::getInstance()->dbc;
	$res = getValues($conn, $term);
	
	if(isset($res) && !empty($res))
	{
		$array = array();
		foreach($res as $key => $value)
		{
			$array[]= $value['qb_invoice'];
		}
		$returnValue = $array;
	}
	$returnValue = json_encode($returnValue);
	$conn = null;
	print_r($returnValue);

function getValues($conn, $term)
{
	$query = "SELECT TOP(100) * FROM (
	SELECT [invoice].[RefNumber] as [qb_invoice]
	  FROM [".DATABASE31."].[dbo].[invoice]
	WHERE [invoice].[RefNumber] LIKE '%".$term."%'
) A
GROUP BY [qb_invoice]
ORDER BY [qb_invoice]";
	//print_r($query);
	try{
		$result = $conn->prepare($query);
		$result->execute();
		$result = $result->fetchAll(PDO::FETCH_ASSOC); 
		return $result;
	}
	catch(PDOException $e)
	{
		return false;
	}
}
?>