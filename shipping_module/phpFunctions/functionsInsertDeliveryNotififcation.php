<?php
$path = $_SERVER['DOCUMENT_ROOT'];
require_once($path.'/db_connect/connect.php');


	if (isset($_REQUEST['po']) && !empty($_REQUEST['po']) && isset($_REQUEST['message']))
	{
		$returnValue = 'log message was not saved';
		$po = $_REQUEST['po'];
		$message = $_REQUEST['message'];
		$conn = Database::getInstance()->dbc;
		$res = insertRow($conn, $po, $message);
		if ($res) 
		{
			$returnValue = 'log message saved';
			$returnValue = json_encode($returnValue);
			print_r($returnValue);
		} else 
		{
			$returnValue = json_encode($returnValue);
			print_r($returnValue);
		}
		$conn = null;
	} else 
	{
		$returnValue = 'Can not get parameters for request';
		$returnValue = json_encode($returnValue);
		print_r($returnValue);
	}

function insertRow($conn, $po, $message)
{
	try{
		$query = "INSERT INTO [dbo].".LOG."
           ([po]
           ,[message]
           ,[DATE])
     VALUES
           ('".$po."'
           ,'".$message."'
           ,GETDATE())";
		$result = $conn->prepare($query);
		$result->execute();
		return $result;
	}
	catch(PDOException $e)
	{
		die($e->getMessage());
	}
}
?>