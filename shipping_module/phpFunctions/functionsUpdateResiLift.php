<?php
$path = $_SERVER['DOCUMENT_ROOT'];
require_once($path.'/db_connect/connect.php');
date_default_timezone_set('America/New_York');

	if (isset($_REQUEST['id']) && !empty($_REQUEST['id']) && isset($_REQUEST['lift']) && isset($_REQUEST['resi']) && isset($_REQUEST['delivery_notification']))
	{
		$returnValue = 'lift and resi was not saved';
		$id = $_REQUEST['id'];
		$lift = $_REQUEST['lift'];
		$resi = $_REQUEST['resi'];
		$delivery_notification = $_REQUEST['delivery_notification'];
		$conn = Database::getInstance()->dbc;
		$res = updateRow($conn, $id, $lift, $resi, $delivery_notification);
		if ($res) 
		{
			$returnValue = 'lift, resi, delivery_notification saved';
			$returnValue = json_encode($returnValue);
			print_r($returnValue);
		} else 
		{
			$returnValue = json_encode($returnValue);
			print_r($returnValue);
		}
		$conn = null;
	} else 
	{
		$returnValue = 'Can not get parameters for request';
		$returnValue = json_encode($returnValue);
		print_r($returnValue);
	}

function updateRow($conn, $id, $lift, $resi, $delivery_notification)
{
	try{
		$query = "UPDATE ".SHIPTRACKWITHLABEL_TABLE." SET [LIFTGATE] = '".$lift."', [RESIDENTIAL] = '".$resi."', [delivery_notification] = '".$delivery_notification."' WHERE [ID] = '".$id."'";
		$result = $conn->prepare($query);
		$result->execute();
		return $result;
	}
	catch(PDOException $e)
	{
		die($e->getMessage());
	}
}
?>