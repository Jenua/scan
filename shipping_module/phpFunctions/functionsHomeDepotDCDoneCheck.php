<?php
//die('stop');
//ini_set('max_execution_time', 7200);
//ini_set("memory_limit", "1000M");
//Include files
require_once('functionsDB.php');
$connect = Database::getInstance()->dbc;


if (isset($_REQUEST['po']) && !empty($_REQUEST['po']) && isset($_REQUEST['txnlineid']) && !empty($_REQUEST['txnlineid']) && isset($_REQUEST['check'])) {
    
	$type = 'done';
	if( isset($_REQUEST['type']) ) {
		$type = $_REQUEST['type'];
	}
	main_check( $_REQUEST['po'], $_REQUEST['txnlineid'], $_REQUEST['check'], $type);
} else {
    echo json_encode( array("error" => 1) );
}

function main_check( $po, $tl, $status, $type = 'done' ) {
    global $connect;
    
	switch( $type ) {
		case 'done': $res = setDoneHomeDepotDC($connect, $po, $tl, $status); break;
		case 'disc': $res = setDiscHomeDepotDC($connect, $po, $tl, $status); break;		
		default:     $res = setDoneHomeDepotDC($connect, $po, $tl, $status); break;
	}
    echo $res;    
}