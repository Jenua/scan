<?php
require('functionsDB.php');
require('functionsExtr.php');
$path = $_SERVER['DOCUMENT_ROOT'];
require_once($path.'/db_connect/connect.php');
date_default_timezone_set('America/New_York');

$connect = Database::getInstance()->dbc;

//Start if have PO number
if ((isset($_REQUEST['value']) && !empty($_REQUEST['value'])) && (isset($_REQUEST['pro']) && !empty($_REQUEST['pro'])) && (isset($_REQUEST['bol']) && !empty($_REQUEST['bol'])) && (isset($_REQUEST['carrier']) && !empty($_REQUEST['carrier'])))
{
	updateProBol($connect, $_REQUEST['value'], $_REQUEST['pro'], $_REQUEST['bol'], $_REQUEST['carrier']);
} else 
{
	echo "Can not get values to save PRO# and INVOICE#";
	echo $_REQUEST['value'];
	echo $_REQUEST['pro'];
	echo $_REQUEST['bol'];
	echo $_REQUEST['carrier'];
}

$connect = null;

//write PRO# and BOL# to ShipTrackWithLabel
function updateProBol($connect, $Po, $pro, $bol, $carrier)
{
	if ($pro == 'test' && $bol == 'test')
	{
		echo 'PRO#, INVOICE# are saved in DB.';
	} else 
	{
		$extrCarrier = extrCarrier($carrier);
		$CARRIERNAME_PRO = $extrCarrier.'_'.$pro;
		try{
			$query = "UPDATE [dbo].[".SHIPTRACKWITHLABEL_TABLE."] SET [CARRIERNAME_PRO] = '".$CARRIERNAME_PRO."' WHERE [ID] = '".$Po."'";
			$result = execQuery($connect, $query);
			
			if ($result)
			{
				$query = "UPDATE [dbo].[".SHIPTRACKWITHLABEL_TABLE."] SET [CARRIERNAME] ='".$carrier."', [PRO] = '".$pro."', [BOL] = '".$bol."' WHERE [ID] = '".$Po."'";
				$result = execQuery($connect, $query);
				if ($result) echo 'PRO#, INVOICE# are saved in DB.';
			} else echo "<br>This PRO# is already in use.<br>";
		}catch (Exception $e) {
			print_r("Can not save PRO#, INVOICE# to database!");
		}
	}
}
?>