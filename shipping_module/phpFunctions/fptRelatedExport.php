<?php
// output headers so that the file is downloaded rather than displayed
ini_set('max_execution_time', 120);
header('Content-Type: text/csv; charset=utf-8');
header('Content-Disposition: attachment; filename=FPT_Related_'.date("Y-m-d").'.csv');

require('functionsExtr.php');
$path = $_SERVER['DOCUMENT_ROOT'];
require_once($path.'/db_connect/connect.php');

function exec_query($conn, $query) {
	try {
		$result = $conn->prepare($query);
		$result->execute();
		return $result;
	}
	catch(PDOException $e) {
		//print_r($e->getMessage());
	}
}
function getData( $conn ) {
	$content = [];
	
	$getDataQuery = "SELECT
						Product,
						RelatedStr
					FROM [fpt_related] AS fpt
					CROSS APPLY
					(
						SELECT [Related] + '|'
						FROM [fpt_related] as internal	
						WHERE internal.[Product] = fpt.[Product]
						ORDER BY [Related]
						FOR XML PATH('')
					) pre_trimmed (RelatedStr)
					GROUP BY [Product], [RelatedStr]
					ORDER BY [Product]";
	$getDataResult = exec_query( $conn, $getDataQuery );
	$output = fopen('php://output', 'w');
	foreach( $getDataResult as $row ) {		
		$related = explode('|',$row['RelatedStr']);
		$result = array_merge( [$row['Product']],  $related );
		//fputcsv($output, array( $row['Product'], $row['RelatedStr']));
		fputcsv($output, $result);
		set_time_limit(30);
	}
	
}
// ==========================================================================================================================================
$conn = Database::getInstance()->dbc;
$content = getData( $conn );
//*/



