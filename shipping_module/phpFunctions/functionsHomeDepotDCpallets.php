<?php
//PHP configuration
ini_set('max_execution_time', 7200);
ini_set("memory_limit", "2000M");
//Include files
//Root path
$path = $_SERVER['DOCUMENT_ROOT'];
//DB connection class
require_once($path.'/db_connect/connect.php');
//QR Code library
require_once($path.'/shipping_module/Include/phpqrcode/qrlib.php');
//Barcode library classes
require_once($path.'/shipping_module/Include/barcode/class/BCGFontFile.php');
require_once($path.'/shipping_module/Include/barcode/class/BCGColor.php');
require_once($path.'/shipping_module/Include/barcode/class/BCGDrawing.php');
require_once($path.'/shipping_module/Include/barcode/class/BCGupca.barcode.php');
require_once($path.'/shipping_module/Include/barcode/class/BCGpdf417.barcode2d.php');
//Twig template engine
require_once($path.'/shipping_module/Include/Twig/Autoloader.php');

function getDealerType( $conn, $type ) {
	$query   = "SELECT TOP 1 * FROM [dc_type] WHERE id = ".$conn->quote( $type );
	$result = $conn->prepare($query);
    $result->execute();
	$dealer  = $result->fetch(PDO::FETCH_ASSOC);
	return $dealer;
}
function dbg( $object ) {
	die( "<pre>".print_r($object,true)."</pre>" );
}

//Global variables
$error_messages = array();//Array to save all errors
$data_error_messages = array();//Array to save all errors in input data
$connect = Database::getInstance()->dbc;

$FOLDER = "Html";
$EXTENSION = ".html";

$filesGenerated = [];

$gsku   = empty($_REQUEST['gsku']) ? '' : $_REQUEST['gsku'];
$type   = empty($_REQUEST['dealerId']) ? '1' : $_REQUEST['dealerId'];
$dealer = getDealerType( $connect, $type );

if( !empty($_REQUEST['po']) ) {
	$po = $_REQUEST['po'];
	$palletData = false;
	$truckData  = false;
	$TruckItems = false;
	$OrderInfo  = false;
	
	$isEXRT = false;
	if( $dealer['ID'] == 2 ) $isEXRT = true;
	
	$GroupsByPallet = GetGroupsByPallet( $connect, $po, $dealer['isPallet'], $isEXRT );
	$ItemsByPallet  = GetItemsByPallet( $connect, $po, $dealer['isPallet'] );
	$OrderInfo      = GetOrderInformation( $connect, $po );
	if( $dealer['isPallet'] ) {
		$TruckItems = GetTruckData( $connect, $po );
	}	
	
	if( $GroupsByPallet && $ItemsByPallet ) {
		$palletData = PreparePalletData( $GroupsByPallet, $ItemsByPallet, $po, $dealer, $gsku );
		unset($GroupsByPallet);
		unset($ItemsByPallet);
	}
	if( $TruckItems ) {
		$truckData = PrepareTruckData($TruckItems);
		unset($TruckItems);
	}
	
	Twig_Autoloader::register();
	$loader = new Twig_Loader_Filesystem('../templates/HDDC');
	$twig = new Twig_Environment($loader, array(
		'cache'       => 'compilation_cache',
		'auto_reload' => true
	));
	
	if( $palletData ) {
		foreach( $palletData as $palletId => $pallet ) {

			
			$htmlBarcodes = $twig->render('palletBarcodes.html', array('pallet' => $palletId, 'data' => $pallet, 'isPallet' => $dealer['isPallet']));
			$filesGenerated[] = saveHtmlDocument( $htmlBarcodes, 'barcodes', $po, $palletId, $gsku );

			$htmlFlyers = $twig->render('palletFlyers.html', array('po'=> $po, 'pallet' => $palletId, 'data' => $pallet, 'order' => $OrderInfo, 'isPallet' => $dealer['isPallet'], 'isFlyer' => $dealer['isFlyer']));
			$filesGenerated[] = saveHtmlDocument( $htmlFlyers, 'flyers', $po, $palletId, $gsku );
			if( !empty( $pallet['LTL'] ) ) {
				foreach( $pallet['LTL']['Groups'] as $Group ) {
					saveQrCode($connect, $po, $Group['HD_SKU'], $Group['UID_9']);				
				}	
			}
			if( !empty( $pallet['GR'] ) ) {
				foreach( $pallet['GR']['Groups'] as $Group ) {
					saveQrCode($connect, $po, $Group['HD_SKU'], $Group['UID_9']);
				}
			}

			if( !$gsku ) {
				$htmlLists = $twig->render('palletPicking.html', array('po'=> $po, 'pallet' => $palletId, 'data' => $pallet, 'isPallet' => $dealer['isPallet']));
				$filesGenerated[] = saveHtmlDocument( $htmlLists, 'picking', $po, $palletId );

				$htmlLists = $twig->render('palletPacking.html', array('po'=> $po, 'pallet' => $palletId, 'data' => $pallet, 'isPallet' => $dealer['isPallet']));
				$filesGenerated[] = saveHtmlDocument( $htmlLists, 'packing', $po, $palletId );
				
				if( !empty($pallet['LTL']) ) {
					$htmlLists = $twig->render('palletCarton.html', array('po'=> $po, 'pallet' => $palletId, 'data' => $pallet, 'isPallet' => $dealer['isPallet']));
					$filesGenerated[] = saveHtmlDocument( $htmlLists, 'carton', $po, $palletId );				
				}
			}

		}
	}
	if( $truckData && !$gsku ) {
		foreach( $truckData as $truckId => $truck ) {
			$htmlBOL = $twig->render('truckBOL.html', array('po'=> $po, 'truck' => $truckId, 'pages' => $truck));
			$filesGenerated[] = saveHtmlDocument( $htmlBOL, 'truck', $po, $truckId, $gsku );
		}
	}
	
	die( "<pre>".print_r($filesGenerated, true)."</pre>" );
}

function saveHtmlDocument( $data, $docType, $po, $palletId, $gsku='' ) {
	global $FOLDER,$EXTENSION;	
	
	$docID = $gsku ? $gsku : $palletId;
	$filename = $po."_".$docType."_".$docID.$EXTENSION;
	file_put_contents("../".$FOLDER."/".$filename, $data);
	return $filename;
}

function saveQrCode($conn, $po, $SKU, $UID_9) {
	if( !empty($po) && !empty($SKU) && !empty($UID_9) ) {
		$query="INSERT INTO [qr_code]
					([po]
					,[sku]
					,[code]
					,[gen_date])
				VALUES
					(".$conn->quote($po)."
					,".$conn->quote($SKU)."
					,".$conn->quote($UID_9)."
					,GETDATE()
					);";
		$result = $conn->prepare($query);
		$result->execute();
		if(!empty($result)) return true; else return false;
	}	
	return false;
}

function GetOrderInformation( $conn, $po ) {
	$query = "	SELECT
					stwl.[PRO],
					stwl.[BOL],
					st.[ShipMethodRef_FullName] as [Carrier],
					st.[ShipAddress_Addr1] as [Addr1],
					st.[ShipAddress_Addr2] as [Addr2],
					st.[ShipAddress_Addr3] as [Addr3],
					st.[ShipAddress_Addr4] as [Addr4],
					st.[ShipAddress_Addr5] as [Addr5],
					st.[ShipAddress_City] as [City],
					st.[ShipAddress_State] as [State],
					st.[ShipAddress_PostalCode] as [Postal],
					ISNULL( st.[ShipAddress_Country], 'USA') as [Country]
				FROM [ShipTrackWithLabel] as [stwl]
				LEFT JOIN [shiptrack] as [st] ON [st].[RefNumber] = [stwl].[ID]
				WHERE [st].[PONumber] = ".$conn->quote($po)."
				;";
	$result = $conn->prepare($query);
	$result->execute();
    $result = $result->fetch(PDO::FETCH_ASSOC);
	
	$address  = (empty($result['Addr1']) ? '' : ($result['Addr1'] . "<br/>"))
			  .	(empty($result['Addr2']) ? '' : ($result['Addr2'] . "<br/>"))
			  .	(empty($result['Addr3']) ? '' : ($result['Addr3'] . "<br/>"))
			  .	(empty($result['Addr4']) ? '' : ($result['Addr4'] . "<br/>"))
			  .	(empty($result['Addr5']) ? '' : ($result['Addr5'] . "<br/>"));
	
	$citystate  = empty($result['City']) ? '' : $result['City'];
	$citystate .= empty($result['State']) ? '' : (empty($citystate) ? $result['State'] : ', '.$result['State'] );
	
	return [
		'address'   => $address,
		'citystate' => $citystate,
		'postal'    => $result['Postal'],
		'carrier'   => $result['Carrier'],
		'PRO'       => $result['PRO'],
		'BOL'       => $result['BOL'],
	];
	
}

function GetGroupsByPallet( $conn, $po, $isPallet=true, $isEXRT=false ) {
	if( $isEXRT == true ) {
		$boxCountTweak = "CASE 
							WHEN [groupdetail].[ItemGroupRef_FullName] LIKE '%-EX%' OR [groupdetail].[ItemGroupRef_FullName] LIKE '%-RT%'
							THEN CAST([DL_valid_SKU].[Box_Count] AS INT) + 1
							ELSE CAST([DL_valid_SKU].[Box_Count] AS INT)
						  END";
	} else {
		$boxCountTweak = "CAST([DL_valid_SKU].[Box_Count] AS INT)";
	}

	if( $isPallet ) {
		$preloadSelect1 = "	,ISNULL(SUM(CAST([pallet_preload].[QUANTITY] AS INT)) OVER (partition by [pallet_preload].[UPC]), 0) AS [Preloaded]
							,[pallet_preload].[PALLET] as [CurrentPallet]
							,[pallet_preload].[Quantity] as [Pallet_Quantity]";
		$preloadSelect2 = "	,CAST([pallet_preload].[Quantity] AS INT) * ".$boxCountTweak." AS [Box_Pallet_Quantity]";
		$preloadJoin    = "	LEFT JOIN [pallet_preload] ON [pallet_preload].[UPC] = [DL_valid_SKU].[UPC] AND [pallet_preload].[PONUMBER] = [shiptrack].[PONumber]";
	} else {
		$preloadSelect1 = "	,[groupdetail].[Quantity] as [Preloaded]
							,'Order' as [CurrentPallet]
							,[groupdetail].[Quantity] as [Pallet_Quantity]";
		$preloadSelect2 = "	,CAST([groupdetail].[Quantity] AS INT) * ".$boxCountTweak." AS [Box_Pallet_Quantity]";
		$preloadJoin    = "";
	}
	
	$query="SET NOCOUNT ON
			BEGIN

			DECLARE @Weights TABLE
			(
				[GroupIDKEY] varchar(255),
				[OneQtyWeight] int
			)
			;

			INSERT INTO @Weights (
				GroupIDKEY,
				OneQtyWeight
				)
			SELECT
				distinct [linedetail].[GroupIDKEY]
				,sum(CAST(dbo.udf_GetNumeric([linedetail].[CustomField9]) AS INT)) over  (partition by [linedetail].[GroupIDKEY])
			FROM [dbo].[linedetail]
			INNER JOIN [dbo].[groupdetail] ON [groupdetail].[TxnLineID] = [linedetail].[GroupIDKEY]
			INNER JOIN [dbo].[shiptrack] ON [shiptrack].[TxnID] = [groupdetail].[IDKEY]
			WHERE [shiptrack].[PONumber] = ".$conn->quote($po)."
			AND [linedetail].[CustomField9] is not null
			AND [linedetail].[CustomField9] != '0'
			;
			END
			;

			SELECT
				ISNULL(
					SUM(a.[Box_Pallet_Quantity]) OVER(ORDER BY [CurrentPallet], [Weight] DESC ROWS BETWEEN UNBOUNDED PRECEDING AND 1 PRECEDING )
					, 0
				) AS [LabelBoxesStart]
				,SUM(a.[Box_Pallet_Quantity]) OVER (PARTITION BY [PONumber]) AS [TotalQuantity]
				,a.*
			FROM
				(
				SELECT
					 [shiptrack].[PONumber]		
					,[groupdetail].[TxnLineID]
					,[groupdetail].[ItemGroupRef_FullName]
					,wt.[OneQtyWeight] AS [Weight]
					,[groupdetail].[Prod_desc]
					,[groupdetail].[Quantity]
					".$preloadSelect1."
					,[DL_valid_SKU].[UPC]
					,[DL_valid_SKU].[HD_SKU]
					,[DL_valid_SKU].[AmazonDS_SKU] AS [ASIN]
					,[DL_valid_SKU].[SKU]
					,[DL_valid_SKU].[SHIP_METHOD]
					,".$boxCountTweak." AS [Box_Count]
					,CAST([groupdetail].[Quantity] AS INT) * ".$boxCountTweak." AS [Box_Quantity]
					".$preloadSelect2."
					,[item_info].[name] as [CollectionName]
					,[item_info].[drawing] as [Drawing_Link]
					,[item_info].[dimensions]
					,LEFT(Replace(CONVERT(varchar(255), NEWID()),'-',''),(9)) AS [UID_9]
				FROM [groupdetail]
				LEFT JOIN @Weights AS wt ON wt.[GroupIDKEY] = [groupdetail].[TxnLineId]
				LEFT JOIN [shiptrack] ON [groupdetail].[IDKEY] = [shiptrack].[TxnID]
				LEFT JOIN [DL_valid_SKU] ON [groupdetail].[ItemGroupRef_FullName] = [DL_valid_SKU].[QB_SKU]	AND [DL_valid_SKU].[UPC] IS NOT NULL
				LEFT JOIN [hddc_checker] ON [hddc_checker].[TxnLineID] = [groupdetail].[TxnLineID]				
				".$preloadJoin."
				LEFT JOIN [item_info] ON [DL_valid_SKU].[SKU] = [item_info].[sku]
				WHERE [shiptrack].[PONumber] = ".$conn->quote($po)."
				  AND ISNULL([hddc_checker].[Discontinued], 0) = 0
				) a
			WHERE [Quantity] > 0
			  AND [CurrentPallet] IS NOT NULL
			GROUP BY [PONumber], [TxnLineID], [ItemGroupRef_FullName], [Weight], [Prod_desc], [Quantity], [Preloaded], [CurrentPallet], [Pallet_Quantity], [UPC], [HD_SKU], [ASIN], [SKU], [SHIP_METHOD], [Box_Count], [Box_Quantity], [Box_Pallet_Quantity], [CollectionName], [Drawing_Link], [dimensions], [UID_9]
			ORDER BY [CurrentPallet], [Weight] DESC
			;";
	$result = $conn->prepare($query);
    $result->execute();
    $result = $result->fetchAll(PDO::FETCH_ASSOC);
    if(!empty($result)) return $result; else return false;
}

function GetItemsByPallet( $conn, $po, $isPallet=true ) {
	if( $isPallet ) {
		$preloadSelect = "	,[pallet_preload].[Quantity] as [Pallet_Quantity]
							,[pallet_preload].[PALLET] as [CurrentPallet]";
		$preloadJoin   = "	LEFT JOIN [pallet_preload] ON [pallet_preload].[UPC] = [DL_valid_SKU].[UPC] AND [pallet_preload].[PONUMBER] = [shiptrack].[PONumber]";
		$preloadWhere  = "	AND [pallet_preload].[PALLET] IS NOT NULL";
	} else {
		$preloadSelect = "	,[groupdetail].[Quantity] as [Pallet_Quantity]
							,'Order' as [CurrentPallet]";
		$preloadJoin   = "";
		$preloadWhere  = "";
	}
	$query="SELECT DISTINCT 
				 [shiptrack].[PONumber]
				,[groupdetail].[TxnLineID] as [GroupId]
				,[linedetail].[TxnLineID] as [ItemId]
				,[linedetail].[ItemRef_FullName]
				,[groupdetail].[Quantity]	
				,[linedetail].[quantity] as [item_quantity]
				,CAST(dbo.udf_GetNumeric([linedetail].[CustomField9]) AS INT) AS [ItemWeight]
				".$preloadSelect."
			FROM [dbo].[groupdetail]
			LEFT JOIN [dbo].[shiptrack] ON [groupdetail].[IDKEY] = [shiptrack].[TxnID]
			LEFT JOIN [DL_valid_SKU] ON [groupdetail].[ItemGroupRef_FullName] = [DL_valid_SKU].[QB_SKU]	AND [DL_valid_SKU].[UPC] IS NOT NULL
			".$preloadJoin."
			LEFT JOIN [dbo].[linedetail] ON [groupdetail].[TxnLineID] = [linedetail].[GroupIDKEY]
			WHERE [shiptrack].[PONumber] = ".$conn->quote($po)."
			".$preloadWhere."
			ORDER BY [CurrentPallet], [ItemWeight] DESC
			;";
	
	//die( "<pre>".print_r($query,true)."</pre>" );
	
	$result = $conn->prepare($query);
	$result->execute();
    $result = $result->fetchAll(PDO::FETCH_ASSOC);
    if(!empty($result)) return $result; else return false;	
}

function GetTruckData( $conn, $po ) {
	$query="SELECT
				tp.[TRUCK]
				,pp.[PALLET]
				,(CASE WHEN dl.[QB_SKU] LIKE 'DLT-%' THEN 'GR' ELSE dl.[SHIP_METHOD] END) as [ShipMethod]
				,dl.[QB_SKU] as [GroupSKU]
				,pp.[Quantity]
			FROM [groupdetail] AS gr
			LEFT JOIN [shiptrack] as st ON st.[TxnID] = gr.[IDKEY]
			LEFT JOIN [DL_valid_SKU] as dl ON gr.[ItemGroupRef_FullName] = dl.[QB_SKU] AND dl.[QB_SKU] like '%'+dl.[SKU] AND (dl.[Menards_SKU] IS NULL OR dl.[QB_SKU] != dl.[Menards_SKU])
			LEFT JOIN [pallet_preload] as pp ON pp.[UPC] = dl.[UPC] AND pp.[PONumber] = st.[PONumber]
			LEFT JOIN [truck_preload] as tp ON pp.[PALLET] = tp.[PALLET] AND tp.[PONumber] = st.[PONumber]
			WHERE st.[PONumber] = ".$conn->quote($po)."
			AND gr.[Quantity] > 0
			AND pp.[PALLET] IS NOT NULL
			AND tp.[TRUCK] IS NOT NULL
			;
";
	$result = $conn->prepare($query);
    $result->execute();
    $result = $result->fetchAll(PDO::FETCH_ASSOC);
    if(!empty($result)) return $result; else return false;	
}

function palletItemsSort( $a, $b ) {
	$dW = $b['ItemWeight'] - $a['ItemWeight'];
	if( $dW != 0 ) return $dW;
	
	$compare = strcmp ( $a['ItemRef_FullName'], $b['ItemRef_FullName'] );
	if( $compare == 0 ) {
		$dA =  $a['item_quantity'] / $a['Quantity'] * $a['Pallet_Quantity'];
		$dB =  $b['item_quantity'] / $b['Quantity'] * $b['Pallet_Quantity'];
		return $dA - $dB;
	}
	return $compare;	
}

function parseItemFullName( $name ) {
    return end( explode(":", $name) );
}

function PreparePalletData( $GroupsByPallet, $ItemsByPallet, $po, $dealer, $gsku='' ) {
	$palletData = [];
	
	foreach( $GroupsByPallet as $Group ) {
		
		$isGSKU = $gsku ? ($Group['ItemGroupRef_FullName'] == $gsku) : true;
		
		if( !empty($Group['CurrentPallet']) and $isGSKU ) {			
			$GRLTL = 'GR';
			if( $Group['SHIP_METHOD'] == 'LTL' ) {
				$GRLTL = checkLTL( $Group['ItemGroupRef_FullName'] );
			}
			$palletData[$Group['CurrentPallet']][$GRLTL]['GRLTL'] = $GRLTL;
			$palletData[$Group['CurrentPallet']]['TotalQuantity'] = $Group['TotalQuantity'];
			//
			
			/*
			$palletData[$Group['CurrentPallet']]['GRLTL'] = 'GR';
			if( $Group['SHIP_METHOD'] == 'LTL' ) {
				$palletData[$Group['CurrentPallet']]['GRLTL'] = checkLTL( $Group['ItemGroupRef_FullName'] );
			}
			//*/
			
			foreach( $ItemsByPallet as $Item ) {
				if( $Item['GroupId'] == $Group['TxnLineID'] && $Item['CurrentPallet'] == $Group['CurrentPallet'] ) {
					$Item['ItemRef_FullName'] = parseItemFullName($Item['ItemRef_FullName']);
					$Group['Items'][] = $Item;
					$palletData[$Group['CurrentPallet']][$GRLTL]['Items'][] = $Item;
				}
			}
			
			$Group['BarcodesQty'] = 2;
			
			$Group['QR'] = GetQR( $Group['UID_9'] );
			$Group['Drawing'] = getDrawing( $Group['Drawing_Link'] );
			$Group['UPCA'] = getUPCA( $Group['UPC'] );
			$Group['isAdditionalFlyer'] = getIsAdditionalFlyer( $dealer, $Group['ItemGroupRef_FullName'] );
			if( $Group['isAdditionalFlyer'] ) {
				$pdf417 = $dealer['P417'].",PO:".$po.",UPC:".$Group['UPC'].",QTY:".$Group['Pallet_Quantity'];
				$Group['PDF417'] = getPdf417( $pdf417 );
			} else {
				$Group['PDF417'] = '';
			}
			$Group['PackingBarcodes'] = [];
			
			
			
			if( empty( $palletData[$Group['CurrentPallet']]['PickingBarcode'] ) ) {
				$palletData[$Group['CurrentPallet']]['PickingBarcode'] = getTextBarcode( 'Picking_'.$Group['PONumber'].'_'.$Group['CurrentPallet'] );
			}
			if( empty( $palletData[$Group['CurrentPallet']]['PackingBarcode'] ) ) {
				$palletData[$Group['CurrentPallet']]['PackingBarcode'] = getTextBarcode( 'Packing_'.$Group['PONumber'].'_'.$Group['CurrentPallet'] );
			}
			for( $i=1; $i<=$Group['Pallet_Quantity']; $i++ ) {
				$Group['PackingBarcodes'][$i] = getTextBarcode( 'Packing_'.$Group['PONumber'].'_'.$Group['CurrentPallet'].'_'.$Group['ItemGroupRef_FullName'].'_'.$i );
			}

			$palletData[$Group['CurrentPallet']][$GRLTL]['Groups'][] = $Group;
			usort( $palletData[$Group['CurrentPallet']][$GRLTL]['Items'], 'palletItemsSort' );
		}
	}
	//dbg( $palletData );
	return $palletData;	
}

function checkLTL( $GSKU ) {
	if( $GSKU == 'DLT-113660' ) return 'GR';
	if( $GSKU == 'DLT-103660' ) return 'GR';
	return 'LTL';
}

function PrepareTruckData( $TruckItems ) {
	
	$currentTruck = null;
	$currentPage = 0;
	$pages = [];
	$SKU_LIMIT = 26;
	$counter = 0;

	foreach( $TruckItems as $TruckItem ){
		if( !$currentTruck ) {
			$currentTruck = $TruckItem['TRUCK'];
		}
		
		if( $currentTruck != $TruckItem['TRUCK'] ) {
			$currentTruck = $TruckItem['TRUCK'];
			$currentPage = 0;
			$counter = 0;
		}
		
		if( $counter >= $SKU_LIMIT ) {
			++$currentPage;
			$counter = 0;
		}
		$pages[ $currentTruck ][ $currentPage ][ $TruckItem['PALLET'] ][] = [
			'GroupSKU'   => $TruckItem['GroupSKU'],
			'ShipMethod' => $TruckItem['ShipMethod'],
			'Quantity'   => $TruckItem['Quantity'],
		];
		++$counter;
	}

	return $pages;
}

/*

$valueForGen = "AMZN,PO:".$row['PONumber'].",UPC:".$row['UPC'].",QTY:".$row['quantity'];
//print_r($valueForGen);
$image = gen_imageBCGpdf417($valueForGen, $row['UPC']);
   
 */

function GetQR($uid)
{
	try{
		$value = 'http://dreamline.com/warranty/?SRVID='.$uid;
		ob_start();
			QRCode::png($value, null);
			$result = base64_encode( ob_get_contents() );
		ob_end_clean();
		return $result;
	} catch (Exception $e){
		$result = "Error. Can not generate QR code. ".$e->getMessage();
		$result = json_encode($result);
		print_r($result);
		die();
	}
}

function getDrawing($link)
{
    if($link) $link = "../drawings/".$link; else $link = "../drawings/spacer.png";
    if (!file_exists($link)) {
        $link = "../drawings/spacer.gif";
    }
    $drawing = file_get_contents($link);
    $result = base64_encode( $drawing );
    return $result;
}

function getUPCA($value)
{
    global $path;
	
	if( strlen($value) > 12 ) {
		$value = substr($value, -12);
	}
	
    $uniqid = uniqid();
    try{
        // Loading Font
        $font = new BCGFontFile($path.'/shipping_module/Include/barcode/font/Arial.ttf', 18);

        // The arguments are R, G, B for color.
        $color_black = new BCGColor(0, 0, 0);
        $color_white = new BCGColor(255, 255, 255); 

        $code = new BCGupca();
        $code->setScale(3); // Resolution
        $code->setThickness(50); // Thickness
        $code->setForegroundColor($color_black); // Color of bars
        $code->setBackgroundColor($color_white); // Color of spaces
        $code->setFont($font); // Font (or 0)
        $code->parse($value); // Text
        $drawing = new BCGDrawing('', $color_white);
        $drawing->setBarcode($code);
        $drawing->draw();

	
        ob_start();
            $drawing->finish(BCGDrawing::IMG_FORMAT_PNG);
            $result = base64_encode( ob_get_contents() );
        ob_end_clean();
        
        unset($color_black);
        unset($color_white);
        
        return $result;
    } catch (Exception $e){
        $result = "Error. Can not generate UPC-A barcode. ".$e->getMessage();
        $result = json_encode($result);
        print_r($result);
        die();
    }
}

function GetPdf417($value)
{
	global $error_messages;
	$uniqid = uniqid();
	try {
		// The arguments are R, G, B for color.
		$color_black = new BCGColor(0, 0, 0);
		$color_white = new BCGColor(255, 255, 255); 

		$code = new BCGpdf417();
		$code->setScale(2); // Resolution
		$code->setColumn(3);
		$code->setForegroundColor($color_black); // Color of bars
		$code->setBackgroundColor($color_white); // Color of spaces
		$code->setErrorLevel(-1);
		$code->setCompact(false);
		$code->setQuietZone(true);
		$code->parse($value); // Text

		$drawing = new BCGDrawing('', $color_white);
		$drawing->setBarcode($code);
		$drawing->draw();

        ob_start();
            $drawing->finish(BCGDrawing::IMG_FORMAT_PNG);
            $result = base64_encode( ob_get_contents() );
        ob_end_clean();
        
		unset($color_black);
		unset($color_white);
		
		return $result;		
	} catch (Exception $e){		
		$result = "Error. Can not generate Pdf417 barcode. ".$e->getMessage();
		$result = json_encode($result);
		print_r($result);
		die();
	}
}

function getTextBarcode($value)
{
global $error_messages;
$uniqid = uniqid();
	try{
	// The arguments are R, G, B for color.
	$color_black = new BCGColor(0, 0, 0);
	$color_white = new BCGColor(255, 255, 255); 

	$code = new BCGpdf417();
	$code->setScale(2); // Resolution
	$code->setColumn(3);
	$code->setForegroundColor($color_black); // Color of bars
	$code->setBackgroundColor($color_white); // Color of spaces
	$code->setErrorLevel(-1);
	$code->setCompact(false);
	$code->setQuietZone(true);
	$code->parse($value); // Text
	
	$drawing = new BCGDrawing('', $color_white);
	$drawing->setBarcode($code);
	$drawing->draw();
	
	ob_start();
        $drawing->finish(BCGDrawing::IMG_FORMAT_PNG);
        $result = base64_encode( ob_get_contents() );
    ob_end_clean();
	
	unset($color_black);
	unset($color_white);
	
	return $result;
	} catch (Exception $e){
        $result = "Error. Can not generate Pallet barcode. ".$e->getMessage();
        $result = json_encode($result);
        print_r($result);
        die();
	}
}

function getIsAdditionalFlyer( $dealer, $gsku ) {
	if( $dealer['ID'] != 2 ) { return false; }
	return true;
	/*
	$correct_gskus = [
		'DL-6152-01CL',
		'DL-6153-01CL',
		'DL-6154-01CL',
		'DL-6152-01FR',
		'DL-6153-01FR',
		'DL-6154-01FR',
		'DL-6701-01CL',
		'DL-6702-01CL',
		'DL-6703-01CL',
		'DL-6701-01FR',
		'DL-6702-01FR',
		'DL-6703-01FR',
		'DL-6701-22-01',
		'DL-6701-89-01',
		'DL-6702-22-01',
		'DL-6702-89-01',
		'DL-6703-22-01',
		'DL-6703-89-01',
		'DL-6701-22-01FR',
		'DL-6701-89-01FR',
		'DL-6702-22-01FR',
		'DL-6702-89-01FR',
		'DL-6703-22-01FR',
		'DL-6703-89-01FR'
	];
	return in_array( $gsku, $correct_gskus );
	//*/
}