<?php

$path = $_SERVER['DOCUMENT_ROOT'];
require_once($path.'/db_connect/connect.php');
	if (isset($_REQUEST['id']) && !empty($_REQUEST['id']))
	{
		$returnValue = 'Error. Can not get suborders data.';
		$id = $_REQUEST['id'];
		$conn = Database::getInstance()->dbc;
		$res = getDivided($conn, $id);
		if ($res) 
		{
			$returnValue = $res;
		}
		$returnValue = json_encode($returnValue);
		print_r($returnValue);
		$conn = null;
	} else 
	{
		$returnValue = 'Id is missing.';
		$returnValue = json_encode($returnValue);
		print_r($returnValue);
	}

function getDivided($conn, $id)
{
	try{
		$query = "SELECT 
[Orders_combined].[PONumber], 
[Orders_combined].[RefNumber],
[ShipTrackWithLabel].[PRO],
[ShipTrackWithLabel].[BOL]
						  FROM [dbo].[Orders_combined]
						  INNER JOIN [dbo].[manually_shiptrack] ON [Orders_combined].[Combined_into_RefNumber] = [manually_shiptrack].[RefNumber]
						  INNER JOIN [dbo].[ShipTrackWithLabel] ON [Orders_combined].[RefNumber] = [ShipTrackWithLabel].[ID]
						  WHERE [Combined_into_RefNumber] = '".$id."'
						 ";
						 //print_r($query);
		$result = $conn->prepare($query);
		$result->execute();
		$result = $result->fetchAll(PDO::FETCH_ASSOC); 
		return $result;
	}
	catch(PDOException $e)
	{
		return false;
	}
}
?>