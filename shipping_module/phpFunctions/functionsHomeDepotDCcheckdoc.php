<?php
//die('stop');
//ini_set('max_execution_time', 7200);
//ini_set("memory_limit", "1000M");
//Include files
if( empty($_REQUEST['po']) || empty($_REQUEST['type']) || empty($_REQUEST['id']) ) {
	die(json_encode(['error'=>'Not enough data']));
}

$FOLDER    = 'Html';
$EXTENSION = '.html';
$po        = $_REQUEST['po'];
$type      = $_REQUEST['type'];
$id        = $_REQUEST['id'];
$filename  = $po."_".$type."_".$id.$EXTENSION;
$path      = $FOLDER."/".$filename;
$fileExist = file_exists("../".$path);

if( !$fileExist ) {
	die(json_encode(['error'=>'No file: '.$path]));
}
die(json_encode(['error'=>0, 'path'=>$path]));
