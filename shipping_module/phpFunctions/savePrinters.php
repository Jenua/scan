<?php

$path = $_SERVER['DOCUMENT_ROOT'];
require_once($path.'/db_connect/connect.php');



	if (isset($_REQUEST['id']) && !empty($_REQUEST['id']))
	{
		$returnValue = 'Error saving printers.';
		$id = $_REQUEST['id'];
		if (isset($_REQUEST['printer_label']) && !empty($_REQUEST['printer_label'])) $printer_label = $_REQUEST['printer_label']; else $printer_label = null;
		if (isset($_REQUEST['printer_bol']) && !empty($_REQUEST['printer_bol'])) $printer_bol = $_REQUEST['printer_bol']; else $printer_bol = null;
		if (isset($_REQUEST['printer_hddc1']) && !empty($_REQUEST['printer_hddc1'])) $printer_hddc1 = $_REQUEST['printer_hddc1']; else $printer_hddc1 = null;
		if (isset($_REQUEST['printer_hddc2']) && !empty($_REQUEST['printer_hddc2'])) $printer_hddc2 = $_REQUEST['printer_hddc2']; else $printer_hddc2 = null;
		$conn = Database::getInstance()->dbc;
		$res = savePrinters($conn, $id, $printer_label, $printer_bol, $printer_hddc1, $printer_hddc2);
		if (isset($res) && !empty($res)) 
		{
			$returnValue = 'Printers saved.';
		}
		$returnValue = json_encode($returnValue);
		print_r($returnValue);
		$conn = null;
	} else 
	{
		$returnValue = 'Can not get user ID or printer names.';
		$returnValue = json_encode($returnValue);
		print_r($returnValue);
	}

function savePrinters($conn, $id, $printer_label, $printer_bol, $printer_hddc1, $printer_hddc2)
{
	try{
		$query = "UPDATE [dbo].[User]
   SET [printer_label] = '".$printer_label."'
      ,[printer_bol] = '".$printer_bol."'
	  ,[printer_hddc1] = '".$printer_hddc1."'
	  ,[printer_hddc2] = '".$printer_hddc2."'
 WHERE [ID] = ".$id;
		$result = $conn->prepare($query);
		$result->execute();
		return $result;
	}
	catch(PDOException $e)
	{
		return false;
	}
}
?>