<?php
ini_set("memory_limit", "500M");
//Include files

require_once('functionsDB.php');
require_once('functionsExtr.php');
require_once('fedexShipService.php');
$path = $_SERVER['DOCUMENT_ROOT'];
require_once($path.'/db_connect/connect.php');

//Global variables
$error_messages = array();//Array to save all errors
$data_error_messages = array();//Array to save all errors in input data
$connect = Database::getInstance()->dbc;
$pdf_pathLabelBill = false;//path to pdf. False while not created


//Start if have PO number
if (isset($_REQUEST['value']) && !empty($_REQUEST['value']) && isset($_REQUEST['bol']) && !empty($_REQUEST['bol']))
	{
		main($_REQUEST['value'], $_REQUEST['pro'], $_REQUEST['bol']);
		$connect = null;
	}

//Main function to execute functions kit depending on store name
function main($PONumber, $pro, $bol)
{
	global $error_messages;
	global $data_error_messages;
	global $connect;
	global $pdf_pathLabelBill;
	
	
	$array = getProductCode($connect, $PONumber);
	
	$array = boxes($array);
	$array = dublic($array);
    $array = getUPC($connect, $array);
	usort($array, 'ab');
	/*print_r('<pre>');
	print_r($array);
	print_r('</pre>');
	die();*/
	
	$recipientArray = recipientArray($array);
	$FedexServiceType = extrFedexServiceType($array[0]['CARRIERNAME']);
	$units = ceil($array[0]['WEIGHT'] / 2500);
	$description = array();
	$descriptionHtml = '';
				foreach ($array as $arr0)
				{
					if (!in_array($arr0['Descr'],$description))
					{
						$description[] = $arr0['Descr'];
						$descriptionHtml.=$arr0['Descr'].'<br>';
					}
				}
	$pdf_pathLabelBill = shipToFedex($descriptionHtml, $units, $FedexServiceType, $PONumber, $bol, $array[0]['WEIGHT'], $array[0]['boxes'], $recipientArray);
	/*print_r($pdf_pathLabelBill);
	die();*/
	send_error();
	
	return send_error();
}

function recipientArray($array){
	$memo = explode(",", $array[0]['Memo']);
	if (isset($memo[1]) && !empty($memo[1])) $customer = $memo[1]; else $customer = '';
	if ($array[0]['RESIDENTIAL'] == '1') $RESIDENTIAL = true; else $RESIDENTIAL = false;
	$ShipAddress_PostalCode = extrCountryCode( $array[0]['ShipAddress_PostalCode']);
	
	if (!isset($customer) || empty($customer))
	{
		error_msg("PersonName missing!");
		send_error();
	}
	if (!isset($array[0]['ShipAddress_Addr1']) || empty($array[0]['ShipAddress_Addr1']))
	{
		error_msg("CompanyName missing!");
		send_error();
	}
	if (!isset($array[0]['FOB']) || empty($array[0]['FOB']))
	{
		error_msg("PhoneNumber missing!");
		send_error();
	}
	if (!isset($array[0]['ShipAddress_Addr2']) || empty($array[0]['ShipAddress_Addr2']))
	{
		error_msg("StreetLines missing!");
		send_error();
	}
	if (!isset($array[0]['ShipAddress_City']) || empty($array[0]['ShipAddress_City']))
	{
		error_msg("City missing!");
		send_error();
	}
	if (!isset($array[0]['ShipAddress_State']) || empty($array[0]['ShipAddress_State']))
	{
		error_msg("StateOrProvinceCode missing!");
		send_error();
	}
	if (!isset($array[0]['ShipAddress_PostalCode']) || empty($array[0]['ShipAddress_PostalCode']))
	{
		error_msg("PostalCode missing!");
		send_error();
	}
	if (!isset($ShipAddress_PostalCode) || empty($ShipAddress_PostalCode))
	{
		error_msg("CountryCode missing!");
		send_error();
	}
	if ((!isset($RESIDENTIAL) || empty($RESIDENTIAL)) && $RESIDENTIAL != '0')
	{
		error_msg("RESIDENTIAL missing!");
		send_error();
	}
	
	$recipient = array(
		'Contact' => array(
			'PersonName' => $customer,
			'CompanyName' => $array[0]['ShipAddress_Addr1'],
			'PhoneNumber' => $array[0]['FOB']
		),
		'Address' => array(
			'StreetLines' => array($array[0]['ShipAddress_Addr2']),
			'City' => $array[0]['ShipAddress_City'],
			'StateOrProvinceCode' => $array[0]['ShipAddress_State'],
			'PostalCode' => $array[0]['ShipAddress_PostalCode'],
			'CountryCode' => $ShipAddress_PostalCode,
			'Residential' => $RESIDENTIAL
		)
	);
	return $recipient;	                                    
}

//is set pro and bol
function issetProBol($pro, $bol)
{
	if ((!isset($pro) || !isset($bol)) || (empty($pro) || empty($bol))  || ($pro == ' ' || $bol == ' '))
		{
			error_msg("PRO# and INVOICE# data needed!");
			send_error();
		} else return true;
}

//Sort array
function ab($a,$b)
{
	return ($a['boxNumber']-$b['boxNumber']);
}

//Fills an array with error messages
function error_msg($msg)
{
	global $error_messages;
	$error_messages[] = $msg;
}

//Fills an array with errors in input data
function data_error_msg($type, $id, $msg)
{
	global $data_error_messages;
	$row = array('type' => $type, 'id' => $id, 'msg' => $msg);
	$data_error_messages[] = $row;
}

//Generate list of error messages
function formErrors($error_messages)
{
	if (isset($error_messages) && !empty($error_messages))
	{
		$mes_text = "<b><p style='font-size: 12pt; color: red;'>Fedex Label and BOL</p></b>";
		foreach($error_messages as $message){
			$mes_text.= "<b><p style='font-size: 10pt; color: black;'>".$message."</p></b>";
		}
		return $mes_text;
	} else return false;
}

//Generate table with errors in input data
function formDataErrors($data_error_messages)
{
	if (isset($data_error_messages) && !empty($data_error_messages))
	{
		$mes_text = "<b><p style='font-size: 12pt; color: red;'>Fedex Label and BOL. Detailed problems:</p></b><p><table border='1' style='font-size: 12pt; color: black;'>
		<tr>
			<th>type</th>
			<th>data</th>
			<th>problem</th>
		</tr>";
		foreach($data_error_messages as $message){
			$mes_text.= "<tr>
			<td>".$message['type']."</td>
			<td>".$message['id']."</td>
			<td>".$message['msg']."</td>
			</tr>";
		}
		$mes_text.= "</table></p>";
		return $mes_text;
	} else return false;
}

//Returns array to ajax query with errors and pathes to pdf files and terminates script
function send_error()
{
	global $error_messages;
	global $data_error_messages;
	global $pdf_pathLabelBill;
	try
	{
		$formErrors = (string)formErrors($error_messages);
		$formDataErrors = (string)formDataErrors($data_error_messages);
		if ($pdf_pathLabelBill['path'] && isset($pdf_pathLabelBill['path']) && !empty($pdf_pathLabelBill['path']))
		{
			$html = (string)$pdf_pathLabelBill['path'];
		} else 
		{
			$html = 'false';
		}
		
		if ($pdf_pathLabelBill['path2'] && isset($pdf_pathLabelBill['path2']) && !empty($pdf_pathLabelBill['path2']))
		{
			$html2 = (string)$pdf_pathLabelBill['path2'];
		} else 
		{
			$html2 = 'false';
		}
		
		if ($pdf_pathLabelBill['pro'] && isset($pdf_pathLabelBill['pro']) && !empty($pdf_pathLabelBill['pro']))
		{
			$htmlPro = (string)$pdf_pathLabelBill['pro'];
		} else 
		{
			$htmlPro = 'false';
		}
		$result = array(
		'path' => $html,
		'path2' => $html2,
		'pro' => $htmlPro,
		'er' => $formErrors,
		'DataEr' => $formDataErrors
		);
		$result = json_encode($result);
		print_r($result);
	} catch (Exception $e){
		echo $e->getMessage();
	}
	die();
}
?>