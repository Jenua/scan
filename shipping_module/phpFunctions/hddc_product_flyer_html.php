<?php
//PHP configuration
ini_set('max_execution_time', 7200);
ini_set("memory_limit", "2000M");

//start timer
//$start_time = microtime(true);   //comment this out!!!

//Include files
//Root path
$path = $_SERVER['DOCUMENT_ROOT'];

//DB connection class
require_once($path.'/db_connect/connect.php');

//QR Code library
require_once($path.'/shipping_module/Include/phpqrcode/qrlib.php');

//Twig template engine
require_once($path.'/shipping_module/Include/Twig/Autoloader.php');

if (isset($_REQUEST['po']) && !empty($_REQUEST['po']))
{
    // Get PONumber and RefNumber from global array
    $po = $_REQUEST['po'];
    
    // In case of single product generation check if we have product id
    $TxnLineID = false;
    if(isset($_REQUEST['TxnLineID']) && !empty($_REQUEST['TxnLineID']))
    {
        $TxnLineID = $_REQUEST['TxnLineID'];
    }
    
    //Initialize Twig
    Twig_Autoloader::register();
    $loader = new Twig_Loader_Filesystem('../templates');
    $twig = new Twig_Environment($loader, array(
        'cache'       => 'compilation_cache',
        'auto_reload' => true
    ));
    
    //Get DB data for this order
    $product_data = getProductData($po, $TxnLineID);
    $item_data = getItemData($po, $TxnLineID);
    
    //Check if data exist
    if($product_data && $item_data)
    {
        //Assign drawing and qr code image for each product in base64 format
        foreach($product_data as $key => $row)
        {
            $product_data[$key]['qr_code'] = get_qr_code($row['UID_9']);
            $product_data[$key]['drawing'] = get_drawing($row['drawing']);
        }

        //Save appropriate items to products
        $product_data = mergeItemsToProduct($product_data, $item_data);
        //Clear some memory
        unset($item_data);

        //Array to store file names
        $files_generated = [];

        //Loop through products, generate files and save it to disk, and save file name to array
        foreach($product_data as $row)
        {
            $html = $twig->render('flyerHDDC.html', array('data' => $row));
            file_put_contents("../Pdf/".$row['PONumber']."flyer_".$row['TxnLineID']."_date(".date('Y-m-d').").html", $html);
            $files_generated[]= $row['PONumber']."flyer_".$row['TxnLineID']."_date(".date('Y-m-d').").html";
        }

        //Encode to JSON format
        $files_generated = json_encode($files_generated);

        //Return JSON
        print_r($files_generated);    //uncomment this!!!

        //Stop timer
        //$time_elapsed_secs = microtime(true) - $start_time;   //comment this out!!!

        //Display timer result
        //echo "<br>Done! Time: ".date("H:i:s",$time_elapsed_secs)."<br>";   //comment this out!!!
    } else 
    {
        $result = "Can not get products or items data to generate flyers.";
        $result = json_encode($result);
        print_r($result);
    }
} else 
{
    $result = "Can not get PONumber or RefNumber to generate flyers.";
    $result = json_encode($result);
    print_r($result);
}

function mergeItemsToProduct($product_data, $item_data)
{
    foreach($product_data as $p_key => $p_row)
    {
        foreach($item_data as $i_key => $i_row)
        {
            if($p_row['TxnLineID'] == $i_row['GroupIDKEY'])
            {
                $item_data[$i_key]['ItemRef_FullName'] = extrSinglePc($i_row['ItemRef_FullName']);
                $product_data[$p_key]['item_data'][]=$item_data[$i_key];
            }
        }
    }
    return $product_data;
}

function getProductData($po, $TxnLineID)
{
    $conn = Database::getInstance()->dbc;
    
    // if isset product id
    $query_part = "";
    if($TxnLineID) $query_part = "AND groupdetail.[TxnLineID] = ".$conn->quote($TxnLineID);
    
    $query = "SELECT distinct 
	shiptrack.RefNumber,   
	shiptrack.PONumber,	   
	shiptrack.CustomerRef_FullName,	
	groupdetail.ItemGroupRef_FullName,
	groupdetail.[Quantity],
        groupdetail.[TxnLineID],
	[DL_valid_SKU].UPC,
	[DL_valid_SKU].HD_SKU,
        [DL_valid_SKU].SKU,
        [DL_valid_SKU].[Box_Count],
	[item_info].[name] as [collection_name],
	[item_info].[drawing],
	[item_info].[dimensions],
	CAST(groupdetail.[Quantity] AS INT)*CAST([DL_valid_SKU].[Box_Count] AS INT) AS QTY_BOX,
        left(Replace(CONVERT(varchar(255), NEWID()),'-',''),(9)) AS UID_9
	FROM [groupdetail]
		INNER JOIN shiptrack ON shiptrack.TxnID = groupdetail.IDKEY
		INNER JOIN [dbo].[DL_valid_SKU] ON [DL_valid_SKU].[QB_SKU] = groupdetail.[ItemGroupRef_FullName]
		INNER JOIN [dbo].[item_info] ON [DL_valid_SKU].[SKU] = [item_info].[sku]
	WHERE shiptrack.PONumber = ".$conn->quote($po)." 
        ".$query_part;
    
    $result = $conn->prepare($query);
    $result->execute();
    $result = $result->fetchAll(PDO::FETCH_ASSOC);
    if(!empty($result)) return $result; else return false;
}

function getItemData($po, $TxnLineID)
{
    $conn = Database::getInstance()->dbc;
    
    // if isset product id
    $query_part = "";
    if($TxnLineID) $query_part = "AND groupdetail.[TxnLineID] = ".$conn->quote($TxnLineID);
    
    $query = "SELECT distinct 
        [groupdetail].[TxnLineID]
      ,[groupdetail].[ItemGroupRef_FullName]
      ,[groupdetail].[Prod_desc]
      ,[groupdetail].[Quantity]
	  ,[linedetail].[TxnLineID] as [item_id]
	  ,[linedetail].[ItemRef_FullName]
	  ,[linedetail].[quantity] as [item_quantity]
          ,[linedetail].[GroupIDKEY]
	  ,[shiptrack].[PONumber]
  FROM [dbo].[groupdetail]
  LEFT JOIN [dbo].[linedetail] ON [groupdetail].[TxnLineID] = [linedetail].[GroupIDKEY]
  LEFT JOIN [dbo].[shiptrack] ON [groupdetail].[IDKEY] = [shiptrack].[TxnID]
  WHERE shiptrack.PONumber = ".$conn->quote($po)."
  and
						 (
						  [linedetail].ItemRef_FullName not like '%Price-Adjustment%' 
						  and [linedetail].ItemRef_FullName not like '%Freight%'
						  and [linedetail].ItemRef_FullName not like '%warranty%' 
						  and [linedetail].ItemRef_FullName is not NULL
						  and [linedetail].ItemRef_FullName not like '%Subtotal%'
						  and [linedetail].ItemRef_FullName not like '%IDSC-10%'
						  and [linedetail].ItemRef_FullName not like '%DISCOUNT%'
						  and [linedetail].ItemRef_FullName not like '%SPECIAL ORDER4%'
						  and [linedetail].ItemRef_FullName not like '%Manual%'
						  ) 
        ".$query_part;
    
    $result = $conn->prepare($query);
    $result->execute();
    $result = $result->fetchAll(PDO::FETCH_ASSOC);
    if(!empty($result)) return $result; else return false;
}

function get_qr_code($uid)
{
	try{
		$value = 'http://dreamline.com/warranty/?SRVID='.$uid;
                ob_start();
                    QRCode::png($value, null);
                    $result = base64_encode( ob_get_contents() );
                ob_end_clean();
		return $result;
	} catch (Exception $e){
            $result = "Error. Can not generate QR code. ".$e->getMessage();
            $result = json_encode($result);
            print_r($result);
            die();
	}
}

function get_drawing($link)
{
    if($link) $link = "../drawings/".$link; else $link = "../drawings/spacer.png";
    if (!file_exists($link)) {
        $link = "../drawings/spacer.gif";
    }
    $drawing = file_get_contents($link);
    $result = base64_encode( $drawing );
    return $result;
}

function extrSinglePc($value)
{
    $pieces = explode(":", $value);
    $index = count($pieces)-1;
    $ProductCode = $pieces[$index];
    if (isset($ProductCode) && !empty($ProductCode))
    {
	$value = $ProductCode;
    }
    return $value;
}