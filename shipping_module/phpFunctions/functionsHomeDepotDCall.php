<?php
//die('stop');
ini_set('max_execution_time', 7200);
ini_set("memory_limit", "1000M");
//Include files
require_once('../Include/barcode/class/BCGFontFile.php');
require_once('../Include/barcode/class/BCGcode39.barcode.php');
require_once('../Include/barcode/class/BCGcode128.barcode.php');
require_once('../Include/barcode/class/BCGColor.php');
require_once('../Include/barcode/class/BCGDrawing.php');
require_once('../Include/barcode/class/BCGpdf417.barcode2d.php');
require_once('../Include/barcode/class/BCGi25.barcode.php');
require_once('../Include/barcode/class/BCGgs1128.barcode.php');
require_once('../Include/barcode/class/BCGupca.barcode.php');
require_once('../Include/fpdf17/fpdf.php');
require_once('../Include/tcpdf/tcpdf.php');
require_once('functionsPDF.php');
require_once('functionsDB.php');
require_once('functionsBar.php');
require_once('functionsExtr.php');
$path = $_SERVER['DOCUMENT_ROOT'];
require_once($path.'/db_connect/connect.php');

//Global variables
$error_messages = array();//Array to save all errors
$data_error_messages = array();//Array to save all errors in input data
$connect = Database::getInstance()->dbc;
$pdf_patheLabel = false;//path to pdf. False while not created


//Start if have PO number
if (isset($_REQUEST['po']) && !empty($_REQUEST['po']) )
	{
                $po = $_REQUEST['po'];                
                $query_groupdetail_DL= "
                SELECT * FROM
                (
                SELECT 
                                                [groupdetail].[TxnLineID]
                                          ,[groupdetail].[ItemGroupRef_FullName]
                                          ,[groupdetail].[Prod_desc]
                                          ,[groupdetail].[Quantity]
                                        ,[DL_valid_SKU].[SHIP_METHOD]
                                FROM [dbo].[groupdetail]
                                LEFT JOIN [shiptrack] ON [groupdetail].[IDKEY] = [shiptrack].[TxnID]
                                LEFT JOIN [DL_valid_SKU] ON [groupdetail].[ItemGroupRef_FullName] = [DL_valid_SKU].[QB_SKU]
                                WHERE [shiptrack].[PONumber] = '".$po."'
                                UNION ALL
                                SELECT 
                                                [manually_groupdetail].[TxnLineID]
                                          ,[manually_groupdetail].[ItemGroupRef_FullName]
                                          ,[manually_groupdetail].[Prod_desc]
                                          ,[manually_groupdetail].[Quantity]
                                        ,[DL_valid_SKU].[SHIP_METHOD]
                                FROM [dbo].[manually_groupdetail]
                                LEFT JOIN [manually_shiptrack] ON [manually_groupdetail].[IDKEY] = [manually_shiptrack].[TxnID]
                                LEFT JOIN [DL_valid_SKU] ON [manually_groupdetail].[ItemGroupRef_FullName] = [DL_valid_SKU].[QB_SKU]
                                WHERE [manually_shiptrack].[PONumber] = '".$po."'
                ) a
                GROUP BY [TxnLineID], [ItemGroupRef_FullName], [Prod_desc], [Quantity], [SHIP_METHOD]";

                $result_groupdetail_DL = exec_query($connect, $query_groupdetail_DL);
                $result_groupdetail_DL = $result_groupdetail_DL->fetchAll(PDO::FETCH_ASSOC);
                
                if($result_groupdetail_DL) {
                    foreach ($result_groupdetail_DL as $key1 => $result) {
                            try{
                                main_label($po, $result['TxnLineID']);
                                main_sku_upc($po, $result['TxnLineID']);                                
                            } catch(Exception $e) { 
                                //TODO
                            }
                            
                    }
                }
		$connect = null;
	} else{
		error_msg('Can not get PO ');
		send_error();
	}

function main_label($PONumber, $txnlineid)
{
	global $error_messages;
	global $data_error_messages;
	global $connect;
	global $pdf_patheLabel;
	
		$array = getProductCodeHomeDepotDCLabel($connect, $PONumber, $txnlineid);
		$qty = $array[0]['Quantity'];
		for($i = 0; $i < $qty; $i++)
		{
			if(!empty($array[0]['collection_name']) && !empty($array[0]['drawing']))
			{
				$array[$i] = $array[0];
				$value = getToken(9);
				$array[$i]['code'] = $value;
				$array[$i]['qr_code'] = gen_qrcode($value);
			} else{
				error_msg("Error. Collection name or drawing is empty for this product.");
				send_error();
			}
		}
		
		foreach($array as $key => $row)
		{
			$array[$key]['SKU'] = remove_G($row['SKU']);
			$array[$key]['ItemGroupRef_FullName'] = remove_G($row['ItemGroupRef_FullName']);
			if(isset($row['dimensions']) && !empty($row['dimensions']))
			{
				$dimensions = explode(",", $row['dimensions']);
				foreach($dimensions as $key2 => $dimension)
				{
					$dimensions[$key2] = trim($dimension);
				}
				$array[$key]['dimensions'] = $dimensions;
			} else
			{
				$array[$key]['dimensions'] = null;
			}
		}
		
		$array2 = getProductCodeHomeDepotDCpickinglist($connect, $PONumber, $txnlineid);
		foreach($array2 as $key => $row)
		{
			$array2[$key]['ItemGroupRef_FullName'] = remove_G($row['ItemGroupRef_FullName']);
		}
		
		$pdf_patheLabel= homeDCPdf_label($array, $array2);
		send_error();
	
	return send_error();
}

function main_sku_upc($PONumber, $txnlineid)
{
	global $error_messages;
	global $data_error_messages;
	global $connect;
	global $pdf_patheLabel;
		
		$array2 = getProductCodeHomeDepotDCpickinglist($connect, $PONumber, $txnlineid);
	
		$array = getProductCodeHomeDepotDCLabel($connect, $PONumber, $txnlineid);
		$qty = $array[0]['Quantity'];
		for($i = 0; $i < $qty; $i++)
		{
			$array[$i] = $array[0];
		}
		foreach($array as $key => $row)
		{
			$array[$key]['SKU'] = remove_G($row['SKU']);
			$array[$key]['ItemGroupRef_FullName'] = remove_G($row['ItemGroupRef_FullName']);
			if(!empty($row['collection_name']) && !empty($row['drawing']))
			{
				/*$array[$key]['sku_barcode'] = gen_imageCode128($row['SKU']);*/
				$array[$key]['upc_a_barcode'] = gen_imageUPCA_scale($row['UPC']);
			} else{
				error_msg("Error. Collection name or drawing is empty for this product.");
				send_error();
			}
		}
		foreach($array2 as $key => $row)
		{
			$array2[$key]['ItemGroupRef_FullName'] = remove_G($row['ItemGroupRef_FullName']);
		}
		
		/*print_r('<pre>');
		print_r($array);
		print_r('</pre>');
		die();*/
		
		$pdf_patheLabel= homeDCPdf_upc_sku($array, $array2);
		send_error();
	
	return send_error();
}

//Sort array
function ab($a,$b)
{
	return ($a['boxNumber']-$b['boxNumber']);
}

//Fills an array with error messages
function error_msg($msg)
{
	global $error_messages;
	$error_messages[] = $msg;
}

//Fills an array with errors in input data
function data_error_msg($type, $id, $msg)
{
	global $data_error_messages;
	$row = array('type' => $type, 'id' => $id, 'msg' => $msg);
	$data_error_messages[] = $row;
}

//Generate list of error messages
function formErrors($error_messages)
{
	if (isset($error_messages) && !empty($error_messages))
	{
		$mes_text = "<b><p style='font-size: 12pt; color: red;'>Label:</p></b>";
		foreach($error_messages as $message){
			$mes_text.= "<b><p style='font-size: 10pt; color: black;'>".$message."</p></b>";
		}
		return $mes_text;
	} else return false;
}

//Generate table with errors in input data
function formDataErrors($data_error_messages)
{
	if (isset($data_error_messages) && !empty($data_error_messages))
	{
		$mes_text = "<b><p style='font-size: 12pt; color: red;'>Label. Detailed problems:</p></b><p><table border='1' style='font-size: 12pt; color: black;'>
		<tr>
			<th>type</th>
			<th>data</th>
			<th>problem</th>
		</tr>";
		foreach($data_error_messages as $message){
			$mes_text.= "<tr>
			<td>".$message['type']."</td>
			<td>".$message['id']."</td>
			<td>".$message['msg']."</td>
			</tr>";
		}
		$mes_text.= "</table></p>";
		return $mes_text;
	} else return false;
}

//Returns array to ajax query with errors and pathes to pdf files and terminates script
function send_error()
{
	
        global $error_messages;
	global $data_error_messages;
	global $pdf_patheLabel;
	try
	{
		$formErrors = (string)formErrors($error_messages);
		$formDataErrors = (string)formDataErrors($data_error_messages);
		if ($pdf_patheLabel && isset($pdf_patheLabel) && !empty($pdf_patheLabel))
		{
			foreach($pdf_patheLabel as $key => $pdf_path)
			{
				$key_display = $key+1;
				$html = (string)$pdf_path;
				$html = urlencode($html);
				$ref_path = '/shipping_module/newTab.php?value='.$html.'&random='.getToken(9);
				$result = '<a target="_blank" href="'.$ref_path.'">Display PDF #'.$key_display.'</a>';
				//print_r($result.'<br><br>');
			}
		} else {
			print_r("Errors:".$formErrors);
			print_r("Errors:".$formDataErrors);
		}
	} catch (Exception $e){
		echo $e->getMessage();
                echo "<br />";
	}
	//die();
        
        
}

function exec_query($conn, $query)
{
	try{
		$result = $conn->prepare($query);
		$result->execute();
		return $result;
	}
	catch(PDOException $e)
	{
		print_r($e->getMessage());
	}
}
?>