<?php
$path = $_SERVER['DOCUMENT_ROOT'];
require_once($path.'/db_connect/connect.php');
date_default_timezone_set('America/New_York');


	if (isset($_REQUEST['po']) && !empty($_REQUEST['po'])
		&& isset($_REQUEST['reason'])
		&& isset($_REQUEST['reasonNote'])
		&& isset($_REQUEST['userName']))
	{
		$po = $_REQUEST['po'];
		$reason = $_REQUEST['reason'];
		$reasonNote = $_REQUEST['reasonNote'];
		$reasonNote = str_replace("'", "''", $reasonNote);
		$reasonNote = htmlspecialchars($reasonNote);
		$userName = $_REQUEST['userName'];

		$conn = Database::getInstance()->dbc;
		
		$query0 = "SELECT [Orders_combined].[PONumber], [Orders_combined].[Combined_into_POnumber]
						  FROM [dbo].[Orders_combined]
						  INNER JOIN [dbo].[manually_shiptrack] ON [Orders_combined].[Combined_into_RefNumber] = [manually_shiptrack].[RefNumber]
						  WHERE [Combined_into_POnumber] IN
							(
								SELECT [Combined_into_POnumber]
								FROM [dbo].[Orders_combined]
								WHERE 
								[Orders_combined].[PONumber] = '".$po."' 
								OR [Orders_combined].[Combined_into_POnumber] = '".$po."'
							)";
		
		$res0 = execQuery($conn, $query0);
		if(isset($res0) && !empty($res0))
		{
			doRequest($res0[0]['Combined_into_POnumber'], $reason, $reasonNote, $userName);
			foreach($res0 as $value)
			{
				doRequest($value['PONumber'], $reason, $reasonNote, $userName);
			}
		} else
		{
			doRequest($po, $reason, $reasonNote, $userName);
		}
		
		$conn = null;
	}

function doRequest($po, $reason, $reasonNote, $userName)
{
	$myCurl = curl_init();
	$actual_link = "http://$_SERVER[HTTP_HOST]";
	curl_setopt($myCurl, CURLOPT_RETURNTRANSFER, TRUE);
	curl_setopt($myCurl, CURLOPT_HEADER, 0);
    curl_setopt($myCurl, CURLOPT_FOLLOWLOCATION, 1);
	
	curl_setopt($myCurl, CURLOPT_URL, $actual_link.'/shipping_module/phpFunctions/refreshOrderHandler.php');
	curl_setopt($myCurl, CURLOPT_POST, TRUE);
	curl_setopt($myCurl, CURLOPT_POSTFIELDS, http_build_query(array('po' => $po, 'reason' => $reason, 'reasonNote' => $reasonNote, 'userName' => $userName)));
	
	$response = curl_exec($myCurl);
	curl_close($myCurl);

	print_r($response);
}

function execQuery($conn, $query)
{
	try{
		$result = $conn->prepare($query);
		$result->execute();
		$result = $result->fetchAll(PDO::FETCH_ASSOC); 
		return $result;
	}
	catch(PDOException $e)
	{
		print_r($e->getMessage());
	}
}
?>