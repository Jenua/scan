<?php
//Generate Sears Manifest pdf file
function searsManifest2($arrays)
{

	try{
		$titleDate = date("n_j_Y");
		$pdf_path = "../PdfManifest/".$titleDate."PdfManifest.pdf";
		$pdf_path2 = "PdfManifest/".$titleDate."PdfManifest.pdf";
		$result = array();
	
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		$pdf->SetPrintHeader(false);
		$pdf->SetPrintFooter(false);
		$pdf->SetMargins(5, 5);
		$pdf->SetAutoPageBreak(TRUE, 1);
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
		$pdf->AddPage();
		$pdf->SetFont('helvetica', '', 8);
		
		date_default_timezone_set('America/New_York');
		$todaysDate = date("n/j/Y");
		$orderLines = '';
		$totalBoxes = 0;
		$totalWEIGHT = 0;
		foreach ($arrays as $key => $array)
		{
			$description = array();
				$descriptionHtml = '';
				foreach ($array as $arr0)
				{
					if (!in_array($arr0['Descr'],$description))
					{
						$description[] = $arr0['Descr'];
						$descriptionHtml.=$arr0['Descr'].'<br>';
					}
				}
			$Day = date("l");
			$units = ceil($array[0]['WEIGHT'] / 2500);
			$totalQty = 0;
			$totalBoxes = $totalBoxes + $array[0]['boxes'];
			$totalWEIGHT = $totalWEIGHT + $array[0]['WEIGHT'];
			$phoneNumberWODashes = str_replace('-', '', $array[0]['FOB']);
			foreach ($array as $arr)
			{
				$totalQty = $totalQty + $arr['quantity'];
			}
			$orderLines.= '
				<tr>
					<td>
						683
					</td>
					<td>
						'.$array[0]['PONumber'].'
					</td>
					<td>
						'.$array[0]['validSKU'].'
					</td>
					<td>
						'.$array[0]['ProductCode'].'
					</td>
					<td>
						'.$totalQty.'
					</td>
					<td>
						'.$array[0]['boxes'].'
					</td>
					<td>
						'.$array[0]['WEIGHT'].'
					</td>
				</tr>
			';
		}
		
		$tbl = '
			<table border="1" cellpadding="2">
				<tr>
					<td>
						<table border="0" cellpadding="2">
							<tr>
								<td colspan="6" align="center">
									SHIPMENT MANIFEST
								</td>
							</tr>
							<tr>
								<td colspan="6">
								</td>
							</tr>
							<tr>
								<td>
									Ship From
								</td>
								<td colspan="2">
									Bath Authority, LLC<br>
									75 Hawk Road<br>
									Warminster, PA 18974
								</td>
								<td>
									Ship To
								</td>
								<td colspan="2">
									Demar Logistics<br>
									376 Lies Road<br>
									Carol Stream, IL 60188
								</td>
							</tr>
							<tr>
								<td colspan="6">
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td align="center">
						Bill of Lading #  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Trailer # N/A
					</td>
				</tr>
			</table>
			<table border="1" cellpadding="2" align="center">
				<tr>
					<td>
						Sears<br>
						Dept #:
					</td>
					<td>
						PO Number:
					</td>
					<td>
						Sears<br>
						Item/SKU #:
					</td>
					<td>
						Vendor<br>
						Item/Model #:
					</td>
					<td>
						Total Item<br>
						Qty:
					</td>
					<td>
						Total<br>
						Cartons:
					</td>
					<td>
						Total<br>
						Weight:
					</td>
				</tr>
				'.$orderLines.'
				<tr>
					<td colspan="5" align="right">
						Totals:
					</td>
					<td>
						'.$totalBoxes.'
					</td>
					<td>
						'.$totalWEIGHT.'
					</td>
				</tr>
			</table>
		';
		
		$pdf->writeHTML($tbl, true, false, false, false, '');
		$pdf->Output($pdf_path, 'F');
		$result[] = $pdf_path2;
			
			if (!empty($result))
			{
				return $result;
			} else
			{
				error_msg("Can not generate any pdf!");
				send_error();
			}
	}catch (Exception $e) {
		error_msg($e->getMessage());
		send_error();
	}
}

//Generate Sears Manifest pdf file
function searsManifest($arrays)
{

	try{
		$titleDate = date("n_j_Y");
		$pdf_path = "../PdfManifest/".$titleDate."PdfManifest.pdf";
		$pdf_path2 = "PdfManifest/".$titleDate."PdfManifest.pdf";
		$result = array();
	
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		$pdf->SetPrintHeader(false);
		$pdf->SetPrintFooter(false);
		$pdf->SetMargins(5, 5);
		$pdf->SetAutoPageBreak(TRUE, 1);
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
		$pdf->AddPage();
		$pdf->SetFont('helvetica', '', 8);
		
		date_default_timezone_set('America/New_York');
		$todaysDate = date("n/j/Y");
		$orderLines = '';
		$totalBoxes = 0;
		$totalWEIGHT = 0;
		foreach ($arrays as $key => $array)
		{
			$description = array();
				$descriptionHtml = 'PO#: '.$array[0]['PONumber'].'; PART#: '.$array[0]['PartNumber'].';<br>';
				foreach ($array as $arr0)
				{
					if (!in_array($arr0['Descr'],$description))
					{
						$description[] = $arr0['Descr'];
						$descriptionHtml.=$arr0['Descr'].'<br>';
					}
				}
			$Day = date("l");
			$units = ceil($array[0]['WEIGHT'] / 2500);
			$totalQty = 0;
			$totalBoxes = $totalBoxes + $array[0]['boxes'];
			$totalWEIGHT = $totalWEIGHT + $array[0]['WEIGHT'];
			$phoneNumberWODashes = str_replace('-', '', $array[0]['FOB']);
			foreach ($array as $arr)
			{
				$totalQty = $totalQty + $arr['quantity'];
			}
			$orderLines.= '
				<tr>
					<td colspan="2">
						'.$array[0]['ShipAddress_Addr1'].'
					</td>
					<td colspan="2">
						'.$array[0]['ShipAddress_Addr2'].'
					</td>
					<td>
						'.$array[0]['ShipAddress_City'].'
					</td>
					<td>
						'.$array[0]['ShipAddress_State'].'
					</td>
					<td>
						'.$array[0]['ShipAddress_PostalCode'].'
					</td>
					<td colspan="2">
						'.$phoneNumberWODashes.'
					</td>
					<td colspan="2">
						'.$descriptionHtml.'
					</td>
					<td colspan="2">
						'.$array[0]['PONumber'].'
					</td>
					<td>
						'.$array[0]['boxes'].'
					</td>
					<td>
						'.$units.'
					</td>
					<td>
						'.$array[0]['WEIGHT'].'
					</td>
				</tr>
			';
		}
		
		$tbl = '
			<table align="center">
				<tr>
					<td colspan="2">
						<img id="img1" src="../Include/pictures/DEMARLogisticsInc.png">
					</td>
					<td>
						
					</td>
					<td colspan="3">
						<img id="img1" src="../Include/pictures/searsHoldings.png">
					</td>
					<td>
						
					</td>
					<td>
						<img id="img1" src="../Include/pictures/SearsRedCurve.png">
					</td>
					<td>
						<img id="img1" src="../Include/pictures/KmartIDiag.png">
					</td>
				</tr>
				<tr>
					<td colspan="9" align="center" style="font-size: 16px; color: #000080;">
						Request for Transportation
					</td>
				</tr>
			</table>
			<br>
			<br>
			
			<table>
				<tr>
					<td colspan="3">
						<table border="1" cellpadding="2">
							<tr align="right">
								<td style="background-color: silver;">
									Request Date: 
								</td>
								<td colspan="2" align="left">
									'.$todaysDate.'
								</td>
							</tr>
							<tr align="right">
								<td style="background-color: silver;">
									Vendor name: 
								</td>
								<td colspan="2" align="left">
									BathAuthority, LLC
								</td>
							</tr>
							<tr align="right">
								<td style="background-color: silver;">
									Address: 
								</td>
								<td colspan="2" align="left">
									75 Hawk Road
								</td>
							</tr>
							<tr align="right">
								<td style="background-color: silver;">
									City/State/Zip: 
								</td>
								<td colspan="2" align="left">
									Warminster, PA 18974
								</td>
							</tr>
							<tr align="right">
								<td style="background-color: silver;">
									Contact: 
								</td>
								<td align="left">
									
								</td>
								<td style="background-color: silver;" align="center">
									Assigned Ship Day
								</td>
							</tr>
							<tr align="right">
								<td style="background-color: silver;">
									Phone: 
								</td>
								<td align="left">
									866-731-8378
								</td>
								<td align="left">
									'.$Day.'
								</td>
							</tr>
						</table>
					</td>
					<td>
					</td>
					<td colspan="3">
						<table>
							<tr>
								<td style="color: red;">
									Notes
								</td>
							</tr>
							<tr>
								<td border="1">
									<br>
									<br>
									<br>
									<br>
									<br>
									<br>
									<br>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<br>
			<br>
			
			<table border="1" cellpadding="2" align="center">
				<tr style="background-color: silver;">
					<td colspan="2">
						Customer Name
					</td>
					<td colspan="2">
						Address
					</td>
					<td>
						City
					</td>
					<td>
						ST
					</td>
					<td>
						Zip
					</td>
					<td colspan="2">
						Phone<br><i>(No Dashes)</i>
					</td>
					<td colspan="2">
						Description
					</td>
					<td colspan="2">
						Order
					</td>
					<td>
						Pcs
					</td>
					<td>
						Skids
					</td>
					<td>
						Wgt
					</td>
				</tr>
				'.$orderLines.'
			</table>
			
			<table>
				<tr>
					<td style="color: red;">
						<u>Contact Information for Shipping</u>
					</td>
				</tr>
				<tr>
					<td style="font-size: 10px;">
						Email this form to the following contacts by Noon (central time) on day prior to pick up:
					</td>
				</tr>
				<tr>
					<td style="font-weight: bold;">
						TO:searsdispatchDG@demarlogistics.com
					</td>
				</tr>
				<tr>
					<td style="font-weight: bold;">
						CC:  jdoerr@demarlogistics.com, jamesb@demarlogistics.com, jmccabe@demarlogistics.com 
					</td>
				</tr>
			</table>
		';
		
		$pdf->writeHTML($tbl, true, false, false, false, '');
		$pdf->Output($pdf_path, 'F');
		$result[] = $pdf_path2;
			
			if (!empty($result))
			{
				return $result;
			} else
			{
				error_msg("Can not generate any pdf!");
				send_error();
			}
	}catch (Exception $e) {
		error_msg($e->getMessage());
		send_error();
	}
}
?>