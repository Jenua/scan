<?php
$conn = Database::getInstance()->dbc;
$qpo = $conn->quote($po);
$conn = null;

$query_settings = "SELECT * FROM ".GENERAL_SETTINGS." WHERE [app] = 'shipping_module' ORDER BY [group_name]";

//All costs
if($ref) $query_allcosts_part1 = "AND [ID] = '".$ref."'"; else $query_allcosts_part1 = "";
$query_allcosts = "SELECT [CARRIERNAME],[COST]
  FROM [dbo].[AllCosts]
  WHERE [PONUMBER] = ".$qpo." 
  ".$query_allcosts_part1."group by [CARRIERNAME],[COST];";



//shiptrack + manually_shiptrack
if($txn) $query_shiptrack_part1 = "AND shiptrack.[TxnID] = '".$txn."'"; else $query_shiptrack_part1 = "";
if($ref) $query_shiptrack_part2 = "AND shiptrack.[RefNumber] = '".$ref."'"; else $query_shiptrack_part2 = "";
if($txn) $query_shiptrack_part3 = "AND manually_shiptrack.[TxnID] = '".$txn."'"; else $query_shiptrack_part3 = "";
if($ref) $query_shiptrack_part4 = "AND manually_shiptrack.[RefNumber] = '".$ref."'"; else $query_shiptrack_part4 = "";
$query_shiptrack = "SELECT distinct
        CASE
            WHEN [ia].[Date] IS NOT NULL THEN 'negative'
            WHEN [ia_fd].[Date] IS NOT NULL THEN 'positive'
            ELSE 'not_calculated'
	END as [inventory_flag],
        [ia_fd].[Date] as [inventory],
    shiptrack.[PONumber]
      ,shiptrack.[RefNumber]
	  ,shiptrack.[CustomerRef_FullName] as Customer
	  ,shiptrack.[TimeCreated]
      ,shiptrack.[ShipAddress_Addr1] as Addr1
      ,shiptrack.[ShipAddress_Addr2] as Addr2
      ,shiptrack.[ShipAddress_Addr3] as Addr3
      ,shiptrack.[ShipAddress_City] as City
      ,shiptrack.[ShipAddress_State] as State
      ,shiptrack.[ShipAddress_PostalCode] as Zip
      ,shiptrack.[ShipAddress_Country] as Country
      ,shiptrack.[FOB] as Phone
      ,shiptrack.[ShipMethodRef_FullName] as Ship_method
      ,shiptrack.[IsManuallyClosed]
      ,shiptrack.[IsFullyInvoiced]
      ,shiptrack.[Memo]
	  ,-1 as [hide_order]
	  FROM [".DATABASE."].[dbo].[shiptrack]
            LEFT JOIN [dbo].[inventory_allocation] [ia] ON [ia].[PONumber] = [shiptrack].[PONumber] AND [ia].[RefNumber] = [shiptrack].[RefNumber]
            LEFT JOIN [dbo].[inventory_allocation_full_details] [ia_fd] ON [ia_fd].[PONumber] = [shiptrack].[PONumber] AND [ia_fd].[RefNumber] = [shiptrack].[RefNumber]
	  WHERE shiptrack.[PONumber] = ".$qpo." 
	  ".$query_shiptrack_part1."
	  ".$query_shiptrack_part2."
	  UNION ALL
	  SELECT distinct
          'not_calculated' as [inventory_flag],
          NULL as [inventory],
	  manually_shiptrack.[PONumber]
      ,manually_shiptrack.[RefNumber]
	  ,manually_shiptrack.[CustomerRef_FullName] as Customer
	  ,manually_shiptrack.[TimeCreated]
      ,manually_shiptrack.[ShipAddress_Addr1] as Addr1
      ,manually_shiptrack.[ShipAddress_Addr2] as Addr2
      ,manually_shiptrack.[ShipAddress_Addr3] as Addr3
      ,manually_shiptrack.[ShipAddress_City] as City
      ,manually_shiptrack.[ShipAddress_State] as State
      ,manually_shiptrack.[ShipAddress_PostalCode] as Zip
      ,manually_shiptrack.[ShipAddress_Country] as Country
      ,manually_shiptrack.[FOB] as Phone
      ,manually_shiptrack.[ShipMethodRef_FullName] as Ship_method
      ,manually_shiptrack.[IsManuallyClosed]
      ,manually_shiptrack.[IsFullyInvoiced]
      ,manually_shiptrack.[Memo]
	  ,manually_shiptrack.[hide_order]
	  FROM [".DATABASE."].[dbo].[manually_shiptrack]
	  WHERE manually_shiptrack.[PONumber] = ".$qpo." 
	  ".$query_shiptrack_part3."
	  ".$query_shiptrack_part4.";";
/*print_r('<pre>');
print_r($query_shiptrack);
print_r('</pre>');
die;
*/
//invoice
if($txn) $query_invoice_part1 = "AND [invoice].[TxnID] = '".$txn."'"; else $query_invoice_part1 = "";
$query_invoice = "SELECT 
		[invoice].[PONumber]
		,[invoice].[RefNumber] as [QB Invoice#]
	  ,[invoice].[CustomerRef_FullName] as Customer
	  ,[invoice].[TimeCreated]
      ,[invoice].[ShipAddress_Addr1] as Addr1
      ,[invoice].[ShipAddress_Addr2] as Addr2
      ,[invoice].[ShipAddress_Addr3] as Addr3
      ,[invoice].[ShipAddress_City] as City
      ,[invoice].[ShipAddress_State] as State
      ,[invoice].[ShipAddress_PostalCode] as Zip
      ,[invoice].[ShipAddress_Country] as Country
      ,[invoice].[FOB] as Phone
      ,[invoice].[ShipMethodRef_FullName] as Ship_method
      ,[invoice].[Memo]
	  ,[invoice].[other] as [Tracking #]
	  FROM [".DATABASE31."].[dbo].[invoice]
	  WHERE [invoice].[PONumber] = ".$qpo." 
	  ".$query_invoice_part1.";";

//salesorder
if($txn) $query_salesorder_part1 = "AND [salesorder].[TxnID] = '".$txn."'"; else $query_salesorder_part1 = "";
if($ref) $query_salesorder_part2 = "AND [salesorder].[RefNumber] = '".$ref."'"; else $query_salesorder_part2 = "";
$query_salesorder = "SELECT 
		[salesorder].[PONumber]
		,[salesorder].[RefNumber]
	,[salesorder].[CustomerRef_FullName] as Customer
	  ,[salesorder].[TimeCreated]
      ,[salesorder].[ShipAddress_Addr1] as Addr1
      ,[salesorder].[ShipAddress_Addr2] as Addr2
      ,[salesorder].[ShipAddress_Addr3] as Addr3
      ,[salesorder].[ShipAddress_City] as City
      ,[salesorder].[ShipAddress_State] as State
      ,[salesorder].[ShipAddress_PostalCode] as Zip
      ,[salesorder].[ShipAddress_Country] as Country
      ,[salesorder].[FOB] as Phone
      ,[salesorder].[ShipMethodRef_FullName] as [Actual Ship_method]
	  ,[salesorder].[TimeModified] as [Actual Modified Date]
	  ,[salesorder].[ShipDate] as [Actual ShipDate]
	  ,CONVERT(varchar(255), [salesorder].[Subtotal]) as [Actual Subtotal]
      ,[salesorder].[IsManuallyClosed]
      ,[salesorder].[IsFullyInvoiced]
      ,[salesorder].[Memo]
      ,[salesorder].[SalesRepRef_FullName]
	  FROM [".DATABASE31."].[dbo].[salesorder]
	  WHERE [salesorder].[PONumber] = ".$qpo." 
	  ".$query_salesorder_part1."
          ".$query_salesorder_part2.";";

//shiptrackwithlabel
if($ref) $query_shiptrackwithlabel_part1 = "AND stwl.[ID] = '".$ref."'"; else $query_shiptrackwithlabel_part1 = "";
$query_shiptrackwithlabel = "SELECT distinct 
    stwl.[PONUMBER] as PO
      ,stwl.[ID] as RefNumber
      ,stwl.[CARRIERNAME]
      ,stwl.[QUOTENUMBER]
      ,stwl.[DEALER]
      ,stwl.[FROMZIP]
      ,stwl.[TOZIP]
      ,stwl.[TRANSITTIME]
      ,stwl.[COST]
      ,stwl.[WEIGHT]
      ,stwl.[LIFTGATE]
      ,stwl.[PRO]
      ,stwl.[BOL] as [SM Invoice#]
      ,stwl.[SERIAL_REFERENCE]
      ,stwl.[RESIDENTIAL]
      ,stwl.[delivery_notification] 
      FROM ".SHIPTRACKWITHLABEL_TABLE." stwl
      WHERE stwl.[PONUMBER] = ".$qpo." 
	  ".$query_shiptrackwithlabel_part1.";";

/*print_r('<pre>');
print_r($query_shiptrackwithlabel);
print_r('</pre>');
die();*/

//groupdetail
if($ref) $query_groupdetail_DL_part1 = "AND [shiptrack].[RefNumber] = '".$ref."'"; else $query_groupdetail_DL_part1 = "";
if($ref) $query_groupdetail_DL_part2 = "AND [manually_shiptrack].[RefNumber] = '".$ref."'"; else $query_groupdetail_DL_part2 = "";
if($txn) $query_groupdetail_DL_part3 = "AND shiptrack.[TxnID] = '".$txn."'"; else $query_groupdetail_DL_part3 = "";
if($txn) $query_groupdetail_DL_part4 = "AND manually_shiptrack.[TxnID] = '".$txn."'"; else $query_groupdetail_DL_part4 = "";
$query_groupdetail_DL= "SELECT [RefNumber],
    [TxnLineID],
    [ItemGroupRef_FullName],
    [Prod_desc],
    [Quantity],
    [SHIP_METHOD],
    [UPC],
    [drawing],
    [master_cartons],
	[Related]
FROM
(SELECT 
				[shiptrack].[RefNumber]
                                ,[shiptrack].[PONumber]
			  ,[groupdetail].[TxnLineID]
			  ,[groupdetail].[ItemGroupRef_FullName]
			  ,[groupdetail].[Prod_desc]
			  ,[groupdetail].[Quantity]
			,[DL_valid_SKU].[SHIP_METHOD]
                        ,[DL_valid_SKU].[UPC]
                        ,[item_info].[drawing]
                        ,[DL_valid_SKU].[Box_Count] as [master_cartons]
			,a.[Related]
		FROM [dbo].[groupdetail]
		LEFT JOIN [shiptrack] ON [groupdetail].[IDKEY] = [shiptrack].[TxnID]
		LEFT JOIN [DL_valid_SKU] ON [groupdetail].[ItemGroupRef_FullName] = [DL_valid_SKU].[QB_SKU]
                LEFT JOIN [dbo].[item_info] ON [DL_valid_SKU].[SKU] = [item_info].[sku]
		LEFT JOIN [orders_statuses] ON [shiptrack].[PONumber] = [orders_statuses].[PONumber] AND [shiptrack].[RefNumber] = [orders_statuses].[RefNumber] AND [orders_statuses].[Type] = '1'		
		OUTER APPLY (
			SELECT TOP 1
				dl.[SKU] as [Related]
			FROM [dbo].[groupdetail] as gr
			LEFT JOIN [DL_valid_SKU] as dl ON gr.[ItemGroupRef_FullName] = dl.[QB_SKU]
			LEFT JOIN [fpt_related]  as fr ON dl.[SKU] = fr.[Related] AND fr.[Product] = [DL_valid_SKU].[SKU]
			WHERE gr.[IDKEY] = [shiptrack].[TxnID]
			  AND dl.[SKU] IS NOT NULL
			  AND dl.[SKU] != fr.[Product]
			  AND [orders_statuses].[isActive] = '1'
		) a
		
		WHERE [shiptrack].[PONumber] = ".$qpo." 
		".$query_groupdetail_DL_part1."
		".$query_groupdetail_DL_part3."
		UNION ALL
		SELECT 
				[manually_shiptrack].[RefNumber]
                                ,[manually_shiptrack].[PONumber]
			  ,[manually_groupdetail].[TxnLineID]
			  ,[manually_groupdetail].[ItemGroupRef_FullName]
			  ,[manually_groupdetail].[Prod_desc]
			  ,[manually_groupdetail].[Quantity]
			,[DL_valid_SKU].[SHIP_METHOD]
                        ,[DL_valid_SKU].[UPC]
                        ,[item_info].[drawing]
                        ,[DL_valid_SKU].[Box_Count] as [master_cartons]
			,a.[Related]		
		FROM [dbo].[manually_groupdetail]
		LEFT JOIN [manually_shiptrack] ON [manually_groupdetail].[IDKEY] = [manually_shiptrack].[TxnID]
		LEFT JOIN [DL_valid_SKU] ON [manually_groupdetail].[ItemGroupRef_FullName] = [DL_valid_SKU].[QB_SKU]
		LEFT JOIN [dbo].[item_info] ON [DL_valid_SKU].[SKU] = [item_info].[sku]
		LEFT JOIN [orders_statuses] ON [manually_shiptrack].[PONumber] = [orders_statuses].[PONumber] AND [manually_shiptrack].[RefNumber] = [orders_statuses].[RefNumber] AND [orders_statuses].[Type] = '1'		
		OUTER APPLY (
			SELECT TOP 1
				dl.[SKU] as [Related]
			FROM [dbo].[manually_groupdetail] as gr
			LEFT JOIN [DL_valid_SKU] as dl ON gr.[ItemGroupRef_FullName] = dl.[QB_SKU]
			LEFT JOIN [fpt_related]  as fr ON dl.[SKU] = fr.[Related] AND fr.[Product] = [DL_valid_SKU].[SKU]
			WHERE gr.[IDKEY] = [manually_shiptrack].[TxnID]
			  AND dl.[SKU] IS NOT NULL
			  AND dl.[SKU] != fr.[Product]
			  AND [orders_statuses].[isActive] = '1'
		) a
		
WHERE [manually_shiptrack].[PONumber] = ".$qpo." 
		".$query_groupdetail_DL_part2."
		".$query_groupdetail_DL_part4."
		) c
GROUP BY [RefNumber], [TxnLineID], [ItemGroupRef_FullName], [Prod_desc], [Quantity], [SHIP_METHOD], [UPC], [drawing], [master_cartons], [Related]
ORDER BY [RefNumber], [ItemGroupRef_FullName];";
		
/*
print_r('<pre>');
print_r($query_groupdetail_DL);
print_r('</pre>');
die();
//*/

//invoicelinegroupdetail
if($txn) $query_invoicelinegroupdetail_DL_part1 = "AND [invoice].[TxnID] = '".$txn."'"; else $query_invoicelinegroupdetail_DL_part1 = "";
$query_invoicelinegroupdetail_DL= "SELECT * FROM
(SELECT
				[invoice].[RefNumber] as [QB Invoice#]
			  ,[invoicelinegroupdetail].[TxnLineID]
			  ,[invoicelinegroupdetail].[ItemGroupRef_FullName]
			  ,[invoicelinegroupdetail].[Desc] as [Prod_desc]
			  ,[invoicelinegroupdetail].[Quantity]
			,[DL_valid_SKU].[SHIP_METHOD]
		FROM [orders31].[dbo].[invoicelinegroupdetail]
		LEFT JOIN [orders31].[dbo].[invoice] ON [invoicelinegroupdetail].[IDKEY] = [invoice].[TxnID]
		LEFT JOIN [dbo].[DL_valid_SKU] ON [invoicelinegroupdetail].[ItemGroupRef_FullName] = [DL_valid_SKU].[QB_SKU]
		WHERE [invoice].[PONumber] = ".$qpo."
		".$query_invoicelinegroupdetail_DL_part1."
		) a
GROUP BY [QB Invoice#], [TxnLineID], [ItemGroupRef_FullName], [Prod_desc], [Quantity], [SHIP_METHOD]
ORDER BY [QB Invoice#], [ItemGroupRef_FullName];";

//salesorderlinegroupdetail
if($txn) $query_salesorderlinegroupdetail_DL_part1 = "AND [salesorder].[TxnID] = '".$txn."'"; else $query_salesorderlinegroupdetail_DL_part1 = "";
if($ref) $query_salesorderlinegroupdetail_DL_part2 = "AND [salesorder].[RefNumber] = '".$ref."'"; else $query_salesorderlinegroupdetail_DL_part2 = "";
$query_salesorderlinegroupdetail_DL="SELECT * FROM
(SELECT
				[salesorder].[RefNumber]
			  ,[salesorderlinegroupdetail].[TxnLineID]
			  ,[salesorderlinegroupdetail].[ItemGroupRef_FullName]
			  ,[salesorderlinegroupdetail].[Desc] as [Prod_desc]
			  ,[salesorderlinegroupdetail].[Quantity]
			,[DL_valid_SKU].[SHIP_METHOD]
		FROM [".DATABASE31."].[dbo].[salesorderlinegroupdetail]
		LEFT JOIN [".DATABASE31."].[dbo].[salesorder] ON [salesorderlinegroupdetail].[IDKEY] = [salesorder].[TxnID]
		LEFT JOIN [".DATABASE."].[dbo].[DL_valid_SKU] ON [salesorderlinegroupdetail].[ItemGroupRef_FullName] = [DL_valid_SKU].[QB_SKU]
		WHERE [salesorder].[PONumber] = ".$qpo." 
		".$query_salesorderlinegroupdetail_DL_part1."
                ".$query_salesorderlinegroupdetail_DL_part2."
		) a
GROUP BY [RefNumber], [TxnLineID], [ItemGroupRef_FullName], [Prod_desc], [Quantity], [SHIP_METHOD]
ORDER BY [RefNumber], [ItemGroupRef_FullName];";

//linedetail
if($ref) $query_linedetail_part1 = "AND [shiptrack].[RefNumber] = '".$ref."'"; else $query_linedetail_part1 = "";
if($ref) $query_linedetail_part2 = "AND [manually_shiptrack].[RefNumber] = '".$ref."'"; else $query_linedetail_part2 = "";
if($txn) $query_linedetail_part3 = "AND shiptrack.[TxnID] = '".$txn."'"; else $query_linedetail_part3 = "";
if($txn) $query_linedetail_part4 = "AND manually_shiptrack.[TxnID] = '".$txn."'"; else $query_linedetail_part4 = "";
$query_linedetail= "SELECT
				[shiptrack].[RefNumber]
			  ,[linedetail].[TxnLineID]
			  ,[linedetail].[CustomField9] as [ItemWeight]
			  ,[linedetail].[ItemRef_FullName]
			  ,[linedetail].[Rate]
			  ,[linedetail].[quantity]
			  ,[linedetail].[GroupIDKEY]
		FROM [dbo].[linedetail]
		LEFT JOIN [groupdetail] ON [groupdetail].[TxnLineID] = [linedetail].[GroupIDKEY]
		LEFT JOIN [shiptrack] ON [groupdetail].[IDKEY] = [shiptrack].[TxnID]
		WHERE [shiptrack].[PONumber] = ".$qpo." 
		".$query_linedetail_part1."
		".$query_linedetail_part3."
		and
						 (
						  [linedetail].ItemRef_FullName not like '%Price-Adjustment%' 
						  and [linedetail].ItemRef_FullName not like '%Freight%'
						  and [linedetail].ItemRef_FullName not like '%warranty%' 
						  and [linedetail].ItemRef_FullName is not NULL
						  and [linedetail].ItemRef_FullName not like '%Subtotal%'
						  and [linedetail].ItemRef_FullName not like '%IDSC-10%'
						  and [linedetail].ItemRef_FullName not like '%DISCOUNT%'
						  and [linedetail].ItemRef_FullName not like '%SPECIAL ORDER%'
						  and [linedetail].ItemRef_FullName not like '%Manual%'
						  )
		UNION ALL
		SELECT 
				[manually_shiptrack].[RefNumber]
			  ,[manually_linedetail].[TxnLineID]
			  ,[manually_linedetail].[CustomField9] as [ItemWeight]
			  ,[manually_linedetail].[ItemRef_FullName]
			  ,[manually_linedetail].[Rate]
			  ,[manually_linedetail].[quantity]
			  ,[manually_linedetail].[GroupIDKEY]
		FROM [dbo].[manually_linedetail]
		LEFT JOIN [manually_groupdetail] ON [manually_groupdetail].[TxnLineID] = [manually_linedetail].[GroupIDKEY]
		LEFT JOIN [manually_shiptrack] ON [manually_groupdetail].[IDKEY] = [manually_shiptrack].[TxnID]
		WHERE [manually_shiptrack].[PONumber] = ".$qpo." 
		".$query_linedetail_part2."
		".$query_linedetail_part4."
		and
						 (
						  [manually_linedetail].ItemRef_FullName not like '%Price-Adjustment%' 
						  and [manually_linedetail].ItemRef_FullName not like '%Freight%'
						  and [manually_linedetail].ItemRef_FullName not like '%warranty%' 
						  and [manually_linedetail].ItemRef_FullName is not NULL
						  and [manually_linedetail].ItemRef_FullName not like '%Subtotal%'
						  and [manually_linedetail].ItemRef_FullName not like '%IDSC-10%'
						  and [manually_linedetail].ItemRef_FullName not like '%DISCOUNT%'
						  and [manually_linedetail].ItemRef_FullName not like '%SPECIAL ORDER%'
						  and [manually_linedetail].ItemRef_FullName not like '%Manual%'
						  )
						  ORDER BY [RefNumber], ItemRef_FullName;";

//invoicelinedetail
if($txn) $query_invoicelinedetail_part1 = "AND [invoice].[TxnID] = '".$txn."'"; else $query_invoicelinedetail_part1 = "";
$query_invoicelinedetail= "SELECT 
				[invoice].[RefNumber] as [QB Invoice#]
			  ,[invoicelinedetail].[TxnLineID]
			  ,[invoicelinedetail].[CustomField9] as [ItemWeight]
			  ,[invoicelinedetail].[ItemRef_FullName]
			  ,[invoicelinedetail].[Rate]
			  ,[invoicelinedetail].[quantity]
			  ,[invoicelinedetail].[GroupIDKEY]
		FROM [".DATABASE31."].[dbo].[invoicelinedetail]
		LEFT JOIN [".DATABASE31."].[dbo].[invoicelinegroupdetail] ON [invoicelinegroupdetail].[TxnLineID] = [invoicelinedetail].[GroupIDKEY]
		LEFT JOIN [".DATABASE31."].[dbo].[invoice] ON [invoicelinegroupdetail].[IDKEY] = [invoice].[TxnID]
		WHERE [invoice].[PONumber] = ".$qpo."
		".$query_invoicelinedetail_part1."
		and
						 (
						  [invoicelinedetail].ItemRef_FullName not like '%Price-Adjustment%' 
						  and [invoicelinedetail].ItemRef_FullName not like '%Freight%'
						  and [invoicelinedetail].ItemRef_FullName not like '%warranty%' 
						  and [invoicelinedetail].ItemRef_FullName is not NULL
						  and [invoicelinedetail].ItemRef_FullName not like '%Subtotal%'
						  and [invoicelinedetail].ItemRef_FullName not like '%IDSC-10%'
						  and [invoicelinedetail].ItemRef_FullName not like '%DISCOUNT%'
						  and [invoicelinedetail].ItemRef_FullName not like '%SPECIAL ORDER%'
						  and [invoicelinedetail].ItemRef_FullName not like '%Manual%'
						  )
						  ORDER BY [RefNumber], ItemRef_FullName;";

//salesorderlinedetail
if($txn) $query_salesorderlinedetail_part1 = "AND [salesorder].[TxnID] = '".$txn."'"; else $query_salesorderlinedetail_part1 = "";
if($ref) $query_salesorderlinedetail_part2 = "AND [salesorder].[RefNumber] = '".$ref."'"; else $query_salesorderlinedetail_part2 = "";
$query_salesorderlinedetail= "SELECT 
				[salesorder].[RefNumber]
			  ,[salesorderlinedetail].[TxnLineID]
			  ,[salesorderlinedetail].[CustomField9] as [ItemWeight]
			  ,[salesorderlinedetail].[ItemRef_FullName]
			  ,[salesorderlinedetail].[Rate]
			  ,[salesorderlinedetail].[quantity]
                          ,[salesorderlinedetail].[Invoiced]
			  ,[salesorderlinedetail].[GroupIDKEY]
		FROM [".DATABASE31."].[dbo].[salesorderlinedetail]
		LEFT JOIN [".DATABASE31."].[dbo].[salesorderlinegroupdetail] ON [salesorderlinegroupdetail].[TxnLineID] = [salesorderlinedetail].[GroupIDKEY]
		LEFT JOIN [".DATABASE31."].[dbo].[salesorder] ON [salesorderlinegroupdetail].[IDKEY] = [salesorder].[TxnID]
		WHERE [salesorder].[PONumber] = ".$qpo."
		".$query_salesorderlinedetail_part1."
                ".$query_salesorderlinedetail_part2."
		and
						 (
						  [salesorderlinedetail].ItemRef_FullName not like '%Price-Adjustment%' 
						  and [salesorderlinedetail].ItemRef_FullName not like '%Freight%'
						  and [salesorderlinedetail].ItemRef_FullName not like '%warranty%' 
						  and [salesorderlinedetail].ItemRef_FullName is not NULL
						  and [salesorderlinedetail].ItemRef_FullName not like '%Subtotal%'
						  and [salesorderlinedetail].ItemRef_FullName not like '%IDSC-10%'
						  and [salesorderlinedetail].ItemRef_FullName not like '%DISCOUNT%'
						  and [salesorderlinedetail].ItemRef_FullName not like '%SPECIAL ORDER%'
						  and [salesorderlinedetail].ItemRef_FullName not like '%Manual%'
						  )
						  ORDER BY [RefNumber], ItemRef_FullName;";

//validation
$query_validation= "SELECT [orders31].[dbo].GROUP_CONCAT_S('Error '+[error_code]+': '+[MESSAGE], 1) as [MESSAGE], [orders31].[dbo].GROUP_CONCAT_S([error_code], 1) as [error_code]
    FROM
(
Select distinct [PO_NUMBER], [MESSAGE], [error_code]
From
(
SELECT [po] as [PO_NUMBER]
      ,[message] as [MESSAGE]
      ,case
		when [error_code] IS NULL then ''
		else [error_code]
	   end as [error_code]
  FROM [dbo].[validation_log] vl
  WHERE NOT EXISTS
  (SELECT PONumber FROM [dbo].[ShipTrackWithLabel] stwl
   WHERE stwl.PONUMBER = vl.po
   AND stwl.[DATE] IS NOT NULL)
  AND [po] =  ".$qpo."
) A
) B GROUP BY [PO_NUMBER];";

/*print_r('<pre>');
print_r($query_validation);
print_r('</pre>');
die();*/

$query_warning1 = "SELECT 
		[id],
		[Type],
		[Text]
  FROM [dbo].[orders_statuses]
  WHERE [isActive] = 1 AND [PONumber] = ".$qpo;
  
$query_warning2 = "SELECT 
[itemgroup].[Name],
[itemgroup].[CustomField6]
FROM [".DATABASE31."].[dbo].[salesorderlinegroupdetail]
LEFT JOIN [".DATABASE31."].[dbo].[itemgroup] ON [itemgroup].[Name] = [salesorderlinegroupdetail].[ItemGroupRef_FullName]
INNER JOIN [".DATABASE31."].[dbo].[salesorder] ON [salesorderlinegroupdetail].[IDKEY] = [salesorder].[TxnID]
INNER JOIN [".DATABASE."].[dbo].[ShipTrackWithLabel] ON [ShipTrackWithLabel].[PONUMBER] = [salesorder].[PONumber]
WHERE [ShipTrackWithLabel].[PONUMBER] = ".$qpo." 
AND 
(
	[itemgroup].[CustomField6] LIKE '%DO NOT SHIP%'
	OR [itemgroup].[CustomField6] LIKE '%SHIP GLASS OD205 AND BELOW%'
	OR [itemgroup].[CustomField6] LIKE '%SHIP GLASS OD206 AND ABOVE%'
);";

$query_warning3 = "SELECT 
		'Warning: May need recalculation! SHIP_METHOD was updated!' as [recalculation]
		FROM [".DATABASE."].[dbo].[groupdetail]
		LEFT JOIN [".DATABASE."].[dbo].[shiptrack] ON [shiptrack].[TxnID] = [groupdetail].[IDKEY]
		LEFT JOIN [".DATABASE."].[dbo].[ShipTrackWithLabel] ON [ShipTrackWithLabel].[ID] = [shiptrack].[RefNumber]
		INNER JOIN [".DATABASE."].[dbo].[SHIP_METHOD_CHANGED] ON [groupdetail].[ItemGroupRef_FullName] = [SHIP_METHOD_CHANGED].[SKU]
		WHERE [ShipTrackWithLabel].[DATE] < [SHIP_METHOD_CHANGED].[CHANGE_DATE]
		AND [ShipTrackWithLabel].[PONUMBER] =  ".$qpo."
		GROUP BY [ShipTrackWithLabel].[ID];";
		
$query_warning4 = "SELECT [ShipMethodRef_FullName], [TimeModified]
		  FROM [".DATABASE31."].[dbo].[salesorder]
		  WHERE ([ShipMethodRef_FullName] LIKE '%BACKORDER%'
		  OR [ShipMethodRef_FullName] LIKE '%CANCELLED%'
		  OR [ShipMethodRef_FullName] LIKE '%CC DECLINED%'
		  OR [ShipMethodRef_FullName] LIKE '%ON HOLD%'
		  OR [ShipMethodRef_FullName] LIKE '%ON HOLD-INCOMPL%'
		  OR [ShipMethodRef_FullName] LIKE '%ON HOLD - PSI%'
		  OR [ShipMethodRef_FullName] LIKE '%PARTIAL_SHIP%'
		  OR [ShipMethodRef_FullName] LIKE '%PRODUCTION%'
		  OR [ShipMethodRef_FullName] LIKE '%VOID%')
		  AND [PONumber] = ".$qpo.";";

$query_warning5 = "SELECT [zm].[message]
	FROM [dbo].[zip_message] [zm]
	INNER JOIN [dbo].[ShipTrackWithLabel] [stwl] ON LEFT ([zm].[zip], 5) = LEFT ([stwl].[TOZIP], 5)
	WHERE [stwl].[PONUMBER] = ".$qpo;

$query_statuses = "EXECUTE shipping_module_status_history3 @PO = ".$qpo.";";

$query_get_combined = "SELECT [Orders_combined].[PONumber], [Orders_combined].[Combined_into_POnumber]
						  FROM [dbo].[Orders_combined]
						  INNER JOIN [dbo].[manually_shiptrack] ON [Orders_combined].[Combined_into_RefNumber] = [manually_shiptrack].[RefNumber]
						  WHERE [Combined_into_POnumber] IN
							(
								SELECT [Combined_into_POnumber]
								FROM [dbo].[Orders_combined]
								WHERE 
								[Orders_combined].[PONumber] = ".$qpo." 
								OR [Orders_combined].[Combined_into_POnumber] = ".$qpo."
							)
							group by [Orders_combined].[PONumber], [Orders_combined].[Combined_into_POnumber];";

$query_get_splitted = "SET NOCOUNT ON
BEGIN

DECLARE @ID varchar(255);

SELECT TOP 1 @ID = [ID] FROM [dbo].[ShipTrackWithLabel] WHERE [PONumber] = ".$qpo.";

END

SELECT 
	[Divided_into_RefNumber] as [RefNumber], 
	[Divided_into_POnumber] as [PONumber] 
FROM [dbo].[Orders_divided] 
WHERE 
[RefNumber] = @ID 
AND [POnumber] = ".$qpo." 
UNION ALL 
SELECT 
@ID as [RefNumber], 
".$qpo." as [PONumber] WHERE @ID IS NOT NULL;";

$results_all = get_all_data();

function getSettings()
{
    global $query_settings;
	$conn = Database::getInstance()->dbc;
	
	$result = $conn->prepare($query_settings);
	$result->execute();
	$result = $result->fetchAll(PDO::FETCH_ASSOC);
	if (!isset($result) || empty($result)) die('Can not get settings.');
	return $result;
}

function get_combined_files($po)
{
    global $query_get_combined;
		$files_array = array();
		$conn = Database::getInstance()->dbc;
		
		$res0 = exec_query($query_get_combined);
		
		if(!empty($res0))
		{
			$bol_po = $res0[0]['Combined_into_POnumber'];
			$bol_po = repPo($bol_po);
			$bol_po = trim($bol_po);
			$bolName = 'PdfBill/'.$bol_po.'Bill.pdf';
			$isBolExists = findFile($bolName);
			if ($isBolExists)
			{
				$files_array[]=array('type' => 'bol', 'path' => urlencode($bolName));
			}
			
			foreach($res0 as $value)
			{
				$labelName = 'Pdf/'.$value['PONumber'].'.pdf';
                                $flyerName = 'Pdf/'.$value['PONumber'].'flyer.pdf';
                                
				$isLabelExists = findFile($labelName);
                                $isFlyerExists = findFile($flyerName);
                                
				if ($isLabelExists)
				{
					$files_array[]=array('type' => 'label', 'path' => urlencode($labelName));
				}
                                if ($isFlyerExists)
				{
					$files_array[]=array('type' => 'flyer', 'path' => urlencode($flyerName));
				}
			}
		}
	return $files_array;
}

function repPo($po)
{
$bad = array('/', ' ', '#', '%', '&', '{', '}', '<', '>', '*', '?', '/', '$', '!', "'", '"', ':', '@', '+', '`', '|', '=', '­');
	$good = "_";
	$repPo = str_replace($bad, $good, $po);
	return $repPo;
}

function get_generated_files($po)
{
	$po = repPo($po);
	$po = trim($po);
        
	$labelName = 'Pdf/'.$po.'.pdf';	
	$bolName = 'PdfBill/'.$po.'Bill.pdf';
        $flyerName = 'Pdf/'.$po.'flyer.pdf';
		
	$isLabelExists = findFile($labelName);
	$isBolExists = findFile($bolName);
        $isFlyerExists = findFile($flyerName);
		
	if ($isLabelExists) $html = $labelName; else $html = 'false';
	if ($isBolExists) $html2 = $bolName; else $html2 = 'false';
        if ($isFlyerExists) $html3 = $flyerName; else $html3 = 'false';
		
	$returnValue = array(
	array('type' => 'label', 'path' => urlencode($html)),
	array('type' => 'bol', 'path' => urlencode($html2)),
        array('type' => 'flyer', 'path' => urlencode($html3))
	);
	return $returnValue;
}

function findFile($filename)
{
	if (file_exists($filename)) {
     return true;
	} else
	{
		return false;
	}
}

function generate_info($result_shiptrack, $result_shiptrackwithlabel, $result_shiptrack_s, $po)
{
    global $results_all;
	$info_messages = array();
	if (!empty($result_shiptrack['data'][0]))
	{
		$result_shiptrack_temp = $result_shiptrack['data'][0];
		if($result_shiptrack_temp['IsFullyInvoiced'] == 1) $info_messages[]= 'This order was fully invoiced. It can not be rated for shipping any more.';
		if($result_shiptrack_temp['IsManuallyClosed'] == 1) $info_messages[]= 'This order was manually closed. It can not be rated for shipping any more.';
	}
	if(empty($result_shiptrack_s['data'][0]) && empty($result_shiptrack['data'][0]) &&
	empty($result_shiptrackwithlabel['data'][0]))
	{
		$info_messages[]= 'This order has not been transferred from QB.';
	}
        
        
        
        if(!empty($result_shiptrack_s['data'][0]) && $result_shiptrack_s['data'][0]['SalesRepRef_FullName'] == 'HDDC')
	{
		$info_messages[]= 'This is HDDC order. Please process it with appropriate UI.';
	}
        
        if(!empty($result_shiptrack_s['data'][0]) && $result_shiptrack_s['data'][0]['SalesRepRef_FullName'] == 'HOSP')
	{
		$info_messages[]= 'This is Hospitality order. Shipping cost will not be calculated for this order.';
	}
        
        /*print_r('<pre>');
        print_r($result_shiptrack_s);
        print_r($result_shiptrack_s['data'][0]['SalesRepRef_FullName']);
        print_r($info_messages);
        print_r('</pre>');
        die();*/
	

	if (!empty($result_shiptrack['data']) && 
	count($result_shiptrack['data']) > 1)
	{
		$links = '';
		$last_ref = '';
		$dup_ref = 0;
		foreach($result_shiptrack['data'] as $order)
		{
			if ($order['RefNumber'] == $last_ref) $dup_ref++;
			$links.= '<br><a target="_blank" href="order_status_history.php?po='.$order['PONumber'].'&ref='.$order['RefNumber'].'"><button class="ref_button">'.$order['RefNumber'].'</button></a><br>';
			$last_ref = $order['RefNumber'];
		}
		if ($dup_ref == 0) $info_messages[]= 'This order has duplicating PONumber. Please choose RefNumber:<br>'.$links; else $info_messages[]= 'This order has duplicating PONumber and RefNumber. Shipping Module is not able to differentiate these orders. Number of duplicates: '.$dup_ref;
	}
	
	if(!empty($result_shiptrack['data']))
	{
		$temp_ref = explode('_split_', $result_shiptrack['data'][0]['RefNumber']);
		$temp_ref = $temp_ref[0];
		$temp_po = explode('_split_', $result_shiptrack['data'][0]['PONumber']);
		$temp_po = $temp_po[0];
		$related_orders = $results_all['result_get_splitted'];
		if(!empty($related_orders))
		{
			$links = '';
			foreach($related_orders as $related_order)
			{
				if($result_shiptrack['data'][0]['RefNumber'] != $related_order['RefNumber'] && $result_shiptrack['data'][0]['PONumber'] != $related_order['PONumber'])
				{
					$links.= '<br><a target="_blank" href="order_status_history.php?po='.$related_order['PONumber'].'&ref='.$related_order['RefNumber'].'"><button class="ref_button">'.$related_order['PONumber'].'</button></a><br>';
				}
			}
			if(strpos($result_shiptrack['data'][0]['RefNumber'], '_split_') !== false)
                        {
                            $message = 'This order is splitted part.<br>';
                        } else
                        {
                            $message = 'This order was splitted. Related orders:<br>';
                        }
			if(!empty($links)){
                            $info_messages[]= $message.$links;
                        }/* else
                        {
                            $info_messages[]= $message;
                        }*/
		}
	}
	
	//combine orders
	if(!empty($result_shiptrack['data']))
	{
		$related_orders = $results_all['result_get_combined'];
		if(!empty($related_orders))
		{
			$links = '';
			foreach($related_orders as $related_order)
			{
				if($result_shiptrack['data'][0]['PONumber'] != $related_order['PONumber'])
				{
					$links.= '<br><a target="_blank" href="order_status_history.php?po='.$related_order['PONumber'].'"><button class="ref_button">'.$related_order['PONumber'].'</button></a><br>';
				}
			}
			
			if($result_shiptrack['data'][0]['PONumber'] != $related_orders[0]['Combined_into_POnumber'])
			{
				if (strlen($related_orders[0]['Combined_into_POnumber'])>25) $Combined_into_POnumber = substr($related_orders[0]['Combined_into_POnumber'], 0, 25).'...'; else $Combined_into_POnumber = $related_orders[0]['Combined_into_POnumber'];
				$links.= '<br><a target="_blank" href="order_status_history.php?po='.$related_orders[0]['Combined_into_POnumber'].'"><button class="ref_button">'.$Combined_into_POnumber.'</button></a><br>';
			}
				
			if(strpos($result_shiptrack['data'][0]['RefNumber'], 'combine_') !== false) 
				$message = 'This order is combined order. Related orders:<br>'; else
					$message = 'This order is one of suborders of combined order. Related orders:<br>';
			if(!empty($links)) $info_messages[]= $message.$links;
			
			$generated_files = get_combined_files($po);
		}
	}
        
        $links_to_files = '';
        if(empty($generated_files)) $generated_files = get_generated_files($po);
        if(!empty($generated_files))
        {
                foreach($generated_files as $value)
                {
                        if($value['path'] != 'false')
                        {
                                if($value['type'] == 'label') $type = 'Label'; 
                                else if($value['type'] == 'bol') $type = 'Bill Of Lading';
                                else $type = 'Flyer';
                                $links_to_files.= '<br><button onclick="goReprint('."'".$po."'".', '."'".$value['path']."'".')" class="ref_button">'.$type.'</button><br>';
                        }
                }
                if(!empty($links_to_files)) $info_messages[]= 'Documents generated for this order: <br>'.$links_to_files;
        }
	
	if(!empty($result_shiptrack['data']))
	{
		if(strpos($result_shiptrack['data'][0]['RefNumber'], 'manually_') !== false) 
		$info_messages[]= 'This order was created manually in Shipping Module.';
	}
	
	if(!empty($result_shiptrack['data']))
	{
		if (isCanadaState($result_shiptrack['data'][0]['State'], $result_shiptrack['data'][0]['Country'])) $info_messages[]= 'This order goes to Canada.';
		if (isHI_AL($result_shiptrack['data'][0]['State']) == true)
		{
			if ($result_shiptrack['data'][0]['State'] == 'AK') $info_messages[]= 'This order goes to Alaska.';
			if ($result_shiptrack['data'][0]['State'] == 'HI') $info_messages[]= 'This order goes to Hawaii.';
		}
	}
	
	return $info_messages;
}

function isCanadaState($state, $country)
{
	$returnVal = false;
	$canada_states = array('AB', 'BC', 'MB', 'NB', 'NL', 'NS', 'NT', 'NU', 'ON', 'PE', 'QC', 'SK', 'YT');
	if (isset($country) && !empty($country))
	{
		if ($country == 'CA' || $country == 'CAN') $returnVal = true;
	}
	if (isset($state) && !empty($state))
	{
		if (in_array($state, $canada_states)) $returnVal = true;
	}
	return $returnVal;
}

function isHI_AL($state)
{
	$returnVal = false;
	if (isset($state) && !empty($state))
	{
		if ($state == 'AK' || $state == 'HI') $returnVal = true;
	}
	return $returnVal;
}

function format_date($value)
{
	$time = strtotime($value);
	$time = date("m-d | H:i", $time);
	return $time;
}

function exec_query($query)
{
	try{
		$conn = Database::getInstance()->dbc;
		$result = $conn->prepare($query);
		$result->execute();
		$result = $result->fetchAll(PDO::FETCH_ASSOC);
		$conn = null;
		return $result;
	}
	catch(PDOException $e)
	{
		print_r($e->getMessage());
		print_r($query);
		die();
	}
}

function get_status($po)
{
	global $query_statuses;
	$result = exec_query($query_statuses);
	return $result;
}

/*function get_allcosts()
{
	$retval = '';
	global $query_allcosts;
	$result_allcosts = exec_query($query_allcosts);
	if($result_allcosts)
	{
		usort($result_allcosts, "sort_by_cost");
		foreach($result_allcosts as $key => $value)
		{
			$retval.=$value['CARRIERNAME'].": ".$value['COST']."<br>";
		}
	}
	return $retval;
}*/

function sort_by_cost($a,$b) {
	return $a['COST']>$b['COST'];
}

function get_warnings($inventory)
{
	global $results_all;
	$returnValue = false;
	$res = $results_all['result_warning1'];
	$res2 = $results_all['result_warning2'];
	$res3 = $results_all['result_warning3'];
	$res4 = $results_all['result_warning4'];
        $res5 = $results_all['result_warning5'];
	$resultArray = array();
		
	if (isset($res2) && !empty($res2)) 
	{
		foreach ($res2 as $key => $value)
		{
			$resultArray[]= [
				'Warning' => $value['Name'].': '.$value['CustomField6'],
				'Code'    => 2,
				'ID'      => 0,
			];
		}
	}
	if (isset($res) && !empty($res)) 
	{
		foreach ($res as $key => $value)
		{			
			$resultArray[]= [
				'Warning' => $value['Text'],
				'Code'    => $value['Type'],
				'ID'      => $value['id'],
			];
		}
	}
	if (isset($res3) && !empty($res3)) 
	{
		foreach ($res3 as $key => $value)
		{
			$resultArray[]= [
				'Warning' => $value['recalculation'],
				'Code'    => 3,
				'ID'      => 0,
			];
		}
	}
	if (isset($res4) && !empty($res4)) 
	{
		foreach ($res4 as $key => $value)
		{
			$resultArray[]= [
				'Warning' => $value['ShipMethodRef_FullName'].' - Order last modified - '.$value['TimeModified'],
				'Code'    => 4,
				'ID'      => 0,
			];
		}
	}
        if ($inventory) $resultArray[]=[
				'Warning' => 'Not enough items for this order! Calculated date: '.$inventory,
				'Code'    => 5,
				'ID'      => 0,
			];
        if (!empty($res5)) 
	{
		foreach ($res5 as $key => $value)
		{
			$resultArray[]= [
				'Warning' => $value['message'],
				'Code'    => 6,
				'ID'      => 0,
			];
		}
	}
	if(!empty($resultArray)) $returnValue = $resultArray;
	return $returnValue;
}

function get_all_data()
{
    global $query_allcosts,
	$query_shiptrack,
	$query_invoice,
	$query_salesorder,
	$query_shiptrackwithlabel,
	$query_groupdetail_DL,
	$query_invoicelinegroupdetail_DL,
	$query_salesorderlinegroupdetail_DL,
	$query_linedetail,
	$query_invoicelinedetail,
	$query_salesorderlinedetail,
	$query_validation,
	$query_warning1,
	$query_warning2,
	$query_warning3,
	$query_warning4,
        $query_warning5,
	$query_statuses,
	$query_get_combined,
        $query_combined_files,
        $query_settings,
        $query_get_splitted;
    
	$queries = 
	$query_allcosts.
	$query_shiptrack.
	$query_invoice.
	$query_salesorder.
	$query_shiptrackwithlabel.
	$query_groupdetail_DL.
	$query_invoicelinegroupdetail_DL.
	$query_salesorderlinegroupdetail_DL.
	$query_linedetail.
	$query_invoicelinedetail.
	$query_salesorderlinedetail.
	$query_validation.
	$query_warning1.
	$query_warning2.
	$query_warning3.
	$query_warning4.
        $query_warning5.
	$query_statuses.
	$query_get_combined.
        $query_settings.
        $query_get_splitted;
		
	$results_all = [];
        
        try{
		$conn = Database::getInstance()->dbc;
		$result = $conn->prepare($queries);
		$result->execute();
		
		$result_allcosts = $result->fetchAll(PDO::FETCH_ASSOC);
                $results_all['result_allcosts'] = '';
                
                if($result_allcosts)
                {
                        usort($result_allcosts, "sort_by_cost");
                        foreach($result_allcosts as $key => $value)
                        {
                                $results_all['result_allcosts'].=$value['CARRIERNAME'].": ".$value['COST']."<br>";
                        }
                }
		
		$result->nextRowset();
		$result_shiptrack = $result->fetchAll(PDO::FETCH_ASSOC);
                
                if($result_shiptrack) $info = "[".DATABASE."].[shiptrack]"; else $info = "";
                $results_all['result_shiptrack'] = array('info' => $info, 'data' => $result_shiptrack);
		
		$result->nextRowset();
		$result_invoice = $result->fetchAll(PDO::FETCH_ASSOC);
                
                if ($result_invoice) $info = "[".DATABASE31."].[invoice]"; else $info = "";
                $results_all['result_invoice'] = array('info' => $info, 'data' => $result_invoice);
		
		$result->nextRowset();
		$result_salesorder = $result->fetchAll(PDO::FETCH_ASSOC);
                
                if ($result_salesorder) $info = "[".DATABASE31."].[salesorder]"; else $info = "";
                $results_all['result_salesorder'] = array('info' => $info, 'data' => $result_salesorder);
		
		$result->nextRowset();
		$result_shiptrackwithlabel = $result->fetchAll(PDO::FETCH_ASSOC);
                
                if($result_shiptrackwithlabel) $info = "[".DATABASE."].[shiptrackwithlabel]"; else $info = "";
                $results_all['result_shiptrackwithlabel'] = array('info' => $info, 'data' => $result_shiptrackwithlabel);
		
		$result->nextRowset();
		$result_groupdetail_DL = $result->fetchAll(PDO::FETCH_ASSOC);
                
                if($result_groupdetail_DL) $info = "[".DATABASE."].[groupdetail]"; else $info = "";
                $results_all['result_groupdetail_DL'] = array('info' => $info, 'data' => $result_groupdetail_DL);
		
		$result->nextRowset();
		$result_invoicelinegroupdetail_DL = $result->fetchAll(PDO::FETCH_ASSOC);
                
                if ($result_invoicelinegroupdetail_DL) $info = "[".DATABASE31."].[invoicelinegroupdetail]"; else $info = "";
                $results_all['result_invoicelinegroupdetail_DL'] = array('info' => $info, 'data' => $result_invoicelinegroupdetail_DL);
		
		$result->nextRowset();
		$result_salesorderlinegroupdetail_DL = $result->fetchAll(PDO::FETCH_ASSOC);
                
                if ($result_salesorderlinegroupdetail_DL) $info = "[".DATABASE31."].[salesorderlinegroupdetail]"; else $info = "";
                $results_all['result_salesorderlinegroupdetail_DL'] = array('info' => $info, 'data' => $result_salesorderlinegroupdetail_DL);
		
		$result->nextRowset();
		$result_linedetail = $result->fetchAll(PDO::FETCH_ASSOC);
                
                if($result_linedetail) $info = "[".DATABASE."].[linedetail]"; else $info = "";
                $results_all['result_linedetail'] = array('info' => $info, 'data' => $result_linedetail);
		
		$result->nextRowset();
		$result_invoicelinedetail = $result->fetchAll(PDO::FETCH_ASSOC);
                
                if ($result_invoicelinedetail) $info = "[".DATABASE31."].[invoicelinedetail]"; else $info = "";
                $results_all['result_invoicelinedetail'] = array('info' => $info, 'data' => $result_invoicelinedetail);
		
		$result->nextRowset();
		$result_salesorderlinedetail = $result->fetchAll(PDO::FETCH_ASSOC);
                
                if ($result_salesorderlinedetail) $info = "[".DATABASE31."].[salesorderlinedetail]"; else $info = "";
                $results_all['result_salesorderlinedetail'] = array('info' => $info, 'data' => $result_salesorderlinedetail);
		
		$result->nextRowset();
		$results_all['result_validation'] = $result->fetchAll(PDO::FETCH_ASSOC);
		
		$result->nextRowset();
		$results_all['result_warning1'] = $result->fetchAll(PDO::FETCH_ASSOC);
		
		$result->nextRowset();
		$results_all['result_warning2'] = $result->fetchAll(PDO::FETCH_ASSOC);
		
		$result->nextRowset();
		$results_all['result_warning3'] = $result->fetchAll(PDO::FETCH_ASSOC);
		
		$result->nextRowset();
		$results_all['result_warning4'] = $result->fetchAll(PDO::FETCH_ASSOC);
                
                $result->nextRowset();
		$results_all['result_warning5'] = $result->fetchAll(PDO::FETCH_ASSOC);
		
		$result->nextRowset();
		$results_all['result_statuses'] = $result->fetchAll(PDO::FETCH_ASSOC);
		
		$result->nextRowset();
		$results_all['result_get_combined'] = $result->fetchAll(PDO::FETCH_ASSOC);
                
                $result->nextRowset();
		$results_all['result_settings'] = $result->fetchAll(PDO::FETCH_ASSOC);
                
                $result->nextRowset();
		$results_all['result_get_splitted'] = $result->fetchAll(PDO::FETCH_ASSOC);
		
		$conn = null;
		return $results_all;
	}
	catch(PDOException $e)
	{
		print_r($e->getMessage());
		print_r($queries);
		die();
	}
}