<?php
$path = $_SERVER['DOCUMENT_ROOT'];
require_once($path.'/db_connect/connect.php');
date_default_timezone_set('America/New_York');

$message = '';

	if (isset($_REQUEST['po']) && !empty($_REQUEST['po'])
		&& isset($_REQUEST['reason'])
		&& isset($_REQUEST['reasonNote'])
		&& isset($_REQUEST['userName']))
	{
		$po = $_REQUEST['po'];
		$reason = $_REQUEST['reason'];
		$reasonNote = $_REQUEST['reasonNote'];
		$reasonNote = str_replace("'", "''", $reasonNote);
		$reasonNote = htmlspecialchars($reasonNote);
		$userName = $_REQUEST['userName'];

		/*print_r($id);
		print_r($po);*/
		$conn = Database::getInstance()->dbc;
		$query0 = "[dbo].[recycle_order] @PO = ".$conn->quote($po).", @reason = ".$conn->quote($reason).", @reasonNote = ".$conn->quote($reasonNote).", @userName = ".$conn->quote($userName);
		$res0 = removeRow($conn, $query0);
		if ($res0)
		{
                    $message.="Order recycled.";
		} else $message.="Error. Can not recycle order";
		$conn = null;
	}

print_r($message);

function removeRow($conn, $query)
{
	global $message;
	try{
		$result = $conn->prepare($query);
		$result->execute();
		return true;
	}
	catch(PDOException $e)
	{
		$result.=$e->getMessage();
	}
}
?>