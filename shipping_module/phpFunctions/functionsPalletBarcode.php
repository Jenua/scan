<?php
//die('stop');
ini_set('max_execution_time', 600);
ini_set("memory_limit", "500M");
//Include files
require_once('../Include/barcode/class/BCGFontFile.php');
require_once('../Include/barcode/class/BCGcode39.barcode.php');
require_once('../Include/barcode/class/BCGcode128.barcode.php');
require_once('../Include/barcode/class/BCGColor.php');
require_once('../Include/barcode/class/BCGDrawing.php');
require_once('../Include/barcode/class/BCGpdf417.barcode2d.php');
require_once('../Include/barcode/class/BCGi25.barcode.php');
require_once('../Include/barcode/class/BCGgs1128.barcode.php');
require_once('../Include/barcode/class/BCGupca.barcode.php');
require_once('../Include/fpdf17/fpdf.php');
require_once('../Include/tcpdf/tcpdf.php');
require_once('functionsPDF.php');
require_once('functionsDB.php');
require_once('functionsBar.php');
require_once('functionsExtr.php');
$path = $_SERVER['DOCUMENT_ROOT'];
require_once($path.'/db_connect/connect.php');

//Global variables
$error_messages = array();//Array to save all errors
$data_error_messages = array();//Array to save all errors in input data
$pdf_patheLabel = false;//path to pdf. False while not created

//Start if have PO number
if (isset($_REQUEST['po']) && !empty($_REQUEST['po']) && isset($_REQUEST['ref']) && !empty($_REQUEST['ref']))
	{
            /*print_r($_REQUEST);
            die('request');*/
            main($_REQUEST['po'], $_REQUEST['ref']);
	}

//Main function to execute functions kit depending on store name
function main($po, $ref)
{
	global $error_messages;
	global $data_error_messages;
	global $connect;
	global $pdf_patheLabel;
	
	$array = getPalletsTrucks($po, $ref);
        if($array)
        {
            foreach($array as $key => $pallet)
            {
                $string = "PLT_".$pallet['PALLET']." TRU_".$pallet['TRUCK']." PO_".$pallet['PONUMBER'];
                $array[$key]['barcode'] = gen_imageCode128($string).".png";
            }
            
            $pdf_patheLabel = truckPreload($array);
        } else
        {
            error_msg("Can not get truck preload data for this order!");
            send_error();
        }

	return send_error();
}

//is set pro and bol
function issetProBol($pro, $bol)
{
	if ((!isset($pro) || !isset($bol)) || (empty($pro) || empty($bol))  || ($pro == ' ' || $bol == ' '))
		{
			error_msg("PRO# and INVOICE# data needed!");
			send_error();
		} else return true;
}

//Sort array
function ab($a,$b)
{
	return ($a['boxNumber']-$b['boxNumber']);
}

//Fills an array with error messages
function error_msg($msg)
{
	global $error_messages;
	$error_messages[] = $msg;
}

//Fills an array with errors in input data
function data_error_msg($type, $id, $msg)
{
	global $data_error_messages;
	$row = array('type' => $type, 'id' => $id, 'msg' => $msg);
	$data_error_messages[] = $row;
}

//Generate list of error messages
function formErrors($error_messages)
{
	if (isset($error_messages) && !empty($error_messages))
	{
		$mes_text = "<b><p style='font-size: 12pt; color: red;'>Label:</p></b>";
		foreach($error_messages as $message){
			$mes_text.= "<b><p style='font-size: 10pt; color: black;'>".$message."</p></b>";
		}
		return $mes_text;
	} else return false;
}

//Generate table with errors in input data
function formDataErrors($data_error_messages)
{
	if (isset($data_error_messages) && !empty($data_error_messages))
	{
		$mes_text = "<b><p style='font-size: 12pt; color: red;'>Label. Detailed problems:</p></b><p><table border='1' style='font-size: 12pt; color: black;'>
		<tr>
			<th>type</th>
			<th>data</th>
			<th>problem</th>
		</tr>";
		foreach($data_error_messages as $message){
			$mes_text.= "<tr>
			<td>".$message['type']."</td>
			<td>".$message['id']."</td>
			<td>".$message['msg']."</td>
			</tr>";
		}
		$mes_text.= "</table></p>";
		return $mes_text;
	} else return false;
}

//Returns array to ajax query with errors and pathes to pdf files and terminates script
function send_error()
{
	global $error_messages;
	global $data_error_messages;
	global $pdf_patheLabel;
	try
	{
		/*
		$formErrors = (string)formErrors($error_messages);
		$formDataErrors = (string)formDataErrors($data_error_messages);
		//*/
		$formErrors = $error_messages;
		$formDataErrors = $data_error_messages;
		
		if ($pdf_patheLabel && isset($pdf_patheLabel) && !empty($pdf_patheLabel))
		{
			$html = (string)$pdf_patheLabel;
			$html = urlencode($html);
		} else $html = 'false';
		
		$result = array(
		'path' => $html,
		'er' => $formErrors,
		'DataEr' => $formDataErrors
		);
		$result = json_encode($result);
		print_r($result);
	} catch (Exception $e){
		echo $e->getMessage();
	}
	die();
}
?>