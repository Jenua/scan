<?php
//PHP configuration
ini_set('max_execution_time', 7200);
ini_set("memory_limit", "2000M");

//start timer
//$start_time = microtime(true);   //comment this out!!!

//Include files
//Root path
$path = $_SERVER['DOCUMENT_ROOT'];

//DB connection class
require_once($path.'/db_connect/connect.php');

//Barcode library classes
require_once($path.'/shipping_module/Include/barcode/class/BCGFontFile.php');
require_once($path.'/shipping_module/Include/barcode/class/BCGColor.php');
require_once($path.'/shipping_module/Include/barcode/class/BCGDrawing.php');
require_once($path.'/shipping_module/Include/barcode/class/BCGupca.barcode.php');

//Twig template engine
require_once($path.'/shipping_module/Include/Twig/Autoloader.php');

if (isset($_REQUEST['po']) && !empty($_REQUEST['po']))
{
    // Get PONumber and RefNumber from global array
    $po = $_REQUEST['po'];
    
    // In case of single product generation check if we have product id
    $TxnLineID = false;
    if(isset($_REQUEST['TxnLineID']) && !empty($_REQUEST['TxnLineID']))
    {
        $TxnLineID = $_REQUEST['TxnLineID'];
    }
    
    //Initialize Twig
    Twig_Autoloader::register();
    $loader = new Twig_Loader_Filesystem('../templates');
    $twig = new Twig_Environment($loader, array(
        'cache'       => 'compilation_cache',
        'auto_reload' => true
    ));
    
    //Get DB data for this order
    $product_data = getProductData($po, $TxnLineID);
    
    //Check if data exist
    if($product_data)
    {
        //Assign drawing and qr code image for each product in base64 format
        foreach($product_data as $key => $row)
        {
            $product_data[$key]['upca_code'] = get_upca_code($row['UPC']);
        }

        //Array to store file names
        $files_generated = [];

        //Loop through products, generate files and save it to disk, and save file name to array
        foreach($product_data as $row)
        {
            $html = $twig->render('barcode_labelHDDC.html', array('data' => $row));
            file_put_contents("../Pdf/".$row['PONumber']."barcodes_".$row['TxnLineID']."_date(".date('Y-m-d').").html", $html);
            $files_generated[]= $row['PONumber']."barcodes_".$row['TxnLineID']."_date(".date('Y-m-d').").html";
        }

        //Encode to JSON format
        $files_generated = json_encode($files_generated);

        //Return JSON
        print_r($files_generated);    //uncomment this!!!

        //Stop timer
        //$time_elapsed_secs = microtime(true) - $start_time;   //comment this out!!!

        //Display timer result
        //echo "<br>Done! Time: ".date("H:i:s",$time_elapsed_secs)."<br>";   //comment this out!!!
    } else 
    {
        $result = "Can not get products data to generate barcode labels.";
        $result = json_encode($result);
        print_r($result);
    }
} else 
{
    $result = "Can not get PONumber or RefNumber to generate barcode labels.";
    $result = json_encode($result);
    print_r($result);
}

function getProductData($po, $TxnLineID)
{
    $conn = Database::getInstance()->dbc;
    
    // if isset product id
    $query_part = "";
    if($TxnLineID) $query_part = "AND groupdetail.[TxnLineID] = ".$conn->quote($TxnLineID);
    
    $query = "SELECT distinct 
	shiptrack.RefNumber,   
	shiptrack.PONumber, 
	groupdetail.[Quantity],
        groupdetail.[TxnLineID],
	[DL_valid_SKU].UPC,
	[DL_valid_SKU].HD_SKU,
        [DL_valid_SKU].SKU,
        [DL_valid_SKU].[Box_Count],
	CAST(groupdetail.[Quantity] AS INT)*CAST([DL_valid_SKU].[Box_Count] AS INT) AS QTY_BOX
	FROM [groupdetail]
		INNER JOIN shiptrack ON shiptrack.TxnID = groupdetail.IDKEY
		INNER JOIN [dbo].[DL_valid_SKU] ON [DL_valid_SKU].[QB_SKU] = groupdetail.[ItemGroupRef_FullName]
	WHERE shiptrack.PONumber = ".$conn->quote($po)." 
        ".$query_part;
    
    $result = $conn->prepare($query);
    $result->execute();
    $result = $result->fetchAll(PDO::FETCH_ASSOC);
    if(!empty($result)) return $result; else return false;
}

function get_upca_code($value)
{
    global $path;
    $uniqid = uniqid();
    try{
        // Loading Font
        $font = new BCGFontFile($path.'/shipping_module/Include/barcode/font/Arial.ttf', 18);

        // The arguments are R, G, B for color.
        $color_black = new BCGColor(0, 0, 0);
        $color_white = new BCGColor(255, 255, 255); 

        $code = new BCGupca();
        $code->setScale(3); // Resolution
        $code->setThickness(50); // Thickness
        $code->setForegroundColor($color_black); // Color of bars
        $code->setBackgroundColor($color_white); // Color of spaces
        $code->setFont($font); // Font (or 0)
        $code->parse($value); // Text
        $drawing = new BCGDrawing('', $color_white);
        $drawing->setBarcode($code);
        $drawing->draw();

	
        ob_start();
            $drawing->finish(BCGDrawing::IMG_FORMAT_PNG);
            $result = base64_encode( ob_get_contents() );
        ob_end_clean();
        
        unset($color_black);
        unset($color_white);
        
        return $result;
    } catch (Exception $e){
        $result = "Error. Can not generate UPC-A barcode. ".$e->getMessage();
        $result = json_encode($result);
        print_r($result);
        die();
    }
}