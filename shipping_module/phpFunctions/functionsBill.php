<?php
ini_set('max_execution_time', 7200);
ini_set("memory_limit", "2000M");
//Include files
require_once('../Include/barcode/class/BCGFontFile.php');
require_once('../Include/barcode/class/BCGcode39.barcode.php');
require_once('../Include/barcode/class/BCGcode128.barcode.php');
require_once('../Include/barcode/class/BCGColor.php');
require_once('../Include/barcode/class/BCGDrawing.php');
require_once('../Include/barcode/class/BCGpdf417.barcode2d.php');
require_once('../Include/fpdf17/fpdf.php');
require_once('../Include/tcpdf/tcpdf.php');
require_once('functionsPDFBillofLading.php');
require_once('functionsDB.php');
require_once('functionsBar.php');
require_once('functionsExtr.php');
$path = $_SERVER['DOCUMENT_ROOT'];
require_once($path.'/db_connect/connect.php');

//Global variables
$error_messages = array();//Array to save all errors
$data_error_messages = array();//Array to save all errors in input data
$connect = Database::getInstance()->dbc;
$pdf_patheBill = false;//path to pdf. False while not created

$settings = getSettings($connect);

foreach ($settings as $setting)
{
	if ($setting['group_name'] == 'bol_amazon' && $setting['type_name'] == 'IsPlaceAmazonSticker')
	{
		define("IsPlaceAmazonSticker", $setting['setting_value']);
	} else if ($setting['group_name'] == 'PrintCarrierForAmazon' && $setting['type_name'] == 'PrintCarrierForAmazonMode')
	{
		define("PrintCarrierForAmazonMode", $setting['setting_value']);
		//define("PrintCarrierForAmazonMode", '0');
	}
}

//Start if have PO number
if (isset($_REQUEST['value']) && !empty($_REQUEST['value']) && isset($_REQUEST['id']) && !empty($_REQUEST['id']))
	{
		main($_REQUEST['value'], $_REQUEST['id'], $_REQUEST['pro'], $_REQUEST['bol']);
		$connect = null;
	}

function checkProBol($pro, $bol)
{
	if (!issetProBolBill($pro, $bol))
	{
		error_msg("PRO# and INVOICE# data needed to generate Bill of Lading!");
		send_error();
	}
}


//Main function to execute functions kit depending on store name
function main($PONumber, $id, $pro, $bol)
{
	global $error_messages;
	global $data_error_messages;
	global $connect;
	global $pdf_patheBill;
	
	/*$pdf_path = "../PdfBill/".repPo($PONumber)."Bill.pdf";
	if (file_exists($pdf_path))
	{
		$pdf_patheBill[] = "PdfBill/".repPo($PONumber)."Bill.pdf";
		send_error();
	}*/
	
	
	
	$array = getProductCode($connect, $PONumber, $id);
	
	/*foreach($array as $key => $value)
	{
		$array[$key]['CustomerRef_FullName'] = 'Amazon';
	}*/
	
	$originCarrierName = 'none';
	/*print_r("<pre>");
	print_r($array);
	print_r("</pre>");
	send_error();
	die('stop');*/
	
	if (isset($array[0]['CARRIERNAME']) && !empty($array[0]['CARRIERNAME']) && $array[0]['CARRIERNAME'] != null)
	{
		$originCarrierName = $array[0]['CARRIERNAME'];
		$carrierName = extrCarrier($array[0]['CARRIERNAME']);
		$storeName = extrStore($array[0]['CustomerRef_FullName']);
		
		if($originCarrierName != 'UPS Ground')
		{
			/*$storeName = "Some store";
			$carrierName = "Some Carrier";
			$originCarrierName = "Some Carrier";*/
			/*print_r($storeName);
			die();*/
			
			switch ($storeName)
			{
			case "Depot":
				checkProBol($pro, $bol);
				$array = boxes($array);
				$array = dublic($array);
				$barcodeBill_pro10 = home_depot_carriers_gen_img($carrierName, $pro);
				$barcodeBill = $barcodeBill_pro10['barcodeBill'];
				$pro10 = $barcodeBill_pro10['pro10'];
				usort($array, 'ab');
				$pdf_patheBill = homedepotBill($array, $pro, $bol, $pro10, $barcodeBill, $originCarrierName);
				send_error();
				break;
			case "Wayfair":
				$array = boxes($array);
				$array = dublic($array);
				if (isset($pro) && !empty($pro)) 
				{
					$pro10 = $pro;
					$barcodeBill = gen_imageCode128wayfair($pro);
				} else{
					$pro10 = 'none';
					$barcodeBill = 'none';
				}
				$array = get_carrier_info($array, $originCarrierName, $connect);
				usort($array, 'ab');
				$pdf_patheBill = wayfairBill2($array, $pro, $bol, $pro10, $barcodeBill, $originCarrierName);
				send_error();
				break;
			case "AmazonDS":
				checkProBol($pro, $bol);
				$array = boxes($array);
				$array = dublic($array);
				$carrier = extrCarrier($array[0]['ShipMethodRef_FullName']);
				if ($carrier == 'CEVA') 
				{
					$pro10 = $pro;
					$barcodeBill = gen_imageFedex($pro10);
				} else if($carrier == 'ESTES')
				{
					$pro10 = $pro;
					$barcodeBill = gen_imageFedex($pro10);
				} else if($carrier == 'ABF')
				{
					$pro10 = $pro;
					$barcodeBill = gen_imageFedex($pro10);
				} else 
				{
					$pro10 = 'none';
					$barcodeBill = 'none';
				}
				
				usort($array, 'ab');
				$pdf_patheBill = amazonDSBill($array, $pro, $bol, $pro10, $barcodeBill, $originCarrierName);
				send_error();
				break;
			case "YOW":
				checkProBol($pro, $bol);
				$array = boxes($array);
				$array = dublic($array);
				$barcodeBill_pro10 = home_depot_carriers_gen_img($carrierName, $pro);
				$barcodeBill = $barcodeBill_pro10['barcodeBill'];
				$pro10 = $barcodeBill_pro10['pro10'];
				usort($array, 'ab');
				$pdf_patheBill = yowBill($array, $pro, $bol, $pro10, $barcodeBill, $originCarrierName);
				send_error();
				break;
			case "Sears":
				checkProBol($pro, $bol);
				$array = boxes($array);
				$array = dublic($array);
				$barcodeBill = '';
				$pro10 = '';
				$array = getValidSKU($connect, $array);
				usort($array, 'ab');
				$pdf_patheBill = searsBill($array, $pro, $bol, $pro10, $barcodeBill, $originCarrierName);
				send_error();
				break;
			/*case "BUILD.COM CONWAY":
				$array = boxes($array);
				$array = dublic($array);
				usort($array, 'ab');
				$pdf_patheBill = buildcomBill($array, $pro, $bol, $originCarrierName);
				send_error();
				break;*/
			default:
				switch ($carrierName)
				{
				case "FEDEX":
					checkProBol($pro, $bol);
					$pro10 = validatePro($pro);
                                         if($array[0]['CustomerRef_FullName'] == 'Amazon') 
                                        {
                                            $list_of_products = get_products_for_po_amazon($PONumber);
                                            if ($list_of_products) $array = filterAmazonSKUs($array, $list_of_products);
                                        } else if($storeName == "Lowe's" && strpos($array[0]['RefNumber'], 'combine_') !== false)
                                        {
                                            $list_of_products = get_products_for_po_lowes($PONumber);
                                            if ($list_of_products) $array = filterAmazonSKUs($array, $list_of_products);
                                        } else if ($array[0]['CustomerRef_FullName'] == 'Amazon.Canada' && $originCarrierName != 'Shipping Ground') 
                                        {
                                            $list_of_products = get_products_for_po_amazon_canada($PONumber);
                                            if ($list_of_products) $array = filterAmazonSKUs($array, $list_of_products);
                                        }
					$array = boxes($array);
					$array = dublic($array);
					$barcodeBill = gen_imageFedex($pro10);
					usort($array, 'ab');
					$pdf_patheBill = fedexBill($array, $pro, $bol, $pro10, $barcodeBill, $originCarrierName);
					send_error();
					break;
				case "RL":
					checkProBol($pro, $bol);
					$pro10 = validatePro($pro);
					$pro10 = str_replace("-", "", $pro10);
                                         if($array[0]['CustomerRef_FullName'] == 'Amazon') 
                                        {
                                            $list_of_products = get_products_for_po_amazon($PONumber);
                                            if ($list_of_products) $array = filterAmazonSKUs($array, $list_of_products);
                                        } else if($storeName == "Lowe's" && strpos($array[0]['RefNumber'], 'combine_') !== false)
                                        {
                                            $list_of_products = get_products_for_po_lowes($PONumber);
                                            if ($list_of_products) $array = filterAmazonSKUs($array, $list_of_products);
                                        } else if ($array[0]['CustomerRef_FullName'] == 'Amazon.Canada' && $originCarrierName != 'Shipping Ground') 
                                        {
                                            $list_of_products = get_products_for_po_amazon_canada($PONumber);
                                            if ($list_of_products) $array = filterAmazonSKUs($array, $list_of_products);
                                        }
                                        
                                        /*print_r('debug: ');
                                        print_r($storeName);
                                        print_r($array[0]['RefNumber']);
                                        print_r($list_of_products);
                                        print_r($array);*/
                                        
					$array = boxes($array);
					$array = dublic($array);
					$barcodeBill = gen_imageFedex($pro10);
					usort($array, 'ab');
					$pdf_patheBill = rlBill($array, $pro, $bol, $pro10, $barcodeBill, $originCarrierName);
					send_error();
					break;
				case "YRC":
					checkProBol($pro, $bol);
					$pro10 = validatePro($pro);
                                         if($array[0]['CustomerRef_FullName'] == 'Amazon') 
                                        {
                                            $list_of_products = get_products_for_po_amazon($PONumber);
                                            if ($list_of_products) $array = filterAmazonSKUs($array, $list_of_products);
                                        } else if($storeName == "Lowe's" && strpos($array[0]['RefNumber'], 'combine_') !== false)
                                        {
                                            $list_of_products = get_products_for_po_lowes($PONumber);
                                            if ($list_of_products) $array = filterAmazonSKUs($array, $list_of_products);
                                        } else if ($array[0]['CustomerRef_FullName'] == 'Amazon.Canada' && $originCarrierName != 'Shipping Ground') 
                                        {
                                            $list_of_products = get_products_for_po_amazon_canada($PONumber);
                                            if ($list_of_products) $array = filterAmazonSKUs($array, $list_of_products);
                                        }
					$array = boxes($array);
					$array = dublic($array);
					$barcodeBill = gen_imageFedex($pro10);
					usort($array, 'ab');
					$pdf_patheBill = yrcBill($array, $pro, $bol, $pro10, $barcodeBill, $originCarrierName);
					send_error();
					break;
				default:
					$barcodeBill = 'none';
					$pro10 = 'none';
					if(isset($pro) && !empty($pro) && $carrierName == 'UPS')
					{
						$pro10 = $pro;
						$barcodeBill = gen_imageFedex($pro10);
					}
                                        if($array[0]['CustomerRef_FullName'] == 'Amazon') 
                                        {
                                            $list_of_products = get_products_for_po_amazon($PONumber);
                                            if ($list_of_products) $array = filterAmazonSKUs($array, $list_of_products);
                                        } else if($storeName == "Lowe's" && strpos($array[0]['RefNumber'], 'combine_') !== false)
                                        {
                                            $list_of_products = get_products_for_po_lowes($PONumber);
                                            if ($list_of_products) $array = filterAmazonSKUs($array, $list_of_products);
                                        } else if ($array[0]['CustomerRef_FullName'] == 'Amazon.Canada' && $originCarrierName != 'Shipping Ground') 
                                        {
                                            $list_of_products = get_products_for_po_amazon_canada($PONumber);
                                            if ($list_of_products) $array = filterAmazonSKUs($array, $list_of_products);
                                        }
					$array = boxes($array);
					$array = dublic($array);
					/*print_r($array);
					die();*/
					usort($array, 'ab');
					$pdf_patheBill = genericBill($array, $pro, $bol, $pro10, $barcodeBill, $originCarrierName);
					send_error();
					break;
				}
				break;
			}
		}else{
			error_msg("This order was rated with UPS Groud web-service so BOL generating is disabled.");
			send_error();
		}
	} else
	{
			error_msg("CARRIERNAME needed to generate Bill of Lading.");
			send_error();
	}
	
	
	return send_error();
}

function home_depot_carriers_gen_img($carrierName, $pro)
{
	$barcodeBill = 'none';
	$pro10 = 'none';
	switch ($carrierName)
	{
    case 'ESTES':
		$pro10 = validatePro($pro);
		$barcodeBill = gen_imageFedex($pro10);
        break;
	case 'UPS':
		$pro10 = validatePro($pro);
		$barcodeBill = gen_imageFedex($pro10);
        break;
	case 'NEW ENGL':
		$pro10 = validatePro($pro);
		$barcodeBill = gen_imageFedex($pro10);
        break;
	case "YRC":
		$pro10 = validatePro($pro);
		$barcodeBill = gen_imageFedex($pro10);
		break;
	case 'ODFL':
		$pro10 = validatePro($pro);
		$barcodeBill = gen_imageFedex($pro10);
        break;
	}
	return array('barcodeBill' => $barcodeBill, 'pro10' => $pro10);
}

//is set pro and bol
function issetProBolBill($pro, $bol)
{
	if ((!isset($pro) || !isset($bol)) || (empty($pro) || empty($bol))) return false; else return true;
}

//Sort array
function ab($a,$b)
{
	return ($a['boxNumber']-$b['boxNumber']);
}

//Fills an array with error messages
function error_msg($msg)
{
	global $error_messages;
	$error_messages[] = $msg;
}

//Fills an array with errors in input data
function data_error_msg($type, $id, $msg)
{
	global $data_error_messages;
	$row = array('type' => $type, 'id' => $id, 'msg' => $msg);
	$data_error_messages[] = $row;
}

//Generate list of error messages
function formErrors($error_messages)
{
	if (isset($error_messages) && !empty($error_messages))
	{
		$mes_text = "<b><p style='font-size: 12pt; color: red;'>Bill of Lading:</p></b>";
		foreach($error_messages as $message){
			$mes_text.= "<b><p style='font-size: 10pt; color: black;'>".$message."</p></b>";
		}
		return $mes_text;
	} else return false;
}

//Generate table with errors in input data
function formDataErrors($data_error_messages)
{
	if (isset($data_error_messages) && !empty($data_error_messages))
	{
		$mes_text = "<b><p style='font-size: 12pt; color: red;'>Bill of Lading. Detailed problems:</p></b><p><table border='1' style='font-size: 12pt; color: black;'>
		<tr>
			<th>type</th>
			<th>data</th>
			<th>problem</th>
		</tr>";
		foreach($data_error_messages as $message){
			$mes_text.= "<tr>
			<td>".$message['type']."</td>
			<td>".$message['id']."</td>
			<td>".$message['msg']."</td>
			</tr>";
		}
		$mes_text.= "</table></p>";
		return $mes_text;
	} else return false;
}

//Returns array to ajax query with errors and pathes to pdf files and terminates script
function send_error()
{
	global $error_messages;
	global $data_error_messages;
	global $pdf_patheBill;
	try
	{
		$formErrors = (string)formErrors($error_messages);
		$formDataErrors = (string)formDataErrors($data_error_messages);

		if ($pdf_patheBill && isset($pdf_patheBill) && !empty($pdf_patheBill))
		{
			$html = (string)$pdf_patheBill[0];
			$html = urlencode($html);
		} else $html = 'false';
		$result = array(
		'path' => $html,
		'er' => $formErrors,
		'DataEr' => $formDataErrors
		);
		$result = json_encode($result);
		//$result = json_encode("Some php error.");
		print_r($result);
	} catch (Exception $e){
		echo $e->getMessage();
	}
	die();
}
?>