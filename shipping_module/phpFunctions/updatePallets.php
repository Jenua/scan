<?php
$path = $_SERVER['DOCUMENT_ROOT'];
require_once($path.'/db_connect/connect.php');
date_default_timezone_set('America/New_York');


	if (isset($_REQUEST['id']) && !empty($_REQUEST['id'])
		&& isset($_REQUEST['pallets']) && !empty($_REQUEST['pallets']))
	{
		$id = $_REQUEST['id'];
		$pallets = $_REQUEST['pallets'];

		$conn = Database::getInstance()->dbc;
		$query = "
		UPDATE [dbo].[ShipTrackWithLabel]
		   SET [PALLETS] = ".$pallets."
		 WHERE [ID] = '".$id."'";
		$res = exec_query($conn, $query);
		if ($res)
		{
			print_r(json_encode('pallets_updated'));
		} else
		{
			print_r(json_encode('Error. Can not update pallets.'));
		}
		$conn = null;
	} else print_r(json_encode('Error. No id or pallets number.'));

function exec_query($conn, $query)
{
	try{
		$result = $conn->prepare($query);
		$result->execute();
		return $result;
	}
	catch(PDOException $e)
	{
		print_r($e->getMessage());
	}
}
?>