<?php
session_start();
$path = $_SERVER['DOCUMENT_ROOT'];
require_once($path.'/db_connect/connect.php');
require_once($path.'/auth/auth.php');
date_default_timezone_set('America/New_York');
$auth = new AuthClass();

if ($auth->isAuth()) {
    if( isset($_REQUEST['pwd']) && !empty($_REQUEST['pwd']) && isset($_REQUEST['newpwd']) && !empty($_REQUEST['newpwd']) && isset($_REQUEST['confirmpwd']) && !empty($_REQUEST['confirmpwd'])) {
        $id       = $auth->getId();
        $oldPass  = trim($_REQUEST['pwd']);
	$newPass1 = trim($_REQUEST['newpwd']);
        $newPass2 = trim($_REQUEST['confirmpwd']);
        if( $newPass1 == $newPass2 ) {
            $conn = Database::getInstance()->dbc;
            $query0 = "SELECT [password]
                         FROM [dbo].[".USER_TABLE."]
                        WHERE [ID] = ".$conn->quote($id);
            
            $res0 = exec_query($conn, $query0);
            $res0 = $res0->fetch(PDO::FETCH_ASSOC);
            if ($res0 && $res0['password'] == $oldPass)
            {
                echo json_encode("Updating password");
                $query1 = "UPDATE [dbo].[".USER_TABLE."]
                              SET [password] = ".$conn->quote($newPass1)."
                            WHERE [ID] = ".$id;
                
                $res1 = exec_query($conn, $query1);
                if ($res1) {
                    echo json_encode("Password successfully updated");
                } else {
                    echo json_encode("Error: New password is not updated. Try again later.");
                }
            } else {
                echo json_encode("Error: Incorrect password");
            }
        } else { echo json_encode("Error: Passwords dont match"); }
    } else {
        echo json_encode("Error: Wrong Data");
    }
    
    $conn = null;
} else {
    echo json_encode("Error: Not Authorized");
}

function exec_query($conn, $query)
{
	try{
		$result = $conn->prepare($query);
		$result->execute();
		return $result;
	}
	catch(PDOException $e)
	{
		print_r($e->getMessage());
	}
}