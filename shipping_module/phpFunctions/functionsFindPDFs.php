<?php
$path = $_SERVER['DOCUMENT_ROOT'];
require_once($path.'/db_connect/connect.php');
require_once('functionsExtr.php');

	if (isset($_REQUEST['po']) && !empty($_REQUEST['po']))
	{
		$po = $_REQUEST['po'];
		$files_array = array();
		$conn = Database::getInstance()->dbc;
		$query0 = "SELECT [Orders_combined].[PONumber], [Orders_combined].[Combined_into_POnumber]
						  FROM [dbo].[Orders_combined]
						  INNER JOIN [dbo].[manually_shiptrack] ON [Orders_combined].[Combined_into_RefNumber] = [manually_shiptrack].[RefNumber]
						  WHERE [Combined_into_POnumber] IN
							(
								SELECT [Combined_into_POnumber]
								FROM [dbo].[Orders_combined]
								WHERE 
								[Orders_combined].[PONumber] = '".$po."' 
								OR [Orders_combined].[Combined_into_POnumber] = '".$po."'
							)
							group by [Orders_combined].[PONumber], [Orders_combined].[Combined_into_POnumber]";
		$res0 = execQuery($conn, $query0);
		
		if(isset($res0) && !empty($res0))
		{
			$bol_po = $res0[0]['Combined_into_POnumber'];
			$bol_po = repPo($bol_po);
			$bol_po = trim($bol_po);
                        
			$bolName = '../PdfBill/'.$bol_po.'Bill.pdf';
			$bolName2 = 'PdfBill/'.$bol_po.'Bill.pdf';
                        
			$isBolExists = findFile($bolName);
			if ($isBolExists)
			{
				$files_array[]=array('type' => 'bol', 'path' => urlencode($bolName2));
			}
			
			foreach($res0 as $value)
			{
				$labelName = '../Pdf/'.$value['PONumber'].'.pdf';
				$labelName2 = 'Pdf/'.$value['PONumber'].'.pdf';
                $flyerName = '../Pdf/'.$value['PONumber'].'flyer.pdf';
				$flyerName2 = 'Pdf/'.$value['PONumber'].'flyer.pdf';
                                
				$isLabelExists = findFile($labelName);
                                $isFlyerExists = findFile($flyerName);
                                
				if ($isLabelExists)
				{
					$files_array[]=array('type' => 'label', 'path' => urlencode($labelName2), 'po' => $value['PONumber']);
				}
                if ($isFlyerExists)
				{
					$files_array[]=array('type' => 'flyer', 'path' => urlencode($flyerName2), 'po' => $value['PONumber']);
				}
			}
			if(!empty($files_array))
			{
				$returnValue = json_encode($files_array);
				print_r($returnValue);
			}else{
				$returnValue = 'No files';
				$returnValue = json_encode($returnValue);
				print_r($returnValue);
			}
		} else
		{
			$returnValue = 'Can not get combined orders PO numbers';
			$returnValue = json_encode($returnValue);
			print_r($returnValue);
		}
	} else 
	{
		$returnValue = 'Can not get parameters for request';
		$returnValue = json_encode($returnValue);
		print_r($returnValue);
	}

function findFile($filename)
{
	if (file_exists($filename)) {
     return true;
	} else
	{
		return false;
	}
}

function execQuery($conn, $query)
{
	try{
		$result = $conn->prepare($query);
		$result->execute();
		$result = $result->fetchAll(PDO::FETCH_ASSOC); 
		return $result;
	}
	catch(PDOException $e)
	{
		print_r($e->getMessage());
	}
}