<?php

$path = $_SERVER['DOCUMENT_ROOT'];
require_once($path . '/db_connect/connect.php');
date_default_timezone_set('America/New_York');

//Connection to DB
function con() {
	$conn = Database::getInstance()->dbc;
	return $conn;
}
/*
function getMasterCartonableSKUs()
{
    $conn = Database::getInstance()->dbc;
    $query = "SELECT [SKU]
  FROM [dbo].[master_carton_sku]";
   $result = $conn->prepare($query);
	$result->execute();
	$result = $result->fetchAll(PDO::FETCH_ASSOC);
        $return_result = [];
        foreach($result as $row)
        {
            $return_result[]=$row['SKU'];
        }
        if (!empty($return_result)) return $return_result; else return false;
}
*/

function getPalletsTrucks($po, $ref)
{
    $conn = Database::getInstance()->dbc;
    $query = "SELECT * FROM [dbo].[truck_preload] WHERE [PONUMBER] = '".$po."' AND [REFNUMBER] = '".$ref."' ORDER BY LEN([TRUCK]), [TRUCK], LEN([PALLET]), [PALLET] ;";
    //print_r($query);
    $result = $conn->prepare($query);
    $result->execute();
    $result = $result->fetchAll(PDO::FETCH_ASSOC);
    if($result) return $result; else return false;
}

function get_products_for_po($PONumber)
{
    $conn = Database::getInstance()->dbc;
    $query = "SELECT [groupdetail].[TxnLineID], 1 as [Box_Count] /*[DL_valid_SKU].[Box_Count]*/
  FROM [dbo].[shiptrack]
  LEFT JOIN [dbo].[groupdetail] ON [shiptrack].[TxnID] = [groupdetail].[IDKEY]
  /*LEFT JOIN [dbo].[DL_valid_SKU] ON [groupdetail].[ItemGroupRef_FullName] = [DL_valid_SKU].[QB_SKU]*/
  WHERE [groupdetail].[ItemGroupRef_FullName] LIKE 'DLT%'
  AND [shiptrack].[PONumber] = '".$PONumber."'";
    $result = $conn->prepare($query);
    $result->execute();
    $result = $result->fetchAll(PDO::FETCH_ASSOC);
    if($result) return $result; else return false;
}

function get_products_for_po_amazon($PONumber)
{
    $conn = Database::getInstance()->dbc;
    $query = "SELECT * FROM
(
SELECT [groupdetail].[TxnLineID], [DL_valid_SKU].[Box_Count]
  FROM [dbo].[shiptrack]
  LEFT JOIN [dbo].[groupdetail] ON [shiptrack].[TxnID] = [groupdetail].[IDKEY]
  LEFT JOIN [dbo].[DL_valid_SKU] ON [groupdetail].[ItemGroupRef_FullName] = [DL_valid_SKU].[QB_SKU]
  WHERE ([groupdetail].[ItemGroupRef_FullName] IN 
  (
    SELECT [SKU]
    FROM [dbo].[master_carton]
    WHERE [DEALER] like 'Amazon'
  ) OR [groupdetail].[ItemGroupRef_FullName] LIKE 'DLT%')
  AND [shiptrack].[PONumber] = '".$PONumber."'
  UNION ALL
  SELECT [manually_groupdetail].[TxnLineID], [DL_valid_SKU].[Box_Count]
  FROM [dbo].[manually_shiptrack]
  LEFT JOIN [dbo].[manually_groupdetail] ON [manually_shiptrack].[TxnID] = [manually_groupdetail].[IDKEY]
  LEFT JOIN [dbo].[DL_valid_SKU] ON [manually_groupdetail].[ItemGroupRef_FullName] = [DL_valid_SKU].[QB_SKU]
  WHERE ([manually_groupdetail].[ItemGroupRef_FullName] IN 
  (
    SELECT [SKU]
    FROM [dbo].[master_carton]
    WHERE [DEALER] like 'Amazon'
  ) OR [manually_groupdetail].[ItemGroupRef_FullName] LIKE 'DLT%')
  AND [manually_shiptrack].[PONumber] = '".$PONumber."') A GROUP BY [TxnLineID], [Box_Count]";
    $result = $conn->prepare($query);
    $result->execute();
    $result = $result->fetchAll(PDO::FETCH_ASSOC);
    if($result) return $result; else return false;
}

function get_products_for_po_amazon_canada($PONumber)
{
    $conn = Database::getInstance()->dbc;
    $query = "SELECT * FROM
(
SELECT [groupdetail].[TxnLineID], [DL_valid_SKU].[Box_Count]
  FROM [dbo].[shiptrack]
  LEFT JOIN [dbo].[groupdetail] ON [shiptrack].[TxnID] = [groupdetail].[IDKEY]
  LEFT JOIN [dbo].[DL_valid_SKU] ON [groupdetail].[ItemGroupRef_FullName] = [DL_valid_SKU].[QB_SKU]
  WHERE ([groupdetail].[ItemGroupRef_FullName] IN 
  (
    SELECT [SKU]
    FROM [dbo].[master_carton]
    WHERE [DEALER] like 'Amazon.Canada'
  ) OR [groupdetail].[ItemGroupRef_FullName] LIKE 'DLT%')
  AND [shiptrack].[PONumber] = '".$PONumber."'
  UNION ALL
  SELECT [manually_groupdetail].[TxnLineID], [DL_valid_SKU].[Box_Count]
  FROM [dbo].[manually_shiptrack]
  LEFT JOIN [dbo].[manually_groupdetail] ON [manually_shiptrack].[TxnID] = [manually_groupdetail].[IDKEY]
  LEFT JOIN [dbo].[DL_valid_SKU] ON [manually_groupdetail].[ItemGroupRef_FullName] = [DL_valid_SKU].[QB_SKU]
  WHERE ([manually_groupdetail].[ItemGroupRef_FullName] IN 
  (
    SELECT [SKU]
    FROM [dbo].[master_carton]
    WHERE [DEALER] like 'Amazon.Canada'
  ) OR [manually_groupdetail].[ItemGroupRef_FullName] LIKE 'DLT%')
  AND [manually_shiptrack].[PONumber] = '".$PONumber."') A GROUP BY [TxnLineID], [Box_Count]";
    $result = $conn->prepare($query);
    $result->execute();
    $result = $result->fetchAll(PDO::FETCH_ASSOC);
    if($result) return $result; else return false;
}

function get_products_for_po_lowes($PONumber)
{
    $conn = Database::getInstance()->dbc;
    $query = "SELECT * FROM
(
SELECT [groupdetail].[TxnLineID], [DL_valid_SKU].[Box_Count]
  FROM [dbo].[shiptrack]
  LEFT JOIN [dbo].[groupdetail] ON [shiptrack].[TxnID] = [groupdetail].[IDKEY]
  LEFT JOIN [dbo].[DL_valid_SKU] ON [groupdetail].[ItemGroupRef_FullName] = [DL_valid_SKU].[QB_SKU]
  WHERE ([groupdetail].[ItemGroupRef_FullName] IN 
  (
    SELECT [SKU]
    FROM [dbo].[master_carton]
    WHERE [DEALER] like 'Lowes'
  ) OR [groupdetail].[ItemGroupRef_FullName] LIKE 'DLT%')
  AND [shiptrack].[PONumber] = '".$PONumber."'
  UNION ALL
  SELECT [manually_groupdetail].[TxnLineID], [DL_valid_SKU].[Box_Count]
  FROM [dbo].[manually_shiptrack]
  LEFT JOIN [dbo].[manually_groupdetail] ON [manually_shiptrack].[TxnID] = [manually_groupdetail].[IDKEY]
  LEFT JOIN [dbo].[DL_valid_SKU] ON [manually_groupdetail].[ItemGroupRef_FullName] = [DL_valid_SKU].[QB_SKU]
  WHERE ([manually_groupdetail].[ItemGroupRef_FullName] IN 
  (
    SELECT [SKU]
    FROM [dbo].[master_carton]
    WHERE [DEALER] like 'Lowes'
  ) OR [manually_groupdetail].[ItemGroupRef_FullName] LIKE 'DLT%')
  AND [manually_shiptrack].[PONumber] = '".$PONumber."') A GROUP BY [TxnLineID], [Box_Count]";
  //print_r($query);
    $result = $conn->prepare($query);
    $result->execute();
    $result = $result->fetchAll(PDO::FETCH_ASSOC);
    if($result) return $result; else return false;
}

function return_amazon_qty($array, $GroupIDKEYs)
{
	foreach($GroupIDKEYs as $key_gik => $GIK)
	{
		foreach($array as $key => $row)
		{
			if ($key_gik == $row['GroupIDKEY']) $array[$key]['quantity'] = $GIK['amazon_quantity']; //wrong! multiply amazon_quantity by quantity
		}
	}
	return $array;
}

function filterAmazonSKUs($array, $list_of_products) {
    $arary_master_carton = [];
    $array_filtered_txn = [];
    foreach($array as $key => $value)
    {
        foreach($list_of_products as $TxnLineID)
        {
            if($TxnLineID['TxnLineID'] === $value['TxnLineID'])
            {
                if(!in_array($TxnLineID['TxnLineID'], $array_filtered_txn))
                {
                    $array_filtered_txn[]=$TxnLineID['TxnLineID'];
                    $value['Box_Count']= $TxnLineID['Box_Count'];
                    $arary_master_carton[]=$value;
                }
                unset($array[$key]);
            }
        }
    }

    foreach($arary_master_carton as $key => $value)
    {
        $value['quantity'] = $value['group_quantity'];
        for($i=0;$i<$value['Box_Count'];$i++)
        {
            $array[]=$value;
        }
    }
    
    return array_values($array);
}

function fergusonAccount($value) {
	/*$conn = Database::getInstance()->dbc;
	$value = extrCarrier($value);
	$query = "SELECT *
  FROM [dbo].[ferguson_accounts]
  WHERE [key] = '" . $value . "'";
	$result = $conn->prepare($query);
	$result->execute();
	$result = $result->fetchAll(PDO::FETCH_ASSOC);
	if (!empty($result))
		return $result[0]['account'];
	else*/
		return false;
}

function saveQrCode($arr) {
	$curdate = date("Y-m-d H:i:s");
	$conn = con();
	foreach ($arr as $row) {
		if (isset($row['code']) && !empty($row['code'])) {
			$query = "INSERT INTO [dbo].[qr_code]
           ([po]
           ,[sku]
           ,[code]
		   ,[gen_date])
     VALUES
           ('" . $row['PONumber'] . "'
           ,'" . $row['SKU'] . "'
           ,'" . $row['code'] . "'
		   ,'" . $curdate . "')";
			execQuery($conn, $query);
		}
	}
}

function get_carrier_info($array, $storeName, $conn) {
	$query = "SELECT 
	[ShipMethodRef_fullname], 
	[carrier_info].[phone], 
	[WFR_SCAC], 
	'3rd Party' as [FCT] 
	FROM [" . DATABASE31 . "].[dbo].[SCAC] 
	LEFT JOIN [" . DATABASE . "].[dbo].[carrier_info] ON [SCAC].[WFR_SCAC] = [carrier_info].[SCAC]
	WHERE [ShipMethodRef_fullname] = '" . $storeName . "'";
	$result = $conn->prepare($query);
	$result->execute();
	$result = $result->fetchAll(PDO::FETCH_ASSOC);
	/* print_r($result);
	  die(); */
	foreach ($array as $key => $row) {
		if (!empty($result[0]['ShipMethodRef_fullname']))
			$array[$key]['carrier_info_name'] = $result[0]['ShipMethodRef_fullname'];
		else
			$array[$key]['carrier_info_name'] = '';
		if (!empty($result[0]['phone']))
			$array[$key]['carrier_info_phone'] = $result[0]['phone'];
		else
			$array[$key]['carrier_info_phone'] = '';
		if (!empty($result[0]['WFR_SCAC']))
			$array[$key]['carrier_info_SCAC'] = $result[0]['WFR_SCAC'];
		else
			$array[$key]['carrier_info_SCAC'] = '';
		if (!empty($result[0]['FCT']))
			$array[$key]['carrier_info_FCT'] = $result[0]['FCT'];
		else
			$array[$key]['carrier_info_FCT'] = '';
	}

	return $array;
}

function getProductsWarnings($conn, $id) {
	try {
		$query = "SELECT 
/*[ShipTrackWithLabel].[ID],*/
[itemgroup].[Name],
[itemgroup].[CustomField6]
FROM [" . DATABASE31 . "].[dbo].[salesorderlinegroupdetail]
LEFT JOIN [" . DATABASE31 . "].[dbo].[itemgroup] ON [itemgroup].[Name] = [salesorderlinegroupdetail].[ItemGroupRef_FullName]
INNER JOIN [" . DATABASE31 . "].[dbo].[salesorder] ON [salesorderlinegroupdetail].[IDKEY] = [salesorder].[TxnID]
INNER JOIN [" . DATABASE . "].[dbo].[ShipTrackWithLabel] ON [ShipTrackWithLabel].[PONUMBER] = [salesorder].[PONumber]
WHERE [ShipTrackWithLabel].[ID] = '" . $id . "'
AND 
(
	[itemgroup].[CustomField6] LIKE '%DO NOT SHIP%'
	OR [itemgroup].[CustomField6] LIKE '%SHIP GLASS OD205 AND BELOW%'
	OR [itemgroup].[CustomField6] LIKE '%SHIP GLASS OD206 AND ABOVE%'
)";
		$result = $conn->prepare($query);
		$result->execute();
		$result = $result->fetchAll(PDO::FETCH_ASSOC);
		$resultArray = array();
		if (isset($result) && !empty($result)) {
			foreach ($result as $key => $value) {
				$resultArray[] = 'ATTENTION: ' . $value['Name'] . ': ' . $value['CustomField6'];
			}
		}
		return $resultArray;
	} catch (PDOException $e) {
		return false;
	}
}

function getItemInfo($arr) {
	$conn = con();
	foreach ($arr as $key => $row) {
		$query = "SELECT top 1
      [name]
      ,[drawing]
      ,[dimensions]
  FROM [dbo].[item_info] WHERE [sku] = '" . $row['ItemRef_FullName'] . "'";
		$result = $conn->prepare($query);
		$result->execute();
		$result = $result->fetchAll(PDO::FETCH_ASSOC);
		//print_r($result);
		if (!empty($result[0]['dimensions'])) {
			$arr[$key]['collection_name'] = $result[0]['name'];
			$arr[$key]['drawing'] = $result[0]['drawing'];

			$dimensions = explode(",", $result[0]['dimensions']);
			foreach ($dimensions as $key2 => $dimension) {
				$dimensions[$key2] = trim($dimension);
			}
		} else {
			$arr[$key]['collection_name'] = null;
			$arr[$key]['drawing'] = null;
			$dimensions = null;
		}
		$arr[$key]['dimensions'] = $dimensions;
	}
	return $arr;
}

function getSettings($conn) {
	$query = "SELECT * FROM " . GENERAL_SETTINGS . " WHERE [app] = 'shipping_module' ORDER BY [group_name]";
	$result = $conn->prepare($query);
	$result->execute();
	$result = $result->fetchAll(PDO::FETCH_ASSOC);
	if (!isset($result) || empty($result))
		die('Can not get settings.');
	return $result;
}

//Execute input query
function execQuery($connect, $query) {
	try {
		$result = $connect->prepare($query);
		$result->execute();
		//print_r($result);
		return $result;
	} catch (Exception $e) {
		echo '<br>' . $e . '<br>';
		return false;
	}
}

function getProductCodeHomeDepotDCpickinglist($connect, $PONumber, $txnlineid) {
	$query = "SELECT [groupdetail].[TxnLineID]
      ,[groupdetail].[ItemGroupRef_FullName]
      ,[groupdetail].[Prod_desc]
      ,[groupdetail].[Quantity]
	  ,[linedetail].[TxnLineID] as [item_id]
	  ,[linedetail].[ItemRef_FullName]
	  ,[linedetail].[quantity] as [item_quantity]
	  ,[shiptrack].[PONumber]
  FROM [dbo].[groupdetail]
  LEFT JOIN [dbo].[linedetail] ON [groupdetail].[TxnLineID] = [linedetail].[GroupIDKEY]
  LEFT JOIN [dbo].[shiptrack] ON [groupdetail].[IDKEY] = [shiptrack].[TxnID]
  WHERE [groupdetail].[TxnLineID] = '" . $txnlineid . "'
  and
						 (
						  [linedetail].ItemRef_FullName not like '%Price-Adjustment%' 
						  and [linedetail].ItemRef_FullName not like '%Freight%'
						  and [linedetail].ItemRef_FullName not like '%warranty%' 
						  and [linedetail].ItemRef_FullName is not NULL
						  and [linedetail].ItemRef_FullName not like '%Subtotal%'
						  and [linedetail].ItemRef_FullName not like '%IDSC-10%'
						  and [linedetail].ItemRef_FullName not like '%DISCOUNT%'
						  and [linedetail].ItemRef_FullName not like '%SPECIAL ORDER4%'
						  and [linedetail].ItemRef_FullName not like '%Manual%'
						  )";
	$result = execQuery($connect, $query);
	$result = $result->fetchAll(PDO::FETCH_ASSOC);
	if (isset($result) && !empty($result)) {
		return $result;
	} else {
		error_msg("Can not get data for this order!");
		send_error();
	}
}

//Get all data from DB
function getProductCodeHomeDepotDCLabel($connect, $PONumber, $txnlineid) {

	$query = "SELECT 
	shiptrack.RefNumber,   
	shiptrack.PONumber,	   
	shiptrack.CustomerRef_FullName,	
	groupdetail.ItemGroupRef_FullName,
	groupdetail.[Quantity],
	[DL_valid_SKU].UPC,
	[DL_valid_SKU].HD_SKU as SKU,
        [DL_valid_SKU].[Box_Count],
	[item_info].[name] as [collection_name],
	[item_info].[drawing],
	[item_info].[dimensions],
	CAST(groupdetail.[Quantity] AS INT)*CAST([DL_valid_SKU].[Box_Count] AS INT) AS QTY_BOX
	FROM [groupdetail]
		INNER JOIN shiptrack ON shiptrack.TxnID = groupdetail.IDKEY
		INNER JOIN [dbo].[DL_valid_SKU] ON [DL_valid_SKU].[QB_SKU] = groupdetail.[ItemGroupRef_FullName]
		INNER JOIN [dbo].[item_info] ON [DL_valid_SKU].[SKU] = [item_info].[sku]
	WHERE 
		[groupdetail].[TxnLineID] = '" . $txnlineid . "' 
		AND [DL_valid_SKU].[UPC] IS NOT NULL
UNION ALL
SELECT 
	manually_shiptrack.RefNumber,   
	manually_shiptrack.PONumber,	   
	manually_shiptrack.CustomerRef_FullName,	
	manually_groupdetail.ItemGroupRef_FullName,
	manually_groupdetail.[Quantity],
	[DL_valid_SKU].UPC,
	[DL_valid_SKU].HD_SKU as SKU,
        [DL_valid_SKU].[Box_Count],
	[item_info].[name] as [collection_name],
	[item_info].[drawing],
	[item_info].[dimensions],
	CAST(manually_groupdetail.[Quantity] AS INT)*CAST([DL_valid_SKU].[Box_Count] AS INT) AS QTY_BOX
	FROM [manually_groupdetail]
		INNER JOIN manually_shiptrack ON manually_shiptrack.TxnID = manually_groupdetail.IDKEY
		INNER JOIN [dbo].[DL_valid_SKU] ON [DL_valid_SKU].[QB_SKU] = manually_groupdetail.[ItemGroupRef_FullName]
		INNER JOIN [dbo].[item_info] ON [DL_valid_SKU].[SKU] = [item_info].[sku]
	WHERE 
		manually_groupdetail.[TxnLineID] = '" . $txnlineid . "' 
		AND [DL_valid_SKU].[UPC] IS NOT NULL";

	/* print_r('<pre>');
	  print_r($query);
	  print_r('</pre>');
	  die(); */
	$result = execQuery($connect, $query);
	$result = $result->fetchAll(PDO::FETCH_ASSOC);
	if (isset($result) && !empty($result)) {
		return $result;
	} else {
		error_msg("Can not get data for this TxnLineID! Check groupdetail, DL_valid_SKU, item_info tables.");
		data_error_msg("TxnLineID", $txnlineid, "No such TxnLineID found!");
		send_error();
	}
}

function setDoneHomeDepotDC($connect, $PONumber, $txnlineid, $status) {
	$query = "
        IF EXISTS (
            SELECT * FROM [dbo].[hddc_checker] WHERE [PONUMBER] = '" . $PONumber . "' AND [TxnLineID] = '" . $txnlineid . "'
        )
        BEGIN
            UPDATE [dbo].[hddc_checker] 
            SET [Status] = '" . $status . "'
            WHERE [PONUMBER] = '" . $PONumber . "' AND [TxnLineID] = '" . $txnlineid . "'
        END
        ELSE
        BEGIN
            INSERT INTO [dbo].[hddc_checker]([PONUMBER], [TxnLineId], [Status])
            VALUES('" . $PONumber . "', '" . $txnlineid . "', '" . $status . "')
        END;";

	$stmt = $connect->prepare($query);
	$result = $stmt->execute();
	return $result;
}
function setDiscHomeDepotDC($connect, $PONumber, $txnlineid, $discontinued) {
	$query = "
        IF EXISTS (
            SELECT * FROM [dbo].[hddc_checker] WHERE [PONUMBER] = '" . $PONumber . "' AND [TxnLineID] = '" . $txnlineid . "'
        )
        BEGIN
            UPDATE [dbo].[hddc_checker] 
            SET [Discontinued] = '" . $discontinued . "'
            WHERE [PONUMBER] = '" . $PONumber . "' AND [TxnLineID] = '" . $txnlineid . "'
        END
        ELSE
        BEGIN
            INSERT INTO [dbo].[hddc_checker]([PONUMBER], [TxnLineId], [Discontinued])
            VALUES('" . $PONumber . "', '" . $txnlineid . "', '" . $discontinued . "')
        END;";

	$stmt = $connect->prepare($query);
	$result = $stmt->execute();
	return $result;
}


//Get all data from DB
function getProductCodeHomeDepotDC($connect, $PONumber, $txnlineid) {

	$query = "SELECT 
	shiptrack.RefNumber,   
	shiptrack.PONumber,	   
	shiptrack.CustomerRef_FullName, 
	shiptrack.ShipMethodRef_FullName,
	shiptrack.ShipAddress_Addr1, 
	shiptrack.ShipAddress_Addr2, 
	shiptrack.ShipAddress_Addr3, 
	shiptrack.ShipAddress_Addr4, 
	shiptrack.ShipAddress_Addr5, 
	shiptrack.ShipAddress_City, 
	shiptrack.ShipAddress_State, 
	shiptrack.ShipAddress_Country,
	shiptrack.ShipAddress_PostalCode, 
	shiptrack.FOB,
	shiptrack.TimeCreated, 
	shiptrack.Memo, 
	groupdetail.ItemGroupRef_FullName, 
	groupdetail.Prod_desc, 
	linedetail.quantity,
	linedetail.ItemRef_FullName,
	[DL_valid_SKU].UPC,
	[DL_valid_SKU].HD_SKU as SKU,
	[item_info].[name] as [collection_name],
	[item_info].[drawing],
	[item_info].[dimensions],
	case when linedetail.Quantity is NULL then 1*cast(replace(Replace(replace(replace(linedetail.CustomField9,'lbs', ''), 'lb', ''), ' lbs', ''),' lb','') as int) 
 	else  linedetail.Quantity*cast(replace(Replace(replace(replace(linedetail.CustomField9,'lbs', ''), 'lb', ''), ' lbs', ''),' lb','') as int) end as itemWeight,
	NULL as CARRIERNAME,
	NULL as WEIGHT,
	NULL as PALLETS,
	NULL as SERIAL_REFERENCE,
	NULL as RESIDENTIAL,
	NULL as LIFTGATE,
	NULL as delivery_notification,
	NULL as [original_po]
	FROM [groupdetail]
		LEFT JOIN shiptrack ON shiptrack.TxnID = groupdetail.IDKEY
		LEFT JOIN linedetail ON [groupdetail].TxnLineID = linedetail.GroupIDKEY
		LEFT JOIN [dbo].[DL_valid_SKU] ON [DL_valid_SKU].[QB_SKU] = groupdetail.[ItemGroupRef_FullName]
		LEFT JOIN [dbo].[item_info] ON [DL_valid_SKU].[SKU] = [item_info].[sku]
	WHERE 
		linedetail.ItemRef_FullName not like '%Price-Adjustment%'   and 
		linedetail.ItemRef_FullName not like '%Freight%'            and
		linedetail.ItemRef_FullName not like '%warranty%'           and        
		linedetail.ItemRef_FullName is not NULL                     and
		linedetail.ItemRef_FullName not like '%Subtotal%'           and
		linedetail.ItemRef_FullName not like '%IDSC-10%'            and
		linedetail.ItemRef_FullName not like '%DISCOUNT%'           and
		linedetail.ItemRef_FullName not like '%SPECIAL ORDER%'    	and 	
		linedetail.CustomField9 is not NULL							and 
		linedetail.GroupIDKEY = groupdetail.TxnLineID  				and
		[groupdetail].[TxnLineID] = '" . $txnlineid . "'";

	/* print_r('<pre>');
	  print_r($query);
	  print_r('</pre>');
	  die(); */
	try {
		$result = execQuery($connect, $query);
		$res = array();
		while ($row = $result->fetch()) {
			$phoneNumber = preg_replace("/[^0-9]/", "", $row['FOB']);
			$resultPhoneNumber = substr($phoneNumber, 0, 3) . '-' . substr($phoneNumber, 3, 3) . '-' . substr($phoneNumber, 6, 4);

			$arr = array(
				'RefNumber' => $row['RefNumber'],
				'PONumber' => strtoupper($row['PONumber']),
				'ProductCode' => $row['ItemGroupRef_FullName'],
				'Descr' => $row['Prod_desc'],
				'CustomerRef_FullName' => $row['CustomerRef_FullName'],
				'ShipMethodRef_FullName' => $row['ShipMethodRef_FullName'],
				'ItemRef_FullName' => $row['ItemRef_FullName'],
				'quantity' => $row['quantity'],
				'ShipAddress_Addr1' => $row['ShipAddress_Addr1'],
				'ShipAddress_Addr2' => $row['ShipAddress_Addr2'],
				'ShipAddress_Addr3' => $row['ShipAddress_Addr3'],
				'ShipAddress_Addr4' => $row['ShipAddress_Addr4'],
				'ShipAddress_Addr5' => $row['ShipAddress_Addr5'],
				'ShipAddress_City' => $row['ShipAddress_City'],
				'ShipAddress_State' => $row['ShipAddress_State'],
				'ShipAddress_Country' => $row['ShipAddress_Country'],
				'ShipAddress_PostalCode' => $row['ShipAddress_PostalCode'],
				'FOB' => $resultPhoneNumber,
				'TimeCreated' => $row['TimeCreated'],
				'Memo' => $row['Memo'],
				'CARRIERNAME' => $row['CARRIERNAME'],
				'WEIGHT' => $row['WEIGHT'],
				'PALLETS' => $row['PALLETS'],
				'SERIAL_REFERENCE' => $row['SERIAL_REFERENCE'],
				'RESIDENTIAL' => $row['RESIDENTIAL'],
				'LIFTGATE' => $row['LIFTGATE'],
				'delivery_notification' => $row['delivery_notification'],
				'itemWeight' => $row['itemWeight'],
				'original_po' => $row['original_po'],
				'UPC' => $row['UPC'],
				'SKU' => $row['SKU'],
				'collection_name' => $row['collection_name'],
				'drawing' => $row['drawing'],
				'dimensions' => $row['dimensions']
			);
			$res[] = $arr;
		}
		if (isset($res) && !empty($res)) {
			/* print_r($res);
			  die(); */
			return $res;
		} else {
			error_msg("Can not get data for this order! This order may have already been processed. Try again in a few minutes.");
			data_error_msg("TxnLineID", $txnlineid, "No such TxnLineID found!");
			send_error();
		}
	} catch (Exception $e) {
		error_msg($e->getMessage());
		send_error();
	}
}

function getProductCodeHomeDepotDCBOL($connect, $PONumber) {
	$query = "
        SELECT * FROM (
            SELECT 
                 [groupdetail].[TxnLineID]
                ,[groupdetail].[ItemGroupRef_FullName]			
                ,[groupdetail].[Quantity]
                ,W.[FullWeight]
                ,[DL_valid_SKU].[HD_SKU]
                ,[DL_valid_SKU].[SKU]
                ,[shiptrack].[ShipAddress_Addr1]
                ,[shiptrack].[ShipAddress_Addr2]
                ,[shiptrack].[ShipAddress_Addr3]
                ,[shiptrack].[ShipAddress_City]
                ,[shiptrack].[ShipAddress_State]
                ,[shiptrack].[ShipAddress_PostalCode]
            FROM [orders].[dbo].[groupdetail]
            LEFT JOIN [orders].[dbo].[shiptrack] ON [groupdetail].[IDKEY] = [shiptrack].[TxnID]
            LEFT JOIN [orders].[dbo].[DL_valid_SKU] ON [groupdetail].[ItemGroupRef_FullName] = [DL_valid_SKU].[QB_SKU]
            LEFT JOIN (
                SELECT
                     SUM([Weight]) as [FullWeight]
                    ,[GroupIDKEY]
                FROM (
                    SELECT
                         case when [quantity] is NULL then
                            cast(replace(Replace(replace(replace(ISNULL([WeightA], [WeightB]),'lbs', ''), 'lb', ''), ' lbs', ''),' lb','') as int)
                         else
                            [quantity] * cast(replace(Replace(replace(replace(ISNULL([WeightA], [WeightB]),'lbs', ''), 'lb', ''), ' lbs', ''),' lb','') as int)
                         end as [Weight]
                        ,[GroupIDKEY]
                    FROM (
                        SELECT
                             [linedetail].[GroupIDKEY]
                            ,[linedetail].[quantity]
                            ,[linedetail].[CustomField9] as WeightA
                            ,[iteminventory].[CustomField9] as WeightB
                        FROM [orders].[dbo].[linedetail]
                        JOIN [orders31].[dbo].[iteminventory] ON [iteminventory].[FullName] = [linedetail].[ItemRef_FullName]
                        WHERE (
                                linedetail.ItemRef_FullName not like '%Price-Adjustment%' 
                            and linedetail.ItemRef_FullName not like '%Freight%'
                            and linedetail.ItemRef_FullName not like '%warranty%' 
                            and linedetail.ItemRef_FullName is not NULL
                            and linedetail.ItemRef_FullName not like '%Subtotal%'
                            and linedetail.ItemRef_FullName not like '%IDSC-10%'
                            and linedetail.ItemRef_FullName not like '%DISCOUNT%'
                            and linedetail.ItemRef_FullName not like '%SPECIAL ORDER4%'
                            and linedetail.ItemRef_FullName not like '%Manual%'
                            )
                    ) L1
                ) L2
                GROUP BY [GroupIDKEY] 
            ) W ON W.[GroupIDKEY] = [groupdetail].[TxnLineID]
            WHERE [shiptrack].[PONumber] = '" . $PONumber . "' and [HD_SKU] is not null
            
            UNION ALL 
            SELECT 
                 [manually_groupdetail].[TxnLineID]
                ,[manually_groupdetail].[ItemGroupRef_FullName]			
                ,[manually_groupdetail].[Quantity]
                ,W.[FullWeight]
                ,[DL_valid_SKU].[HD_SKU]
                ,[DL_valid_SKU].[SKU]
                ,[manually_shiptrack].[ShipAddress_Addr1]
                ,[manually_shiptrack].[ShipAddress_Addr2]
                ,[manually_shiptrack].[ShipAddress_Addr3]
                ,[manually_shiptrack].[ShipAddress_City]
                ,[manually_shiptrack].[ShipAddress_State]
                ,[manually_shiptrack].[ShipAddress_PostalCode]
            FROM [orders].[dbo].[manually_groupdetail]
            LEFT JOIN [orders].[dbo].[manually_shiptrack] ON [manually_groupdetail].[IDKEY] = [manually_shiptrack].[TxnID]
            LEFT JOIN [orders].[dbo].[DL_valid_SKU] ON [manually_groupdetail].[ItemGroupRef_FullName] = [DL_valid_SKU].[QB_SKU]
            LEFT JOIN (
                SELECT
                     SUM([Weight]) as [FullWeight]
                    ,[GroupIDKEY]
                FROM (
                    SELECT
                         case when [quantity] is NULL then
                            cast(replace(Replace(replace(replace(ISNULL([WeightA], [WeightB]),'lbs', ''), 'lb', ''), ' lbs', ''),' lb','') as int)
                         else
                            [quantity] * cast(replace(Replace(replace(replace(ISNULL([WeightA], [WeightB]),'lbs', ''), 'lb', ''), ' lbs', ''),' lb','') as int)
                         end as [Weight]
                        ,[GroupIDKEY]
                    FROM (
                        SELECT
                             [manually_linedetail].[GroupIDKEY]
                            ,[manually_linedetail].[quantity]
                            ,[manually_linedetail].[CustomField9] as WeightA
                            ,[iteminventory].[CustomField9] as WeightB
                        FROM [orders].[dbo].[manually_linedetail]
                        JOIN [orders31].[dbo].[iteminventory] ON [iteminventory].[FullName] = [manually_linedetail].[ItemRef_FullName]
                        WHERE (
                                [manually_linedetail].ItemRef_FullName not like '%Price-Adjustment%' 
                            and [manually_linedetail].ItemRef_FullName not like '%Freight%'
                            and [manually_linedetail].ItemRef_FullName not like '%warranty%' 
                            and [manually_linedetail].ItemRef_FullName is not NULL
                            and [manually_linedetail].ItemRef_FullName not like '%Subtotal%'
                            and [manually_linedetail].ItemRef_FullName not like '%IDSC-10%'
                            and [manually_linedetail].ItemRef_FullName not like '%DISCOUNT%'
                            and [manually_linedetail].ItemRef_FullName not like '%SPECIAL ORDER4%'
                            and [manually_linedetail].ItemRef_FullName not like '%Manual%'
                            )
                    ) L1
                ) L2
                GROUP BY [GroupIDKEY] 
            ) W ON W.[GroupIDKEY] = [manually_groupdetail].[TxnLineID]
            WHERE [manually_shiptrack].[PONumber] = '" . $PONumber . "' AND [HD_SKU] is not null
        ) a
        GROUP BY [TxnLineID], [ItemGroupRef_FullName], [Quantity], [FullWeight], [HD_SKU], [SKU], [ShipAddress_Addr1], [ShipAddress_Addr2], [ShipAddress_Addr3], [ShipAddress_City], [ShipAddress_State], [ShipAddress_PostalCode]
        ";

	try {
		$result = execQuery($connect, $query);
		$res = array();
		while ($row = $result->fetch()) {

			$addr = '';
			if (isset($row['ShipAddress_Addr1']) && $row['ShipAddress_Addr1'] != '') {
				$addr .= $row['ShipAddress_Addr1'] . '<br />';
			}
			if (isset($row['ShipAddress_Addr2']) && $row['ShipAddress_Addr2'] != '') {
				$addr .= $row['ShipAddress_Addr2'] . '<br />';
			}
			if (isset($row['ShipAddress_Addr3']) && $row['ShipAddress_Addr3'] != '') {
				$addr .= $row['ShipAddress_Addr3'] . '<br />';
			}
			if (isset($row['ShipAddress_City']) && $row['ShipAddress_City'] != '') {
				$addr .= $row['ShipAddress_City'];
			}
			if (isset($row['ShipAddress_State']) && $row['ShipAddress_State'] != '') {
				$addr .= ($addr == '' ? '' : ', ') . $row['ShipAddress_State'];
			}
			if (isset($row['ShipAddress_PostalCode']) && $row['ShipAddress_PostalCode'] != '') {
				$addr .= ($addr == '' ? '' : ' ') . $row['ShipAddress_PostalCode'];
			}

			$arr = array(
				'PONumber' => $PONumber,
				'TxnLineID' => $row['TxnLineID'],
				'ItemGroupRef_FullName' => $row['ItemGroupRef_FullName'],
				'Quantity' => $row['Quantity'],
				'FullWeight' => $row['FullWeight'],
				'HD_SKU' => $row['HD_SKU'],
				'SKU' => $row['SKU'],
				'Address' => $addr
			);
			$res[] = $arr;
		}
		if (isset($res) && !empty($res)) {
			return $res;
		} else {
			error_msg("Can not get data for this order! This order may have already been processed. Try again in a few minutes.");
			send_error();
		}
	} catch (Exception $e) {
		error_msg($e->getMessage());
		send_error();
	}
}

function getInvoiceData1($connect, $po) { // Header data
	$query = "SELECT 
                         [invoice].[PONumber]
                        ,[invoice].[RefNumber]
                        ,[invoice].[CustomerRef_FullName] as Customer
                        ,[invoice].[TimeCreated]
                        ,[invoice].[ShipAddress_Addr1] as Addr1
                        ,[invoice].[ShipAddress_Addr2] as Addr2
                        ,[invoice].[ShipAddress_Addr3] as Addr3
                        ,[invoice].[ShipAddress_City] as City
                        ,[invoice].[ShipAddress_State] as State
                        ,[invoice].[ShipAddress_PostalCode] as Zip
                        ,[invoice].[ShipAddress_Country] as Country
                        ,[invoice].[FOB] as Phone
                        ,[invoice].[ShipMethodRef_FullName] as Ship_method
                        ,[invoice].[TermsRef_FullName] as Terms
                        ,[invoice].[SalesRepRef_FullName] as Rep
                        ,[invoice].[ShipDate] as ShipDate
                        ,[invoice].[TxnDate] as InvoiceDate
                        ,[invoice].[ItemSalesTaxRef_FullName] as [SalesTaxName]
                        ,[invoice].[SalesTaxPercentage] as [SalesTaxPercent]
                        ,[invoice].[SalesTaxTotal] as [SalesTaxAmount]
                        ,[invoice].[Subtotal] as [Subtotal]
                        ,[invoice].[AppliedAmount] as [AppliedAmount]
                        ,[invoice].[BalanceRemaining] as [BalanceRemaining]
                        ,[salesorder].[ShipMethodRef_FullName] as [Actual Ship_method]
                        ,[salesorder].[TimeModified] as [Actual Modified Date]
                        ,[salesorder].[ShipDate] as [Actual ShipDate]
                        ,CONVERT(varchar(255), [salesorder].[Subtotal]) as [Actual Subtotal]
                        ,[invoice].[Memo]
                        ,[invoice].[other] as [Tracking #]
                FROM [" . DATABASE31 . "].[dbo].[invoice]
           LEFT JOIN [" . DATABASE31 . "].[dbo].[salesorder] ON invoice.[PONumber] = [salesorder].[PONumber]
               WHERE [invoice].[PONumber] = '" . $po . "'";

	try {
		$result = execQuery($connect, $query);
		$res = array();
		while ($row = $result->fetch()) {
			$arr = array("PONumber" => $row['PONumber']
				, "RefNumber" => $row['RefNumber']
				, "Customer" => $row["Customer"]
				, "TimeCreated" => $row["TimeCreated"]
				, "Addr1" => $row["Addr1"]
				, "Addr2" => $row["Addr2"]
				, "Addr3" => $row["Addr3"]
				, "City" => $row["City"]
				, "State" => $row["State"]
				, "Zip" => $row["Zip"]
				, "Country" => $row["Country"]
				, "Phone" => $row["Phone"]
				, "Ship_Method" => $row["Ship_method"]
				, "Terms" => $row["Terms"]
				, "Rep" => $row["Rep"]
				, "Ship_Date" => $row["ShipDate"]
				, "Invoice_Date" => $row["InvoiceDate"]
				, "SalesTaxName" => $row["SalesTaxName"]
				, "SalesTaxPercent" => $row["SalesTaxPercent"]
				, "SalesTaxAmount" => $row["SalesTaxAmount"]
				, "Subtotal" => $row["Subtotal"]
				, "AppliedAmount" => $row["AppliedAmount"]
				, "BalanceRemaining" => $row["BalanceRemaining"]
				, "Actual_Ship_Method" => $row["Actual Ship_method"]
				, "Actual_Modified_Date" => $row["Actual Modified Date"]
				, "Actual_Ship_Date" => $row["Actual ShipDate"]
				, "Actual_Subtotal" => $row["Actual Subtotal"]
				, "Memo" => $row["Memo"]
				, "Tracking" => $row["Tracking #"]
			);
			$res[] = $arr;
		}
		if (isset($res) && !empty($res)) {
			/* print_r($res);
			  die(); */
			return $res;
		} else {
			error_msg("Can not get data for this order! This order may have already been processed. Try again in a few minutes.");
			data_error_msg("PO", $PONumber, "No such PO found!");
			send_error();
		}
	} catch (Exception $e) {
		error_msg($e->getMessage());
		send_error();
	}
}

function getInvoiceData2($connect, $po) { // Table data
	$query = "SELECT [invoicelinedetail].[quantity] as [Qty]
                        ,[invoicelinedetail].[ItemRef_FullName] as [Item]
                        ,[invoicelinedetail].[Desc] as [Description]
                        ,[invoicelinedetail].[Rate] as [UnitPrice]
                        ,[invoicelinedetail].[Amount] as [Amount]	  			  
                FROM [" . DATABASE31 . "].[dbo].[invoicelinedetail]		
                LEFT JOIN [" . DATABASE31 . "].[dbo].[invoice] ON [invoicelinedetail].[IDKEY] = [invoice].[TxnID]
                WHERE [invoice].[PONumber] = '" . $po . "'";

	try {
		$result = execQuery($connect, $query);
		$res = array();
		while ($row = $result->fetch()) {
			$arr = array("Qty" => $row['Qty']
				, "Comments" => " "
				, "Item" => $row['Item']
				, "Description" => $row["Description"]
				, "UnitPrice" => $row["UnitPrice"]
				, "Amount" => $row["Amount"]
			);
			$res[] = $arr;
		}
		if (isset($res) && !empty($res)) {
			/* print_r($res);
			  die(); */
			return $res;
		} else {
			error_msg("Can not get data for this order! This order may have already been processed. Try again in a few minutes.");
			data_error_msg("PO", $PONumber, "No such PO found!");
			send_error();
		}
	} catch (Exception $e) {
		error_msg($e->getMessage());
		send_error();
	}
}

//Get all data from DB
function getProductCodeLabels($connect, $PONumber, $id) {

	$query = "SELECT 
	shiptrack.RefNumber,   
	shiptrack.PONumber,	   
	shiptrack.CustomerRef_FullName, 
	shiptrack.ShipMethodRef_FullName,
	shiptrack.ShipAddress_Addr1, 
	shiptrack.ShipAddress_Addr2, 
	shiptrack.ShipAddress_Addr3, 
	shiptrack.ShipAddress_Addr4, 
	shiptrack.ShipAddress_Addr5, 
	shiptrack.ShipAddress_City, 
	shiptrack.ShipAddress_State, 
	shiptrack.ShipAddress_Country,
	shiptrack.ShipAddress_PostalCode, 
	shiptrack.FOB,
	shiptrack.TimeCreated, 
	shiptrack.Memo, 
	groupdetail.ItemGroupRef_FullName, 
	groupdetail.TxnLineID,
	groupdetail.Prod_desc, 
	linedetail.quantity,
        groupdetail.[quantity] as [group_quantity],
	linedetail.[quantity] as [item_quantity],
	linedetail.ItemRef_FullName,
	linedetail.[GroupIDKEY],
		DL_valid_SKU.SKU as DL_SKU,
	case when linedetail.Quantity is NULL then 1*cast(dbo.udf_GetNumeric(linedetail.CustomField9) as int) 
 	else  linedetail.Quantity*cast(dbo.udf_GetNumeric(linedetail.CustomField9) as int) end as itemWeight,
	ShipTrackWithLabel.CARRIERNAME,
	ShipTrackWithLabel.WEIGHT,
	ShipTrackWithLabel.PALLETS,
	ShipTrackWithLabel.SERIAL_REFERENCE,
	ShipTrackWithLabel.RESIDENTIAL,
	ShipTrackWithLabel.LIFTGATE,
	ShipTrackWithLabel.delivery_notification,
	ShipTrackWithLabel.ASN,
	NULL as [original_po]
	FROM shiptrack
		INNER JOIN [dbo].[groupdetail] ON [shiptrack].TxnID = [groupdetail].IDKEY
		INNER JOIN [dbo].[linedetail] ON [groupdetail].[TxnLineID] = [linedetail].[GroupIDKEY]
		INNER JOIN ShipTrackWithLabel ON shiptrack.RefNumber = ShipTrackWithLabel.ID 
		LEFT JOIN [dbo].[DL_valid_SKU] ON groupdetail.ItemGroupRef_FullName = DL_valid_SKU.QB_SKU AND groupdetail.ItemGroupRef_FullName LIKE '%'+DL_valid_SKU.SKU+'%'
	WHERE 
		linedetail.ItemRef_FullName not like '%Price-Adjustment%'   and 
		linedetail.ItemRef_FullName not like '%Freight%'            and
		linedetail.ItemRef_FullName not like '%warranty%'           and        
		linedetail.ItemRef_FullName is not NULL                     and
		linedetail.ItemRef_FullName not like '%Subtotal%'           and
		linedetail.ItemRef_FullName not like '%IDSC-10%'            and
		linedetail.ItemRef_FullName not like '%DISCOUNT%'           and
		linedetail.ItemRef_FullName not like '%SPECIAL ORDER%'    	and 	
		linedetail.CustomField9 is not NULL							and
		shiptrack.PONumber = ".$connect->quote($PONumber)."
		AND shiptrack.RefNumber = ".$connect->quote($id)."
		UNION ALL
		SELECT 
	manually_shiptrack.RefNumber,   
	manually_shiptrack.PONumber,	   
	manually_shiptrack.CustomerRef_FullName, 
	manually_shiptrack.ShipMethodRef_FullName,
	manually_shiptrack.ShipAddress_Addr1, 
	manually_shiptrack.ShipAddress_Addr2, 
	manually_shiptrack.ShipAddress_Addr3, 
	manually_shiptrack.ShipAddress_Addr4, 
	manually_shiptrack.ShipAddress_Addr5, 
	manually_shiptrack.ShipAddress_City, 
	manually_shiptrack.ShipAddress_State, 
	manually_shiptrack.ShipAddress_Country,
	manually_shiptrack.ShipAddress_PostalCode, 
	manually_shiptrack.FOB,
	manually_shiptrack.TimeCreated, 
	manually_shiptrack.Memo, 
	manually_groupdetail.ItemGroupRef_FullName, 
	manually_groupdetail.TxnLineID,
	manually_groupdetail.Prod_desc, 
	manually_linedetail.quantity,
        manually_groupdetail.[quantity] as [group_quantity],
	manually_linedetail.[quantity] as [item_quantity],
	manually_linedetail.ItemRef_FullName,
	manually_linedetail.[GroupIDKEY],
		DL_valid_SKU.SKU as DL_SKU,
	case when manually_linedetail.Quantity is NULL then 1*cast(dbo.udf_GetNumeric(manually_linedetail.CustomField9) as int) 
 	else  manually_linedetail.Quantity*cast(dbo.udf_GetNumeric(manually_linedetail.CustomField9) as int) end as itemWeight,
	ShipTrackWithLabel.CARRIERNAME,
	ShipTrackWithLabel.WEIGHT,
	ShipTrackWithLabel.PALLETS,
	ShipTrackWithLabel.SERIAL_REFERENCE,
	ShipTrackWithLabel.RESIDENTIAL,
	ShipTrackWithLabel.LIFTGATE,
	ShipTrackWithLabel.delivery_notification,
	ShipTrackWithLabel.ASN,
	manually_linedetail.[original_po]
	FROM manually_shiptrack
		INNER JOIN [dbo].[manually_groupdetail] ON [manually_shiptrack].TxnID = [manually_groupdetail].IDKEY
		INNER JOIN [dbo].[manually_linedetail] ON [manually_groupdetail].[TxnLineID] = [manually_linedetail].[GroupIDKEY]
		INNER JOIN ShipTrackWithLabel ON manually_shiptrack.RefNumber = ShipTrackWithLabel.ID 
			LEFT JOIN [dbo].[DL_valid_SKU] ON manually_groupdetail.ItemGroupRef_FullName = DL_valid_SKU.QB_SKU AND manually_groupdetail.ItemGroupRef_FullName LIKE '%'+DL_valid_SKU.SKU+'%'
	WHERE 
		manually_linedetail.ItemRef_FullName not like '%Price-Adjustment%'   and 
		manually_linedetail.ItemRef_FullName not like '%Freight%'            and
		manually_linedetail.ItemRef_FullName not like '%warranty%'           and        
		manually_linedetail.ItemRef_FullName is not NULL                     and
		manually_linedetail.ItemRef_FullName not like '%Subtotal%'           and
		manually_linedetail.ItemRef_FullName not like '%IDSC-10%'            and
		manually_linedetail.ItemRef_FullName not like '%DISCOUNT%'           and
		manually_linedetail.ItemRef_FullName not like '%SPECIAL ORDER%'    	and 	
		manually_linedetail.CustomField9 is not NULL							and 
		manually_shiptrack.PONumber = ".$connect->quote($PONumber)."
		AND manually_shiptrack.RefNumber = ".$connect->quote($id);
        
        /*print_r($query);
        die();*/
	
	try {
		$result = execQuery($connect, $query);
		$res = array();
		while ($row = $result->fetch()) {
			if ((!isset($row['ShipAddress_Addr1']) || empty($row['ShipAddress_Addr1'])) &&
					(!isset($row['ShipAddress_Addr2']) || empty($row['ShipAddress_Addr2'])) &&
					(!isset($row['ShipAddress_Addr3']) || empty($row['ShipAddress_Addr3'])) &&
					(!isset($row['ShipAddress_Addr4']) || empty($row['ShipAddress_Addr4'])) &&
					(!isset($row['ShipAddress_Addr5']) || empty($row['ShipAddress_Addr5']))) {
				error_msg("All address fields are empty!");
				data_error_msg("PO", $PONumber, "No SHIP TO address for this PO found!");
				send_error();
			} else {
				$phoneNumber = preg_replace("/[^0-9]/", "", $row['FOB']);
				$resultPhoneNumber = substr($phoneNumber, 0, 3) . '-' . substr($phoneNumber, 3, 3) . '-' . substr($phoneNumber, 6, 4);

				$arr = array(
					'RefNumber' => $row['RefNumber'],
					'PONumber' => strtoupper($row['PONumber']),
					'ProductCode' => $row['ItemGroupRef_FullName'],
					'TxnLineID' => $row['TxnLineID'],
					'ItemGroupRef_FullName' => $row['ItemGroupRef_FullName'],
					'Prod_desc' => $row['Prod_desc'],
					'item_quantity' => $row['item_quantity'],
					'Descr' => $row['Prod_desc'],
					'CustomerRef_FullName' => $row['CustomerRef_FullName'],
					'GroupIDKEY' => $row['GroupIDKEY'],
					'DL_SKU' => $row['DL_SKU'],
					'ShipMethodRef_FullName' => $row['ShipMethodRef_FullName'],
					'ItemRef_FullName' => $row['ItemRef_FullName'],
					'quantity' => $row['quantity'],
                                        'group_quantity' => $row['group_quantity'],
					'ShipAddress_Addr1' => $row['ShipAddress_Addr1'],
					'ShipAddress_Addr2' => $row['ShipAddress_Addr2'],
					'ShipAddress_Addr3' => $row['ShipAddress_Addr3'],
					'ShipAddress_Addr4' => $row['ShipAddress_Addr4'],
					'ShipAddress_Addr5' => $row['ShipAddress_Addr5'],
					'ShipAddress_City' => $row['ShipAddress_City'],
					'ShipAddress_State' => $row['ShipAddress_State'],
					'ShipAddress_Country' => $row['ShipAddress_Country'],
					'ShipAddress_PostalCode' => $row['ShipAddress_PostalCode'],
					'FOB' => $resultPhoneNumber,
					'TimeCreated' => $row['TimeCreated'],
					'Memo' => $row['Memo'],
					'CARRIERNAME' => $row['CARRIERNAME'],
					'WEIGHT' => $row['WEIGHT'],
					'PALLETS' => $row['PALLETS'],
					'SERIAL_REFERENCE' => $row['SERIAL_REFERENCE'],
					'RESIDENTIAL' => $row['RESIDENTIAL'],
					'LIFTGATE' => $row['LIFTGATE'],
					'delivery_notification' => $row['delivery_notification'],
					'itemWeight' => $row['itemWeight'],
					'original_po' => $row['original_po'],
					'ASN' => $row['ASN']
				);
				$res[] = $arr;
			}
		}
		if (isset($res) && !empty($res)) {
			/* print_r($res);
			  die(); */
			return $res;
		} else {
			error_msg("Can not get data for this order!");
			data_error_msg("PO", $PONumber, "No such PO found!");
			send_error();
		}
	} catch (Exception $e) {
		error_msg($e->getMessage());
		send_error();
	}
}

//Get all data from DB
function getProductCode($connect, $PONumber, $id) {

	$query = "SELECT 
	" . SHIPTRACK_TABLE . ".RefNumber,   
	" . SHIPTRACK_TABLE . ".PONumber,	   
	" . SHIPTRACK_TABLE . ".CustomerRef_FullName, 
	" . SHIPTRACK_TABLE . ".ShipMethodRef_FullName,
	" . SHIPTRACK_TABLE . ".ShipAddress_Addr1, 
	" . SHIPTRACK_TABLE . ".ShipAddress_Addr2, 
	" . SHIPTRACK_TABLE . ".ShipAddress_Addr3, 
	" . SHIPTRACK_TABLE . ".ShipAddress_Addr4, 
	" . SHIPTRACK_TABLE . ".ShipAddress_Addr5, 
	" . SHIPTRACK_TABLE . ".ShipAddress_City, 
	" . SHIPTRACK_TABLE . ".ShipAddress_State, 
	" . SHIPTRACK_TABLE . ".ShipAddress_Country,
	" . SHIPTRACK_TABLE . ".ShipAddress_PostalCode, 
	" . SHIPTRACK_TABLE . ".FOB,
	" . SHIPTRACK_TABLE . ".TimeCreated, 
	" . SHIPTRACK_TABLE . ".Memo, 
	" . GROUPDETAIL_TABLE . ".ItemGroupRef_FullName, 
	" . GROUPDETAIL_TABLE . ".TxnLineID,
	" . GROUPDETAIL_TABLE . ".Prod_desc, 
	" . LINEDETAIL_TABLE . ".quantity,
        " . GROUPDETAIL_TABLE . ".[quantity] as [group_quantity],
	" . LINEDETAIL_TABLE . ".[quantity] as [item_quantity],
	" . LINEDETAIL_TABLE . ".ItemRef_FullName,
	" . LINEDETAIL_TABLE . ".[GroupIDKEY],
		DL_valid_SKU.SKU as DL_SKU,
	case when " . LINEDETAIL_TABLE . ".Quantity is NULL then 1*cast(replace(Replace(replace(replace(" . LINEDETAIL_TABLE . ".CustomField9,'lbs', ''), 'lb', ''), ' lbs', ''),' lb','') as int) 
 	else  " . LINEDETAIL_TABLE . ".Quantity*cast(replace(Replace(replace(replace(" . LINEDETAIL_TABLE . ".CustomField9,'lbs', ''), 'lb', ''), ' lbs', ''),' lb','') as int) end as itemWeight,
	" . SHIPTRACKWITHLABEL_TABLE . ".CARRIERNAME,
	" . SHIPTRACKWITHLABEL_TABLE . ".WEIGHT,
	" . SHIPTRACKWITHLABEL_TABLE . ".PALLETS,
	" . SHIPTRACKWITHLABEL_TABLE . ".SERIAL_REFERENCE,
	" . SHIPTRACKWITHLABEL_TABLE . ".RESIDENTIAL,
	" . SHIPTRACKWITHLABEL_TABLE . ".LIFTGATE,
	" . SHIPTRACKWITHLABEL_TABLE . ".delivery_notification,
	" . SHIPTRACKWITHLABEL_TABLE . ".ASN,
	NULL as [original_po],
	case
						  when  shiptrack.ponumber in (select case when altdocid like '' then accountid else altdocid end  as [PO] from [SQLSERVER2].[integrator].dbo.ttcTxnTransactions where partnerid='14' and TxnType='Special Order') then 'commercial'
						 when  shiptrack.ponumber in (select case when altdocid like '' then accountid else altdocid end  as [PO] from [SQLSERVER2].[integrator].dbo.ttcTxnTransactions where partnerid='14' and TxnType='Direct Ship') then 'residential'
 						  else 'no'
 						end as lowes_mode
	FROM [dbo].[shiptrack]
		INNER JOIN [dbo].[groupdetail] ON [shiptrack].TxnID = [groupdetail].IDKEY
		INNER JOIN [dbo].[linedetail] ON [groupdetail].[TxnLineID] = [linedetail].[GroupIDKEY]
		INNER JOIN " . SHIPTRACKWITHLABEL_TABLE . " ON " . SHIPTRACK_TABLE . ".RefNumber = " . SHIPTRACKWITHLABEL_TABLE . ".ID 
		LEFT JOIN [dbo].[DL_valid_SKU] ON " . GROUPDETAIL_TABLE . ".ItemGroupRef_FullName = DL_valid_SKU.QB_SKU AND " . GROUPDETAIL_TABLE . ".ItemGroupRef_FullName LIKE '%'+DL_valid_SKU.SKU+'%'
	WHERE 
		" . SHIPTRACK_TABLE . ".ShipMethodRef_FullName not like '%cancel%'  and 
		" . SHIPTRACK_TABLE . ".ShipMethodRef_FullName not like '%void%'and 
		" . SHIPTRACK_TABLE . ".ShipMethodRef_FullName not like '%cancel%'  and 
		" . SHIPTRACK_TABLE . ".ShipMethodRef_FullName not like '%void%'  and 
		" . LINEDETAIL_TABLE . ".ItemRef_FullName not like '%Price-Adjustment%'   and 
		" . LINEDETAIL_TABLE . ".ItemRef_FullName not like '%Freight%'            and
		" . LINEDETAIL_TABLE . ".ItemRef_FullName not like '%warranty%'           and        
		" . LINEDETAIL_TABLE . ".ItemRef_FullName is not NULL                     and
		" . LINEDETAIL_TABLE . ".ItemRef_FullName not like '%Subtotal%'           and
		" . LINEDETAIL_TABLE . ".ItemRef_FullName not like '%IDSC-10%'            and
		" . LINEDETAIL_TABLE . ".ItemRef_FullName not like '%DISCOUNT%'           and
		" . LINEDETAIL_TABLE . ".ItemRef_FullName not like '%SPECIAL ORDER%'    	and 	
		" . LINEDETAIL_TABLE . ".CustomField9 is not NULL							and 
		" . LINEDETAIL_TABLE . ".GroupIDKEY = " . GROUPDETAIL_TABLE . ".TxnLineID  				and
		" . SHIPTRACK_TABLE . ".PONumber = '" . $PONumber . "'
		AND " . SHIPTRACK_TABLE . ".RefNumber = '" . $id . "'
		UNION ALL
		SELECT 
	" . MANUALLY_SHIPTRACK_TABLE . ".RefNumber,   
	" . MANUALLY_SHIPTRACK_TABLE . ".PONumber,	   
	" . MANUALLY_SHIPTRACK_TABLE . ".CustomerRef_FullName, 
	" . MANUALLY_SHIPTRACK_TABLE . ".ShipMethodRef_FullName,
	" . MANUALLY_SHIPTRACK_TABLE . ".ShipAddress_Addr1, 
	" . MANUALLY_SHIPTRACK_TABLE . ".ShipAddress_Addr2, 
	" . MANUALLY_SHIPTRACK_TABLE . ".ShipAddress_Addr3, 
	" . MANUALLY_SHIPTRACK_TABLE . ".ShipAddress_Addr4, 
	" . MANUALLY_SHIPTRACK_TABLE . ".ShipAddress_Addr5, 
	" . MANUALLY_SHIPTRACK_TABLE . ".ShipAddress_City, 
	" . MANUALLY_SHIPTRACK_TABLE . ".ShipAddress_State, 
	" . MANUALLY_SHIPTRACK_TABLE . ".ShipAddress_Country,
	" . MANUALLY_SHIPTRACK_TABLE . ".ShipAddress_PostalCode, 
	" . MANUALLY_SHIPTRACK_TABLE . ".FOB,
	" . MANUALLY_SHIPTRACK_TABLE . ".TimeCreated, 
	" . MANUALLY_SHIPTRACK_TABLE . ".Memo, 
	" . MANUALLY_GROUPDETAIL_TABLE . ".ItemGroupRef_FullName, 
	" . MANUALLY_GROUPDETAIL_TABLE . ".TxnLineID,
	" . MANUALLY_GROUPDETAIL_TABLE . ".Prod_desc, 
	" . MANUALLY_LINEDETAIL_TABLE . ".quantity,
        " . MANUALLY_GROUPDETAIL_TABLE . ".[quantity] as [group_quantity],
	" . MANUALLY_LINEDETAIL_TABLE . ".[quantity] as [item_quantity],
	" . MANUALLY_LINEDETAIL_TABLE . ".ItemRef_FullName,
	" . MANUALLY_LINEDETAIL_TABLE . ".[GroupIDKEY],
		DL_valid_SKU.SKU as DL_SKU,
	case when " . MANUALLY_LINEDETAIL_TABLE . ".Quantity is NULL then 1*cast(replace(Replace(replace(replace(" . MANUALLY_LINEDETAIL_TABLE . ".CustomField9,'lbs', ''), 'lb', ''), ' lbs', ''),' lb','') as int) 
 	else  " . MANUALLY_LINEDETAIL_TABLE . ".Quantity*cast(replace(Replace(replace(replace(" . MANUALLY_LINEDETAIL_TABLE . ".CustomField9,'lbs', ''), 'lb', ''), ' lbs', ''),' lb','') as int) end as itemWeight,
	" . SHIPTRACKWITHLABEL_TABLE . ".CARRIERNAME,
	" . SHIPTRACKWITHLABEL_TABLE . ".WEIGHT,
	" . SHIPTRACKWITHLABEL_TABLE . ".PALLETS,
	" . SHIPTRACKWITHLABEL_TABLE . ".SERIAL_REFERENCE,
	" . SHIPTRACKWITHLABEL_TABLE . ".RESIDENTIAL,
	" . SHIPTRACKWITHLABEL_TABLE . ".LIFTGATE,
	" . SHIPTRACKWITHLABEL_TABLE . ".delivery_notification,
	" . SHIPTRACKWITHLABEL_TABLE . ".ASN,
	" . MANUALLY_LINEDETAIL_TABLE . ".[original_po],
	case
						  when  manually_shiptrack.ponumber in (select case when altdocid like '' then accountid else altdocid end  as [PO] from [SQLSERVER2].[integrator].dbo.ttcTxnTransactions where partnerid='14' and TxnType='Special Order') then 'commercial'
						 when  manually_shiptrack.ponumber in (select case when altdocid like '' then accountid else altdocid end  as [PO] from [SQLSERVER2].[integrator].dbo.ttcTxnTransactions where partnerid='14' and TxnType='Direct Ship') then 'residential'
 						  else 'no'
 						end as lowes_mode
	FROM [dbo].[manually_shiptrack]
		INNER JOIN [dbo].[manually_groupdetail] ON [manually_shiptrack].TxnID = [manually_groupdetail].IDKEY
		INNER JOIN [dbo].[manually_linedetail] ON [manually_groupdetail].[TxnLineID] = [manually_linedetail].[GroupIDKEY]
		INNER JOIN " . SHIPTRACKWITHLABEL_TABLE . " ON " . MANUALLY_SHIPTRACK_TABLE . ".RefNumber = " . SHIPTRACKWITHLABEL_TABLE . ".ID 
			LEFT JOIN [dbo].[DL_valid_SKU] ON " . MANUALLY_GROUPDETAIL_TABLE . ".ItemGroupRef_FullName = DL_valid_SKU.QB_SKU AND " . MANUALLY_GROUPDETAIL_TABLE . ".ItemGroupRef_FullName LIKE '%'+DL_valid_SKU.SKU+'%'
	WHERE 
		" . MANUALLY_SHIPTRACK_TABLE . ".ShipMethodRef_FullName not like '%cancel%'  and 
		" . MANUALLY_SHIPTRACK_TABLE . ".ShipMethodRef_FullName not like '%void%'and 
		" . MANUALLY_SHIPTRACK_TABLE . ".ShipMethodRef_FullName not like '%cancel%'  and 
		" . MANUALLY_SHIPTRACK_TABLE . ".ShipMethodRef_FullName not like '%void%'  and 
		" . MANUALLY_LINEDETAIL_TABLE . ".ItemRef_FullName not like '%Price-Adjustment%'   and 
		" . MANUALLY_LINEDETAIL_TABLE . ".ItemRef_FullName not like '%Freight%'            and
		" . MANUALLY_LINEDETAIL_TABLE . ".ItemRef_FullName not like '%warranty%'           and        
		" . MANUALLY_LINEDETAIL_TABLE . ".ItemRef_FullName is not NULL                     and
		" . MANUALLY_LINEDETAIL_TABLE . ".ItemRef_FullName not like '%Subtotal%'           and
		" . MANUALLY_LINEDETAIL_TABLE . ".ItemRef_FullName not like '%IDSC-10%'            and
		" . MANUALLY_LINEDETAIL_TABLE . ".ItemRef_FullName not like '%DISCOUNT%'           and
		" . MANUALLY_LINEDETAIL_TABLE . ".ItemRef_FullName not like '%SPECIAL ORDER%'    	and 	
		" . MANUALLY_LINEDETAIL_TABLE . ".CustomField9 is not NULL							and 
		" . MANUALLY_LINEDETAIL_TABLE . ".GroupIDKEY = " . MANUALLY_GROUPDETAIL_TABLE . ".TxnLineID  				and
		" . MANUALLY_SHIPTRACK_TABLE . ".PONumber = '" . $PONumber . "'
		AND " . MANUALLY_SHIPTRACK_TABLE . ".RefNumber = '" . $id . "'";

	
	try {
		$result = execQuery($connect, $query);
		$res = array();
		while ($row = $result->fetch()) {
			if ((!isset($row['ShipAddress_Addr1']) || empty($row['ShipAddress_Addr1'])) &&
					(!isset($row['ShipAddress_Addr2']) || empty($row['ShipAddress_Addr2'])) &&
					(!isset($row['ShipAddress_Addr3']) || empty($row['ShipAddress_Addr3'])) &&
					(!isset($row['ShipAddress_Addr4']) || empty($row['ShipAddress_Addr4'])) &&
					(!isset($row['ShipAddress_Addr5']) || empty($row['ShipAddress_Addr5']))) {
				error_msg("All address fields are empty!");
				data_error_msg("PO", $PONumber, "No SHIP TO address for this PO found!");
				send_error();
			} else {
				$phoneNumber = preg_replace("/[^0-9]/", "", $row['FOB']);
				$resultPhoneNumber = substr($phoneNumber, 0, 3) . '-' . substr($phoneNumber, 3, 3) . '-' . substr($phoneNumber, 6, 4);

				$arr = array(
					'RefNumber' => $row['RefNumber'],
					'PONumber' => strtoupper($row['PONumber']),
					'ProductCode' => $row['ItemGroupRef_FullName'],
					'TxnLineID' => $row['TxnLineID'],
					'ItemGroupRef_FullName' => $row['ItemGroupRef_FullName'],
					'Prod_desc' => $row['Prod_desc'],
					'item_quantity' => $row['item_quantity'],
					'Descr' => $row['Prod_desc'],
					'CustomerRef_FullName' => $row['CustomerRef_FullName'],
					'GroupIDKEY' => $row['GroupIDKEY'],
					'DL_SKU' => $row['DL_SKU'],
					'ShipMethodRef_FullName' => $row['ShipMethodRef_FullName'],
					'ItemRef_FullName' => $row['ItemRef_FullName'],
					'quantity' => $row['quantity'],
                                        'group_quantity' => $row['group_quantity'],
					'ShipAddress_Addr1' => $row['ShipAddress_Addr1'],
					'ShipAddress_Addr2' => $row['ShipAddress_Addr2'],
					'ShipAddress_Addr3' => $row['ShipAddress_Addr3'],
					'ShipAddress_Addr4' => $row['ShipAddress_Addr4'],
					'ShipAddress_Addr5' => $row['ShipAddress_Addr5'],
					'ShipAddress_City' => $row['ShipAddress_City'],
					'ShipAddress_State' => $row['ShipAddress_State'],
					'ShipAddress_Country' => $row['ShipAddress_Country'],
					'ShipAddress_PostalCode' => $row['ShipAddress_PostalCode'],
					'FOB' => $resultPhoneNumber,
					'TimeCreated' => $row['TimeCreated'],
					'Memo' => $row['Memo'],
					'CARRIERNAME' => $row['CARRIERNAME'],
					'WEIGHT' => $row['WEIGHT'],
					'PALLETS' => $row['PALLETS'],
					'SERIAL_REFERENCE' => $row['SERIAL_REFERENCE'],
					'RESIDENTIAL' => $row['RESIDENTIAL'],
					'LIFTGATE' => $row['LIFTGATE'],
					'delivery_notification' => $row['delivery_notification'],
					'itemWeight' => $row['itemWeight'],
					'original_po' => $row['original_po'],
					'lowes_mode' => $row['lowes_mode'],
					'ASN' => $row['ASN']
				);
				$res[] = $arr;
			}
		}
		if (isset($res) && !empty($res)) {
			/* print_r($res);
			  die(); */
			return $res;
		} else {
			error_msg("Can not get data for this order! This order may have already been processed. Try again in a few minutes.");
			data_error_msg("PO", $PONumber, "No such PO found!");
			send_error();
		}
	} catch (Exception $e) {
		error_msg($e->getMessage());
		send_error();
	}
}

//Get all Sears today data from DB
function getSearsTodayOrders($connect, $currentDate) {

	$query = "SELECT 
	" . SHIPTRACK_TABLE . ".RefNumber,   
	" . SHIPTRACK_TABLE . ".PONumber,	   
	" . SHIPTRACK_TABLE . ".CustomerRef_FullName, 
	" . SHIPTRACK_TABLE . ".ShipAddress_Addr1, 
	" . SHIPTRACK_TABLE . ".ShipAddress_Addr2, 
	" . SHIPTRACK_TABLE . ".ShipAddress_Addr3, 
	" . SHIPTRACK_TABLE . ".ShipAddress_Addr4, 
	" . SHIPTRACK_TABLE . ".ShipAddress_Addr5, 
	" . SHIPTRACK_TABLE . ".ShipAddress_City, 
	" . SHIPTRACK_TABLE . ".ShipAddress_State, 
	" . SHIPTRACK_TABLE . ".ShipAddress_PostalCode, 
	" . SHIPTRACK_TABLE . ".FOB,
	" . SHIPTRACK_TABLE . ".TimeCreated, 
	" . SHIPTRACK_TABLE . ".Memo, 
	" . GROUPDETAIL_TABLE . ".ItemGroupRef_FullName, 
	" . GROUPDETAIL_TABLE . ".Prod_desc, 
	" . LINEDETAIL_TABLE . ".quantity,
	" . LINEDETAIL_TABLE . ".ItemRef_FullName,
	case when " . LINEDETAIL_TABLE . ".Quantity is NULL then 1*cast(replace(Replace(replace(replace(" . LINEDETAIL_TABLE . ".CustomField9,'lbs', ''), 'lb', ''), ' lbs', ''),' lb','') as int) 
 	else  " . LINEDETAIL_TABLE . ".Quantity*cast(replace(Replace(replace(replace(" . LINEDETAIL_TABLE . ".CustomField9,'lbs', ''), 'lb', ''), ' lbs', ''),' lb','') as int) end as itemWeight,
	" . SHIPTRACKWITHLABEL_TABLE . ".CARRIERNAME,
	" . SHIPTRACKWITHLABEL_TABLE . ".WEIGHT,
	" . SHIPTRACKWITHLABEL_TABLE . ".SERIAL_REFERENCE
	FROM " . SHIPTRACK_TABLE . "
		INNER JOIN " . GROUPDETAIL_TABLE . " ON " . SHIPTRACK_TABLE . ".TxnID = " . GROUPDETAIL_TABLE . ".IDKEY
		INNER JOIN " . LINEDETAIL_TABLE . " ON " . SHIPTRACK_TABLE . ".TxnID = " . LINEDETAIL_TABLE . ".IDKEY
		INNER JOIN " . SHIPTRACKWITHLABEL_TABLE . " ON " . SHIPTRACK_TABLE . ".RefNumber = " . SHIPTRACKWITHLABEL_TABLE . ".ID 
	WHERE 
		" . SHIPTRACK_TABLE . ".ShipMethodRef_FullName not like '%cancel%'  and 
		" . SHIPTRACK_TABLE . ".ShipMethodRef_FullName not like '%void%'and 
		" . SHIPTRACK_TABLE . ".ShipMethodRef_FullName not like '%cancel%'  and 
		" . SHIPTRACK_TABLE . ".ShipMethodRef_FullName not like '%void%'  and 
		" . LINEDETAIL_TABLE . ".ItemRef_FullName not like '%Price-Adjustment%'   and 
		" . LINEDETAIL_TABLE . ".ItemRef_FullName not like '%Freight%'            and
		" . LINEDETAIL_TABLE . ".ItemRef_FullName not like '%warranty%'           and        
		" . LINEDETAIL_TABLE . ".ItemRef_FullName is not NULL                     and
		" . LINEDETAIL_TABLE . ".ItemRef_FullName not like '%Subtotal%'           and
		" . LINEDETAIL_TABLE . ".ItemRef_FullName not like '%IDSC-10%'            and
		" . LINEDETAIL_TABLE . ".ItemRef_FullName not like '%DISCOUNT%'           and
		" . LINEDETAIL_TABLE . ".ItemRef_FullName not like '%SPECIAL ORDER%'    	and 	
		" . LINEDETAIL_TABLE . ".CustomField9 is not NULL							and 
		" . LINEDETAIL_TABLE . ".GroupIDKEY = " . GROUPDETAIL_TABLE . ".TxnLineID 
		and CAST([GenerateTimeStamp] AS DATE) = '" . $currentDate . "'
		and " . SHIPTRACK_TABLE . ".CustomerRef_FullName LIKE '%Sears%'";
	/* print_r($query);
	  die(); */

	try {
		$result = execQuery($connect, $query);
		$res = array();
		while ($row = $result->fetch()) {
			if ((!isset($row['ShipAddress_Addr1']) || empty($row['ShipAddress_Addr1'])) &&
					(!isset($row['ShipAddress_Addr2']) || empty($row['ShipAddress_Addr2'])) &&
					(!isset($row['ShipAddress_Addr3']) || empty($row['ShipAddress_Addr3'])) &&
					(!isset($row['ShipAddress_Addr4']) || empty($row['ShipAddress_Addr4'])) &&
					(!isset($row['ShipAddress_Addr5']) || empty($row['ShipAddress_Addr5']))) {
				error_msg("All address fields are empty!");
				data_error_msg("PO", $PONumber, "No SHIP TO address for this PO found!");
				send_error();
			} else {
				if (preg_match_all("/[0-9]/", $row['FOB']) == 10) {
					$phoneNumber = str_replace('-', '', $row['FOB']);
					$resultPhoneNumber = substr($phoneNumber, 0, 3) . '-' . substr($phoneNumber, 3, 3) . '-' . substr($phoneNumber, 6, 4);
				} else
					$resultPhoneNumber = '';

				$arr = array(
					'RefNumber' => $row['RefNumber'],
					'PONumber' => strtoupper($row['PONumber']),
					'ProductCode' => $row['ItemGroupRef_FullName'],
					'Descr' => $row['Prod_desc'],
					'CustomerRef_FullName' => $row['CustomerRef_FullName'],
					'ItemRef_FullName' => $row['ItemRef_FullName'],
					'quantity' => $row['quantity'],
					'ShipAddress_Addr1' => $row['ShipAddress_Addr1'],
					'ShipAddress_Addr2' => $row['ShipAddress_Addr2'],
					'ShipAddress_Addr3' => $row['ShipAddress_Addr3'],
					'ShipAddress_Addr4' => $row['ShipAddress_Addr4'],
					'ShipAddress_Addr5' => $row['ShipAddress_Addr5'],
					'ShipAddress_City' => $row['ShipAddress_City'],
					'ShipAddress_State' => $row['ShipAddress_State'],
					'ShipAddress_PostalCode' => $row['ShipAddress_PostalCode'],
					'FOB' => $resultPhoneNumber,
					'TimeCreated' => $row['TimeCreated'],
					'Memo' => $row['Memo'],
					'CARRIERNAME' => $row['CARRIERNAME'],
					'WEIGHT' => $row['WEIGHT'],
					'SERIAL_REFERENCE' => $row['SERIAL_REFERENCE'],
					'itemWeight' => $row['itemWeight']
				);
				$res[] = $arr;
			}
		}
		if (isset($res) && !empty($res)) {
			return $res;
		} else {
			error_msg("Can not get Sears orders on date: " . $currentDate);
			send_error();
		}
	} catch (Exception $e) {
		error_msg($e->getMessage());
		send_error();
	}
}

//Get UPC for each relevant product code
function getSKUHD($connect, $res) {
	try {
		$c = count($res);
		for ($i = 0; $i < $c; $i++) {
			$row2 = $res[$i];
			$ProductCode = $row2['ProductCode'];
//Sandeep (Jan-01-2014): added this code to strip the product code of the leading 'G' which represents a group.	
			if (substr($ProductCode, 0, 1) == 'G') {
				$codelen = strlen($ProductCode) - 1;
				$ProductCode = substr($ProductCode, 1, $codelen);
			}
			$query = "SELECT [HD_SKU] FROM " . DL_VALID_SKU . " WHERE sku = '" . $ProductCode . "'";
			$result = execQuery($connect, $query);
			$row3 = $result->fetch();
			$UPC = $row3['HD_SKU'];
			if (isset($UPC) && !empty($UPC)) {
				$row2['SKU'] = $UPC;
				$res[$i] = $row2;
			} else {
				data_error_msg("ProductCode", $row2['ProductCode'], "Can not get SKU!");
				unset($res[$i]);
				send_error();
			}
		}
		if (!empty($res)) {
			sort($res);
			return $res;
		} else {
			error_msg("Can not get any SKU!");
			send_error();
		}
	} catch (Exception $e) {
		error_msg($e->getMessage());
		send_error();
	}
}

//Get UPC for each relevant product code
function getUPC($connect, $res) {
    //$resTest = $res;
	try {
		$c = count($res);
		for ($i = 0; $i < $c; $i++) {
			$row2 = $res[$i];
			$ProductCode = $row2['ProductCode'];
//Sandeep (Jan-01-2014): added this code to strip the product code of the leading 'G' which represents a group.	
			if (substr($ProductCode, 0, 1) == 'G') {
				$codelen = strlen($ProductCode) - 1;
				$ProductCode = substr($ProductCode, 1, $codelen);
			}
			$query = "SELECT distinct upc FROM " . DL_VALID_SKU . " WHERE sku = '" . $ProductCode . "'";
			$result = execQuery($connect, $query);
			$row3 = $result->fetch();
			$UPC = $row3['upc'];
			if (isset($UPC) && !empty($UPC)) {
				$row2['UPC'] = $UPC;
				$res[$i] = $row2;
			} else {
				data_error_msg("ProductCode", $row2['ProductCode'], "Can not get UPC!");
				unset($res[$i]);
				send_error();
			}
		}
		if (!empty($res)) {
			sort($res);
			return $res;
		} else {
                        //print_r($resTest);
			error_msg("Can not get any UPC!");
			send_error();
		}
	} catch (Exception $e) {
		error_msg($e->getMessage());
		send_error();
	}
}

function getValidSKU($connect, $res) {
	try {
		$c = count($res);
		for ($i = 0; $i < $c; $i++) {
			$row2 = $res[$i];
			$ProductCode = $row2['ProductCode'];
//Sandeep (Jan-01-2014): added this code to strip the product code of the leading 'G' which represents a group.	
			if (substr($ProductCode, 0, 1) == 'G') {
				$codelen = strlen($ProductCode) - 1;
				$ProductCode = substr($ProductCode, 1, $codelen);
			}
			$query = "SELECT [Sears CommerceHub Merchant SKU], [Manufacturer Part Number] FROM " . DL_VALID_SKU . " WHERE SKU = '" . $ProductCode . "'";
			$result = execQuery($connect, $query);
			$row3 = $result->fetch();
			$validSKU = $row3['Sears CommerceHub Merchant SKU'];
			$PartNumber = $row3['Manufacturer Part Number'];
			if (isset($row3) && !empty($row3)) {
				$row2['validSKU'] = $validSKU;
				$row2['PartNumber'] = $PartNumber;
				$res[$i] = $row2;
			} else {
				data_error_msg("ProductCode", $row2['ProductCode'], "Can not get valid SKU!");
				unset($res[$i]);
				send_error();
			}
		}



		if (!empty($res)) {
			sort($res);
			return $res;
		} else {
			error_msg("Can not get any valid SKU!");
			send_error();
		}
	} catch (Exception $e) {
		error_msg($e->getMessage());
		send_error();
	}
}

function generateSSCCvalue($connect, $value, $po) {
	$retValue = false;
	$min = 1;
	$max = 99999999;

	if ($value == false) {
		try {
			$query = "SELECT MAX([SERIAL_REFERENCE]) as maxSerial
			FROM " . SHIPTRACKWITHLABEL_TABLE . " WHERE [SERIAL_REFERENCE] IS NOT NULL
			AND [DEALER] LIKE '%Depot%'";
			$result = execQuery($connect, $query);
			$result = $result->fetch(PDO::FETCH_ASSOC);
			$result = $result['maxSerial'];
		} catch (Exception $e) {
			error_msg($e->getMessage());
			send_error();
		}
		if (isset($result) && !empty($result) && $result > 0) {
			$result++;
			$resultToWrite = $result;
			if ($min <= $result && $result <= $max) {
				$result = str_pad($result, 8, "0", STR_PAD_LEFT);
				$retValue = (string) $result;
			} else {
				error_msg("Error! Value to generate GS-1 128 barcode is out of range! Please contact support.");
				send_error();
			}
		} else {
			$result = 1;
			$resultToWrite = $result;
			$result = str_pad($result, 8, "0", STR_PAD_LEFT);
			$retValue = (string) $result;
		}
		$query2 = "UPDATE " . SHIPTRACKWITHLABEL_TABLE . " SET [SERIAL_REFERENCE] = '" . $resultToWrite . "' WHERE [PONUMBER] = '" . $po . "'";
		try {
			$result = execQuery($connect, $query2);
		} catch (Exception $e) {
			error_msg($e->getMessage());
			send_error();
		}
	} else {
		$result = $value;
		$result = str_pad($result, 8, "0", STR_PAD_LEFT);
		$retValue = (string) $result;
	}

	return $retValue;
}

//Replacement of getProductCode function to test script
function test1() {
	return $arr = array(
		array(
			'RefNumber' => 'ref12345',
			'PONumber' => '45lk64ll456lk',
			'ProductCode' => 'DLVRB-102-WG',
			'Descr' => 'SOME-DOOR',
			'CustomerRef_FullName' => "Amazon",
			'TimeCreated' => '12-12-12 12:12:12',
			'quantity' => '3',
			'ShipAddress_Addr1' => 'ShipAddress_Addr1',
			'ShipAddress_Addr2' => 'ShipAddress_Addr2',
			'ShipAddress_Addr3' => 'ShipAddress_Addr3',
			'ShipAddress_Addr4' => 'ShipAddress_Addr4',
			'ShipAddress_Addr5' => 'ShipAddress_Addr5',
			'ShipAddress_City' => 'ShipAddress_City',
			'ShipAddress_State' => 'ShipAddress_State',
			'FOB' => 'FOB',
			'ShipAddress_PostalCode' => 'ShipAddress_PostalCode'),
		'CARRIERNAME' => 'fedex',
		'WEIGHT' => '286'
	); // test data
}

?>