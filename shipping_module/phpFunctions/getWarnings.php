<?php

$path = $_SERVER['DOCUMENT_ROOT'];
require_once($path.'/db_connect/connect.php');



	if (isset($_REQUEST['po']) && !empty($_REQUEST['po']) && isset($_REQUEST['id']) && !empty($_REQUEST['id']))
	{
		$returnValue = false;
		$id = $_REQUEST['id'];
		$po = $_REQUEST['po'];
		$conn = Database::getInstance()->dbc;
		$res = getWarnings($conn, $id, $po);
		$res2 = getWarnings2($conn, $id, $po);
		$res3 = getWarnings3($conn, $id, $po);
		$res4 = getWarnings4($conn, $id, $po);
		$resultArray = array();
		
		if (isset($res2) && !empty($res2)) 
		{
			foreach ($res2 as $key => $value)
			{
				$resultArray[]= $value['Name'].': '.$value['CustomField6'];
			}
		}
		if (isset($res) && !empty($res)) 
		{
			foreach ($res as $key => $value)
			{
				$resultArray[]= $value['Warning'];
			}
		}
		if (isset($res3) && !empty($res3)) 
		{
			foreach ($res3 as $key => $value)
			{
				$resultArray[]= $value['recalculation'];
			}
		}
		if (isset($res4) && !empty($res4)) 
		{
			foreach ($res4 as $key => $value)
			{
				$resultArray[]= $value['ShipMethodRef_FullName'].' - Order last modified - '.$value['TimeModified'];
			}
		}
		if(!empty($resultArray)) $returnValue = $resultArray;
		$returnValue = json_encode($returnValue);
		print_r($returnValue);
		$conn = null;
	} else 
	{
		$returnValue = 'Id or po is missing.';
		$returnValue = json_encode($returnValue);
		print_r($returnValue);
	}

function getWarnings($conn, $id, $po)
{
	try{
		$query = "SELECT 
      [Warning]
  FROM [dbo].[warnings_log]
  WHERE [RefNumber] = '".$id."'
  AND [PONumber] = '".$po."'";
		$result = $conn->prepare($query);
		$result->execute();
		$result = $result->fetchAll(PDO::FETCH_ASSOC); 
		return $result;
	}
	catch(PDOException $e)
	{
		return false;
	}
}

function getWarnings2($conn, $id, $po)
{
	try{
		$query = "SELECT 
/*[ShipTrackWithLabel].[ID],*/
[itemgroup].[Name],
[itemgroup].[CustomField6]
FROM [".DATABASE31."].[dbo].[salesorderlinegroupdetail]
LEFT JOIN [".DATABASE31."].[dbo].[itemgroup] ON [itemgroup].[Name] = [salesorderlinegroupdetail].[ItemGroupRef_FullName]
INNER JOIN [".DATABASE31."].[dbo].[salesorder] ON [salesorderlinegroupdetail].[IDKEY] = [salesorder].[TxnID]
INNER JOIN [".DATABASE."].[dbo].[ShipTrackWithLabel] ON [ShipTrackWithLabel].[PONUMBER] = [salesorder].[PONumber]
WHERE [ShipTrackWithLabel].[ID] = '".$id."'
AND 
(
	[itemgroup].[CustomField6] LIKE '%DO NOT SHIP%'
	OR [itemgroup].[CustomField6] LIKE '%SHIP GLASS OD205 AND BELOW%'
	OR [itemgroup].[CustomField6] LIKE '%SHIP GLASS OD206 AND ABOVE%'
)";
		$result = $conn->prepare($query);
		$result->execute();
		$result = $result->fetchAll(PDO::FETCH_ASSOC); 
		return $result;
	}
	catch(PDOException $e)
	{
		return false;
	}
}

function getWarnings3($conn, $id, $po)
{
	try{
		$query = "SELECT 
		'Warning: May need recalculation! SHIP_METHOD was updated!' as [recalculation]
		FROM [".DATABASE."].[dbo].[groupdetail]
		LEFT JOIN [".DATABASE."].[dbo].[shiptrack] ON [shiptrack].[TxnID] = [groupdetail].[IDKEY]
		LEFT JOIN [".DATABASE."].[dbo].[ShipTrackWithLabel] ON [ShipTrackWithLabel].[ID] = [shiptrack].[RefNumber]
		INNER JOIN [".DATABASE."].[dbo].[SHIP_METHOD_CHANGED] ON [groupdetail].[ItemGroupRef_FullName] = [SHIP_METHOD_CHANGED].[SKU]
		WHERE [ShipTrackWithLabel].[DATE] < [SHIP_METHOD_CHANGED].[CHANGE_DATE]
		AND [ShipTrackWithLabel].[ID] =  '".$id."'
		GROUP BY [ShipTrackWithLabel].[ID]";
		$result = $conn->prepare($query);
		$result->execute();
		$result = $result->fetchAll(PDO::FETCH_ASSOC); 
		return $result;
	}
	catch(PDOException $e)
	{
		return false;
	}
}

function getWarnings4($conn, $id, $po)
{
	try{
		$query = "SELECT [ShipMethodRef_FullName], [TimeModified]
		  FROM [".DATABASE31."].[dbo].[salesorder]
		  WHERE ([ShipMethodRef_FullName] LIKE '%BACKORDER%'
		  OR [ShipMethodRef_FullName] LIKE '%CANCELLED%'
		  OR [ShipMethodRef_FullName] LIKE '%CC DECLINED%'
		  OR [ShipMethodRef_FullName] LIKE '%ON HOLD%'
		  OR [ShipMethodRef_FullName] LIKE '%ON HOLD-INCOMPL%'
		  OR [ShipMethodRef_FullName] LIKE '%ON HOLD - PSI%'
		  OR [ShipMethodRef_FullName] LIKE '%PARTIAL_SHIP%'
		  OR [ShipMethodRef_FullName] LIKE '%PRODUCTION%'
		  OR [ShipMethodRef_FullName] LIKE '%VOID%')
		  AND [PONumber] = '".$po."'";
		$result = $conn->prepare($query);
		$result->execute();
		$result = $result->fetchAll(PDO::FETCH_ASSOC); 
		return $result;
	}
	catch(PDOException $e)
	{
		return false;
	}
}
?>