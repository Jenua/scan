<?php
$path = $_SERVER['DOCUMENT_ROOT'];
require_once($path.'/db_connect/connect.php');

function retQueryOne( $conn, $query ) {
	$result = $conn->prepare($query);
    $result->execute();
	$ret  = $result->fetch(PDO::FETCH_ASSOC);
	return $ret;
}
function retQueryAll( $conn, $query ) {
	$result = $conn->prepare($query);
    $result->execute();
	$ret  = $result->fetchAll(PDO::FETCH_ASSOC);
	return $ret;
}
function getDealerType( $conn, $type ) {
	$query = "SELECT TOP 1 * FROM [dc_type] WHERE id = ".$conn->quote( $type );
	return retQueryOne( $conn, $query );
}
function getProductsList( $conn, $po ) {
	$query   = "SELECT
					DISTINCT gr.[ItemGroupRef_FullName] as [GroupSKU]
				FROM [groupdetail] AS gr
				LEFT JOIN [shiptrack] as st ON st.[TxnId] = gr.[IDKEY]
				WHERE st.[PONumber] = ".$conn->quote( $po )."
				;";
	return retQueryAll( $conn, $query );
}
function getPalletsList( $conn, $po ) {
	$query   = "SELECT 
					pp.[PALLET]
				FROM [pallet_preload] AS pp
				WHERE pp.[PONumber] = ".$conn->quote( $po )."
				GROUP BY pp.[PALLET]
				ORDER BY LEN(pp.[PALLET]), pp.[PALLET]
				;";
	return retQueryAll( $conn, $query );
}
function getTrucksList( $conn, $po ) {
	$query   = "SELECT 
					tp.[TRUCK]
				FROM [truck_preload] AS tp
				WHERE tp.[PONumber] = ".$conn->quote( $po )."
				GROUP BY tp.[TRUCK]
				ORDER BY LEN(tp.[TRUCK]), tp.[TRUCK]
				;";
	return retQueryAll( $conn, $query );	
}
function checDoc( $name ) {
	$FOLDER    = 'Html';
	$EXTENSION = '.html';
	$filename  = $name.$EXTENSION;
	$path      = $FOLDER."/".$filename;
	$fileExist = file_exists("../".$path);
	
	return $fileExist ? $path : '';
}

//=======================================================================================================================================

if( empty($_REQUEST['po']) || empty($_REQUEST['dealer']) ) {
	die(json_encode(['error'=>'Not enough data']));
}

$conn   = Database::getInstance()->dbc;
$po     = trim($_REQUEST['po']);
$type   = empty($_REQUEST['dealer']) ? '1' : $_REQUEST['dealer'];
$dealer = getDealerType( $conn, $type );

$palletsA  = [];
$trucksA   = [];
$productsA = [];

if( $dealer['isPallet'] ) {
	$pallets  = getPalletsList( $conn, $po );
	$trucks   = getTrucksList( $conn, $po );	
	
	if( $pallets ) {
		foreach( $pallets as $pallet ) {
			$pName = $pallet['PALLET'];
			
			$flyer   = $po . '_flyers_' . $pName;
			$barcode = $po . '_barcodes_' . $pName;
			$picking = $po . '_picking_' . $pName;
			$packing = $po . '_packing_' . $pName;
			$carton  = $po . '_carton_' . $pName;
			
			$palletsA[$pName] = [
				'flyers'   => checDoc( $flyer ),
				'barcodes' => checDoc( $barcode ),
				'picking'  => checDoc( $picking ),
				'packing'  => checDoc( $packing ),
				'carton'   => checDoc( $carton ),
			];
		}
	}
	if( $trucks ) {
		foreach( $trucks as $truck ) {
			$tName = $truck['TRUCK'];
			
			$truck   = $po . '_truck_' . $tName;
			
			$trucksA[$tName] = [
				'truck' => checDoc( $truck )
			];
		}
	}
	
} else {
	$products = getProductsList( $conn, $po );
	
	if( $products ) {
		foreach( $products as $product ) {
			$pName = $product['GroupSKU'];
			
			$flyer   = $po . '_flyers_' . $pName;
			$barcode = $po . '_barcodes_' . $pName;
			
			$productsA[$pName] = [
				'flyers'   => checDoc( $flyer ),
				'barcodes' => checDoc( $barcode ),				
			];
		}
	}
	
	$oName = 'Order';
	
	$flyer   = $po . '_flyers_' . $oName;
	$barcode = $po . '_barcodes_' . $oName;
	$picking = $po . '_picking_' . $oName;
	$packing = $po . '_packing_' . $oName;
	$carton  = $po . '_carton_' . $oName;

	$palletsA[$oName] = [
		'flyers'   => checDoc( $flyer ),
		'barcodes' => checDoc( $barcode ),
		'picking'  => checDoc( $picking ),
		'packing'  => checDoc( $packing ),
		'carton'   => checDoc( $carton ),
	];	
}

$ret = [
	'pallets'  => $palletsA,
	'trucks'   => $trucksA,
	'products' => $productsA,	
];

echo json_encode( $ret );