<?php
require('functionsDB.php');
require('functionsExtr.php');
$path = $_SERVER['DOCUMENT_ROOT'];
require_once($path.'/db_connect/connect.php');

function exec_query($conn, $query) {
	try {
		$result = $conn->prepare($query);
		$result->execute();
		return $result;
	}
	catch(PDOException $e) {
		print_r($e->getMessage());
	}
}
function returnValue( $type, $value ) {
	$error = [
		$type => $value
	];
	unset( $conn );
	die( json_encode($error) );	
}

function checkOldData( $conn, $prod, $rel=false ) {
	$check1Query = "SELECT
						count(fpt.[ID]) as Count
					FROM [fpt_related] AS fpt				
					WHERE [Product] = ".$prod."
					".($rel?"AND [Related] = ".$rel:"").";";
	$check1Result = exec_query($conn, $check1Query)->fetch();
	return $check1Result['Count'];
}

$conn    = Database::getInstance()->dbc;
$type    = empty($_REQUEST['type']) ? false : $_REQUEST['type'];
$product = empty($_REQUEST['product']) ? false : $conn->quote($_REQUEST['product']);
$oldVal  = empty($_REQUEST['oldVal']) ? false : $conn->quote($_REQUEST['oldVal']);
$newVal  = empty($_REQUEST['newVal']) ? false : $conn->quote($_REQUEST['newVal']);
$user    = empty($_REQUEST['user']) ? false : $conn->quote($_REQUEST['user']);

// GLOBAL CHECKS
if( $type === false ) {
	unset( $conn );
	returnValue('error', 'Action Type missing');
}
if( $type == 'delete' && $oldVal === false ) {
	unset( $conn );
	returnValue('error', 'Nothing to delete');
}
if( $type == 'restore' && $oldVal === false ) {
	unset( $conn );
	returnValue('error', 'Nothing to restore');
}
if( $type == 'related' && $product === false ) {
	unset( $conn );
	returnValue('error', 'Product info missing');
}
if( ($type == 'product' || $type == 'related') && $newVal === false ) {
	unset( $conn );
	returnValue('error', 'New value missing');	
}

//DELETE CHECK
if( $type == 'delete' ){
	$rel = $product==$oldVal ? false : $oldVal;	
	if( checkOldData($conn, $product, $rel) > 0 ) {
		$compareStr = ($product==$oldVal)
					? "[Product] = ".$oldVal
					: "[Product] = ".$product." AND [Related] = ".$oldVal;
		
		$message = 'User '.$user.' deleted ' . (($product==$oldVal) ? ($oldVal.' product') : ($oldVal.' related to product '.$product));
				
		$deleteQuery  = "DELETE FROM [fpt_related] WHERE ".$compareStr;
		$deleteQuery .= "; INSERT INTO [log] ([po],[message],[DATE]) VALUES ('FPTRelated', ".$conn->quote($message).", GETDATE());";
		$deleteResult = exec_query($conn, $deleteQuery);
		unset( $conn );
		returnValue('result', '1');
	} else {
		//NOTHING TO DELETE
		unset( $conn );
		returnValue('result', '1');		
	}
}

if( $type == 'restore' ){
	$restoreQuery  = "UPDATE [orders_statuses] SET [isActive] = 1 WHERE [id] = ".$oldVal;
	$restoreResult = exec_query($conn, $restoreQuery);
	unset( $conn );
	returnValue('result', '1');
}

if( $oldVal ) {
	$rel = $product==$oldVal ? false : $oldVal;	
	if( checkOldData($conn, $product, $rel) == 0 ) {
		unset( $conn );
		returnValue('error', 'Row not found');
	}
}

$check2Query = "SELECT
					Count([SKU]) as Count
				FROM [DL_valid_SKU]
				WHERE [SKU] = ".$newVal."
				AND [UPC] IS NOT NULL
				AND ISNULL([QB_SKU], '') != ISNULL([Menards_SKU], '');";
$check2Result = exec_query($conn, $check2Query)->fetch();
if( $check2Result['Count'] == 0 ) {
	unset( $conn );
	returnValue('error', 'New value is incorrect');	
}

// == CHECK NEW VALUE IN FPT Related
$compareStr = $type=='related'
			? "[Product] = ".$product." AND [Related] = ".$newVal
			: "[Product] = ".$newVal;
$check3Query = "SELECT
					Count([ID]) as Count
				FROM [fpt_related]
				WHERE ".$compareStr.";";
$check3Result = exec_query($conn, $check3Query)->fetch();
if( $check3Result['Count'] > 0 ) {
	unset( $conn );
	returnValue('error', 'New value already exists');
}

if( $oldVal != '' ) {
	// UPDATE
	$compareStr = $type=='related'
				? "[Product] = ".$product." AND [Related] = ".$oldVal
				: "[Product] = ".$oldVal;			
	$updateStr = $type=='related'
			   ? "[Related] = ".$newVal
			   : "[Product] = ".$newVal;			
	
	$message = 'User '.$user.' updated '. ($type=='related') ? ($product.' product with related '.$oldVal) : ($oldVal.' product');
	
	$updateQuery  = "UPDATE [fpt_related] SET ".$updateStr." WHERE ".$compareStr;
	$updateQuery .= "; INSERT INTO [log] ([po],[message],[DATE]) VALUES ('FPTRelated', ".$conn->quote($message).", GETDATE());";
	$updateResult = exec_query($conn, $updateQuery);
	unset( $conn );
	returnValue('result', '1');	

} else {
	// INSERT
	if( $type=='related' ) {
		
		$message = 'User '.$user.' added new '.$newVal.' related tp product '.$product;
		
		$insertQuery  = "INSERT INTO [fpt_related] ([Product],[Related]) VALUES(".$product.",".$newVal.");";
		$insertQuery .= "; INSERT INTO [log] ([po],[message],[DATE]) VALUES ('FPTRelated', ".$conn->quote($message).", GETDATE());";
		
		$insertResult = exec_query($conn, $insertQuery);
		unset( $conn );
		returnValue('result', '1');
	} else {
		unset( $conn );
		returnValue('result', '1');
	}
}