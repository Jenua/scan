<?php
session_start();
$path = $_SERVER['DOCUMENT_ROOT'];
require_once($path.'/db_connect/connect.php');
require_once($path.'/auth/auth.php');
require_once($path.'/shipsingle/functions.php');

date_default_timezone_set('America/New_York');
$auth = new AuthClass();

if ($auth->isAuth()) {
	$Ref      = (isset($_REQUEST['Ref']) && !empty($_REQUEST['Ref'])) ? $_REQUEST['Ref'] : NULL;
	$Dealer   = (isset($_REQUEST['Dealer']) && !empty($_REQUEST['Dealer'])) ? $_REQUEST['Dealer'] : NULL;
	$Carrier  = (isset($_REQUEST['Carrier']) && !empty($_REQUEST['Carrier'])) ? $_REQUEST['Carrier'] : NULL;
	$CostTime = (isset($_REQUEST['CostTime']) && !empty($_REQUEST['CostTime'])) ? $_REQUEST['CostTime'] : NULL;
	$Cost     = (isset($_REQUEST['Cost']) && !empty($_REQUEST['Cost'])) ? $_REQUEST['Cost'] : 0;
	$Time     = (isset($_REQUEST['Time']) && !empty($_REQUEST['Time'])) ? $_REQUEST['Time'] : 0;
	
	if( $Ref != NULL && $Dealer != NULL && $Carrier != NULL ) {
		if( $CostTime != 'on' ) {
			$Cost = 0;
			$Time = 0;
		}		
		$conn = Database::getInstance()->dbc;

		$settings = getSettings($conn);
		$authorized_dealers = array();
		$FreightClass = false;
		$originZip = false;
		$originStatecode = false;
		$originCountry = false;
		$originCity = false;
		$pro_numbers_estes_min = false;
		$pro_numbers_estes_max = false;
		$pro_numbers_estes_fd = false;
		$pro_numbers_fedex_min = false;
		$pro_numbers_fedex_max = false;
		$pro_numbers_rl_min = false;
		$pro_numbers_rl_max = false;
		$pro_numbers_ups_min = false;
		$pro_numbers_ups_max = false;
		$pro_numbers_nemf_min = false;
		$pro_numbers_nemf_max = false;
		$pro_numbers_yrc_min = false;
		$pro_numbers_yrc_max = false;
		$pro_numbers_amazonds_min = false;
		$pro_numbers_amazonds_max = false;
		$pro_numbers_yrc_before_hyphen = false;
		$pro_numbers_odfl_min = false;
		$pro_numbers_odfl_max = false;
		$ziphead_not_allowed_RL = array();
		$rl_pal_addon_cost_liftgate = 0;
		$fedex_addon_cost_liftgate = 0;
		$ORDER_PROCESSING_MODE_FEDEX = false;
		$rl_pal_addon_cost_residential = 0;
		$query_mode_dealer_setting = false;
		$remote_orders = array();
		$remote_orders_dealer_name = array();
		$shipping_ground = array();
		$PrintCarrierForAmazonMode = false;
		$shipping_ground_weight_max = false;
		$pro_numbers_pyle_min = false;
		$pro_numbers_pyle_max = false;
		$rate_ups = false;

		foreach ($settings as $setting)
		{
			if ($setting['group_name'] == 'authorized_dealers' && $setting['type_name'] == 'dealer_name')
			{
				$authorized_dealers[]=$setting['setting_value'];
			} else if ($setting['group_name'] == 'freight_setting' && $setting['type_name'] == 'FreightClass')
			{
				$FreightClass=$setting['setting_value'];
			} else if ($setting['group_name'] == 'UPS' && $setting['type_name'] == 'rate')
			{
				$rate_ups=$setting['setting_value'];
			} else if ($setting['group_name'] == 'origin_address' && $setting['type_name'] == 'zip')
			{
				$originZip=$setting['setting_value'];
			} else if ($setting['group_name'] == 'origin_address' && $setting['type_name'] == 'statecode')
			{
				$originStatecode=$setting['setting_value'];
			} else if ($setting['group_name'] == 'origin_address' && $setting['type_name'] == 'country')
			{
				$originCountry=$setting['setting_value'];
			} else if ($setting['group_name'] == 'origin_address' && $setting['type_name'] == 'city')
			{
				$originCity=$setting['setting_value'];
			} else if ($setting['group_name'] == 'pro_numbers_estes' && $setting['type_name'] == 'min')
			{
				$pro_numbers_estes_min=$setting['setting_value'];
			} else if ($setting['group_name'] == 'pro_numbers_estes' && $setting['type_name'] == 'max')
			{
				$pro_numbers_estes_max=$setting['setting_value'];
			} else if ($setting['group_name'] == 'pro_numbers_estes' && $setting['type_name'] == 'first digit')
			{
				$pro_numbers_estes_fd=$setting['setting_value'];
			} else if ($setting['group_name'] == 'pro_numbers_fedex' && $setting['type_name'] == 'min')
			{
				$pro_numbers_fedex_min=$setting['setting_value'];
			} else if ($setting['group_name'] == 'pro_numbers_fedex' && $setting['type_name'] == 'max')
			{
				$pro_numbers_fedex_max=$setting['setting_value'];
			} else if ($setting['group_name'] == 'pro_numbers_rl' && $setting['type_name'] == 'min')
			{
				$pro_numbers_rl_min=$setting['setting_value'];
			} else if ($setting['group_name'] == 'pro_numbers_rl' && $setting['type_name'] == 'max')
			{
				$pro_numbers_rl_max=$setting['setting_value'];
			} else if ($setting['group_name'] == 'pro_numbers_ups' && $setting['type_name'] == 'min')
			{
				$pro_numbers_ups_min=$setting['setting_value'];
			} else if ($setting['group_name'] == 'pro_numbers_ups' && $setting['type_name'] == 'max')
			{
				$pro_numbers_ups_max=$setting['setting_value'];
			} else if ($setting['group_name'] == 'pro_numbers_nemf' && $setting['type_name'] == 'min')
			{
				$pro_numbers_nemf_min=$setting['setting_value'];
			} else if ($setting['group_name'] == 'pro_numbers_nemf' && $setting['type_name'] == 'max')
			{
				$pro_numbers_nemf_max=$setting['setting_value'];
			} else if ($setting['group_name'] == 'pro_numbers_yrc' && $setting['type_name'] == 'min')
			{
				$pro_numbers_yrc_min=$setting['setting_value'];
			} else if ($setting['group_name'] == 'pro_numbers_yrc' && $setting['type_name'] == 'max')
			{
				$pro_numbers_yrc_max=$setting['setting_value'];
			} else if ($setting['group_name'] == 'pro_numbers_amazonds' && $setting['type_name'] == 'min')
			{
				$pro_numbers_amazonds_min=$setting['setting_value'];
			} else if ($setting['group_name'] == 'pro_numbers_amazonds' && $setting['type_name'] == 'max')
			{
				$pro_numbers_amazonds_max=$setting['setting_value'];
			} else if ($setting['group_name'] == 'pro_numbers_yrc' && $setting['type_name'] == 'before_hyphen')
			{
				$pro_numbers_yrc_before_hyphen=$setting['setting_value'];
			} else if ($setting['group_name'] == 'pro_numbers_odfl' && $setting['type_name'] == 'min')
			{
				$pro_numbers_odfl_min=$setting['setting_value'];
			} else if ($setting['group_name'] == 'pro_numbers_odfl' && $setting['type_name'] == 'max')
			{
				$pro_numbers_odfl_max=$setting['setting_value'];
			} else if ($setting['group_name'] == 'pro_numbers_pyle' && $setting['type_name'] == 'min')
			{
				$pro_numbers_pyle_min=$setting['setting_value'];
			} else if ($setting['group_name'] == 'pro_numbers_pyle' && $setting['type_name'] == 'max')
			{
				$pro_numbers_pyle_max=$setting['setting_value'];
			} else if ($setting['group_name'] == 'rl_pal_addon_cost' && $setting['type_name'] == 'liftgate')
			{
				$rl_pal_addon_cost_liftgate=$setting['setting_value'];
			} else if ($setting['group_name'] == 'fedex_addon_cost' && $setting['type_name'] == 'liftgate')
			{
				$fedex_addon_cost_liftgate=$setting['setting_value'];
			} else if ($setting['group_name'] == 'order_processing_mode' && $setting['type_name'] == 'fedex')
			{
				$ORDER_PROCESSING_MODE_FEDEX=$setting['setting_value'];
			} else if ($setting['group_name'] == 'PrintCarrierForAmazon' && $setting['type_name'] == 'PrintCarrierForAmazonMode')
			{
				$PrintCarrierForAmazonMode=$setting['setting_value'];
			} else if ($setting['group_name'] == 'rl_pal_addon_cost' && $setting['type_name'] == 'residential')
			{
				$rl_pal_addon_cost_residential=$setting['setting_value'];
			} else if ($setting['group_name'] == 'query_mode' && $setting['type_name'] == 'dealer_setting')
			{
				$query_mode_dealer_setting=$setting['setting_value'];
			} else if ($setting['group_name'] == 'remote_orders' && $setting['type_name'] == 'state')
			{
				$remote_orders[]=$setting['setting_value'];
			} else if ($setting['group_name'] == 'ziphead_not_allowed' && $setting['type_name'] == 'RL')
			{
				$ziphead_not_allowed_RL[]=$setting['setting_value'];
			} else if ($setting['group_name'] == 'remote_orders' && $setting['type_name'] == 'dealer_name')
			{
				$remote_orders_dealer_name[]=$setting['setting_value'];
			} else if ($setting['group_name'] == 'shipping_ground' && $setting['type_name'] == 'sku')
			{
				$shipping_ground[]=$setting['setting_value'];
			} else if ($setting['group_name'] == 'shipping_ground_weight' && $setting['type_name'] == 'max')
			{
				$shipping_ground_weight_max=$setting['setting_value'];
			}
		}
		
		clearProFromShipTrackWithLabel( $conn, $Ref, $Dealer );
		ob_start();
		$res = updateToShipTrackWithLabel($conn, $Ref, $Carrier, "N/A", $Time, $Cost, $Dealer, true);		
		$err = ob_get_clean();
		
		$result = '';
		
		if( $res ) {
			if(is_array($res) && $res['pro'] ) {
				$result .= 'New PRO# assigned: <b>'.$res['pro'].'</b><br />';
			} else {
				$result .= 'New PRO# assigned: <b>'.$res.'</b><br />';
			}
		} else {
			$result .= 'No PRO# assigned<br />';
		}
		if( $err ) {
			$result .= $err;
		}
		$res_log = change_carrier_log($conn, $Ref, $_REQUEST['PONUMBER'], $_REQUEST['old_CARRIERNAME'], $_REQUEST['old_COST'], $_REQUEST['old_TRANSITTIME'], $_REQUEST['old_PRO'], $Carrier, $Time, $Cost, $auth->getName());
		if(!$res_log) $result .= 'Can not save change to log.<br />';
		
		print_r(json_encode($result));
	}
} else {
	print_r(json_encode('Erorr: User not autorized'));
}

function exec_query($conn, $query)
{
	try{
		$result = $conn->prepare($query);
		$result->execute();
		return $result;
	}
	catch(PDOException $e)
	{
		print_r($e->getMessage());
	}
}

function display_message($value)
{
	print_r($value);	
}
?>