<?php
$path = $_SERVER['DOCUMENT_ROOT'];
require_once($path.'/db_connect/connect.php');

$conn = Database::getInstance()->dbc;
$result = getRefNumbers($conn);
if ($result)
{
	$result = json_encode($result);
	print_r($result);
} else
{
	$result = json_encode('error');
	print_r($result);
}
$conn = null;

function getRefNumbers($conn)
{
	$query1 = "SELECT 
shiptrack.RefNumber
FROM shiptrack 
INNER JOIN ShipTrackWithLabel ON shiptrack.RefNumber=ShipTrackWithLabel.ID 
LEFT JOIN Orders_shipped ON ShipTrackWithLabel.PONUMBER=Orders_shipped.POnumber 
LEFT JOIN Orders_picked ON ShipTrackWithLabel .PONUMBER=Orders_picked.POnumber
UNION ALL
SELECT 
manually_shiptrack.RefNumber
FROM manually_shiptrack 
INNER JOIN ShipTrackWithLabel ON manually_shiptrack.RefNumber=ShipTrackWithLabel.ID 
LEFT JOIN Orders_shipped ON ShipTrackWithLabel.PONUMBER=Orders_shipped.POnumber 
LEFT JOIN Orders_picked ON ShipTrackWithLabel .PONUMBER=Orders_picked.POnumber";
	$result1 = $conn->prepare($query1);
	$result1->execute();
	$result1 = $result1->fetchAll(PDO::FETCH_ASSOC);
	return $result1;
}
?>