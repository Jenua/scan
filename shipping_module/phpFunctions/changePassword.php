<?php
session_start();
$path = $_SERVER['DOCUMENT_ROOT'];
require_once($path.'/db_connect/connect.php');
require_once($path.'/auth/auth.php');
date_default_timezone_set('America/New_York');
$auth = new AuthClass();

if ($auth->isAuth()) {
	if (isset($_REQUEST['id']) && !empty($_REQUEST['id'])
		&& isset($_REQUEST['oldPass']) && !empty($_REQUEST['oldPass'])
		&& isset($_REQUEST['newPass']) && !empty($_REQUEST['newPass']))
	{
		$id = $_REQUEST['id'];
		$oldPass = $_REQUEST['oldPass'];
		$newPass = $_REQUEST['newPass'];

		$conn = Database::getInstance()->dbc;
		$query0 = "
		SELECT [password]
		  FROM [dbo].[".USER_TABLE."]
		  WHERE [ID] = ".$id;
		$res0 = exec_query($conn, $query0);
		//$res0 = $res0->fetchAll(PDO::FETCH_ASSOC);
		$res0 = $res0->fetch(PDO::FETCH_ASSOC);
		if ($res0 && $res0['password'] == $oldPass)
		{
			$query1 = "
			UPDATE [dbo].[".USER_TABLE."]
			   SET [password] = '".$newPass."'
			WHERE [ID] = ".$id;
			$res1 = exec_query($conn, $query1);
			if ($res1) print_r(json_encode('pass_updated')); else print_r(json_encode('pass_update_error'));
		} else print_r(json_encode('wrong_pass'));
	} else print_r(json_encode('no_data'));
	$conn = null;
} else print_r(json_encode('no_auth'));

function exec_query($conn, $query)
{
	try{
		$result = $conn->prepare($query);
		$result->execute();
		return $result;
	}
	catch(PDOException $e)
	{
		print_r($e->getMessage());
	}
}
?>