<?php

$path = $_SERVER['DOCUMENT_ROOT'];
require_once($path.'/db_connect/connect.php');
		
		$conn = Database::getInstance()->dbc;
		$returnValue = getProdNumber($conn);
		$returnValue = json_encode($returnValue);
		print_r($returnValue);
		$conn = null;

function sortProdNumber($result1)
{
	/*print_r('<pre>');
	print_r($result1);
	print_r('</pre>');*/
	foreach($result1 as $key1 => $res1)
	{
		$id1 = explode('_split_', $res1['RefNumber']);
		$id1 = $id1[0];
		foreach ($result1 as $key2 => $res2)
		{
			$id2 = explode('_split_', $res2['RefNumber']);
			$id2 = $id2[0];
			if (($id1 == $id2) && ($key1 != $key2))
			{
				if (isset($result1[$key1]['sum']) && isset($result1[$key2]['sum']))
				{
					$result1[$key1]['sum'] = $result1[$key1]['sum'] + $result1[$key2]['sum'];
					unset($result1[$key2]);
				}
			}
		}
	}
	return $result1;
}

function getProdNumber($conn)
{
	$query1 = "SELECT [RefNumber], SUM(CAST([Quantity] AS INT)) AS sum
FROM
(
SELECT 
[manually_shiptrack].[RefNumber],
case when [manually_groupdetail].[Quantity] IS NULL THEN '1' ELSE [manually_groupdetail].[Quantity] END as [Quantity]
FROM [manually_groupdetail]
INNER JOIN [manually_shiptrack] ON [manually_groupdetail].[IDKEY] = [manually_shiptrack].[TxnID]
where [manually_shiptrack].[RefNumber] like '%_split_%'
) SUM1
GROUP BY [RefNumber]";
	$query2 = "SELECT [RefNumber], SUM(CAST([Quantity] AS INT)) AS sum
FROM
(
SELECT 
[shiptrack].[RefNumber],
case when [groupdetail].[Quantity] IS NULL THEN '1' ELSE [groupdetail].[Quantity] END as [Quantity]
FROM [groupdetail]
INNER JOIN [shiptrack] ON [groupdetail].[IDKEY] = [shiptrack].[TxnID]
) SUM2
GROUP BY [RefNumber]";
	$result1 = $conn->prepare($query1);
	$result1->execute();
	$result1 = $result1->fetchAll(PDO::FETCH_ASSOC);
	$result1 = sortProdNumber($result1);
	
	$result2 = $conn->prepare($query2);
	$result2->execute();
	$result2 = $result2->fetchAll(PDO::FETCH_ASSOC);
	
	/*print_r('<pre>');
	print_r($result1);
	print_r($result2);
	print_r('</pre>');
	die();*/
	
	$allRefNumbers2 = array();
	foreach($result2 as $res2)
	{
		$allRefNumbers2[]=$res2['RefNumber'];
	}
	/*print_r('<pre>');
	print_r($allRefNumbers2);
	print_r('</pre>');
	die();*/
	
	$returnValue = array();
	foreach ($result2 as $res2)
	{
		if (!empty($result1))
		{
			$flag = false;
			foreach ($result1 as $res1)
			{
				$id1 = explode('_split_', $res1['RefNumber']);
				$id1 = $id1[0];
				if ($res2['RefNumber'] == $id1)
				{
					if (!empty($res1['sum']) && !empty($res2['sum']))
					{
						$text1 = $res1['sum'];
						$text2 = $res2['sum'];
					} else if (!empty($res2['sum']) && empty($res1['sum']))
					{
						$text1 = '0';
						$text2 = $res2['sum'];
					} else
					{
						$text1 = '-';
						$text2 = '-';
					}
					$returnValue[]= array('id' => $res2['RefNumber'], 'text1' => $text1, 'text2' => $text2);
					$flag = true;
				}
			}
			if (!$flag)
				{
					$text1 = '0';
					$text2 = $res2['sum'];
					$returnValue[]= array('id' => $res2['RefNumber'], 'text1' => $text1, 'text2' => $text2);
				}
		} else
		{
			$text1 = '0';
			$text2 = $res2['sum'];
			$returnValue[]= array('id' => $res2['RefNumber'], 'text1' => $text1, 'text2' => $text2);
		}
	}
	return $returnValue;
}
?>