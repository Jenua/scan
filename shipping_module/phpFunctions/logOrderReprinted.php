<?php
$path = $_SERVER['DOCUMENT_ROOT'];
require_once($path.'/db_connect/connect.php');
date_default_timezone_set('America/New_York');

	if (isset($_REQUEST['po']) && !empty($_REQUEST['po'])
		&& isset($_REQUEST['reason'])
		&& isset($_REQUEST['reasonNote'])
		&& isset($_REQUEST['userName']))
	{
		$po = $_REQUEST['po'];
		$reason = $_REQUEST['reason'];
		$reasonNote = $_REQUEST['reasonNote'];
		$reasonNote = str_replace("'", "''", $reasonNote);
		$reasonNote = htmlspecialchars($reasonNote);
		$userName = $_REQUEST['userName'];

		$conn = Database::getInstance()->dbc;
		$query = "INSERT INTO [dbo].[Orders_reprinted]
				   ([POnumber]
				   ,[ReprintDate]
				   ,[reason]
				   ,[reasonNote]
				   ,[userName])
			 VALUES
				  ('".$po."'
				   ,GETDATE()
				   ,'".$reason."'
				   ,'".$reasonNote."'
				   ,'".$userName."')";
		$res1 = exec_query($conn, $query);
		
		
		if ($res1)
		{
			echo "Order with po: '".$po."' logged as reprinted";
		} else echo "Error. Can not log order as reprinted. po: '".$po."'";
		$conn = null;
	}

function exec_query($conn, $query)
{
	try{
		$result = $conn->prepare($query);
		$result->execute();
		return true;
	}
	catch(PDOException $e)
	{
		print_r($e->getMessage());
	}
}
?>