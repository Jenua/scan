<?php
$path = $_SERVER['DOCUMENT_ROOT'];
require_once($path.'/db_connect/connect.php');
date_default_timezone_set('America/New_York');

	if (isset($_REQUEST['po']) && !empty($_REQUEST['po']) && isset($_REQUEST['userName']))
	{
		$po = $_REQUEST['po'];
		$userName = $_REQUEST['userName'];
		$conn = Database::getInstance()->dbc;
		$query = "INSERT INTO 
	[dbo].[Orders_regenerated]
SELECT
    distinct
	'".$po."' as [POnumber]
	,GETDATE() as [RegenDate]
	,'".$userName."' as [userName]
	,CASE
		WHEN [ia].[Date] IS NOT NULL THEN 'negative'
		WHEN [ia_fd].[Date] IS NOT NULL THEN 'positive'
		ELSE 'not_calculated'
	END as [inventory_status]
FROM [dbo].[ShipTrackWithLabel] [stwl]
LEFT JOIN [dbo].[inventory_allocation] [ia] ON [stwl].[PONUMBER] = [ia].[PONumber] AND [stwl].[ID] = [ia].[RefNumber]
LEFT JOIN [dbo].[inventory_allocation_full_details] [ia_fd] ON [ia_fd].[PONumber] = [stwl].[PONumber] AND [ia_fd].[RefNumber] = [stwl].[ID]
WHERE [stwl].[PONUMBER] = '".$po."'";
		//print_r($query);
		$res1 = exec_query($conn, $query);
		
		
		if ($res1)
		{
			echo "Order with po: '".$po."' logged as generated";
		} else echo "Error. Can not log order as generated. po: '".$po."'";
		$conn = null;
	}


function getConnection()
{
	try{
		$conn = new PDO("sqlsrv:server = ".SERVER."; Database = ".DATABASE, USER, PASSWORD);
		$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); }
		catch(PDOException $e)
		{
			die($e->getMessage());
		}
	return $conn;
}

function exec_query($conn, $query)
{
	try{
		$result = $conn->prepare($query);
		$result->execute();
		return true;
	}
	catch(PDOException $e)
	{
		print_r($e->getMessage());
	}
}
?>