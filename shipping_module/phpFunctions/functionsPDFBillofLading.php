<?php
//Functions to generate PDF depending on store name (look function name)
define("globalCarrierFont", 28);

function get_packing_list($arr, $pdf)
{
	//print_r($arr);
	//Packing List
	if (extrStore($arr[0]['CustomerRef_FullName']) == 'Ferguson' && !is_in_str($arr[0]['RefNumber'],'combine_'))
	{
				$pdf->AddPage('P', array(210, 350), false, false);
				$pdf->SetFont('helvetica', '', 8);
				$last_id = $arr[0]['TxnLineID'];
				$tbl = '<table border="0" width="670" cellpadding="2" align="center">
						<tr>
							<td colspan="1"><img id="img1" src="../../Include/img/logo.png"></td>
							<td colspan="1"><font size="20"><br><strong>Packing List</strong></font></td>
							<td colspan="1"><font size="12"><br><br><strong>PO: '.$arr[0]['PONumber'].'</strong></font></td>
						</tr>
						</table><br>_______________________________________________________________________________________________________________________';
				foreach($arr as $key => $row)
				{
					if (empty($row['quantity'])) $row['quantity'] = '1';
					if ($last_id != $row['TxnLineID'] || $key == 0)
					{	
						$tbl.= '
						<br>
						<br>
						<br>
						<table nobr="true" border="2" width="670" cellpadding="2" align="center">
						<tr>
							<td colspan="2"><strong>Product SKU</strong></td>
							<td colspan="6"><strong>Description</strong></td>
							<td colspan="1"><strong>QTY</strong></td>
							<td colspan="1"><strong>Packed</strong></td>
						</tr>
						<tr>
							<td colspan="2"><strong>'.$row['ItemGroupRef_FullName'].'</strong></td>
							<td colspan="6"><strong>'.$row['Prod_desc'].'</strong></td>
							<td colspan="1"><strong>'.$row['quantity'].'</strong></td>
							<td colspan="1"><img id="img1" src="../Include/pictures/checkboxfalse.png" width="12" height="12"></td>
						</tr>
						</table>
						<br>
						<br>
						<table width="670">
						<tr>
							<td width="135"></td>
							<td>
								<table align="center" nobr="true" border="1" width="535" cellpadding="2">
								<tr>
									<td colspan="8">Item Name</td>
									<td colspan="1">QTY</td>
									<td colspan="1">Packed</td>
								</tr>
								<tr>
									<td colspan="8">'.$row['ItemRef_FullName'].'</td>
									<td colspan="1">'.$row['quantity']/$row['item_quantity'].'</td>
									<td colspan="1"><img id="img1" src="../Include/pictures/checkboxfalse.png" width="12" height="12"></td>
								</tr>
								</table>
							</td>
						</tr>
						</table><br>
						';
					} else{
						$tbl.= '
						<table width="670">
						<tr>
							<td width="135"></td>
							<td>
								<table align="center" nobr="true" border="1" width="535" cellpadding="2">
								<tr>
									<td colspan="8">'.$row['ItemRef_FullName'].'</td>
									<td colspan="1">'.$row['quantity']/$row['item_quantity'].'</td>
									<td colspan="1"><img id="img1" src="../Include/pictures/checkboxfalse.png" width="12" height="12"></td>
								</tr>
								</table>
							</td>
						</tr>
						</table><br>
						';
					}
					$last_id = $row['TxnLineID'];
				}
				$pdf->writeHTML($tbl, true, false, false, false, '');
	}else if (is_in_str($arr[0]['RefNumber'],'combine_'))
	{
				$pdf->AddPage('P', array(210, 350), false, false);
				$pdf->SetFont('helvetica', '', 8);
				$last_id = $arr[0]['TxnLineID'];
				$tbl = '<table border="0" width="670" cellpadding="2" align="center">
						<tr>
							<td colspan="1"><img id="img1" src="../../Include/img/logo.png"></td>
							<td colspan="1"><font size="20"><br><strong>Packing List</strong></font></td>
							<td colspan="1"><font size="12"><br><br><strong>PO: '.$arr[0]['PONumber'].'</strong></font></td>
						</tr>
						</table><br>_______________________________________________________________________________________________________________________';
				foreach($arr as $key => $row)
				{
					if (empty($row['quantity'])) $row['quantity'] = '1';
					if ($last_id != $row['TxnLineID'] || $key == 0)
					{	
						$tbl.= '
						<p>PO: '.$row['original_po'].'</p>
						<br>
						<br>
						<br>
						<table nobr="true" border="2" width="670" cellpadding="2" align="center">
						<tr>
							<td colspan="2"><strong>Product SKU</strong></td>
							<td colspan="6"><strong>Description</strong></td>
							<td colspan="1"><strong>QTY</strong></td>
							<td colspan="1"><strong>Packed</strong></td>
						</tr>
						<tr>
							<td colspan="2"><strong>'.$row['ItemGroupRef_FullName'].'</strong></td>
							<td colspan="6"><strong>'.$row['Prod_desc'].'</strong></td>
							<td colspan="1"><strong>'.$row['quantity'].'</strong></td>
							<td colspan="1"><img id="img1" src="../Include/pictures/checkboxfalse.png" width="12" height="12"></td>
						</tr>
						</table>
						<br>
						<br>
						<table width="670">
						<tr>
							<td width="135"></td>
							<td>
								<table align="center" nobr="true" border="1" width="535" cellpadding="2">
								<tr>
									<td colspan="8">Item Name</td>
									<td colspan="1">QTY</td>
									<td colspan="1">Packed</td>
								</tr>
								<tr>
									<td colspan="8">'.$row['ItemRef_FullName'].'</td>
									<td colspan="1">'.$row['quantity']/$row['item_quantity'].'</td>
									<td colspan="1"><img id="img1" src="../Include/pictures/checkboxfalse.png" width="12" height="12"></td>
								</tr>
								</table>
							</td>
						</tr>
						</table><br>
						';
					} else{
						$tbl.= '
						<table width="670">
						<tr>
							<td width="135"></td>
							<td>
								<table align="center" nobr="true" border="1" width="535" cellpadding="2">
								<tr>
									<td colspan="8">'.$row['ItemRef_FullName'].'</td>
									<td colspan="1">'.$row['quantity']/$row['item_quantity'].'</td>
									<td colspan="1"><img id="img1" src="../Include/pictures/checkboxfalse.png" width="12" height="12"></td>
								</tr>
								</table>
							</td>
						</tr>
						</table><br>
						';
					}
					$last_id = $row['TxnLineID'];
				}
				$pdf->writeHTML($tbl, true, false, false, false, '');
	}
	return $pdf;
}

function descr_reduction($ProductCode)
{
	$result = $ProductCode;
	$ProductCode = strtolower($ProductCode);
	if (strpos($ProductCode, 'dlt') !== false) {
		$result = 'Shower Base';
	} else if (strpos($ProductCode, 'shbw') !== false) {
		$result = 'Shower Wall';
	} else if (strpos($ProductCode, 'shst') !== false || strpos($ProductCode, 'shcm') !== false) {
		$result = 'Shower Accessories';
	} else{
		$result = 'Shower Door';
	}
	return $result;
}

function get_description($arr, $numberOfRows)
{
	$description = array();
	$descriptionHtml = '';
	foreach ($arr as $arr0)
	{
		if (!in_array($arr0['Descr'],$description))
		{
			$description[] = $arr0['Descr'];
			$descriptionHtml.=$arr0['Descr'].'<br>';
		}
	}
	if (count($description) > $numberOfRows)
	{
		$description = array();
		$descriptionHtml = '';
		foreach ($arr as $arr0)
		{
			$Descr = descr_reduction($arr0['ProductCode']);
			if (!in_array($Descr,$description))
			{
				$description[] = $Descr;
				$descriptionHtml.=$Descr.'<br>';
			}
		}
		return $descriptionHtml;
		
	} else{
		return $descriptionHtml;
	}
	//return 'Shower base<br>Shower wall<br>Shower accessories<br>Shower door<br>';
}

function getAmazonSticker($storeName, $po, $descriptionHtml, $memo)
			{
				if (IsPlaceAmazonSticker == 'True')
				{
					if ($storeName == 'Amazon')
					{
						$getAmazonDate = extrAmazonDate($memo);
						$pleaseLogin = '
						<table border="1" cellpadding="3">
							<tr>
								<td>
									<b>PLEASE LOGIN TO YOUR ACCOUNT AT<br>
									<a href="http://transportation.amazon.com/">http://transportation.amazon.com/</a><br>
									TO SCHEDULE THE DELIVERY<br>
									WITH AMAZON CENTER<br>
									'.$getAmazonDate.'</b>
								</td>
							</tr>
						</table>
						';
						$descriptionHtml.= $pleaseLogin;
					}
				}
				return $descriptionHtml;
			}

//Generate Bill of Lading pdf files
function homedepotBill($arr, $pro, $bol, $pro10, $barcodeBill, $originCarrierName)
{
	try{
			
			$barcodeBill = $barcodeBill.'.png';
			
			$PONumbers = array();
			$result = array();
			
			foreach ($arr as $row)
			{
				$PONumbers[] = $row['PONumber'];
			}
			$c = count($PONumbers);
			
				$pdf_path = "../PdfBill/".repPo($PONumbers[0])."Bill.pdf";
				$pdf_path2 = "PdfBill/".repPo($PONumbers[0])."Bill.pdf";
				
				date_default_timezone_set('America/New_York');
				
				$todaysDate = date("n/j/Y");
				$todaysYear = date("Y");
				
				$shipToAddress = '';
				if (isset($arr[0]['ShipAddress_Addr1']) && !empty($row['ShipAddress_Addr1'])) $shipToAddress.=$row['ShipAddress_Addr1'].'<br>';
				if (isset($arr[0]['ShipAddress_Addr2']) && !empty($row['ShipAddress_Addr2'])) $shipToAddress.=$row['ShipAddress_Addr2'].'<br>';
				if (isset($arr[0]['ShipAddress_Addr3']) && !empty($row['ShipAddress_Addr3'])) $shipToAddress.=$row['ShipAddress_Addr3'].'<br>';
				if (isset($arr[0]['ShipAddress_Addr4']) && !empty($row['ShipAddress_Addr4'])) $shipToAddress.=$row['ShipAddress_Addr4'].'<br>';
				if (isset($arr[0]['ShipAddress_Addr5']) && !empty($row['ShipAddress_Addr5'])) $shipToAddress.=$row['ShipAddress_Addr5'].'<br>';
				if (isset($arr[0]['ShipAddress_City']) && !empty($row['ShipAddress_City'])) $shipToAddress.=$row['ShipAddress_City'].', ';
				if (isset($arr[0]['ShipAddress_State']) && !empty($row['ShipAddress_State'])) $shipToAddress.=$row['ShipAddress_State'].'<br>';
				if (isset($arr[0]['ShipAddress_PostalCode']) && !empty($row['ShipAddress_PostalCode'])) $shipToAddress.=$row['ShipAddress_PostalCode'].'<br>';
				if (isset($arr[0]['FOB']) && !empty($row['FOB'])) $shipToAddress.=$row['FOB'].'<br>';
				
				
				if (!isset($arr[0]['PALLETS']) || empty($arr[0]['PALLETS']))
				{
					$units = ceil($arr[0]['WEIGHT'] / 2500);
				} else
				{
					$units = $arr[0]['PALLETS'];
				}
				
				$descriptionHtml = get_description($arr, 4);
				
				if ($arr[0]['RESIDENTIAL'] == '1') $residential = '../Include/pictures/checkboxtrue.png'; else $residential = '../Include/pictures/checkboxfalse.png';
				if ($arr[0]['LIFTGATE'] == '1') $liftgate = '../Include/pictures/checkboxtrue.png'; else $liftgate = '../Include/pictures/checkboxfalse.png';
				if ($arr[0]['delivery_notification'] == '1') $delivery_notification = '../Include/pictures/checkboxtrue.png'; else $delivery_notification = '../Include/pictures/checkboxfalse.png';

				
				$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
				$pdf->SetMargins(11, 11);
				$pdf->SetAutoPageBreak(TRUE, 1);
				$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);


						
						$pdf->AddPage();
						$pdf->SetFont('helvetica', '', 9);
						$tbl = '<table border="1" width="670" height="1000" cellpadding="3">
								<tr>
									<td colspan="3">
										<table border="1" cellpadding="3">
										<tr>
											<td align="center">
												<b>Delivery Problem?<br>
												Contact Home Depot:<br>
												800-430-3376</b>
											</td>
										</tr>
										<tr>
											<td bgcolor="black" align="center">
												<FONT size="9" COLOR="white"><b>SHIP FROM</b></FONT>
											</td>
										</tr>
										<tr>
											<td>
												<b>Bath Authority, LLC<br>
												75 Hawk Road<br>
												Warminster, PA 18974<br>
												866-731-8378</b>
											</td>
										</tr>
										<tr>
											<td bgcolor="black" align="center">
												<FONT size="9" COLOR="white"><b>SHIP TO</b></FONT>
											</td>
										</tr>
										<tr>
											<td>
												<b>'.$shipToAddress.'</b>
											</td>
										</tr>
										<tr>
											<td bgcolor="black" align="center">
												<FONT size="9" COLOR="white"><b>BILL TO</b></FONT>
											</td>
										</tr>
										<tr>
											<td>
												<b>HomeDepot.Com<br>
												Attn: Freight Payables<br>
												2455 Paces Ferry Road<br>
												Atlanta, GA 30339</b>
											</td>
										</tr>
										</table>
									</td>
									<td colspan="3">
										<table border="1" cellpadding="3">
										<tr>
											<td colspan="2" align="right">
												<font size="16"><b>BILL OF LADING</b></font>
											</td>
										</tr>
										<tr>
											<td colspan="1">
												Date
											</td>
											<td colspan="1">
												'.$todaysDate.'
											</td>
										</tr>
										<tr>
											<td colspan="1">
												Bill of Lading #
											</td>
											<td colspan="1">
												<b>'.$pro.'</b>
											</td>
										</tr>
										<tr>
											<td colspan="1">
												Internal Reference #
											</td>
											<td colspan="1">
												'.$bol.'
											</td>
										</tr>
										<tr>
											<td colspan="1">
												Home Depot PO #
											</td>
											<td colspan="1">
												'.checkPOBill($arr[0]['PONumber']).'
											</td>
										</tr>
										<tr>
											<td colspan="1">
												Home Depot Store #
											</td>
											<td colspan="1">
												8119
											</td>
										</tr>
										<tr>';
										
										if ($barcodeBill == 'none' || $pro10 == 'none' || ($originCarrierName == "Amazon" && PrintCarrierForAmazonMode == '1')) $tbl.='
											<td colspan="2" align="center">
												<br>
												<br>
												<br>
												<br>
												<br>
												<br>
												<font color="silver" size="20">Place sticker here</font>
												<br>
												<br>
												<br>
												<br>
												<br>
											</td>'; else 
											$tbl.='
											<td colspan="2" align="center" height="160">
												<p><b><font size="32">'.$pro10.'</font></b></p>
												<img src="'.$barcodeBill.'" with="281" height="50">
												<br>
												<br>
												<br>
												<br>
											</td>';
											
											
										$tbl.='</tr>
										<tr>
											<td colspan="2">
												<b>Freight Terms (FOB Destination)</b><br>
												<label for="img1">  Prepaid</label>
												<img id="img1" src="../Include/pictures/checkboxfalse.png" width="12" height="12">
												<label for="img2">  Collect</label>
												<img id="img2" src="../Include/pictures/checkboxfalse.png" width="12" height="12">
												<label for="img3">  3rd Party</label>
												<img id="img3" src="../Include/pictures/checkboxtrue.png" width="12" height="12">
											</td>
										</tr>
										</table>
									</td>
								</tr> 
								<tr>
									<td colspan="6">
										<b>ADDED SERVICES   </b>
										<font size="9"><img id="img1" src="'.$liftgate.'" width="12" height="12">
										<label for="img1">Liftgate  </label>
										<img id="img2" src="../Include/pictures/checkboxfalse.png" width="12" height="12">
										<label for="img2">Inside Delivery  </label>
										<img id="img3" src="'.$residential.'" width="12" height="12">
										<label for="img3">Residential/Limited Access  </label>
										<img id="img4" src="'.$delivery_notification.'" width="12" height="12">
										<label for="img4">Delivery Notification  </label></font>
									</td>
								</tr>
								<tr>
									<td colspan="6">
									<table border="1" cellpadding="3">
									<tr>
										<td colspan="1" width="55">
											No. of Units
										</td>
										<td colspan="1" width="55">
											Type
										</td>
										<td colspan="1" width="55">
											No. of Boxes
										</td>
										<td colspan="4" width="315">
											Description
										</td>
										<td colspan="1" width="55">
											Weight
										</td>
										<td colspan="1" width="55">
											Class
										</td>
										<td colspan="1" width="75">
											NMFC
										</td>
									</tr>
									<tr>
										<td colspan="1" width="55">
											'.$units.'
										</td>
										<td colspan="1" width="55">
											SKID
										</td>
										<td colspan="1" width="55">
											'.$arr[0]['boxes'].'
										</td>
										<td colspan="4" width="315">
											'.$descriptionHtml.'
										</td>
										<td colspan="1" width="55">
											'.$arr[0]['WEIGHT'].'
										</td>
										<td colspan="1" width="55">
											85
										</td>
										<td colspan="1" width="75">
											159010-02
										</td>
									</tr>
									</table>
									</td>
								</tr>
								<tr>
									<td colspan="6" align="center">
										<font size="16"><b>Residential and Liftgate charges pre- approved</b></font>
									</td>
								</tr>
								<tr>
									<td colspan="6" align="left">
										<font size="9">SPECIAL INSTRUCTIONS :  If no delivery appointment has been made with the consignee within 1 business day of receiving the shipment, please contact Dreamline/Bath Authority at 866-731-2244 or email support@dreamline.com to assist with alternate contact information.
										</font>
									</td>
								</tr>
								<tr>
									<td colspan="6">
										<font size="9">Where the rate is dependent on value, shippers are required to state specifically in writing the agreed or declared value of the
										property as follows:
										“The agreed or declared value of the property is specifically stated by the shipper to be not exceeding ______per______</font>
									</td>
								</tr>
								<tr>
									<td colspan="6">
										<font size="9">NOTE Liability limitation for loss or damage on this shipment shall be applicable. See 49 US. C. - 14706(c)(1)(A) and (B)</font>
									</td>
								</tr>
								<tr>
									<td colspan="6">
										<font size="9">RECEIVED, subject to individually determined rates or contracts that have been agreed upon in writing between the carrier and shipper, if applicable,
										otherwise to the rates, classifications and rules that have been established by the carrier and are available to the shipper, on request, and to all
										applicable state and federal regulations.</font>
									</td>
								</tr>
								<tr>
									<td colspan="3">
										<b>SHIPPER SIGNATURE / DATE</b>
									</td>
									<td colspan="3">
										<b>CARRIER SIGNATURE / PICKUP DATE</b>
									</td>
								</tr>
								<tr>
									<td colspan="3">
										<font size="9">This is to certify that the above named materials are properly classified,
										described, packaged, marked and labeled, and
										are in proper condition for transportation according to the applicable
										regulations of the DOT<br><br>
										_________________________Shipper ________/'.$todaysYear.' Date</font>
									</td>
									<td colspan="3">
										<font size="9">Carrier acknowledges receipt of packages and required placards. 
										Carrier certifies emergency response information was made available
										and/or carrier has the U.S. DOT emergency response guidebook or
										equivalent documentation in the vehicle.<br>
										_____________________ Driver _______/'.$todaysYear.' Date</font>
									</td>
								</tr>
								</table>';
								if (isCarrierNameCorrect($originCarrierName))
								{
									/*if ($originCarrierName == "Amazon" && PrintCarrierForAmazonMode == '1')
									{
										$tblLogo = $tbl;
									} else
									{*/
										$carrierLogo = getCarrierLogo($originCarrierName);
										$tblLogo = $tbl.'<table cellpadding="3">
											<tr>
												<td>
													<font size="'.globalCarrierFont.'"><b>'.$originCarrierName.'</b></font><br>
													
												</td>
												<td>
													<img src="'.$carrierLogo.'" height="130px" width="340px">
												</td>
											</tr>
										</table>';
									/*}*/
								} else 
								{
									$tblLogo = $tbl;
								}
								$pdf->writeHTML($tblLogo, true, false, false, false, '');
								
								$pdf->AddPage();
								$pdf->SetFont('helvetica', '', 9);
								if (isCarrierNameCorrect($originCarrierName))
								{
									/*if ($originCarrierName == "Amazon" && PrintCarrierForAmazonMode == '1')
									{
										$tbl = $tbl;
									} else
									{*/
										$carrierLogo = getCarrierLogo($originCarrierName);
										$tbl.='<table cellpadding="3">
											<tr>
												<td>
													<font size="'.globalCarrierFont.'"><b>'.$originCarrierName.'</b></font>
													<font size="'.globalCarrierFont.'">Carrier Copy</font>
												</td>
												<td>
													<img src="'.$carrierLogo.'" height="130px" width="340px">
												</td>
											</tr>
										</table>';
									/*}*/
								}
								
								$pdf->writeHTML($tbl, true, false, false, false, '');

				$pdf = get_packing_list($arr, $pdf);				
				$pdf->Output($pdf_path, 'F');
				$result[] = $pdf_path2;
			if (!empty($result))
			{
				return $result;
			} else
			{
				error_msg("Can not generate bill of lading!");
				send_error();
			}
	}catch (Exception $e) {
		error_msg($e->getMessage());
		send_error();
	}
}

//Generate Bill of Lading pdf files
function yowBill($arr, $pro, $bol, $pro10, $barcodeBill, $originCarrierName)
{
	try{
			
			$barcodeBill = $barcodeBill.'.png';
			
			$PONumbers = array();
			$result = array();
			
			foreach ($arr as $row)
			{
				$PONumbers[] = $row['PONumber'];
			}
			$c = count($PONumbers);
			
				$pdf_path = "../PdfBill/".repPo($PONumbers[0])."Bill.pdf";
				$pdf_path2 = "PdfBill/".repPo($PONumbers[0])."Bill.pdf";
				
				date_default_timezone_set('America/New_York');
				
				$todaysDate = date("n/j/Y");
				$todaysYear = date("Y");
				
				$shipToAddress = '';
				if (isset($arr[0]['ShipAddress_Addr1']) && !empty($row['ShipAddress_Addr1'])) $shipToAddress.=$row['ShipAddress_Addr1'].'<br>';
				if (isset($arr[0]['ShipAddress_Addr2']) && !empty($row['ShipAddress_Addr2'])) $shipToAddress.=$row['ShipAddress_Addr2'].'<br>';
				if (isset($arr[0]['ShipAddress_Addr3']) && !empty($row['ShipAddress_Addr3'])) $shipToAddress.=$row['ShipAddress_Addr3'].'<br>';
				if (isset($arr[0]['ShipAddress_Addr4']) && !empty($row['ShipAddress_Addr4'])) $shipToAddress.=$row['ShipAddress_Addr4'].'<br>';
				if (isset($arr[0]['ShipAddress_Addr5']) && !empty($row['ShipAddress_Addr5'])) $shipToAddress.=$row['ShipAddress_Addr5'].'<br>';
				if (isset($arr[0]['ShipAddress_City']) && !empty($row['ShipAddress_City'])) $shipToAddress.=$row['ShipAddress_City'].', ';
				if (isset($arr[0]['ShipAddress_State']) && !empty($row['ShipAddress_State'])) $shipToAddress.=$row['ShipAddress_State'].'<br>';
				if (isset($arr[0]['ShipAddress_PostalCode']) && !empty($row['ShipAddress_PostalCode'])) $shipToAddress.=$row['ShipAddress_PostalCode'].'<br>';
				if (isset($arr[0]['FOB']) && !empty($row['FOB'])) $shipToAddress.=$row['FOB'].'<br>';
				
				
				if (!isset($arr[0]['PALLETS']) || empty($arr[0]['PALLETS']))
				{
					$units = ceil($arr[0]['WEIGHT'] / 2500);
				} else
				{
					$units = $arr[0]['PALLETS'];
				}
				
				$descriptionHtml = get_description($arr, 4);
				
				if ($arr[0]['RESIDENTIAL'] == '1') $residential = '../Include/pictures/checkboxtrue.png'; else $residential = '../Include/pictures/checkboxfalse.png';
				if ($arr[0]['LIFTGATE'] == '1') $liftgate = '../Include/pictures/checkboxtrue.png'; else $liftgate = '../Include/pictures/checkboxfalse.png';
				if ($arr[0]['delivery_notification'] == '1') $delivery_notification = '../Include/pictures/checkboxtrue.png'; else $delivery_notification = '../Include/pictures/checkboxfalse.png';
				
				$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
				$pdf->SetMargins(11, 11);
				$pdf->SetAutoPageBreak(TRUE, 1);
				$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);


						
						$pdf->AddPage();
						$pdf->SetFont('helvetica', '', 9);
						$tbl = '<table border="1" width="670" height="1000" cellpadding="3">
								<tr>
									<td colspan="3">
										<table border="1" cellpadding="3">
										<tr>
											<td align="center">
												<b>Delivery Problem?<br>
												Your "other" Warehouse<br>
												800-947-7000</b>
											</td>
										</tr>
										<tr>
											<td bgcolor="black" align="center">
												<FONT size="9" COLOR="white"><b>SHIP FROM</b></FONT>
											</td>
										</tr>
										<tr>
											<td>
												<b>Bath Authority, LLC<br>
												75 Hawk Road<br>
												Warminster, PA 18974<br>
												866-731-8378</b>
											</td>
										</tr>
										<tr>
											<td bgcolor="black" align="center">
												<FONT size="9" COLOR="white"><b>SHIP TO</b></FONT>
											</td>
										</tr>
										<tr>
											<td>
												<b>'.$shipToAddress.'</b>
											</td>
										</tr>
										<tr>
											<td bgcolor="black" align="center">
												<FONT size="9" COLOR="white"><b>BILL TO</b></FONT>
											</td>
										</tr>
										<tr>
											<td>
												<b>YOW_MerchandisePayables<br>
												PO Box 105843<br>
												Atlanta, Georgia 30348</b>
											</td>
										</tr>
										</table>
									</td>
									<td colspan="3">
										<table border="1" cellpadding="3">
										<tr>
											<td colspan="2" align="right">
												<font size="16"><b>BILL OF LADING</b></font>
											</td>
										</tr>
										<tr>
											<td colspan="1">
												Date
											</td>
											<td colspan="1">
												'.$todaysDate.'
											</td>
										</tr>
										<tr>
											<td colspan="1">
												Bill of Lading #
											</td>
											<td colspan="1">
												<b>'.$pro.'</b>
											</td>
										</tr>
										<tr>
											<td colspan="1">
												Internal Reference #
											</td>
											<td colspan="1">
												'.$bol.'
											</td>
										</tr>
										<tr>
											<td colspan="1">
												Your "other" Warehouse PO #
											</td>
											<td colspan="1">
												'.checkPOBill($arr[0]['PONumber']).'
											</td>
										</tr>
										<tr>
											<td colspan="1">
												Home Depot Store #
											</td>
											<td colspan="1">
												8119
											</td>
										</tr>
										<tr>';
											
											if ($barcodeBill == 'none' || $pro10 == 'none' || ($arr[0]['CustomerRef_FullName'] == "Amazon" && PrintCarrierForAmazonMode == '1')) $tbl.='
											<td colspan="2" align="center">
												<br>
												<br>
												<br>
												<br>
												<br>
												<font color="silver" size="20">Place sticker here</font>
												<br>
												<br>
												<br>
												<br>
											</td>'; else 
											$tbl.='
											<td colspan="2" align="center" height="160">
												<p><b><font size="32">'.$pro10.'</font></b></p>
												<img src="'.$barcodeBill.'" with="281" height="50">
												<br>
												<br>
												<br>
												<br>
											</td>';
											
										$tbl.='</tr>
										<tr>
											<td colspan="2">
												<b>Freight Terms (FOB Destination)</b><br>
												<label for="img1">  Prepaid</label>
												<img id="img1" src="../Include/pictures/checkboxfalse.png" width="12" height="12">
												<label for="img2">  Collect</label>
												<img id="img2" src="../Include/pictures/checkboxfalse.png" width="12" height="12">
												<label for="img3">  3rd Party</label>
												<img id="img3" src="../Include/pictures/checkboxtrue.png" width="12" height="12">
											</td>
										</tr>
										</table>
									</td>
								</tr> 
								<tr>
									<td colspan="6">
										<b>ADDED SERVICES   </b>
										<font size="9"><img id="img1" src="'.$liftgate.'" width="12" height="12">
										<label for="img1">Liftgate  </label>
										<img id="img2" src="../Include/pictures/checkboxfalse.png" width="12" height="12">
										<label for="img2">Inside Delivery  </label>
										<img id="img3" src="'.$residential.'" width="12" height="12">
										<label for="img3">Residential/Limited Access  </label>
										<img id="img4" src="'.$delivery_notification.'" width="12" height="12">
										<label for="img4">Delivery Notification  </label></font>
									</td>
								</tr>
								<tr>
									<td colspan="6">
									<table border="1" cellpadding="3">
									<tr>
										<td colspan="1" width="70">
											No. of Units
										</td>
										<td colspan="1" width="70">
											Type
										</td>
										<td colspan="1" width="70">
											No. of Boxes
										</td>
										<td colspan="4" width="315">
											Description
										</td>
										<td colspan="1" width="70">
											Weight
										</td>
										<td colspan="1" width="70">
											Class
										</td>
									</tr>
									<tr>
										<td colspan="1" width="70">
											'.$units.'
										</td>
										<td colspan="1" width="70">
											SKID
										</td>
										<td colspan="1" width="70">
											'.$arr[0]['boxes'].'
										</td>
										<td colspan="4" width="315">
											'.$descriptionHtml.'
										</td>
										<td colspan="1" width="70">
											'.$arr[0]['WEIGHT'].'
										</td>
										<td colspan="1" width="70">
											85
										</td>
									</tr>
									</table>
									</td>
								</tr>
								<tr>
									<td colspan="6" align="center">
										<font size="16"><b>Residential and Liftgate charges pre- approved</b></font>
									</td>
								</tr>
								<tr>
									<td colspan="6" align="left">
										<font size="9">SPECIAL INSTRUCTIONS :  If no delivery appointment has been made with the consignee within 1 business day of receiving the shipment, please contact Dreamline/Bath Authority at 866-731-2244 or email support@dreamline.com to assist with alternate contact information.
										</font>
									</td>
								</tr>
								<tr>
									<td colspan="6">
										<font size="9">Where the rate is dependent on value, shippers are required to state specifically in writing the agreed or declared value of the
										property as follows:
										“The agreed or declared value of the property is specifically stated by the shipper to be not exceeding ______per______</font>
									</td>
								</tr>
								<tr>
									<td colspan="6">
										<font size="9">NOTE Liability limitation for loss or damage on this shipment shall be applicable. See 49 US. C. - 14706(c)(1)(A) and (B)</font>
									</td>
								</tr>
								<tr>
									<td colspan="6">
										<font size="9">RECEIVED, subject to individually determined rates or contracts that have been agreed upon in writing between the carrier and shipper, if applicable,
										otherwise to the rates, classifications and rules that have been established by the carrier and are available to the shipper, on request, and to all
										applicable state and federal regulations.</font>
									</td>
								</tr>
								<tr>
									<td colspan="3">
										<b>SHIPPER SIGNATURE / DATE</b>
									</td>
									<td colspan="3">
										<b>CARRIER SIGNATURE / PICKUP DATE</b>
									</td>
								</tr>
								<tr>
									<td colspan="3">
										<font size="9">This is to certify that the above named materials are properly classified,
										described, packaged, marked and labeled, and
										are in proper condition for transportation according to the applicable
										regulations of the DOT<br><br>
										_________________________Shipper ________/'.$todaysYear.' Date</font>
									</td>
									<td colspan="3">
										<font size="9">Carrier acknowledges receipt of packages and required placards. 
										Carrier certifies emergency response information was made available
										and/or carrier has the U.S. DOT emergency response guidebook or
										equivalent documentation in the vehicle.<br>
										_____________________ Driver _______/'.$todaysYear.' Date</font>
									</td>
								</tr>
								</table>';
								if (isCarrierNameCorrect($originCarrierName))
								{
									/*if ($arr[0]['CustomerRef_FullName'] == "Amazon" && PrintCarrierForAmazonMode == '1')
									{
										$tblLogo = $tbl;
									} else
									{*/
										$carrierLogo = getCarrierLogo($originCarrierName);
										$tblLogo = $tbl.'<table cellpadding="3">
											<tr>
												<td>
													<font size="'.globalCarrierFont.'"><b>'.$originCarrierName.'</b></font><br>
													
												</td>
												<td>
													<img src="'.$carrierLogo.'" height="130px" width="340px">
												</td>
											</tr>
										</table>';
									/*}*/
								} else 
								{
									$tblLogo = $tbl;
								}
								$pdf->writeHTML($tblLogo, true, false, false, false, '');
								
								$pdf->AddPage();
								$pdf->SetFont('helvetica', '', 9);
								if (isCarrierNameCorrect($originCarrierName))
								{
									/*if ($arr[0]['CustomerRef_FullName'] == "Amazon" && PrintCarrierForAmazonMode == '1')
									{
										$tbl = $tbl;
									} else
									{*/
										$carrierLogo = getCarrierLogo($originCarrierName);
										$tbl.='<table cellpadding="3">
											<tr>
												<td>
													<font size="'.globalCarrierFont.'"><b>'.$originCarrierName.'</b></font>
													<font size="'.globalCarrierFont.'">Carrier Copy</font>
												</td>
												<td>
													<img src="'.$carrierLogo.'" height="130px" width="340px">
												</td>
											</tr>
										</table>';
									/*}*/
								}
								
								$pdf->writeHTML($tbl, true, false, false, false, '');

				$pdf = get_packing_list($arr, $pdf);				
				$pdf->Output($pdf_path, 'F');
				$result[] = $pdf_path2;
			if (!empty($result))
			{
				return $result;
			} else
			{
				error_msg("Can not generate bill of lading!");
				send_error();
			}
	}catch (Exception $e) {
		error_msg($e->getMessage());
		send_error();
	}
}

//Generate Bill of Lading pdf files
function fedexBill($arr, $pro, $bol, $pro10, $barcodeBill, $originCarrierName)
{
	try{
			
			$barcodeBill = $barcodeBill.'.png';
			
			$PONumbers = array();
			$result = array();
			
			foreach ($arr as $row)
			{
				$PONumbers[] = $row['PONumber'];
			}
			$c = count($PONumbers);
			
				$pdf_path = "../PdfBill/".repPo($PONumbers[0])."Bill.pdf";
				$pdf_path2 = "PdfBill/".repPo($PONumbers[0])."Bill.pdf";
				
				date_default_timezone_set('America/New_York');
				
				$todaysDate = date("n/j/Y");
				$todaysYear = date("Y");
				
				$address = '';
				$consignee = '';
				if (isset($arr[0]['ShipAddress_Addr1']) && !empty($row['ShipAddress_Addr1'])) $consignee = $row['ShipAddress_Addr1']; else $consignee = '';
				
				if (extrStore($arr[0]['ShipAddress_Addr1']) != "Lowe's" && extrStore($arr[0]['CustomerRef_FullName']) == "Lowe's" && $arr[0]['RESIDENTIAL'] == '0')
				{
					$consignee.='<br>C/O '.$arr[0]['CustomerRef_FullName'];
					$consigneeTitle = 'Consignee:<br><br>';
				} else $consigneeTitle = 'Consignee:<br>';
				
				if (isset($arr[0]['ShipAddress_Addr2']) && !empty($row['ShipAddress_Addr2'])) $address.=$row['ShipAddress_Addr2'].', ';
				if (isset($arr[0]['ShipAddress_Addr3']) && !empty($row['ShipAddress_Addr3'])) $address.=$row['ShipAddress_Addr3'].', ';
				if (isset($arr[0]['ShipAddress_Addr4']) && !empty($row['ShipAddress_Addr4'])) $address.=$row['ShipAddress_Addr4'].', ';
				if (isset($arr[0]['ShipAddress_Addr5']) && !empty($row['ShipAddress_Addr5'])) $address.=$row['ShipAddress_Addr5'];
				
				if (isset($arr[0]['FOB']) && !empty($row['FOB'])) $shipperPhone=$row['FOB']; else $shipperPhone='';
				
				$cityStateZip = '';
				if (isset($arr[0]['ShipAddress_City']) && !empty($row['ShipAddress_City'])) $cityStateZip.=$row['ShipAddress_City'].', ';
				if (isset($arr[0]['ShipAddress_State']) && !empty($row['ShipAddress_State'])) $cityStateZip.=$row['ShipAddress_State'].', ';
				if (isset($arr[0]['ShipAddress_PostalCode']) && !empty($row['ShipAddress_PostalCode'])) $cityStateZip.=$row['ShipAddress_PostalCode'];
				
										
				
				if ($arr[0]['CARRIERNAME'] == "FEDEX-ECONOMY")
				{
					$economy = 'checkboxtrue.png';
					$priority = 'checkboxfalse.png';
				} else {
					$economy = 'checkboxfalse.png';
					$priority = 'checkboxtrue.png';
				}
				
				
				
				if (!isset($arr[0]['PALLETS']) || empty($arr[0]['PALLETS']))
				{
					$units = ceil($arr[0]['WEIGHT'] / 2500);
				} else
				{
					$units = $arr[0]['PALLETS'];
				}
				
				$descriptionHtml = get_description($arr, 4);
				
				//$arr[0]['CustomerRef_FullName'] = 'Ferguson';
				
				$fergAcc = '______________________';
				if (extrStore($arr[0]['CustomerRef_FullName']) == 'Ferguson')
				{
					$fergAcc = fergusonAccount($arr[0]['CARRIERNAME']);
					if ($fergAcc) $fergAccTitle = 'Account #:'; else $fergAcc = '______________________';
				}
				
				$descriptionHtml = getAmazonSticker($arr[0]['CustomerRef_FullName'], $arr[0]['PONumber'], $descriptionHtml, $arr[0]['Memo']);
					if ($arr[0]['CustomerRef_FullName'] != 'Amazon')
					{					
						$consigneeAllInfo = '<tr>
											<td colspan="3">
												'.$consigneeTitle.'
												Address:<br>
												City/State/Zip:<br>
												Phone#:<br>
											</td>
											<td colspan="3">
												<b>'.$consignee.'<br>
												'.$address.'<br>
												'.$cityStateZip.'<br>
												'.$shipperPhone.'<br></b>
											</td>
										</tr>';
					} else
					{
						$consigneeAllInfo = '<tr>
											<td colspan="3">
												'.$consigneeTitle.'
												Address:<br>
												City/State/Zip:<br>
											</td>
											<td colspan="3">
												<b>'.$consignee.'<br>
												'.$address.'<br>
												'.$cityStateZip.'<br></b>
											</td>
										</tr>';
					}
					if (IsPlaceAmazonSticker == 'True')
					{
						if ($arr[0]['CustomerRef_FullName'] == 'Amazon')
						{
							$redeliveryCharges = '';
						} else 
						{
							$redeliveryCharges = '<tr>
									<td colspan="6">
										<font size="10"><b>Redelivery charges require authorization, all accessorial charges. 
Please contact support team at 866-731-8378 ext.193 or ext.134</b></font>
									</td>
								</tr>';
						}
					} else 
					{
							$redeliveryCharges = '<tr>
									<td colspan="6">
										<font size="10"><b>Redelivery charges require authorization, all accessorial charges. 
Please contact support team at 866-731-8378 ext.193 or ext.134</b></font>
									</td>
								</tr>';
					}
				
				if ($arr[0]['RESIDENTIAL'] == '1') $residential = '../Include/pictures/checkboxtrue.png'; else $residential = '../Include/pictures/checkboxfalse.png';
				if ($arr[0]['LIFTGATE'] == '1') $liftgate = '../Include/pictures/checkboxtrue.png'; else $liftgate = '../Include/pictures/checkboxfalse.png';
				if ($arr[0]['delivery_notification'] == '1') $delivery_notification = '../Include/pictures/checkboxtrue.png'; else $delivery_notification = '../Include/pictures/checkboxfalse.png';
				
				$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
				$pdf->SetMargins(11, 11);
				$pdf->SetAutoPageBreak(TRUE, 1);
				$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);


						
						$pdf->AddPage();
						$pdf->SetFont('helvetica', '', 8);
						$tbl = '<table border="1" width="670" height="1000" cellpadding="3">
								<tr>
									<td colspan="6">
										<font size="12"><b>BILL OF LADING    </b></font>
										Delivery Problem? Contact Bath Authority: support@dreamline.com or 866-731-2244
									</td>
								</tr>
								<tr>';
						if ($barcodeBill == 'none' || $pro10 == 'none' || ($arr[0]['CustomerRef_FullName'] == "Amazon" && PrintCarrierForAmazonMode == '1'))
						{
							$tbl.='<td colspan="4" align="center">
										<br>
												<br>
												<br>
												<br>
												<br>
												<font color="silver" size="20">Place sticker here</font>
												<br>
												<br>
												<br>
												<br>
									</td>';
						} else
						{
							$tbl.='<td colspan="4" align="center">
										<p><b><font size="32">'.$pro10.'</font></b></p>
										<img src="'.$barcodeBill.'" with="281" height="50">
										<br>
										<br>
										<br>
										<br>
									</td>';
						}
									
								$tbl.='	<td colspan="2">
										<table border="1" cellpadding="3">
										<tr>
											<td border="2">
												<font size="7"><b>REQUIRED: Please Select a service type<br>
												<img id="img1" src="../Include/pictures/'.$priority.'" width="12" height="12">
												<label for="img1">FedEx Freight® Priority</label><br>
												<img id="img2" src="../Include/pictures/'.$economy.'" width="12" height="12">
												<label for="img2">FedEx Freight® Economy</label></b></font>
											</td>
										</tr>
										<tr>
											<td>
												<font size="8"><b><label for="img3">Freight charges are PREPAID
												unless marked collect.<br>
												Check box if collect </label>
												<img id="img3" src="../Include/pictures/checkboxfalse.png" width="12" height="12"></b></font>
											</td>
										</tr>
										</table>
										<table border="1" cellpadding="3">
										<tr>
											<td colspan="1">
												Date
											</td>
											<td colspan="1">
												<b>'.$todaysDate.'</b>
											</td>
										</tr>
										<tr>
											<td colspan="1">
												Shipper BOL#
											</td>
											<td colspan="1">
												<b>'.$bol.'</b>
											</td>
										</tr>
										<tr>
											<td colspan="1">
												Carrier Pro#
											</td>
											<td colspan="1">
												'.$pro.'
											</td>
										</tr>
										<tr>
											<td colspan="1">
												Purchase Order #
											</td>
											<td colspan="1">
												'.checkPOBill($arr[0]['PONumber']).'
											</td>
										</tr>
										</table>
									</td>
								</tr>

								<tr>
									<td colspan="3">
										<table border="1" cellpadding="3">
										<tr>
											<td bgcolor="silver" align="center" colspan="6">
												<FONT size="8" COLOR="black"><b>SHIPPER (from)</b></FONT>
											</td>
										</tr>
										<tr>
											<td colspan="3">
												FXF Account#<br>
												Shipper:<br>
												Address:<br>
												City/State/Zip:<br>
												Phone#:
											</td>
											<td colspan="3">
												<b>483857888<br>
												Bath Authority<br>
												75 Hawk Road<br>
												Warminster, PA 18974<br>
												866-731-8378</b>
											</td>
										</tr>
										</table>
									</td>
									<td colspan="3">
										<table border="1" cellpadding="3">
										<tr>
											<td bgcolor="silver" align="center" colspan="6">
												<FONT size="8" COLOR="black"><b>CONSIGNEE (to)</b></FONT>
											</td>
										</tr>
										'.$consigneeAllInfo.'
										</table>
									</td>
								</tr>
								<tr>
									<td bgcolor="silver" colspan="6">
										<b>BILL FREIGHT CHARGES TO (if different):</b>
									</td>
								</tr>
								<tr>
									<td colspan="6">
										<font size="8">Name__________________________________________Address__________________________FXF Acct#:'.$fergAcc.'
City____________________________State_____________Zip________________________Phone#__________________________________</font>
									</td>
								</tr>
								<tr>
									<td colspan="6">
										<font size="8">RECEIVED, subject to individually determined rates or contracts that have been agreed upon in writing between the carrier and shipper, if applicable, otherwise to the rates,
classifications and rules that have been established by the carrier and are available to the shipper, on request, and to all applicable state and federal regulations.</font>
									</td>
								</tr>
								<tr>
									<td colspan="1" align="center">
										<b>ADDED SERVICES</b><br>
										<font size="7" color="grey">(May Require Additional Charges)</font>
									</td>
									<td colspan="5">
										<font size="8">  <img id="img1" src="'.$liftgate.'" width="12" height="12">
										<label for="img1">Liftgate  </label>
										<img id="img2" src="../Include/pictures/checkboxfalse.png" width="12" height="12">
										<label for="img2">Inside Delivery  </label>
										<img id="img3" src="'.$residential.'" width="12" height="12">
										<label for="img3">Residential Delivery  </label>
										<img id="img4" src="../Include/pictures/checkboxfalse.png" width="12" height="12">
										<label for="img4">Custom Delivery Window  </label></font>
									</td>
								</tr>
								<tr>
									<td colspan="6">
									<table border="1" cellpadding="3">
									<tr>
										<td colspan="1" width="50" align="center">
											Units
										</td>
										<td colspan="1" width="50" align="center">
											Type
										</td>
										<td colspan="1" width="50" align="center">
											Boxes
										</td>
										<td colspan="1" width="50" align="center">
											HM*
										</td>
										<td colspan="4" width="285" align="center">
											Description
										</td>
										<td colspan="1" width="50" align="center">
											Weight
										</td>
										<td colspan="1" width="80" align="center">
											NMFC Item #
										</td>
										<td colspan="1" width="50" align="center">
											Class
										</td>
									</tr>
									<tr>
										<td colspan="1" width="50">
											'.$units.'
										</td>
										<td colspan="1" width="50">
											SKID
										</td>
										<td colspan="1" width="50">
											'.$arr[0]['boxes'].'
										</td>
										<td colspan="1" width="50">
											
										</td>
										<td colspan="4" width="285">
											'.$descriptionHtml.'
										</td>
										<td colspan="1" width="50">
											'.$arr[0]['WEIGHT'].'
										</td>
										<td colspan="1" width="80">
											159010-02
										</td>
										<td colspan="1" width="50">
											85
										</td>
									</tr>
									</table>
									</td>
								</tr>
								'.$redeliveryCharges.'
								<tr>
									<td colspan="6" align="left">
										<font size="8">SPECIAL INSTRUCTIONS :  If no delivery appointment has been made with the consignee within 1 business day of receiving the shipment, please contact Dreamline/Bath Authority at 866-731-2244 or email support@dreamline.com to assist with alternate contact information.
										</font>
									</td>
								</tr>
								<tr>
									<td colspan="6">
										<font size="8">* MARK “X” or “RQ” in the HM column to designate hazardous materials or reportable quantity as defined in DOT regulations.
SPECIAL INSTRUCTIONS: Call for delivery! Notify Consignee. Do not Double stack pallets. No additional accessorial authorized by shipper</font>
									</td>
								</tr>
								<tr>
									<td colspan="6">
										<font size="8">NOTE (I) Where the rate is dependent on value, shippers are required to state specifically in writing the agreed or declared value of the property as
follows:
“The agreed or declared value of the property is specifically stated by the shipper to be not exceeding ______per______
NOTE(II) Liability limitation for loss or damage on this shipment shall be applicable. See 49 US. C. - 14706(c)(1)(A) and (B)
NOTE(III) commodities requiring special or additional care or attention in handling or stowing must be so
marked and packaged as to ensure safe transportation with ordinary care. see sec. 2(e) of nMfc item 360</font>
									</td>
								</tr>
								<tr>
									<td colspan="6">
										<font size="8">Subject to Section 7 of conditions of applicable Bill of Lading. if this shipment is to be delivered to the consignee,
without recourse on the consignor, the consignor shall sign the following statement. The carrier may decline to
make delivery of this shipment without payment of freight and all other lawful charges.
consignor signature _______________________________________________________________________</font>
									</td>
								</tr>
								<tr>
									<td bgcolor="silver" colspan="3">
										<b>SHIPPER SIGNATURE / DATE</b>
									</td>
									<td bgcolor="silver" colspan="3">
										<b>CARRIER SIGNATURE / PICKUP DATE</b>
									</td>
								</tr>
								<tr>
									<td colspan="3">
										<font size="8">This is to certify that the above named materials are properly classified,
described, packaged, marked and labeled, and
are in proper condition for transportation according to the applicable
regulations of the DOT
<br><br>_________________________Shipper ________/'.$todaysYear.' Date</font>
									</td>
									<td colspan="3">
										<font size="8">Carrier acknowledges receipt of packages and required placards. 
Carrier certifies emergency response information was made available
and/or carrier has the U.S. DOT emergency response guidebook or
equivalent documentation in the vehicle.
<br><br>_____________________ Driver _______/'.$todaysYear.' Date</font>
									</td>
								</tr>
								</table>';
								if (isCarrierNameCorrect($originCarrierName))
								{
									/*if ($arr[0]['CustomerRef_FullName'] == "Amazon" && PrintCarrierForAmazonMode == '1')
									{
										$tblLogo = $tbl;
									} else
									{*/
										$carrierLogo = getCarrierLogo($originCarrierName);
										$tblLogo = $tbl.'<table cellpadding="3">
											<tr>
												<td>
													<font size="'.globalCarrierFont.'"><b>'.$originCarrierName.'</b></font><br>
													
												</td>
												<td>
													<img src="'.$carrierLogo.'" height="120px" width="340px">
												</td>
											</tr>
										</table>';
									/*}*/
								} else 
								{
									$tblLogo = $tbl;
								}
								$pdf->writeHTML($tblLogo, true, false, false, false, '');
								
								$pdf->AddPage();
								$pdf->SetFont('helvetica', '', 8);
								if (isCarrierNameCorrect($originCarrierName))
								{
									/*if ($arr[0]['CustomerRef_FullName'] == "Amazon" && PrintCarrierForAmazonMode == '1')
									{
										$tbl = $tbl;
									} else
									{*/
										$carrierLogo = getCarrierLogo($originCarrierName);
										$tbl.='<table cellpadding="3">
											<tr>
												<td>
													<font size="'.globalCarrierFont.'"><b>'.$originCarrierName.'</b></font>
													<font size="'.globalCarrierFont.'">Carrier Copy</font>
												</td>
												<td>
													<img src="'.$carrierLogo.'" height="120px" width="340px">
												</td>
											</tr>
										</table>';
									/*}*/
								}
								
								$pdf->writeHTML($tbl, true, false, false, false, '');

				$pdf = get_packing_list($arr, $pdf);				
				$pdf->Output($pdf_path, 'F');
				$result[] = $pdf_path2;
			if (!empty($result))
			{
				return $result;
			} else
			{
				error_msg("Can not generate bill of lading!");
				send_error();
			}
	}catch (Exception $e) {
		error_msg($e->getMessage());
		send_error();
	}
}

//Generate Bill of Lading pdf files
function buildcomBill($arr, $pro, $bol, $originCarrierName)
{
	try{
			$PONumbers = array();
			$result = array();
			
			foreach ($arr as $row)
			{
				$PONumbers[] = $row['PONumber'];
			}
			$c = count($PONumbers);
			
				$pdf_path = "../PdfBill/".repPo($PONumbers[0])."Bill.pdf";
				$pdf_path2 = "PdfBill/".repPo($PONumbers[0])."Bill.pdf";
				
				date_default_timezone_set('America/New_York');
				
				$todaysDate = date("n/j/Y");
				$todaysYear = date("Y");
				
				$address = '';
				$consignee = '';
				if (isset($arr[0]['ShipAddress_Addr1']) && !empty($row['ShipAddress_Addr1'])) $consignee = $row['ShipAddress_Addr1']; else $consignee = '';
				
				if (isset($arr[0]['ShipAddress_Addr2']) && !empty($row['ShipAddress_Addr2'])) $address.=$row['ShipAddress_Addr2'].', ';
				if (isset($arr[0]['ShipAddress_Addr3']) && !empty($row['ShipAddress_Addr3'])) $address.=$row['ShipAddress_Addr3'].', ';
				if (isset($arr[0]['ShipAddress_Addr4']) && !empty($row['ShipAddress_Addr4'])) $address.=$row['ShipAddress_Addr4'].', ';
				if (isset($arr[0]['ShipAddress_Addr5']) && !empty($row['ShipAddress_Addr5'])) $address.=$row['ShipAddress_Addr5'];
				
				if (isset($arr[0]['FOB']) && !empty($row['FOB'])) $shipperPhone=$row['FOB']; else $shipperPhone='';
				
				$cityStateZip = '';
				if (isset($arr[0]['ShipAddress_City']) && !empty($row['ShipAddress_City'])) $cityStateZip.=$row['ShipAddress_City'].', ';
				if (isset($arr[0]['ShipAddress_State']) && !empty($row['ShipAddress_State'])) $cityStateZip.=$row['ShipAddress_State'].', ';
				if (isset($arr[0]['ShipAddress_PostalCode']) && !empty($row['ShipAddress_PostalCode'])) $cityStateZip.=$row['ShipAddress_PostalCode'];
				
				if (!isset($arr[0]['PALLETS']) || empty($arr[0]['PALLETS']))
				{
					$units = ceil($arr[0]['WEIGHT'] / 2500);
				} else
				{
					$units = $arr[0]['PALLETS'];
				}
				
				$descriptionHtml = get_description($arr, 4);
				
				$descriptionHtml = getAmazonSticker($arr[0]['CustomerRef_FullName'], $arr[0]['PONumber'], $descriptionHtml, $arr[0]['Memo']);
				if ($arr[0]['CustomerRef_FullName'] != 'Amazon')
					{					
						$consigneeAllInfo = '<tr>
											<td colspan="3">
												Consignee:<br>
												Address:<br>
												City/State/Zip:<br>
												Phone#:<br>
											</td>
											<td colspan="3">
												<b>'.$consignee.'<br>
												'.$address.'<br>
												'.$cityStateZip.'<br>
												'.$shipperPhone.'</b>
											</td>
										</tr>';
					} else
					{
						$consigneeAllInfo = '<tr>
											<td colspan="3">
												Consignee:<br>
												Address:<br>
												City/State/Zip:<br>
											</td>
											<td colspan="3">
												<b>'.$consignee.'<br>
												'.$address.'<br>
												'.$cityStateZip.'<br></b>
											</td>
										</tr>';
					}
				
				if ($arr[0]['RESIDENTIAL'] == '1') $residential = '../Include/pictures/checkboxtrue.png'; else $residential = '../Include/pictures/checkboxfalse.png';
				if ($arr[0]['LIFTGATE'] == '1') $liftgate = '../Include/pictures/checkboxtrue.png'; else $liftgate = '../Include/pictures/checkboxfalse.png';
				if ($arr[0]['delivery_notification'] == '1') $delivery_notification = '../Include/pictures/checkboxtrue.png'; else $delivery_notification = '../Include/pictures/checkboxfalse.png';
					
				$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
				$pdf->SetMargins(11, 11);
				$pdf->SetAutoPageBreak(TRUE, 1);
				$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);


						
						$pdf->AddPage();
						$pdf->SetFont('helvetica', '', 9);
						$tbl = '<table border="1" width="670" height="1000" cellpadding="3">
								<tr>
									<td colspan="6">
										<font size="12"><b>BILL OF LADING    </b></font>
										Delivery Problem? Contact Bath Authority: support@dreamline.com or 866-731-2244
									</td>
								</tr>
								<tr>
									<td colspan="3" align="center">
										<br>
										<br>
										<br>
										<br>
										<br>
										<br>
										<font color="silver" size="8">Attach Sticker here</font>
										<br>
										<br>
										<br>
										<br>
										<br>
									</td>
									<td colspan="3">
										<table border="1" cellpadding="3">
										<tr>
											<td colspan="1">
												Date
											</td>
											<td colspan="1">
												<b>'.$todaysDate.'</b>
											</td>
										</tr>
										<tr>
											<td colspan="1">
												Shipper BOL#
											</td>
											<td colspan="1">
												<b>'.$bol.'</b>
											</td>
										</tr>
										<tr>
											<td colspan="1">
												Carrier Pro#
											</td>
											<td colspan="1">
												'.$pro.'
											</td>
										</tr>
										<tr>
											<td colspan="1">
												Purchase Order #
											</td>
											<td colspan="1">
												'.checkPOBill($arr[0]['PONumber']).'<br>
											</td>
										</tr>
										</table>
									</td>
								</tr>

								<tr>
									<td colspan="3">
										<table border="1" cellpadding="3">
										<tr>
											<td bgcolor="silver" align="center" colspan="6">
												<FONT size="8" COLOR="black"><b>SHIPPER (from)</b></FONT>
											</td>
										</tr>
										<tr>
											<td colspan="3">
												Shipper:<br>
												Address:<br>
												City/State/Zip:<br>
												Phone#:
											</td>
											<td colspan="3">
												<b>Bath Authority<br>
												75 Hawk Road<br>
												Warminster, PA 18974<br>
												866-731-8378</b>
											</td>
										</tr>
										</table>
									</td>
									<td colspan="3">
										<table border="1" cellpadding="3">
										<tr>
											<td bgcolor="silver" align="center" colspan="6">
												<FONT size="8" COLOR="black"><b>CONSIGNEE (to)</b></FONT>
											</td>
										</tr>
										'.$consigneeAllInfo.'
										</table>
									</td>
								</tr>
								<tr>
									<td colspan="6">
										<font size="8"><b><label for="img1">Freight charges are PREPAID unless marked collect.Check box if collect  </label>
										<img id="img1" src="../Include/pictures/checkboxfalse.png" width="12" height="12"></b></font>
									</td>
								</tr>
								<tr>
									<td bgcolor="silver" colspan="6">
										<b>BILL FREIGHT CHARGES TO (if different):</b>
									</td>
								</tr>
								<tr>
									<td colspan="6">
										<font size="8">Name_Build.com/2573_______________________Address_12500_Jefferson_Ave_________________City_Newport_News__________________State_VA____________Zi
p_23602_____________________Phone#_916-373-3275_____________</font>
									</td>
								</tr>
								<tr>
									<td colspan="6">
										<font size="8">RECEIVED, subject to individually determined rates or contracts that have been agreed upon in writing between the carrier and shipper, if applicable, otherwise to the rates,
classifications and rules that have been established by the carrier and are available to the shipper, on request, and to all applicable state and federal regulations.</font>
									</td>
								</tr>
								<tr>
									<td colspan="1" align="center">
										<b>ADDED SERVICES</b><br>
									</td>
									<td colspan="5">
										<font size="8">  <img id="img1" src="'.$liftgate.'" width="12" height="12">
										<label for="img1">Liftgate  </label>
										<img id="img2" src="../Include/pictures/checkboxfalse.png" width="12" height="12">
										<label for="img2">Inside Delivery  </label>
										<img id="img3" src="'.$residential.'" width="12" height="12">
										<label for="img3">Residential  </label>
										<img id="img4" src="'.$delivery_notification.'" width="12" height="12">
										<label for="img4">Delivery Notification  </label>
										<img id="img4" src="../Include/pictures/checkboxfalse.png" width="12" height="12">
										<label for="img4">Custom Delivery Window  </label></font>
									</td>
								</tr>
								<tr>
									<td colspan="6">
									<table border="1" cellpadding="3">
									<tr>
										<td colspan="1" width="50" align="center">
											Units
										</td>
										<td colspan="1" width="50" align="center">
											Type
										</td>
										<td colspan="1" width="50" align="center">
											Boxes
										</td>
										<td colspan="1" width="50" align="center">
											HM*
										</td>
										<td colspan="4" width="285" align="center">
											Description
										</td>
										<td colspan="1" width="50" align="center">
											Weight
										</td>
										<td colspan="1" width="80" align="center">
											NMFC Item #
										</td>
										<td colspan="1" width="50" align="center">
											Class
										</td>
									</tr>
									<tr>
										<td colspan="1" width="50">
											'.$units.'
										</td>
										<td colspan="1" width="50">
											SKID
										</td>
										<td colspan="1" width="50">
											'.$arr[0]['boxes'].'
										</td>
										<td colspan="1" width="50">
											
										</td>
										<td colspan="4" width="285">
											'.$descriptionHtml.'
										</td>
										<td colspan="1" width="50">
											'.$arr[0]['WEIGHT'].'
										</td>
										<td colspan="1" width="80">
											159010-02
										</td>
										<td colspan="1" width="50">
											85
										</td>
									</tr>
									</table>
									</td>
								</tr>
								<tr>
									<td colspan="6">
										<font size="8">* MARK “X” or “RQ” in the HM column to designate hazardous materials or reportable quantity as defined in DOT regulations.</font>
									</td>
								</tr>
								<tr>
									<td colspan="6">
										<font size="8"><b>SPECIAL INSTRUCTIONS: Call for delivery! Notify Consignee. Do not Double stack pallets. No additional accessorial
authorized by shipper</b></font>
									</td>
								</tr>
								<tr>
									<td colspan="6" align="left">
										<font size="9">SPECIAL INSTRUCTIONS :  If no delivery appointment has been made with the consignee within 1 business day of receiving the shipment, please contact Dreamline/Bath Authority at 866-731-2244 or email support@dreamline.com to assist with alternate contact information.
										</font>
									</td>
								</tr>
								<tr>
									<td colspan="6">
										<font size="8">NOTE (I) Where the rate is dependent on value, shippers are required to state specifically in writing the agreed or declared value of the property as
follows:
“The agreed or declared value of the property is specifically stated by the shipper to be not exceeding ______per______
NOTE(II) Liability limitation for loss or damage on this shipment shall be applicable. See 49 US. C. - 14706(c)(1)(A) and (B)
NOTE(III) commodities requiring special or additional care or attention in handling or stowing must be so
marked and packaged as to ensure safe transportation with ordinary care. see sec. 2(e) of nMfc item 360</font>
									</td>
								</tr>
								<tr>
									<td colspan="6">
										<font size="8">Subject to Section 7 of conditions of applicable Bill of Lading. if this shipment is to be delivered to the consignee,
without recourse on the consignor, the consignor shall sign the following statement. The carrier may decline to
make delivery of this shipment without payment of freight and all other lawful charges.
consignor signature _______________________________________________________________________</font>
									</td>
								</tr>
								<tr>
									<td bgcolor="silver" colspan="3">
										<b>SHIPPER SIGNATURE / DATE</b>
									</td>
									<td bgcolor="silver" colspan="3">
										<b>CARRIER SIGNATURE / PICKUP DATE</b>
									</td>
								</tr>
								<tr>
									<td colspan="3">
										<font size="8">This is to certify that the above named materials are properly classified,
described, packaged, marked and labeled, and
are in proper condition for transportation according to the applicable
regulations of the DOT
<br><br>_________________________Shipper ________/'.$todaysYear.' Date</font>
									</td>
									<td colspan="3">
										<font size="8">Carrier acknowledges receipt of packages and required placards. 
Carrier certifies emergency response information was made available
and/or carrier has the U.S. DOT emergency response guidebook or
equivalent documentation in the vehicle.
<br><br>_____________________ Driver _______/'.$todaysYear.' Date</font>
									</td>
								</tr>
								</table>';
								if (isCarrierNameCorrect($originCarrierName))
								{
									$carrierLogo = getCarrierLogo($originCarrierName);
									$tblLogo = $tbl.'<table cellpadding="3">
										<tr>
											<td>
												<font size="'.globalCarrierFont.'"><b>'.$originCarrierName.'</b></font><br>
												
											</td>
											<td>
												<img src="'.$carrierLogo.'" height="130px" width="340px">
											</td>
										</tr>
									</table>';
								} else 
								{
									$tblLogo = $tbl;
								}
								$pdf->writeHTML($tblLogo, true, false, false, false, '');
								
								$pdf->AddPage();
								$pdf->SetFont('helvetica', '', 9);
								if (isCarrierNameCorrect($originCarrierName))
								{
									$carrierLogo = getCarrierLogo($originCarrierName);
									$tbl.='<table cellpadding="3">
										<tr>
											<td>
												<font size="'.globalCarrierFont.'"><b>'.$originCarrierName.'</b></font>
												<font size="'.globalCarrierFont.'">Carrier Copy</font>
											</td>
											<td>
												<img src="'.$carrierLogo.'" height="130px" width="340px">
											</td>
										</tr>
									</table>';
								}
								
								$pdf->writeHTML($tbl, true, false, false, false, '');

				$pdf = get_packing_list($arr, $pdf);				
				$pdf->Output($pdf_path, 'F');
				$result[] = $pdf_path2;
			if (!empty($result))
			{
				return $result;
			} else
			{
				error_msg("Can not generate bill of lading!");
				send_error();
			}
	}catch (Exception $e) {
		error_msg($e->getMessage());
		send_error();
	}
}

//Generate Bill of Lading pdf files
function genericBill($arr, $pro, $bol, $pro10, $barcodeBill, $originCarrierName)
{
	try{
			
			$barcodeBill = $barcodeBill.'.png';
			
			$PONumbers = array();
			$result = array();
			
			foreach ($arr as $row)
			{
				$PONumbers[] = $row['PONumber'];
			}
			$c = count($PONumbers);
			
				$pdf_path = "../PdfBill/".repPo($PONumbers[0])."Bill.pdf";
				$pdf_path2 = "PdfBill/".repPo($PONumbers[0])."Bill.pdf";
				
				date_default_timezone_set('America/New_York');
				
				$todaysDate = date("n/j/Y");
				$todaysYear = date("Y");
				
				$address = '';
				$consignee = '';
				if (isset($arr[0]['ShipAddress_Addr1']) && !empty($row['ShipAddress_Addr1'])) $consignee = $row['ShipAddress_Addr1']; else $consignee = '';
				
				if (extrStore($arr[0]['ShipAddress_Addr1']) != "Lowe's" && extrStore($arr[0]['CustomerRef_FullName']) == "Lowe's" && $arr[0]['RESIDENTIAL'] == '0')
				{
					$consignee.='<br>C/O '.$arr[0]['CustomerRef_FullName'];
					$consigneeTitle = 'Consignee:<br><br>';
				} else $consigneeTitle = 'Consignee:<br>';
				
				if (isset($arr[0]['ShipAddress_Addr2']) && !empty($row['ShipAddress_Addr2'])) $address.=$row['ShipAddress_Addr2'].', ';
				if (isset($arr[0]['ShipAddress_Addr3']) && !empty($row['ShipAddress_Addr3'])) $address.=$row['ShipAddress_Addr3'].', ';
				if (isset($arr[0]['ShipAddress_Addr4']) && !empty($row['ShipAddress_Addr4'])) $address.=$row['ShipAddress_Addr4'].', ';
				if (isset($arr[0]['ShipAddress_Addr5']) && !empty($row['ShipAddress_Addr5'])) $address.=$row['ShipAddress_Addr5'];
				
				if (isset($arr[0]['FOB']) && !empty($row['FOB'])) $shipperPhone=$row['FOB']; else $shipperPhone='';
				
				$cityStateZip = '';
				if (isset($arr[0]['ShipAddress_City']) && !empty($row['ShipAddress_City'])) $cityStateZip.=$row['ShipAddress_City'].', ';
				if (isset($arr[0]['ShipAddress_State']) && !empty($row['ShipAddress_State'])) $cityStateZip.=$row['ShipAddress_State'].', ';
				if (isset($arr[0]['ShipAddress_PostalCode']) && !empty($row['ShipAddress_PostalCode'])) $cityStateZip.=$row['ShipAddress_PostalCode'];
				
				if (!isset($arr[0]['PALLETS']) || empty($arr[0]['PALLETS']))
				{
					$units = ceil($arr[0]['WEIGHT'] / 2500);
				} else
				{
					$units = $arr[0]['PALLETS'];
				}
				
				$fergAcc = '___________';
				if (extrStore($arr[0]['CustomerRef_FullName']) == 'Ferguson')
				{
					$fergAcc = fergusonAccount($arr[0]['CARRIERNAME']);
					if ($fergAcc) $fergAccTitle = 'Account #:'; else $fergAcc = '___________';
				}
				
				$descriptionHtml = get_description($arr, 4);
				
				$descriptionHtml = getAmazonSticker($arr[0]['CustomerRef_FullName'], $arr[0]['PONumber'], $descriptionHtml, $arr[0]['Memo']);
				if ($arr[0]['CustomerRef_FullName'] != 'Amazon')
					{					
						$consigneeAllInfo = '<tr>
											<td colspan="3">
												'.$consigneeTitle.'
												Address:<br>
												City/State/Zip:<br>
												Phone#:<br>
											</td>
											<td colspan="3">
												<b>'.$consignee.'<br>
												'.$address.'<br>
												'.$cityStateZip.'<br>
												'.$shipperPhone.'</b>
											</td>
										</tr>';
					} else
					{
						$consigneeAllInfo = '<tr>
											<td colspan="3">
												'.$consigneeTitle.'
												Address:<br>
												City/State/Zip:<br>
											</td>
											<td colspan="3">
												<b>'.$consignee.'<br>
												'.$address.'<br>
												'.$cityStateZip.'<br></b>
											</td>
										</tr>';
					}
				if ($arr[0]['RESIDENTIAL'] == '1') $residential = '../Include/pictures/checkboxtrue.png'; else $residential = '../Include/pictures/checkboxfalse.png';
				if ($arr[0]['LIFTGATE'] == '1') $liftgate = '../Include/pictures/checkboxtrue.png'; else $liftgate = '../Include/pictures/checkboxfalse.png';
				if ($arr[0]['delivery_notification'] == '1') $delivery_notification = '../Include/pictures/checkboxtrue.png'; else $delivery_notification = '../Include/pictures/checkboxfalse.png';
				//$residential = '../Include/pictures/checkboxfalse.png';
				//$liftgate = '../Include/pictures/checkboxfalse.png';
				
				$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
				$pdf->SetMargins(11, 11);
				$pdf->SetAutoPageBreak(TRUE, 1);
				$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);


						
						$pdf->AddPage();
						$pdf->SetFont('helvetica', '', 9);
						$tbl = '<table border="1" width="670" height="1000" cellpadding="3">
								<tr>
									<td colspan="6">
										<font size="12"><b>BILL OF LADING    </b></font>
										Delivery Problem? Contact Bath Authority: support@dreamline.com or 866-731-2244
									</td>
								</tr>
								<tr>';
						if ($barcodeBill == 'none' || $pro10 == 'none' || ($arr[0]['CustomerRef_FullName'] == "Amazon" && PrintCarrierForAmazonMode == '1'))
						{
							$tbl.='<td colspan="3" align="center">
												<br>
												<br>
												<br>
												<br>
												<br>
												<font color="silver" size="20">Place sticker here</font>
												<br>
												<br>
												<br>
												<br>
											</td>';
						} else
						{
							$tbl.='<td colspan="3" align="center">
										<p><b><font size="32">'.$pro10.'</font></b></p>
										<img src="'.$barcodeBill.'" with="281" height="50">
										<br>
										<br>
										<br>
										<br>
									</td>';
						}
									
								$tbl.='	<td colspan="3">
										<table border="1" cellpadding="3">
										<tr>
											<td colspan="1">
												Date
											</td>
											<td colspan="1">
												<b>'.$todaysDate.'</b>
											</td>
										</tr>
										<tr>
											<td colspan="1">
												Shipper BOL#
											</td>
											<td colspan="1">
												<b>'.$bol.'</b>
											</td>
										</tr>
										<tr>
											<td colspan="1">
												Carrier Pro#
											</td>
											<td colspan="1">
												'.$pro.'
											</td>
										</tr>
										<tr>
											<td colspan="1">
												Purchase Order #
											</td>
											<td colspan="1">
												'.checkPOBill($arr[0]['PONumber']).'<br>
											</td>
										</tr>
										</table>
										<br>
										<br>
										<br>
										<br>
									</td>
								</tr>

								<tr>
									<td colspan="3">
										<table border="1" cellpadding="3">
										<tr>
											<td bgcolor="silver" align="center" colspan="6">
												<FONT size="8" COLOR="black"><b>SHIPPER (from)</b></FONT>
											</td>
										</tr>
										<tr>
											<td colspan="3">
												Shipper:<br>
												Address:<br>
												City/State/Zip:<br>
												Phone#:
											</td>
											<td colspan="3">
												<b>Bath Authority<br>
												75 Hawk Road<br>
												Warminster, PA 18974<br>
												866-731-8378</b>
											</td>
										</tr>
										</table>
									</td>
									<td colspan="3">
										<table border="1" cellpadding="3">
										<tr>
											<td bgcolor="silver" align="center" colspan="6">
												<FONT size="8" COLOR="black"><b>CONSIGNEE (to)</b></FONT>
											</td>
										</tr>
										'.$consigneeAllInfo.'
										</table>
									</td>
								</tr>
								<tr>
									<td colspan="6">
										<font size="8"><b><label for="img1">Freight charges are PREPAID unless marked collect.Check box if collect  </label>
										<img id="img1" src="../Include/pictures/checkboxfalse.png" width="12" height="12"></b></font>
									</td>
								</tr>
								<tr>
									<td bgcolor="silver" colspan="6">
										<b>BILL FREIGHT CHARGES TO (if different):</b>
									</td>
								</tr>
								<tr>
									<td colspan="6">
										<font size="8">Name___________________________________Address____________________________Acct#'.$fergAcc.'
City____________________________State_____________Zip________________________Phone#_________________</font>
									</td>
								</tr>
								<tr>
									<td colspan="6">
										<font size="8">RECEIVED, subject to individually determined rates or contracts that have been agreed upon in writing between the carrier and shipper, if applicable, otherwise to the rates,
classifications and rules that have been established by the carrier and are available to the shipper, on request, and to all applicable state and federal regulations.</font>
									</td>
								</tr>
								<tr>
									<td colspan="1" align="center">
										<b>ADDED SERVICES</b><br>
									</td>
									<td colspan="5">
										<font size="8">  <img id="img1" src="'.$liftgate.'" width="12" height="12">
										<label for="img1">Liftgate  </label>
										<img id="img2" src="../Include/pictures/checkboxfalse.png" width="12" height="12">
										<label for="img2">Inside Delivery  </label>
										<img id="img3" src="'.$residential.'" width="12" height="12">
										<label for="img3">Residential  </label>
										<img id="img4" src="'.$delivery_notification.'" width="12" height="12">
										<label for="img4">Delivery Notification  </label>
										<img id="img4" src="../Include/pictures/checkboxfalse.png" width="12" height="12">
										<label for="img4">Custom Delivery Window  </label></font>
									</td>
								</tr>
								<tr>
									<td colspan="6">
									<table border="1" cellpadding="3">
									<tr>
										<td colspan="1" width="50" align="center">
											Units
										</td>
										<td colspan="1" width="50" align="center">
											Type
										</td>
										<td colspan="1" width="50" align="center">
											Boxes
										</td>
										<td colspan="1" width="50" align="center">
											HM*
										</td>
										<td colspan="4" width="285" align="center">
											Description
										</td>
										<td colspan="1" width="50" align="center">
											Weight
										</td>
										<td colspan="1" width="80" align="center">
											NMFC Item #
										</td>
										<td colspan="1" width="50" align="center">
											Class
										</td>
									</tr>
									<tr>
										<td colspan="1" width="50">
											'.$units.'
										</td>
										<td colspan="1" width="50">
											SKID
										</td>
										<td colspan="1" width="50">
											'.$arr[0]['boxes'].'
										</td>
										<td colspan="1" width="50">
											
										</td>
										<td colspan="4" width="285">
											'.$descriptionHtml.'
										</td>
										<td colspan="1" width="50">
											'.$arr[0]['WEIGHT'].'
										</td>
										<td colspan="1" width="80">
											159010-02
										</td>
										<td colspan="1" width="50">
											85
										</td>
									</tr>
									</table>
									</td>
								</tr>
								<tr>
									<td colspan="6">
										<font size="8">* MARK “X” or “RQ” in the HM column to designate hazardous materials or reportable quantity as defined in DOT regulations.</font>
									</td>
								</tr>
								<tr>
									<td colspan="6">
										<font size="8"><b>SPECIAL INSTRUCTIONS: Call for delivery! Notify Consignee. Do not Double stack pallets. No additional accessorial
authorized by shipper</b></font>
									</td>
								</tr>
								<tr>
									<td colspan="6" align="left">
										<font size="9">SPECIAL INSTRUCTIONS :  If no delivery appointment has been made with the consignee within 1 business day of receiving the shipment, please contact Dreamline/Bath Authority at 866-731-2244 or email support@dreamline.com to assist with alternate contact information.
										</font>
									</td>
								</tr>
								<tr>
									<td colspan="6">
										<font size="8">NOTE (I) Where the rate is dependent on value, shippers are required to state specifically in writing the agreed or declared value of the property as
follows:
“The agreed or declared value of the property is specifically stated by the shipper to be not exceeding ______per______
NOTE(II) Liability limitation for loss or damage on this shipment shall be applicable. See 49 US. C. - 14706(c)(1)(A) and (B)
NOTE(III) commodities requiring special or additional care or attention in handling or stowing must be so
marked and packaged as to ensure safe transportation with ordinary care. see sec. 2(e) of nMfc item 360</font>
									</td>
								</tr>
								<tr>
									<td colspan="6">
										<font size="8">Subject to Section 7 of conditions of applicable Bill of Lading. if this shipment is to be delivered to the consignee,
without recourse on the consignor, the consignor shall sign the following statement. The carrier may decline to
make delivery of this shipment without payment of freight and all other lawful charges.
consignor signature _______________________________________________________________________</font>
									</td>
								</tr>
								<tr>
									<td bgcolor="silver" colspan="3">
										<b>SHIPPER SIGNATURE / DATE</b>
									</td>
									<td bgcolor="silver" colspan="3">
										<b>CARRIER SIGNATURE / PICKUP DATE</b>
									</td>
								</tr>
								<tr>
									<td colspan="3">
										<font size="8">This is to certify that the above named materials are properly classified,
described, packaged, marked and labeled, and
are in proper condition for transportation according to the applicable
regulations of the DOT
<br><br>_________________________Shipper ________/'.$todaysYear.' Date</font>
									</td>
									<td colspan="3">
										<font size="8">Carrier acknowledges receipt of packages and required placards. 
Carrier certifies emergency response information was made available
and/or carrier has the U.S. DOT emergency response guidebook or
equivalent documentation in the vehicle.
<br><br>_____________________ Driver _______/'.$todaysYear.' Date</font>
									</td>
								</tr>
								</table>';
								if (isCarrierNameCorrect($originCarrierName))
								{
									/*if ($arr[0]['CustomerRef_FullName'] == "Amazon" && PrintCarrierForAmazonMode == '1')
									{
										$tblLogo = $tbl;
									} else
									{*/
										$carrierLogo = getCarrierLogo($originCarrierName);
										$tblLogo = $tbl.'<table cellpadding="3">
											<tr>
												<td>
													<font size="'.globalCarrierFont.'"><b>'.$originCarrierName.'</b></font><br>
													
												</td>
												<td>
													<img src="'.$carrierLogo.'" height="100px" width="340px">
												</td>
											</tr>
										</table>';
									/*}*/
								} else 
								{
									$tblLogo = $tbl;
								}
								$pdf->writeHTML($tblLogo, true, false, false, false, '');
								
								$pdf->AddPage();
								$pdf->SetFont('helvetica', '', 9);
								if (isCarrierNameCorrect($originCarrierName))
								{
									/*if ($arr[0]['CustomerRef_FullName'] == "Amazon" && PrintCarrierForAmazonMode == '1')
									{
										$tbl = $tbl;
									} else
									{*/
										$carrierLogo = getCarrierLogo($originCarrierName);
										$tbl.='<table cellpadding="3">
											<tr>
												<td>
													<font size="'.globalCarrierFont.'"><b>'.$originCarrierName.'</b></font><br>
													<font size="'.globalCarrierFont.'">Carrier Copy</font>
												</td>
												<td>
													<img src="'.$carrierLogo.'" height="100px" width="340px">
												</td>
											</tr>
										</table>';
									/*}*/
								}
								
								$pdf->writeHTML($tbl, true, false, false, false, '');

				$pdf = get_packing_list($arr, $pdf);				
				$pdf->Output($pdf_path, 'F');
				$result[] = $pdf_path2;
			if (!empty($result))
			{
				return $result;
			} else
			{
				error_msg("Can not generate bill of lading!");
				send_error();
			}
	}catch (Exception $e) {
		error_msg($e->getMessage());
		send_error();
	}
}

//Generate Bill of Lading pdf files
function wayfairBill($arr, $pro, $bol, $pro10, $barcodeBill, $originCarrierName)
{
	try{
			
			$barcodeBill = $barcodeBill.'.png';
			
			$PONumbers = array();
			$result = array();
			
			foreach ($arr as $row)
			{
				$PONumbers[] = $row['PONumber'];
			}
			$c = count($PONumbers);
			
				$pdf_path = "../PdfBill/".repPo($PONumbers[0])."Bill.pdf";
				$pdf_path2 = "PdfBill/".repPo($PONumbers[0])."Bill.pdf";
				
				date_default_timezone_set('America/New_York');
				
				$todaysDate = date("n/j/Y");
				$todaysYear = date("Y");
				
				$address = '';
				$consignee = '';
				if (isset($arr[0]['ShipAddress_Addr1']) && !empty($row['ShipAddress_Addr1'])) $consignee = $row['ShipAddress_Addr1']; else $consignee = '';
				
				if (isset($arr[0]['ShipAddress_Addr2']) && !empty($row['ShipAddress_Addr2'])) $address.=$row['ShipAddress_Addr2'].', ';
				if (isset($arr[0]['ShipAddress_Addr3']) && !empty($row['ShipAddress_Addr3'])) $address.=$row['ShipAddress_Addr3'].', ';
				if (isset($arr[0]['ShipAddress_Addr4']) && !empty($row['ShipAddress_Addr4'])) $address.=$row['ShipAddress_Addr4'].', ';
				if (isset($arr[0]['ShipAddress_Addr5']) && !empty($row['ShipAddress_Addr5'])) $address.=$row['ShipAddress_Addr5'];
				
				if (isset($arr[0]['FOB']) && !empty($row['FOB'])) $shipperPhone=$row['FOB']; else $shipperPhone='';
				
				$cityStateZip = '';
				if (isset($arr[0]['ShipAddress_City']) && !empty($row['ShipAddress_City'])) $cityStateZip.=$row['ShipAddress_City'].', ';
				if (isset($arr[0]['ShipAddress_State']) && !empty($row['ShipAddress_State'])) $cityStateZip.=$row['ShipAddress_State'].', ';
				if (isset($arr[0]['ShipAddress_PostalCode']) && !empty($row['ShipAddress_PostalCode'])) $cityStateZip.=$row['ShipAddress_PostalCode'];
				
				if (!isset($arr[0]['PALLETS']) || empty($arr[0]['PALLETS']))
				{
					$units = ceil($arr[0]['WEIGHT'] / 2500);
				} else
				{
					$units = $arr[0]['PALLETS'];
				}
				
				$descriptionHtml = get_description($arr, 4);
				
				$descriptionHtml = getAmazonSticker($arr[0]['CustomerRef_FullName'], $arr[0]['PONumber'], $descriptionHtml, $arr[0]['Memo']);
				if ($arr[0]['CustomerRef_FullName'] != 'Amazon')
					{					
						$consigneeAllInfo = '<tr>
											<td colspan="3">
												Consignee:<br>
												Address:<br>
												City/State/Zip:<br>
												Phone#:<br>
											</td>
											<td colspan="3">
												<b>'.$consignee.'<br>
												'.$address.'<br>
												'.$cityStateZip.'<br>
												'.$shipperPhone.'</b>
											</td>
										</tr>';
					} else
					{
						$consigneeAllInfo = '<tr>
											<td colspan="3">
												Consignee:<br>
												Address:<br>
												City/State/Zip:<br>
											</td>
											<td colspan="3">
												<b>'.$consignee.'<br>
												'.$address.'<br>
												'.$cityStateZip.'<br></b>
											</td>
										</tr>';
					}
				if ($arr[0]['RESIDENTIAL'] == '1') $residential = '../Include/pictures/checkboxtrue.png'; else $residential = '../Include/pictures/checkboxfalse.png';
				if ($arr[0]['LIFTGATE'] == '1') $liftgate = '../Include/pictures/checkboxtrue.png'; else $liftgate = '../Include/pictures/checkboxfalse.png';
				if ($arr[0]['delivery_notification'] == '1') $delivery_notification = '../Include/pictures/checkboxtrue.png'; else $delivery_notification = '../Include/pictures/checkboxfalse.png';
				//$residential = '../Include/pictures/checkboxfalse.png';
				//$liftgate = '../Include/pictures/checkboxfalse.png';
				
				$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
				$pdf->SetMargins(11, 11);
				$pdf->SetAutoPageBreak(TRUE, 1);
				$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);


						
						$pdf->AddPage();
						$pdf->SetFont('helvetica', '', 9);
						$tbl = '<table border="1" width="670" height="1000" cellpadding="3">
								<tr>
									<td colspan="6">
										<font size="12"><b>BILL OF LADING    </b></font>
										Delivery Problem? Contact Bath Authority: support@dreamline.com or 866-731-2244
									</td>
								</tr>
								<tr>';
						if ($barcodeBill == 'none' || $pro10 == 'none' || ($arr[0]['CustomerRef_FullName'] == "Amazon" && PrintCarrierForAmazonMode == '1'))
						{
							$tbl.='<td colspan="3" align="center">
												<br>
												<br>
												<br>
												<br>
												<br>
												<font color="silver" size="20">Place sticker here</font>
												<br>
												<br>
												<br>
												<br>
											</td>';
						} else
						{
							$tbl.='<td colspan="3" align="center">
										<p><b><font size="32">'.$pro10.'</font></b></p>
										<img src="'.$barcodeBill.'" with="281" height="50">
										<br>
										<br>
										<br>
										<br>
									</td>';
						}
									
								$tbl.='	<td colspan="3">
										<table border="1" cellpadding="3">
										<tr>
											<td colspan="1">
												Date
											</td>
											<td colspan="1">
												<b>'.$todaysDate.'</b>
											</td>
										</tr>
										<tr>
											<td colspan="1">
												Shipper BOL#
											</td>
											<td colspan="1">
												<b>'.$bol.'</b>
											</td>
										</tr>
										<tr>
											<td colspan="1">
												Carrier Pro#
											</td>
											<td colspan="1">
												'.$pro.'
											</td>
										</tr>
										<tr>
											<td colspan="1">
												Purchase Order #
											</td>
											<td colspan="1">
												'.checkPOBill($arr[0]['PONumber']).'<br>
											</td>
										</tr>
										</table>
										<br>
										<br>
										<br>
										<br>
									</td>
								</tr>

								<tr>
									<td colspan="3">
										<table border="1" cellpadding="3">
										<tr>
											<td bgcolor="silver" align="center" colspan="6">
												<FONT size="8" COLOR="black"><b>SHIPPER (from)</b></FONT>
											</td>
										</tr>
										<tr>
											<td colspan="3">
												Shipper:<br>
												Address:<br>
												City/State/Zip:<br>
												Phone#:
											</td>
											<td colspan="3">
												<b>Bath Authority<br>
												75 Hawk Road<br>
												Warminster, PA 18974<br>
												866-731-8378</b>
											</td>
										</tr>
										</table>
									</td>
									<td colspan="3">
										<table border="1" cellpadding="3">
										<tr>
											<td bgcolor="silver" align="center" colspan="6">
												<FONT size="8" COLOR="black"><b>CONSIGNEE (to)</b></FONT>
											</td>
										</tr>
										'.$consigneeAllInfo.'
										</table>
									</td>
								</tr>
								<tr>
									<td colspan="6">
										<font size="8"><b><label for="img1">Freight charges are PREPAID unless marked collect.Check box if collect  </label>
										<img id="img1" src="../Include/pictures/checkboxfalse.png" width="12" height="12"></b></font>
									</td>
								</tr>
								<tr>
									<td bgcolor="silver" colspan="6">
										<b>BILL FREIGHT CHARGES TO (if different):</b>
									</td>
								</tr>
								<tr>
									<td colspan="6">';
									if($originCarrierName == 'FEDEX GR - PROC' || $originCarrierName == 'FEDEX GR')	$tbl.='<font size="8">Name: Wayfair LLC, Address: 4 Copley Place Floor 7, 
City: Boston, State: MA, Zip: 02116, Acct#: 346593300</font>';
									else $tbl.= '<font size="8">Name: Wayfair LLC, Address: 4 Copley Place Floor 7, 
City: Boston, State: MA, Zip: 02116</font>';
									$tbl.='</td>
								</tr>
								<tr>
									<td colspan="6">
										<font size="8">RECEIVED, subject to individually determined rates or contracts that have been agreed upon in writing between the carrier and shipper, if applicable, otherwise to the rates,
classifications and rules that have been established by the carrier and are available to the shipper, on request, and to all applicable state and federal regulations.</font>
									</td>
								</tr>
								<tr>
									<td colspan="1" align="center">
										<b>ADDED SERVICES</b><br>
									</td>
									<td colspan="5">
										<font size="8">  <img id="img1" src="'.$liftgate.'" width="12" height="12">
										<label for="img1">Liftgate  </label>
										<img id="img2" src="../Include/pictures/checkboxfalse.png" width="12" height="12">
										<label for="img2">Inside Delivery  </label>
										<img id="img3" src="'.$residential.'" width="12" height="12">
										<label for="img3">Residential  </label>
										<img id="img4" src="'.$delivery_notification.'" width="12" height="12">
										<label for="img4">Delivery Notification  </label>
										<img id="img4" src="../Include/pictures/checkboxfalse.png" width="12" height="12">
										<label for="img4">Custom Delivery Window  </label></font>
									</td>
								</tr>
								<tr>
									<td colspan="6">
									<table border="1" cellpadding="3">
									<tr>
										<td colspan="1" width="50" align="center">
											Units
										</td>
										<td colspan="1" width="50" align="center">
											Type
										</td>
										<td colspan="1" width="50" align="center">
											Boxes
										</td>
										<td colspan="1" width="50" align="center">
											HM*
										</td>
										<td colspan="4" width="285" align="center">
											Description
										</td>
										<td colspan="1" width="50" align="center">
											Weight
										</td>
										<td colspan="1" width="80" align="center">
											NMFC Item #
										</td>
										<td colspan="1" width="50" align="center">
											Class
										</td>
									</tr>
									<tr>
										<td colspan="1" width="50">
											'.$units.'
										</td>
										<td colspan="1" width="50">
											SKID
										</td>
										<td colspan="1" width="50">
											'.$arr[0]['boxes'].'
										</td>
										<td colspan="1" width="50">
											
										</td>
										<td colspan="4" width="285">
											'.$descriptionHtml.'
										</td>
										<td colspan="1" width="50">
											'.$arr[0]['WEIGHT'].'
										</td>
										<td colspan="1" width="80">
											159010-02
										</td>
										<td colspan="1" width="50">
											85
										</td>
									</tr>
									</table>
									</td>
								</tr>
								<tr>
									<td colspan="6">
										<font size="8">* MARK “X” or “RQ” in the HM column to designate hazardous materials or reportable quantity as defined in DOT regulations.</font>
									</td>
								</tr>
								<tr>
									<td colspan="6">
										<font size="8"><b>SPECIAL INSTRUCTIONS: Call for delivery! Notify Consignee. Do not Double stack pallets. No additional accessorial
authorized by shipper</b></font>
									</td>
								</tr>
								<tr>
									<td colspan="6" align="left">
										<font size="9">SPECIAL INSTRUCTIONS :  If no delivery appointment has been made with the consignee within 1 business day of receiving the shipment, please contact Dreamline/Bath Authority at 866-731-2244 or email support@dreamline.com to assist with alternate contact information.
										</font>
									</td>
								</tr>
								<tr>
									<td colspan="6">
										<font size="8">NOTE (I) Where the rate is dependent on value, shippers are required to state specifically in writing the agreed or declared value of the property as
follows:
“The agreed or declared value of the property is specifically stated by the shipper to be not exceeding ______per______
NOTE(II) Liability limitation for loss or damage on this shipment shall be applicable. See 49 US. C. - 14706(c)(1)(A) and (B)
NOTE(III) commodities requiring special or additional care or attention in handling or stowing must be so
marked and packaged as to ensure safe transportation with ordinary care. see sec. 2(e) of nMfc item 360</font>
									</td>
								</tr>
								<tr>
									<td colspan="6">
										<font size="8">Subject to Section 7 of conditions of applicable Bill of Lading. if this shipment is to be delivered to the consignee,
without recourse on the consignor, the consignor shall sign the following statement. The carrier may decline to
make delivery of this shipment without payment of freight and all other lawful charges.
consignor signature _______________________________________________________________________</font>
									</td>
								</tr>
								<tr>
									<td bgcolor="silver" colspan="3">
										<b>SHIPPER SIGNATURE / DATE</b>
									</td>
									<td bgcolor="silver" colspan="3">
										<b>CARRIER SIGNATURE / PICKUP DATE</b>
									</td>
								</tr>
								<tr>
									<td colspan="3">
										<font size="8">This is to certify that the above named materials are properly classified,
described, packaged, marked and labeled, and
are in proper condition for transportation according to the applicable
regulations of the DOT
<br><br>_________________________Shipper ________/'.$todaysYear.' Date</font>
									</td>
									<td colspan="3">
										<font size="8">Carrier acknowledges receipt of packages and required placards. 
Carrier certifies emergency response information was made available
and/or carrier has the U.S. DOT emergency response guidebook or
equivalent documentation in the vehicle.
<br><br>_____________________ Driver _______/'.$todaysYear.' Date</font>
									</td>
								</tr>
								</table>';
								if (isCarrierNameCorrect($originCarrierName))
								{
									/*if ($arr[0]['CustomerRef_FullName'] == "Amazon" && PrintCarrierForAmazonMode == '1')
									{
										$tblLogo = $tbl;
									} else
									{*/
										$carrierLogo = getCarrierLogo($originCarrierName);
										$tblLogo = $tbl.'<table cellpadding="3">
											<tr>
												<td>
													<font size="'.globalCarrierFont.'"><b>'.$originCarrierName.'</b></font><br>
													
												</td>
												<td>
													<img src="'.$carrierLogo.'" height="100px" width="340px">
												</td>
											</tr>
										</table>';
									/*}*/
								} else 
								{
									$tblLogo = $tbl;
								}
								$pdf->writeHTML($tblLogo, true, false, false, false, '');
								
								$pdf->AddPage();
								$pdf->SetFont('helvetica', '', 9);
								if (isCarrierNameCorrect($originCarrierName))
								{
									/*if ($arr[0]['CustomerRef_FullName'] == "Amazon" && PrintCarrierForAmazonMode == '1')
									{
										$tbl = $tbl;
									} else
									{*/
										$carrierLogo = getCarrierLogo($originCarrierName);
										$tbl.='<table cellpadding="3">
											<tr>
												<td>
													<font size="'.globalCarrierFont.'"><b>'.$originCarrierName.'</b></font><br>
													<font size="'.globalCarrierFont.'">Carrier Copy</font>
												</td>
												<td>
													<img src="'.$carrierLogo.'" height="100px" width="340px">
												</td>
											</tr>
										</table>';
									/*}*/
								}
								
								$pdf->writeHTML($tbl, true, false, false, false, '');

				$pdf = get_packing_list($arr, $pdf);				
				$pdf->Output($pdf_path, 'F');
				$result[] = $pdf_path2;
			if (!empty($result))
			{
				return $result;
			} else
			{
				error_msg("Can not generate bill of lading!");
				send_error();
			}
	}catch (Exception $e) {
		error_msg($e->getMessage());
		send_error();
	}
}

//Generate Bill of Lading pdf files
function wayfairBill2($arr, $pro, $bol, $pro10, $barcodeBill, $originCarrierName)
{
	try{
			
			if($barcodeBill != 'none') $barcodeBill = $pro.'<br><img width="150px" src="'.$barcodeBill.'.png">'; else $barcodeBill='';
			if($pro10 == 'none') $pro10 ='';
			
			$PONumbers = array();
			$result = array();
			
			foreach ($arr as $row)
			{
				$PONumbers[] = $row['PONumber'];
			}
			$c = count($PONumbers);
			
				$pdf_path = "../PdfBill/".repPo($PONumbers[0])."Bill.pdf";
				$pdf_path2 = "PdfBill/".repPo($PONumbers[0])."Bill.pdf";
				
				date_default_timezone_set('America/New_York');
				
				$todaysDate = date("n/j/Y");
				$todaysYear = date("Y");
				
				$address = '';
				$consignee = '';
				if (isset($arr[0]['ShipAddress_Addr1']) && !empty($arr[0]['ShipAddress_Addr1'])) $consignee = $arr[0]['ShipAddress_Addr1']; else $consignee = '';
				/*print_r($arr);
				die();*/
				if (isset($arr[0]['ShipAddress_Addr2']) && !empty($arr[0]['ShipAddress_Addr2'])) $address.=$arr[0]['ShipAddress_Addr2'].', ';
				if (isset($arr[0]['ShipAddress_Addr3']) && !empty($arr[0]['ShipAddress_Addr3'])) $address.=$arr[0]['ShipAddress_Addr3'].', ';
				if (isset($arr[0]['ShipAddress_Addr4']) && !empty($arr[0]['ShipAddress_Addr4'])) $address.=$arr[0]['ShipAddress_Addr4'].', ';
				if (isset($arr[0]['ShipAddress_Addr5']) && !empty($arr[0]['ShipAddress_Addr5'])) $address.=$arr[0]['ShipAddress_Addr5'];
				
				if (isset($arr[0]['FOB']) && !empty($arr[0]['FOB'])) $shipperPhone=$arr[0]['FOB']; else $shipperPhone='';
				
				if (isset($arr[0]['ShipAddress_Country']) && !empty($arr[0]['ShipAddress_Country'])) $country=$arr[0]['ShipAddress_Country']; else $country='';
				
				$cityStateZip = '';
				if (isset($arr[0]['ShipAddress_City']) && !empty($arr[0]['ShipAddress_City'])) $cityStateZip.=$arr[0]['ShipAddress_City'].', ';
				if (isset($arr[0]['ShipAddress_State']) && !empty($arr[0]['ShipAddress_State'])) $cityStateZip.=$arr[0]['ShipAddress_State'].', ';
				if (isset($arr[0]['ShipAddress_PostalCode']) && !empty($arr[0]['ShipAddress_PostalCode'])) $cityStateZip.=$arr[0]['ShipAddress_PostalCode'];
				
				
				if (!isset($arr[0]['PALLETS']) || empty($arr[0]['PALLETS']))
				{
					$units = ceil($arr[0]['WEIGHT'] / 2500);
				} else
				{
					$units = $arr[0]['PALLETS'];
				}
							
						$consigneeAllInfo = $consignee.'<br>
												'.$address.'<br>
												'.$cityStateZip.'<br>
												'.$country.'<br>
												'.$shipperPhone;
				
				if ($arr[0]['RESIDENTIAL'] == '1') $residential = '<td border="1" align="center" width="4%"><b>X</b></td>'; else $residential = '<td border="1" align="center" width="4%">&nbsp;</td>';
				if ($arr[0]['LIFTGATE'] == '1') $liftgate = '<td border="1" align="center" width="4%"><b>X</b></td>'; else $liftgate = '<td border="1" align="center" width="4%">&nbsp;</td>';
				if ($arr[0]['delivery_notification'] == '1') $delivery_notification = '<td border="1" align="center" width="4%"><b>X</b></td>'; else $delivery_notification = '<td border="1" align="center" width="4%">&nbsp;</td>';
				
				$descriptionHtml = get_description($arr, 4);
				
				$quantity = 0;
				foreach ($arr as $ar)
				{
					$quantity = $quantity + $ar['quantity'];
				}
				
				if($originCarrierName == 'FEDEX GR - PROC' || $originCarrierName == 'FEDEX GR') $account = '<br>Acct#: 346593300'; else $account = '';
				
				$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
				$pdf->SetPrintHeader(false);
				$pdf->SetPrintFooter(false);
				$pdf->SetMargins(11, 11);
				$pdf->SetAutoPageBreak(TRUE, 1);
				$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);


						
						$pdf->AddPage('P', array(210, 350), false, false);
						$pdf->SetFont('helvetica', '', 9);
						$tbl = '<table width="670" cellspacing="4" cellpadding="5">
<tr>
	<td border="1" align="center" width="20%">'.$todaysDate.'</td>
	<td border="1" align="center" width="40%"><b>Bill of Lading - Non Negotiable</b></td>	
	<td border="1" align="center" width="40%">
		<strong>BOL:</strong>'.$barcodeBill.'
	</td>
</tr>
</table>

<table width="670" cellspacing="4" cellpadding="2">
<tr>
	<td border="1" align="center" width="50%" colspan="2"><b>SHIP FROM</b></td>	
	<td border="1" align="center" width="50%" colspan="2"><b>Waybill # / PRO Number</b></td>
</tr>
<tr>
	<td border="1" align="left" width="50%" colspan="2">
	Dreamline<br />
	75 Hawk Rd<br />
	Warminster, PA - 18974<br />
	United States<br />
	PH: 866 731 8378 ext 144
	</td>	
	<td border="1" align="center" width="50%" colspan="2"></td>
</tr>
<tr>
	<td border="1" align="center" width="25%"><b>SHIP TO</b></td>
	<td border="1" align="center" width="25%"><b>DA:</b></td>
	<td border="1" align="center" width="50%" colspan="2"><b>CARRIER INFO</b></td>
</tr>
<tr>
	<td border="1" align="left" width="50%" colspan="2">
	'.$consigneeAllInfo.'
	</td>
	<td border="1" align="center" width="50%" colspan="2">
		<table width="100%" cellspacing="4" cellpadding="2">
			<tr>
				<td border="1" align="left" width="40%">Carrier Name:</td>				
				<td border="1" align="left" width="60%">'.$arr[0]['carrier_info_name'].'</td>
			</tr>
			<tr>
				<td border="1" align="left" width="40%">Carrier Phone:</td>				
				<td border="1" align="left" width="60%">'.$arr[0]['carrier_info_phone'].'</td>
			</tr>
			<tr>
				<td border="1" align="left" width="40%">SCAC Code:</td>				
				<td border="1" align="left" width="60%">'.$arr[0]['carrier_info_SCAC'].'</td>
			</tr>
			<tr>
				<td border="1" align="left" width="40%">Freight Charge Terms:</td>				
				<td border="1" align="left" width="60%">'.$arr[0]['carrier_info_FCT'].'</td>
			</tr>
		</table>
	</td>
</tr>
<tr>
	<td border="1" align="center" width="50%" colspan="2"><b>THIRD PARTY FREIGHT BILL TO</b></td>	
	<td border="1" align="center" width="50%" colspan="2"><b>ADDITIONAL SERVICES (ALL CHECKED SERVICES APPLY)</b></td>
</tr>
<tr>
	<td border="1" align="center" width="50%" colspan="2">
		<table width="100%" cellspacing="4" cellpadding="2">
			<tr>
				<td border="1" align="left" valign="middle" width="50%" rowspan="2">
				Wayfair LLC<br />
				4 Copley Place Floor 7<br />
				Boston, MA 02116<br />
				United States
				'.$account.'
				</td>	
				<td border="1" align="left" width="50%"><font size="12" style="line-height:24pt"><b>PO: </b>'.$arr[0]['PONumber'].'</font></td>
			</tr>
			<tr>
				<td border="1" align="left" width="50%"><font size="12" style="line-height:24pt"><b>BOL: </b>'.$pro10.'</font></td>
			</tr>
			<tr>
				<td border="1" align="left" width="100%" colspan="2">
				<b>Special Instructions:</b><br />
				For Carrier convenience 30 minute precall is recommended<br />
				<br />
				For all dispositions please contact freight@wayfair.com or call (617) 532-6106 for assistance
				</td>
			</tr>
		</table>
	</td>	
	<td border="1" align="left" width="50%" colspan="2">
		<table width="100%" cellspacing="1" cellpadding="1">
			<tr>
				'.$residential.'
				<td border="0" align="left" width="96%">Residential Delivery</td>
			</tr>
			<tr>
				'.$delivery_notification.'
				<td border="0" align="left" width="96%">Must Notify Consignee Prior to Delivery</td>
			</tr>
			<tr>
				'.$liftgate.'
				<td border="0" align="left" width="96%">Liftgate Service Reqired at Delivery</td>
			</tr>
			<tr>
				<td border="1" align="center" width="4%">&nbsp;</td>
				<td border="0" align="left" width="96%">Inside Delivery</td>
			</tr>
			<tr>
				<td border="1" align="center" width="4%">&nbsp;</td>
				<td border="0" align="left" width="96%">Additional Flight of Stair (1 additional flight)</td>
			</tr>
			<tr>
				<td border="1" align="center" width="4%">&nbsp;</td>
				<td border="0" align="left" width="96%">Time Stop</td>
			</tr>
		</table>
		<b>Note: </b>Wayfair will not be responsible for any additional accessorial charges not checked here. Only Wayfair can authorize charges to thiss Bill of Lading by written authority. <b>Please contact Wayfair at (617) 532-6106 or FREIGHT@WAYFAIR.COM for written authority</b>
	</td>
</tr>
</table>

<table width="670" cellspacing="4" cellpadding="2">
	<tr>
		<td border="1" align="center" width="100%" colspan="7"><b>ORDER INFORMATION</b></td>		
	</tr>
	<tr>
		<td border="1" align="center" width="14%" colspan="2"><b>Shipment Specs</b></td>
		<td border="1" align="center" width="66%" colspan="3"><b>Commodity Specs</b></td>
		<td border="1" align="center" width="20%" colspan="2"><b>LTL Only</b></td>
	</tr>
	<tr>
		<td border="1" width="6%" align="center"><b>Qty</b></td>
		<td border="1" width="8%" align="center"><b>Type</b></td>
		<td border="1" width="8%" align="center"><b>Weight</b></td>
		<td border="1" width="5%" align="center"><b>HM</b></td>	
		<td border="1" width="53%" align="center"><b>Commodity Description</b></td>
		<td border="1" width="10%" align="center"><b>NMFC#</b></td>
		<td border="1" width="10%" align="center"><b>Class</b></td>
	</tr>
	<tr>
		<td border="1" width="6%" align="center">'.$quantity.'</td>
		<td border="1" width="8%" align="center">Carton</td>
		<td border="1" width="8%" align="center">'.$arr[0]['WEIGHT'].'</td>
		<td border="1" width="5%" align="center">&nbsp;</td>	
		<td border="1" width="53%" align="center">'.$descriptionHtml.'</td>
		<td border="1" width="10%" align="center">159010</td>
		<td border="1" width="10%" align="center">85</td>
	</tr>
	<tr>
		<td border="1" width="8%" colspan="2" align="left"><b>Totals:</b></td>
		<td border="1" width="92%" colspan="5" align="left">1 PO, '.$arr[0]['boxes'].' Cartons, '.$arr[0]['WEIGHT'].' lbs</td>
	</tr>	
</table>

<table width="670" cellspacing="4" cellpadding="2">
	<tr>
		<td border="1" align="left" width="100%" colspan="4"><b>Emergency Contact Information:</b></td>		
	</tr>
	<tr>
		<td border="1" align="center" width="100%" colspan="4"><b>Note: Liability limitation for loss or damage in this shipment may be applicable. See 49 USC &sect; 14706 &copy; (1)(A) and (B)</b></td>		
	</tr>
	<tr>
		<td border="1" align="left" width="50%" colspan="2">
		Received, subject to individually determined rates or contracts that have been argeed upon in writing between the carrier and shipper, if applicable, otherwise to the rates, classifications, and rules that have been established by the carrier and are available to the shipper, on request, and to all applicable state and federal regulations.
		</td>
		<td border="1" align="left" width="50%" colspan="2">
		If this shipment is to be delivered to the consignee, without recourse on the consignor, the consignor shall sign the following statement. The carrier may decline to make delivery of this shipment without payment of freight and all other lawful charges.<br />
		<b>Shipper Signature:</b><br /><br />
		</td>
	</tr>
	<tr>
		<td border="1" align="left" width="33%">
		<b>Shipper Signature/Date:</b><br /><br />
		_________________________________<br />
		This is to certify the above named materials are properly classified, packed, marked, and labeled, and are in proper condition for transportation according to the applicable regulations of the DOT
		</td>		
		<td border="1" align="left" width="17%">
			<b>Trailer Loaded:</b><br /><br />
			<table width="100%" cellspacing="1" cellpadding="1">
				<tr>
					<td border="1" align="center" width="15%">&nbsp;</td>
					<td border="0" align="left" width="85%">  By shipper</td>
				</tr>
				<tr>
					<td border="1" align="center" width="15%">&nbsp;</td>
					<td border="0" align="left" width="85%">  By driver</td>
				</tr>
			</table>
		</td>		
		<td border="1" align="left" width="17%">
			<b>Freight Counted:</b><br /><br />
			<table width="100%" cellspacing="1" cellpadding="1">
				<tr>
					<td border="1" align="center" width="15%">&nbsp;</td>
					<td border="0" align="left" width="85%"> By shipper</td>
				</tr>
				<tr>
					<td border="1" align="center" width="15%">&nbsp;</td>
					<td border="0" align="left" width="85%"> By driver/pallets</td>
				</tr>
				<tr>
					<td border="0" align="left" width="85%" colspan="2">said to<br />contain</td>
				</tr>
				<tr>
					<td border="1" align="center" width="15%">&nbsp;</td>
					<td border="0" align="left" width="85%"> By driver/pieces</td>
				</tr>
			</table>		
		</td>
		<td border="1" align="left" width="33%">
		<b>Carrier Signature/Pickup Date:</b><br /><br />
		_________________________________<br />
		Carrier acknowledges receipt of the package and required placards. Carrier certifies emergencies response information was made available and/or carrier has the DOT emergency response guidebook or equivalent documentation in the vehicle. Properly described above is received in good order, exept as noted.
		</td>		
	</tr>	
</table>';
								
			if (isCarrierNameCorrect($originCarrierName))
								{
										$carrierLogo = getCarrierLogo($originCarrierName);
										$tblLogo = $tbl.'<table cellpadding="3">
											<tr>
												<td>
													<font size="'.globalCarrierFont.'"><b>'.$originCarrierName.'</b></font><br>
												</td>
												<td>
													<img src="'.$carrierLogo.'" height="100px" width="340px">
												</td>
											</tr>
										</table>';
									/*}*/
								} else 
								{
									$tblLogo = $tbl;
								}
								$pdf->writeHTML($tblLogo, true, false, false, false, '');
								
								$pdf->AddPage();
								$pdf->SetFont('helvetica', '', 9);
								if (isCarrierNameCorrect($originCarrierName))
								{
										$carrierLogo = getCarrierLogo($originCarrierName);
										$tbl.='<table cellpadding="3">
											<tr>
												<td>
													<font size="'.globalCarrierFont.'"><b>'.$originCarrierName.'</b></font><br>
													<font size="'.globalCarrierFont.'">Carrier Copy</font>
												</td>
												<td>
													<img src="'.$carrierLogo.'" height="100px" width="340px">
												</td>
											</tr>
										</table>';
									/*}*/
								}
								
								$pdf->writeHTML($tbl, true, false, false, false, '');

				$pdf = get_packing_list($arr, $pdf);				
				$pdf->Output($pdf_path, 'F');
				$result[] = $pdf_path2;
			if (!empty($result))
			{
				return $result;
			} else
			{
				error_msg("Can not generate bill of lading!");
				send_error();
			}
	}catch (Exception $e) {
		error_msg($e->getMessage());
		send_error();
	}
}

//Generate Bill of Lading pdf files
function rlBill($arr, $pro, $bol, $pro10, $barcodeBill, $originCarrierName)
{
	try{
			
			$barcodeBill = $barcodeBill.'.png';
			
			$PONumbers = array();
			$result = array();
			
			foreach ($arr as $row)
			{
				$PONumbers[] = $row['PONumber'];
			}
			$c = count($PONumbers);
			
				$pdf_path = "../PdfBill/".repPo($PONumbers[0])."Bill.pdf";
				$pdf_path2 = "PdfBill/".repPo($PONumbers[0])."Bill.pdf";
				
				date_default_timezone_set('America/New_York');
				
				$todaysDate = date("n/j/Y");
				$todaysYear = date("Y");
				
				$address = '';
				$consignee = '';
				if (isset($arr[0]['ShipAddress_Addr1']) && !empty($row['ShipAddress_Addr1'])) $consignee = $row['ShipAddress_Addr1']; else $consignee = '';
				
				if (extrStore($arr[0]['ShipAddress_Addr1']) != "Lowe's" && extrStore($arr[0]['CustomerRef_FullName']) == "Lowe's" && $arr[0]['RESIDENTIAL'] == '0')
				{
					$consignee.='<br>C/O '.$arr[0]['CustomerRef_FullName'];
					$consigneeTitle = 'Consignee:<br><br>';
				} else $consigneeTitle = 'Consignee:<br>';
				
				if (isset($arr[0]['ShipAddress_Addr2']) && !empty($row['ShipAddress_Addr2'])) $address.=$row['ShipAddress_Addr2'].', ';
				if (isset($arr[0]['ShipAddress_Addr3']) && !empty($row['ShipAddress_Addr3'])) $address.=$row['ShipAddress_Addr3'].', ';
				if (isset($arr[0]['ShipAddress_Addr4']) && !empty($row['ShipAddress_Addr4'])) $address.=$row['ShipAddress_Addr4'].', ';
				if (isset($arr[0]['ShipAddress_Addr5']) && !empty($row['ShipAddress_Addr5'])) $address.=$row['ShipAddress_Addr5'];
				
				if (isset($arr[0]['FOB']) && !empty($row['FOB'])) $shipperPhone=$row['FOB']; else $shipperPhone='';
				
				$cityStateZip = '';
				if (isset($arr[0]['ShipAddress_City']) && !empty($row['ShipAddress_City'])) $cityStateZip.=$row['ShipAddress_City'].', ';
				if (isset($arr[0]['ShipAddress_State']) && !empty($row['ShipAddress_State'])) $cityStateZip.=$row['ShipAddress_State'].', ';
				if (isset($arr[0]['ShipAddress_PostalCode']) && !empty($row['ShipAddress_PostalCode'])) $cityStateZip.=$row['ShipAddress_PostalCode'];
				
				if (!isset($arr[0]['PALLETS']) || empty($arr[0]['PALLETS']))
				{
					$units = ceil($arr[0]['WEIGHT'] / 2500);
				} else
				{
					$units = $arr[0]['PALLETS'];
				}
				
				$fergAcc = '___________';
				if (extrStore($arr[0]['CustomerRef_FullName']) == 'Ferguson')
				{
					$fergAcc = fergusonAccount($arr[0]['CARRIERNAME']);
					if ($fergAcc) $fergAccTitle = 'Account #:'; else $fergAcc = '___________';
				}
				
				$descriptionHtml = get_description($arr, 4);
				
				$descriptionHtml = getAmazonSticker($arr[0]['CustomerRef_FullName'], $arr[0]['PONumber'], $descriptionHtml, $arr[0]['Memo']);
				if ($arr[0]['CustomerRef_FullName'] != 'Amazon')
					{					
						$consigneeAllInfo = '<tr>
											<td colspan="3">
												'.$consigneeTitle.'
												Address:<br>
												City/State/Zip:<br>
												Phone#:<br>
											</td>
											<td colspan="3">
												<b>'.$consignee.'<br>
												'.$address.'<br>
												'.$cityStateZip.'<br>
												'.$shipperPhone.'</b>
											</td>
										</tr>';
					} else
					{
						$consigneeAllInfo = '<tr>
											<td colspan="3">
												'.$consigneeTitle.'
												Address:<br>
												City/State/Zip:<br>
											</td>
											<td colspan="3">
												<b>'.$consignee.'<br>
												'.$address.'<br>
												'.$cityStateZip.'<br></b>
											</td>
										</tr>';
					}
				if ($arr[0]['RESIDENTIAL'] == '1') $residential = '../Include/pictures/checkboxtrue.png'; else $residential = '../Include/pictures/checkboxfalse.png';
				if ($arr[0]['LIFTGATE'] == '1') $liftgate = '../Include/pictures/checkboxtrue.png'; else $liftgate = '../Include/pictures/checkboxfalse.png';
				if ($arr[0]['delivery_notification'] == '1') $delivery_notification = '../Include/pictures/checkboxtrue.png'; else $delivery_notification = '../Include/pictures/checkboxfalse.png';
				//$residential = '../Include/pictures/checkboxfalse.png';
				//$liftgate = '../Include/pictures/checkboxfalse.png';
				
				$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
				$pdf->SetMargins(11, 11);
				$pdf->SetAutoPageBreak(TRUE, 1);
				$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);


						
						$pdf->AddPage();
						$pdf->SetFont('helvetica', '', 9);
						$tbl = '<table border="1" width="670" height="1000" cellpadding="3">
								<tr>
									<td colspan="6">
										<font size="12"><b>BILL OF LADING    </b></font>
										Delivery Problem? Contact Bath Authority: support@dreamline.com or 866-731-2244
									</td>
								</tr>
								<tr>';
						if ($barcodeBill == 'none' || $pro10 == 'none' || ($arr[0]['CustomerRef_FullName'] == "Amazon" && PrintCarrierForAmazonMode == '1'))
						{
							$tbl.='<td colspan="3" align="center">
												<br>
												<br>
												<br>
												<br>
												<br>
												<font color="silver" size="20">Place sticker here</font>
												<br>
												<br>
												<br>
												<br>
											</td>';
						} else
						{
							$tbl.='<td colspan="3" align="center">
										<p><b><font size="32">'.$pro10.'</font></b></p>
										<img src="'.$barcodeBill.'" with="281" height="50">
										<br>
										<br>
										<br>
										<br>
									</td>';
						}
									
								$tbl.='	<td colspan="3">
										<table border="1" cellpadding="3">
										<tr>
											<td colspan="1">
												Date
											</td>
											<td colspan="1">
												<b>'.$todaysDate.'</b>
											</td>
										</tr>
										<tr>
											<td colspan="1">
												Shipper BOL#
											</td>
											<td colspan="1">
												<b>'.$bol.'</b>
											</td>
										</tr>
										<tr>
											<td colspan="1">
												Carrier Pro#
											</td>
											<td colspan="1">
												'.$pro.'
											</td>
										</tr>
										<tr>
											<td colspan="1">
												Purchase Order #
											</td>
											<td colspan="1">
												'.checkPOBill($arr[0]['PONumber']).'<br>
											</td>
										</tr>
										</table>
										<br>
										<br>
										<br>
										<br>
									</td>
								</tr>

								<tr>
									<td colspan="3">
										<table border="1" cellpadding="3">
										<tr>
											<td bgcolor="silver" align="center" colspan="6">
												<FONT size="8" COLOR="black"><b>SHIPPER (from)</b></FONT>
											</td>
										</tr>
										<tr>
											<td colspan="3">
												Shipper:<br>
												Address:<br>
												City/State/Zip:<br>
												Phone#:
											</td>
											<td colspan="3">
												<b>Bath Authority<br>
												75 Hawk Road<br>
												Warminster, PA 18974<br>
												866-731-8378</b>
											</td>
										</tr>
										</table>
									</td>
									<td colspan="3">
										<table border="1" cellpadding="3">
										<tr>
											<td bgcolor="silver" align="center" colspan="6">
												<FONT size="8" COLOR="black"><b>CONSIGNEE (to)</b></FONT>
											</td>
										</tr>
										'.$consigneeAllInfo.'
										</table>
									</td>
								</tr>
								<tr>
									<td colspan="6">
										<font size="8"><b><label for="img1">Freight charges are PREPAID unless marked collect.Check box if collect  </label>
										<img id="img1" src="../Include/pictures/checkboxfalse.png" width="12" height="12"></b></font>
									</td>
								</tr>
								<tr>
									<td bgcolor="silver" colspan="6">
										<b>BILL FREIGHT CHARGES TO (if different):</b>
									</td>
								</tr>
								<tr>
									<td colspan="6">
										<font size="8">Name___________________________________Address____________________________Acct#'.$fergAcc.'
City____________________________State_____________Zip________________________Phone#_________________</font>
									</td>
								</tr>
								<tr>
									<td colspan="6">
										<font size="8">RECEIVED, subject to individually determined rates or contracts that have been agreed upon in writing between the carrier and shipper, if applicable, otherwise to the rates,
classifications and rules that have been established by the carrier and are available to the shipper, on request, and to all applicable state and federal regulations.</font>
									</td>
								</tr>
								<tr>
									<td colspan="1" align="center">
										<b>ADDED SERVICES</b><br>
									</td>
									<td colspan="5">
										<font size="8">  <img id="img1" src="'.$liftgate.'" width="12" height="12">
										<label for="img1">Liftgate  </label>
										<img id="img2" src="../Include/pictures/checkboxfalse.png" width="12" height="12">
										<label for="img2">Inside Delivery  </label>
										<img id="img3" src="'.$residential.'" width="12" height="12">
										<label for="img3">Residential  </label>
										<img id="img4" src="'.$delivery_notification.'" width="12" height="12">
										<label for="img4">Delivery Notification  </label>
										<img id="img4" src="../Include/pictures/checkboxfalse.png" width="12" height="12">
										<label for="img4">Custom Delivery Window  </label></font>
									</td>
								</tr>
								<tr>
									<td colspan="6">
									<table border="1" cellpadding="3">
									<tr>
										<td colspan="1" width="50" align="center">
											Units
										</td>
										<td colspan="1" width="50" align="center">
											Type
										</td>
										<td colspan="1" width="50" align="center">
											Boxes
										</td>
										<td colspan="1" width="50" align="center">
											HM*
										</td>
										<td colspan="4" width="285" align="center">
											Description
										</td>
										<td colspan="1" width="50" align="center">
											Weight
										</td>
										<td colspan="1" width="80" align="center">
											NMFC Item #
										</td>
										<td colspan="1" width="50" align="center">
											Class
										</td>
									</tr>
									<tr>
										<td colspan="1" width="50">
											'.$units.'
										</td>
										<td colspan="1" width="50">
											SKID
										</td>
										<td colspan="1" width="50">
											'.$arr[0]['boxes'].'
										</td>
										<td colspan="1" width="50">
											
										</td>
										<td colspan="4" width="285">
											'.$descriptionHtml.'
										</td>
										<td colspan="1" width="50">
											'.$arr[0]['WEIGHT'].'
										</td>
										<td colspan="1" width="80">
											159010-02
										</td>
										<td colspan="1" width="50">
											85
										</td>
									</tr>
									</table>
									</td>
								</tr>
								<tr>
									<td colspan="6">
										<font size="8">* MARK “X” or “RQ” in the HM column to designate hazardous materials or reportable quantity as defined in DOT regulations.</font>
									</td>
								</tr>
								<tr>
									<td colspan="6">
										<font size="8"><b>SPECIAL INSTRUCTIONS: Call for delivery! Notify Consignee. Do not Double stack pallets. No additional accessorial
authorized by shipper</b></font>
									</td>
								</tr>
								<tr>
									<td colspan="6" align="left">
										<font size="9">SPECIAL INSTRUCTIONS :  If no delivery appointment has been made with the consignee within 1 business day of receiving the shipment, please contact Dreamline/Bath Authority at 866-731-2244 or email support@dreamline.com to assist with alternate contact information.
										</font>
									</td>
								</tr>
								<tr>
									<td colspan="6">
										<font size="8">NOTE (I) Where the rate is dependent on value, shippers are required to state specifically in writing the agreed or declared value of the property as
follows:
“The agreed or declared value of the property is specifically stated by the shipper to be not exceeding ______per______
NOTE(II) Liability limitation for loss or damage on this shipment shall be applicable. See 49 US. C. - 14706(c)(1)(A) and (B)
NOTE(III) commodities requiring special or additional care or attention in handling or stowing must be so
marked and packaged as to ensure safe transportation with ordinary care. see sec. 2(e) of nMfc item 360</font>
									</td>
								</tr>
								<tr>
									<td colspan="6">
										<font size="8">Subject to Section 7 of conditions of applicable Bill of Lading. if this shipment is to be delivered to the consignee,
without recourse on the consignor, the consignor shall sign the following statement. The carrier may decline to
make delivery of this shipment without payment of freight and all other lawful charges.
consignor signature _______________________________________________________________________</font>
									</td>
								</tr>
								<tr>
									<td bgcolor="silver" colspan="3">
										<b>SHIPPER SIGNATURE / DATE</b>
									</td>
									<td bgcolor="silver" colspan="3">
										<b>CARRIER SIGNATURE / PICKUP DATE</b>
									</td>
								</tr>
								<tr>
									<td colspan="3">
										<font size="8">This is to certify that the above named materials are properly classified,
described, packaged, marked and labeled, and
are in proper condition for transportation according to the applicable
regulations of the DOT
<br><br>_________________________Shipper ________/'.$todaysYear.' Date</font>
									</td>
									<td colspan="3">
										<font size="8">Carrier acknowledges receipt of packages and required placards. 
Carrier certifies emergency response information was made available
and/or carrier has the U.S. DOT emergency response guidebook or
equivalent documentation in the vehicle.
<br><br>_____________________ Driver _______/'.$todaysYear.' Date</font>
									</td>
								</tr>
								</table>';
								if (isCarrierNameCorrect($originCarrierName))
								{
									/*if ($arr[0]['CustomerRef_FullName'] == "Amazon" && PrintCarrierForAmazonMode == '1')
									{
										$tblLogo = $tbl;
									} else
									{*/
										$carrierLogo = getCarrierLogo($originCarrierName);
										$tblLogo = $tbl.'<table cellpadding="3">
											<tr>
												<td>
													<font size="'.globalCarrierFont.'"><b>'.$originCarrierName.'</b></font><br>
													
												</td>
												<td>
													<img src="'.$carrierLogo.'" height="100px" width="340px">
												</td>
											</tr>
										</table>';
									/*}*/
								} else 
								{
									$tblLogo = $tbl;
								}
								$pdf->writeHTML($tblLogo, true, false, false, false, '');
								
								$pdf->AddPage();
								$pdf->SetFont('helvetica', '', 9);
								if (isCarrierNameCorrect($originCarrierName))
								{
									/*if ($arr[0]['CustomerRef_FullName'] == "Amazon" && PrintCarrierForAmazonMode == '1')
									{
										$tbl = $tbl;
									} else
									{*/
										$carrierLogo = getCarrierLogo($originCarrierName);
										$tbl.='<table cellpadding="3">
											<tr>
												<td>
													<font size="'.globalCarrierFont.'"><b>'.$originCarrierName.'</b></font>
													<font size="'.globalCarrierFont.'">Carrier Copy</font>
												</td>
												<td>
													<img src="'.$carrierLogo.'" height="100px" width="340px">
												</td>
											</tr>
										</table>';
									/*}*/
								}
								
								$pdf->writeHTML($tbl, true, false, false, false, '');

				$pdf = get_packing_list($arr, $pdf);				
				$pdf->Output($pdf_path, 'F');
				$result[] = $pdf_path2;
			if (!empty($result))
			{
				return $result;
			} else
			{
				error_msg("Can not generate bill of lading!");
				send_error();
			}
	}catch (Exception $e) {
		error_msg($e->getMessage());
		send_error();
	}
}

//Generate Bill of Lading pdf files
function yrcBill($arr, $pro, $bol, $pro10, $barcodeBill, $originCarrierName)
{
	try{
			
			$barcodeBill = $barcodeBill.'.png';
			
			$PONumbers = array();
			$result = array();
			
			foreach ($arr as $row)
			{
				$PONumbers[] = $row['PONumber'];
			}
			$c = count($PONumbers);
			
				$pdf_path = "../PdfBill/".repPo($PONumbers[0])."Bill.pdf";
				$pdf_path2 = "PdfBill/".repPo($PONumbers[0])."Bill.pdf";
				
				date_default_timezone_set('America/New_York');
				
				$todaysDate = date("n/j/Y");
				$todaysYear = date("Y");
				
				$address = '';
				$consignee = '';
				if (isset($arr[0]['ShipAddress_Addr1']) && !empty($row['ShipAddress_Addr1'])) $consignee = $row['ShipAddress_Addr1']; else $consignee = '';
				
				if (extrStore($arr[0]['ShipAddress_Addr1']) != "Lowe's" && extrStore($arr[0]['CustomerRef_FullName']) == "Lowe's" && $arr[0]['RESIDENTIAL'] == '0')
				{
					$consignee.='<br>C/O '.$arr[0]['CustomerRef_FullName'];
					$consigneeTitle = 'Consignee:<br><br>';
				} else $consigneeTitle = 'Consignee:<br>';
				
				if (isset($arr[0]['ShipAddress_Addr2']) && !empty($row['ShipAddress_Addr2'])) $address.=$row['ShipAddress_Addr2'].', ';
				if (isset($arr[0]['ShipAddress_Addr3']) && !empty($row['ShipAddress_Addr3'])) $address.=$row['ShipAddress_Addr3'].', ';
				if (isset($arr[0]['ShipAddress_Addr4']) && !empty($row['ShipAddress_Addr4'])) $address.=$row['ShipAddress_Addr4'].', ';
				if (isset($arr[0]['ShipAddress_Addr5']) && !empty($row['ShipAddress_Addr5'])) $address.=$row['ShipAddress_Addr5'];
				
				if (isset($arr[0]['FOB']) && !empty($row['FOB'])) $shipperPhone=$row['FOB']; else $shipperPhone='';
				
				$cityStateZip = '';
				if (isset($arr[0]['ShipAddress_City']) && !empty($row['ShipAddress_City'])) $cityStateZip.=$row['ShipAddress_City'].', ';
				if (isset($arr[0]['ShipAddress_State']) && !empty($row['ShipAddress_State'])) $cityStateZip.=$row['ShipAddress_State'].', ';
				if (isset($arr[0]['ShipAddress_PostalCode']) && !empty($row['ShipAddress_PostalCode'])) $cityStateZip.=$row['ShipAddress_PostalCode'];
				
				if (!isset($arr[0]['PALLETS']) || empty($arr[0]['PALLETS']))
				{
					$units = ceil($arr[0]['WEIGHT'] / 2500);
				} else
				{
					$units = $arr[0]['PALLETS'];
				}
				
				$fergAcc = '______________________';
				if (extrStore($arr[0]['CustomerRef_FullName']) == 'Ferguson')
				{
					$fergAcc = fergusonAccount($arr[0]['CARRIERNAME']);
					if ($fergAcc) $fergAccTitle = 'Account #:'; else $fergAcc = '______________________';
				}
				
				$descriptionHtml = get_description($arr, 4);
				
				$descriptionHtml = getAmazonSticker($arr[0]['CustomerRef_FullName'], $arr[0]['PONumber'], $descriptionHtml, $arr[0]['Memo']);
				if ($arr[0]['CustomerRef_FullName'] != 'Amazon')
					{					
						$consigneeAllInfo = '<tr>
											<td colspan="3">
												'.$consigneeTitle.'
												Address:<br>
												City/State/Zip:<br>
												Phone#:<br>
											</td>
											<td colspan="3">
												<b>'.$consignee.'<br>
												'.$address.'<br>
												'.$cityStateZip.'<br>
												'.$shipperPhone.'</b>
											</td>
										</tr>';
					} else
					{
						$consigneeAllInfo = '<tr>
											<td colspan="3">
												'.$consigneeTitle.'
												Address:<br>
												City/State/Zip:<br>
											</td>
											<td colspan="3">
												<b>'.$consignee.'<br>
												'.$address.'<br>
												'.$cityStateZip.'<br></b>
											</td>
										</tr>';
					}
					
				if ($arr[0]['RESIDENTIAL'] == '1') $residential = '../Include/pictures/checkboxtrue.png'; else $residential = '../Include/pictures/checkboxfalse.png';
				if ($arr[0]['LIFTGATE'] == '1') $liftgate = '../Include/pictures/checkboxtrue.png'; else $liftgate = '../Include/pictures/checkboxfalse.png';
				if ($arr[0]['delivery_notification'] == '1') $delivery_notification = '../Include/pictures/checkboxtrue.png'; else $delivery_notification = '../Include/pictures/checkboxfalse.png';
					
				$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
				$pdf->SetMargins(11, 11);
				$pdf->SetAutoPageBreak(TRUE, 1);
				$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);


						
						$pdf->AddPage();
						$pdf->SetFont('helvetica', '', 9);
						$tbl = '<table border="1" width="670" height="1000" cellpadding="3">
								<tr>
									<td colspan="6">
										<font size="12"><b>BILL OF LADING    </b></font>
										Delivery Problem? Contact Bath Authority: support@dreamline.com or 866-731-2244
									</td>
								</tr>
								<tr>';
						if ($barcodeBill == 'none' || $pro10 == 'none' || ($arr[0]['CustomerRef_FullName'] == "Amazon" && PrintCarrierForAmazonMode == '1'))
						{
							$tbl.='<td colspan="3" align="center" height="150">
												<br>
												<br>
												<br>
												<br>
												<br>
												<font color="silver" size="20">Place sticker here</font>
												<br>
												<br>
												<br>
												<br>
									</td>';
						} else
						{
							$tbl.='<td colspan="3" align="center" height="150">
										<p><b><font size="32">'.$pro10.'</font></b></p>
										<img src="'.$barcodeBill.'" with="281" height="50">
									</td>';
						}
									
								$tbl.='	<td colspan="3">
										<table border="1" cellpadding="3">
										<tr>
											<td colspan="1">
												Date
											</td>
											<td colspan="1">
												<b>'.$todaysDate.'</b>
											</td>
										</tr>
										<tr>
											<td colspan="1">
												Shipper BOL#
											</td>
											<td colspan="1">
												<b>'.$bol.'</b>
											</td>
										</tr>
										<tr>
											<td colspan="1">
												Carrier Pro#
											</td>
											<td colspan="1">
												'.$pro.'
											</td>
										</tr>
										<tr>
											<td colspan="1">
												Purchase Order #
											</td>
											<td colspan="1">
												'.checkPOBill($arr[0]['PONumber']).'<br>
											</td>
										</tr>
										</table>
									</td>
								</tr>

								<tr>
									<td colspan="3">
										<table border="1" cellpadding="3">
										<tr>
											<td bgcolor="silver" align="center" colspan="6">
												<FONT size="8" COLOR="black"><b>SHIPPER (from)</b></FONT>
											</td>
										</tr>
										<tr>
											<td colspan="3">
												Shipper:<br>
												Address:<br>
												City/State/Zip:<br>
												Phone#:
											</td>
											<td colspan="3">
												<b>Bath Authority<br>
												75 Hawk Road<br>
												Warminster, PA 18974<br>
												866-731-8378</b>
											</td>
										</tr>
										</table>
									</td>
									<td colspan="3">
										<table border="1" cellpadding="3">
										<tr>
											<td bgcolor="silver" align="center" colspan="6">
												<FONT size="8" COLOR="black"><b>CONSIGNEE (to)</b></FONT>
											</td>
										</tr>
										'.$consigneeAllInfo.'
										</table>
									</td>
								</tr>
								<tr>
									<td colspan="6">
										<font size="8"><b><label for="img1">Freight charges are PREPAID unless marked collect.Check box if collect  </label>
										<img id="img1" src="../Include/pictures/checkboxfalse.png" width="12" height="12"></b></font>
									</td>
								</tr>
								<tr>
									<td bgcolor="silver" colspan="6">
										<b>BILL FREIGHT CHARGES TO (if different):</b>
									</td>
								</tr>
								<tr>
									<td colspan="6">
										<font size="8">Name_______________________________________________Address________________________________________Acct#:'.$fergAcc.'
City____________________________State_____________Zip________________________Phone#______________________________________________</font>
									</td>
								</tr>
								<tr>
									<td colspan="6">
										<font size="8">RECEIVED, subject to individually determined rates or contracts that have been agreed upon in writing between the carrier and shipper, if applicable, otherwise to the rates,
classifications and rules that have been established by the carrier and are available to the shipper, on request, and to all applicable state and federal regulations.</font>
									</td>
								</tr>
								<tr>
									<td colspan="1" align="center">
										<b>ADDED SERVICES</b><br>
									</td>
									<td colspan="5">
										<font size="8">  <img id="img1" src="'.$liftgate.'" width="12" height="12">
										<label for="img1">Liftgate  </label>
										<img id="img2" src="../Include/pictures/checkboxfalse.png" width="12" height="12">
										<label for="img2">Inside Delivery  </label>
										<img id="img3" src="'.$residential.'" width="12" height="12">
										<label for="img3">Residential  </label>
										<img id="img4" src="'.$delivery_notification.'" width="12" height="12">
										<label for="img4">Delivery Notification  </label>
										<img id="img4" src="../Include/pictures/checkboxfalse.png" width="12" height="12">
										<label for="img4">Custom Delivery Window  </label></font>
									</td>
								</tr>
								<tr>
									<td colspan="6">
									<table border="1" cellpadding="3">
									<tr>
										<td colspan="1" width="50" align="center">
											Units
										</td>
										<td colspan="1" width="50" align="center">
											Type
										</td>
										<td colspan="1" width="50" align="center">
											Boxes
										</td>
										<td colspan="1" width="50" align="center">
											HM*
										</td>
										<td colspan="4" width="285" align="center">
											Description
										</td>
										<td colspan="1" width="50" align="center">
											Weight
										</td>
										<td colspan="1" width="80" align="center">
											NMFC Item #
										</td>
										<td colspan="1" width="50" align="center">
											Class
										</td>
									</tr>
									<tr>
										<td colspan="1" width="50">
											'.$units.'
										</td>
										<td colspan="1" width="50">
											SKID
										</td>
										<td colspan="1" width="50">
											'.$arr[0]['boxes'].'
										</td>
										<td colspan="1" width="50">
											
										</td>
										<td colspan="4" width="285">
											'.$descriptionHtml.'
										</td>
										<td colspan="1" width="50">
											'.$arr[0]['WEIGHT'].'
										</td>
										<td colspan="1" width="80">
											159010-02
										</td>
										<td colspan="1" width="50">
											85
										</td>
									</tr>
									</table>
									</td>
								</tr>
								<tr>
									<td colspan="6">
										<font size="8">* MARK “X” or “RQ” in the HM column to designate hazardous materials or reportable quantity as defined in DOT regulations.</font>
									</td>
								</tr>
								<tr>
									<td colspan="6">
										<font size="8"><b>SPECIAL INSTRUCTIONS: Call for delivery! Notify Consignee. Do not Double stack pallets. No additional accessorial
authorized by shipper</b></font>
									</td>
								</tr>
								<tr>
									<td colspan="6" align="left">
										<font size="9">SPECIAL INSTRUCTIONS :  If no delivery appointment has been made with the consignee within 1 business day of receiving the shipment, please contact Dreamline/Bath Authority at 866-731-2244 or email support@dreamline.com to assist with alternate contact information.
										</font>
									</td>
								</tr>
								<tr>
									<td colspan="6">
										<font size="8">NOTE (I) Where the rate is dependent on value, shippers are required to state specifically in writing the agreed or declared value of the property as
follows:
“The agreed or declared value of the property is specifically stated by the shipper to be not exceeding ______per______
NOTE(II) Liability limitation for loss or damage on this shipment shall be applicable. See 49 US. C. - 14706(c)(1)(A) and (B)
NOTE(III) commodities requiring special or additional care or attention in handling or stowing must be so
marked and packaged as to ensure safe transportation with ordinary care. see sec. 2(e) of nMfc item 360</font>
									</td>
								</tr>
								<tr>
									<td colspan="6">
										<font size="8">Subject to Section 7 of conditions of applicable Bill of Lading. if this shipment is to be delivered to the consignee,
without recourse on the consignor, the consignor shall sign the following statement. The carrier may decline to
make delivery of this shipment without payment of freight and all other lawful charges.
consignor signature _______________________________________________________________________</font>
									</td>
								</tr>
								<tr>
									<td bgcolor="silver" colspan="3">
										<b>SHIPPER SIGNATURE / DATE</b>
									</td>
									<td bgcolor="silver" colspan="3">
										<b>CARRIER SIGNATURE / PICKUP DATE</b>
									</td>
								</tr>
								<tr>
									<td colspan="3">
										<font size="8">This is to certify that the above named materials are properly classified,
described, packaged, marked and labeled, and
are in proper condition for transportation according to the applicable
regulations of the DOT
<br><br>_________________________Shipper ________/'.$todaysYear.' Date</font>
									</td>
									<td colspan="3">
										<font size="8">Carrier acknowledges receipt of packages and required placards. 
Carrier certifies emergency response information was made available
and/or carrier has the U.S. DOT emergency response guidebook or
equivalent documentation in the vehicle.
<br><br>_____________________ Driver _______/'.$todaysYear.' Date</font>
									</td>
								</tr>
								</table>';
								if (isCarrierNameCorrect($originCarrierName))
								{
									/*if ($arr[0]['CustomerRef_FullName'] == "Amazon" && PrintCarrierForAmazonMode == '1')
									{
										$tblLogo = $tbl;
									} else
									{*/
										$carrierLogo = getCarrierLogo($originCarrierName);
										$tblLogo = $tbl.'<table cellpadding="3">
											<tr>
												<td>
													<font size="'.globalCarrierFont.'"><b>'.$originCarrierName.'</b></font><br>
													
												</td>
												<td>
													<img src="'.$carrierLogo.'" height="100px" width="340px">
												</td>
											</tr>
										</table>';
									/*}*/
								} else 
								{
									$tblLogo = $tbl;
								}
								$pdf->writeHTML($tblLogo, true, false, false, false, '');
								
								$pdf->AddPage();
								$pdf->SetFont('helvetica', '', 9);
								if (isCarrierNameCorrect($originCarrierName))
								{
									/*if ($arr[0]['CustomerRef_FullName'] == "Amazon" && PrintCarrierForAmazonMode == '1')
									{
										$tbl = $tbl;
									} else
									{*/
										$carrierLogo = getCarrierLogo($originCarrierName);
										$tbl.='<table cellpadding="3">
											<tr>
												<td>
													<font size="'.globalCarrierFont.'"><b>'.$originCarrierName.'</b></font>
													<font size="'.globalCarrierFont.'">Carrier Copy</font>
												</td>
												<td>
													<img src="'.$carrierLogo.'" height="100px" width="340px">
												</td>
											</tr>
										</table>';
									/*}*/
								}
								
								$pdf->writeHTML($tbl, true, false, false, false, '');

				$pdf = get_packing_list($arr, $pdf);				
				$pdf->Output($pdf_path, 'F');
				$result[] = $pdf_path2;
			if (!empty($result))
			{
				return $result;
			} else
			{
				error_msg("Can not generate bill of lading!");
				send_error();
			}
	}catch (Exception $e) {
		error_msg($e->getMessage());
		send_error();
	}
}

//Generate Bill of Lading pdf files
function sekoBill($arr, $pro, $bol, $pro10, $barcodeBill, $originCarrierName)
{
	try{
			
			$barcodeBill = $barcodeBill.'.png';
			
			$PONumbers = array();
			$result = array();
			
			foreach ($arr as $row)
			{
				$PONumbers[] = $row['PONumber'];
			}
			$c = count($PONumbers);
			
				$pdf_path = "../PdfBill/".repPo($PONumbers[0])."Bill.pdf";
				$pdf_path2 = "PdfBill/".repPo($PONumbers[0])."Bill.pdf";
				
				date_default_timezone_set('America/New_York');
				
				$todaysDate = date("n/j/Y");
				$todaysYear = date("Y");
				
				$address = '';
				$consignee = '';
				if (isset($arr[0]['ShipAddress_Addr1']) && !empty($row['ShipAddress_Addr1'])) $consignee = $row['ShipAddress_Addr1']; else $consignee = '';
				
				if (isset($arr[0]['ShipAddress_Addr2']) && !empty($row['ShipAddress_Addr2'])) $address.=$row['ShipAddress_Addr2'].', ';
				if (isset($arr[0]['ShipAddress_Addr3']) && !empty($row['ShipAddress_Addr3'])) $address.=$row['ShipAddress_Addr3'].', ';
				if (isset($arr[0]['ShipAddress_Addr4']) && !empty($row['ShipAddress_Addr4'])) $address.=$row['ShipAddress_Addr4'].', ';
				if (isset($arr[0]['ShipAddress_Addr5']) && !empty($row['ShipAddress_Addr5'])) $address.=$row['ShipAddress_Addr5'];
				
				if (isset($arr[0]['FOB']) && !empty($row['FOB'])) $shipperPhone=$row['FOB']; else $shipperPhone='';
				
				$cityStateZip = '';
				if (isset($arr[0]['ShipAddress_City']) && !empty($row['ShipAddress_City'])) $cityStateZip.=$row['ShipAddress_City'].', ';
				if (isset($arr[0]['ShipAddress_State']) && !empty($row['ShipAddress_State'])) $cityStateZip.=$row['ShipAddress_State'].', ';
				if (isset($arr[0]['ShipAddress_PostalCode']) && !empty($row['ShipAddress_PostalCode'])) $cityStateZip.=$row['ShipAddress_PostalCode'];
				
				if ($arr[0]['CARRIERNAME'] == "FEDEX-ECONOMY")
				{
					$economy = 'checkboxtrue.png';
					$priority = 'checkboxfalse.png';
				} else {
					$economy = 'checkboxfalse.png';
					$priority = 'checkboxtrue.png';
				}
				
				
				
				if (!isset($arr[0]['PALLETS']) || empty($arr[0]['PALLETS']))
				{
					$units = ceil($arr[0]['WEIGHT'] / 2500);
				} else
				{
					$units = $arr[0]['PALLETS'];
				}
				
				$descriptionHtml = get_description($arr, 4);
				
				$descriptionHtml = getAmazonSticker($arr[0]['CustomerRef_FullName'], $arr[0]['PONumber'], $descriptionHtml, $arr[0]['Memo']);
				if ($arr[0]['CustomerRef_FullName'] != 'Amazon')
					{					
						$consigneeAllInfo = '<tr>
											<td colspan="6">
												<b>'.$consignee.'<br>
												'.$address.'<br>
												'.$cityStateZip.'<br>
												'.$shipperPhone.'</b>
											</td>
										</tr>';
					} else
					{
						$consigneeAllInfo = '<tr>
											<td colspan="6">
												<b>'.$consignee.'<br>
												'.$address.'<br>
												'.$cityStateZip.'</b>
											</td>
										</tr>';
					}
					if (IsPlaceAmazonSticker == 'True')
					{
						if ($arr[0]['CustomerRef_FullName'] == 'Amazon')
						{
							$redeliveryCharges = '';
						} else
						{
							$redeliveryCharges = '<tr>
									<td colspan="6">
										<font size="12"><b>Redelivery charges require authorization. 
Please contact support team at 866-731-8378 ext.169 or ext.134</b></font>
									</td>
								</tr>';
						}
					} else 
					{

						$redeliveryCharges = '<tr>
									<td colspan="6">
										<font size="12"><b>Redelivery charges require authorization. 
Please contact support team at 866-731-8378 ext.169 or ext.134</b></font>
									</td>
								</tr>';
					}
				
				if ($arr[0]['RESIDENTIAL'] == '1') $residential = '../Include/pictures/checkboxtrue.png'; else $residential = '../Include/pictures/checkboxfalse.png';
				if ($arr[0]['LIFTGATE'] == '1') $liftgate = '../Include/pictures/checkboxtrue.png'; else $liftgate = '../Include/pictures/checkboxfalse.png';
				if ($arr[0]['delivery_notification'] == '1') $delivery_notification = '../Include/pictures/checkboxtrue.png'; else $delivery_notification = '../Include/pictures/checkboxfalse.png';
				
				$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
				$pdf->SetMargins(11, 11);
				$pdf->SetAutoPageBreak(TRUE, 1);
				$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);


						
						$pdf->AddPage();
						$pdf->SetFont('helvetica', '', 9);
						$tbl = '<table border="1" width="670" height="1000" cellpadding="3">
								<tr>
									<td colspan="6">
										<font size="12"><b>BILL OF LADING    </b></font>
										Delivery Problem? Contact Bath Authority: support@dreamline.com or 866-731-2244
									</td>
								</tr>
								<tr>';
						if ($barcodeBill == 'none' || $pro10 == 'none' || ($arr[0]['CustomerRef_FullName'] == "Amazon" && PrintCarrierForAmazonMode == '1'))
						{
							$tbl.='<td colspan="3" align="center">
												<br>
												<br>
												<br>
												<br>
												<br>
												<font color="silver" size="20">Place sticker here</font>
												<br>
												<br>
												<br>
												<br>
											</td>';
						} else
						{
							$tbl.='<td colspan="3" align="center">
										<p><b><font size="32">'.$pro10.'</font></b></p>
										<img src="'.$barcodeBill.'" with="281" height="50">
										<br>
										<br>
										<br>
										<br>
									</td>';
						}
									
									$tbl.='<td colspan="3">
										<table border="1" cellpadding="3">
										<tr>
											<td>
												<font size="8"><b><label for="img3">Freight charges are PREPAID
												unless marked collect.<br>
												Check box if collect </label>
												<img id="img3" src="../Include/pictures/checkboxfalse.png" width="12" height="12"></b></font>
											</td>
										</tr>
										</table>
										<table border="1" cellpadding="3">
										<tr>
											<td colspan="1">
												Date
											</td>
											<td colspan="1">
												<b>'.$todaysDate.'</b>
											</td>
										</tr>
										<tr>
											<td colspan="1">
												Shipper BOL#
											</td>
											<td colspan="1">
												<b>BAT'.checkPOBill($arr[0]['PONumber']).'</b>
											</td>
										</tr>
										<tr>
											<td colspan="1">
												Carrier Pro#
											</td>
											<td colspan="1">
												BAT'.checkPOBill($arr[0]['PONumber']).'
											</td>
										</tr>
										<tr>
											<td colspan="1">
												Purchase Order #
											</td>
											<td colspan="1">
												'.checkPOBill($arr[0]['PONumber']).'
											</td>
										</tr>
										<tr>
											<td colspan="2">
												<b>Freight Terms (FOB Destination)</b><br>
												3rd Party
											</td>
										</tr>
										</table>
									</td>
								</tr>

								<tr>
									<td colspan="3">
										<table border="1" cellpadding="3">
										<tr>
											<td bgcolor="silver" align="center" colspan="6">
												<FONT size="8" COLOR="black"><b>SHIPPER (from)</b></FONT>
											</td>
										</tr>
										<tr>
											<td colspan="3">
												SEKO Account #:<br>
												Shipper:<br>
												Address:<br>
												City/State/Zip:<br>
												Phone#:
											</td>
											<td colspan="3">
												<b>18974HD<br>
												Home Depot /Bath Authority<br>
												75 Hawk Road<br>
												Warminster, PA 18974<br>
												800-430-3376</b>
											</td>
										</tr>
										</table>
									</td>
									<td colspan="3">
										<table border="1" cellpadding="3">
										<tr>
											<td bgcolor="silver" align="center" colspan="6">
												<FONT size="8" COLOR="black"><b>CONSIGNEE (to)</b></FONT>
											</td>
										</tr>
										'.$consigneeAllInfo.'
										</table>
									</td>
								</tr>
								<tr>
									<td bgcolor="silver" colspan="6">
										<b>BILL FREIGHT CHARGES TO (if different):</b>
									</td>
								</tr>
								<tr>
									<td colspan="6">
										<font size="8">Name__________________________________________Address__________________________FXF Acct#:______________________
City____________________________State_____________Zip________________________Phone#__________________________________</font>
									</td>
								</tr>
								<tr>
									<td colspan="6">
										<font size="8">RECEIVED, subject to individually determined rates or contracts that have been agreed upon in writing between the carrier and shipper, if applicable, otherwise to the rates,
classifications and rules that have been established by the carrier and are available to the shipper, on request, and to all applicable state and federal regulations.</font>
									</td>
								</tr>
								<tr>
									<td colspan="1" align="center">
										<b>ADDED SERVICES</b>
									</td>
									<td colspan="5">
										<font size="8">Home delivery</font>
									</td>
								</tr>
								<tr>
									<td colspan="6">
									<table border="1" cellpadding="3">
									<tr>
										<td colspan="1" width="50" align="center">
											Units
										</td>
										<td colspan="1" width="50" align="center">
											Type
										</td>
										<td colspan="1" width="50" align="center">
											Boxes
										</td>
										<td colspan="1" width="50" align="center">
											HM*
										</td>
										<td colspan="4" width="335" align="center">
											Description
										</td>
										<td colspan="1" width="80" align="center">
											NMFC Item #
										</td>
										<td colspan="1" width="50" align="center">
											Class
										</td>
									</tr>
									<tr>
										<td colspan="1" width="50">
											'.$units.'
										</td>
										<td colspan="1" width="50">
											SKID
										</td>
										<td colspan="1" width="50">
											'.$arr[0]['boxes'].'
										</td>
										<td colspan="1" width="50">
											
										</td>
										<td colspan="4" width="285">
											'.$descriptionHtml.'
										</td>
										<td colspan="1" width="80">
											159010-02
										</td>
										<td colspan="1" width="50">
											85
										</td>
									</tr>
									</table>
									</td>
								</tr>
								'.$redeliveryCharges.'
								<tr>
									<td colspan="6" align="left">
										<font size="9">SPECIAL INSTRUCTIONS :  If no delivery appointment has been made with the consignee within 1 business day of receiving the shipment, please contact Dreamline/Bath Authority at 866-731-2244 or email support@dreamline.com to assist with alternate contact information.
										</font>
									</td>
								</tr>
								<tr>
									<td colspan="6">
										<font size="8">* MARK “X” or “RQ” in the HM column to designate hazardous materials or reportable quantity as defined in DOT regulations.
SPECIAL INSTRUCTIONS: Call for delivery! Notify Consignee. Do not Double stack pallets. No additional accessorial authorized by shipper</font>
									</td>
								</tr>
								<tr>
									<td colspan="6">
										<font size="8">NOTE (I) Where the rate is dependent on value, shippers are required to state specifically in writing the agreed or declared value of the property as
follows:
“The agreed or declared value of the property is specifically stated by the shipper to be not exceeding ______per______
NOTE(II) Liability limitation for loss or damage on this shipment shall be applicable. See 49 US. C. - 14706(c)(1)(A) and (B)
NOTE(III) commodities requiring special or additional care or attention in handling or stowing must be so
marked and packaged as to ensure safe transportation with ordinary care. see sec. 2(e) of nMfc item 360</font>
									</td>
								</tr>
								<tr>
									<td colspan="6">
										<font size="8">Subject to Section 7 of conditions of applicable Bill of Lading. if this shipment is to be delivered to the consignee,
without recourse on the consignor, the consignor shall sign the following statement. The carrier may decline to
make delivery of this shipment without payment of freight and all other lawful charges.
consignor signature _______________________________________________________________________</font>
									</td>
								</tr>
								<tr>
									<td bgcolor="silver" colspan="3">
										<b>SHIPPER SIGNATURE / DATE</b>
									</td>
									<td bgcolor="silver" colspan="3">
										<b>CARRIER SIGNATURE / PICKUP DATE</b>
									</td>
								</tr>
								<tr>
									<td colspan="3">
										<font size="8">This is to certify that the above named materials are properly classified,
described, packaged, marked and labeled, and
are in proper condition for transportation according to the applicable
regulations of the DOT
<br><br>_________________________Shipper ________/'.$todaysYear.' Date</font>
									</td>
									<td colspan="3">
										<font size="8">Carrier acknowledges receipt of packages and required placards. 
Carrier certifies emergency response information was made available
and/or carrier has the U.S. DOT emergency response guidebook or
equivalent documentation in the vehicle.
<br><br>_____________________ Driver _______/'.$todaysYear.' Date</font>
									</td>
								</tr>
								</table>';
								if (isCarrierNameCorrect($originCarrierName))
								{
									/*if ($arr[0]['CustomerRef_FullName'] == "Amazon" && PrintCarrierForAmazonMode == '1')
									{
										$tblLogo = $tbl;
									} else
									{*/
										$carrierLogo = getCarrierLogo($originCarrierName);
										$tblLogo = $tbl.'<table cellpadding="3">
											<tr>
												<td>
													<font size="'.globalCarrierFont.'"><b>'.$originCarrierName.'</b></font><br>
													
												</td>
												<td>
													<img src="'.$carrierLogo.'" height="120px" width="340px">
												</td>
											</tr>
										</table>';
									/*}*/
								} else 
								{
									$tblLogo = $tbl;
								}
								$pdf->writeHTML($tblLogo, true, false, false, false, '');
								
								$pdf->AddPage();
								$pdf->SetFont('helvetica', '', 9);
								if (isCarrierNameCorrect($originCarrierName))
								{
									/*if ($arr[0]['CustomerRef_FullName'] == "Amazon" && PrintCarrierForAmazonMode == '1')
									{
										$tbl = $tbl;
									} else
									{*/
										$carrierLogo = getCarrierLogo($originCarrierName);
										$tbl.='<table cellpadding="3">
											<tr>
												<td>
													<font size="'.globalCarrierFont.'"><b>'.$originCarrierName.'</b></font>
													<font size="'.globalCarrierFont.'">Carrier Copy</font>
												</td>
												<td>
													<img src="'.$carrierLogo.'" height="130px" width="340px">
												</td>
											</tr>
										</table>';
									/*}*/
								}
								
								$pdf->writeHTML($tbl, true, false, false, false, '');

				$pdf = get_packing_list($arr, $pdf);				
				$pdf->Output($pdf_path, 'F');
				$result[] = $pdf_path2;
			if (!empty($result))
			{
				return $result;
			} else
			{
				error_msg("Can not generate bill of lading!");
				send_error();
			}
	}catch (Exception $e) {
		error_msg($e->getMessage());
		send_error();
	}
}

//Generate Bill of Lading pdf files
function searsBill($arr, $pro, $bol, $pro10, $barcodeBill, $originCarrierName)
{
	try{
			
			$barcodeBill = $barcodeBill.'.png';
			
			$PONumbers = array();
			$result = array();
			
			foreach ($arr as $row)
			{
				$PONumbers[] = $row['PONumber'];
			}
			$c = count($PONumbers);
			
				$pdf_path = "../PdfBill/".repPo($PONumbers[0])."Bill.pdf";
				$pdf_path2 = "PdfBill/".repPo($PONumbers[0])."Bill.pdf";
				
				$pdf_path = "../PdfBill/".repPo($PONumbers[0])."Bill.pdf";
				$pdf_path2 = "PdfBill/".repPo($PONumbers[0])."Bill.pdf";
				
				date_default_timezone_set('America/New_York');
				
				$todaysDate = date("n/j/Y");
				$todaysYear = date("Y");
				
				$shipToAddress = '';
				if (isset($arr[0]['ShipAddress_Addr1']) && !empty($row['ShipAddress_Addr1'])) $shipToAddress.=$row['ShipAddress_Addr1'].'<br>';
				if (isset($arr[0]['ShipAddress_Addr2']) && !empty($row['ShipAddress_Addr2'])) $shipToAddress.=$row['ShipAddress_Addr2'].'<br>';
				if (isset($arr[0]['ShipAddress_Addr3']) && !empty($row['ShipAddress_Addr3'])) $shipToAddress.=$row['ShipAddress_Addr3'].'<br>';
				if (isset($arr[0]['ShipAddress_Addr4']) && !empty($row['ShipAddress_Addr4'])) $shipToAddress.=$row['ShipAddress_Addr4'].'<br>';
				if (isset($arr[0]['ShipAddress_Addr5']) && !empty($row['ShipAddress_Addr5'])) $shipToAddress.=$row['ShipAddress_Addr5'].'<br>';
				if (isset($arr[0]['ShipAddress_City']) && !empty($row['ShipAddress_City'])) $shipToAddress.=$row['ShipAddress_City'].', ';
				if (isset($arr[0]['ShipAddress_State']) && !empty($row['ShipAddress_State'])) $shipToAddress.=$row['ShipAddress_State'].'<br>';
				if (isset($arr[0]['ShipAddress_PostalCode']) && !empty($row['ShipAddress_PostalCode'])) $shipToAddress.=$row['ShipAddress_PostalCode'].'<br>';
				if (isset($arr[0]['FOB']) && !empty($row['FOB'])) $shipToAddress.=$row['FOB'].'<br>';
				
				
				
				
				if (!isset($arr[0]['PALLETS']) || empty($arr[0]['PALLETS']))
				{
					$units = ceil($arr[0]['WEIGHT'] / 2500);
				} else
				{
					$units = $arr[0]['PALLETS'];
				}
				
				$descriptionHtml = 'PO#: '.$arr[0]['PONumber'].'; PART#: '.$arr[0]['PartNumber'].';<br>';
				$descriptionHtml.= get_description($arr, 4);
				
				if ($arr[0]['RESIDENTIAL'] == '1') $residential = '../Include/pictures/checkboxtrue.png'; else $residential = '../Include/pictures/checkboxfalse.png';
				if ($arr[0]['LIFTGATE'] == '1') $liftgate = '../Include/pictures/checkboxtrue.png'; else $liftgate = '../Include/pictures/checkboxfalse.png';
				if ($arr[0]['delivery_notification'] == '1') $delivery_notification = '../Include/pictures/checkboxtrue.png'; else $delivery_notification = '../Include/pictures/checkboxfalse.png';
					
				$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
				$pdf->SetPrintHeader(false);
				$pdf->SetPrintFooter(false);
				$pdf->SetMargins(5, 5);
				$pdf->SetAutoPageBreak(TRUE, 1);
				$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);


						
						$pdf->AddPage();
						$pdf->SetFont('helvetica', '', 9);
						$tbl = '
						<table align="center">
							<tr>
								<td>
									<img id="img1" src="../Include/pictures/SearsBlue.png">
								</td>
								<td colspan="3">
									<img id="img1" src="../Include/pictures/searsHoldings.png">
								</td>
								<td>
									<img id="img1" src="../Include/pictures/KkmartRed.png">
								</td>
							</tr>
						</table>
						
						<table border="1" style="font-weight: bold;" cellpadding="0">
							<tr>
								<td colspan="2" style="color: white; background-color: #000080;" align="center">
									BILL OF LADING
								</td>
							</tr>
							<tr>
								<td>
									<table border="1" cellpadding="2">
										<tr>
											<td colspan="4" align="center" style="background-color: silver;">
												Ship From
											</td>
										</tr>
										<tr>
											<td style="background-color: silver;" align="right">
												Name:
											</td>
											<td colspan="3">
												Bath Authority, LLC
											</td>
										</tr>
										<tr>
											<td style="background-color: silver;" align="right">
												Address:
											</td>
											<td colspan="3">
												75 Hawk Road
											</td>
										</tr>
										<tr>
											<td style="background-color: silver;" align="right">
												City/State/Zip:
											</td>
											<td colspan="3">
												Warminster, PA 18974
											</td>
										</tr>
										<tr>
											<td colspan="4" style="background-color: silver;" align="center">
												Consign To
											</td>
										</tr>
										<tr>
											<td style="background-color: silver;" align="right">
												Name:
											</td>
											<td colspan="3">
												'.$row['ShipAddress_Addr1'].'
											</td>
										</tr>
										<tr>
											<td style="background-color: silver;" align="right">
												Address:
											</td>
											<td colspan="3">
												'.$row['ShipAddress_Addr2'].'
											</td>
										</tr>
										<tr>
											<td style="background-color: silver;" align="right">
												City/State/Zip:
											</td>
											<td colspan="3">
												'.$row['ShipAddress_City'].' / '.$row['ShipAddress_State'].' / '.$row['ShipAddress_PostalCode'].'
											</td>
										</tr>
										<tr>
											<td colspan="4" style="background-color: silver;" align="center">
												Bill To
											</td>
										</tr>
										<tr>
											<td style="background-color: silver;" align="right">
												Type:
											</td>
											<td colspan="3" style="color: red;">
												Third Party
											</td>
										</tr>
										<tr>
											<td style="background-color: silver;" align="right">
												Name:
											</td>
											<td colspan="3">
												Demar Logistics
											</td>
										</tr>
										<tr>
											<td style="background-color: silver;" align="right">
												Address:
											</td>
											<td colspan="3">
												376 Lies Road
											</td>
										</tr>
										<tr>
											<td style="background-color: silver;" align="right">
												City/State/Zip:
											</td>
											<td colspan="3">
												Carol Stream, IL 60188
											</td>
										</tr>
									</table>
								</td>
								<td>
									<table border="1" cellpadding="2">
										<tr>
											<td colspan="3" align="right" style="background-color: silver;">
												Bill of Lading Number:
											</td>
											<td colspan="2">
												'.$bol.'
											</td>
										</tr>
										<tr>
											<td colspan="3" style="background-color: silver;" align="right">
												Ship Date:
											</td>
											<td	colspan="2">
												'.$todaysDate.'
											</td>
										</tr>
										<tr>
											<td colspan="3" style="background-color: silver;" align="right">
												Booking Number:
											</td>
											<td	colspan="2">
												N/A
											</td>
										</tr>
										<tr>
											<td colspan="3" style="background-color: silver;" align="right">
												Carrier SCAC:
											</td>
											<td	colspan="2">
												DELG
											</td>
										</tr>
										<tr>
											<td colspan="3" style="background-color: silver;" align="right">
												Trailer Number:
											</td>
											<td	colspan="2">
												N/A
											</td>
										</tr>
										<tr>
											<td colspan="3" style="background-color: silver;" align="right">
												Seal Number:
											</td>
											<td	colspan="2">
												N/A
											</td>
										</tr>
										<tr>
											<td colspan="3" style="background-color: silver;" align="right">
												Pro Number:
											</td>
											<td	colspan="2">
												'.$pro.'
											</td>
										</tr>
										<tr>
											<td colspan="5" style="background-color: silver;" align="center">
												Freight Terms:
											</td>
										</tr>
										<tr align="center">
											<td colspan="5" align="center">
												 <img id="img3" src="../Include/pictures/checkboxfalse.png" width="10" height="10"> Prepaid 
												 <img id="img3" src="../Include/pictures/checkboxfalse.png" width="10" height="10"> Collect 
												 <img id="img3" src="../Include/pictures/checkboxtrue.png" width="10" height="10"> Third Party 
											</td>
										</tr>
										<tr>
											<td colspan="5" style="background-color: silver;" align="center">
												Notes
											</td>
										</tr>
										<tr>
											<td colspan="5">
											</td>
										</tr>
										<tr>
											<td colspan="5">
											</td>
										</tr>
										<tr>
											<td colspan="5">
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						
						<table border="1" style="font-weight: bold;" cellpadding="2" align="center">
							<tr>
								<td colspan="2" style="background-color: silver;">
									Total Shipping Units:
								</td>
								<td colspan="2">
									'.$units.'
								</td>
								<td style="background-color: silver;">
									Type(s):
								</td>
								<td colspan="3">
									 <img id="img3" src="../Include/pictures/checkboxtrue.png" width="10" height="10"> Pallet/Skids 
									 <img id="img3" src="../Include/pictures/checkboxfalse.png" width="10" height="10"> Carton/Pieces 
								</td>
								<td colspan="2" style="background-color: #99CCFF;">
									LTL Only
								</td>
							</tr>
						</table>
						
						<table border="1" cellpadding="2" style="font-weight: bold;" align="center">
							<tr style="background-color: silver;">
								<td>
									Total Skids
								</td>
								<td>
									Total<br>Cartons
								</td>
								<td>
									Sears<br>Dept #
								</td>
								<td>
									Total<br>Weight
								</td>
								<td>
									Check<br>for Haz<br>Mat
								</td>
								<td colspan="3">
									Description (include PO# and Part #)
								</td>
								<td style="background-color: #99CCFF;">
									NMFC #
								</td>
								<td style="background-color: #99CCFF;">
									Class
								</td>
							</tr>
							<tr>
								<td>
									'.$units.'
								</td>
								<td>
									'.$arr[0]['boxes'].'
								</td>
								<td>
									683
								</td>
								<td>
									'.$arr[0]['WEIGHT'].'
								</td>
								<td>
									<img id="img3" src="../Include/pictures/checkboxfalse.png" width="10" height="10">
								</td>
								<td colspan="3">
									'.$descriptionHtml.'
								</td>
								<td>
									159010-02
								</td>
								<td>
									85
								</td>
							</tr>
							<tr>
								<td>
									'.$units.'
								</td>
								<td>
									'.$arr[0]['boxes'].'
								</td>
								<td style="background-color: silver;">
									
								</td>
								<td>
									'.$arr[0]['WEIGHT'].'
								</td>
								<td colspan="6" style="background-color: silver;">	
								</td>
							</tr>
						</table>
						
						<table border="1" cellpadding="2" style="font-weight: bold;">
							<tr>
								<td style="background-color: silver;" align="right">
									Shipper Name Printed:
								</td>
								<td colspan="2">
								</td>
								<td style="background-color: silver;" align="center">
									Date:
								</td>
								<td>
								</td>
							</tr>
							<tr>
								<td style="background-color: silver;" align="right">
									Shipper Signature:
								</td>
								<td colspan="4">
								</td>
							</tr>
							<tr>
								<td colspan="5">
									<i>This is to certify that the above named materials are properly classified, packaged, marked, and labeled; and are in<br>
proper condition for transportation according to the applicable regulations of the DOT and other enforcement agencies.</i>
								</td>
							</tr>
							<tr>
								<td style="background-color: silver; align="right"">
									Receiver Name Printed:
								</td>
								<td colspan="2">
								</td>
								<td style="background-color: silver;" align="center">
									Date:
								</td>
								<td>
								</td>
							</tr>
							<tr>
								<td style="background-color: silver;" align="right">
									Receiver Signature:
								</td>
								<td colspan="4">
								</td>
							</tr>
						</table>
						';
								
								
								$pdf->writeHTML($tbl, true, false, false, false, '');

				$pdf = get_packing_list($arr, $pdf);				
				$pdf->Output($pdf_path, 'F');
				$result[] = $pdf_path2;
			if (!empty($result))
			{
				return $result;
			} else
			{
				error_msg("Can not generate bill of lading!");
				send_error();
			}
	}catch (Exception $e) {
		error_msg($e->getMessage());
		send_error();
	}
}

//Generate Bill of Lading pdf files
function amazonDSBill($arr, $pro, $bol, $pro10, $barcodeBill, $originCarrierName)
{
	/*print_r('<pre>');
	print_r($arr);
	print_r('</pre>');
	die();*/
	try{
			
			$barcodeBill = $barcodeBill.'.png';
			
			$PONumbers = array();
			$result = array();
			
			foreach ($arr as $row)
			{
				$PONumbers[] = $row['PONumber'];
			}
			$c = count($PONumbers);
			
				$pdf_path = "../PdfBill/".repPo($PONumbers[0])."Bill.pdf";
				$pdf_path2 = "PdfBill/".repPo($PONumbers[0])."Bill.pdf";
				
				date_default_timezone_set('America/New_York');
				
				$todaysDate = date("n/j/Y");
				$todaysYear = date("Y");
				
				$address = '';
				$consignee = '';
				if (isset($arr[0]['ShipAddress_Addr1']) && !empty($row['ShipAddress_Addr1'])) $consignee = $row['ShipAddress_Addr1']; else $consignee = '';
				
				if (isset($arr[0]['ShipAddress_Addr2']) && !empty($row['ShipAddress_Addr2'])) $address.=$row['ShipAddress_Addr2'].', ';
				if (isset($arr[0]['ShipAddress_Addr3']) && !empty($row['ShipAddress_Addr3'])) $address.=$row['ShipAddress_Addr3'];
				
				if (isset($arr[0]['FOB']) && !empty($row['FOB'])) $shipperPhone=$row['FOB']; else $shipperPhone='';
				
				$cityStateZip = '';
				if (isset($arr[0]['ShipAddress_City']) && !empty($row['ShipAddress_City'])) $cityStateZip.=$row['ShipAddress_City'].', ';
				if (isset($arr[0]['ShipAddress_State']) && !empty($row['ShipAddress_State'])) $cityStateZip.=$row['ShipAddress_State'].', ';
				if (isset($arr[0]['ShipAddress_PostalCode']) && !empty($row['ShipAddress_PostalCode'])) $cityStateZip.=$row['ShipAddress_PostalCode'];
				
				if (!isset($arr[0]['PALLETS']) || empty($arr[0]['PALLETS']))
				{
					$units = ceil($arr[0]['WEIGHT'] / 2500);
				} else
				{
					$units = $arr[0]['PALLETS'];
				}
				
				$descriptionHtml = get_description($arr, 4);
				
				$quantity = 0;
				foreach ($arr as $ar)
				{
					$quantity = $quantity + $ar['quantity'];
				}
				
				if ($arr[0]['RESIDENTIAL'] == '1') $residential = '../Include/pictures/checkboxtrue.png'; else $residential = '../Include/pictures/checkboxfalse.png';
				if ($arr[0]['LIFTGATE'] == '1') $liftgate = '../Include/pictures/checkboxtrue.png'; else $liftgate = '../Include/pictures/checkboxfalse.png';
				if ($arr[0]['delivery_notification'] == '1') $delivery_notification = '../Include/pictures/checkboxtrue.png'; else $delivery_notification = '../Include/pictures/checkboxfalse.png';
				$carrier = extrCarrier($row['ShipMethodRef_FullName']);
				
				$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
				$pdf->SetPrintHeader(false);
				$pdf->SetPrintFooter(false);
				$pdf->SetMargins(11, 11);
				$pdf->SetAutoPageBreak(TRUE, 1);
				$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);


						
						$pdf->AddPage();
						$pdf->SetFont('helvetica', '', 8);
						$tbl = '
						<table width="670" border="0" cellpadding="8" cellspacing="0">
            <tr align="center" valign="center">
                <td align="center">

                    <table width="100%" border="1" cellpadding="3" cellspacing="0">
                        <tr align="center" valign="center">
                            <td width="50%" rowspan="6">
                                <table width="100%" cellpadding="2" cellspacing="0">
                                    <tr align="center">
                                        <td align="center"><img src="../Include/pictures/Amazon.png" alt="Amazon" width="200px" /></td>
                                    </tr>
                                    <tr>
                                        <td align="center"><font size="12"><b>BILL OF LADING</b></font></td>
                                    </tr>
                                    <tr>
                                        <td align="center"><font size="11"><b>Delivery Problem?</b></font></td>
                                    </tr>
                                    <tr>
                                        <td align="center"><font size="11">Contact Amazon at (866) 423-5353 or</font></td>
                                    </tr>
                                    <tr>
                                        <td align="center"><font size="11">LTL@amazon.com</font></td>
                                    </tr>
                                    <tr>
                                        <td align="center"><font size="11"><b>Undeliverable? Re-route to:</b></font></td>
                                    </tr>';
									if($arr[0]['CustomerRef_FullName'] != 'AmazonDS Canada')
									{
										$tbl.='<tr>
											<td align="left"><font size="9">&nbsp;&nbsp;Amazon Fulfillment Services</font></td>
										</tr>
										<tr>
											<td align="left"><font size="9">&nbsp;&nbsp;1600 Worldwide Blvd</font></td>
										</tr>
										<tr>
											<td align="left"><font size="9">&nbsp;&nbsp;Herbon, KY 41048-8639</font></td>
										</tr>';
									} else{
										$tbl.='<tr>
											<td align="left"><font size="9">&nbsp;&nbsp;Amazon.com.ca, Inc.</font></td>
										</tr>
										<tr>
											<td align="left"><font size="9">&nbsp;&nbsp;c/o Amazon Canada Fulfillment Services, Inc.</font></td>
										</tr>
										<tr>
											<td align="left"><font size="9">&nbsp;&nbsp;6363 Millcreek Drive</font></td>
										</tr>
										<tr>
											<td align="left"><font size="9">&nbsp;&nbsp;Mississauga, Ontario L5N 1L8</font></td>
										</tr>';
									}
                                $tbl.='</table>
                            </td>
                            <td width="50%" align="left"><font size="9"><b>Today`s Date: </b>'.$todaysDate.'</font></td>
                        </tr>
                        <tr valign="center" align="left">
                            <td><font size="9"><b>Shipper BOL Number # '.$pro.'</b></font></td>
                        </tr>
                        <tr valign="center" align="left">
                            <td><font size="9"><b>Amazon`s PO #: </b>'.checkPOBill($arr[0]['PONumber']).'</font></td>
                        </tr>
                        <tr valign="center" align="left">
                            <td><font size="9"><b>Shipper`s Internal Reference #: '.$bol.'</b></font></td>
                        </tr>
                        <tr align="center" valign="center">
                            <td height="120" valign="center">';
				if ($pro10 != 'none' && $barcodeBill != 'none' && $carrier == 'CEVA')
				{
					$tbl.= '<p><b><font size="16">CEVA Logistics</font></b></p>
							<img src="'.$barcodeBill.'" with="281" height="50">
							<p><b><font size="16">'.$pro10.'</font></b></p>';
				} else if ($pro10 != 'none' && $barcodeBill != 'none' && ($carrier == 'ESTES' || $carrier == 'ABF'))
				{
					$tbl.= '<p><b><font size="16">'.$carrier.'</font></b></p>
							<img src="'.$barcodeBill.'" with="281" height="50">
							<p><b><font size="16">'.$pro10.'</font></b></p>';
				} else
				{
					$tbl.= '<font size="16" color="grey"><br/>Place sticker here</font>';
				}
							
							
							$tbl.= '</td>
                        </tr>
                        <tr valign="center" align="left">
                            <td><font size="9"><b>Shipping Service: Economy (3-5 day)</b></font></td>
                        </tr>
                    </table>        
                </td>
            </tr>
            <tr align="center" valign="center">
                <td>

                    <table width="100%" border="1" cellpadding="3" cellspacing="0">
                        <!-- 1 -->
                        <tr align="center" valign="center">
                            <td bgcolor="#cfcfcf"><font size="9"><b>Ship From:</b></font></td>
                        </tr>
                        <tr align="center" valign="center">
                            <td>
                                <table>
                                    <tr valign="center">
                                        <td width="20%" align="left"><font size="9"><b>Drop Shipper Name</b></font></td>
                                        <td align="left"><font size="9">Bath Authority, LLC</font></td>
                                    </tr>
                                    <tr valign="center">
                                        <td width="20%" align="left"><font size="9"><b>Address</b></font></td>
                                        <td align="left"><font size="9">75 Hawk Road</font></td>
                                    </tr>
                                    <tr valign="center">
                                        <td width="20%" align="left"><font size="9"><b>City/State/Zip:</b></font></td>
                                        <td align="left"><font size="9">Warminster, PA 18974</font></td>
                                    </tr>
                                    <tr valign="center">
                                        <td width="20%" align="left"><font size="9"><b>Phone Number:</b></font></td>
                                        <td align="left"><font size="9">866-731-2244</font></td>
                                    </tr>
                                    <tr valign="center">
                                        <td width="20%" align="left"><font size="9"><b>Warehouse Contact:</b></font></td>
                                        <td align="left"><font size="9">Sergio Ratner</font></td>
                                    </tr>

                                </table>
                            </td>
                        </tr>            

                        <!-- 2 -->
                        <tr align="center" valign="center">
                            <td bgcolor="#cfcfcf"><font size="9"><b>Ship To:</b></font></td>
                        </tr>
                        <tr align="center" valign="center">
                            <td>
                                <table cellpadding="0" cellspacing="0">
                                    <tr valign="center">
                                        <td width="20%" align="left"><font size="9"><b>Customer Name:</b></font></td>
                                        <td><font size="9" align="left">'.$consignee.'</font></td>
                                    </tr>
                                    <tr valign="center">
                                        <td width="20%" align="left"><font size="9"><b>Address:</b></font></td>
                                        <td><font size="9" align="left">'.$address.'</font></td>
                                    </tr>
                                    <tr valign="center">
                                        <td width="20%" align="left"><font size="9"><b>City/State/Zip:</b></font></td>
                                        <td><font size="9" align="left">'.$cityStateZip.'</font></td>
                                    </tr>
                                    <tr valign="center">
                                        <td width="20%" align="left"><font size="9"><b>Phone Number:</b></font></td>
                                        <td align="left"><font size="9">'.$shipperPhone.'</font></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>

                        <!-- 3 -->
                        <tr align="center" valign="center">
                            <td bgcolor="#cfcfcf"><font size="9"><b>Bill To:</b></font></td>
                        </tr>
                        <tr align="center" valign="center">
                            <td>
                                <table>
                                    <tr valign="center">
                                        <td width="20%" align="left"><font size="9"><b>Name:</b></font></td>
                                        <td align="left"><font size="9">Amazom.com</font></td>
                                    </tr>
                                    <tr valign="center">
                                        <td width="20%" align="left"><font size="9"><b>Address:</b></font></td>
                                        <td align="left"><font size="9">P.O. Box 80683, Seattle, WA 98108</font></td>
                                    </tr>
                                    <tr valign="center">
                                        <td width="20%" align="left"><font size="9"><b>Account Number:</b></font></td>
                                        <td align="left"><font size="9"></font></td>
                                    </tr>
                                    <tr valign="center">
                                        <td width="20%" align="left"><font size="9"><b>Payment Terms:</b></font></td>
                                        <td align="left"><font size="9">Third Party</font></td>
                                    </tr>

                                </table>
                            </td>
                        </tr>

                    </table>        
                </td>
            </tr>
            <tr align="center" valign="center">
                <td>

                    <table width="100%" border="1" cellpadding="3" cellspacing="0">
                        <tr>
                            <td align="center" bgcolor="#cfcfcf"><font size="9"><b>Quantity</b></font></td>
                            <td align="center" bgcolor="#cfcfcf"><font size="9"><b>Type</b></font></td>
                            <td align="center" bgcolor="#cfcfcf"><font size="9"><b>No. of Boxes</b></font></td>
                            <td colspan="3" align="center" bgcolor="#cfcfcf"><font size="9"><b>Description</b></font></td>
                            <td align="center" bgcolor="#cfcfcf"><font size="9"><b>Hazmat?</b></font></td>
                            <td align="center" bgcolor="#cfcfcf"><font size="9"><b>Weight</b></font></td>
                        </tr>
                        <tr valign="top">
                            <td align="center">'.$units.'</td>
                            <td align="center">SKID</td>
                            <td align="center">'.$arr[0]['boxes'].'</td>
                            <td colspan="3">'.$descriptionHtml.'</td>
                            <td align="center">NO</td>
                            <td align="center">'.$arr[0]['WEIGHT'].'</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr align="left" valign="center">
                <td>
                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td width="40%" align="left">
                                <table width="100%" border="1" cellpadding="3" cellspacing="0">
                                    <tr>
                                        <td align="center" bgcolor="#cfcfcf"><font size="9"><b>Shipper</b></font></td>
                                    </tr>
                                    <tr>
                                        <td><font size="9"><b>Signature:</b></font></td>
                                    </tr>
                                    <tr>
                                        <td><font size="9"><b>Printed name:</b></font></td>
                                    </tr>
                                    <tr>
                                        <td><font size="9"><b>Tender Date:</b></font></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                    </tr>
                                </table>    
                            </td>
                            <td width="20%"></td>
                            <td width="40%" align="right">
                                <table width="100%" border="1" cellpadding="3" cellspacing="0">
                                    <tr>
                                        <td align="center" bgcolor="#cfcfcf"><font size="9"><b>Shipper</b></font></td>
                                    </tr>
                                    <tr>
                                        <td align="left"><font size="9"><b>Signature:</b></font></td>
                                    </tr>
                                    <tr>
                                        <td align="left"><font size="9"><b>Printed name:</b></font></td>
                                    </tr>
                                    <tr>
                                        <td align="left"><font size="9"><b>Pickup Date:</b></font></td>
                                    </tr>
                                    <tr>
                                        <td align="left"><font size="9"><b>Driver/Agent/Vehicle #:</b></font></td>
                                    </tr>
                                </table>    

                            </td>
                        </tr>
                    </table>

                </td>
            </tr>
            <tr>
                <td align="center">
                    <font size="9"><b>Disclaimer:</b> Terms & Conditions apply as set forth by Amazon.com and Carrier.</font>
                </td>
            </tr>
        </table>
						';
								/*if (isCarrierNameCorrect($originCarrierName))
								{
									$carrierLogo = getCarrierLogo($originCarrierName);
									$tblLogo = $tbl.'<table cellpadding="3">
										<tr>
											<td>
												<font size="'.globalCarrierFont.'"><b>'.$originCarrierName.'</b></font><br>
												
											</td>
											<td>
												<img src="'.$carrierLogo.'" height="120px" width="340px">
											</td>
										</tr>
									</table>';
								} else 
								{
									$tblLogo = $tbl;
								}
								$pdf->writeHTML($tblLogo, true, false, false, false, '');
								
								$pdf->AddPage();
								$pdf->SetFont('helvetica', '', 8);
								if (isCarrierNameCorrect($originCarrierName))
								{
									$carrierLogo = getCarrierLogo($originCarrierName);
									$tbl.='<table cellpadding="3">
										<tr>
											<td>
												<font size="'.globalCarrierFont.'"><b>'.$originCarrierName.'</b></font>
												<font size="'.globalCarrierFont.'">Carrier Copy</font>
											</td>
											<td>
												<img src="'.$carrierLogo.'" height="120px" width="340px">
											</td>
										</tr>
									</table>';
								}*/
								
								$pdf->writeHTML($tbl, true, false, false, false, '');
								$pdf->AddPage();
								$pdf->SetFont('helvetica', '', 8);
								$pdf->writeHTML($tbl, true, false, false, false, '');

				$pdf = get_packing_list($arr, $pdf);				
				$pdf->Output($pdf_path, 'F');
				$result[] = $pdf_path2;
			if (!empty($result))
			{
				return $result;
			} else
			{
				error_msg("Can not generate bill of lading!");
				send_error();
			}
	}catch (Exception $e) {
		error_msg($e->getMessage());
		send_error();
	}
}
?>