<?php

$path = $_SERVER['DOCUMENT_ROOT'];
require_once($path.'/db_connect/connect.php');



	if (isset($_REQUEST['id']) && !empty($_REQUEST['id']) && isset($_REQUEST['pro']) && !empty($_REQUEST['pro']))
	{
		$returnValue = 'used';
		$po = $_REQUEST['id'];
		$pro = $_REQUEST['pro'];
		$conn = Database::getInstance()->dbc;
		$res = getProCount($conn, $po, $pro);
		if ($res)
		{
			$proNumberCount = $res['proNumberCount'];
			if ($proNumberCount == 0) $returnValue = 'unused';
		} else 
		{
			$returnValue = 'Error on database query!';
		}
		$returnValue = json_encode($returnValue);
		print_r($returnValue);
		$conn = null;
	} else 
	{
		$returnValue = 'ID or PRO# is missing.';
		$returnValue = json_encode($returnValue);
		print_r($returnValue);
	}

function getProCount($conn, $po, $pro)
{
	try{
		$query = "
		SELECT COUNT([PRO]) as proNumberCount 
		FROM ".SHIPTRACKWITHLABEL_TABLE." 
		WHERE [PRO] = '".$pro."' AND 
		[ID] <> '".$po."' AND 
		[CARRIERNAME] = (SELECT [CARRIERNAME] FROM ".SHIPTRACKWITHLABEL_TABLE." WHERE [ID] = '".$po."')
		";
		$result = $conn->prepare($query);
		$result->execute();
		$result = $result->fetch(PDO::FETCH_ASSOC); 
		return $result;
	}
	catch(PDOException $e)
	{
		return false;
	}
}
?>