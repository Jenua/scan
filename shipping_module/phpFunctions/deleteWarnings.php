<?php
require('functionsDB.php');
require('functionsExtr.php');
$path = $_SERVER['DOCUMENT_ROOT'];
require_once($path.'/db_connect/connect.php');

$connect = Database::getInstance()->dbc;

if( !empty($_REQUEST['id']) and !empty($_REQUEST['po']) and !empty($_REQUEST['user']) ) {	
	$po = $_REQUEST['po'];
	$id = $_REQUEST['id'];
	$user = $_REQUEST['user'];
	$message = "User ".$user." removed incompatible product warning.";

	if( $id != '' and $user != '' ) {
		//$query  = "DELETE FROM [warnings_log] WHERE [id_warnings_log] = ".$connect->quote($id)." AND [PONumber] = ".$connect->quote($po).";";
		$query = "UPDATE [orders_statuses] SET isActive = 0 WHERE id = ".$connect->quote($id)." AND [PONumber] = ".$connect->quote($po)."; ";
		$query .= "INSERT INTO [log] ([po],[message],[DATE]) VALUES (".$connect->quote($po).", ".$connect->quote($message).", GETDATE());";
		
		try {
			
			$result = execQuery($connect, $query);
			echo "OK";
			
		} catch ( Exception $e ) {
			echo $e->getMessage();
		}
	} else {
		echo "Error: empty warning ID";
	}
	
	
} else {
	echo "Error: empty warning ID or PONumber";
}

unset( $connect );
 