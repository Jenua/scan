<?php
function remove_G($value)
{
	if (substr($value,0,1)=='G')
	{
		$codelen = strlen($value) -1; 
		$value = substr($value, 1, $codelen);	
	}
	return $value;
}

function getPdfParams( $arr ) {
	$res = '<tr>';
	$symbols = array( 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K' );
	$length = count($arr);
	
	foreach( $arr as $k => $v ) {
		$v = trim($v);
		$first_char = substr($v, 0, 2);
		$length = strlen($v);
		$other_char = substr($v, 3, $length-2);
		/*print_r($v.'<br>');
		print_r($first_char.'<br>');
		print_r($other_char.'<br>');
		die();*/
		//$res .= "<td><b>".$symbols[$k].":</b> ".$v."</td>";
		$res .= "<td><font size='10'><b>".$first_char."</b>".$other_char."</font></td>";
		if( $k%2 == 1 && $k < $length-1 ) {
			$res .= "</tr><tr>";
		} else if( $k%2 == 0 && $k == $length-1 ) {
			$res .= "<td>&nbsp;</td>";
		}
	}
	$res .= "</tr>";
	return $res;
}

function crypto_rand_secure($min, $max)
{
    $range = $max - $min;
    if ($range < 1) return $min; // not so random...
    $log = ceil(log($range, 2));
    $bytes = (int) ($log / 8) + 1; // length in bytes
    $bits = (int) $log + 1; // length in bits
    $filter = (int) (1 << $bits) - 1; // set all lower bits to 1
    do {
        $rnd = hexdec(bin2hex(openssl_random_pseudo_bytes($bytes)));
        $rnd = $rnd & $filter; // discard irrelevant bits
    } while ($rnd >= $range);
    return $min + $rnd;
}

function getToken($length)
{
    $token = "";
    $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    $codeAlphabet.= "abcdefghijklmnopqrstuvwxyz";
    $codeAlphabet.= "0123456789";
    $max = strlen($codeAlphabet) - 1;
    for ($i=0; $i < $length; $i++) {
        $token .= $codeAlphabet[crypto_rand_secure(0, $max)];
    }
    return $token;
}

function checkPO($po, $ref, $original_po)
{
	$po_temp = strtoupper($po);
	if (is_in_str($po_temp, '_SPLIT_'))
	{
		$poArray = explode('_SPLIT_', $po_temp);
		$po = $poArray[0];
	}
	
	if (is_in_str($ref, 'combine_') || is_in_str($ref, 'COMBINE_'))
	{
		$po = $original_po;
	}
	return $po;
}

function checkPOBill($po)
{
	$po_temp = strtoupper($po);
	if (is_in_str($po_temp, '_SPLIT_'))
	{
		$poArray = explode('_SPLIT_', $po_temp);
		$po = $poArray[0];
	}
	return $po;
}

function isCanadaState($state, $country)
{
	$returnVal = false;
	$canada_states = array('AB', 'BC', 'MB', 'NB', 'NL', 'NS', 'NT', 'NU', 'ON', 'PE', 'QC', 'SK', 'YT');
	if (isset($country) && !empty($country))
	{
		if ($country == 'CA' || $country == 'CAN') $returnVal = true;
	}
	if (isset($state) && !empty($state))
	{
		if (in_array($state, $canada_states)) $returnVal = true;
	}
	return $returnVal;
}

function isHI_AL($state)
{
	$returnVal = false;
	if (isset($state) && !empty($state))
	{
		if ($state == 'AK' || $state == 'HI') $returnVal = true;
	}
	return $returnVal;
}

function extrAmazonDate($memo)
{
	$memo = explode(",", $memo);
	if (isset($memo[7]) && !empty($memo[7])) $date1 = $memo[7]; else $date1 = '';
	if (isset($memo[8]) && !empty($memo[8])) $date2 = $memo[8]; else $date2 = '';
	$date3 = $date1.' - '.$date2;
	return $date3;
}

function validatePro($value)
{
	if (isset($value) && !empty($value))
	{
		return $value;
	} else 
	{
		error_msg("PRO# data needed!");
		send_error();
	}
	//if (@preg_match("/[0-9]{9}[-][0-9]{1}$/",$pro))
}

function isCarrierNameCorrect($originCarrierName)
{
	if ($originCarrierName != 'none' && is_in_str($originCarrierName, "N/A") == false && $originCarrierName != 'Shipping Ground')
	{
		$returnValue = true;
	} else {
		$returnValue = false;
	}
	return $returnValue;
}

//get carrier logo
function getCarrierLogo($carrier)
{
	$carrier = extrCarrier($carrier);
	switch ($carrier)
	{
	case "FEDEX":
		$src = '../Include/pictures/fedex.png';
        break;
	case "RL":
		$src = '../Include/pictures/rl.png';
        break;
	case "YRC":
		$src = '../Include/pictures/yrc.png';
        break;
	case "ESTES":
		$src = '../Include/pictures/estes.png';
        break;
	case "ABF":
		$src = '../Include/pictures/abf.png';
        break;
	case "CEVA":
		$src = '../Include/pictures/ceva.png';
        break;
	case "EMPIRE":
		$src = '../Include/pictures/empire.PNG';
        break;
	case "NEW ENGL":
		$src = '../Include/pictures/newengl.png';
        break;
	case "ODFL":
		$src = '../Include/pictures/odfl.png';
        break;
	case "SEKO":
		$src = '../Include/pictures/seko.png';
        break;
	case "UPS":
		$src = '../Include/pictures/ups.png';
        break;
	case "YRC":
		$src = '../Include/pictures/yrc.png';
        break;
	default:
		$src = '../Include/pictures/noImage.png';
		break;
	}
	return $src;
}


//Extract carrier name from value
function extrCarrier($value)
{
	$result = false;
		if (is_in_str($value, "FEDEX") != false)
		{
			$result = "FEDEX";
		} else if (is_in_str($value, "RL") != false)
		{
			$result = "RL";
		} else if (is_in_str($value, "CEVA") != false)
		{
			$result = "CEVA";
		} else if (is_in_str($value, "YELLOW") != false || is_in_str($value, "YRC") != false)
		{
			$result = "YRC";
		} else if (is_in_str($value, "ESTES") != false)
		{
			$result = "ESTES";
		} else if (is_in_str($value, "NEW ENGL") != false)
		{
			$result = "NEW ENGL";
		} else if (is_in_str($value, "ODFL") != false)
		{
			$result = "ODFL";
		} else if (is_in_str($value, "SEKO") != false)
		{
			$result = "SEKO";
		} else if (is_in_str($value, "UPS") != false)
		{
			$result = "UPS";
		} else if (is_in_str($value, "EMPIRE") != false)
		{
			$result = "EMPIRE";
		} else if (is_in_str($value, "RoadRunner") != false)
		{
			$result = "Roadrunner";
		} else if (is_in_str($value, "CONWAY") != false)
		{
			$result = "CONWAY";
		} else if (is_in_str($value, "Zenith") != false)
		{
			$result = "ZEFL";
		} else if (is_in_str($value, "Watkins") != false)
		{
			$result = "WKSH";
		} else if (is_in_str($value, "HomeDirect") != false)
		{
			$result = "BVLC";
		} else if (is_in_str($value, "Non-Stop") != false)
		{
			$result = "NDLV";
		} else if (is_in_str($value, "Pyle") != false)
		{
			$result = "PYLE";
		} else if (is_in_str($value, "Averitt") != false)
		{
			$result = "AVRT";
		} else if (is_in_str($value, "Lakeville") != false)
		{
			$result = "LKVL";
		} else if (is_in_str($value, "ABF") != false)
		{
			$result = "ABF";
		} else if (is_in_str($value, "Land Air") != false)
		{
			$result = "LAXV";
		} else if (is_in_str($value, "Northwest") != false)
		{
			$result = "NFXR";
		} else if (is_in_str($value, "Southeastern Freight") != false)
		{
			$result = "SEFL";
		} else if (is_in_str($value, "Styline") != false)
		{
			$result = "STYH";
		} else if (is_in_str($value, "SunBelt") != false)
		{
			$result = "SBFE";
		} else if (is_in_str($value, "USF Holland") != false)
		{
			$result = "HMES";
		} else if (is_in_str($value, "USF Reddaway") != false)
		{
			$result = "RETL";
		} else if (is_in_str($value, "Wilson") != false)
		{
			$result = "WTVA";
		} else if (is_in_str($value, "Wiseway") != false)
		{
			$result = "WWMF";
		} else if (is_in_str($value, "NSD") != false)
		{
			$result = "NSD";
		} else $result = $value;
	return $result;
}


//Extract Fedex service type
function extrFedexServiceType($value)
{
	$result = false;
		if (is_in_str($value, "ECONOMY") != false)
		{
			$result = "FEDEX_FREIGHT_ECONOMY";
		} else if (is_in_str($value, "PRIORITY") != false)
		{
			$result = "FEDEX_FREIGHT_PRIORITY";
		} else $result = $value;
	return $result;
}

//replace bad symbols in PO with good :)
function repPo($po)
{
$bad = array('/', ' ', '#', '%', '&', '{', '}', '<', '>', '*', '?', '/', '$', '!', "'", '"', ':', '@', '+', '`', '|', '=', '­');
	$good = "_";
	$repPo = str_replace($bad, $good, $po);
	return $repPo;
}

/*
function getNumberOfPallets($arr)
{
    $MC_SKU = getMasterCartonableSKUs();    //list of SKUs can be packed in master carton
    
    $numberOfMC = 0;    //counter of master cartons
    
    $MasterGroupIDKEYs = [];    //list of products in master carton
    
    if($MC_SKU)
    {
        foreach($arr as $key=>$item)
        {
            $isPMC = isProductMasterCartonamble($arr, $MC_SKU, $item['GroupIDKEY']);    //if all items of the product are in the list
            if($isPMC)
            {
                if(!in_array($item['GroupIDKEY'], $MasterGroupIDKEYs))
                {
                    $numberOfMC++;  //increment counter
                    $MasterGroupIDKEYs[]= $item['GroupIDKEY'];
                }
                $arr[$key]['master_carton'] = true;    //mark item as master carton
            }
        }
        
        foreach($arr as $key=>$item)
        {
            $arr[$key]['numberOfMC'] = $numberOfMC;
        }
    }
    
    
    die('stop');
    
    return $arr;
}

function isProductMasterCartonamble($arr, $MC_SKU, $GroupIDKEY)
{
    $result = true;
    
    foreach($arr as $key=>$item)
    {
        if($item['GroupIDKEY'] == $GroupIDKEY)
        {
            if(!in_array($item['DL_SKU'], $MC_SKU)) 
            {
                $result = false;
            }
        }
    }
    if($result == true) echo 'true';
    
    return $result;
}
*/

//Count boxes for each PO number
function boxes($arr)
{
	try{
		$c = count($arr);
		for($i=0; $i<$c; $i++)
		{
			$row = $arr[$i];
			$PONumber = $row['PONumber'];
			$boxes = 0;
			foreach ($arr as $row2)
			{
				if ($row2['PONumber'] == $PONumber)
				{
					if (($row2['ProductCode']=='GSHST-01-TK') || 
					($row2['ProductCode']=='GSHST-01-PL'))
					{
						if((integer)$row2['quantity']<=5)
						{
							$qua = 1;
						} else 
						{
							$qua = ceil((integer)$row2['quantity'] / 5);
						}
						$boxes += $qua;
					}else 
					{
						$qua = (integer)$row2['quantity'];
						$boxes += $qua;
					}
				}
			}
			$row['boxes'] = $boxes;
			$arr[$i] = $row;
		}
		/*print_r($arr);
		die();*/
		return $arr;
	}catch (Exception $e) {
		error_msg($e->getMessage());
		send_error();
	}
}

function extrCountryCode($zip)
{
	if(preg_match( "/[0-9]{5}\-[0-9]{4}/", $zip ) || preg_match( "/[0-9]{5}/", $zip ))
	{
		return 'US';
	} else
	{
		return 'CA';
	}
}

//Divide records which has quantity more than 1 to separate records and add box numbers
function dublic($arr)
{
	try{
		$PONumbers = array();
		$result = array();
		$arr2 = $arr;
		
		foreach ($arr as $row)
		{
			$PONumbers[] = $row['PONumber'];
		}
		$PONumbers = array_unique($PONumbers);
		sort($PONumbers);
		$c = count($PONumbers);
		
		for ($i=0; $i<$c; $i++)
		{
			$last = 0;
			foreach ($arr2 as $row2)
			{
				if ($row2['PONumber'] == $PONumbers[$i])
				{
					if (($row2['ProductCode']=='GSHST-01-TK') || 
					($row2['ProductCode']=='GSHST-01-PL'))
					{
						if((integer)$row2['quantity']<=5)
						{
							$qua = 1;
						} else 
						{
							$qua = ceil((integer)$row2['quantity'] / 5);
						}
					} else 
					{
						$qua = (integer)$row2['quantity'];
					}
					$left_qua = $row2['quantity'];
					for ($j=$last; $j<($qua+$last); $j++)
					{
						if (($row2['ProductCode']=='GSHST-01-TK') || ($row2['ProductCode']=='GSHST-01-PL'))
						{
							//$quantity_in_box = $row2['quantity'] - 5*($qua - ($j+1));
							if ($left_qua  >= 5)
							{
								$quantity_in_box = 5;
								$left_qua = $left_qua - $quantity_in_box;
							} else 
							{
								$quantity_in_box = $left_qua;
							}
						} else
						{
							$quantity_in_box = 1;
						}
						if(isset($row2['UPC'])) $UPC = $row2['UPC']; else $UPC = null;
						if(isset($row2['SKU'])) $SKU = $row2['SKU']; else $SKU = null;
						if(isset($row2['collection_name'])) $collection_name = $row2['collection_name']; else $collection_name = null;
						if(isset($row2['drawing'])) $drawing = $row2['drawing']; else $drawing = null;
						if(isset($row2['dimensions'])) $dimensions = $row2['dimensions']; else $dimensions = null;
						if(isset($row2['lowes_mode'])) $lowes_mode = $row2['lowes_mode']; else $lowes_mode = null;
						if(isset($row2['amazon_quantity'])) $amazon_quantity = $row2['amazon_quantity']; else $amazon_quantity = null;
						
						$res = array(
						'RefNumber' => $row2['RefNumber'],
						'PONumber' => $row2['PONumber'],
						'ProductCode' => $row2['ProductCode'],
						'Descr' => $row2['Descr'],
						'CustomerRef_FullName' => $row2['CustomerRef_FullName'],
						'ShipMethodRef_FullName' => $row2['ShipMethodRef_FullName'],
						'ItemRef_FullName' => $row2['ItemRef_FullName'],
						'TxnLineID' => $row2['TxnLineID'],
						'GroupIDKEY' => $row2['GroupIDKEY'],
						'DL_SKU' => $row2['DL_SKU'],
						'amazon_quantity' => $amazon_quantity,
						'ItemGroupRef_FullName' => $row2['ItemGroupRef_FullName'],
						'Prod_desc' => $row2['Prod_desc'],
						'item_quantity' => $row2['item_quantity'],
						'quantity' => $row2['quantity'],
                                                'group_quantity' => $row2['group_quantity'],
						'ShipAddress_Addr1' => $row2['ShipAddress_Addr1'],
						'ShipAddress_Addr2' => $row2['ShipAddress_Addr2'],
						'ShipAddress_Addr3' => $row2['ShipAddress_Addr3'],
						'ShipAddress_Addr4' => $row2['ShipAddress_Addr4'],
						'ShipAddress_Addr5' => $row2['ShipAddress_Addr5'],
						'ShipAddress_City' => $row2['ShipAddress_City'],
						'ShipAddress_State' => $row2['ShipAddress_State'],
						'ShipAddress_Country' => $row2['ShipAddress_Country'],
						'ShipAddress_PostalCode' => $row2['ShipAddress_PostalCode'],
						'FOB' => $row2['FOB'],
						'TimeCreated' => $row2['TimeCreated'],
						'Memo' => $row2['Memo'],
						'CARRIERNAME' => $row2['CARRIERNAME'],
						'WEIGHT' => $row2['WEIGHT'],
						'PALLETS' => $row2['PALLETS'],
						'boxes' => $row2['boxes'],
						'quantity_in_box' => $quantity_in_box,
						'SERIAL_REFERENCE' => $row2['SERIAL_REFERENCE'],
						'RESIDENTIAL' => $row2['RESIDENTIAL'],
						'LIFTGATE' => $row2['LIFTGATE'],
						'delivery_notification' => $row2['delivery_notification'],
						'itemWeight' => $row2['itemWeight'],
						'boxNumber' => $j+1,
						'original_po' => $row2['original_po'],
						'UPC' => $UPC,
						'SKU' => $SKU,
						'collection_name' => $collection_name,
						'drawing' => $drawing,
						'dimensions' => $dimensions,
						'lowes_mode' => $lowes_mode,
						'ASN' => $row2['ASN']);
						$result[] = $res;
					}
					$last = $j;
				}
			}
		}
		/*print_r($result);
		die();*/
		return $result;
	}catch (Exception $e) {
		error_msg($e->getMessage());
		send_error();
	}
}

//Extract single product code from ItemRef_FullName field
function extrSinglePc($value)
{
    $pieces = explode(":", $value);
    $index = count($pieces)-1;
    $ProductCode = $pieces[$index];
    if (isset($ProductCode) && !empty($ProductCode))
    {
	$value = $ProductCode;
    }
    return $value;
}

//Extract product code from ItemRef_FullName field
function extrPc($arr)
{
	try{
		$c = count($arr);
		for ($i=0; $i<$c; $i++)
		{
			$row = $arr[$i];
			$value = $row['ItemRef_FullName'];
			$pieces = explode(":", $value);
			$index = count($pieces)-1;
			$ProductCode = $pieces[$index];
			if (isset($ProductCode) && !empty($ProductCode))
			{
				$row['ItemRef_FullName'] = $ProductCode;
				$arr[$i] = $row;
			} else
			{
				data_error_msg("ItemRef_FullName", $row['ItemRef_FullName'], "Can not get product code!");
				unset($arr[$i]);
				send_error();
			}
		}
	if (!empty($arr))
	{
		sort($arr);
		return $arr;
	} else
	{
		error_msg("Can not get any product codes!");
		send_error();
	}
	}catch (Exception $e) {
		error_msg($e->getMessage());
		send_error();
	}
}

//Extract description from ItemRef_FullName field
function extrDescr($arr)
{
	try{
		$c = count($arr);
		for ($i=0; $i<$c; $i++)
		{
			$row = $arr[$i];
			$value = $row['ItemRef_FullName'];
			$pieces = explode(":", $value);
			$Descr = $pieces[0];
			if (isset($Descr) && !empty($Descr))
			{
				$row['Descr'] = $Descr;
				$arr[$i] = $row;
			} else
			{
				data_error_msg("ItemRef_FullName", $row['ItemRef_FullName'], "Can not get description!");
				unset($arr[$i]);
				send_error();
			}
		}
		if (!empty($arr))
		{
			sort($arr);
			return $arr;
		} else
		{
			error_msg("Can not extract any descriptions!");
			send_error();
		}
	}catch (Exception $e) {
		error_msg($e->getMessage());
		send_error();
	}
}

//Extract store number from CustomerRef_FullName field
function extrStoreNum($arr)
{
	try{
		$c = count($arr);
		for ($i=0; $i<$c; $i++)
		{
			$row = $arr[$i];
			$value = $row['CustomerRef_FullName'];
			$len = strlen($value);
			$first = $len - 4;
			$Store = substr($value, $first, $len);
			if (isset($Store) && !empty($Store))
			{
				$row['Store'] = $Store;
				$arr[$i] = $row;
			} else
			{
				data_error_msg("CustomerRef_FullName", $row['CustomerRef_FullName'], "Can not get store number!");
				unset($arr[$i]);
				send_error();
			}
		}
		if (!empty($arr))
		{
			sort($arr);
			return $arr;
		} else
		{
			error_msg("Can not extract any store numbers!");
			send_error();
		}
	}catch (Exception $e) {
		error_msg($e->getMessage());
		send_error();
	}
}

//Extract date from TimeCreated field
function extrDate($arr)
{
	try{
		$c = count($arr);
		for ($i=0; $i<$c; $i++)
		{
			$row = $arr[$i];
			$value = $row['TimeCreated'];
			$date = new DateTime($value);
			$Date = $date->format('Y-m-d');
			if (isset($Date) && !empty($Date))
			{
				$row['Date'] = $Date;
				$arr[$i] = $row;
			} else
			{
				data_error_msg("TimeCreated", $row['TimeCreated'], "Can not get date!");
				unset($arr[$i]);
				send_error();
			}
		}
		if (!empty($arr))
		{
			sort($arr);
			return $arr;
		} else
		{
			error_msg("Can not extract any dates!");
			send_error();
		}
	}catch (Exception $e) {
		error_msg($e->getMessage());
		send_error();
	}
}

//Extract store name from CustomerRef_FullName field
function extrStore($value)
{
		if (is_in_str($value, "Lowe's") != false || is_in_str($value, "Lowe`s") != false)
		{
			$result = "Lowe's";
		} else if (is_in_str($value, "Amazon") != false && $value != "AmazonDS" && $value != "Amazon.Canada" && $value != "AmazonDS Canada")
		{
			$result = "Amazon";
		} else if (is_in_str($value, "AmazonDS") != false)
		{
			$result = "AmazonDS";
		} else if (is_in_str($value, "Amazon.Canada") != false)
		{
			$result = "Amazon.Canada";
		} else if (is_in_str($value, "The Home Depot") != false)
		{
			$result = "Depot";
		} else if (is_in_str($value, "C & R") != false)
		{
			$result = "C & R";
		} else if (is_in_str($value, "MENARDS") != false)
		{
			$result = "MENARDS";
		} else if (is_in_str($value, "HomeClick") != false)
		{
			$result = "HomeClick";
		} else if (is_in_str($value, "Sears") != false)
		{
			$result = "Sears";
		} else if (is_in_str($value, "Your other Warehouse") != false)
		{
			$result = "YOW";
		} else if (is_in_str($value, "Wayfair") != false)
		{
			$result = "Wayfair";
		} else if (is_in_str($value, "Ferguson") != false)
		{
			$result = "Ferguson";
		} else $result = $value;
	return $result;
}



//Is substring in string
function is_in_str($str,$substr)
{
	$str1 = strtolower($str);
	$substr1 = strtolower($substr);
	if (!empty($str1) && !empty($substr1))
	{
		$result = strpos($str1, $substr1);
	} else 
	{
		$result = FALSE;
	}
	
	if ($result === FALSE) return false; else return true;
}

//Delete oldest file from 10 files
function deletfile($directory, $anof) 
{
	/*$anof = 50000;*/	//$anof - allowable number of files
	// Grab all files from the desired folder
	$files = @glob( $directory.'*.*' );

	// Sort files by modified time, latest to earliest
	// Use SORT_ASC in place of SORT_DESC for earliest to latest
	array_multisort(
	@array_map( 'filemtime', $files ),
	SORT_NUMERIC,
	SORT_ASC,
	$files
	);
	$cf = count($files);
	if($cf > $anof)
	{
		$ftdn = $cf - $anof;
		for ($i = 0; $i <= $ftdn; $i++)
		{
			@unlink($files[$i]);
		}
	}
}

//Extract customer from ShipAddress_Addr1 field
function extrShipAddress_Addr1($value)
{
		if (is_in_str($value, "Mygofer") != false)
		{
			$result = "Mygofer";
		} else if (is_in_str($value, "Sears") != false)
		{
			$result = "Sears";
		} else $result = $value;
	return $result;
}
?>