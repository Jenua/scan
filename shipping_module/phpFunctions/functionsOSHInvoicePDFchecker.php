<?php
//die('stop');
//ini_set('max_execution_time', 7200);
//ini_set("memory_limit", "1000M");
//Include files

if ( isset($_REQUEST['po']) && !empty($_REQUEST['po']) ) {
    main_check( $_REQUEST['po'] );
} else {
    echo json_encode( array("error" => 1) );
}

function repPo($po)
{
    $bad = array('/', ' ', '#', '%', '&', '{', '}', '<', '>', '*', '?', '/', '$', '!', "'", '"', ':', '@', '+', '`', '|', '=');
    $good = "_";
    $repPo = str_replace($bad, $good, $po);
    return $repPo;
}

function main_check( $po ) {
        $invurl = "PdfInvoice/".repPo($po)."_invoice.pdf";

        $inv = file_exists("../".$invurl);

        echo json_encode( array( "error"   => 0                  
                               , "invoice" => $inv ? $invurl : ""
                               ));
}