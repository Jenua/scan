<?php
//die('stop');
//ini_set('max_execution_time', 7200);
//ini_set("memory_limit", "1000M");
//Include files

if (isset($_REQUEST['po']) && !empty($_REQUEST['po']) && isset($_REQUEST['txnlineid']) && !empty($_REQUEST['txnlineid'])) {
    main_check( '', $_REQUEST['po'], $_REQUEST['txnlineid']);
} else if(isset($_REQUEST['po']) && !empty($_REQUEST['po']) && isset($_REQUEST['type']) && !empty($_REQUEST['type'])) {
    main_check( $_REQUEST['type'], $_REQUEST['po'], '');    
} else {
    echo json_encode( array("error" => 1) );
}

function repPo($po)
{
    $bad = array('/', ' ', '#', '%', '&', '{', '}', '<', '>', '*', '?', '/', '$', '!', "'", '"', ':', '@', '+', '`', '|', '=');
    $good = "_";
    $repPo = str_replace($bad, $good, $po);
    return $repPo;
}

function main_check( $type, $po, $tl ) {
    
    if( $type == 'bol' ) {
        $bol_url = "Pdf/".repPo($po)."hddc_bol.pdf";

        $bol = file_exists("../".$bol_url);

        echo json_encode( array( "error"   => 0                  
                               , "bol" => $bol ? $bol_url : ""
                               ));
        
    } else {
        $label_url   = "Pdf/".repPo($po)."flyer_".$tl.".pdf";
        $barcode_url = "Pdf/".repPo($po)."barcodes_".$tl.".pdf";

        $label   = file_exists("../".$label_url);
        $barcode = file_exists("../".$barcode_url);

        echo json_encode( array( "error"   => 0
                               , "label"   => $label   ? $label_url   : ""
                               , "barcode" => $barcode ? $barcode_url : ""
                               ));
        
    }
    
 
}