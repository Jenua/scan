<?php
$path = $_SERVER['DOCUMENT_ROOT'];
require_once($path.'/db_connect/connect.php');

if(!empty($_REQUEST['user_id']))
{
    try{
        $conn = Database::getInstance()->dbc;
        $query = "SELECT [User].[id]
      FROM [dbo].[User]
      LEFT JOIN [dbo].[user_permission_matches] ON [user_permission_matches].[user_id] = [User].[id]
      LEFT JOIN [dbo].[user_permission_modules] ON [user_permission_modules].[permission_id] = [user_permission_matches].[permission_id]
      LEFT JOIN [dbo].[permissions] ON [user_permission_modules].[permission_id] = [permissions].[id]
      where
            [User].[id] = ".$conn->quote($_REQUEST['user_id'])."
            AND [user_permission_modules].[module_name] = 'LogisticsHub'
            AND [permissions].[name] = 'Internal Sync'";
        $result = $conn->prepare($query);
        $result->execute();
        $result = $result->fetch(PDO::FETCH_ASSOC);
    }catch (Exception $e) {
            print_r('Can not check user permissions!');
    }
    
    if($result['id'] == $_REQUEST['user_id'])
    {
        runSync();
    } else {
        print_r('You do not have permission to execute the internal sync. Please consult the IT team for access.');
    }
} else 
{
    print_r('Please log in and try again.');
}

function runSync()
{
	try{
                $conn = Database::getInstance()->dbc;
                $query = "EXEC [dbo].[A_SYNC_ORDERS31_TO_ORDERS_V1]";
		$result = $conn->prepare($query);
		$result->execute();
		if ($result) 
                {
                    print_r('Sync completed.');
                }
	}catch (Exception $e) {
		print_r('Can not run sync!');
	}
}