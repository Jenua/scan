<?php
$path = $_SERVER['DOCUMENT_ROOT'];
require_once($path.'/db_connect/connect.php');

	if (isset($_REQUEST['id']) && !empty($_REQUEST['id']))
	{
		$id = $_REQUEST['id'];
		$conn = Database::getInstance()->dbc;
		$res = getRow($conn, $id);
		//print_r($res);
		if ($res) 
		{
			$lift = $res['LIFTGATE'];
			$resi = $res['RESIDENTIAL'];
			$delivery_notification = $res['delivery_notification'];
			if ($lift != '1') $lift = '0';
			if ($resi != '1') $resi = '0';
			if ($delivery_notification != '1') $delivery_notification = '0';
			$returnValue = array('lift' => $lift, 'resi' => $resi/*, 'delivery_notification' => $delivery_notification*/);
			$returnValue = json_encode($returnValue);
			print_r($returnValue);
		} else 
		{
			$returnValue = 'Can not get LIFTGATE or RESIDENTIAL from DB.';
			$returnValue = json_encode($returnValue);
			print_r($returnValue);
		}
		$conn = null;
	} else 
	{
		$returnValue = 'Can not get parameters for request';
		$returnValue = json_encode($returnValue);
		print_r($returnValue);
	}

function getRow($conn, $id)
{
	try{
		$query = "SELECT [LIFTGATE], [RESIDENTIAL], [delivery_notification] FROM ".SHIPTRACKWITHLABEL_TABLE." WHERE [ID] = '".$id."'";
		//print_r($query);
		$result = $conn->prepare($query);
		$result->execute();
		$result = $result->fetch(PDO::FETCH_ASSOC);
		return $result;
	}
	catch(PDOException $e)
	{
		die($e->getMessage());
	}
}
?>