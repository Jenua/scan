<?php

$path = $_SERVER['DOCUMENT_ROOT'];
require_once($path.'/db_connect/connect.php');
$conn = Database::getInstance()->dbc;

if(!empty($_POST['user']))
{
    if(!empty($_POST['po_numbers']))
    {
            $arr = json_decode($_POST['po_numbers']);
            $user = $_POST['user'];
            $query_values = '';
            foreach($arr as $key => $row)
            {
                $query_values.='('.$conn->quote($row).', GETDATE(), '.$conn->quote($user).')';
                if(isset($arr[$key+1]))
                {
                    $query_values.=', ';
                }
            }
            $query = "DELETE FROM [dbo].[restore_order];
            INSERT INTO
            [dbo].[restore_order] ( [po], [date], [user] ) VALUES ".$query_values.";";
    } else
    {
        $query = "DELETE FROM [dbo].[restore_order];";
    }
} else
{
    die('Error. Can get user name.');
}

$result = execQuery($conn, $query);

if($result){
    print_r('Data successfully saved.');
}

function execQuery($conn, $query)
{
	try{
		$result = $conn->prepare($query);
		$result->execute();
	}
	catch(PDOException $e){
		error_msg($e->getMessage());
	}
	return $result;
}