<?php
$header_name = "Change Carrier";
require_once( "header.php" );


$path = $_SERVER['DOCUMENT_ROOT'];
require_once($path.'/db_connect/connect.php');
$conn = Database::getInstance()->dbc;


///*
$Ref = (isset($_REQUEST['Ref']) && !empty($_REQUEST['Ref'])) ? $_REQUEST['Ref'] : NULL;
$Dealer = (isset($_REQUEST['Dealer']) && !empty($_REQUEST['Dealer'])) ? $_REQUEST['Dealer'] : NULL;

$PONUMBER = (isset($_REQUEST['po']) && !empty($_REQUEST['po'])) ? $_REQUEST['po'] : NULL;
$old_CARRIERNAME = (isset($_REQUEST['old_carrier']) && !empty($_REQUEST['old_carrier'])) ? $_REQUEST['old_carrier'] : NULL;
$old_COST = (isset($_REQUEST['old_cost']) && !empty($_REQUEST['old_cost'])) ? $_REQUEST['old_cost'] : NULL;
$old_TRANSITTIME = (isset($_REQUEST['old_transit_time']) && !empty($_REQUEST['old_transit_time'])) ? $_REQUEST['old_transit_time'] : NULL;
$old_PRO = (isset($_REQUEST['old_pro']) && !empty($_REQUEST['old_pro'])) ? $_REQUEST['old_pro'] : NULL;

if( $Ref == NULL || $Dealer == NULL ) {
?>
<link rel="stylesheet" href="Include/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="Include/bootstrap/css/bootstrap-theme.min.css">
<link href="Include/jquery-ui-1.11.4.custom/themes/black-tie/jquery-ui.css" rel="stylesheet" type="text/css" />
<div class="container" style="color:#a94442">
	<h3>Invalid RefNumber or Dealer Name</h3>
</div>
<?php
die();
} //*/

?>
		<link rel="stylesheet" href="Include/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="Include/bootstrap/css/bootstrap-theme.min.css">
        <link href="Include/jquery-ui-1.11.4.custom/themes/black-tie/jquery-ui.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="js/jquery-1.10.2.js"></script>
        <script src="Include/bootstrap/js/bootstrap.min.js"></script>        
        <script type="text/javascript" src="Include/jquery-ui-1.11.4.custom/jquery-ui.min.js"></script>
		<script src="Include/bootstrap-validator-master/js/validator.js"></script>
        <script>
			function ToggleCostTime( state ) {
				$( "input#Cost" ).attr('disabled',state);
				$( "input#Time" ).attr('disabled',state);
			}
			
			$(document).ready(function(){
				$("#Carrier").autocomplete({
					source: "phpFunctions/autocomplete/carrier.php",
					minLength: 1
				});
			});
		</script>
		<?php require_once( "../Include/header_section.php" ); ?>
		
		<div class="container">
			<form id="changeForm" role="form" action="phpFunctions/changeCarrier.php" method="POST">
				<input type="hidden" id="Dealer" name="Dealer" value="<?php echo $Dealer; ?>">
				<div class="group">
					<h2>Change Carrier</h2>
					<div class="form-group">
						<label for="Ref">RefNumber</label>
						<input type="text" class="form-control" id="Ref" name="Ref" value="<?php echo $Ref; ?>" readonly>
					</div>
					<div class="form-group">
						<label for="Carrier">Carrier</label>
						<input type="text" class="form-control" id="Carrier" name="Carrier" placeholder="UPS" required>
					</div>
					<div class="checkbox">
						<label for="CostTime"><input type="checkbox" class="" id="CostTime" name="CostTime" onchange="ToggleCostTime( !this.checked )"><b>Set Cost and Transit Time Manually</b></label>						
					</div>
					<div class="form-group">
						<label for="Cost">New Cost</label>
						<input type="text" class="form-control" id="Cost" name="Cost" placeholder="0.0" pattern="[0-9]+(\.[0-9]{1,2})?" disabled>
					</div>
					<div class="form-group">
						<label for="Time">New Transit Time</label>
						<input type="text" class="form-control" id="Time" name="Time" placeholder="0" pattern="[0-9]+" disabled>
					</div>
					
					<input type="hidden" id="PONUMBER" name="PONUMBER" value="<?php echo $PONUMBER;?>">
					<input type="hidden" id="old_CARRIERNAME" name="old_CARRIERNAME" value="<?php echo $old_CARRIERNAME;?>">
					<input type="hidden" id="old_COST" name="old_COST" value="<?php echo $old_COST;?>">
					<input type="hidden" id="old_TRANSITTIME" name="old_TRANSITTIME" value="<?php echo $old_TRANSITTIME;?>">
					<input type="hidden" id="old_PRO" name="old_PRO" value="<?php echo $old_PRO;?>">
					
				<button type="submit" class="btn btn-success">Change</button>
				<button type="button" onClick="location.reload();" class="btn btn-default">Reset</button>                          
			</form>
		</div>
		<div id="dialog" title="Change Carrier">
			<div id="dialogContent"></div>
		</div>
		</div>
		<script>
			$('#changeForm').validator().on('submit', function (e) {
				if (e.isDefaultPrevented()) {
					// Invalid Form
				} else {
					event.preventDefault();
					var form = $('#changeForm');
					var url = $(form).attr("action");
					var formData = {};
					formData = $(form).serialize();
					//console.log(formData);
					$.ajax({
						url: url,
						type: 'POST',
						data: formData,
						dataType: "json",
						complete: function(data){
							//console.log( "data.responseJSON" );
							var res = data.responseJSON;
							$('#dialogContent').html( res );
							dialog.dialog('open');
						},
						error: function (request, status, error) {
							console.log( "ERROR" );
							$('#dialogContent').html( "Error in Ajax Request. Please, try again later." );
							dialog.dialog('open');
						}
					});
				}
				return false;
			});
			var dialog = $('#dialog').dialog({
				autoOpen: false,
				height: 'auto',
				width: 'auto',
				resizable: false,
				autoResize: true,
				modal: true,
				overlay: { backgroundColor: "#000", opacity: 0.5 }
			});	
			dialog.dialog('close');
			
		</script>
<?php


