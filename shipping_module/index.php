<?php
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
session_start();
require('phpFunctions/functionsExtr.php');
$path = $_SERVER['DOCUMENT_ROOT'];
require_once($path.'/db_connect/connect.php');
require_once($path.'/auth/auth.php');
 
$auth = new AuthClass();
 
if (isset($_POST["login"]) && isset($_POST["password"])) {
    if (!$auth->auth($_POST["login"], $_POST["password"])) {
        echo "<script>alert('Wrong login or password!')</script>";
    }
}
 
if (isset($_GET["is_exit"])) {
    if ($_GET["is_exit"] == 1) {
        $auth->out();
        header("Location: ?is_exit=0");
    }
}
 
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="initial-scale=1.0, maximum-scale=2.0">
	<title>Logistics Hub</title>
        <link rel="stylesheet" type="text/css" href="DataTables-1.10.3/media/css/jquery.dataTables.css">
	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
	<script type="text/javascript" language="javascript" src="DataTables-1.10.3/media/js/jquery.js"></script>
	<script type="text/javascript" src="js/script.js?rndmid=<?php echo uniqid(); ?>"></script>
	<script type="text/javascript" language="javascript" src="Include/DataTables/datatables.min.js"></script>
	<script src="Include/jquery-ui-1.11.4.custom/jquery-ui.js"></script>
        <script type="text/javascript" src="js/clipboard.min.js"></script>
	<script src="Include/Dropit-master/dropit.js"></script>
	<script type="text/javascript" src="js/jquery.inputmask.bundle.min.js"></script>
	<script type="text/javascript" src="js/proRanges.js?rndmid=<?php echo uniqid(); ?>"></script>
	
	<!--QZ-->
	<script type="text/javascript" src="Include/qz/js/dependencies/rsvp-3.1.0.min.js"></script>
	<script type="text/javascript" src="Include/qz/js/dependencies/sha-256.min.js"></script>
	<script type="text/javascript" src="Include/qz/js/qz-tray.js"></script>
	<script type="text/javascript" src="js/print.js?rndmid=<?php echo uniqid(); ?>"></script>
	<!--QZ-->
	
	<link rel="stylesheet" href="Include/Dropit-master/dropit.css" type="text/css" />
	<link rel="stylesheet" href="Include/Dropit-master/sample-styles.css" type="text/css" />
	<link rel="stylesheet" href="Include/css/style.css" type="text/css" />
        <link rel="stylesheet" href="Include/css/fontello.css" type="text/css" />

	<script>
	var table;
	$(document).ready(function() {
        $('.menu').dropit();
        $('.menu').fadeIn(100);
        var clipboard1 = new Clipboard('.copy_icon');
        clipboard1.on('success', function (e) {
            $(e.trigger).attr('src', 'Include/pictures/copy_visited.png');
        });

        table = $("#example").DataTable({
            "bAutoWidth": false,
            "bPaginate": false,
            "bSortClasses": false,
            "order": [[ <?php echo ($auth->getPermission() == 'superuser') ? "12" : "11" ?>, 'asc']]
        });

        /*$("#datepicker").datepicker({
            dateFormat: "yy-mm-dd"
        });*/
    });
	function blinker() {
		$('.blink_me').fadeOut(300).fadeIn(300);
	}
	</script>
</head>
<?php
$conn = Database::getInstance()->dbc;

function duplicated_status($conn)
{
	$query = "EXECUTE [".DATABASE31."].[dbo].[Orders_shipped_twice]";
	$result = $conn->prepare($query);
	$result->execute();
	$Orders_shipped_twice = $result->fetchAll();
	$query = "EXECUTE [".DATABASE31."].[dbo].[Orders_scanned_twice]";
	$result = $conn->prepare($query);
	$result->execute();
	$Orders_scanned_twice = $result->fetchAll();
	$query = "EXECUTE [".DATABASE31."].[dbo].[Orders_picked_twice]";
	$result = $conn->prepare($query);
	$result->execute();
	$Orders_picked_twice = $result->fetchAll();
	if ((isset($Orders_shipped_twice) && !empty($Orders_shipped_twice))
		|| (isset($Orders_scanned_twice) && !empty($Orders_scanned_twice))
		|| (isset($Orders_picked_twice) && !empty($Orders_picked_twice)))
	{
		return true;
	} else
	{
		return false;
	}
}

function getSettings($conn)
{
	$query = "SELECT * FROM ".GENERAL_SETTINGS." WHERE [app] = 'shipping_module'";
	$result = $conn->prepare($query);
	$result->execute();
	$result = $result->fetchAll(PDO::FETCH_ASSOC);
	if (!isset($result) || empty($result)) die('Can not get settings.');
	return $result;
}

$settings = getSettings($conn);

foreach ($settings as $setting)
{
    if ($setting['group_name'] == 'order_processing_mode' && $setting['type_name'] == 'fedex')
    {
        define("ORDER_PROCESSING_MODE_FEDEX", $setting['setting_value']);
    }
}
?>
<body class="dt-example" style="margin: 0; padding: 0;">
<input id="userNameHidden" type="hidden" value="<?php echo $auth->getName(); ?>"></input>
		<div style="position: fixed; z-index: 2000; background: #F7F7F7; width: 100%; height: 100px; margin: 0; padding: 0; box-shadow: 0 0 30px rgba(0,0,0,0.8);<?php if( DATABASE == "Orders_Test" ) { echo "background-color: #f88;"; }?>">
			<div style="position: absolute; left: 10px; top: 15px;">
				<img src='../Include/img/logo.png' height='67px'>
			</div>
		<?php 
		if ($auth->isAuth()) {
			$printer_label = $auth->getPrinter_label();
			$printer_bol = $auth->getPrinter_bol();
			$printer_flyer = $auth->getPrinter_hddc1();
			$user_id_global = $auth->getId();
			echo "<input type='hidden' id='printer_flyer' value='".$printer_flyer."'>";
			echo "<input type='hidden' id='printer_label' value='".$printer_label."'>";
			echo "<input type='hidden' id='printer_bol' value='".$printer_bol."'>";
			echo "<div style='position: absolute; left: 50%; margin-left: -160px; top: 0px; width: 500px; height: 20px;'>Hello, " . $auth->getName() .". <a href='?is_exit=1'><button class='loginsubmit'>Exit</button></a>&nbsp<button onclick='showChangePass()' class='loginsubmit'>Change Pass</button></div>";
		?>
<script>
assignPrinters();
</script>
<!--<div id="help"><a href="/help/index.php" target="_blank"><img title="Manual" src="Include/pictures/help.png"></a></div>-->
<ul class="menu" style="display:none;">
    <li>
        <a href="#"><button>Features</button></a>
        <ul>
			
                        <li><a href="#" onclick='window.open("/shipsingle/", "_blank")'> Quick Shipping Quote <img src="Include/pictures/quick.png"></a></li>
			<li><a href="#" onclick='goStatus()'> Order Status History <img src="Include/pictures/history.png"></a></li>
			<li><a href="#" onclick='window.open("search.php", "_blank")'> Advanced Search <img src="Include/pictures/search.png"></a></li>
			<li><a href="#" onclick='goReprint()'> Reprint <img src="Include/pictures/reprintSmall.png"></a></li>
			<li><a href="#" onclick='window.open("/combine_orders/", "_blank")'> Combine Orders <img src="Include/pictures/combineSmall.png"></a></li>
			<li><a href="#" onclick='window.open("/order_details/subprogram/reports/index.php", "_blank")'> Reports <img src="Include/pictures/unhandled.png"></a></li>
			<li><a href="#" onclick='window.open("/new_order/", "_blank")'> New Order <img src="Include/pictures/new_order.png"></a></li>
			<li><a href="#" onclick='printers_popup(<?php echo $user_id_global; ?>)'> Printer settings <img src="Include/pictures/printer_settings.png"></a></li>
			<li><a href="#" onclick='connectQZ()'> Connect printers <img src="Include/pictures/connectPrinters.png"></a></li>
			<li><a href="#" onclick='homeDepotDC()'> Home Depot DC Label <img src="Include/pictures/homeDepotDC.png"></a></li>
			<li><a href="#" onclick='window.open("/shipsingle/monitor.php", "_blank")'> Monitor Shipsingle <img src="Include/pictures/monitoring.png"></a></li>
			<li><a href="#" onclick='window.open("hddc_list.php", "_blank")'> DC Orders List <img src="Include/pictures/list.png"></a></li>
			<li><a href="#" onclick='window.open("fptrelated.php", "_blank")'> FPT Related List <img src="Include/pictures/list.png"></a></li>
                        <li><a href="#" onclick='window.open("restore_orders.php", "_blank")'> Restore Orders<img src="Include/pictures/restore_page.png"></a></li>
                        <li><a href="#" onclick='executeInternalSync(<?php echo $user_id_global; ?>)'> Internal Sync <img src="Include/pictures/sync.png"></a></li>
                        <li><a href="#" onclick='window.open("/help/index.php", "_blank")'> Manual<img src="Include/pictures/book.png"></a></li>
                        
        </ul>
    </li>
</ul>
		<?php
			echo "
			<div style='position: absolute; right: 340px; top: 20px;' id='buttonManifest'></div>
			<div style='position: absolute; right: 120px; top: 20px;' id='button'></div>
                        <div style='position: absolute; right: 230px; top: 20px;' id='buttonFlyer'></div>
			<div style='position: absolute; right: 10px; top: 20px;' id='buttonBill'></div>
			<div style='position: absolute; right: 230px; top: 75px;' id='buttonFlyerDirect'></div>
			<div style='position: absolute; right: 120px; top: 75px;' id='buttonDirect'></div>
			<div style='position: absolute; right: 10px; top: 75px;' id='buttonBillDirect'></div>
			<center>
				<h3 style='color: #367FBB; padding: 5px;'>Logistics Hub</h3>
			</center>";
					
					
					
			
					$num = 0;
					function readFormShipTrackWithLabel($conn)
					{
						$query = "EXECUTE [shipping_module_select_main11]";
                                                try { 
                                                    $stmt = $conn->query($query);
                                                    
                                                    /*$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                                    
                                                    print_r('<pre>');
                                                    print_r($result);
                                                    print_r('</pre>');
                                                    die();*/
                                                    
                                                    
                                                } catch (PDOException $e) {

                                                    die( $e->getMessage() );

                                                }
						
						if (!empty($stmt))
						{
							return $stmt;
						}
					}
					
					function strRepl($value)
					{
						$value = (string)$value;
						$value = str_replace("'", "`", $value);
						return $value;
					}
					
					function readFormLEFTINRANGE($conn)
					{
						$query = "SELECT [CARRIERNAME], [LEFT] FROM ".LEFTINRANGE;
						$stmt = $conn->prepare($query);
						$stmt->execute();
						$result = $stmt->fetchAll();
						if ($result)
						{
							return $result;
						}
					}
					
					$leftInRanges = readFormLEFTINRANGE($conn);
					
					$result = readFormShipTrackWithLabel($conn);
					/*print_r('<pre>');
					print_r($result);
					print_r('</pre>');
					die('stop');*/
					$missing = '<b style="color: red;">Missing</b>';
					if (isset($result) && !empty($result))
					{
					?>
					
					</div>
		<div style="position: relative; top: 110px; width: 100%; font-size: 18px;">
			<center>
			<?php
			foreach($leftInRanges as $leftInRange)
			{
				if ($leftInRange['LEFT'] <= 500)
				{
					echo "<button class='pointer' onclick='updateProRange".str_replace(' ', '', $leftInRange['CARRIERNAME'])."();'>".$leftInRange['CARRIERNAME'].":</button> <b class='blinkingText' style='color: red;'>".$leftInRange['LEFT']."</b>; ";
				} else if ($leftInRange['LEFT'] <= 1000)
				{
					echo "<button class='pointer' onclick='updateProRange".str_replace(' ', '', $leftInRange['CARRIERNAME'])."();'>".$leftInRange['CARRIERNAME'].":</button> <b style='color: #BC7A14;'>".$leftInRange['LEFT']."</b>; ";
				} else 
				{
					echo "<button class='pointer' onclick='updateProRange".str_replace(' ', '', $leftInRange['CARRIERNAME'])."();'>".$leftInRange['CARRIERNAME'].":</button> <b style='color: green;'>".$leftInRange['LEFT']."</b>; ";
				}
			}
			/*if (duplicated_status($conn) != falsetrue)
			{*/
				echo '<button class="duplicate_status" onClick = "runDuplicate_status()">Duplicated Status</button>';
			/*}*/
                        $date = $result->fetch();
                        $date = $date['sync_date'];
                        echo '<br><br>Last sync date: '.$date;
			?>
			</center>
		</div>
		<div style="position: relative; top: 115px; width: 100%;">
			<table id="example" class="display" cellspacing="0" width="100%">
				<thead>
					<tr>
						<?php if($auth->getPermission() == 'superuser') echo '<th>RECYCLE</th>'; ?>
						<th>LIFT</th>
						<th>RESI</th>
						<th>TOZIP</th>
						<th>STATE</th>
						<th>CARRIERNAME</th>
						<th>TRANSIT TIME</th>
						<th>COST</th>
						<th>DEALER</th>
						<th>PO NUMBER</th>
						<th>WEIGHT</th>
						<?php if($auth->getPermission() == 'superuser') echo '<th>SPLIT</th>'; ?>
						<th>CALCULATION DATE</th>
						<th>PRO#</th>
						<th>LOCK</th>
						<th>INVOICE#</th>
						<th>STATUS</th>
						<th>Generate Label & BOL</th>
					</tr>
				</thead>
				<tbody>
				<?php

					
					
						foreach ($result as $res)
						{
							if ((!isset($res['LIFTGATE']) || empty($res['LIFTGATE'])) && $res['LIFTGATE'] != '0')
							{
								$res['LIFTGATE'] = $missing;
								$liftgateVal = 0;
							} else {
								if ($res['LIFTGATE'] == '1')
								{
									$res['LIFTGATE'] = "<img src='Include/pictures/liftgate-icon.png' width='32px' height='32px' alt='LIFTGATE'>";
									$liftgateVal = 1;
								} else {
									$res['LIFTGATE'] = '-';
									$liftgateVal = 0;
								}
							}
							if ((!isset($res['RESIDENTIAL']) || empty($res['RESIDENTIAL'])) && $res['RESIDENTIAL'] != '0')
							{
								$res['RESIDENTIAL'] = $missing;
								$residentialVal = 0;
							} else {
								if ($res['RESIDENTIAL'] == '1')
								{
									$res['RESIDENTIAL'] = "<img src='Include/pictures/residentialLogo.png' width='32px' height='32px' alt='RESIDENTIAL'>";
									$residentialVal = 1;
								} else {
									$res['RESIDENTIAL'] = '-';
									$residentialVal = 0;
								}
							}

							if (!empty($res['ShipAddress_Country'])) $ShipAddress_Country = strRepl($res['ShipAddress_Country']); else $ShipAddress_Country = '-';
							if (!empty($res['ShipAddress_State'])) $ShipAddress_State = strRepl($res['ShipAddress_State']); else $ShipAddress_State = '-';
							if (!empty($res['ShipAddress_City'])) $ShipAddress_City = strRepl($res['ShipAddress_City']); else $ShipAddress_City = '-';
							if (!empty($res['ShipAddress_PostalCode'])) $ShipAddress_PostalCode = strRepl($res['ShipAddress_PostalCode']); else $ShipAddress_PostalCode = '-';
							if (!empty($res['ShipAddress_Addr1'])) $ShipAddress_Addr1 = strRepl($res['ShipAddress_Addr1']); else $ShipAddress_Addr1 = '-';
							if (!empty($res['ShipAddress_Addr2'])) $ShipAddress_Addr2 = strRepl($res['ShipAddress_Addr2']); else $ShipAddress_Addr2 = '-';
							if (!empty($res['ShipAddress_Addr3'])) $ShipAddress_Addr3 = strRepl($res['ShipAddress_Addr3']); else $ShipAddress_Addr3 = '-';
							if (!empty($res['FOB'])) $FOB = strRepl($res['FOB']); else $FOB = '-';
							if (preg_match_all( "/[0-9]/", $FOB ) == 10)
							{
								$FOB = str_replace('-', '', $FOB);
								$FOB = substr($FOB, 0, 3).'-'.substr($FOB, 3, 3).'-'.substr($FOB, 6, 4);
							} else $FOB = '-';
							
							if (empty($res['CARRIERNAME'])) $res['CARRIERNAME'] = '-';
							if (empty($res['TRANSITTIME'])) $res['TRANSITTIME'] = '-';
							if ((empty($res['COST'])) && $res['COST'] != '0') $res['COST'] = '-';
							if (empty($res['CALCULATIONDATE'])) $res['CALCULATIONDATE'] = '-';
							if (empty($res['ID'])) $res['ID'] = $missing;
							if (empty($res['ShipAddress_PostalCode'])) $res['ShipAddress_PostalCode'] = $missing;
							if (empty($res['DEALER'])) $res['DEALER'] = $missing;
							if (empty($res['PONUMBER'])) $res['PONUMBER'] = $missing;
							if (empty($res['WEIGHT'])) $res['WEIGHT'] = $missing;
							if (empty($res['PALLETS'])) $res['PALLETS'] = $missing;
							
							$printed_class = 'order-printed';
							if (!empty($res['GenerateTimeStamp']))
							{
								$GenerateTimeStamp = new DateTime($res['GenerateTimeStamp']);
								$NowDate = new DateTime($res['NowDate']);
								$Scanned_flag = false;
								if((!empty($res['ScannedDate'])) || (!empty($res['PickDate'])) || (!empty($res['ShipDate'])))
								{
									$Scanned_flag = true;
								} else 
								{
									$interval = $GenerateTimeStamp->diff($NowDate);
									$hours = $interval->h;
									$hours = $hours + ($interval->days*24);
									if(4 <= $hours) $Scanned_flag = false; else $Scanned_flag = true;
								}
								
								if(!$Scanned_flag) $printed_class = 'order-printed_red';
							}
                                                        
							echo '<tr>';
							if($auth->getPermission() == 'superuser')
							{
								if ($res['PONUMBER'] != '<b style="color: red;">Missing</b>')
								{
									if (is_in_str($res['ID'],'_split_'))
									{
										echo "<td><img onclick='refreshOrder(".'"'.$res['ID'].'"'.",".'"'.$res['PONUMBER'].'"'.", this)' src='Include/pictures/trash-128.png' width='24px' height='24px' style='cursor: pointer;' title='It will remove order from database.'></td>";
									} else if (is_in_str($res['ID'],'combine_'))
                                                                        {
                                                                            echo "<td><img onclick='uncombine(".'"'.$res['PONUMBER'].'"'.", ".'"'.$auth->getName().'"'.")' src='Include/pictures/uncombine_button.png' width='24px' height='24px' style='cursor: pointer;' title='It will rollback combination.'></td>";
                                                                        } else if (is_in_str($res['ID'],'manually_'))
									{
										echo "<td><img onclick='refreshOrder(".'"'.$res['ID'].'"'.",".'"'.$res['PONUMBER'].'"'.", this)' src='Include/pictures/trash-128.png' width='24px' height='24px' style='cursor: pointer;' title='It will remove order from database.'></td>";
									} else if (!empty($res['Combined_into_RefNumber']))
                                                                        {
                                                                            echo "<td></td>";
                                                                        } else
									{
										echo "<td><img onclick='refreshOrder(".'"'.$res['ID'].'"'.",".'"'.$res['PONUMBER'].'"'.", this)' src='Include/pictures/Refresh-icon.png' width='24px' height='24px' style='cursor: pointer;' title='It will remove order from database. Order will come back later.'></td>";
									}
								} else
								{
									echo "<td></td>";
								}
							}
								
							echo "<td>".$res['LIFTGATE']."</td>
								<td>".$res['RESIDENTIAL']."</td>";
                                                        
                                                        //$res['message'] = 'test';
                                                                
							if ($res['ShipAddress_PostalCode'] == $missing)
							{
                                                            echo "<td><button style='cursor: pointer;' onclick='updateTOZIP(".$res['ID'].")'>".$res['ShipAddress_PostalCode']."</button></td>";
							} else if(!empty($res['message']))
                                                        {
                                                            echo "<td><a href='https://www.google.com/maps/dir/Warminster,+PA+18974/".$res['ShipAddress_PostalCode']."' target='_blank'>".$res['ShipAddress_PostalCode']."</a><img src='Include/pictures/private-sign.png' title='".$res['message']."'></td>";
                                                        } else
							{
                                                            echo "<td><a href='https://www.google.com/maps/dir/Warminster,+PA+18974/".$res['ShipAddress_PostalCode']."' target='_blank'>".$res['ShipAddress_PostalCode']."</a></td>";
							}
							if (isCanadaState($res['ShipAddress_State'], $res['ShipAddress_Country']) == true || isHI_AL($res['ShipAddress_State']) == true) echo "<td style='color:red; font-weight: bold;'>".$res['ShipAddress_State']."</td>"; else echo "<td>".$res['ShipAddress_State']."</td>";
							$order_details_query_name = "Order Data";
							$order_details_query_text = "EXECUTE order_details_get_order_data @PONUMBER = %27".$res['PONUMBER']."%27";
							$order_details_href = "/order_details/display_data.php?query_name=".$order_details_query_name."&query_text=".$order_details_query_text;
							if(strpos($res['ID'],'combine_') !== false || strpos($res['ID'],'COMBINE_') !== false || !empty($res['Combined_into_RefNumber'])) $change_carrier_link = ''; else
								$change_carrier_link = "<a target=\"_blank\" href=\"change_carrier.php?Ref=".$res['ID']."&Dealer=".$res['DEALER']."&po=".$res['PONUMBER']."&old_carrier=".$res['CARRIERNAME']."&old_cost=".$res['COST']."&old_transit_time=".$res['TRANSITTIME']."&old_pro=".$res['PRO']."\"><img src=\"Include/pictures/settings.png\"/></a> ";
							echo "<td>" .
								 $change_carrier_link.
								$res['CARRIERNAME']."</td>
								<td>".$res['TRANSITTIME']."</td>
								<td>".$res['COST']."</td>
								<td>".$res['DEALER']."</td>
								<td class='word_break'><a target='_blank' href='/shipping_module/order_status_history.php?po=".urlencode($res['PONUMBER'])."&ref=".urlencode($res['ID'])."'>".$res['PONUMBER']."</a><img data-clipboard-text='".$res['PONUMBER']."' class='copy_icon' src='Include/pictures/copy.png' title='Copy PO to clipboard.'></td>";
								echo "<td>".$res['WEIGHT']."</td>";
							if($auth->getPermission() == 'superuser')
							{
								if (strpos($res['ID'],'_split_') !== false || strpos($res['ID'],'_SPLIT_') !== false) echo "<td>Splitted part</td>";
								else if (!empty($res['Combined_into_RefNumber'])) echo "<td>Combined part</td>";
								else if (strpos($res['ID'],'manually_') !== false || strpos($res['ID'],'MANUALLY_') !== false) echo "<td>Manually created order</td>";
								else if (strpos($res['ID'],'combine_') !== false || strpos($res['ID'],'COMBINE_') !== false) echo "<td>Combined order<!--<br><button style='color: red; cursor: pointer;' onclick='uncombine(".'"'.$res['ID'].'"'.")'>Uncombine</button>--></td>";
								else if (!empty($res['distr_sum']) && !empty($res['orig_sum'])) echo "<td><a href='/split_order/?id=".$res['ID']."' target='_blank'><button style='cursor: pointer; width: 70px; height: 21px;'><span id='SPLIT".$res['ID']."'>".$res['distr_sum']." of ".$res['orig_sum']."</span></button></a></td>";
                                                                else echo "<td><a href='/split_order/?id=".$res['ID']."' target='_blank'><button style='cursor: pointer; width: 70px; height: 21px;'><span id='SPLIT".$res['ID']."'>Split</span></button></a></td>";
							}
							//echo "<td><button style='cursor: pointer;' onclick='pallets_popup(".'"'.$res['ID'].'"'.")'><span id='PALLETS".$res['ID']."'>".$res['PALLETS']."</span></button></td>";
							
							echo "<td>".$res['CALCULATIONDATE']."</td>";
							if (($res['CARRIERNAME'] !== '-') &&
							($res['TRANSITTIME'] !== '-') && 
							($res['COST'] !== '-') && 
							($res['CALCULATIONDATE']  !== '-'))
							{
								$num++;
							}
								$shortCarrierName = extrCarrier($res['CARRIERNAME']);
							if (!isset($res['PRO']) || empty($res['PRO']))
								{
									echo "<td><input style='width: 120px;' id='PRO".$res['ID']."' type='text' value='".$res['PRO']."'><br><strong style='font-size: 11px; font-weight: normal;'>".$res['CARRIERNAME']."</strong></td>";
									echo "<td><span style='cursor: pointer;' class='unlockedInput' onclick='lock(".'"'.$res['ID'].'"'.", this)'><img width='21px' height='21px' src='Include/pictures/unlock.png'></span></td>";
								} else if (isset($res['PRO']) && !empty($res['PRO']))
								{
									echo "<td data-search='".$res['PRO']."'><input style='width: 120px;' id='PRO".$res['ID']."' type='text' value='".$res['PRO']."' disabled><br><strong style='font-size: 11px; font-weight: normal;'>".$res['CARRIERNAME']."</strong><img data-clipboard-text='".$res['PRO']."' class='copy_icon' src='Include/pictures/copy.png' title='Copy PRO# to clipboard.'></td>";
									echo "<td><span style='cursor: pointer;' class='lockedInput' onclick='lock(".'"'.$res['ID'].'"'.", this)'><img width='21px' height='21px' src='Include/pictures/lock.png'></span></td>";
								}
								echo "<td data-search='".$res['BOL']."'><input style='width: 120px;' id='BOL".$res['ID']."' type='text' value='".$res['BOL']."'></td>";
							$statusNumber = 0;
							echo '<td class="status">';
                                                        
                                                            if ($res['inventory_flag'] == 'negative')
                                                            {
                                                                echo '<a target="_blank" href="/shipping_module/inventory_allocation.php?po='.urlencode($res['PONUMBER']).'&ref='.urlencode($res['ID']).'"><i class="order-supply2" title="Not enough items for this order! Click for details. Calculated date: '.$res['inventory'].'"></i></a>';
                                                            } else if ($res['inventory_flag'] == 'positive')
                                                            {
                                                                echo '<a target="_blank" href="/shipping_module/inventory_allocation.php?po='.urlencode($res['PONUMBER']).'&ref='.urlencode($res['ID']).'"><i class="order-supply-ok" title="Inventory - OK. Calculated date: '.$res['inventory'].'"></i></a>';
                                                            } else
                                                            {
                                                                echo '<i class="order-supply-no" title="Inventory was not calculated for this order."></i>';
                                                            }
                                                        
							   if (isset($res['GenerateTimeStamp']) && !empty($res['GenerateTimeStamp']))
							   {
									echo '<i class="'.$printed_class.'" title="Printed - '.$res['GenerateTimeStamp'].'"></i>';
									$statusNumber++;
							   }
							   
							   if (isset($res['ScannedDate']) && !empty($res['ScannedDate']))
								{
									echo '<i class="order-scanned" title="Scanned - '.$res['ScannedDate'].'"></i>';
									$statusNumber++;
								}
							   
							   if (isset($res['PickDate']) && !empty($res['PickDate']))
								{
									echo '<i class="order-picked" title="Picked - '.$res['PickDate'].'"></i>';
									$statusNumber++;
								}
								
								if (isset($res['ShipDate']) && !empty($res['ShipDate']))
								{
									echo '<i class="order-shipped" title="Shipped - '.$res['ShipDate'].'"></i>';
									$statusNumber++;
								}
								
								if (!empty($res['Warning']))
								{
									echo '<i class="warning" title="Warning: '.$res['Warning'].'"></i>';
									$statusNumber++;
								}
								
								if (!empty($res['recalculation']))
								{
									echo '<i class="warning" title="'.$res['recalculation'].'"></i>';
									$statusNumber++;
								}
								
								if (!empty($res['real_status']))
								{
									echo '<i class="order-stop" title="'.$res['real_status'].' - Order last modified - '.$res['backorder_date'].'"></i>';
									$statusNumber++;
								}
                                                                
                                                                if (!empty($res['data_updated']))
                                                                {
                                                                    echo '<i class="data_updated" title="Order data was updated. Shipping cost recalculation initiated. Date: '.$res['data_updated'].'"></i>';
                                                                }
                                                                
                                                                if (!empty($res['recalculation_needed']))
                                                                {
                                                                    echo "<i onclick='refreshOrder(".'"'.$res['ID'].'"'.",".'"'.$res['PONUMBER'].'"'.", this)' class='recalculation_needed' title='This order data was updated in QB. Click to refresh order data.'></i>";
                                                                }
								
								
							echo "<p style='display: none;'>".$statusNumber."</p>";
							echo '</td>';
							
							$dealer_key = addslashes( extrStore($res['DEALER']) );
														
							if ($res['PONUMBER'] != $missing && strpos($res['ID'],'combine_') === false && strpos($res['ID'],'COMBINE_') === false)
							{
								echo "<td><button style='font-family:Arial, Helvetica, sans-serif; font-size:10px; font-weight: 600; width: 130px; height: 21px; color: #ffffff; background: #367FBB; border:0; cursor: pointer;' class='genbutton' id='genbutton".$res['PONUMBER']."' onclick=\"liftResi( '".$res['PONUMBER']."', '".$res['ID']."', this, '".ORDER_PROCESSING_MODE_FEDEX."', '".extrCarrier($res['CARRIERNAME'])."', '".$ShipAddress_Country."', '".$ShipAddress_State."', '".$ShipAddress_City."', '".$ShipAddress_PostalCode."', '".$ShipAddress_Addr1."', '".$ShipAddress_Addr2."', '".$ShipAddress_Addr3."', '".$FOB."', '".$res['display_popup']."', '".$auth->getLogin()."', '".$dealer_key."', false, '".$res['CARRIERNAME']."' );\">Generate Label & BOL</button></td>";
							} else if (strpos($res['ID'],'combine_') !== false || strpos($res['ID'],'COMBINE_') !== false)
							{
								echo "<td><button style='font-family:Arial, Helvetica, sans-serif; font-size:10px; font-weight: 600; width: 130px; height: 21px; color: #ffffff; background: #367FBB; border:0; cursor: pointer;' class='genbutton' id='genbutton".$res['PONUMBER']."' onclick='goReprint(".'"'.$res['PONUMBER'].'"'.");'>Display Documents</button></td>";
							}
							echo "</tr>";
						}
					}
					?>
				</tbody>
			</table>
			<?php
			echo "<div>".$num." entries processed.</div>";
			?>

			<div id="MissingBackground" style="position: absolute; left: 0px; top: 0px; width: 100%; height: 100%; z-index: 1999; display: none; background: rgba(0, 0, 0, 0.5);">
				<div id="MissingMessageBox">
					<span id="messageSpan" onclick="updateTOZIPcancel()">x</span>
					<label>Please type "TOZIP" value:</label>
					<input type="text" name="TOZIP" id="TOZIP">
					<input type="hidden" name="ID" id="ID" value="">
					<button onclick="updateTOZIPhandle()">OK</button>
				</div>
			</div>
			<div id="palletsBackground" style="position: absolute; left: 0px; top: 0px; width: 100%; height: 100%; z-index: 1999; display: none; background: rgba(0, 0, 0, 0.5);">
				<div id="palletsMessageBox">
					<span id="messageSpan" onclick="palletsCancel()">x</span>
					<label>Please type new pallets number:</label>
					<input type="text" name="pallets" id="pallets">
					<input type="hidden" name="palletsID" id="palletsID" value="">
					<button onclick="updatePallets()">OK</button>
				</div>
			</div>
			<div id="dateBackground" style="position: absolute; left: 0px; top: 0px; width: 100%; height: 100%; z-index: 1999; display: none; background: rgba(0, 0, 0, 0.5);">
				<div id="dateMessageBox">
					<span id="messageSpan" onclick="goManifestCancel()">x</span>
					<label>Select a date to generate manifest:</label>
					<div id="datepicker"></div>
					<button onclick="goStartManifest()">OK</button>
				</div>
			</div>
			<div id="resiLiftBackground" style="position: absolute; left: 0px; top: 0px; width: 100%; height: 100%; z-index: 1999; display: none; background: rgba(0, 0, 0, 0.5);">
				<div id="resiLiftMessageBox">
					<span id="messageSpan" onclick="goResiLiftCancel()">x</span>
					<label id="messageRLCLabel"></label>
					<div id="resiLiftDiv">
						<label><input id="RESIDENTIAL" type="checkbox" name="RESIDENTIAL" value="RESIDENTIAL"> RESIDENTIAL <span id="resiIconOnPopup"></span></label><br>
						<!--<label><input id="TheCheckBox" type="checkbox" data-off-text="COMMERCIAL" data-on-text="RESIDENTIAL" checked="false" class="BSswitch"> <span id="resiIconOnPopup"></span></label><br>-->
						<label class="sublabel"><input id="LIFTGATE" type="checkbox" name="LIFTGATE" value="LIFTGATE"> LIFTGATE <span id="liftIconOnPopup"></span></label><br>
						<label class="sublabel" id="delivery_notification_label"><input id="delivery_notification" type="checkbox" name="delivery_notification" value="delivery_notification"> DELIVERY NOTIFICATION</label><br><br>
					</div>
					<button onclick="goResiLift('<?php echo $auth->getLogin(); ?>')">OK</button>
					<button class="cancelButton" onclick="goResiLiftCancel()">CANCEL</button>
				</div>
			</div>
			<div id="resiLiftLoader" style="position: fixed; left: 50%; top: 300px; width: 150px; height: 150px; margin-left: -75px; z-index: 1998; display: none;">
				<center>
					<img src='Include/gif/294.GIF' height='150px' width='150px'>
				</center>
			</div>
			<div id="removeBackground" style="position: absolute; left: 0px; top: 0px; width: 100%; height: 100%; z-index: 1999; display: none; background: rgba(0, 0, 0, 0.5);">
				<div id="removeMessageBox">
					<span id="messageSpan" onclick="removeCancel()">x</span>
					<label>Please type PO to recycle:</label>
					<input type="text" name="removePO" id="removePO">
					<label>Please choose reason:</label>
					<center>
						<select onchange="showRemoveReasonNote()" id="reasonSelect">
							<?php
							$remove_reasons = '';
							foreach ($settings as $setting)
							{
								if ($setting['group_name'] == 'remove_reasons' && $setting['type_name'] == 'reasons')
								$remove_reasons = $setting['setting_value'];
							}
							$removeReasons = explode(",", $remove_reasons);
							$lastElementRemove = end($removeReasons);
							foreach ($removeReasons as $removeReason)
							{
								echo '<option value="'.$removeReason.'">'.$removeReason.'</option>';
							}
							?>
						</select>
					</center>
					<label id="removeLabel" style="display: none;">Please type reason:</label>
					<textarea id="reasonNote" style="display: none; resize: none;" rows="7" cols="41" name="text" maxlength="255"></textarea>
					<br>
					<br>
					<button onclick="removeOrder(<?php echo "'".$auth->getName()."'"; ?>)">OK</button>
				</div>
			</div>
			<div id="reprintBackground" style="position: absolute; left: 0px; top: 0px; width: 100%; height: 100%; z-index: 1999; display: none; background: rgba(0, 0, 0, 0.5);">
				<div id="reprintMessageBox">
					<span id="messageSpan" onclick="reprintCancel()">x</span>
                    <div>
					<label>Please type PO to reprint:</label>
					<input type="text" name="reprintPO" id="reprintPO">
                    </div>
					<label>Please choose reason:</label>
					<center>
						<select onchange="showReprintReasonNote()" id="reasonSelectreprint">
						  <?php
							$reprint_reasons = '';
							foreach ($settings as $setting)
							{
								if ($setting['group_name'] == 'reprint_reasons' && $setting['type_name'] == 'reasons')
								$reprint_reasons = $setting['setting_value'];
							}
							$reprintReasons = explode(",", $reprint_reasons);
							foreach ($reprintReasons as $reprintReason)
							{
								echo '<option value="'.$reprintReason.'">'.$reprintReason.'</option>';
							}
							?>
						</select>
					</center>
					<label id="reprintLabel" style="display: none;">Please type reason:</label>
					<textarea id="reasonNotereprint" style="display: none; resize: none;" rows="7" cols="41" name="text" maxlength="255"></textarea>
					<br>
					<br>
					<button type="button" onclick="beforeReprint(<?php echo "'".$auth->getName()."'"; ?>)">OK</button>
				</div>
			</div>
                    <div id="reprintBackground2" style="position: absolute; left: 0px; top: 0px; width: 100%; height: 100%; z-index: 1999; display: none; background: rgba(0, 0, 0, 0.5);">
				<div id="reprintMessageBox">
					<span id="messageSpan" onclick="reprintCancel2()">x</span>
					<label>Please choose reason:</label>
					<center>
						<select onchange="showReprintReasonNote2()" id="reasonSelectreprint2">
						  <?php
							$reprint_reasons = '';
							foreach ($settings as $setting)
							{
								if ($setting['group_name'] == 'reprint_reasons' && $setting['type_name'] == 'reasons')
								$reprint_reasons = $setting['setting_value'];
							}
							$reprintReasons = explode(",", $reprint_reasons);
							foreach ($reprintReasons as $reprintReason)
							{
								echo '<option value="'.$reprintReason.'">'.$reprintReason.'</option>';
							}
							?>
						</select>
					</center>
					<label id="reprintLabel2" style="display: none;">Please type reason:</label>
					<textarea id="reasonNotereprint2" style="display: none; resize: none;" rows="7" cols="41" name="text" maxlength="255"></textarea>
					<br>
					<br>
					<button id="okDisplayDoc">OK</button>
				</div>
			</div>
			<div id="changepassBackground" style="position: absolute; left: 0px; top: 0px; width: 100%; height: 100%; z-index: 1999; display: none; background: rgba(0, 0, 0, 0.5);">
				<div id="changepassMessageBox">
					<span id="messageSpan" onclick="changepassCancel()">x</span>
					<label>Please type current password:</label>
					<input type="text" name="changepassOldPass" id="changepassOldPass">
					<label>Please type new password:</label>
					<input type="text" name="changepassNewPass" id="changepassNewPass">
					<br>
					<br>
					<button onclick="changePassword('<?php echo $auth->getId(); ?>')">OK</button>
				</div>
			</div>
			<div id="statusBackground" style="position: absolute; left: 0px; top: 0px; width: 100%; height: 100%; z-index: 1999; display: none; background: rgba(0, 0, 0, 0.5);">
				<div id="statusMessageBox">
					<span id="messageSpan" onclick="statusCancel()">x</span>
					<label>Please type PO to see status history:</label>
					<input type="text" name="statusPO" id="statusPO">
					<button onclick="get_status_history('')">OK</button>
					<br><br><a onclick="statusCancel()" target="_blank" href="search.php">Advanced search</a>
				</div>
			</div>
			<div id="errorsInvisDiv" style="position: fixed; width: 800px; max-height: 500px; padding: 10px; top: 200px; left: 50%; margin-left: -400px; background: #f7f7f7; border: 1px solid grey;  box-shadow: 0 0 30px grey; z-index: 999; display: none; overflow: auto;">
				<center><p style="color: #367FBB; font-weight: 600; font-size: 16px;">Validation report</p></center>
				<div style="margin-top: 20px; width: 780px; word-wrap: break-word;" id="errorsGlobal" class="ui-state-highlight ui-corner-all"></div>
				<div style="margin-top: 20px; width: 780px; word-wrap: break-word;" id="errors"></div>
				<div style="margin-top: 20px; width: 780px; word-wrap: break-word;" id="errorsBill"></div>
				<div style="margin-top: 20px; width: 780px; word-wrap: break-word;" id="errorsFlyer"></div>
				<div style="margin-top: 20px; width: 780px; word-wrap: break-word;" id="errorsManifest"></div>
				<center><button style='font-family:Arial, Helvetica, sans-serif; font-size:11px; width: 121px; height: 21px; color: #ffffff; background: #367FBB; border:0; cursor: pointer; margin-top: 20px;' onclick="hideErrors()">Close</button></center>
			</div>
			<div style="bottom: 2px; width: 100%; text-align:center; margin: 10px 0 0 0;"><p>Copyright © <?php echo date('Y'); ?> Dreamline</p></div>
		</div>
		<div id="dialog">
			<div id="content"></div>
		</div>
<?php
$conn = null;
}
else {
?>
<div style="position: relative; top: 110px; width: 100%;">
<div style="
position: relative;
width: 330px;
height: 160px;
left: 50%;
margin-left: -160px;
padding: 10px;
background: #367FBB;
">
<form method="post" action="">
<fieldset>
    <p><label class="field" for="login">Login:</label><input class="logininput" type="text" name="login"
    value="<?php echo (isset($_POST["login"])) ? $_POST["login"] : null; ?>" /></p>
    <p><label class="field" for="password">Password:</label><input class="logininput" type="password" name="password" value="" /></p>
	<p><label class="field" for="password"> </label><input class="loginsubmit" type="submit" value="login" /></p>
</fieldset>
</form>
</div>
<div style="bottom: 2px; width: 100%; text-align:center; margin: 10px 0 0 0;"><p>Copyright © 2016 Dreamline</p></div>
</div>
<?php 
$conn = null;
}
?>
<script>
	 connectQZ(true);
</script>
</body>
</html>