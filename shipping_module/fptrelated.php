<?php
set_time_limit(120);
session_start();
//order status history
require('phpFunctions/functionsExtr.php');
$path = $_SERVER['DOCUMENT_ROOT'];

$GLOBALS['config'] = array(
    'session'=> array('session_name'=>'id','token_name'=>'token'),
    'master_modules' => array('rits','packing','picking','loading')
);

require_once($path.'/db_connect/connect.php');
require_once($path.'/shipping_module/Include/Twig/Autoloader.php');

require_once $path . '/Include/classes/Config.php';
require_once $path . '/Include/classes/Session.php';
require_once $path . '/Include/classes/Redirect.php';
require_once $path . '/Include/classes/User.php';

require_once($path . '/Include/helpers/helpers.php');
require_once($path . '/Include/helpers/us_helpers.php');


function exec_query($conn, $query) {
	try {
		$result = $conn->prepare($query);
		$result->execute();
		return $result;
	}
	catch(PDOException $e) {
		print_r($e->getMessage());
	}
}
function parseItemFullName( $name ) {
    return end( explode(":", $name) );
}




Twig_Autoloader::register();
$loader = new Twig_Loader_Filesystem('templates/Pages');
$twig = new Twig_Environment($loader, array(
	'cache'       => 'compilation_cache',
	'auto_reload' => true
));


$user = new User();

if( isset($_POST['login']) && isset($_POST["password"]) ) {
	if( !$user->login($_POST["login"], $_POST["password"]) ) {		
		$loginName = $_POST["login"] ? $_POST["login"] : '';
		$wrongCredentials = $twig->render('loginPage.html', array('login' => $loginName, 'errmessage' => 'Error: wrong login or password'));
		die($wrongCredentials);		
	}
}
if( isset($_GET["is_exit"]) ) {
    if ($_GET["is_exit"] == 1) {
        $user->logout();
        header("Location: ?is_exit=0");
    }
}
if( !$user->isLoggedIn() ) {
	$noAuth = $twig->render('loginPage.html', array('login' => '', 'errmessage' => ''));
	die($noAuth);
}

$isEditor = hasModuleAccess('FptRelatedEdit');

// DB Connect
$conn = Database::getInstance()->dbc;
$getRelatedQuery = "SELECT * FROM [fpt_related] ORDER BY [Product], [Related]";
$relatedResult   = exec_query($conn, $getRelatedQuery);
$relatedArray = [];
foreach( $relatedResult as $row ) {
	if( empty($relatedArray[$row['Product']]) ) {
		$relatedArray[$row['Product']] = [];
	}
	$relatedArray[$row['Product']][] = $row['Related'];
}

$username = $user->data()->name;

$fptRelated = $twig->render('fptRelated.html', array( 'data' => $relatedArray, 'editor' => $isEditor, 'username' => $username ));
die($fptRelated);
