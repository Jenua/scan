<?php
//Get project root path
$path = $_SERVER['DOCUMENT_ROOT'];

//Include database connection class
require_once($path.'/db_connect/connect.php');

if(!empty($_REQUEST['po']) && !empty($_REQUEST['ref']))
{
    $po = urldecode($_REQUEST['po']);
    $ref = urldecode($_REQUEST['ref']);
    
    $conn = Database::getInstance()->dbc;
    
    $query = "SELECT iafd.[POnumber]
		,iafd.[RefNumber]
                ,st.[CustomerRef_FullName]
		,iafd.[TimeModified]
		,iafd.[ItemGroupRef_FullName]
		,iafd.[ItemRef_FullName]
		,iafd.[inventory_quantity]
		,iafd.[total_quantity_needed]
		,CASE
		WHEN iafd.[allocated_quantity] > iafd.[inventory_quantity]
		THEN iafd.[Date]
		ELSE NULL
		END AS [IS_NEGATIVE]
	FROM [dbo].[inventory_allocation_full_details] iafd
        LEFT JOIN [dbo].[shiptrack] st ON st.[PONumber] = iafd.[POnumber] AND st.[RefNumber] = iafd.[RefNumber]
	WHERE [ItemRef_FullName] IN 
	(
	SELECT [ItemRef_FullName]
	FROM [dbo].[inventory_allocation_full_details]
	WHERE
	[POnumber] = ?
        AND [RefNumber] = ?
	)
ORDER BY iafd.[TimeModified], iafd.[POnumber], iafd.[ItemRef_FullName];
EXEC [orders].[dbo].[supply_date] @PO = ?, @REF = ?;";
    
    $obj = $conn->prepare($query);
    $obj->execute(array($po, $ref, $po, $ref));
    $allocation_datas = $obj->fetchAll(PDO::FETCH_ASSOC);
    
    $obj->nextRowset();
    $result_supply_date = $obj->fetchAll(PDO::FETCH_ASSOC);
    
    $supply_date = 'Supply date: N/A';
    
    if(!empty($allocation_datas))
    {
        $is_order_negative = false;
        foreach($allocation_datas as $allocation_data)
        {
            if (!empty($allocation_data['IS_NEGATIVE']) && $po == $allocation_data['POnumber'] && $ref == $allocation_data['RefNumber'])
            {
                $is_order_negative = true;
            }
        }
        
        if($is_order_negative)
        {
            if(!empty($result_supply_date))
            {
                $dates = [];
                $no_nulls_in_array = true;
                foreach($result_supply_date as $row)
                {
                    if(empty($row['ExpectedDate']))
                    {
                        $no_nulls_in_array = false;
                    }
                    $dates[]= $row['ExpectedDate'];
                }

                if($no_nulls_in_array)
                {
                    $max = max(array_map('strtotime', $dates));
                    $supply_date = 'Supply date: '.date('F d Y', $max);
                }
            }
        } else
        {
            $supply_date = 'Order is fully stocked';
        }
?>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Inventory Allocation</title>
  <script type="text/javascript" language="javascript" src="DataTables-1.10.3/media/js/jquery.js"></script>
  <style>
table {
    border-collapse: collapse;
    width: 100%;
}

th, td {
    border-bottom: 1px solid #ddd;
}

th, tr, td {
    padding: 5px;
    text-align: left;
    height: 32px;
}

th {
    background-color: #4CAF50;
    color: white;
}

caption {
    padding: 15px;
    background-color: #696f75;
    color: white;
    font-weight: bold;
    font-size: 20pt;
    border-bottom: 8px solid white;
}

caption a{
    text-decoration: none;
}

caption #supply_date{
    float: right;
    color: yellow;
}

tr:hover {background-color: #e6ffe6}

.po_title{
    background-color: #f2f2f2;
    font-weight: bold;
}

.original_order_title{
    background-color: orange;
}

.original_order_items{
    background-color: yellow;
}

.original_po_ref{
    color: yellow;
}

.original_po_ref:hover {color: orange}

.black_font{
    color: black;
}

.order-supply2{
    background: url('Include/pictures/negative.png');
}
.order-supply-ok{
    background: url('Include/pictures/check.png');
}
.status {
    text-align: left;
    padding: 0;
    white-space: nowrap;
}
.status i {
    margin-left: 2px;
    margin-right: 2px;
    display: inline-block;
    width: 32px;
    height: 32px;
    vertical-align: middle;
    background-size: 100%;
    border-radius: 5px;
}

.empty_row{
    border: none;
}
  </style>
</head>

<body>
<?php
    echo "<table>
    <caption>The list of orders with common items for the specified order. <a class='anchor' href='#original'><span class='original_po_ref'>PO: ".$po." REF: ".$ref." <img src='Include/pictures/anchor.png' title='Click to display original order.'/></span></a><span id='supply_date'>".$supply_date."</span></caption>
    <thead>
        <tr>
            <th class='black_font'>QUEUE</th>
            <th class='black_font'>PO</th>
            <th class='black_font'>REF</th>
            <th class='black_font'>DEALER</th>
            <th class='black_font'>ORDER CREATED</th>
            <th>GROUP SKU</th>
            <th>ITEM NAME</th>
            <th>QUANTITY ON HAND</th>
            <th>INVENTORY</th>
        </tr>
    </thead>
    <tbody>";
    
    $queue = 0;
    
    $last_po = ['po'=>'', 'ref'=>''];
    foreach($allocation_datas as $allocation_data)
    {
        if (!empty($allocation_data['IS_NEGATIVE']))
        {
            $supply2 = '<i class="order-supply2" title="Not enough items! Calculated date: '.$allocation_data['IS_NEGATIVE'].'"></i>';
        } else $supply2 = '<i class="order-supply-ok" title="Inventory - OK!"></i>';
        if($po == $allocation_data['POnumber'] && $ref == $allocation_data['RefNumber'])
        {
            $original_order_class_title = ' original_order_title';
            $original_order_class_items = 'original_order_items';
            $original_id = "id='original'";
        } else
        {
            $original_order_class_title = '';
            $original_order_class_items = '';
            $original_id = "";
        }
        if($allocation_data['POnumber'] != $last_po['po'] || $allocation_data['RefNumber'] != $last_po['ref'])
        {
            $date = new DateTime($allocation_data['TimeModified']);
            $date = $date->format('m-d-Y H:i:s');
            echo "<tr ".$original_id." class='po_title".$original_order_class_title."'>
                <td>".++$queue."</td>
                <td><a target='_blank' href='/shipping_module/order_status_history.php?po=".urlencode($allocation_data['POnumber'])."&ref=".urlencode($allocation_data['RefNumber'])."'>".$allocation_data['POnumber']."</a></td>
                <td><a target='_blank' href='/shipping_module/order_status_history.php?po=".urlencode($allocation_data['POnumber'])."&ref=".urlencode($allocation_data['RefNumber'])."'>".$allocation_data['RefNumber']."</a></td>
                <td>".$allocation_data['CustomerRef_FullName']."</td>
                <td>".$date."</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>";
            echo "<tr class='".$original_order_class_items."'>
                <td class='empty_row'></td>
                <td class='empty_row'></td>
                <td class='empty_row'></td>
                <td class='empty_row'></td>
                <td class='empty_row'></td>
                <td>".$allocation_data['ItemGroupRef_FullName']."</td>
                <td>".$allocation_data['ItemRef_FullName']."</td>
                <td>".$allocation_data['inventory_quantity']."</td>
                <td class='status'>".$supply2."</td>
            </tr>";
        }else
        {
            echo "<tr class='".$original_order_class_items."'>
                <td class='empty_row'></td>
                <td class='empty_row'></td>
                <td class='empty_row'></td>
                <td class='empty_row'></td>
                <td class='empty_row'></td>
                <td>".$allocation_data['ItemGroupRef_FullName']."</td>
                <td>".$allocation_data['ItemRef_FullName']."</td>
                <td>".$allocation_data['inventory_quantity']."</td>
                <td class='status'>".$supply2."</td>
            </tr>";
        }
        $last_po['po'] = $allocation_data['POnumber'];
        $last_po['ref'] = $allocation_data['RefNumber'];
    }
    
    echo '</tbody>
    </table><br><br>';
    } else
    {
        echo "There is not inventory allocation data for this order.<br>
        Check Order Status History page for details: <a target='_blank' href='/shipping_module/order_status_history.php?po=".urlencode($po)."&ref=".urlencode($ref)."'>".$po."</a>";
    }
} else
{
    echo 'Empty po or ref value in URL.';
}
?>
</body>
<script>
    $("body").on("click", "a.anchor", function(){
        var idtop = $($(this).attr("href")).offset().top;
        $('html,body').animate({scrollTop: idtop}, 500);
        return false;
    });
  </script>
</html>