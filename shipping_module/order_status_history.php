<?php
$header_name = "Order Status History";
require_once( "header.php" );
//get PO
if (isset($_GET['po']) && !empty($_GET['po']))
{
	$po = urldecode($_GET['po']);
        //die($po);
	
	if(isset($_GET['txn']) && !empty($_GET['txn'])) $txn = $_GET['txn']; else $txn = false;
	if(isset($_GET['ref']) && !empty($_GET['ref'])) $ref = $_GET['ref']; else $ref = false;
	
	//Connect to db and include files
	$path = $_SERVER['DOCUMENT_ROOT'];
	require_once($path.'/db_connect/connect.php');
	require_once($path.'/shipping_module/phpFunctions/order_status_history_functions.php');
	require_once($path . '/Include/classes/Input.php');
	
        
	//Get all data about order
	$result_status = $results_all['result_statuses']; //Statuses
	$result_validation = $results_all['result_validation']; //Validation messages
        /*print_r('<pre>');
        print_r($result_validation);
        print_r('</pre>');
        die();*/
	
	$result_shiptrackwithlabel = $results_all['result_shiptrackwithlabel']; //Shipping data
        
	
	$result_shiptrack = $results_all['result_shiptrack']; //Order data
        
        $inventory = false;
        if(!empty($result_shiptrack['data']))
        {
            if($result_shiptrack['data'][0]['inventory_flag'] == 'negative')
            {
                $inventory = $result_shiptrack['data'][0]['inventory'];
            }
        }
        $result_warnings = get_warnings($inventory); //Warnings
        
	$result_groupdetail_DL = $results_all['result_groupdetail_DL']; //Products in order
	
	//die("<pre>".print_r($result_groupdetail_DL,true)."</pre>");
	
	$result_linedetail = $results_all['result_linedetail']; //Items in order
	
	$result_shiptrack_s = $results_all['result_salesorder']; //Order data
	$result_groupdetail_DL_s = $results_all['result_salesorderlinegroupdetail_DL']; //Products in order
	$result_linedetail_s = $results_all['result_salesorderlinedetail']; //Items in order
	
	$result_shiptrack_i = $results_all['result_invoice']; //Order data
	$result_groupdetail_DL_i = $results_all['result_invoicelinegroupdetail_DL'] ; //Products in order
	$result_linedetail_i = $results_all['result_invoicelinedetail']; //Items in order
	
	$result_allcosts = $results_all['result_allcosts']; //Alternative rates for current order
	
	$info_messages = generate_info($result_shiptrack, $result_shiptrackwithlabel, $result_shiptrack_s, $po);
	
	$settings = $results_all['result_settings'];
	
?>
	<title><?php echo $header_name; ?> </title>


        <script type="text/javascript" src="js/clipboard.min.js"></script>
	<link rel="stylesheet" type="text/css" href="Include/css/status_history.css" media="all"/>

	<script>
		var reprintConfirmed = false;
		$(function() {
		$( "#tabs" ).tabs();
		});
                
                
                function generateInvoice( po ) {
                    var tmp = $( "#gen_invoice_"+po+" button" ).html();
                    $( "#gen_invoice_"+po+" button" ).html( '<span style="display:inline-block; width:57px;"><img style="vertical-align: middle;" src="Include/gif/tiny_loading.GIF"/></span>' );
                    $.post( "phpFunctions/functionsOSHInvoicePDF.php"
                              , { po: po }
                              , function( data ) {
                                            if(data.indexOf('ERROR')+1) {
                                                    alert(data);
                                            } else {
                                                    checkInvoicePdf( po );
                                            }
                                            $( "#gen_invoice_"+po+" button" ).html( tmp );
                                    }
                    );        
                }
                function checkInvoicePdf( po ) {
                    $.post( "phpFunctions/functionsOSHInvoicePDFchecker.php"
                        , { po: po }
                        , function( data ) {
                            var result = $.parseJSON(data);
                            if( result.invoice ) {
                                    $( "#open_invoice_"+po ).show();
                                    $( "#open_invoice_"+po ).attr('href', result.invoice);
                            }
                          }
);
                }

function reprintCancel()
{
	$('#reprintBackground').fadeOut(100);
	$('#reprintPO').val('');
	$("#reasonNotereprint").val('');
}

function goReprint(po, path)
{
	if (!reprintConfirmed)
	{
		$('#reprintBackground').fadeIn(100);
		$('#reprintPO').val(po);
		$('#reprintPath').val(path);
	} else
	{
		reprintOrderReady(path);
	}
}

function reprintOrderReady(path)
{
	var win = window.open('newTab.php?value='+path, '_blank');
	win.focus();
}

function showReprintReasonNote()
{
	var lastValue = $('#reasonSelectreprint option:last-child').val();
	var selectedValue = $("#reasonSelectreprint option:selected").text();
	if (lastValue == selectedValue)
	{
		$('#reprintLabel').fadeIn(50);
		$('#reasonNotereprint').fadeIn(50);
		setFocusToId('#reasonNotereprint');
	} else
	{
		$('#reprintLabel').fadeOut(50);
		$('#reasonNotereprint').fadeOut(50);
	}
}

function setFocusToId(id) {
	setTimeout(function () {
		$(id).focus();
	}, 100);
}

function reprintOrder(userName)
{
	var po = $("#reprintPO").val();
	var path = $("#reprintPath").val();
	var reason = $("#reasonSelectreprint option:selected").val();
	var reasonNote = $("#reasonNotereprint").val();

					$.ajax({
						url: 'phpFunctions/logOrderReprinted.php',
						type: 'POST',
						data: {
							po: po,
							reason: reason,
							reasonNote: reasonNote,
							userName: userName
						},
						complete: function (data) {
							console.log(data.responseText);
							reprintConfirmed = true;
							reprintOrderReady(path);
						},
						error: function (request, status, error) {
							console.log("Ajax request error: " + request.responseText);
						}
					});
					
	reprintCancel();
}

function saveHideOrderValue(flag, id) {

    $.ajax({
        url: 'phpFunctions/handlerOSH.php',
        dataType: 'json',
        type: 'POST',
        data: {hide_order: flag, id: id},
        complete: function () {
            // dialog.dialog("close");
            console.log('updated by' + flag);

            if (flag == 1 ){

                $('.eye-close').switchClass( "hidden", "show", 1000, "easeInOutQuad" );
                $('.eye').addClass('hidden');$('#hide_order').attr('value',0);
                }
            else {

                $('.eye').switchClass( "hidden", "show", 1000, "easeInOutQuad" );
                $('.eye-close').addClass('hidden');$('#hide_order').attr('value',1);
                }
        }
    })
}

$(document).ready(function(){
	
	$('.js-warning').click(function(){		
		var code = $(this).attr('data-code');
		var id = $(this).attr('data-id');
		var po = $(this).attr('data-po');
		var user = <?php echo "'".$auth->getName()."'"; ?>;
		var url = '';
		var message = 'Do you want to delete this warning ?';
		
		switch( code ) {
			case '1': 
			default:  url = 'phpFunctions/deleteWarnings.php';
		}
		
		if( confirm(message) ) {
			$.ajax({
				url: url,
				type: 'POST',
				data: {			
					id: id,
					po: po,
					user: user
				},
				complete: function (data) {
					console.log(data.responseText);
					if( data.responseText == 'OK' ) {
						location.reload();
					}
				},
				error: function (request, status, error) {
					console.log("Ajax request error: " + request.responseText);
				}
			});
		}
	});
if($('#hidden_status').html() != undefined ){
    if($('#hide_order').val() == 0 ){
        $('.eye').removeClass('hidden');
        $('.eye-close').addClass('hidden');
    } else  if($('#hide_order').val() == 1 ) {
        $('.eye-close').removeClass('hidden');
        $('.eye').addClass('hidden');
    }
}
    $('#hidden_status button').on("click", function (event) {
        event.preventDefault();
        if ($(this).val() != undefined && $('#RefNumber').val()!= undefined) {
            saveHideOrderValue($(this).val(), $('#RefNumber').val());
        }
    });
});

</script>
</head>

<body>
<?php require_once($path.'/Include/header_section.php');?>
	<div id="wrapper">
		<div id="order-content">
			<!--<h1>Order Status History</h1>-->
			<div class="order-holder">
				<div class="top-holder">
					<h2>PO: <?php echo $po; ?></h2>
					<!--<button class="genbutton" id="genbuttonGLBG30086659">Generate Label &amp; BOL</button>-->
				</div>
				<div class="order-block left">

                <?php if(!empty($result_shiptrack['data'])){
                    if(!empty($result_shiptrack['data']['0']))
                    {
                        if(strpos($result_shiptrack['data']['0']['RefNumber'], 'manually_') === FALSE)
                        {
                            $manually_flag = false;
                        } else
                        {
                            $manually_flag = true;
                        }
                    }
                ?>
                    <!-- manually_shiptrack.hide_order status -->
                    <?php
                    $key1 = array_search('1', array_column($result_shiptrack['data'], 'hide_order'));
                    $key0 = array_search('0', array_column($result_shiptrack['data'], 'hide_order'));
                    if(($key0 !== false || $key1 !== false) && $manually_flag) {
                        $key = ($key0 === false)? $key1 : $key0;
                        ?>
                        <div id = "hidden_status">
                                <div class="eye hidden">
                                    <div class="center-aligned">This order is viewed in mainGrid</div>
                                    <button value="1"><span class="eye-hide"></span> Hide</button>
                                </div>
                        <?php // } else if($isHiddenInMainGrid == 1) {
                            ?>
                                <div class="eye-close hidden">
                                    <div class="center-aligned">This order is hidden in mainGrid</div>
                                    <button value="0"><span class="eye-open"></span> Show</button>
                                </div>
                        <?php //}
                         ?>
                            <input type="hidden" name="hide_order" id="hide_order" value="<?php echo $result_shiptrack['data'][$key]['hide_order']; ?>" />
                            <input type="hidden" name="RefNumber" id="RefNumber" value="<?php echo $result_shiptrack['data'][$key]['RefNumber']; ?>" />
                        </div>
                    <?php } ?>

                <?php  }?>
					<?php
					if(isset($result_validation[0]['MESSAGE']) && !empty($result_validation[0]['MESSAGE']))
					{
                                            if(!empty($result_validation[0]['error_code']))
                                            {
                                                $error_codes = explode(",", $result_validation[0]['error_code']);
                                            } else
                                            {
                                                $error_codes=[];
                                            }
						$result_validation = $result_validation[0]['MESSAGE'];
						$result_validation = explode(",", $result_validation);
                                                
						echo '<span class="validation-message">
						<em>Validation:</em>';
						foreach($result_validation as $validation)
						{
							$value = trim($validation);
							echo '<span> - '.$value.'</span>';
						}
						echo '</span>';
                                            if(strlen(implode($error_codes)) != 0)
                                            {
                                                echo '<span class="validation-message">
						<em>Validation details:</em>';
						foreach($error_codes as $error_code)
						{
							$value = trim($error_code);
                                                        if(!empty($value))
							echo '<span><a target="_blank" href="../analyzer/index.php?error='.$value.'&po='. urlencode($po).'"><button>Details on error '.$value.'</button></a></span>';
						}
						echo '</span>';
                                            }
					}
					
					if(isset($result_warnings) && !empty($result_warnings))
					{
						echo '<span class="validation-message">
							<em>Warnings:</em>';
							foreach($result_warnings as $result_warning)
							{
								$value = trim($result_warning['Warning']);
								$code = $result_warning['Code'];
								$id   = $result_warning['ID'];
								
								$addAction = '';
								
								if( $code == 1 ) {
									$addAction = 'style="cursor:pointer" class="js-warning" data-code="1" data-po="'.$po.'" data-id="'.$id.'"';
								}
								
								echo '<span '.$addAction.'> - '.$value.'</span>';
								
							}
						echo '</span>';
					}
					
					if(isset($info_messages) && !empty($info_messages))
					{
						echo '<span class="validation-message">
							<em>Info:</em>';
							foreach($info_messages as $info_message)
							{
								$value = trim($info_message);
								echo '<span> - '.$value.'</span>';
							}
						echo '</span>';
					}
					?>
					<span class="status">
						<?php 
						if(isset($result_status) && !empty($result_status))
						{
							$last_status = end($result_status);
							$active_status = $last_status['Operation'];
						} else{
							$active_status = 'unknown';
						}
						?>
						Current Status: <em><?php echo $active_status; ?></em>
					</span>
					<?php 
					if(isset($result_status) && !empty($result_status))
					{
						echo '<div class="status-holder">';
						
							foreach($result_status as $status)
							{
                                                            if(!empty($status['OperationDateTime']))
                                                            {
								echo '
								<div class="status-box '.$status['Operation'].'">
									<strong>'.ucfirst($status['Operation']).'</strong>
									<span>Date: '.format_date($status['OperationDateTime']).'</span>';
								if($status['Reason'] != 'N/A' && !empty($status['Reason']))
									echo
									'<span>Reason: '.$status['Reason'].'</span>';
								if($status['Note'] != 'N/A' && !empty($status['Note']))
									echo
									'<span>Note: '.$status['Note'].'</span>';
								if($status['userName'] != 'N/A' && !empty($status['userName']))
									echo
									'<span>User: '.$status['userName'].'</span>';
								echo '</div>
								';
                                                            }
							}
						echo '</div>';
					}
					?>
				</div>
				<div class="right" id="tabs">
					<ul class="data-menu">
						<li><a class="source" href="#tabs-1">Order data at time of calculation (shiptrack)</a></li>
						<li><a class="work" href="#tabs-2">Current synched salesorder data (salesorder)</a></li>
						<li><a class="archive" href="#tabs-3">Current synched invoiced data (invoice)</a></li>
					</ul>
					<div id="tabs-1">
				<?php
				$addr_to_copy = array();
				$addr_string = '';

				if(isset($result_shiptrack['data']) && !empty($result_shiptrack['data']))
				{
					foreach($result_shiptrack['data'] as $key_temp0 => $shiptrack)
					{
                        //don't show the table row with order visibility status
                        unset($shiptrack['hide_order']);

                        if($key_temp0 > 0) $vertical_title0 = 'vertical_title_red'; else $vertical_title0 = 'vertical_title';
						$info = $result_shiptrack['info'];
						echo '<div class="order-block">
							<table>
								<thead>
									<tr>
										<th colspan="2">Order data <span class="db_table">'.$info.'</span></th>
									</tr>
								</thead>
								<tbody>';
								
						foreach($shiptrack as $key => $value)
						{
                                                        if($key != 'inventory_flag')
                                                        {
                                                            if($key == 'RefNumber' || $key == 'PONumber' || $key == 'IDKEY' || $key == 'TxnLineID' || $key == 'GroupIDKEY') $td_class = 'class="word_break"'; else $td_class = '';

                                                            if ($key == 'inventory' && $shiptrack['inventory_flag'] == 'negative')
                                                            {
                                                                $value = '<a target="_blank" href="/shipping_module/inventory_allocation.php?po='.urlencode($shiptrack['PONumber']).'&ref='.urlencode($shiptrack['RefNumber']).'"><i class="order-supply2" title="Not enough items for this order! Click for details. Calculated date: '.$value.'"></i></a>';
                                                            } else if($key == 'inventory' && $shiptrack['inventory_flag'] == 'positive')
                                                            {
                                                                $value = '<a target="_blank" href="/shipping_module/inventory_allocation.php?po='.urlencode($shiptrack['PONumber']).'&ref='.urlencode($shiptrack['RefNumber']).'"><i class="order-supply-ok" title="Inventory - OK. Calculated date: '.$value.'"></i></a>';
                                                            } else if($key == 'inventory'  && $shiptrack['inventory_flag'] == 'not_calculated')
                                                            {
                                                                $value = '<i class="order-supply-no" title="Inventory was not calculated for this order."></i>';
                                                            }

                                                            if ($key == 'inventory') $td_class = 'class="status1"';

                                                            echo '<tr>
                                                                    <td class="'.$vertical_title0.'">'.$key.'</td>
                                                                    <td '.$td_class.'>'.$value.'</td>
                                                            </tr>';
                                                        }

							switch( $key ) {
								case 'Addr1': $addr_to_copy[0] = $value; break;
								case 'Addr2': $addr_to_copy[1] = $value; break;
								case 'Addr3': $addr_to_copy[2] = $value; break;
								case 'City':  $addr_to_copy[3] = $value; break;
								case 'State': $addr_to_copy[4] = $value; break;
								case 'Zip':   $addr_to_copy[5] = $value; break;
							}
						}

						$addr_string = ( $addr_to_copy[0]=='' ? '' : $addr_to_copy[0].chr(10) )
									 . ( $addr_to_copy[1]=='' ? '' : $addr_to_copy[1].chr(10) )
									 . ( $addr_to_copy[2]=='' ? '' : $addr_to_copy[2].chr(10) )
									 . ( $addr_to_copy[3]=='' ? '' : $addr_to_copy[3].', ' )
									 . ( $addr_to_copy[4]=='' ? '' : $addr_to_copy[4].' ' )
									 . ( $addr_to_copy[5]=='' ? '' : $addr_to_copy[5] );
						echo '<tr>
								<td class="'.$vertical_title0.'">Copy Address</td>
								<td><button class="copy-btn1" data-clipboard-text="'.$addr_string.'">Copy to clipboard</button></td>
							</tr><script>var clipboard1 = new Clipboard(\'.copy-btn1\');clipboard1.on(\'success\', function(e) { console.log(e); });</script>';
						
						echo '</tbody>
								</table>
							</div>';
					}
				} else{
					echo '<div class="order-block">
						<table>
							<thead>
								<tr>
									<th colspan="2">Order data</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td colspan="2">No data</td>
								</tr>
							</tbody>
						</table>
					</div>';
				}
				
				if(isset($result_shiptrackwithlabel['data']) && !empty($result_shiptrackwithlabel['data']))
				{
					foreach($result_shiptrackwithlabel['data'] as $key_temp1 => $shiptrackwithlabel)
					{
						if($key_temp1 > 0) $vertical_title1 = 'vertical_title_red'; else $vertical_title1 = 'vertical_title';
						$info = $result_shiptrackwithlabel['info'];
						echo '<div class="order-block">
							<table>
								<thead>
									<tr>
										<th colspan="2">Calculated Order data <span class="db_table">'.$info.'</span></th>
									</tr>
								</thead>
								<tbody>';
								
						foreach($shiptrackwithlabel as $key => $value)
						{
                                                    if($key != 'inventory_flag')
                                                    {
							if($key == 'RefNumber' || $key == 'PO' || $key == 'IDKEY' || $key == 'TxnLineID' || $key == 'GroupIDKEY' || $key == 'SM Invoice#') $td_class = 'class="word_break"'; else $td_class = '';
                                                        
                                                        if ($key == 'inventory' && $shiptrackwithlabel['inventory_flag'] == 'negative')
                                                        {
                                                            $value = '<a target="_blank" href="/shipping_module/inventory_allocation.php?po='.urlencode($shiptrackwithlabel['PO']).'&ref='.urlencode($shiptrackwithlabel['RefNumber']).'"><i class="order-supply2" title="Not enough items for this order! Click for details. Calculated date: '.$value.'"></i></a>';
                                                        } else if($key == 'inventory' && $shiptrackwithlabel['inventory_flag'] == 'positive')
                                                        {
                                                            $value = '<a target="_blank" href="/shipping_module/inventory_allocation.php?po='.urlencode($shiptrackwithlabel['PO']).'&ref='.urlencode($shiptrackwithlabel['RefNumber']).'"><i class="order-supply-ok" title="Inventory - OK. Calculated date: '.$value.'"></i></a>';
                                                        } else if($key == 'inventory'  && $shiptrackwithlabel['inventory_flag'] == 'not_calculated')
                                                        {
                                                            $value = '<i class="order-supply-no" title="Inventory was not calculated for this order."></i>';
                                                        }
                                                        
                                                        if ($key == 'inventory') $td_class = 'class="status1"';
                                                        
							echo '<tr>
								<td class="'.$vertical_title1.'">'.$key.'</td>
								<td '.$td_class.'>'.$value.'</td>
							</tr>';
                                                    }
						}
						
						echo '<tr>
								<td class="'.$vertical_title1.'">Alternative rates</td>
								<td>'.$result_allcosts.'</td>
							</tr>';
						
						echo '</tbody>
								</table>
							</div>';
					}
				} else{
					echo '<div class="order-block">
						<table>
							<thead>
								<tr>
									<th colspan="2">Calculated order data</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td colspan="2">No data</td>
								</tr>
							</tbody>
						</table>
					</div>';
				}
				?>
				
				<div class="order-block order-content">
					<div class="order-table-box">
						<?php
						if(isset($result_groupdetail_DL['data']) && !empty($result_groupdetail_DL['data']))
						{
						$info = $result_groupdetail_DL['info'];
						echo '<table>
							<thead>
								<tr>
									<th colspan="9">Products in order <span class="db_table">'.$info.'</span></th>
								</tr>
							</thead>
							<tbody>
								<tr class="top-titles">';
						
						foreach($result_groupdetail_DL['data'][0] as $key => $groupdetail_DL)
						{
							if( $key != 'Related' ) {
								echo '<td>'.$key.'</td>';
							}
						}
						echo '</tr>';
						$ref0 = $result_groupdetail_DL['data'][0]['RefNumber'];
						foreach($result_groupdetail_DL['data'] as $groupdetail_DL)
						{
							if($groupdetail_DL['RefNumber'] != $ref0)
							{
								echo '<tr class="top-titles-red">';
								foreach($result_groupdetail_DL['data'][0] as $key => $groupdetail_DL0)
								{
									if( $key != 'Related' ) {
										echo '<td>'.$key.'</td>';
									}
								}
								echo '</tr>';
							}
							$isRealted = empty($groupdetail_DL['Related']) ? '' : 'class="related" title="This produt may be incompatible with SKU '.$groupdetail_DL['Related'].'"';
							echo '<tr '.$isRealted.'>';
								foreach($groupdetail_DL as $key => $value)
								{
									if( $key != 'Related' ) {
										if($key == 'RefNumber' || $key == 'PONumber' || $key == 'IDKEY' || $key == 'TxnLineID' || $key == 'GroupIDKEY') $td_class = 'class="word_break"'; else $td_class = '';
										echo '<td '.$td_class.'>'.$value.'</td>';
									}
								}
							echo '</tr>';
							$ref0 = $groupdetail_DL['RefNumber'];
						}
						echo '
						</tbody>
						</table>';
						} else{
							echo '<table>
								<thead>
									<tr>
										<th colspan="9">Products in order</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td colspan="9">No data</td>
									</tr>
								</tbody>
							</table>';
						}
						?>
					</div>
					<div class="order-table-box">
						<?php
						if(isset($result_linedetail['data']) && !empty($result_linedetail['data']))
						{
						$info = $result_linedetail['info'];
						echo '<table>
							<thead>
								<tr>
									<th colspan="8">Items in order <span class="db_table">'.$info.'</span></th>
								</tr>
							</thead>
							<tbody>
								<tr class="top-titles">';
						
						foreach($result_linedetail['data'][0] as $key => $linedetail_DL)
						{
							echo '<td>'.$key.'</td>';
						}
						echo '</tr>';
						$ref1 = $result_linedetail['data'][0]['RefNumber'];
						foreach($result_linedetail['data'] as $linedetail_DL)
						{
							if($linedetail_DL['RefNumber'] != $ref1)
							{
								echo '<tr class="top-titles-red">';
								foreach($result_linedetail['data'][0] as $key => $linedetail_DL1)
								{
									echo '<td>'.$key.'</td>';
								}
								echo '</tr>';
							}
							echo '<tr>';
								foreach($linedetail_DL as $key => $value)
								{
									if($key == 'RefNumber' || $key == 'PONumber' || $key == 'IDKEY' || $key == 'TxnLineID' || $key == 'GroupIDKEY') $td_class = 'class="word_break"'; else $td_class = '';
									echo '<td '.$td_class.'>'.$value.'</td>';
								}
							echo '</tr>';
							$ref1 = $linedetail_DL['RefNumber'];
						}
						echo '
						</tbody>
						</table>';
						} else{
							echo '<table>
								<thead>
									<tr>
										<th colspan="8">Items in order</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td colspan="8">No data</td>
									</tr>
								</tbody>
							</table>';
						}
						?>
					</div>
				</div>
			</div>
			<div id="tabs-2">
				<?php
				$addr_to_copy = array();
				$addr_string = '';

				if(isset($result_shiptrack_s['data']) && !empty($result_shiptrack_s['data']))
				{
					foreach($result_shiptrack_s['data'] as $key_temp2 => $shiptrack)
					{
						if($key_temp2 > 0) $vertical_title2 = 'vertical_title_red'; else $vertical_title2 = 'vertical_title';
						$info = $result_shiptrack_s['info'];
						echo '<div class="order-block">
							<table>
								<thead>
									<tr>
										<th colspan="2">Order data <span class="db_table">'.$info.'</span></th>
									</tr>
								</thead>
								<tbody>';
								
						foreach($shiptrack as $key => $value)
						{
							if($key == 'RefNumber' || $key == 'PONumber' || $key == 'IDKEY' || $key == 'TxnLineID' || $key == 'GroupIDKEY') $td_class = 'class="word_break"'; else $td_class = '';
							echo '<tr>
								<td class="'.$vertical_title2.'">'.$key.'</td>
								<td '.$td_class.'>'.$value.'</td>
							</tr>';

							switch( $key ) {
								case 'Addr1': $addr_to_copy[0] = $value; break;
								case 'Addr2': $addr_to_copy[1] = $value; break;
								case 'Addr3': $addr_to_copy[2] = $value; break;
								case 'City':  $addr_to_copy[3] = $value; break;
								case 'State': $addr_to_copy[4] = $value; break;
								case 'Zip':   $addr_to_copy[5] = $value; break;
							}
						}

						$addr_string = ( $addr_to_copy[0]=='' ? '' : $addr_to_copy[0].chr(10) )
									 . ( $addr_to_copy[1]=='' ? '' : $addr_to_copy[1].chr(10) )
									 . ( $addr_to_copy[2]=='' ? '' : $addr_to_copy[2].chr(10) )
									 . ( $addr_to_copy[3]=='' ? '' : $addr_to_copy[3].', ' )
									 . ( $addr_to_copy[4]=='' ? '' : $addr_to_copy[4].' ' )
									 . ( $addr_to_copy[5]=='' ? '' : $addr_to_copy[5] );
						echo '<tr>
								<td class="'.$vertical_title2.'">Copy Address</td>
								<td><button class="copy-btn2" data-clipboard-text="'.$addr_string.'">Copy to clipboard</button></td>
							</tr><script>var clipboard2 = new Clipboard(\'.copy-btn2\');clipboard2.on(\'success\', function(e) { console.log(e); });</script>';
						
						echo '</tbody>
								</table>
							</div>';
					}
				} else{
					echo '<div class="order-block">
						<table>
							<thead>
								<tr>
									<th colspan="2">Order data</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td colspan="2">No data</td>
								</tr>
							</tbody>
						</table>
					</div>';
				}
				
				/*if(isset($result_shiptrackwithlabel['data']) && !empty($result_shiptrackwithlabel['data']))
				{
					foreach($result_shiptrackwithlabel['data'] as $key_temp1 => $shiptrackwithlabel)
					{
						if($key_temp1 > 0) $vertical_title1 = 'vertical_title_red'; else $vertical_title1 = 'vertical_title';
						$info = $result_shiptrackwithlabel['info'];
						echo '<div class="order-block">
							<table>
								<thead>
									<tr>
										<th colspan="2">Calculated Order data <span class="db_table">'.$info.'</span></th>
									</tr>
								</thead>
								<tbody>';
								
						foreach($shiptrackwithlabel as $key => $value)
						{
							if($key == 'RefNumber' || $key == 'PONumber' || $key == 'IDKEY' || $key == 'TxnLineID' || $key == 'GroupIDKEY') $td_class = 'class="word_break"'; else $td_class = '';
							
                                                         if ($key == 'inventory' && !empty($value))
                                                        {
                                                            $value = '<a target="_blank" href="/shipping_module/inventory_allocation.php?po='.urlencode($shiptrackwithlabel['PO']).'&ref='.urlencode($shiptrackwithlabel['RefNumber']).'"><i class="order-supply2" title="Not enough items for this order! Click for details. Calculated date: '.$value.'"></i></a>';
                                                        } else if($key == 'inventory' && empty($value))
                                                        {
                                                            $value = '<i class="order-supply-ok" title="Inventory - OK!"></i>';
                                                        }
                                                        
                                                        if ($key == 'inventory') $td_class = 'class="status1"';
                                                        
                                                        echo '<tr>
								<td class="'.$vertical_title1.'">'.$key.'</td>
								<td '.$td_class.'>'.$value.'</td>
							</tr>';
						}
						
						echo '<tr>
								<td class="'.$vertical_title1.'">Alternative rates</td>
								<td>'.$result_allcosts.'</td>
							</tr>';
						
						echo '</tbody>
								</table>
							</div>';
					}
				} else{
					echo '<div class="order-block">
						<table>
							<thead>
								<tr>
									<th colspan="2">Calculated order data</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td colspan="2">No data</td>
								</tr>
							</tbody>
						</table>
					</div>';
				}*/
                                
                                echo '<div class="order-block"></div>';
				?>
				
				<div class="order-block order-content">
					<div class="order-table-box">
						<?php
						if(isset($result_groupdetail_DL_s['data']) && !empty($result_groupdetail_DL_s['data']))
						{
						$info = $result_groupdetail_DL_s['info'];
						echo '<table>
							<thead>
								<tr>
									<th colspan="8">Products in order <span class="db_table">'.$info.'</span></th>
								</tr>
							</thead>
							<tbody>
								<tr class="top-titles">';
						
						foreach($result_groupdetail_DL_s['data'][0] as $key => $groupdetail_DL)
						{
							if( $key != 'Related' ) {
								echo '<td>'.$key.'</td>';
							}
						}
						echo '</tr>';
						$ref7 = $result_groupdetail_DL_s['data'][0]['RefNumber'];
						foreach($result_groupdetail_DL_s['data'] as $groupdetail_DL)
						{
							if($groupdetail_DL['RefNumber'] != $ref7)
							{
								echo '<tr class="top-titles-red">';
								foreach($result_groupdetail_DL_s['data'][0] as $key => $groupdetail_DL0)
								{
									if( $key != 'Related' ) {
										echo '<td>'.$key.'</td>';
									}
								}
								echo '</tr>';
							}
							
							$isRealted = empty($groupdetail_DL['Related']) ? '' : 'class="related" title="This produt may be incompatible with SKU '.$groupdetail_DL['Related'].'"';
							echo '<tr '.$isRealted.'>';
								foreach($groupdetail_DL as $key => $value)
								{
									if( $key != 'Related' ) {
										if($key == 'RefNumber' || $key == 'PONumber' || $key == 'IDKEY' || $key == 'TxnLineID' || $key == 'GroupIDKEY') $td_class = 'class="word_break"'; else $td_class = '';
										echo '<td '.$td_class.'>'.$value.'</td>';
									}
								}
							echo '</tr>';
							$ref7 = $groupdetail_DL['RefNumber'];
						}
						echo '
						</tbody>
						</table>';
						} else{
							echo '<table>
								<thead>
									<tr>
										<th colspan="8">Products in order</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td colspan="8">No data</td>
									</tr>
								</tbody>
							</table>';
						}
						?>
					</div>
					<div class="order-table-box">
						<?php
						if(isset($result_linedetail_s['data']) && !empty($result_linedetail_s['data']))
						{
						$info = $result_linedetail_s['info'];
						echo '<table>
							<thead>
								<tr>
									<th colspan="8">Items in order <span class="db_table">'.$info.'</span></th>
								</tr>
							</thead>
							<tbody>
								<tr class="top-titles">';
						
						foreach($result_linedetail_s['data'][0] as $key => $linedetail_DL)
						{
							echo '<td>'.$key.'</td>';
						}
						echo '</tr>';
						$ref6 = $result_linedetail_s['data'][0]['RefNumber'];
						foreach($result_linedetail_s['data'] as $linedetail_DL)
						{
							if($linedetail_DL['RefNumber'] != $ref6)
							{
								echo '<tr class="top-titles-red">';
								foreach($result_linedetail_s['data'][0] as $key => $linedetail_DL0)
								{
									echo '<td>'.$key.'</td>';
								}
								echo '</tr>';
							}
							echo '<tr>';
								foreach($linedetail_DL as $key => $value)
								{
									if($key == 'RefNumber' || $key == 'PONumber' || $key == 'IDKEY' || $key == 'TxnLineID' || $key == 'GroupIDKEY') $td_class = 'class="word_break"'; else $td_class = '';
									echo '<td '.$td_class.'>'.$value.'</td>';
								}
							echo '</tr>';
							$ref6 = $linedetail_DL['RefNumber'];
						}
						echo '
						</tbody>
						</table>';
						} else{
							echo '<table>
								<thead>
									<tr>
										<th colspan="8">Items in order</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td colspan="8">No data</td>
									</tr>
								</tbody>
							</table>';
						}
						?>
					</div>
				</div>
			</div>
			<div id="tabs-3">
				<?php
				$addr_to_copy = array();
				$addr_string = '';

				if(isset($result_shiptrack_i['data']) && !empty($result_shiptrack_i['data']))
				{
					foreach($result_shiptrack_i['data'] as $key_temp3 => $shiptrack)
					{
						if($key_temp3 > 0) $vertical_title3 = 'vertical_title_red'; else $vertical_title3 = 'vertical_title';
						$info = $result_shiptrack_i['info'];
						echo '<div class="order-block">
							<table>
								<thead>
									<tr>
										<th colspan="2">Order data <span class="db_table">'.$info.'</span></th>
									</tr>
								</thead>
								<tbody>';
								
						foreach($shiptrack as $key => $value)
						{
							if($key == 'QB Invoice#' || $key == 'PONumber' || $key == 'IDKEY' || $key == 'TxnLineID' || $key == 'GroupIDKEY') $td_class = 'class="word_break"'; else $td_class = '';
							echo '<tr>
								<td class="'.$vertical_title3.'">'.$key.'</td>
								<td '.$td_class.'>'.$value.'</td>
							</tr>';

							switch( $key ) {
								case 'Addr1': $addr_to_copy[0] = $value; break;
								case 'Addr2': $addr_to_copy[1] = $value; break;
								case 'Addr3': $addr_to_copy[2] = $value; break;
								case 'City':  $addr_to_copy[3] = $value; break;
								case 'State': $addr_to_copy[4] = $value; break;
								case 'Zip':   $addr_to_copy[5] = $value; break;
							}
						}

						$addr_string = ( $addr_to_copy[0]=='' ? '' : $addr_to_copy[0].chr(10) )
									 . ( $addr_to_copy[1]=='' ? '' : $addr_to_copy[1].chr(10) )
									 . ( $addr_to_copy[2]=='' ? '' : $addr_to_copy[2].chr(10) )
									 . ( $addr_to_copy[3]=='' ? '' : $addr_to_copy[3].', ' )
									 . ( $addr_to_copy[4]=='' ? '' : $addr_to_copy[4].' ' )
									 . ( $addr_to_copy[5]=='' ? '' : $addr_to_copy[5] );
						echo '<tr>
								<td class="'.$vertical_title3.'">Copy Address</td>
								<td><button class="copy-btn3" data-clipboard-text="'.$addr_string.'">Copy to clipboard</button></td>
							</tr><script>var clipboard3 = new Clipboard(\'.copy-btn3\');clipboard3.on(\'success\', function(e) { console.log(e); });</script>';
						echo '<tr>
								<td class="'.$vertical_title3.'">Invoice PDF</td>
								<td>
                                                                    <a class="osh_invoice_opn" id="'.('gen_invoice_'.$po).'" onclick="generateInvoice( \''.$po.'\' )"><button>Generate</button></a>
                                                                    <a class="osh_invoice_opn" id="'.('open_invoice_'.$po).'" href="#" target="_blank" style="display:none;"><button>Open</button></a>
                                                                </td>
							</tr>
                                                        <script>
                                                            checkInvoicePdf( \''.$po.'\' );
                                                        </script>';
						echo '</tbody>
								</table>
							</div>';
					}
				} else{
					echo '<div class="order-block">
						<table>
							<thead>
								<tr>
									<th colspan="2">Order data</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td colspan="2">No data</td>
								</tr>
							</tbody>
						</table>
					</div>';
				}
				
				/*if(isset($result_shiptrackwithlabel['data']) && !empty($result_shiptrackwithlabel['data']))
				{
					foreach($result_shiptrackwithlabel['data'] as $key_temp1 => $shiptrackwithlabel)
					{
						if($key_temp1 > 0) $vertical_title1 = 'vertical_title_red'; else $vertical_title1 = 'vertical_title';
						$info = $result_shiptrackwithlabel['info'];
						echo '<div class="order-block">
							<table>
								<thead>
									<tr>
										<th colspan="2">Calculated Order data <span class="db_table">'.$info.'</span></th>
									</tr>
								</thead>
								<tbody>';
								
						foreach($shiptrackwithlabel as $key => $value)
						{
							if($key == 'RefNumber' || $key == 'PONumber' || $key == 'IDKEY' || $key == 'TxnLineID' || $key == 'GroupIDKEY') $td_class = 'class="word_break"'; else $td_class = '';
							
                                                         if ($key == 'inventory' && !empty($value))
                                                        {
                                                            $value = '<a target="_blank" href="/shipping_module/inventory_allocation.php?po='.urlencode($shiptrackwithlabel['PO']).'&ref='.urlencode($shiptrackwithlabel['RefNumber']).'"><i class="order-supply2" title="Not enough items for this order! Click for details. Calculated date: '.$value.'"></i></a>';
                                                        } else if($key == 'inventory' && empty($value))
                                                        {
                                                            $value = '<i class="order-supply-ok" title="Inventory - OK!"></i>';
                                                        }
                                                        
                                                        if ($key == 'inventory') $td_class = 'class="status1"';
                                                        
                                                        echo '<tr>
								<td class="'.$vertical_title1.'">'.$key.'</td>
								<td '.$td_class.'>'.$value.'</td>
							</tr>';
						}
						
						echo '<tr>
								<td class="'.$vertical_title1.'">Alternative rates</td>
								<td>'.$result_allcosts.'</td>
							</tr>';
						
						echo '</tbody>
								</table>
							</div>';
					}
				} else{
					echo '<div class="order-block">
						<table>
							<thead>
								<tr>
									<th colspan="2">Calculated order data</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td colspan="2">No data</td>
								</tr>
							</tbody>
						</table>
					</div>';
				}*/
                                
                                echo '<div class="order-block"></div>';
				?>
				
				<div class="order-block order-content">
					<div class="order-table-box">
						<?php
						if(isset($result_groupdetail_DL_i['data']) && !empty($result_groupdetail_DL_i['data']))
						{
						$info = $result_groupdetail_DL_i['info'];
						echo '<table>
							<thead>
								<tr>
									<th colspan="8">Products in order <span class="db_table">'.$info.'</span></th>
								</tr>
							</thead>
							<tbody>
								<tr class="top-titles">';
						
						foreach($result_groupdetail_DL_i['data'][0] as $key => $groupdetail_DL)
						{
							if( $key != 'Related' ) {
								echo '<td>'.$key.'</td>';
							}
						}
						echo '</tr>';
						$ref5 = $result_groupdetail_DL_i['data'][0]['QB Invoice#'];
						foreach($result_groupdetail_DL_i['data'] as $groupdetail_DL)
						{
							if($groupdetail_DL['QB Invoice#'] != $ref5)
							{
								echo '<tr class="top-titles-red">';
								foreach($result_groupdetail_DL_i['data'][0] as $key => $groupdetail_DL0)
								{
									if( $key != 'Related' ) {
										echo '<td>'.$key.'</td>';
									}
								}
								echo '</tr>';
							}
							
							$isRealted = empty($groupdetail_DL['Related']) ? '' : 'class="related" title="This produt may be incompatible with SKU '.$groupdetail_DL['Related'].'"';
							echo '<tr '.$isRealted.'>';
								foreach($groupdetail_DL as $key => $value)
								{
									if( $key != 'Related' ) {
										if($key == 'QB Invoice#' || $key == 'PONumber' || $key == 'IDKEY' || $key == 'TxnLineID' || $key == 'GroupIDKEY') $td_class = 'class="word_break"'; else $td_class = '';
										echo '<td '.$td_class.'>'.$value.'</td>';
									}
								}
							echo '</tr>';
							$ref5 = $groupdetail_DL['QB Invoice#'];
						}
						echo '
						</tbody>
						</table>';
						} else{
							echo '<table>
								<thead>
									<tr>
										<th colspan="8">Products in order</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td colspan="8">No data</td>
									</tr>
								</tbody>
							</table>';
						}
						?>
					</div>
					<div class="order-table-box">
						<?php
						if(isset($result_linedetail_i['data']) && !empty($result_linedetail_i['data']))
						{
						$info = $result_linedetail_i['info'];
						echo '<table>
							<thead>
								<tr>
									<th colspan="8">Items in order <span class="db_table">'.$info.'</span></th>
								</tr>
							</thead>
							<tbody>
								<tr class="top-titles">';
						
						foreach($result_linedetail_i['data'][0] as $key => $linedetail_DL)
						{
							echo '<td>'.$key.'</td>';
						}
						echo '</tr>';
						$ref4 = $result_linedetail_i['data'][0]['QB Invoice#'];
						foreach($result_linedetail_i['data'] as $linedetail_DL)
						{
							if($linedetail_DL['QB Invoice#'] != $ref4)
							{
								echo '<tr class="top-titles-red">';
								foreach($result_linedetail_i['data'][0] as $key => $linedetail_DL0)
								{
									echo '<td>'.$key.'</td>';
								}
								echo '</tr>';
							}
							echo '<tr>';
								foreach($linedetail_DL as $key => $value)
								{
									if($key == 'QB Invoice#' || $key == 'PONumber' || $key == 'IDKEY' || $key == 'TxnLineID' || $key == 'GroupIDKEY') $td_class = 'class="word_break"'; else $td_class = '';
									echo '<td '.$td_class.'>'.$value.'</td>';
								}
							echo '</tr>';
							$ref4 = $linedetail_DL['QB Invoice#'];
						}
						echo '
						</tbody>
						</table>';
						} else{
							echo '<table>
								<thead>
									<tr>
										<th colspan="8">Items in order</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td colspan="8">No data</td>
									</tr>
								</tbody>
							</table>';
						}
						?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="reprintBackground" style="position: absolute; left: 0px; top: 0px; width: 100%; height: 100%; z-index: 1999; display: none; background: rgba(0, 0, 0, 0.5);">
				<div id="reprintMessageBox">
					<span id="messageSpan" onclick="reprintCancel()">x</span>
					<input type="hidden" name="reprintPO" id="reprintPO">
					<input type="hidden" name="reprintPath" id="reprintPath">
					<label>Please choose reason:</label>
					<center>
						<select onchange="showReprintReasonNote()" id="reasonSelectreprint">
						  <?php
							$reprint_reasons = '';
							foreach ($settings as $setting)
							{
								if ($setting['group_name'] == 'reprint_reasons' && $setting['type_name'] == 'reasons')
								$reprint_reasons = $setting['setting_value'];
							}
							$reprintReasons = explode(",", $reprint_reasons);
							foreach ($reprintReasons as $reprintReason)
							{
								echo '<option value="'.$reprintReason.'">'.$reprintReason.'</option>';
							}
							?>
						</select>
					</center>
					<label id="reprintLabel" style="display: none;">Please type reason:</label>
					<textarea id="reasonNotereprint" style="display: none; resize: none;" rows="7" cols="41" name="text" maxlength="255"></textarea>
					<br>
					<br>
					<button onclick="reprintOrder(<?php echo "'".$auth->getName()."'"; ?>)">OK</button>
				</div>
			</div>

<?php
} else
{
	echo "Can not get PO.";
}
?>
<?php require_once($path.'/Include/footer_section.php');?>
