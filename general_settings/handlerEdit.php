<?php
session_start();
$path = $_SERVER['DOCUMENT_ROOT'];
require_once($path.'/db_connect/connect.php');
require_once($path.'/auth/auth.php');
$auth = new AuthClass();

if ($auth->isAuth()) {

	if (isset($_GET['id']) && !empty($_GET['id']) && ((isset($_GET['value']) && !empty($_GET['value'])) || $_GET['value'] == 0) && isset($_GET['description']))
	{
		$id = $_GET['id'];
		$value = $_GET['value'];
		$description = $_GET['description'];
		$conn = Database::getInstance()->dbc;
		updateRow($conn, $id, $value, $description);
		header("Location: /general_settings/");
		die();
	} else die('Can not get parameters for request.');
}

function updateRow($conn, $id, $value, $description)
{
		$query = "UPDATE [dbo].[general_settings] SET [setting_value] = '".$value."', [description] = '".$description."' WHERE [id_sm_settings] = ".$id;
		$result = $conn->prepare($query);
		$result->execute();
}
?>