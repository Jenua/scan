<?php
session_start();
$path = $_SERVER['DOCUMENT_ROOT'];
require_once($path.'/db_connect/connect.php');
require_once($path.'/auth/auth.php');
$auth = new AuthClass();

if ($auth->isAuth()) {

	if (
		isset($_GET['app']) && 
		!empty($_GET['app']) &&
		isset($_GET['group']) && 
		!empty($_GET['group']) && 
		isset($_GET['type']) && 
		!empty($_GET['type']) && 
		isset($_GET['value']) && 
		!empty($_GET['value']) && isset($_GET['description'])
		)
	{
		$app = $_GET['app'];
		$group = $_GET['group'];
		$type = $_GET['type'];
		$value = $_GET['value'];
		$description = $_GET['description'];
		$conn = Database::getInstance()->dbc;
		insertRow($conn, $app, $group, $type, $value, $description);
		header("Location: /general_settings/");
		die();
	} else die('Can not get parameters for request.');
}
function insertRow($conn, $app, $group, $type, $value, $description)
{
		$query = "INSERT INTO [dbo].[general_settings]
           ([app]
		   ,[group_name]
           ,[type_name]
           ,[setting_value]
		   ,[description])
     VALUES
           ('".$app."'
		   ,'".$group."'
           ,'".$type."'
           ,'".$value."'
		   ,'".$description."')";
		$result = $conn->prepare($query);
		$result->execute();
}
?>