<?php
session_start();
$path = $_SERVER['DOCUMENT_ROOT'];
require_once($path.'/db_connect/connect.php');
require_once($path.'/auth/auth.php');
$auth = new AuthClass();

if ($auth->isAuth()) {

	if (isset($_GET['id']) && !empty($_GET['id']))
	{
		$id = $_GET['id'];
		$conn = Database::getInstance()->dbc;
		deleteRow($conn, $id);
		header("Location: /general_settings/");
		die();
	} else die('Can not get parameters for request.');
}

function deleteRow($conn, $id)
{
		$query = "DELETE FROM [dbo].[general_settings] WHERE [id_sm_settings] = ".$id;
		$result = $conn->prepare($query);
		$result->execute();
}
?>