<?php
session_start();
$path = $_SERVER['DOCUMENT_ROOT'];
require_once($path.'/db_connect/connect.php');
require_once($path.'/auth/auth.php');

$auth = new AuthClass();
 
if (isset($_POST["login"]) && isset($_POST["password"])) { //Если логин и пароль были отправлены
    if (!$auth->auth($_POST["login"], $_POST["password"])) { //Если логин и пароль введен не правильно
        echo "<script>alert('Wrong login or password!')</script>";
    }
}
 
if (isset($_GET["is_exit"])) { //Если нажата кнопка выхода
    if ($_GET["is_exit"] == 1) {
        $auth->out(); //Выходим
        header("Location: ?is_exit=0"); //Редирект после выхода
    }
}

if ($auth->isAuth()) { // Если пользователь авторизован, приветствуем:  
			echo "Hello, " . $auth->getLogin() .". <a href='?is_exit=1'>Exit</a>"; //Показываем кнопку выхода

	$deleteArray = array('bol_amazon', 'order_processing_mode', 'freight_setting', 'origin_address', 'pro_numbers_estes', 'pro_numbers_fedex', 'pro_numbers_rl', 'pro_numbers_nemf', 'pro_numbers_ups', 'pro_numbers_yrc', 'pro_numbers_odfl', 'query_mode');
	$addArray = array('bol_amazon', 'order_processing_mode', 'freight_setting', 'origin_address', 'pro_numbers_estes', 'pro_numbers_fedex', 'pro_numbers_rl', 'pro_numbers_nemf', 'pro_numbers_ups', 'pro_numbers_yrc', 'pro_numbers_odfl', 'query_mode');

	$conn = Database::getInstance()->dbc;
	$settings = getSettings($conn);
	echo '<br><a href="/order_details/">Back to Custom Report Builder</a><br><br>';
	echo '<br><br><a href="/shipsingle/shipsingle.php">Run shipsingle.php</a><br><br>';
	displaySettings($settings, $deleteArray, $addArray);
} else { //Если не авторизован, показываем форму ввода логина и пароля
?>
<div style="position: relative; top: 110px; width: 100%;">
<div style="
position: relative;
width: 330px;
height: 160px;
left: 50%;
margin-left: -160px;
padding: 10px;
background: #367FBB;
">
<form method="post" action="">
<fieldset>
    <p><label class="field" for="login">Login:</label><input class="logininput" type="text" name="login"
    value="<?php echo (isset($_POST["login"])) ? $_POST["login"] : null; // Заполняем поле по умолчанию ?>" /></p>
    <p><label class="field" for="password">Password:</label><input class="logininput" type="password" name="password" value="" /></p>
	<p><label class="field" for="password"> </label><input class="loginsubmit" type="submit" value="login" /></p>
</fieldset>
</form>
</div>
</div>
<?php 
}

function changeColor($color)
{
	if ($color == '#fafafa') return '#dbdbdb'; else return '#fafafa';
}

function displaySettings($settings, $deleteArray, $addArray)
{
$prevGroup = '';
$color = '#fafafa';
echo '<table border="1" cellpadding="5">
		<tr>
			<th>ID</th>
			<th>APP</th>
			<th>GROUP</th>
			<th>TYPE</th>
			<th>VALUE</th>
			<th>DESCRIPTION</th>
			<th>DELETE</th>
			<th>UPDATE VALUE</th>
			<th>CLONE WITH EMPTY VALUE</th>
			<th>CREATE NEW EMPTY</th>
		</tr>';
	foreach ($settings as $setting)
	{
		if ($prevGroup != $setting['group_name'])
		{
			$color = changeColor($color);
			$prevGroup = $setting['group_name'];
		}
		echo '<tr style="background: '.$color.';">';
		echo '
		<td>'.$setting['id_sm_settings'].'</td>
		<td>'.$setting['app'].'</td>
		<td>'.$setting['group_name'].'</td>
		<td>'.$setting['type_name'].'</td>
		<td><b>'.$setting['setting_value'].'</b></td>
		<td>'.$setting['description'].'</td>';
		
		if (!in_array($setting['group_name'], $deleteArray))
		{
			echo '<td><form action="delete.php">
				<input type="hidden" name="id" value="'.$setting['id_sm_settings'].'">
				<input style="background: red; cursor: pointer;" type="submit" value="DELETE">
				</form></td>';
			
			//echo '<td><a href="delete.php?id='.$setting['id_sm_settings'].'">DELETE</a></td>';
		} else 
		{
			echo '<td>N/A</td>';
		}
		
		echo '<td><form action="edit.php">
			<input type="hidden" name="id" value="'.$setting['id_sm_settings'].'">
			<input type="hidden" name="value" value="'.$setting['setting_value'].'">
			<input type="hidden" name="description" value="'.$setting['description'].'">
			<input style="background: orange; cursor: pointer;" type="submit" value="UPDATE VALUE">
			</form></td>';
		
		//echo '<td><a href="edit.php?id='.$setting['id_sm_settings'].'&value='.$setting['setting_value'].'&description='.$setting['description'].'">UPDATE VALUE</a></td>';
		
		if (!in_array($setting['group_name'], $addArray))
		{
			echo '<td><form action="add.php">
				<input type="hidden" name="app" value="'.$setting['app'].'">
				<input type="hidden" name="group" value="'.$setting['group_name'].'">
				<input type="hidden" name="type" value="'.$setting['type_name'].'">
				<input type="hidden" name="description" value="'.$setting['description'].'">
				<input style="background: yellow; cursor: pointer;" type="submit" value="CLONE WITH EMPTY VALUE">
				</form></td>';
			
			//echo '<td><a href="add.php?app='.$setting['app'].'&group='.$setting['group_name'].'&type='.$setting['type_name'].'&description='.$setting['description'].'">CLONE WITH EMPTY VALUE</a></td>';
		} else 
		{
			echo '<td>N/A</td>';
		}
		echo '<td><form action="create.php">
				<input type="hidden" name="app" value="'.$setting['app'].'">
				<input type="hidden" name="group" value="'.$setting['group_name'].'">
				<input type="hidden" name="type" value="'.$setting['type_name'].'">
				<input type="hidden" name="description" value="'.$setting['description'].'">
				<input style="background: lightgreen; cursor: pointer;" type="submit" value="CREATE NEW EMPTY">
				</form></td>';
				
		//echo '<td><a href="create.php">CREATE NEW EMPTY</a></td>';
		echo '</tr>';
	}
echo '</table>';
}

function getSettings($conn)
{
		$query = "SELECT * FROM [dbo].[general_settings] ORDER BY [group_name]";
		$result = $conn->prepare($query);
		$result->execute();
		$result = $result->fetchAll(PDO::FETCH_ASSOC);
		return $result;
}
?>

   	