USE [orders]
GO
/**
MS SQL Nested set model
Nested sets of scanned codes
*/
CREATE TABLE Scanned_Tree (
ID int IDENTITY(1,1) NOT NULL,
CodeName VARCHAR(100) NOT NULL,
LeftKey tinyint NOT NULL DEFAULT 0,
RightKey tinyint NOT NULL DEFAULT 0,
[Level] tinyint NOT NULL DEFAULT 0,
CONSTRAINT [PK_ST_ID] PRIMARY KEY (ID),
INDEX LeftKey (LeftKey, RightKey, [Level])
)
GO
Insert into Scanned_Tree(CodeName,LeftKey,RightKey,[Level]) values('PONumber',1,8,1),('Truck',2,7,2), ('Pallet',3,6,3), ('UPC',4,5,4)
GO

/* 07/28/2017 */
USE [orders]
/*UP*/
CREATE TABLE [dbo].[Scan_History](
	[ID] int NOT NULL IDENTITY,
	[ScannedTreeID] [int] NOT NULL,
  ParentID INT DEFAULT NULL,
  Code VARCHAR(50) NOT NULL,
  ScanLevel VARCHAR(5) DEFAULT NULL,
  QTY INT default 1
	CONSTRAINT [PK_SH_ID] PRIMARY KEY (ID)
)
GO
/*DOWN*/
DROP TABLE Scan_History
GO
/*07/31/2017*/
USE [orders]
GO
/**/CREATE TABLE [dbo].[Orders_loaded_full](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PalletUPCID] [int] NULL,
	[POnumber] [varchar](50) NOT NULL,
	[refnumber] varchar(50) DEFAULT NULL,
	[Truck] VARCHAR (50) NOT NULL,
	[Pallet] VARCHAR (50) NOT NULL,
	[DateCreated] [datetime] NULL CONSTRAINT [DF_Orders_loaded_full_DateCreated]  DEFAULT (getdate()),
	[comment] [nvarchar](255) NULL,
	[UserID] [int] NULL,
 CONSTRAINT [PK_loaded_full_ID] PRIMARY KEY (ID))

GO
ALTER TABLE [Orders_loaded_full] ADD CONSTRAINT FK_palletUPCID FOREIGN KEY (PalletUPCID) REFERENCES Scan_History(ID)
GO
