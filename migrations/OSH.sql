USE [orders]
GO
alter table [dbo].[manually_shiptrack] add [hide_order] bit default 0
GO
update [dbo].[manually_shiptrack] set [hide_order] = 0 where [hide_order] is null
GO