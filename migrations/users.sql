/**
up
 */
USE [orders]
GO
-- Table structure for table `permissions`
--

CREATE TABLE [permissions] (
  [id] int NOT NULL IDENTITY PRIMARY KEY,
  [name] varchar(150) NOT NULL
)
GO
--
-- Dumping data for table `permissions`
--

INSERT INTO [permissions] (name) VALUES
('Administrator'),
('User'),
('Warehouse'),
('Logist')
GO
CREATE TABLE [user_permission_matches] (
  [id] int NOT NULL IDENTITY PRIMARY KEY,
  [user_id] int NOT NULL,
  [permission_id] int NOT NULL
)
GO
CREATE TABLE [user_permission_modules] (
  id int NOT NULL IDENTITY PRIMARY KEY,
  permission_id int NOT NULL,
  module_name varchar(50) NOT NULL
)
GO
INSERT INTO user_permission_modules ( [permission_id], [module_name]) VALUES

( 1, 'rits'),
( 1, 'packing'),
( 1, 'picking');

GO
/*
SELECT [ID] as [user_id]
       ,[printer_label]
      ,[printer_bol]
      ,[printer_hddc1]
      ,[printer_hddc2]
      ,[rits_printer]
      ,[rits_print_copies]
      into user_settings
  FROM [User]

GO*/
    ALTER TABLE [user] ADD join_date datetime
GO
    ALTER TABLE [user] ADD last_login datetime

/* down */
DROP TABLE permissions
GO
/* 22/05/2017 */
/* up */
CREATE TABLE [user_configuration] (
  id int NOT NULL IDENTITY,
  [name] VARCHAR (50) NOT NULL,
  [value] VARCHAR (250) NOT NULL,
  CONSTRAINT configuration_pk PRIMARY KEY (id)
)
GO

INSERT INTO [user_configuration] VALUES ('US_LOGOFF_TIME_1','15:00')
GO
INSERT INTO [user_configuration] VALUES ('US_LOGOFF_TIME_2','20:00')
GO

CREATE TABLE user_permission_configurations (
    permission_id int NOT NULL,
    configuration_id int NOT NULL
)
GO
ALTER TABLE [dbo].[user_configuration] ADD UNIQUE (name)
GO
/* down */
ALTER TABLE [user_configuration]
 DROP CONSTRAINT ixu_perm_conf
go
DROP TABLE [user_configuration]
GO
/**/
USE [Orders]
ALTER TABLE [dbo].[user] ADD [is_active] bit not NULL DEFAULT (1)
/* 08/17/2017
UP: Configuration for trigger Orders_Scan_History */
USE [Orders]
insert into  user_configuration(name,value,permission_id)   values ('ON_ORDERS_SCAN_HISTORY',0,1)
update [user_configuration] set permission_id = 1
