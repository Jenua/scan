USE [orders31]
GO

/****** Object:  Table [dbo].[Orders_loaded]    Script Date: 4/17/2017 10:32:49 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Orders_loaded](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[POnumber] [nvarchar](100) NULL,
	[DateCreated] [datetime] NULL CONSTRAINT [DF_Orders_loaded_DateCreated]  DEFAULT (getdate()),
	[comment] [nvarchar](255) NULL,
	[UserID] [int] NULL,
 CONSTRAINT [PK_loaded_ID] PRIMARY KEY(ID) )

GO
/*29/05/2017*/
ALTER TABLE [dbo].[Orders_loaded] ADD [PalletID] VARCHAR (50) NULL
ALTER TABLE [dbo].[Orders_loaded] ADD [TruckID] VARCHAR (50) NULL
/*17/06/17*/
USE [orders31]
GO
drop table [dbo].[Orders_loaded]
GO
CREATE TABLE [dbo].[Orders_loaded] (
	[ID]          [int] IDENTITY (1, 1) NOT NULL,
	[POnumber]    [nvarchar](100)       NULL,
	[PROnumber]   [nvarchar](100)       NOT NULL,
	[DateCreated] [datetime]            NULL CONSTRAINT [DF_Orders_loaded_DateCreated] DEFAULT (getdate()),
	[comment]     [nvarchar](255)       NULL,
	[UserID]      [int]                 NULL,
	CONSTRAINT [PK_loaded_ID] PRIMARY KEY(ID)
)

/*07062017*/
USE [orders31]
GO
CREATE TABLE Inventory_activity(
ID int IDENTITY(1,1) PRIMARY KEY,
SKU VARCHAR(25) NULL, --25 symbols
BIN VARCHAR(100) NULL,  --10 symbols
POnumber VARCHAR(100) NULL,  --10 symbols
QTY INT NOT NULL DEFAULT 0,
TransType VARCHAR(10),
DateCreated [datetime] NULL CONSTRAINT [DF_inventory_DateCreated] DEFAULT GETDATE(), -- default current date
UserID INT   -- user name who scanning.
)
GO
/*down*/
DROP TABLE Inventory_activity
GO
/*13072017*/
USE [orders31]
GO
CREATE TABLE Inventory_action (
ID int IDENTITY(1,1) NOT NULL,
name VARCHAR(20) NOT NULL,
[description] TEXT NULL,
CONSTRAINT [PK_ACT_ID] PRIMARY KEY (ID))
GO
INSERT INTO Inventory_action (name, description) VALUES
('RCVD','Receiving'),
('PICK','Shelf Picking'),
('RPL+','Add to shelf'),
('RPL-','Remove from shelf')
GO
ALTER TABLE Inventory_action ADD [title] VARCHAR(50) NULL
GO
UPDATE Inventory_action SET title = [description]
GO
-- RENAME Inventory_activity.[TransType] and add FK on it
 EXEC sys.sp_rename
    @objname = N'dbo.Inventory_activity.TransType',
    @newname = 'TransTypeID',
    @objtype = 'COLUMN'
GO
DELETE FROM Inventory_activity
GO
ALTER TABLE Inventory_activity ADD [TransTypeID] INT NULL
GO
ALTER TABLE Inventory_activity ADD CONSTRAINT FK_TransType FOREIGN KEY (TransTypeID) REFERENCES Inventory_action(ID)
GO
/*21072017*/
USE [orders31]
ALTER TABLE Inventory_activity ADD [Pallet] INT NULL
GO
/*down*/
alter table  [Inventory_activity] DROP CONSTRAINT [FK_TransType]
DROP TABLE Inventory_action

USE [orders31]
GO
CREATE TABLE [dbo].[Orders_loaded_full](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[POnumber] [nvarchar](100) NULL,
	[Truck] VARCHAR (50) NOT NULL,
	[Pallet] VARCHAR (50) NOT NULL,
	[DateCreated] [datetime] NULL CONSTRAINT [DF_Orders_loaded_full_DateCreated]  DEFAULT (getdate()),
	[comment] [nvarchar](255) NULL,
	[UserID] [int] NULL,
 CONSTRAINT [PK_loaded_full_ID] PRIMARY KEY (ID))

GO
/*07/19/2017*/
USE [orders31]
GO
ALTER TABLE Inventory_action ADD [sort] tinyint default 100 NOT NULL
update Inventory_action set sort = 1 where id = 1
update Inventory_action set sort = 10 where id = 4
update Inventory_action set sort = 11 where id = 3
/*rename Picking*/
update Inventory_action set title = 'Shelf picking' where title = 'Picking'
update Inventory_action set [description] = 'Shelf picking' where id=2

/*09062017*/
use [orders]
CREATE TABLE [Orders_Scan_History] (
	[ID] int NOT NULL identity,
	[PrintID] int default NULL,
	[PickID] int default NULL,
	[PackID] int default NULL,
	[LoadID] int default NULL,
	[ShippID] int default NULL,
	[TimeCreated] [datetime] NULL constraint [DF_OScanH_TimeCreated] DEFAULT (getdate()),
	[TimeFinished] [datetime] default NULL,
	[Status] [nvarchar](255) NULL,
	[PONumber] varchar(250) NOT NULL,
	[refnumber] varchar(250) DEFAULT NULL
);
/*down*/
DROP table [Orders_Scan_History] ;
