/**
up
 */
USE [orders]
GO
CREATE TABLE orders.dbo.TRN_CREATEMEMO(
ID int IDENTITY(1,1) PRIMARY KEY,
SKU varchar(25) not null, --25 symbols
LOT varchar(10) not null,  --10 symbols
ENTERED_DATE date DEFAULT GETDATE(), -- default current date
MODIFIED_DATE date,    -- when the record was lastly updated for SKU or LOT columns, ideally enter_date= modified_date. On Insert place to sysdate.
MODIFIED_BY varchar(50),   -- user name who either entered the record or last modified SKU or LOT columns.On insert place user id of the insert app.
LABEL_PRINT_DATE date DEFAULT NULL, -- default blank, populated when label printed
REPORTED_TO_ACCT_DATE date DEFAULT NULL -- when the id was sent in the report form to accounting
)
GO

ALTER TABLE dbo.TRN_CREATEMEMO ADD [MARK] bit DEFAULT 0 NOT NULL
/* down */
USE [orders]
GO

DROP TABLE [dbo].[TRN_CREATEMEMO]
GO
/* up 010317 */
USE [orders]
ALTER TABLE dbo.[User] ADD [rits_printer] VARCHAR(50)
ALTER TABLE dbo.[User] ADD [rits_print_copies] tinyint
/* down 010317 */
USE [orders]
GO
ALTER TABLE dbo.[User] DROP column [rits_printer], [rits_print_copies]
GO

/*up 02032017*/
USE [orders31]
GO

/****** Object:  Table [dbo].[Orders_packed]    Script Date: 3/2/2017 10:07:14 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Orders_packed](
	[ID] INT IDENTITY PRIMARY KEY,
	[POnumber] [nvarchar](100) NOT NULL,
	[refnumber] [nvarchar](100) NULL,
	[DateCreated] [datetime] DEFAULT GETDATE(),
	[comment] [nvarchar](255) NULL,
	[UserID] [int] NULL
) ON [PRIMARY]

GO
/*down 02032017*/
USE [orders31]
GO
drop table [dbo].[Orders_packed]
go

/*up 09032017*/
USE [orders31]
ALTER TABLE [dbo].[Orders_picked] ADD ID INT IDENTITY;
/*down 09032017 */
USE [orders31]
ALTER TABLE [dbo].[Orders_picked] DROP COLUMN ID;

/*up 23032017*/
USE [orders31]
ALTER TABLE [dbo].[Orders_picked] ADD UserID INT;
/*down 23032017 */
USE [orders31]
ALTER TABLE [dbo].[Orders_picked] DROP COLUMN UserID;

/*up 05062017*/
USE [orders31]
ALTER TABLE [dbo].[Orders_shipped] ADD UserID INT;
ALTER TABLE [dbo].[Orders_shipped] ADD ID INT IDENTITY;
ALTER TABLE [dbo].[Orders_printed] ADD UserID INT;
ALTER TABLE [dbo].[Orders_printed] ADD ID INT IDENTITY;
/* down */
ALTER TABLE [dbo].[Orders_shipped] DROP COLUMN UserID;
ALTER TABLE [dbo].[Orders_printed] DROP COLUMN UserID;
ALTER TABLE [dbo].[Orders_shipped] DROP COLUMN ID;
ALTER TABLE [dbo].[Orders_printed] DROP COLUMN ID;

/*07062017*/
USE [orders31]
update [dbo].[user_permission_modules] set module_name='scanning' where module_name='printing'
--sorting
ALTER TABLE [dbo].[user_permission_modules] ADD [sort] smallint default 100 NOT NULL
--dump data
update [dbo].[user_permission_modules] set sort =10 where module_name = 'scanning'
update [dbo].[user_permission_modules] set sort =20 where module_name = 'picking'
update [dbo].[user_permission_modules] set sort =30 where module_name = 'packing'
update [dbo].[user_permission_modules] set sort =40 where module_name = 'loading'
update [dbo].[user_permission_modules] set sort =41 where module_name = 'shipping'
/*down*/
ALTER TABLE [dbo].[user_permission_modules] DROP [sort]