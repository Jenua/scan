<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Manual</title>
</head>
<body>
	<h1>Manuals</h1>
	<p>Updated on 06.19.2017</p>
	<ul>
	  <li>
		Logistics Hub
		<ul>
			<li><a href="/help/main_grid_manual.html">User Interface</a></li>
			<li>
				Features
				<ul>
					<li><a href="/help/QSQ_manual.html">Quick Shipping Quote</a></li>
					<li><a href="/help/OSH_manual.html">Order Status History</a></li>
					<li><a style="color: silver; cursor: default;" href="#">Advanced Search</a></li>
					<li><a style="color: silver; cursor: default;" href="#">Reprint</a></li>
					<li><a href="/help/COMBINE_ORDERS_manual.html">Combine Orders</a></li>
					<li><a style="color: silver; cursor: default;" href="#">Reports</a></li>
					<li><a style="color: silver; cursor: default;" href="#">New Order</a></li>
					<li><a href="/help/QZ_Tray_manual.html">Printing Label and Bol on Specific Printers</a></li>
					<li><a style="color: silver; cursor: default;" href="#">DC Orders Processing</a></li>
					<li><a style="color: silver; cursor: default;" href="#">Shipsingle Monitor</a></li>
					<li><a style="color: silver; cursor: default;" href="#">FPT</a></li>
					<li><a style="color: silver; cursor: default;" href="#">Split Order</a></li>
				</ul>
			</li>
			<li><a style="color: silver; cursor: default;" href="#">Background Logic</a></li>
			<li><a style="color: silver; cursor: default;" href="#">Label Generators</a></li>
			<li><a style="color: silver; cursor: default;" href="#">Bill of Lading Generators</a></li>
			<li><a style="color: silver; cursor: default;" href="#">Flyer Generators</a></li>
		</ul>
	  </li>
	  <li>
		<span  style="color: silver; cursor: default;">Rating Script</span>
		<ul>
			<li><a style="color: silver; cursor: default;" href="#">Rules</a></li>
			<li><a style="color: silver; cursor: default;" href="#">Background logic</a></li>
		</ul>
	  </li>
	  <li>
		<span  style="color: silver; cursor: default;">Database</span>
		<ul>
			<li><a style="color: silver; cursor: default;" href="#">Sync logic</a></li>
			<li><a style="color: silver; cursor: default;" href="#">Stored procedures</a></li>
		</ul>
	  </li>
	  <li>
		<span  style="color: silver; cursor: default;">Duplicating Orders Report</span>
		<ul>
			<li><a style="color: silver; cursor: default;" href="#">Background logic</a></li>
			<li><a style="color: silver; cursor: default;" href="#">User Interface</a></li>
		</ul>
	  </li>
	</ul>
</body>
</html>