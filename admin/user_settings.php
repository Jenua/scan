<?php
/**
 * Created by PhpStorm.
 * User: Yeuvheniia Melnykova
 * Date: 22.05.2017
 * Time: 13:19
 */
?>
<?php require_once '../Include/init.php'; ?>
<?php require_once '../Include/header.php'; ?>
<?php require_once '../Include/navigation.php'; ?>
<?php
global $user;
if( (!$user->isLoggedIn()) ){
    Redirect::to('../Include/login.php');
} else {
//PHP Goes Here!
    $errors = [];
    $successes = [];

    $validation = new Validate();

//Temporary Success Message
    $holdover = Input::get('success');
    if ($holdover == 'true') {
        bold("Account Updated");
    }

//Forms posted
    if (!empty($_POST)) {
        $token = Input::get('csrf');
        if (!Token::check($token)) {
            //die('Token doesn\'t match!');
            $_POST = [];
        }

        //Update settings values
        if (Input::get('SettingsSubmit')) {
            $db = Database::getInstance();

            $fields = Input::get('settings');

            $validation->check($fields, array(
                0 => array(
                    'display' => 'US_LOGOFF_TIME_1',
                    'required' => true,
                    'min' => 5,
                    'valid_time' => true
                ),
                1 => array(
                    'display' => 'US_LOGOFF_TIME_2',
                    'required' => true,
                    'min' => 5,
                    'valid_time' => true
                )
            ));
            if ($validation->passed()) {
                $key =0;
                while ($key < 7) {
                    if ($key < 3)
                        $result = $db->update('user_configuration', $key, array('value' => $_POST['settings'][$key]));
                    else
                        $result = $db->update('user_configuration', $key, array('value' => isset($_POST['settings'][$key])));

                    if (!$result)
                        $errors[] = lang("SQL_ERROR");

                    $key++;
                }

                $successes[] = lang("CONFIG_UPDATE_SUCCESSFUL");

            } else {
                //var_dump($validation->errors());
                //validation did not pass
                foreach ($validation->errors() as $error)
                    $errors = $error;
            }
        }
    }
    ?>
    <!-- Page Heading -->
    <div class="row" id="user_settings">

            <div class="col-lg-8 col-lg-offset-2 col-md-9">
                <?= resultBlock($errors, $successes); ?>
                <!-- Content Goes Here. Class width can be adjusted -->
                <form name='adminSettings' action='<?= $_SERVER['PHP_SELF'] ?>' method='post'>
                    <h2>Update the configuration value</h2>

                    <br>
                    <table class='table table-hover table-list-search'>
                        <tr>
                            <th>Setting Name</th>
                            <th>Value</th>
                        </tr>

                        <?php
                        $settingsR = getUserSettings();
                        foreach ($settingsR as $key => $value) { ?>
                            <tr>
                                <td><label class="form-label"><i><?= $value->name ?></i></label></td>
                                <td>
                                <?php
                                if (strstr($value->name,"ON_") ):
                                    $checked = ($value->value)?"checked":""; ?>
                                    <input class="checkbox" type='checkbox'
                                           name='settings[<?= $value->id ?>]'
                                           <?=$checked?>
                                           value="<?= $value->value ?>" />
                                <?php elseif( strpos($value->name,"TIME_" ) ): ?>
                                    <input class="form-control timepicker required" type='text'
                                           name='settings[<?= $value->id ?>]'
                                           value="<?= $value->value ?>" required/>
                                <?php else: ?>
                                    <input class="form-control required" type='text'
                                           name='settings[<?= $value->id ?>]'
                                           value="<?= $value->value ?>" required/>
                                <?php endif; ?>
                                </td>
                            </tr>
                            <?php
                        } ?>
                        <tr>
                            <td colspan="2" align="left">
                                <input type="hidden" name="csrf" value="<?php echo Token::generate(); ?>">
                                <input class='btn btn-primary' type='submit' name='SettingsSubmit'
                                       value='Save/Apply'/><br><br>
                            </td>
                        </tr>
                    </table>

                </form>
            </div>
            <div class="col-sm-2 col-md-3 col-lg-2"></div>

    </div> <!-- /row -->

    <?php //is logged
}
    ?>
<!-- footers -->
    <!-- Place any per-page javascript here -->
    <script>
        $(function () {
            $('#time2off').html('<?php echo "Next system restart is:".date('y-m-d H:i',$user->timeToOff()+time()); ?>');
        })
    </script>
    <!-- footers -->
<?php require_once $abs_us_root . $us_url_root . 'Include/footer.php'; // the final html footer copyright row + the external js calls ?>