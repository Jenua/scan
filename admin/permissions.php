<?php
/**
 * Created by PhpStorm.
 * User: Yeuvheniia Melnykova
 * Date: 4/5/2017
 * Time: 3:11 PM
 */
?>
<?php
require_once '../Include/init.php';
require_once $abs_us_root . $us_url_root . 'Include/header.php';
require_once $abs_us_root . $us_url_root . 'Include/navigation.php';
?>

<?php
if (!($user->isLoggedIn() && $user->isAdmin())) {
    Redirect::to('../Include/login.php');
} else { ?>

<?php

$validation = new Validate();
$errors = [];
$successes = [];
$validationErrors = '';
//Forms posted
if (!empty($_POST)) {
//    $token = Input::get('csrf');
//    if (!Token::check($token)) {
//        die('Token doesn\'t match!');
//    }

    //Delete permission levels
    if(!empty($_POST['delete'])){
        $deletions = $_POST['delete'];
        if ($deletion_count = deletePermission($deletions)){
            $successes[] = lang("PERMISSION_DELETIONS_SUCCESSFUL", array($deletion_count));
        }
    }

    //Create new permission level
    if (!empty($_POST['name'])) {
        $permission = Input::get('name');
        $fields = array('name' => $permission);
        //NEW Validations
        $validation->check($_POST, array(
            'name' => array(
                'display' => 'Permission Name',
                'required' => true,
                'unique' => 'permissions',
                'min' => 1,
                'max' => 25
            )
        ));
        if ($validation->passed()) {
            $db = Database::getInstance();
            $db->insert('permissions', $fields);
            //add new permission to Admin
            $id = $db->lastId();
            $res = addPermission($id, array(2));

            $successes[] = "Permission Created";

        } else {
            $errors[] = lang("SQL_ERROR");
        }
    }

}
?>
    <!-- Page Heading -->
    <div class="row" id="permissions">
        <div class="col-xs-12 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2">
            <!-- Left Column -->

            <div id="form-errors">
                <?php
                echo resultBlock($errors, $successes);
                echo $validationErrors;
                ?>
            </div>
            <div>
                <form name='adminPermissions' action='<?= $_SERVER['PHP_SELF'] ?>' method='post'>
                    <h2>Create a new permission group</h2>
                    <p class="form-group">
                        <label class="form-label">Permission Name:</label>
                        <input class="form-control" type='text' name='name'/>
                    </p>

                    <br>
                    <table class='table table-hover table-list-search'>
                        <tr>
                            <th>Delete</th>
                            <th>Permission Name</th>
                            <th>Modules</th>
<!--                            <th>Settings</th>-->
                        </tr>

                        <?php
                        $permissionData = fetchAllPermissions();
                        $allModulePermissions = getPermissionModules();
                        //List each permission level
                        foreach ($permissionData as $v1) { ?>
                            <tr>
                                <td><input type='checkbox' name='delete[<?php echo $v1->id; ?>]'
                                           id='delete[<?php echo $v1->id; ?>]'
                                           value='<?php echo $v1->id; ?>'></td>
                                <td>
                                    <a href='admin_permission.php?id=<?php echo $v1->id; ?>'><?php echo $v1->name; ?></a>
                                </td>
                                <td>
                                    <?php
                                    if (key_exists($v1->id, $allModulePermissions))
                                        foreach ($allModulePermissions[$v1->id] as $module_name)
                                            echo '<input type="checkbox" checked="checked" name="addmodule[' . $module_name . ']" disabled>' . $module_name . ' ';
                                    ?>
                                </td>
<!--                                <td>-->
                                    <?php
                                    //configuration values
                                    //echo $settingsList;

                                    ?>
<!--                                </td>-->
                            </tr>
                            <?php
                        } ?>
                    </table>
<!--                    <input type="hidden" name="csrf" value="--><?php //echo Token::generate(); ?><!--">-->
                    <input class='btn btn-primary' type='submit' name='Submit'
                           value='Add/Delete'/><br><br>
                </form>
                <!-- End of main content section -->
            </div>

        </div>
        <div class="col-lg-2 col-md-1"></div>
    </div><!-- /.row -->
<?php } ?>

<!-- Place any per-page javascript here -->
<!-- footers -->
<?php require_once $abs_us_root . $us_url_root . 'Include/footer.php'; // the final html footer copyright row + the external js calls ?>

<!-- Place any per-page javascript here -->