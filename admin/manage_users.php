<?php
require_once "../Include/init.php";
require_once $abs_us_root.$us_url_root.'Include/header.php';
require_once $abs_us_root.$us_url_root.'Include/navigation.php';
?>

<?php  if(!($user->isLoggedIn() && $user->isAdmin() )) {
    Redirect::to('../Include/login.php');
    } else { ?>

<?php
$errors = $successes = [];
$form_valid = true;
$validationErrors = '';
if (Input::exists() && isset($_POST['userCreate'])) {//Input::get('csrf')

    $permissions = Input::get('permissions');
    $name = Input::get('name');
    $login = Input::get('login');
    $password = Input::get('userPassword');
    $token = Input::get('csrf');

    if (!Token::check($token)) {
        die('Token doesn\'t match!');
    }

    $form_valid = false; // assume the worst
    $validation = new Validate();
    $validation->check($_POST, array(
        'name' => array(
            'display' => 'Username',
            'required' => true,
            'min' => 4,
            'max' => 35,
            'unique' => 'user',
        ),
        'login' => array(
            'display' => 'Login',
            'required' => true,
            'min' => 2,
            'max' => 35,
            'unique' => 'user'
        ),
        'userPassword' => array(
            'display' => 'Password',
            'required' => true,
            'min' => 4,
            'max' => 20,
        )

    ));

    if ($validation->passed()) {
        //add user to the database
        $form_valid = true;
        $user = new User();
        $join_date = date("Y-m-d H:i:s");

        try {
            // echo "Trying to create user";
            $user->create(array(

                'name' => $name,
                'login' => $login,
                'password' => $password,
                //password_hash(Input::get('password'), PASSWORD_BCRYPT, array('cost' => 12)),
                'permissions' => $permissions,
                'join_date' => $join_date,
                'is_active' => 1
            ));

        } catch (Exception $e) {
            die($e->getMessage());
        }
        //Redirect::to($us_url_root.'users/joinThankYou.php');
        $successes[] = lang("ACCOUNT_USER_ADDED");
    } else {
        $validationErrors = $validation->display_errors();
    }//Validation and agreement checbox
    ?>
        <?php
    }//Input exists
    ?>

    <div class="row">
        <div class="col-lg-8 col-lg-offset-2">

                <div id="form-errors">
                    <?php echo resultBlock($errors,$successes); ?>
                    <?php echo $validationErrors; ?>
                </div>
                <h3>Add new user</h3>
            <hr>
                <!-- User Add Form-->
            <div class="col-lg-12">
                <form id="saveUser" class="form-vertical" action="manage_users.php" method="post">
                    <div>
                    <fieldset>
                        <!--<legend>Add user</legend>-->
                        <div class="row form-group">
                        <label class="col-lg-2 label-control" for="userName">Name</label>
                        <div class="col-lg-4"> <input type="text" name="name" id="userName"
                                                      value="<?php if (!$form_valid && !empty($_POST)){ echo $name;} ?>" required autofocus
                                                      class="text ui-widget-content ui-corner-all form-control"></div>

                        <label class="col-lg-2 label-control" for="userLogin">Login</label>
                        <div class="col-lg-4"> <input type="text" name="login" id="userLogin"
                               value="<?php if (!$form_valid && !empty($_POST)){ echo $login;} ?>" required
                                                      class="text ui-widget-content ui-corner-all form-control"></div>
                        </div>
                        <div class="row form-group">
                        <label class="col-lg-2 label-control" for="userPassword">Password</label>
                        <div class="col-lg-4"><input type="text" name="userPassword" id="userPassword"
                               value="<?php if (!$form_valid && !empty($_POST)){ echo $password;} ?>" required
                               class="text ui-widget-content ui-corner-all form-control">
                        </div>
                        <label class="col-lg-2 label-control" for="permissions">Groups(new Permissions)</label>
                        <div class="col-lg-4"><select class="form-control" name="permissions">
                            <?php
                            $db = Database::getInstance();

                            $permOpsQ = $db->query("SELECT * FROM permissions");
                            $permOps = $permOpsQ->results();

                            foreach ($permOps as $permOp){
                                echo "<option value='$permOp->id'>$permOp->name</option>";
                            }
                            ?></select>
                        </div>

                        <input type="hidden" name="csrf" value="<?=Token::generate();?>" />
                        </div>
                        <div class="col-lg-12 text-center">
                            <button type="submit" class="btn btn-lg btn-primary" id="addUser" name="userCreate"> Add user </button>
                        </div>

                    </fieldset>
                    </div>
                </form>
<br>
            </div>
            <!-- End of the user settings dialog -->
            <h3>All Users</h3>
            <hr>
<!--jqGrid -->
            <div class="text-center">
                <button class="btn btn-lg btn-info" id="toggleUserActive" name="userStatus"> Activate/Deactivate the selected users </button>
                <table class="jqGridTable" id="jqGridUsers"></table>
                <div id="pagernav"></div>

            </div>
            </div>

        <div class="col-lg-2 col-md-1"></div>
    </div>

    <?php
    }
    ?>
<?php include '../Include/footer.php';
