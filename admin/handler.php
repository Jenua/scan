<?php require_once ('../Include/init.php');?>
<?php
global $user;
if(!($user->isLoggedIn() && $user->isAdmin() )) {
   print_r(json_encode("You don't have permissions."));
} else {
    $query = Input::get('action'); //query type
    if (!$query) $query = Input::get('oper');
    $search = Input::get('_search');
    $page = (isset($_REQUEST['page'])) ? $_REQUEST['page'] : "1"; // get the requested page
//$limit = $_REQUEST['rows'];  get how many rows we want to have into the grid
    $limit = (isset($_REQUEST['rows'])) ? $_REQUEST['rows'] : 1000;
    $sidx = (isset($_REQUEST['sidx'])) ? $_REQUEST['sidx'] : "id"; // get index row - i.e. user click to sort
    $sord = (isset($_REQUEST['sord'])) ? $_REQUEST['sord'] : "DESC"; // get the direction
    if (!$sidx) $sidx = 1;

    $totalrows = Input::get('totalrows');
    if ($totalrows) {
        $limit = $totalrows;
    }

// search options
    $wh = "";
    $searchOn = Strip($search);
    if ($searchOn == 'true') {
        $fld = Strip($_REQUEST['searchField']);
        if ($fld == 'id' || $fld == 'login' || $fld == 'name' || $fld == 'permission') {
            //if( $fld=='id' || $fld =='invdate' || $fld=='name' || $fld=='amount' || $fld=='tax' || $fld=='total' || $fld=='note' ) {
            $fldata = Strip($_REQUEST['searchString']);
            $foper = Strip($_REQUEST['searchOper']);
            // costruct where
            $wh .= " AND " . $fld;
            switch ($foper) {
                case "eq":
                    if (is_numeric($fldata)) {
                        $wh .= " = " . $fldata;
                    } else {
                        $wh .= " = '" . $fldata . "'";
                    }
                    break;
                case "ne":
                    if (is_numeric($fldata)) {
                        $wh .= " <> " . $fldata;
                    } else {
                        $wh .= " <> '" . $fldata . "'";
                    }
                    break;
                case "lt":
                    if (is_numeric($fldata)) {
                        $wh .= " < " . $fldata;
                    } else {
                        $wh .= " < '" . $fldata . "'";
                    }
                    break;
                case "le":
                    if (is_numeric($fldata)) {
                        $wh .= " <= " . $fldata;
                    } else {
                        $wh .= " <= '" . $fldata . "'";
                    }
                    break;
                case "gt":
                    if (is_numeric($fldata)) {
                        $wh .= " > " . $fldata;
                    } else {
                        $wh .= " > '" . $fldata . "'";
                    }
                    break;
                case "ge":
                    if (is_numeric($fldata)) {
                        $wh .= " >= " . $fldata;
                    } else {
                        $wh .= " >= '" . $fldata . "'";
                    }
                    break;
                case "bw":
                    $wh .= " LIKE '" . $fldata . "%'";
                    break;
                case "ew":
                    $wh .= " LIKE '%" . $fldata . "'";
                    break;
                case "cn":
                    $wh .= " LIKE '%" . $fldata . "%'";
                    break;
                case "nc":
                    $wh .= " NOT LIKE '%" . $fldata . "%'";
                    break;
                //begin not
                case "bn":
                    $wh .= " NOT LIKE '" . $fldata . "%'";
                    break;
                //end not
                case "en":
                    $wh .= " NOT LIKE '%" . $fldata . "'";
                    break;
                //nulls
                case "nn":
                    $wh .= " IS NOT NULL ";
                    break;
                case "nu":
                    $wh .= " IS NULL ";
                    break;
                //case "in"
                default :
                    $wh = "";
            }
        }
    }
//echo $fld." : ".$wh;
//if ($auth->isAuth() && $auth->getPermission()=='superuser') {
    switch ($query) {
        case 'del':

            $db = Database::getInstance();
            $userId[0] = Input::get('id');
            $res = deleteUsers($userId);
            print_r(json_encode(($res == 1) ? "User with ID=" . Input::get('id') . " is deleted." : "error"));

            break;
        case 'multiActivator':
        case 'data_window':
            $response = new stdClass();
            //multiedit: 'multiActivator'
            if (Input::get('action') === 'multiActivator'){
                $res = 0;
                $userIds = Input::get('selectedUsers');
                if(sizeof($userIds)>0)
                    $res = updateUsersActivity($userIds);
                if($res > 0)
                    $response->upd = "The status of Users( IDs = {" . implode(",",$userIds) . "}) are updated.";
            }
            //show data in the grid: 'data_window'
            // connect to the database
            $conn = Database::getInstance()->dbc;
            $countQ = /** @lang MS SQL */
                "SELECT COUNT(*) AS count
                    FROM [" . DATABASE . "].[dbo].[user] u WHERE 1=1" . $wh . " ";
            if ($res = exec_query($conn, $countQ)) {
                //  pager settings
                $count = $res->fetchColumn();
                if ($count > 0) {
                    $total_pages = ceil($count / $limit);
                } else {
                    $total_pages = 0;
                }
                if ($page > $total_pages) $page = $total_pages;
                if ($limit < 0) $limit = 0;
                $start = $limit * $page - $limit; // do not put $limit*($page - 1)
                if ($start < 0) $start = 0;
                //main query without pagination
                $SQL = "SELECT  mQuery.[id], mQuery.[login], mQuery.[name], mQuery.[password], mQuery.[permission] as [permission],
                [is_active]
                    FROM(
                     SELECT ROW_NUMBER() OVER ( ORDER BY ". $sidx ." " . $sord . " ) AS RowNum,
                      * FROM [" . DATABASE . "].[dbo].[" . USER_TABLE . "]
                        WHERE 1=1 " . $wh . ") as mQuery
                   
                    WHERE RowNum > " . $start . " AND RowNum <= " . $limit * $page . "
                    ORDER BY " . $sidx . " " . $sord . " ";

                $result = exec_query($conn, $SQL);

                $response->page = $page;
                $response->total = $total_pages;
                $response->records = $count;
                $i = 0;
                $result = $result->fetchAll(PDO::FETCH_ASSOC);
                $usersPermissions = usersPermissions();
                foreach ($result as $row) {
                    $response->rows[$i] = $row;
                    $response->rows[$i]['groups'] = (!empty($usersPermissions[$row["id"]]))?
                        implode(",",$usersPermissions[$row["id"]]): '';
                    $i++;
                }
                print_r(json_encode($response));
            }

            break;
        case 'edit':
            //old permission update case action is the edition of the column in the jqGrid table
            $userId = Input::get('id');
            $old_permission = Input::get('permission');
            $user->update(array('permission'=>$old_permission),$userId);
            print_r(json_encode("User permission now is: ".$old_permission));

            break;
        default:
            if (Input::exists('get')) {

                $user->create(array(
                    'name' => Input::get('userName'),
                    'login' => Input::get('userLogin'),
                    'password' => Input::get('userPassword'),
                    'permissions' => Input::get('permissions')

                ));
            }
    }
}
//}//auth
function Strip($value)
{
    $tmp_val = [];
    if(get_magic_quotes_gpc() != 0)
    {
        if(is_array($value))
            if ( array_is_associative($value) )
            {
                foreach( $value as $k=>$v)
                    $tmp_val[$k] = stripslashes($v);
                $value = $tmp_val;
            }
            else
                for($j = 0; $j < sizeof($value); $j++)
                    $value[$j] = stripslashes($value[$j]);
        else
            $value = stripslashes($value);
    }
    return $value;
}
function array_is_associative ($array)
{
    if ( is_array($array) && ! empty($array) )
    {
        for ( $iterator = count($array) - 1; $iterator; $iterator-- )
        {
            if ( ! array_key_exists($iterator, $array) ) { return true; }
        }
        return ! array_key_exists(0, $array);
    }
    return false;
}
function exec_query($conn, $query)
{
    try {
        $result = $conn->prepare($query);
        $result->execute();
        return $result;
    } catch (PDOException $e) {
        print_r($e->getMessage());
    }
}
