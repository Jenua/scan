<?php
/**
 * Created by PhpStorm.
 * User: Yeuvheniia Melnykova
 * Date: 4/5/2017
 * Time: 5:23 PM
 */
?>
<?php require_once '../Include/init.php';
global $abs_us_root,
       $us_url_root,
       $user;
?>
<?php require_once $abs_us_root.$us_url_root.'Include/header.php'.''; ?>
<?php require_once $abs_us_root.$us_url_root.'Include/navigation.php'.''; ?>

<?php if (!($user->isLoggedIn() && $user->isAdmin())){
    Redirect::to('../Include/login.php');
}else{
    ?>
<?php
$validation = new Validate();
//PHP Goes Here!
$errors = [];
$successes = [];
$userId = Input::get('id');

//Check if selected user exists
if(!userIdExists($userId)){
//    Redirect::to("admin_users.php","?id=".$userId);
    die("There are no user with this ID: ". $userId);
}

$userdetails = fetchUserDetails(NULL, NULL, $userId); //Fetch user details

//Forms posted
if(!empty($_POST)) {
    $token = $_POST['csrf'];
    if(!Token::check($token)){
        die('Token doesn\'t match!');
    }else {
        $db = Database::getInstance();

        //Update activity
        if($userdetails->is_active != $_POST['is_active']){
            $displayActivity = Input::get("is_active");
            $fields=array('is_active'=>$displayActivity);
            $validation->check($_POST,array(
                'is_active' => array(
                    'display' => 'Activity',
                    'required' => true,
                    'min' => 0,
                    'max' => 1
                )
            ));
            if($validation->passed()){
                $db->update('user',$userId,$fields);
                $successes[] = "Activity Updated";
            }
            else {
                $errors[] = "Activity update _ERROR";
            }
        }
        //Update display name

        if ($userdetails->name != $_POST['name']){
            $displayname = Input::get("name");

            $fields=array('name'=>$displayname);
            $validation->check($_POST,array(
                'name' => array(
                    'display' => 'Name',
                    'required' => true,
                    'unique_update' => 'user,'.$userId,
                    'min' => 1,
                    'max' => 25
                )
            ));
            if($validation->passed()){
                $db->update('user',$userId,$fields);
                $successes[] = "Name Updated";
            }
            else {
                $errors[] = "Name update _ERROR";
            }
        }


        //Update login

        if ($userdetails->login != $_POST['login']){
            $login = Input::get("login");

            $fields=array('login'=>$login);
            $validation->check($_POST,array(
                'login' => array(
                    'display' => 'Login',
                    'required' => true,
                    'min' => 1,
                    'max' => 25
                )
            ));
            if($validation->passed()){
                $db->update('user',$userId,$fields);
                $successes[] = "Login Updated";
            }else {
                $errors[] = "Login update _ERROR";
            }
        }

        //Update password
        if ($userdetails->password != $_POST['userPassword']){
            $password = Input::get("userPassword");

            $fields=array('password'=>$password);
            $validation->check($_POST,array(
                'userPassword' => array(
                    'display' => 'Password',
                    'required' => true,
                    'min' => 1,
                    'max' => 25
                )
            ));

            if($validation->passed()){
                $db->update('user',$userId,$fields);
                $successes[] = "Password Updated";
            }else {
                $errors[] = "Password update _ERROR";
            }
        }


        //Remove permission level
        if(!empty($_POST['removePermission'])){
            $remove = $_POST['removePermission'];
            if ($deletion_count = removePermission($remove, $userId)){
                $successes[] = lang("ACCOUNT_PERMISSION_REMOVED", array ($deletion_count));
            }
            else {
                $errors[] = lang("SQL_ERROR");
            }
        }

        if(!empty($_POST['addPermission'])){
            $add = $_POST['addPermission'];
            if ($addition_count = addPermission($add, $userId,'user')){
                $successes[] = lang("ACCOUNT_PERMISSION_ADDED", array ($addition_count));
            }
            else {
                $errors[] = lang("SQL_ERROR");
            }
        }
    }
    $userdetails = fetchUserDetails(NULL, NULL, $userId);
}


$userPermission = fetchUserPermissions($userId);
$permissionData = fetchAllPermissions();

//
?>
<div id="page-wrapper">

    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-offset-2 col-sm-10">
                <?=resultBlock($errors,$successes);?>
                <?=$validation->display_errors();?>
            </div>
        </div>



        <div class="row">
            <div class="col-xs-12 col-sm-2"><!--left col-->

            </div><!--/col-2-->

            <div class="col-xs-12 col-sm-8">
                <form class="form" name='adminUser' action='admin_users.php?id=<?=$userdetails->id?>' method='post'>

                    <h3>User Information</h3>
                    <div class="panel panel-default">
                        <div class="panel-heading">User ID: <?php echo $userdetails->id; ?></div>
                        <div class="panel-body">
                            <label>Joined: </label> <?php echo format_date($userdetails->join_date); ?>
                            <br />

                            <label for="is_active">Activity: </label>
                            <input type="checkbox" id="is_active" name="is_active"
                                   value="<?php echo $userdetails->is_active; ?>"
                                <?php echo ($userdetails->is_active === '1')? 'checked="true"':'';?>>
                            <br/>

                            <label>Last seen: </label> <?php echo format_date($userdetails->last_login); ?><br/>

                            <label>Login:</label>
                            <input class='form-control' type='text' name='login' value='<?=$userdetails->login?>' />

                            <label>Name:</label>
                            <input  class='form-control' type='text' name='name' value='<?=$userdetails->name?>' />

                            <label>Password:</label>
                            <input  class='form-control' type='text' name='userPassword' value='<?=$userdetails->password?>' />


                        </div>
                    </div>

                    <h3>Permissions</h3>
                    <div class="panel panel-default">
                        <div class="panel-heading">Remove These Permission(s):</div>
                        <div class="panel-body">
                            <?php
                            //NEW List of permission levels user is apart of

                            $perm_ids = [];
                            foreach($userPermission as $perm){
                                $perm_ids[] = $perm->permission_id;
                            }

                            foreach ($permissionData as $v1){
                                if(in_array($v1->id,$perm_ids)){ ?>
                                    <input type='checkbox' name='removePermission[]' id='removePermission[]' value='<?=$v1->id;?>' />
                                    <a href="admin_permission.php?id=<?=$v1->id?>"> <?=$v1->name;?></a>
                                    <?php
                                }
                            }
                            ?>

                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading">Add These Permission(s):</div>
                        <div class="panel-body">
                            <?php
                            foreach ($permissionData as $v1){
                                if(!in_array($v1->id,$perm_ids)){ ?>
                                    <input type='checkbox' name='addPermission[]' id='addPermission[]' value='<?=$v1->id;?>' />
                                    <a href="admin_permission.php?id=<?=$v1->id?>"> <?=$v1->name;?></a>
                                    <?php
                                }
                            }
                            ?>
                        </div>
                    </div>

                    <input type="hidden" name="csrf" value="<?=Token::generate();?>" />
                    <input class='btn btn-primary' type='submit' value='Update' />
                    <a class='btn btn-warning' href="admin_users.php">Cancel</a><br><br>

                </form>

            </div><!--/col-9-->
        </div><!--/row-->

    </div>
</div>
<?php }?>
<?php require_once $abs_us_root.$us_url_root.'Include/footer.php'.''; // currently just the closing /body and /html ?>
