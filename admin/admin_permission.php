<?php
/**
 * Created by PhpStorm.
 * User: Yeuvheniia Melnykova
 * Date: 4/7/2017
 * Time: 3:15 PM
 */
?>
<?php require_once '../Include/init.php';
global $abs_us_root,
    $us_url_root,
    $user;

require_once $abs_us_root . $us_url_root . 'Include/header.php'.'';
require_once $abs_us_root . $us_url_root . 'Include/navigation.php'.'';
?>
<?php
if (!($user->isLoggedIn() && $user->isAdmin())) {
    Redirect::to('../Include/login.php');
} else {
    $validation = new Validate();
    $errors = [];
    $successes = [];

//PHP Goes Here!
    $permissionId = Input::get('id') ? Input::get('id') : 1;

//Check if selected permission level exists
    if (!permissionIdExists($permissionId)) {
        Redirect::to("permissions.php");
    }

//Forms posted
    if (!empty($_POST)) {
        $token = $_POST['csrf'];
        if (!Token::check($token)) {
            die('Token doesn\'t match!');
        }

        //Delete selected permission level
        if (!empty($_POST['delete'])) {
            $deletions = $_POST['delete'];
            if ($deletion_count = deletePermission($deletions)) {
                $successes[] = lang("PERMISSION_DELETIONS_SUCCESSFUL", array($deletion_count));
                Redirect::to('permissions.php');
            } else {
                $errors[] = lang("SQL_ERROR");
            }
        } else {
            //Update permission level name
            $permissionName = getPermissionName($permissionId);
            if ($permissionName != $_POST['name']) {
                $permission = Input::get('name');
                $fields = array('name' => $permission);
                //NEW Validations
                $validation->check($_POST, array(
                    'name' => array(
                        'display' => 'Permission Name',
                        'required' => true,
                        'unique' => 'permissions',
                        'min' => 1,
                        'max' => 25
                    )
                ));
                if ($validation->passed()) {
                    $db = Database::getInstance();
                    $db->update('permissions', $permissionId, $fields);
                    $successes[] = lang("PERMISSION_NAME_UPDATE",array($permission));
                } else {
                    $errors[] = lang("SQL_ERROR");
                }
            }

            //Remove access to pages
            if (!empty($_POST['removePermission'])) {
                $remove = $_POST['removePermission'];
                if ($deletion_count = removePermission($permissionId, $remove)) {
                    $successes[] = lang("PERMISSION_REMOVE_USERS", array($deletion_count));
                } else {
                    $errors[] = lang("SQL_ERROR");
                }
            }

            //Add access to pages
            if (!empty($_POST['addPermission'])) {
                $add = $_POST['addPermission'];
                if ($addition_count = addPermission($permissionId, $add)) {
                    $successes[] = lang("PERMISSION_ADD_USERS", array($addition_count));
                } else {
                    $errors[] = lang("SQL_ERROR");
                }
            }

            //Remove access to pages
            if (!empty($_POST['removeModule'])) {
                $remove = $_POST['removeModule'];
                if ($deletion_count = removeModule($remove, $permissionId)) {
                    $successes[] = lang("PERMISSION_REMOVE_MODULES", array($deletion_count));
                } else {
                    $errors[] = lang("SQL_ERROR");
                }
            }

            //Add access to modules/pages
            if (!empty($_POST['addModule'])) {
                $add = $_POST['addModule'];
                if ($addition_count = addModule($add, $permissionId)) {
                    $successes[] = lang("PERMISSION_ADD_MODULES", array($addition_count));
                } else {
                    $errors[] = lang("SQL_ERROR");
                }
            }

            //Add access to pages
            if (!empty($_POST['newModule'])) {
                $newModule[0] = $_POST['newModule'];
                if (addModule($newModule, $permissionId)) {
                    $successes[] = lang("PERMISSION_ADD_MODULE");
                } else {
                    $errors[] = lang("SQL_ERROR");
                }
            }
        }
    }
//Fetch information specific to permission level
    $permissionDetails = fetchPermissionDetails($permissionId);
//Retrieve list of accessible modules
    $modulePermissions = fetchPermissionModules($permissionId);
    $moduleData = fetchAllModules();

    //Retrieve list of users with membership
    $permissionUsers = fetchPermissionUsers($permissionId);
    //Fetch all users
    $userData = fetchAllUsers();
    ?>
    <div id="page-wrapper">
        <div class="container">
            <!-- Page Heading -->
            <div class="row">
                <div class="col-xs-12">
                    <div id="form-errors">
                        <?= resultBlock($errors, $successes); ?>
                        <?php echo $validation->display_errors(); ?>
                    </div>
                    <!-- Main Center Column -->

                    <!-- Content Goes Here. Class width can be adjusted -->
                    <h1>Configure Details for this Permission Level</h1>
                    <form name='adminPermission'
                          action='<?php echo $_SERVER['PHP_SELF']; ?>?id=<?php echo $permissionId; ?>' method='post'>
                        <table class='table'>
                            <tr>
                                <td>
                                    <h3>Permission Information</h3>
                                    <div id='regbox'>
                                        <p>
                                            <label>ID:</label>
                                            <?php echo $permissionDetails['id']; ?>
                                        </p>
                                        <p>
                                            <label for="permName">Name:</label>
                                            <input type='text' name='name' id="permName"
                                                   value='<?php echo $permissionDetails['name']; ?>'/>
                                        </p>

                                        <h3>Delete this Level?</h3>
                                        <label for="delete[<?php echo $permissionDetails['id']; ?>]">Delete:</label>
                                        <input type='checkbox' name='delete[<?php echo $permissionDetails['id']; ?>]'
                                               id='delete[<?php echo $permissionDetails['id']; ?>]'
                                               value='<?php echo $permissionDetails['id']; ?>' />


                                        <h3>Add new Module:</h3>
                                        <label for="newModule">Module name:</label>
                                        <input type='text' name='newModule' id='newModule' value='' />


                                    </div>
                                </td>
                                <td>
                                    <h3>Permission Membership</h3>
                                    <div id='regbox'>
                                    <div><strong>
                                            <label for="removePermission[]">Remove Members:</label></strong>
                                    <?php
                                    //Display list of permission levels with access
                                    $perm_users = [];
                                    foreach ($permissionUsers as $perm) {
                                        $perm_users[] = $perm->user_id;
                                    }
                                    foreach ($userData as $v1) {
                                        if (in_array($v1->id, $perm_users)) { ?>
                                            <br><input type='checkbox' name='removePermission[]'
                                                       id='removePermission[]'
                                                       value='<?php echo $v1->id; ?>'>
                                            <?php echo '<a href="admin_users.php?id='.$v1->id.'"> '.$v1->name.'</a>'; ?>
                                            <?php
                                        }
                                    }
                                    ?>

                                    <p>
                                    <label for='addPermission[]'><strong>
                                       Add Members:</strong></label>
                                    <?php
                                    //List users without permission level
                                    $perm_losers = [];
                                    foreach ($permissionUsers as $perm) {
                                        $perm_losers[] = $perm->user_id;
                                    }
                                    foreach ($userData as $v1) {
                                        if (!in_array($v1->id, $perm_losers)) { ?>
                                            <br><input type='checkbox' name='addPermission[]' id='addPermission[]'
                                                       value='<?php echo $v1->id; ?>' />
                                            <?php echo '<a href="admin_users.php?id='.$v1->id.'"> '.$v1->name.'</a>'; ?>

                                            <?php
                                        }
                                    }
                                    ?>
                                    </p>

                </div>
                </td>
                <td>
                    <h3>Permission Access</h3>
                    <div id='regbox'>
                        <p><label for='removeModule[]'><strong>Remove Access From This Level: </strong></label>
                            <?php
                            //Display list of modules/pages with this access level
                            $module_names = [];
                            $master_modules = Config::get('master_modules');
                            foreach ($modulePermissions as $mp) {
                                $module_names[] = $mp->module_name;
                            }
                            foreach ($moduleData as $v1) {
                                if (in_array($v1->module_name, $module_names)) { ?>
                                    <br><input type='checkbox' name='removeModule[]' id='removeModule[]'
                                               value='<?php echo $v1->module_name; ?>' <?php echo ($permissionId == 1 && in_array($v1->module_name, $master_modules)) ? "disabled checked" : ""; ?>> <?php echo $v1->module_name; ?>
                                <?php }
                            } ?>
                        </p>

                        <p><br><label for="addModule[]"></label><strong>Add Access To This Level:</strong>
                            <?php
                            //Display list of modules/pages with this access level
                            foreach ($moduleData as $v1) {
                                if (!in_array($v1->module_name, $module_names)) { ?>
                                    <br><input type='checkbox' name='addModule[]' id='addModule[]'
                                               value='<?php echo $v1->module_name; ?>' /> <?php echo $v1->module_name; ?>
                                <?php }
                            } ?>
                        </p>
                    </div>
                </td>
                </tr>
                </table>

                <input type="hidden" name="csrf" value="<?php echo Token::generate(); ?>">

                <p>
                    <label>&nbsp;</label>
                    <input class='btn btn-primary submit' type='submit' value='Update Permission'/>
                </p>
                </form>


                <!-- End of main content section -->
            </div>
        </div><!-- /.row -->
    </div>
    </div>
    <?php
}
?>
    <!-- footers -->

    <!-- Place any per-page javascript here -->

<?php require_once $abs_us_root . $us_url_root . 'Include/footer.php'.''; // currently just the closing /body and /html