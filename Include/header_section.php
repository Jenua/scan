<?php
/**
 * header section
 * place it after <body> if needed admin header on the page
 * uses with header.php
 */
?>
<?php
echo '<div style="font-size: 16px; top: 100px;">Collecting data ...</div>';
ob_flush();
flush();
?>

<div id="wrapper">
        <header id="header" class="header-light">
                <div class="head-holder">
                        <strong class="logo"><a href="#">Dreamline</a> </strong>
                        <ul class="top-nav">
                                <li class="btn-manual"><a href="/help/index.php" title="Help manual">manual</a></li>
                        </ul>
                        <h1 class="main-title"><?php echo $header_name ?></h1>
<div class="user-box">
    <div class="user-info-box">
        <span class="user-text">Hello, <a href="#"><?php echo $auth->getName() ?></a></span>
        <div class="drop user-info">
            <ul class="user-info-list">
                <li><strong>Access:</strong> <?php echo $auth->getPermission() ?></li>
                <li><strong>Name:</strong> <?php echo $auth->getName() ?></li>
                <li><strong>Printer for Label:</strong> <?php echo $auth->getPrinter_label() ?></li>
                <li><strong>Printer for BOL:</strong> <?php echo $auth->getPrinter_bol() ?></li>
                <li><strong>Printer for HDDC Label:</strong> <?php echo $auth->getPrinter_hddc1() ?></li>
                <li><strong>Printer for HDDC Barcode:</strong> <?php echo $auth->getPrinter_hddc2() ?></li>
            </ul>
        </div>
    </div>
    <div class="user-btn-box">
        <!-- <a class="btn-top user-acc" href="#" data-name="user account" title="User account"></a> <!-- -->
        <div class="change-pass-holder">
            <a class="btn-top ch-password opener-change" href="#" data-name="change password" title="Change password"></a>
            <div class="drop change-pass effect-change">
                <form action="phpFunctions/changePasswordHeader.php" role="form" id="changePassForm" data-toggle="validator" method="POST">
                    <div class="change-pass-form">
                        <h3>Change password</h3>
                        <div class="form-group">
                            <label for="pwd">Current password:</label>
                            <input type="password" data-minlength="6" name="pwd" class="form-control text" placeholder="Current password" required>
                            <sup><span class="help-block with-errors">&nbsp;</span></sup>
                        </div>
                        <div class="form-group">
                            <label for="newpwd">New password:</label>
                            <input type="password" data-minlength="6" id="newpwd" name="newpwd" class="form-control text" placeholder="New password" required>
                            <sup><span class="help-block with-errors">&nbsp;</span></sup>
                        </div>
                        <div class="form-group">
                            <label for="confirmpwd">Confirm password:</label>
                            <input type="password" data-match="#newpwd" data-match-error="Passwords dont match" data-minlength="6" id="confirmpwd" name="confirmpwd" class="form-control text" placeholder="Confirm password" required>
                            <sup><span class="help-block with-errors">&nbsp;</span></sup>
                        </div>
                        <div class="form-btn">
                            <input type="submit" name="submitbtn" class="btn disabled" value="Change password">
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <a class="btn-top logout" href="<?php echo $page_url.$page_delimeter."is_exit=1"; ?>" data-name="logout" title="Logout"></a>
    </div>
</div>
</div>
</header>
</div>
<script>
    $('#changePassForm').validator().on('submit', function (e) {
        if (e.isDefaultPrevented()) {
            // Invalid Form
        } else {
            event.preventDefault();
            var form = $('#changePassForm');
            var url = $(form).attr("action");
            var formData = {};
            formData = $(form).serialize();
            $.ajax({
                url: url,
                type: 'POST',
                data: formData,
                dataType: "json",
                beforeSend : function(){ /* IF some POPUP needed */ },
                complete: function(data){
                    alert(data.responseJSON);
                },
                error: function (request, status, error) {
                    console.log("Ajax request error: "+request.responseText);
                }
            });
        }
    });

</script>
