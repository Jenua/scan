<?php
/**
 * Created by PhpStorm.
 * User: 7
 * Date: 12/29/2016
 * Time: 3:06 PM
 * Generation of barcode.png
 */

require_once('../shipping_module/Include/barcode/class/BCGFontFile.php');
require_once('../shipping_module/Include/barcode/class/BCGColor.php');
require_once('../shipping_module/Include/barcode/class/BCGDrawing.php');
require_once('../shipping_module/Include/barcode/class/BCGcode128.barcode.php');
function genBarcode_skulot($sku,$lot){
$font = new BCGFontFile('../shipping_module/Include/barcode/font/Arial.ttf', 14);
$colorFront = new BCGColor(0, 0, 0);
$colorBack = new BCGColor(255, 255, 255);


// Barcode Part
    $code = new BCGcode128();
    $code->setScale(2);
    $code->setThickness(100);
    $code->setForegroundColor($colorFront);
    $code->setBackgroundColor($colorBack);
    $code->setFont($font);
    $code->setStart(NULL);
    $code->setTilde(true);
    $code->parse($sku.'   '.$lot);
    $code->clearLabels();

// Drawing Part
    $drawing = new BCGDrawing('../inventory/tmp/' . $sku.'_'.$lot . '.png', $colorBack);
    $drawing->setBarcode($code);
    $drawing->draw();

//header('Content-Type: image/png');

    $drawing->finish(BCGDrawing::IMG_FORMAT_PNG);
}
