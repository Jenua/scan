<?php
/**
 * Common footer using with header_section.php.
 */
?>
<div id="footer"><p>Copyright &copy; <?php echo date('Y');?> Dreamline</p></div>
</body>
</html>