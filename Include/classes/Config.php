<?php

/**
 * Created by PhpStorm.
 * User: Yeuvheniia Melnykova
 * Date: 3/30/2017
 * Time: 11:46 AM
 */
class Config
{
    public static function get($path = null){
        if($path){

            $config = $GLOBALS['config'];
            $path = explode('/', $path);

            foreach ($path as $bit) {
                if(isset($config[$bit])){
                    $config = $config[$bit];
                } else {
                    return false;
                }
            }

            return $config;
        }

        return false;
    }

}