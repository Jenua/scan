<?php

/**
 * User class
 * Date: 3/30/2017
 * Time: 11:44 AM
 */
class User
{
    private $_db, $_data, $_sessionName, $_isLoggedIn, $_cookieName;
    public $tableName = 'user';

    public function __construct($user = null){
        $this->_db = Database::getInstance();
/* is in Auth class **/
        $this->_sessionName = Config::get('session/session_name');
//        $this->_cookieName = Config::get('remember/cookie_name');

        if (!$user) {
            if (Session::exists($this->_sessionName)) {
                $user = Session::get($this->_sessionName);

                if ($this->find($user)) {
                    $this->_isLoggedIn = true;
                } else {
                    //process Logout
                }
            }
        } else {
            $this->find($user);
        }/**/
    }

    public function create($fields = array()){
        $permissions = $fields['permissions'];

        unset($fields['permissions']);
        if (!$this->_db->insert($this->tableName, $fields)) {
            throw new Exception('There was a problem creating an account.');
        }else
            $user_id = $this->_db->lastId();

        $query = $this->_db->insert("user_permission_matches",['user_id'=>$user_id,'permission_id'=>$permissions]);
        // return $user_id;
        //$query2 = $this->_db->insert("profiles",['user_id'=>$user_id, 'bio'=>'This is your bio']);
        return $user_id;
    }

    public function find($user = null){
        if ($user) {
            if(is_numeric($user)){
                $field = 'id';
//            }elseif(!filter_var($user, FILTER_VALIDATE_EMAIL) === false){
//                $field = 'email';
            }else{
                $field = 'login';
            }

            $data = $this->_db->get($this->tableName, array($field, '=', $user));

            if ($data->count()) {
                $this->_data = $data->first();
//                if($this->data()->account_id == 0 && $this->data()->account_owner == 1){
//                    $this->_data->account_id = $this->_data->id;
//                }
                return true;
            }
        }
        return false;
    }

    public function login($username = null, $password = null, $remember = false){

        if (!$username && !$password && $this->exists()) {
            Session::put($this->_sessionName, $this->data()->id);
        } else {
            $user = $this->find($username) && $this->isActive();
            if ($user) {
                if ($password === $this->data()->password) {
                    Session::put($this->_sessionName, $this->data()->id);
//                    if ($remember) {
//                        $hash = Hash::unique();
//                        $hashCheck = $this->_db->get('users_session' , array('user_id', '=', $this->data()->id));
//
//                        $this->_db->insert('users_session', array(
//                            'user_id' => $this->data()->id,
//                            'hash' => $hash,
//                            'uagent' => Session::uagent_no_version()
//                        ));
//
//                        Cookie::put($this->_cookieName, $hash, Config::get('remember/cookie_expiry'));
//                    }
                    $this->_db->query("UPDATE [user] SET last_login = ? WHERE id = ?",[date("Y-m-d H:i:s"),$this->data()->id]);
					$this->_isLoggedIn = true;
					return true;
                }
            }
        }
        return false;
    }

    public function exists(){
        return (!empty($this->_data)) ? true : false;
    }

    public function data(){
        return $this->_data;
    }

    public function isLoggedIn(){
        return $this->_isLoggedIn;
    }
    public function lastLogged(){
        if($this->_isLoggedIn)
            return $this->data()->last_login;
        else
            return false;
    }

    public function notLoggedInRedirect($location){
        if($this->_isLoggedIn){
            return true;
        }else{
            Redirect::to($location);
        }
    }

    public function logout($params = null){
        //$this->_db->query("DELETE FROM users_session WHERE user_id = ? AND uagent = ?",array($this->data()->id,Session::uagent_no_version()));

        Session::delete($this->_sessionName);
        //Cookie::delete($this->_cookieName);
        session_unset();
        session_destroy();
    }

    public function update($fields = array(), $id=null){

        if (!$id && $this->isLoggedIn()) {
            $id = $this->data()->id;
        }

        if (!$this->_db->update($this->tableName, $id, $fields)) {
            throw new Exception('There was a problem updating.');
        }
    }

    public function isAdmin(){
        if($this->isLoggedIn())
            return ($this->data()->login == 'DreamLineAdmin');
    }

    /**
     * @return boolean
     */
    public function isActive()
    {
        return ($this->data()->is_active === '1');
    }

    /**
     * Define the time to next user logout
     * @return int - seconds
     */
    public function timeToOff()
    {
        $result = 0;
        $userLoggedTime = $this->lastLogged();

        $tomorrow = strtotime('tomorrow');
        $userTime = strtotime($userLoggedTime);
        //exit if logged more than day ago
        $maxExpiry = strtotime('+1 day', $userTime);

        $diff = time() - strtotime($userLoggedTime);

        if (!($diff < 0 or empty($userLoggedTime))) {

            $db = Database::getInstance();

            $query = $db->query("SELECT ((DATEPART(hour, value) * 3600) + (DATEPART(minute, value) * 60)) as [seconds] FROM user_configuration WHERE [name] = 'US_LOGOFF_TIME_1' or [name] = 'US_LOGOFF_TIME_2'");
            $res = $query->results();

            $i=0;
            $count = count($res);
            while(  $i < $count ){
                if($userTime < (strtotime('-1 day', $tomorrow) + $res[$i]->seconds))
                    $resTimes[$i] = strtotime('-1 day', $tomorrow) + $res[$i]->seconds;
                else $resTimes[$i] = $tomorrow + $res[$i]->seconds;
                $i++;
            }
            $resTimes[$count] = $maxExpiry;

            $infimum = infimum($resTimes,$userTime);
            $result = $infimum - time();
        }

        return $result;
    }

}