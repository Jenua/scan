<?php
/**
 * User Specific Functions
 */


function test(){
    echo "<br>";
    echo "User Functions have been properly included";
    echo "<br>";
}


//Check if a permission level ID exists in the DB
function permissionIdExists($id) {
    $db = Database::getInstance();
    $query = $db->query("SELECT TOP 1 id FROM [permissions] WHERE id = ?",array($id));
    $num_returns = $query->count();

    if ($num_returns > 0) {
        return true;
    } else {
        return false;
    }
}

//Check if a user ID exists in the DB
function userIdExists($id) {
    $db = Database::getInstance();
    $query = $db->query("SELECT * FROM [user] WHERE id = ?",array($id));
    $num_returns = $query->count();
    if ($num_returns > 0){
        return true;
    }else{
        return false;
    }
}

//Retrieve information for a single permission level
function fetchPermissionDetails($id) {
    $db = Database::getInstance();
    $query = $db->query("SELECT TOP 1 id, [name] FROM [permissions] WHERE id = ? ",array($id));
    $results = $query->first();
    $row = array('id' => $results->id, 'name' => $results->name);
    return ($row);
}

//Change a permission level's name
function updatePermissionName($id, $name) {
    $db = Database::getInstance();
    $fields=array('name'=>$name);
    $db->update('permissions',$id,$fields);
}

//Checks if a username exists in the DB
function usernameExists($username)   {
    $db = Database::getInstance();
    $query = $db->query("SELECT * FROM [user] WHERE [name] = ?",array($username));
    $results = $query->results();
    return ($results);
}

//Retrieve information for all users
function fetchAllUsers() {
    $db = Database::getInstance();
    $query = $db->query("SELECT * FROM [user]");
    $results = $query->results();
    return ($results);
}

//Retrieve complete user information by username, token or ID
function fetchUserDetails($username=NULL,$token=NULL, $id=NULL){
    if($username!=NULL) {
        $column = "name";
        $data = $username;
    }elseif($id!=NULL) {
        $column = "id";
        $data = $id;
    }
    $db = Database::getInstance();
    $query = $db->query("SELECT TOP 1 * FROM [user] WHERE $column = $data");
    $results = $query->first();
    return ($results);
}

//Retrieve list of permission levels a user has
function fetchUserPermissions($user_id) {
    $db = Database::getInstance();
    $query = $db->query("SELECT * FROM user_permission_matches WHERE user_id = ?",array($user_id));
    $results = $query->results();
    return ($results);
}

function hasModuleAccess($module_name){

    global $user;
    $res = false;
    if($user->isAdmin())
        return true;

    $userModules = getUserModules($user->data()->id);
    if($userModules)
        if(in_array($module_name,$userModules))
            return true;

    return $res;

}
function getUserModules($userId){

    $results = [];
    $db = Database::getInstance();
    $query = $db->query("SELECT module_name, min(sort) as sort FROM user_permission_modules  
      WHERE permission_id in ( select permission_id from user_permission_matches where user_id =  " .$userId .")
      GROUP BY module_name
      ORDER BY sort ASC");
    $userModules = $query->results();
    if($userModules)
        foreach ($userModules as $module)
            $results[] = $module->module_name;

    return $results;
}

//Retrieve list of users who have a permission level
function fetchPermissionUsers($permission_id) {
    $db = Database::getInstance();
    $query = $db->query("SELECT id, user_id FROM user_permission_matches WHERE permission_id = ?",array($permission_id));
    $results = $query->results();
    return ($results);
    $row[$user] = array('id' => $id, 'user_id' => $user);
    if (isset($row)){
        return ($row);
    }
}

//Unmatch permission level(s) from user(s)
function removePermission($permissions, $members) {
    $db = Database::getInstance();
    if(is_array($members)){
        $memberString = '';
        foreach($members as $member){
            $memberString .= $member.',';
        }
        $memberString = rtrim($memberString,',');

        $q = $db->query("DELETE FROM user_permission_matches WHERE permission_id = ? AND user_id IN ({$memberString})",[$permissions]);
    }elseif(is_array($permissions)){
        $permissionString = '';
        foreach($permissions as $permission){
            $permissionString .= $permission.',';
        }
        $permissionString = rtrim($permissionString,',');
        $q = $db->query("DELETE FROM user_permission_matches WHERE user_id = ? AND permission_id IN ({$permissionString})",[$members]);
    }
    return $q->count();
}

//Retrieve a list of all .php files in root files folder
function getPathPhpFiles($absRoot,$urlRoot,$fullPath) {
    $directory = $absRoot.$urlRoot.$fullPath;
    var_dump($directory);
    //bold ($directory);
    $pages = glob($directory . "*.php");

    foreach ($pages as $page){
        $fixed = str_replace($absRoot.$urlRoot,'',$page);
        $row[$fixed] = $fixed;
    }
    return $row;
}


//Retrieve list of permission levels that can access a page
function fetchModulePermissions($module_name) {
    $db = Database::getInstance();
    $query = $db->query("SELECT id, permission_id FROM user_permission_modules WHERE module_name = ? ",array($module_name));
    $results = $query->results();
    return($results);
}

//Retrieve list of modules that a permission level can access
function fetchPermissionModules($permission_id) {
    $db = Database::getInstance();
    ///if ($permission_id != 1)
        $query = $db->query(
            "SELECT  module_name
	  FROM user_permission_modules 	
	  WHERE permission_id = ?", [$permission_id]);
    //else
       // $query = $db->query(
      //      "SELECT DISTINCT module_name
	 // FROM user_permission_modules WHERE module_name !='All'");
    $results = $query->results();
    return ($results);
}

/**
 * @return array of module_names with permission_id as key
 */
function getPermissionModules(){
    $db = Database::getInstance();
    $result =[];
    $query = $db->query("SELECT * FROM user_permission_modules");

    $results = $query->results();
    foreach ($results as $key=>$val)
        $result[$val->permission_id][] = $val->module_name;
    return ($result);
}
/**
 * @return array of settings_names with permission_id as key
 */
function getUserSettings(){
    $db = Database::getInstance();
    $result =[];
    $settingsQ = $db->query("SELECT * FROM user_configuration --WHERE permission_id is NULL");
    $settingsR = $settingsQ->results();

    return $settingsR;
}
function getSettingsByName($name){
    $result =[];
    $settingsR = getUserSettings();
    foreach ($settingsR as $val)
        $result[$val->name] = $val->value;

    if( array_key_exists($name,$result))
        return $result[$name];

}
function getPermissionSettings(){

    $db = Database::getInstance();
    $result = [];
    $settingsQ = $db->query("SELECT * FROM dbo.permissions p 
            LEFT JOIN user_permission_configurations upc ON upc.permission_id = p.id
            LEFT JOIN user_configuration uc on uc.id=upc.configuration_id
			");
    $settingsR = $settingsQ->results();

    foreach ($settingsR as $val)
        $result[$val->name] = $val->value;
    return ($result);

}
//Delete a defined array of users
function deleteUsers($users) {
    $db = Database::getInstance();
    $i = 0;
    foreach($users as $id){
        $query1 = $db->query("DELETE FROM [user] WHERE id = ?",array($id));
        $query2 = $db->query("DELETE FROM user_permission_matches WHERE user_id = ?",array($id));

        $i++;
    }
    return $i;
}
// Activate/Deactivate a defined array of users
function updateUsersActivity($users) {
    $db = Database::getInstance();
    $i = 0;
    foreach($users as $id){
        if($id != 2){
            //block Admin deactivation
            $db->query("UPDATE [user] SET [is_active] = CASE WHEN is_active=0 THEN 1 ELSE 0 END WHERE id = ?",array($id));

            $i++;
        }
    }
    return $i;
}

//
//Check if a user has access to a page
function secureModule($module){
    return true;
}

//Does user have permission
//This is the old school UserSpice Permission System
function checkPermission($permission) {
    $db = Database::getInstance();
    global $user;
    //Grant access if master user
    $access = 0;

    foreach($permission[0] as $perm){
        if ($access == 0){
            $query = $db->query("SELECT id FROM user_permission_matches  WHERE user_id = ? AND permission_id = ?",array($user->data()->id,$perm->permission_id));
            $results = $query->count();
            if ($results > 0){
                $access = 1;
            }
        }
    }
    if ($access == 1){
        return true;
    }
    if ($user->data()->id == 2){
        return true;
    }else{
        return false;
    }
}

function checkMenu($permission, $id) {
    $db = Database::getInstance();
    global $user;
    //Grant access if master user
    $access = 0;

    if ($access == 0){
        $query = $db->query("SELECT id FROM user_permission_matches  WHERE user_id = ? AND permission_id = ?",array($id,$permission));
        $results = $query->count();
        if ($results > 0){
            $access = 1;
        }
    }
    if ($access == 1){
        return true;
    }
    if ($user->data()->id == 1){
        return true;
    }else{
        return false;
    }
}

//Retrieve information for all permission levels
function fetchAllPermissions() {
    $db = Database::getInstance();
    $query = $db->query("SELECT id, [name] FROM [permissions]");
    $results = $query->results();
    return ($results);
}
function usersPermissions() {
    $result = [];
    $db = Database::getInstance();
    $query = $db->query("select user_id,(select [name] from  [permissions] p where p.id = permission_id) as [name] from [user_permission_matches] order by user_id ");
    $results = $query->results();
    foreach ($results as $key => $val)
        $result[$val->user_id][] = $val->name;
    return ($result);
}
//Retrieve information for all modules
function fetchAllModules() {
    $db = Database::getInstance();
    $query = $db->query("SELECT DISTINCT module_name FROM [user_permission_modules]");
    $results = $query->results();
    return ($results);
}

//Displays error and success messages
function resultBlock($errors,$successes){
    //Error block
    if(count($errors) > 0){
        echo "<div class='alert alert-danger alert-dismissible' role='alert'> <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
		<ul>";
        foreach($errors as $error){
            echo "<li>".$error."</li>";
        }
        echo "</ul>";
        echo "</div>";
    }

    //Success block
    if(count($successes) > 0){
        echo "<div class='alert alert-success alert-dismissible' role='alert'> <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
		<ul>";
        foreach($successes as $success){
            echo "<li>".$success."</li>";
        }
        echo "</ul>";
        echo "</div>";
    }
    return '';
}

//Match permission level(s) with user(s)
function addPermission($permission_ids, $members) {
    $db = Database::getInstance();
    $i = 0;
    if(is_array($permission_ids)){
        foreach($permission_ids as $permission_id){
            if($db->query("INSERT INTO user_permission_matches (user_id,permission_id) VALUES (?,?)",[$members,$permission_id])){
                $i++;
            }
        }
    }elseif(is_array($members)){
        foreach($members as $member){
            if($db->query("INSERT INTO user_permission_matches (user_id,permission_id) VALUES (?,?)",[$member,$permission_ids])){
                $i++;
            }
        }
    }
    return $i;
}


//Delete a permission level from the Database
function deletePermission($permission) {
    global $errors;
    $i = 0;
    $db = Database::getInstance();
    foreach($permission as $id){
        if ($id == 1){
            $errors[] = "CANNOT_DELETE_NEWUSERS";
        }
        elseif ($id == 2){
            $errors[] = "CANNOT_DELETE_ADMIN";
        }else{
            $query1 = $db->query("DELETE FROM permissions WHERE id = ?",array($id));
            $query2 = $db->query("DELETE FROM user_permission_matches WHERE permission_id = ?",array($id));
            $query3 = $db->query("DELETE FROM user_permission_modules WHERE permission_id = ?",array($id));
            $i++;
        }
    }
    return $i;

    //Redirect::to('admin_permissions.php');
}

//Checks if an email is valid
function isValidEmail($email){
    if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
        return true;
    }
    else {
        return false;
    }
}

function echoId($id,$table,$column){
    $db = Database::getInstance();
    $query = $db->query("SELECT TOP 1 $column FROM [{$table}] WHERE id = $id ");
    $count=$query->count();

    if ($count > 0) {
        $results=$query->first();
        foreach ($results as $result){
            echo $result;
        }
    } else {
        echo "Not in database";
        Return false;
    }
}

function bin($number){
    if ($number == 0){
        echo "<strong><font color='red'>No</font></strong>";
    }
    if ($number == 1){
        echo "<strong><font color='green'>Yes</font></strong>";
    }
    if ($number != 0 && $number !=1){
        echo "<strong><font color='blue'>Other</font></strong>";
    }
}

function generateForm($table,$id, $skip=[]){
    $db = Database::getInstance();
    $fields = [];
    $q=$db->query("SELECT * FROM [{$table}] WHERE id = ?",array($id));
    $r=$q->first();

    foreach($r as $field => $value) {
        if(!in_array($field, $skip)){
            echo '<div class="form-group">';
            echo '<label for="'.$field.'">'.ucfirst($field).'</label>';
            echo '<input type="text" class="form-control" name="'.$field.'" id="'.$field.'" value="'.$value.'">';
            echo '</div>';
        }
    }
    return true;
}

  function generateAddForm($table, $skip=[]){
      $db = Database::getInstance();
      $fields = [];
      $q=$db->query("SELECT * FROM [{$table}]");
      $r=$q->first();

      foreach($r as $field => $value) {
          if(!in_array($field, $skip)){
              echo '<div class="form-group">';
              echo '<label for="'.$field.'">'.ucfirst($field).'</label>';
              echo '<input type="text" class="form-control" name="'.$field.'" id="'.$field.'" value="">';
              echo '</div>';
          }
      }
      return true;
  }

  function updateFields2($post, $skip=[]){
      $fields = [];
      foreach($post as $field => $value) {
          if(!in_array($field, $skip)){
              $fields[$field] = sanitize($post[$field]);
          }
      }
      return $fields;
  }
    function getUserPermission($userId){
        $db = Database::getInstance();
        $query = $db->query("SELECT * FROM user_permission_matches  WHERE user_id = ?",array($userId));
        $result = $query->first();
        return ($result);
    }
    function getPermissionName($id){
        $db = Database::getInstance();
        $query = $db->query("SELECT [name] FROM [permissions]  WHERE id = ?",array($id));
        $result = $query->first()->name;
        return ($result);
    }

    function setUserPermission($userId,$permissionId){
        $db = Database::getInstance();
        $updArr = array('user_id'=>$userId,'permission_id'=>$permissionId);
        $currentPerm = getUserPermission($userId);


            if (is_null($currentPerm))
                $result = $db->insert("user_permission_matches",$updArr);
            else
                $result = $db->update("user_permission_matches",$currentPerm->id,$updArr);
            return $result;
    }

  function hasPerm($permissions, $id) {
      $master_account = [];
      $db = Database::getInstance();
      global $user;
      //Grant access if master user
      $access = 0;

      foreach($permissions as $permission){

          if ($access == 0){
              $query = $db->query("SELECT id FROM user_permission_matches  WHERE user_id = ? AND permission_id = ?",array($id,$permission));
              $results = $query->count();
              if ($results > 0){
                  $access = 1;
              }
          }
      }
      if ($access == 1){
          return true;
      }
//      if (in_array($user->data()->id, $master_account)){
//          return true;
//      }else{
          return false;
      //}
  }

//Inputs language strings from selected language.
function lang($key,$markers = NULL){
    global $lang;
    if($markers == NULL){
        $str = $lang[$key];
    }else{
        //Replace any dyamic markers
        $str = $lang[$key];
        $iteration = 1;
        foreach($markers as $marker){
            $str = str_replace("%m".$iteration."%",$marker,$str);
            $iteration++;
        }
    }
    //Ensure we have something to return
    if($str == ""){
        return ("No language key found");
    }else{
        return $str;
    }
}

//Match permission level(s) with module(s) permission_module_matches
function addModule($module, $permission) {
    $db = Database::getInstance();
    $i = 0;
    if (is_array($permission)){
        foreach($permission as $id){
            $query ="INSERT INTO user_permission_modules (
			permission_id, module_name ) VALUES ( $id , '".$module."' )";
            $query = $db->query($query);
            $i++;
        }
    } elseif (is_array($module)){
        foreach($module as $name){
            $query = $db->query("INSERT INTO user_permission_modules (
			permission_id, module_name ) VALUES ( $permission , '".$name."' )");
            $i++;
        }
    } else {
        $query = $db->query("INSERT INTO user_permission_modules (
		permission_id, module_name ) VALUES ( $permission , '".$module."' )");
        $i++;
    }
    return $i;
}

/**
 * @param $modules
 * @param $permissions
 * @return mixed
 *  Unmatched permission and module
 *  Prevent deletion of static modules: now when create new module it by default is attached to the $permissions == 1
 *
 */

function removeModule($modules, $permissions) {
    $master_modules = Config::get('master_modules');
    $db = Database::getInstance();
    if(is_array($permissions)){
        $ids = '';
        for($i = 0; $i < count($permissions);$i++){
            $ids .= $permissions[$i].',';
        }
        $ids = rtrim($ids,',');
        if($query = $db->query("DELETE FROM user_permission_modules WHERE permission_id IN ({$ids}) AND module_name = '".$modules."' ")){
            return $query->count();
        }
    }elseif(is_array($modules)){
        $not_master_modules = array_diff($modules,$master_modules);
        $names =
            (!empty($not_master_modules) && $permissions == 1)? implode("','",$not_master_modules)
            : implode("','",$modules);

        $inclause = "IN ('$names')";
        $q = "DELETE FROM user_permission_modules WHERE module_name ".$inclause." AND permission_id = $permissions";
        if($query = $db->query($q)){
            return $query->count();
        }
    }
}

function moduleUserList($module){
    global $user;
    $loggedUser = $user->data()->id;
    $db = Database::getInstance();
    $where = " AND [user].id != 2 AND [user].[is_active] = 1 "; //AND [user].id != ".$loggedUser." ";
    // -- exclude admins from the result upma.permission_id != 1
    $query = $db->query("SELECT DISTINCT [user].name, [user].id FROM user_permission_modules upm                        
                        LEFT JOIN [dbo].[user_permission_matches] upma ON upma.permission_id = upm.permission_id                         
                         AND upma.permission_id != 1
                        INNER JOIN [user] on [user].id = upma.user_id
                        WHERE module_name = '" . $module . "' 
                        " .$where. "
                         ORDER BY [user].name");

    $results = $query->results();

    return ($results);

}