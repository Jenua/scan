<?php
/**
 * helper functions
 */
//echo "helpers included";
require_once 'us_helpers.php';
//inventory_activity helpers
require_once 'inv_helpers.php';
//require_once("users_online.php");
require_once("language.php");

//escapes strings and sets character set
function sanitize($string) {
    return htmlentities($string, ENT_QUOTES, 'UTF-8');
}

function currentPage() {
    $uri = $_SERVER['PHP_SELF'];
    $path = explode('/', $uri);
    $currentPage = end($path);
    return $currentPage;
}

function currentFolder() {
    $uri = $_SERVER['PHP_SELF'];
    $path = explode('/', $uri);
    $currentFolder=$path[count($path)-2];
    return $currentFolder;
}

function format_date($date){
    return (empty($date))? '' : date("m/d/Y ~ h:iA", strtotime($date));
}

function money($ugly){
    return '$'.number_format($ugly,2,'.',',');
}

function display_errors($errors = array()){
    $html = '<ul class="bg-danger">';
    foreach($errors as $error){
        if(is_array($error)){
            $html .= '<li class="text-danger">'.$error[0].'</li>';
            $html .= '<script>jQuery("#'.$error[1].'").parent().closest("div").addClass("has-error");</script>';
        }else{
            $html .= '<li class="text-danger">'.$error.'</li>';
        }
    }
    $html .= '</ul>';
    return $html;
}

function display_successes($successes = array()){
    $html = '<ul>';
    foreach($successes as $success){
        if(is_array($success)){
            $html .= '<li>'.$success[0].'</li>';
            $html .= '<script>jQuery("#'.$success[1].'").parent().closest("div").addClass("has-error");</script>';
        }else{
            $html .= '<li>'.$success.'</li>';
        }
    }
    $html .= '</ul>';
    return $html;
}
/*
function email($to,$subject,$body,$attachment=false){
    $db = Database::getInstance();
    $query = $db->query("SELECT * FROM email");
    $results = $query->first();

    $from = $results->from_email;
    $from_name=$results->from_name;
    $smtp_server=$results->smtp_server;
    $smtp_port=$results->smtp_port;
    $smtp_username=$results->email_login;
    $smtp_password=$results->email_pass;
    $smtp_transport=$results->transport;

    $mail = new PHPMailer;

    // $mail->SMTPDebug = 3;                               // Enable verbose debug output

    // $mail->isSMTP();                                    // Set mailer to use SMTP
    $mail->Host = $smtp_server;  													// Specify main and backup SMTP servers
    $mail->SMTPAuth = true;                               // Enable SMTP authentication
    $mail->Username = $smtp_username;                 // SMTP username
    $mail->Password = $smtp_password;                           // SMTP password
    $mail->SMTPSecure = $smtp_transport;                            // Enable TLS encryption, `ssl` also accepted
    $mail->Port = $smtp_port;                                    // TCP port to connect to

    $mail->setFrom($from, $from_name);

    $mail->addAddress(rawurldecode($to));     // Add a recipient, name is optional

    $mail->isHTML(true);                                  // Set email format to HTML

    $mail->Subject = $subject;
    $mail->Body    = $body;

    $result = $mail->send();

    return $result;
}

function email_body($template,$options = array()){
    $abs_us_root=$_SERVER['DOCUMENT_ROOT'];

    $self_path=explode("/", $_SERVER['PHP_SELF']);
    $self_path_length=count($self_path);
    $file_found=FALSE;

    for($i = 1; $i < $self_path_length; $i++){
        array_splice($self_path, $self_path_length-$i, $i);
        $us_url_root=implode("/",$self_path)."/";

        if (file_exists($abs_us_root.$us_url_root.'z_us_root.php')){
            $file_found=TRUE;
            break;
        }else{
            $file_found=FALSE;
        }
    }
    extract($options);
    ob_start();
    require $abs_us_root.$us_url_root.'users/views/'.$template;
    return ob_get_clean();
}
*/
function inputBlock($type,$label,$id,$divAttr=array(),$inputAttr=array(),$helper=''){
    $divAttrStr = '';
    foreach($divAttr as $k => $v){
        $divAttrStr .= ' '.$k.'="'.$v.'"';
    }
    $inputAttrStr = '';
    foreach($inputAttr as $k => $v){
        $inputAttrStr .= ' '.$k.'="'.$v.'"';
    }
    $html = '<div'.$divAttrStr.'>';
    $html .= '<label for="'.$id.'">'.$label.'</label>';
    if($helper != ''){
        $html .= '<button class="help-trigger"><span class="glyphicon glyphicon-question-sign"></span></button>';
    }
    $html .= '<input type="'.$type.'" id="'.$id.'" name="'.$id.'"'.$inputAttrStr.'>';
    if($helper != ''){
        $html .= '<div class="helper-text">'.$helper.'</div>';
    }
    $html .= '</div>';
    return $html;
}

//preformatted var_dump function
function dump($var){
    echo "<pre>";
    var_dump($var);
    echo "</pre>";
}

//preformatted dump and die function
function dnd($var){
    echo "<pre>";
    var_dump($var);
    echo "</pre>";
    die();
}

function bold($text)
{
    echo "<text style = 'padding: 1em; align-content: center'><h4><span style='background:white'>";
    echo $text;
    echo "</h4></span></text>";
}

function err($text)
{
    echo "<span><text style = 'padding:1em; align-content: center'></span><h4 style='color=red'>";
    echo $text;
    echo "</h4></span></text>";
}

function redirect($location){
    header("Location: {$location}");
}

function output_message($message) {
    return $message;
}

/**
 * @param $array
 * @param $majoranta
 * @return mixed
 */
function infimum($array,$majoranta){

    $result = array_filter($array, function ($var) use ($majoranta) {
        return $var >= $majoranta;
    });

    $infimum = min($result);

    return $infimum;
}

/**
 * @param $module
 * @param null $type - for Loading
 * @return string
 */
function module_title($module, $type = null)
{
    switch ($module) {
        case 'scanning':
            $module_title = "Warehouse\nPick-Up";
            break;
        case  'shipping':
            $module_title = 'End-Of-Day';
            break;
        case  'inventory_activity':
            $module_title = 'Inventory activity';
            break;
        case 'loading':
            $module_title = empty($type) ? 'LTL Loading' : $type . ' Loading';
            break;
        default:
            $module_title = ucfirst($module) . ' system';
    }

    return $module_title;
}

function page_title(){
    global $us_url_root;
    $page = currentPage();
    $system_name =  str_replace('/','',$us_url_root);

    if ($page === 'login.php')
        return 'Log in to continue';

    switch ($system_name){
        case 'scanning':
        case 'inventory':
            $result = 'Order Scanning Systems';
            break;
        case '':
            switch ($page){
                // admin/
                case 'manage_users.php':
                    $result = 'Manage Users';
                    break;
                case 'permissions.php':
                    $result = 'Permissions';
                    break;
                case 'user_settings':
                    $result = 'Configuration';
                    break;
                case 'admin_users.php':
                    $result = 'Edit User';
                    break;
                default:
                    $result = 'Admin management';
                    break;
            }
            break;

        default:
            $result = ucfirst($system_name);
    }

    return $result;
}