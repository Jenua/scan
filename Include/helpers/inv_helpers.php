<?php
/**
 * Created by PhpStorm.
 * User: Yeuvheniia Melnykova
 * Date: 7/13/2017
 * Time: 1:28 PM
 */

//Retrieve information for a single action type
function fetchAllTransTypes() {
    $db = Database::getInstance();
    $query = $db->query("SELECT ID,[name],[title],[description] FROM ".DATABASE31.".[dbo].[Inventory_action]
     ORDER BY sort");
    $results = $query->results();
    $row = [];
    foreach ($results as $type)
        $row[$type->ID] = array(
            'abr' => $type->name,
            'title'=>$type->title,
            'desc' =>$type->description);

    return ($row);

}

/**
 * @param $codes
 * @param $level = get Level from SCAN_TREE table
 * @param $userID
 * @return array = false:[] or true: saved data into currentArray instead of SQL SELECT AFTER successfully INSERT
 *
 */
function addCodeSet($parent,$codes, $level)
{
    $justSaved = [];
    $db = Database::getInstance();

    $parentCode = $parent;//key($codes);
    $children = $codes;//[$parentCode];
    if ($parent != '' and is_array($codes)){//(count($codes) === 1) {
        //insert parent
        $fields = array('ScannedTreeID' => $level - 1, 'Code' => $parentCode, 'ParentID' => 0);
        $db->insert(SCAN_HISTORY, $fields);

        //add new parent
        if ($parentID = $db->lastId()) {
            $justSaved[$parentID] = $fields;
            if (count($children) > 0)
                //add children
                foreach ($children as $child=>$qty) {
                    if (!empty($child)) {
                        //insert children field
                        $fields = array('ScannedTreeID' => $level, 'Code' => $child,'QTY' => $qty, 'ParentID' => $parentID);

                        if ($db->insert(SCAN_HISTORY, $fields)) {
                            $childID = $db->lastId();
                            $justSaved[$childID] = $fields;
                        }
                    }
                }
            else
                // add to the saved parent comment field the no-children information
                $db->update(SCAN_HISTORY, $parentID, array('Comment' => 'Childless parent'));
        }
    }

    return $justSaved;
}

/**
 * @param $level
 * @return array parent->[children]
 */
function getCodeSet($level){

    $db = Database::getInstance();

    $result = $resUPC = [];

    if ($level > 1) {
        $resFTL = $db->get(ORDERS_LOADED_FULL_TABLE, array('PalletUPCID', '>', 0), array('ID','DESC'))->results();

        $res = $db->get(SCAN_HISTORY, array('ScannedTreeID', '>=', $level), array('ID','DESC'));
        $resArr = $res->results();
        foreach ($resArr as $codes)
            $resUPC[$codes->ParentID][$codes->ID] = $codes;

        foreach ($resFTL as $row) {
            $result[$row->PalletUPCID]['parent'] = $row->POnumber . '->' . $row->Truck . '->' . $row->Pallet;
            $result[$row->PalletUPCID]['children'] = $resUPC[$row->PalletUPCID];
        }
    }
    return $result;
}
function scanStatus($module){

    return ($module === 'scanning')? 'Print' : ucfirst(substr($module, 0, -3));

}

/**
 * @param $module
 * First status of PO= [orders31].[dbo].[salesorder]
 * @param $id
 * @param $PO
 * @return int scanHistory.ID
 */
function notFoundPo($module, $id, $PO = null)
{

    $master_modules = Config::get('master_modules');
    $moduleColumns = array_map("scanStatus", $master_modules);
    $moduleOrder = array_search($module, $master_modules);

    $db = Database::getInstance();
    //use test db instead of orders31 for testing
    $scanSystemsDB =  (DATABASE == "Orders_Test")? DATABASE : DATABASE31;

    if (empty($PO)) {//in the loading we scans PRONumbers
		// Orders_loaded table is ONLY in orders31
		// This temporary quick fix for switch Database from orders to orders31 for LTL loading module
		$tmpFixTable = $scanSystemsDB."].[dbo].[".constant('ORDERS_' . strtoupper($moduleColumns[$moduleOrder]) . 'ED_TABLE');
		// Need to refator ASAP !!!
		$find = $db->findById($id, $tmpFixTable)->first();
        if($find)
            $PO = $find->POnumber;
        else
            return false;
    }
    //is in the sales
    $refNumbersToStr = getRefByPO($PO);
    $insColumn =  '[refnumber]';
    $insValue = " '". $refNumbersToStr ."' ";
    $updQ = $insColumn." = ".$insValue;

    $moduleColumns = array_map("scanStatus",$master_modules);

    $where = array('PONumber','=',$PO);
    $scanHistoryRow = $db->get(ORDERS_SCAN_HISTORY, $where, array('ID','DESC') )->first();


    if( empty($scanHistoryRow) ){
       $nullFields2Status = (empty($poValid))? '?Valid':'Valid';
        if($moduleOrder>0){
            $nullFields2Status .= '?';
            $prevSystems = array_slice($moduleColumns,0,$moduleOrder);
            $nullFields2Status .= implode("?",$prevSystems);
        }
        $insVal = $nullFields2Status;

        $insColumn .=  ', PoNumber, TimeCreated, [Status]';
        $insValue .= ", '".$PO."', getDate(), '".$insVal."'";

    } else {
        $scanHistoryID = $scanHistoryRow->ID;
        $oldStatus = $scanHistoryRow->Status;
        $updQ .= ",[Status] = '".$oldStatus."'";
    }

    $i = 0;
    while ( $i <= $moduleOrder ) {
        $currName = $moduleColumns[$i];
        $insCol = "[".$currName."ID]";

        if ($i === $moduleOrder && !empty($id)){
            $insVal = $id;
        }
        else {
            if (!empty($scanHistoryRow) && !empty($scanHistoryRow->{$currName . 'ID'})) {
                $insVal = $scanHistoryRow->{$currName . 'ID'};
            }
            else {
                $currTable = constant('ORDERS_' . strtoupper($moduleColumns[$i]) . 'ED_TABLE');
                $insVal = " ( SELECT CASE WHEN COUNT(1) > 0 THEN max(ID) ELSE NULL END  
                    FROM [" . $scanSystemsDB . "].[dbo].[" . $currTable . "]                         
                    WHERE  PONumber = '" . $PO . "' 
                    GROUP BY [PONumber])";
            }
        }

        $insColumn .= ",".$insCol;
        $updQ .= ",".$insCol." = ".$insVal;
        $insValue .= ",".$insVal;

        $i++;
    }
    //if shipping
    if($moduleOrder === (sizeof($master_modules)-1))
        $updQ .= ",[TimeFinished] = getDate()";

    if(isset($scanHistoryID))
        $query = 'UPDATE '.ORDERS_SCAN_HISTORY.' SET '.$updQ.' WHERE [ID] = ' .$scanHistoryID;
    else
        $query = 'INSERT INTO '.ORDERS_SCAN_HISTORY.' ('.$insColumn.') VAlUES ('.$insValue.')';

    $db->query($query);

    return isset($scanHistoryID) ? $scanHistoryID : $db->lastId();

}

/**
 * Check the orders31.dbo.salesorder on existed refnumbers
 * @param $PONumber
 * @return string RefNumber {null, one value or string of values separated by ','}
 */
function getRefByPO($PONumber){
    $result = '';
    if( empty($PONumber))
        return $result;

    $db = Database::getInstance();

    $table = '['.DATABASE31.'].[dbo].[salesorder]';
    $where = " WHERE PONumber = '".$PONumber."' ";
    $isInSales = $db->query('SELECT * FROM '. $table . $where);
    $count = $isInSales->count();

    if($count>0){
        if($count>1) {
            $duplicates = $isInSales->results(); // 58636684
            for ($i = 0; $i < $count; $i++)
                $result .= $duplicates[$i]->RefNumber . ",";
            $result = substr($result,0,-1);
        }
        else //'58880825'
            $result = $isInSales->first()->RefNumber;
    }

    return $result;

}

/**
 * @param $PRONumber
 * @return array of ponumbers if exists in the SHIPTRACKWITHLABEL,
 * else empty array
 * */
function getPOByPRO($PRONumber){
    $result = [];

    $db = Database::getInstance();
    $table = SHIPTRACKWITHLABEL_TABLE;
    $where = array("PRO","=",$PRONumber);
    if($isInTable = $db->get($table, $where))
        foreach ($isInTable->results() as $res)
          array_push($result, $res->PONUMBER);

    return $result;
}

/**
 * Get last Date of scanning PONumber from all logged table
 * @param $PONumber
 * @return array of results:
 *  object(stdClass)[15]
public 'RefNumber' => string '3424873' (length=7)
public 'PrintDate' => null
public 'PickDate' => string '2017-08-28 10:44:33.130' (length=23)
Pack...Load
public 'ShippDate' => null
 */
function getPOHistory($PONumber,$module){
    $result = [];
    $master_modules = Config::get('master_modules');
    $db = Database::getInstance();
    //use test db instead of orders31 for testing
    $scanSystemsDB =  (DATABASE == "Orders_Test")? DATABASE : DATABASE31;

    $q = 'SELECT RefNumber, [Status]';//If Status = 1, the order is Canceled
    $moduleColumns = array_map("scanStatus", $master_modules);

    $i=0;
    while ( $master_modules[$i] != $module ) {
        $column = $moduleColumns[$i];
        $dateColumn = ($column === 'Shipp')? 'ShipDate' : 'DateCreated';
        $q .=", (SELECT TOP 1 CASE WHEN COUNT(1) > 0 THEN max([".$dateColumn."]) ELSE NULL END  
                FROM ".$scanSystemsDB.".dbo.Orders_".$column."ed                       
                WHERE  PONumber = '".$PONumber."'
                GROUP BY [PONumber]) as ".$column."Date";
        $i++;
    }
    $q .= " FROM ".DATABASE31.".dbo.salesorder WHERE PONumber ='".$PONumber."'";

    if( $scanHistory = $db->query($q) )
        $result = $scanHistory->results();

    return $result;

}

/**
 * @param $statusRow

 * Let's use the title of the Scanning stations:
 * The error should say one or some of the following:
"ATTENTION! Missing status:
- WAREHOUSE PICK-UP
- PICKING
- PACKING
- LOADING.
Please scan this document to the appropriate station or report it to the Order and Processing Team."
 */
function getPoStatus($statusRow,$module){
    $master_modules = Config::get('master_modules');

    $result = [];
    $result['status'] = '';//'Valid';
    $result['message'] = '<b>ATTENTION!</b> ';//'This product order number is valid.';

    //absent refNumber
    if(empty($statusRow->RefNumber)){
        $result['status'] .= '?Valid';
        $result['message'] .= 'This product order number is not valid.<br />';

        return $result;
    }
    //canceled
    if (!empty($statusRow->Status)){
        $result['message'] .= 'This order has been cancelled. Please do not continue to process this order and return the documents back to Order and Processing.<br />';
        $result['status'] .= '?Cancel';
    }
    //Generate ScanProcess Messages
    $i=0;
    $message1 = '';
    $message2 = '';
    $moduleColumns = array_map("scanStatus", $master_modules);
    while ( $master_modules[$i] != $module ) {
        $column = $moduleColumns[$i];
        if(empty($statusRow->{$column . 'Date'})) {
            $message1 .= '<li> - ';
            if ($i===0)
                $message1 .= 'WAREHOUSE PICK-UP</li>';
            else
                $message1 .=  $column . 'ing</li>';
            $message2 .= '<u>' . $column . 'ing</u>, ';
            $result['status'] .= '?'.$column;
        }
        $i++;
    }
    if( !empty($message1))
        $result['message'] .= 'Missing status: <UL class="po-status">'. $message1 .'</UL>
        Please scan this document to the appropriate station or report it to the Order and Processing Team.';

   return $result ;

}