<?php
/**
 * User: Yeuvheniia Melnykova
 * Date: 3/30/2017
 * Time: 7:18 PM
 */
//time configs
date_default_timezone_set('America/New_York');

session_start();
error_reporting(0);
ini_set('display_errors', 0);
ini_set('display_startup_errors', 0);
$abs_us_root=$_SERVER['DOCUMENT_ROOT'];

$self_path=explode("/", $_SERVER['PHP_SELF']);
$self_path_length=count($self_path);
$file_found=FALSE;

for($i = 1; $i < $self_path_length; $i++){
    array_splice($self_path, $self_path_length-$i, $i);
    $us_url_root=implode("/",$self_path)."/";

    if (file_exists($abs_us_root.$us_url_root.'z_us_root.php')){
        $file_found=TRUE;
        break;
    }else{
        $file_found=FALSE;
    }
}
// Set config
$GLOBALS['config'] = array(
    'session' => array('session_name' => 'id', 'token_name' => 'token'),
    'master_modules' => array('scanning','picking','packing','loading','shipping' )//order is important
);

spl_autoload_register(function($class){
    if($class == 'Database')
        require_once '../db_connect/connect.php';
    else
        require_once 'classes/'.$class.'.php';
});
include_once '../Include/helpers/helpers.php';

$user = new User();
$expiryTime = $user->timeToOff();
//var_dump($user);die();
if (Input::get("login") && Input::get("password")) { //Если логин и пароль были отправлены

    if(!$user->login(Input::get("login"), Input::get("password"))) {
        echo "<script>alert('Wrong login or password!')</script>";
    }else {
        //redirects
        if (isset($_SERVER['HTTP_REFERER']) )
            Redirect::to($_SERVER['HTTP_REFERER']);
        else if( strpos($_SERVER['HTTP_REFERER'],'login.php')<0 )
            Redirect::to('../inventory/');
    }
}
//Exit
if ((isset($_GET["is_exit"])&&($_GET["is_exit"] == 1)) xor $expiryTime < 0) {
    //Если нажата кнопка выхода OR user logged time is less than config timeout
        $user->logout();
    header("Location: ?is_exit=0"); //Редирект после выхода
}
