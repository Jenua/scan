<?php include_once '../Include/init.php'; ?>
<?php include_once '../Include/header.php'; ?>
<?php
if(($user->isLoggedIn()) ){
    Redirect::to('../inventory/index.php');
} else {
?>
    <div class="container text-center">
    <div class="col-sm-10 col-sm-offset-1">
        <div class="logo box">
<!--            <img class="img-thumbnail"  src="../shipping_module/Include/pictures/DreamLineLogo_Color_final-01.png">-->
        </div>
        <h2></h2>
        <form class="form-horizontal" method="post">
            <div class="form-group">
                <label class="control-label col-sm-2" for="login">Login:</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control required" name="login" id="login"
                           value="<?php echo (isset($_POST["login"])) ? $_POST["login"] : null; // Заполняем поле по умолчанию ?>" />
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" for="pwd">Password:</label>
                <div class="col-sm-8">
                    <input type="password" name="password" class="form-control required" id="pwd">
                </div>
            </div>

                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-8">
                        <button type="submit" class="btn btn-info">Login</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <?php
}?>
<?php include_once 'footer.php'; ?>