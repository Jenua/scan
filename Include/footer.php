<?php
/**
 * Include this file into footer of any html
 */
global $abs_us_root, $us_url_root;
?>
</div> <!-- end of container, is opened in the header.php -->
</body>
<?php
$module = Input::get('module');
if (isset($module) && $module != '')
    //custom scripts for any module with the same name(sample: module_name script: module_name.js)
    if (file_exists($abs_us_root . $us_url_root . 'js/' . $module . '.js'))
        echo '<script src="js/' . $module . '.js"></script>';
//custom scripts for any page with the same name(sample: my_page.php script: my_page.js)
if (file_exists($abs_us_root . $us_url_root . 'Include/js/' . str_replace('php', 'js', currentPage())))
    echo '<script type="application/javascript" src="' . $us_url_root . 'Include/js/' . str_replace('php', 'js', currentPage()) . '"></script>';
?>
</html>