<?php
/**
 * User navigation panel
 */
global
    $us_url_root,
    $user;
?>
<div class="row" id="header-row">
    <div class="col-xs-12 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2">
<?php
if ($user->isLoggedIn()) {
    $user_modules = getUserModules($user->data()->id);
    $master_modules = Config::get('master_modules');
    $transTypes = fetchAllTransTypes();
    ?>
<!-- Navigation menu-->

<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#"></a>

        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
        <ul class="nav navbar-nav">

            <li><a href="<?= ($us_url_root === '/')? '/inventory'.$us_url_root : $us_url_root; ?>">
                    <i class="fa fa-home"></i><span> Home</span>
                </a></li>

        <?php
        if(Session::exists('module') && $user_modules !== false ) { ?>
            <li class="dropdown"><a class="dropdown-toggle" href="" data-toggle="dropdown"><i class="fa fa-barcode"></i>
                   Order Systems <b class="caret"></b></a>
                <ul id="inventory-menu" class="dropdown-menu">
                    <?php
                    $inv_url = ($us_url_root === '/') ? $us_url_root . 'inventory/?module=' : $us_url_root . '?module=';
                    foreach ($user_modules as $module_name) {
                        if (in_array($module_name, $master_modules)) {
                            ?>
                            <li><a href="<?= $inv_url . $module_name ?>"><i
                                            class="fa fa-hand-o-right"></i> <?= module_title($module_name,'LTL'); ?></a></li>
                        <?php }
                    }?>

                    <?php if(in_array('loading',$user_modules)){ ?>
                        <li><a href="<?= $inv_url ?>loading&type=FTL"><i class="fa fa-hand-o-right"></i> FTL Loading </a></li>
                    <?php } ?>
                </ul>
            </li>
            <?php //inventory activity menu with types in sub
            if (in_array('inventory_activity', $user_modules)) {
                ?>
                <li class='dropdown'>
                    <a class='dropdown-toggle' href='' data-toggle='dropdown'><i
                                class='fa fa-assistive-listening-systems'></i> Inventory <b class="caret"></b></a>
                    <ul class='dropdown-menu'>
                        <?php
                        $inv_link = ($us_url_root === '/') ? $us_url_root . 'inventory/' : $us_url_root;
                        $inv_link .= '?module=inventory_activity&mType=';
                        foreach ($transTypes as $id => $type) { ?>
                            <li>
                                <a href="<?= $inv_link . $id ?>" title="<?= $type['abr'] ?>"> <?= $type['title'] ?></a>
                            </li>
                        <?php } ?>
                    </ul>
                </li>
                <?php
            }
        }?>

        <?php if($user->isAdmin()) {?>
        <li class="dropdown">
            <a class="dropdown-toggle" href="" data-toggle="dropdown"><i class="fa fa-wrench"></i> Admin <b class="caret"></b></a>

            <ul class="dropdown-menu">
                <li><a href="/admin/manage_users.php"><i class="fa fa-user"></i> Manage users</a></li>
                <li><a href="/admin/permissions.php"><i class="fa fa-briefcase"></i> Permissions </a></li>
                <li><a href="/admin/user_settings.php"><i class="fa fa-cog"></i> Configurations </a></li>
            </ul>
        </li>
        <?php } // End admin menu
        ?>

    </ul>
<?php
//Additional buttons for different systems
$addButton = '';
if (Session::exists('module') && currentPage()=='index.php' && Session::get('module') != '' && Session::get('module') != 'shipping') {
            $addButton = (Session::get('module') == 'rits') ?
            "<button class=\"btn btn-info navbar-btn\" id='print_dialog' title='Printer settings'>
                    <span class='glyphicon glyphicon-print'></span> Printer settings</button>" :
            "<button class=\"btn btn-info navbar-btn\" id='module_accounts_dialog' title='User Accounts'>
                    <span class='glyphicon glyphicon-plus-sign'></span> User List</button>";
            } ?>
            <?php echo ( Session::get('module') === 'inventory_activity')? '': $addButton; ?>
<?php
$helloString = "<li><a href=\"#\"><span class=\"glyphicon glyphicon-user\"></span>  " . $user->data()->login."</a></li> ";

$helloString .= "<li>
<a href='?is_exit=1' title='Next system restart time: ".date('y-m-d H:i',$user->timeToOff()+time())."'>
<span class=\"glyphicon glyphicon-log-in\"></span><span> Logout</span></a></li>";
?>
            <ul class="nav navbar-nav navbar-right">
                <?php echo $helloString; ?>
            </ul>

        </div>
    </div>
</nav>    <!--End of navigation -->

<?php
    if( Session::exists('module')
        && Session::get('module') == 'rits'
        && currentPage()=='index.php'){ ?>
        <!-- Printer settings Dialog-->
        <div id="printer_settings">
            <form id="printerForm">
                <fieldset>
                    <label for="printerName">Printer name</label>
                    <input type="text" name="printerName"
                           id="printerName"
                           value="<?=$user->data()->rits_printer ?>"
                           class="text ui-widget-content ui-corner-all">
                    <label for="copies">Copies</label>
                    <input type="text" name="copies"
                           id="copies"
                           value="<?=$user->data()->rits_print_copies ?>" size="2"
                           class="text ui-widget-content ui-corner-all">
                </fieldset>
            </form>
        </div>
        <!-- End of the printer settings dialog -->
<?php } else
    if (   Session::exists('module')
        && Session::get('module') != ''
        && Session::get('module') != 'shipping' && Session::get('module') != 'inventory_activity'
        && currentPage()=='index.php') {
        $accounts = moduleUserList(Session::get('module'));
        ?>
        <!-- formAccounts-->
        <div id="module_accounts" class="userListPopup">
            <div id="usersBoxes">
            <?php foreach($accounts as $account){ ?>
                <button type="button" class="btn-default btn-lg" name="<?=$account->name?>" value="<?=$account->id?>"> <?= $account->name ?></button>
            <?php } ?>
            </div>
        </div>
<?php } ?>
<?php
}
?>
    </div>
    <div class="col-lg-2 col-md-1"></div>

</div><!-- row   -->
