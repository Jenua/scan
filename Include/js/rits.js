
$(function () {
    let formattedDate = new Date(),
        form = $('#rForm'),
        LOT = $('#LOT'),
        SKU = $('#SKU'),
        mainInput = $('#scanned');

    //Launch QZ for scanning
    connectRitsQZ(true);
    mainInput.focus();
    //autocomplite SKU + LOT <=> Scanned Code
    $(document).on('input','#LOT, #SKU', function () {
        mainInput.val(SKU.val() + " " + LOT.val());
    });
    form.on('input', '#scanned', function () {
        let scanned = mainInput.val().trim(),
            parced = scanned.replace(/\s+/g, " ").split(" ");
        SKU.val(parced[0]).change();
        LOT.val(parced[1]).change();
    });

    form.on("click", "#defaultLot", function () {

        if (this.checked) {
            $("#LOT")
                .val('RP' + (moment().month() + 1) + (moment().year()))
                .trigger('input');
        }
        else {
            $("#LOT")
                .val('')
                .trigger('input');
        }
    });
    /*
     * Data to csv
     * */
    let daterange = $('input[name="daterange"]');
    daterange.on('apply.daterangepicker', function (ev, picker) {
        $(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
    });
    /**
     * Datepicker for report generation
     */
    daterange.daterangepicker({
            locale: {
                format: 'YYYY-MM-DD'
            },
            linkedCalendars: true,
            startDate: formattedDate.setMonth(formattedDate.getMonth() - 1),
            endDate: formattedDate,
            autoApply: true
        },
        function (start, end, label) {
            //alert('A date range was chosen: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
            startDateReport = start.format('YYYY-MM-DD');
            endDateReport = end.format('YYYY-MM-DD');
        });
    $('#print_html').on('click', function (e) {
        (e).preventDefault();
        let form = $('#rForm'),
            sku = SKU.val(),
            lot = LOT.val(),
            path = "../inventory/tmp/" + sku + "_" + lot + ".html";

        $.ajax({
            url: "labelHtml.php",
            data: {SKU: sku, LOT: lot}
        }).done(function () {
            //reload the today printed labels report
            $("#jqGrid").trigger('reloadGrid');
            // print using QZ
            if (qz.websocket.isActive()) {
                printOnHtml(rits_printer, path);
            }
            else {
                alert('Printers are not connected.');
            }
        });
        form[0].reset();
        mainInput.focus();
    });
    /*
     * Print barcode pdf using qz-tray
     * */
    $('#print_pdf').on('click', function (e) {
        (e).preventDefault();
        let form = $('#rForm'),
            sku = SKU.val(),
            lot = LOT.val(),
            currentPrinter = $('#printerName').val(),
            copies = $('#copies').val(),
            path = "../inventory/Pdf/" + sku + "-" + lot + ".pdf";

        $.ajax({
            url: "labelPdf.php",
            data: {SKU: sku, LOT: lot, action: 'print'}
        }).done(function () {
            // print using QZ
            if (qz.websocket.isActive()) {
                printOnPdf(currentPrinter, path, copies);
            }
            else {
                alert('Printers are not connected.');
            }
            //reload the today printed labels report
            $("#jqGrid").trigger('reloadGrid');
        });
        form[0].reset();
        mainInput.focus();
    });

    /**
     * Printer settings window
     */

    let dialog;

    dialog = $("#printer_settings").dialog({

        title: "Set printer settings",
        autoOpen: false,
        height: 320,
        width: 340,
        modal: true,
        buttons: {
            "Update": changePrinterSettings,
            Cancel: function () {
                dialog.dialog("close");
            }
        },
        close: function () {
            dialog.dialog("close");
        }
    });
    $("#print_dialog").button().on("click", function () {
        dialog.dialog("open");
    });
    /* set printer settings */
    dialog.find('#printerName').val(rits_printer);
    dialog.find('#copies').val(rits_print_copies);

    function changePrinterSettings() {
        let settings = {printerName: $('#printerName').val(), copies: $('#copies').val()};

        $.ajax({
            url: 'handler.php',
            dataType: 'json',
            type: 'POST',
            data: {action: 'set_printer', params: JSON.stringify(settings)},
            success: function () {
                dialog.dialog("close");
            }
        })
    }

    form = dialog.find('form').on("submit", function (event) {
        event.preventDefault();
        changePrinterSettings();
    });
    /**
     * Report to csv
     */
    $('a#report').on('click', function (e) {
        e.preventDefault();
        //get report without range selection, default: last month
        let date_from = (typeof startDateReport != "undefined") ? startDateReport : moment().subtract(1, 'month').format('YYYY-MM-DD'),
            date_to = (typeof endDateReport != "undefined") ? endDateReport : moment().format('YYYY-MM-DD');
        $.ajax({
            url: 'handler.php',
            type: 'POST',
            data: {
                action: 'report',
                date_from: date_from,
                date_to: date_to
            },
            beforeSend: function () {
                displayModal();
                displayLoading();
            },
            complete: function (request) {
                hideLoading();
                if (~(request.responseText).indexOf("no_data"))
                    bootstrapMess('warning', 'There are no printed labels from ' + startDateReport + ' to ' + endDateReport + '.');
                else {
                    bootstrapMess('success', 'Data reported into: ' + request.responseText);
                    //autodownloading csv report
                    window.location = request.responseText;
                }
                //$('#report').html("Report done!").toggleClass('btn-info', 1000, "easeInOutQuad").addClass('disabled');
            },
            error: function (request, error) {
                console.log("Ajax request error: " + request.responseText);
            }
        });
    });
});

