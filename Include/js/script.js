/**
 * main js for inventory page
 */
$(function () {
    // The module links will no longer be called.
    $( document ).on('click','#warehouse li', function () {
        let href = $(this).find('a').attr('jshref');

       if ('undefined' === typeof href)
           href = $(this).find('a').attr('href');

        $(".scanning-modules li").not(this).addClass('disabled');

        window.location.href = href;
    });
    // Prevent the possibility to open link in the new tab
    $('ul#systems > li > a').each(function() {
        let href= $(this).attr('href');
        $(this).attr('href','javascript:void(0);');
        $(this).attr('jshref',href);
    });
    $(document).on('click','ul#systems > li > a', function (e) {

        //stop default propagation.

        e.stopImmediatePropagation();
        e.preventDefault();
        e.stopPropagation();

        let href= $(this).attr('jshref');

        //For non mac browsers
        if ( !e.metaKey && e.ctrlKey ){
            alert('You shouldn\'t open more than one system at once.');
            e.metaKey = e.ctrlKey;
        }

        //if ctrl key is not pressed
        if(!e.metaKey)
        {
            //redirect
            location.href= href;
        }
        return false;
    });
    /*
     Open popup only at the inventory system page
     And for Admin exclude popup timer
    */
    if($(document).find('#module_accounts_dialog') && $(document).find('#userID').val()!=2){
    //restart if no activity more than 1 minute
        (function(seconds) {
            let refresh,
                intvrefresh = function() {
                    clearInterval(refresh);
                    refresh = setTimeout(function() {
                        $('.popup').fadeOut(500);
                        $("#module_accounts").dialog( "open" ).fadeIn(500);

                    }, seconds * 1000);
                };

            $(document).on('keypress click', function() { intvrefresh() });
            intvrefresh();

        }(60*5)); // define here seconds
    }

    //menu in one row
    $('.navbar-collapse span:not(.glyphicon):last-child').toggleClass('hidden-lg hidden-md hidden-sm');
});
/*
 * Bootstrap
 * */
$(document).find('[data-toggle="tooltip"]').tooltip();

/**
 * @param status: alert-{danger|warning|info|success}
 * @param message
 */
function bootstrapMess(status, message) {
    $('.popup').fadeIn(1000);
    let statusMsg = ( status =='info' || status =='danger')? 'ATTENTION': status;
    //    Warning: Please check database connection. Please log off and log back on to the system
    let content = '<div class="alert alert-dismissable alert-' + status + '">'
        + '<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>';

    content += '<strong>' + statusMsg + '! </strong> ' + message + '</div>';
    $('.content').append(content);
    //$('.popup').fadeOut(10000);
}

/**
 * Submit event with prevalidation alerts
 */

function displayLoading() {
    $('.page-loading').fadeIn(100);
}

function hideLoading() {
    $('.page-loading').fadeOut(100);
}

function displayModal() {
    $('.darkBack').fadeIn(100);
}
function hideModal() {
    $('.darkBack').fadeOut(100);
}
/* Hide Alert */
function hideAlert() {
    let alerts = $("#message .alert");
    if (alerts.length > 0) {
        setTimeout(() => {
            $("#message .alert:not(:last-child)").fadeOut();
        }, 3000);
    }
}
/**
 * Run the currently selected effect of the error
 * Effect types:
 * blind, bounce, clip, drop, explode, fade, fold,  highlight, puff, pulsate, scale, shake, size, slide,
 * // transfer
 * Demo: https://jqueryui.com/effect/
 */
//sound effect for null values

function runEffect( selectedEffect ) {

    // Most effect types need no options passed by default
    let options = {},
        scanForm = $('#scanForm'),
        headColor = scanForm.prev('div').css('color');

    // some effects have required parameters
    if ( selectedEffect === "scale" ) {
        options = { percent: 50 };
    } else if ( selectedEffect == "highlight"){
        options =  {  color: "#ff0099"
             } ; // #ffff99 - default
    } else if ( selectedEffect === "size" ) {
        options = { to: { width: 200, height: 60 } };
    }
    // Run the effect
    scanForm.css({'background-color': headColor, 'z-index': -1});
    let i = 10; while(i--)
        scanForm.effect(selectedEffect, options, 800);
}

function stopEffect(){

    $('#scanForm')
        .stop(true,true)
        .css({'z-index': 1, 'background-color': 'transparent', display: 'block'});

    $(".main-input").focus();
}
/**
 * Disable Back-Forward for scanning pages
 * Prevent action for browser's History buttons
 * (prevent the Back event is enough, because Forward enabled when Back event was called previously )
 * Disable the Back for all of scanning
 */

(function (global) {

    if(typeof (global) === "undefined")
    {
        throw new Error("window is undefined");
    }

    let _hash = "!";
    let noBackPlease = function () {
        global.location.href += "#";

        // 50 milliseconds for just once do not cost much
        global.setTimeout(function () {
            global.location.href += "!";
        }, 50);
        return "You work may be lost.";
    };

    // Earlier we had set interval here....
    global.onhashchange = function () {
        if (global.location.hash !== _hash) {
            global.location.hash = _hash;
        }
    };

    global.onload = function () {

        noBackPlease();

        // disables backspace on page except on input fields and textarea..
        document.body.onkeydown = function (e) {
            let elm = e.target.nodeName.toLowerCase();
            if (e.which === 8 && (elm !== 'input' && elm  !== 'textarea')) {
                e.preventDefault();
            }
            // stopping event bubbling up the DOM tree..
            e.stopPropagation();
        };

    };

})(window);
