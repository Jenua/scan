/**
 * Created by Yeuvheniia Melnykova on 7/12/2017.
 */
$(function () {
    /* Users accounts values */
        let  usersDialog
            ,selectedUserID = (superUser)? userID : 0;

        /**
         * User accounts window
         */
        ////////////////////////
        /**
         *  Loading FTL form
         */
        /* LOADING Parse Barcode add fields for the scanning system     */
        let lform = $('#lForm'),
            lParams = {};

        /* Loading parser test = PLT_P1 TRU_Truck1 PO_8692563 */

        $(document).on('input', '#scanned', function () {

            let pattern = /(PLT|PO|TRU)_([\w-_]+)/gi,
                scanned = $(this).val(),
                result = '';

            while (result = pattern.exec(scanned)) {

                lParams[result[1]] = result[2];
                $('#' + result[1] + '').val(result[2]).change();
            }
            //console.log(addParams.PLT);
        });
        /**
         * Scanning form validation
         */
        lform.validator().on('submit', function (e) {
            if (e.isDefaultPrevented()) {
                bootstrapMess('warning','Please, fill required data correct.');
            } else {
                e.preventDefault();
                e.stopPropagation();

                let scanType = $('#scannedType').val();


                savedMess += (module != 'loading') ? "Scanned code has <u>" + lParams.PO + " </u> saved." :
                "Pallet ID: <u>" + lParams.PLT + "</u> Truck ID: <u>" + lParams.TRU + "</u> PO Number: <u>" + lParams.PO + "</u> saved.";

                savedMess += " <br> Expand the collapsed 'Scanned today' table to see changes.";
                $.ajax({
                    url: url,
                    method: 'POST',
                    data: {
                        action: 'save',
                        module: module,
                        type: scanType,
                        PO: lParams.PO, Truck: lParams.TRU, Pallet: lParams.PLT,
                        scannedBy: selectedUserID
                    },
                    dataType: 'json',

                    beforeSend: function () {
                        displayModal();
                        displayLoading();
                    },
                    complete: function (request) {
                        hideLoading();
                        hideModal();
                        let res = request.responseText;

                        if (~res.indexOf('data')) {
                            bootstrapMess('success', savedMess);
                        } else {
                            bootstrapMess('danger', res + '');
                        }

                        if (dataWindow.getGridParam("gridstate") == "visible")
                            $(".ui-jqgrid-titlebar-close", dataWindow[0].grid.cDiv).trigger("click");
                        //$(".jqGridScannedOrders").setGridParam('hiddengrid','hidden').trigger();

                    },
                    error: function (request, error) {
                        console.log("Ajax request error: " + request.responseText);
                    }
                });
                form[0].reset();
            }
            return false;  // Stop form submit
        });

});
