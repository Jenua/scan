/**
 * Created by Yeuvheniia Melnykova on 7/10/2017.
 */
$(function () {
    //user activity checkbox action
    let checkboxActivity = $("#is_active");
    //Set
    //Get
    checkboxActivity.on('change', function () {
        let checkbox = $(this);

        return checkbox.val(checkbox.prop('checked') ? 1 : 0);

    });
});