/**
 * Created by Yeuvheniia Melnykova on 3/27/2017.
 */
$(function () {
    $('addUser').on('click', function () {
        $.ajax({
            url: "handler.php",
            data: {action: 'save'}
        }).done(function () {
            //reload the today printed labels report
            $("#jqGrid").trigger('reloadGrid');
        });

    });

    jgrid = $("#jqGridUsers").jqGrid({
        datatype: "json",
        url: "handler.php",
        postData: { action:'data_window' },
        height: 'auto',
        type: 'POST',
        caption: 'Users',
        colNames: ["Edit", "id", "Name", "Login", "Password", "Permission", "Groups", "Delete"],
        colModel: [
            {
                label: "Edit",
                name: "edit",
                width: 60, align: "center", search: false,
                formatter: editLink,
                sortable: false
            },
            {name: 'id', index: 'id', key: true, hidden: true},
            {
                name: 'name', index: 'name', width: 160,
                sorttype: 'string',
                searchoptions: {
                    sopt: ['eq', 'bw', 'bn', 'cn', 'nc', 'ew', 'en', 'nn', 'nu']
                }
            },
            {
                name: 'login', index: 'login',
                sorttype: 'string',
                searchoptions: {
                    sopt: ['eq', 'bw', 'bn', 'cn', 'nc', 'ew', 'en']
                }
            },
            {name: 'password', index: 'password', width: 120, search: false, hidden: true},
            {
                name: 'permission', index: 'permission', width: 60, hidden: false, editable: true,
                sorttype: 'string',
                searchoptions: {
                    sopt: ['eq', 'bw', 'bn', 'cn', 'nc', 'ew', 'en', 'nn', 'nu']
                }
            },
            {
                name: 'groups', index: 'groups', editable: false, search: false, sortable: false,
                edittype: "select", editoptions: { //todo select permissions from DB
                value: "1:Administrator;2:User;3:Warehouse;4:Logist",
                dataInit: function (elem) {
                    $(elem).width(120);  // set the width which you need
                }
            }
            },
            {
                label: "actions",
                name: "actions",
                width: 60,
                align: "center", search: false,
                formatter: "actions",
                formatoptions: {
                    keys: true, delbutton: true, editbutton: false, deltitle: "Delete user from database"
                },
                sortable: false
            }
        ],
        rowNum: 15,
        rowList: [15, 30, 50, 100],
        gridview: true,
        jsonReader: {
            repeatitems: false,
            id: "id"
        },
        width: 'auto',
        pager: '#pagernav',
        sortname: 'id',
        sortorder: 'ASC',
        rownumbers: true,
        viewrecords: true,
        shrinkToFit: true,
        autowidth: true,

        editurl: 'handler.php',
        multiselect: true,
        //caption: "Manipulating Array Data"
        autoencode: true, // the data for the cells should be interpreted as text and not as HTML fragment
        //deactivator users
        //sort per page
        onSortCol: function (index, columnIndex, sortOrder) {
            let currPage = parseInt($('#pagernav').find("input.ui-pg-input").val());
            $(this).jqGrid('setGridParam', {
                page: currPage,sortname:index,sortorder:sortOrder
            }).trigger('reloadGrid');
        }
    });

    jgrid.navGrid('#pagernav',
        // the buttons to appear on the toolbar of the grid
        {
            edit: false, //hide button in the navGrid
            add: false,
            del: false,
            search: true,
            refresh: true,
            view: false,
            position: "left",
            cloneToTop: false
        },
        {}, // use default settings for edit
        {}, // use default settings for add
        {},  // delete instead that del:false we need this
        {multipleSearch: false}, // enable the advanced searching
        {closeOnEscape: true} /* allow the view dialog to be closed when user press ESC key*/
    );
    //buttons in the navGrid
    parameters = {
        edit: true,
        editicon: "ui-icon-person",
        editCaption: 'Change the user\'s permission',
        edittext: 'Change permission',
        add: false
    }
    jgrid.inlineNav('#pagernav', parameters);

    function editLink(cellValue, options, rowdata, action) {

         return (rowdata.is_active != 1)?  "<span class ='ui-icon ui-icon-circle-close'></span>":
            "<a title='Edit user' href='admin_users.php?id=" + rowdata.id + "' class='ui-icon ui-icon-person' ></a>";

    }

    //Activate users
    function setUserActivity(selusers) {
        jgrid.jqGrid('setGridParam', {
                datatype: 'json',
                postData: { action:'multiActivator', selectedUsers: selusers }
            }).trigger("reloadGrid");
    }

    $("#toggleUserActive").click(function () {

        var s, userIDs = {};

        s = jgrid.jqGrid('getGridParam', 'selarrrow');
        iCol = jgrid.getRowData(s);//,$(e.target).closest("td")[0]
        userIDs = $.makeArray(jgrid.jqGrid('getGridParam', 'selarrrow').map(function (elem) {
            return jgrid.jqGrid('getCell', elem, 'id');
        }));
        setUserActivity(userIDs);
        //console.log(userIDs);
    });



});
//jqGrid adaptive
$(window).on("resize", function () {
    var $grid = $('[class^="jqGrid"]'),
        newWidth = $grid.closest(".ui-jqgrid").parent().width();
    $grid.jqGrid("setGridWidth", newWidth, true);
});