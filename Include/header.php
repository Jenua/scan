<?php
/**
 * Include this file into header of any html
 */
ob_start();
//header('X-Frame-Options: SAMEORIGIN');
?>
<?php
//check for a custom page
$currentPage = currentPage();
if(isset($_GET['err'])){
    $err = Input::get('err');
}

if(isset($_GET['msg'])){
    $msg = Input::get('msg');
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- To ensure proper rendering and touch zooming for all devices -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php if (isset($expiryTime) && $expiryTime>0) { ?>
        <meta http-equiv="refresh" content="<?=$expiryTime?>">
    <?php } ?>

    <title><?= (Input::get('module') and Input::get('module')!='') ? module_title(Session::get('module')) : page_title() ?></title>

    <!-- Bootstrap Core CSS -->
    <link rel="stylesheet" href="../Include/frameworks/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../Include/frameworks/bootstrap/css/bootstrap-theme.min.css">

    <!-- Custom Fonts/Animation/Styling-->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

    <script src="../Include/frameworks/jquery/2.2.4/jquery.min.js"></script>

    <script type="text/javascript" src="../Include/frameworks/jquery/moment.min.js"></script>

    <script src="../Include/frameworks/bootstrap/js/bootstrap.min.js"></script>
    <script src="../Include/frameworks/bootstrap-validator-master/js/validator.js"></script>
    <!--End of new-order styles end-->

    <!-- jqGridFree -->

    <link rel="stylesheet" href="../Include/frameworks/free-jqgrid/css/ui.jqgrid.min.css">

    <script src="../Include/frameworks/free-jqgrid/js/jquery.jqgrid.min.js"></script>

    <script src="../Include/frameworks/jquery-ui-1.12.1.custom/jquery-ui.min.js"></script>
    <link rel="stylesheet" href="../Include/frameworks/jquery-ui-1.12.1.custom/jquery-ui.min.css">
    <link rel="stylesheet" href="../Include/frameworks/jquery-ui-1.12.1.custom/jquery-ui.structure.min.css">
<!--    <link rel="stylesheet" href="../frameworks/jquery-ui-1.12.1.custom/jquery-ui.theme.min.css">-->

    <!-- Include Date Range Picker -->
    <script type="text/javascript" src="../Include/frameworks/bootstrap.daterangepicker/daterangepicker.js"></script>
    <link rel="stylesheet" type="text/css" href="../Include/frameworks/bootstrap.daterangepicker/daterangepicker.css" />
    <!-- Timepicker -->
    <?php if (currentPage() == 'user_settings.php'): ?>
        <link rel="stylesheet" type="text/css" href="../Include/frameworks/jquery/timepicker/timepicker.css">
        <script src= "../Include/frameworks/jquery/timepicker/timepicker.js"></script>
        <script>
        $(function () {
            $('.timepicker').timepicker({
                showPeriod: false,
                showLeadingZero: true
            });
            $(".timepicker").attr("autocomplete","off");
        })
        </script>
    <?php endif; ?>

    <!-- Primary JS -->
    <script type="text/javascript" src="../Include/js/script.js"></script>
    <!-- Primary CSS -->
    <link rel="stylesheet" href="../Include/css/style.css">
</head>
<body class="nav-md">
<?php
if(isset($_GET['err'])){
    err("<br>".$err);
}

if(isset($_GET['msg'])){
    bold("<br>".$msg);
}

?>
<div class="container"> <!-- closed in the footer -->