<?php
/*print_r("<pre>");
print_r($_REQUEST);
print_r("</pre>");
die();*/
ini_set('max_execution_time', 1200);
error_reporting( E_ERROR );

$path = $_SERVER['DOCUMENT_ROOT'];
require_once($path.'/db_connect/connect.php');
if(isset($_REQUEST['po']) && !empty($_REQUEST['po']) && isset($_REQUEST['trucks']) && !empty($_REQUEST['trucks']))
{
	$result_type = 1;
	if(isset($_REQUEST['type']) && !empty($_REQUEST['type']) ) {
		if( $_REQUEST['type'] == 'simple' ) {
			$result_type = 0;
		}
	}

	$po = $_REQUEST['po'];
	$trucks = json_decode($_REQUEST['trucks']);
        
        $truck_UPC_arr = array();
        foreach( $trucks as $truck ) {
            $truck_id   = explode('_', $truck[0])[1];
            $truck_UPCs = $truck[1];
            
            
            foreach($truck_UPCs as $UPC) {
                if( $UPC != '' ) {                
                    if( !isset($truck_UPC_arr[$truck_id][$UPC]) ){
                        $truck_UPC_arr[$truck_id][$UPC] = 0;
                    }
                    $truck_UPC_arr[$truck_id][$UPC] += 1;
                }
            }
        }
        
        $preload_query = "SELECT a.*
                               , b.[SKU]
                              FROM [".DATABASE."].[dbo].[truck_preload] as a
                              JOIN [".DATABASE."].[dbo].[DL_valid_SKU] as b ON a.[UPC] = b.[UPC]
                             WHERE [PONUMBER] = ".addquote($po)."
                          ORDER BY [TRUCK];";        
        try {
                if( $result_type ) {
                        save_loaded_data( $truck_UPC_arr, $po );
                }
                $preload_arr = runQuery_with_result($preload_query);

                $preload_arr = prepare_preload( $preload_arr );
                $compare_msg = compare_load( $preload_arr, $truck_UPC_arr );
                $compare_msg = translateMessage( $compare_msg, $result_type );

                print_r( json_encode($compare_msg) );

        }
        catch (PDOException $e)
        {	
                die('Error. Can not run queries: '.$queries.'');
        }

        
        
        
        
	
} else
{
	echo "Can not get data.";
}


//=================================================================================================================================
function prepare_preload( $src_arr ) {
    $result = array();
    foreach( $src_arr as $src ) {
        $result[ $src['TRUCK'] ][ $src['UPC'] ] = array( $src['QUANTITY'], $src['SKU'] );
    }
    return $result;
}

function compare_load( $preload, $load ) {
    
    $message = array();
    
    foreach( $preload as $truck_id => $pre_truck ) {        
        $load_truck = $load[$truck_id];        
        if( !is_array($load_truck) ){
            $load_truck = array();
        }
        
        $pUPC = array_keys( $pre_truck );
        $lUPC = array_keys( $load_truck );

        $both_UPC = array_intersect( $pUPC, $lUPC );
        $pre_UPC  = array_diff( $pUPC, $lUPC );
        $load_UPC = array_diff( $lUPC, $pUPC );

        foreach( $pre_UPC as $UPC ){
            $message[$truck_id]['preload'][$UPC] = $pre_truck[$UPC];
        }

        foreach( $load_UPC as $UPC ){
            $message[$truck_id]['load'][$UPC][0] = $load_truck[$UPC];
            $message[$truck_id]['load'][$UPC][1] = '';
        }

        foreach( $both_UPC as $UPC ){
            $diff = $pre_truck[$UPC][0] - $load_truck[$UPC];
            if( $diff != 0 ) {
                $message[$truck_id]['diff'][$UPC] = array( $diff, $pre_truck[$UPC][1] );
            }
        }         
        
    }
    return $message;
}
function translateMessage( $message, $type ) {
    
    $msg = array();
    
    foreach( $message as $truck => $data ) {
        $msg[$truck] = '';
        
        if( isset( $data['preload'] ) && $type ){
            $msg[$truck] .= "Not loaded at all:<br>";
            $msg[$truck] .= "<ul>";
            foreach( $data['preload'] as $UPC => $value ) {
                $qty = $value[0];
                $sku = $value[1];
                $msg[$truck] .= "<li>".$qty." x ".$UPC." ( ".$sku." )</li>";
            }
            $msg[$truck] .= "</ul><br>";
        }
        if( isset( $data['load'] ) ){
            $msg[$truck] .= "Loaded, but not expected:<br>";
            $msg[$truck] .= "<ul>";
            foreach( $data['load'] as $UPC => $value ) {
                $qty = $value[0];
                $sku = $value[1];
                $msg[$truck] .= "<li>".$qty." x ".$UPC."</li>";
            }
            $msg[$truck] .= "</ul><br>";
        }
        if( isset( $data['diff'] ) ){
            $msg[$truck] .= "Loaded, but different count:<br>";
            $msg[$truck] .= "<ul>";
            foreach( $data['diff'] as $UPC => $value ) {
                $qty = $value[0];
                $sku = $value[1];                
                if( $type ) {
                    $msg[$truck] .= "<li>".$UPC." ( ".$sku." ) -- ".abs($qty)." ".($qty>0?"more":"less")." needed</li>";
                } else {
                    if( $qty<0 ) {
                        $msg[$truck] .= "<li>".$UPC." ( ".$sku." ) -- ".abs($qty)." less needed</li>";
                    }
                }
                
            }
            $msg[$truck] .= "</ul><br>";
        }
        
        
    }
    
    return $msg;
}

function generate_query($arr, $po){
	$queries = "DELETE FROM [".DATABASE."].[dbo].[truck_load] WHERE [PONUMBER] = '".addquote($po)."'; ";
        foreach( $arr as $truck => $data ) {
            foreach($data as $upc => $qty) {
                    if ($qty > 0 && preg_replace('~\D+~','',$upc) == $upc) {
                            $queries.= "                     
                                INSERT INTO [".DATABASE."].[dbo].[truck_load]
                                ([PONUMBER]
                                ,[UPC]
                                ,[QUANTITY]
                                ,[TRUCK]
                                ,[DISTRIBUTE_DATE])
                                VALUES
                                ('".addquote($po)."'
                                ,'".addquote($upc)."'
                                ,'".addquote($qty)."'
                                ,'".addquote($truck)."'
                                ,GETDATE());";
                    }
            }
        }
        return $queries;
}
function save_loaded_data( $arr, $po ) {
    $queries = generate_query($arr, $po);
    runQuery_empty_result($queries);
    
}

//=================================================================================================================================
function beginTransaction()
{
	try
	{
		$conn = Database::getInstance()->dbc;
		$result = $conn->beginTransaction();
		return $result;
	}catch (Exception $e) {
		die("<br>".$e."<br>");
		return false;
	}
}

function commitTransaction()
{
	try
	{
		$conn = Database::getInstance()->dbc;
		$result = $conn->commit();
		return $result;
	}catch (Exception $e) {
		die("<br>".$e."<br>");
		return false;
	}
}

function rollbackTransaction()
{
	try
	{
		$conn = Database::getInstance()->dbc;
		$result = $conn->rollback();
		return $result;
	}catch (Exception $e) {
		die("<br>".$e."<br>");
		return false;
	}
}

function runQuery_empty_result($query)
{
	try
	{
		$conn = Database::getInstance()->dbc;
                $stmt = $conn->prepare($query);
		$result = $stmt->execute();
		return $result;
	}catch (Exception $e) {
		die("<br>".$e."<br>");
		return false;
	}
}

function runQuery_with_result($query)
{
	try
	{
		$conn = Database::getInstance()->dbc;
		$stmt = $conn->prepare($query);
		$stmt->execute();
                $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}catch (Exception $e) {
		die("<br>".$e."<br>");
		return false;
	}
}

function addquote($str)
{
	$str = str_replace("'", "`", $str);
	return $str;
}
?>