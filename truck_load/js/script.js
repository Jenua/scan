function display_messages( messages ) {
    for (var key in messages) {
        $('#truck_'+key+'_notes').html( messages[key] );
    }    
}

function simpleHandler(field, editor, tags) {
        var tovalidate = new Array();

        $( '#truckForm textarea' ).each(function(){
            var tagstr = $(this).tagEditor('getTags')[0].tags;
            var tagarr = tagstr.toString().split(',');
            var resultarr = new Array( this.id, tagarr );
            //var resultstr = JSON.strigify( resultarr );
            tovalidate.push( resultarr );            
        });

        $.ajax({
                type: "POST",
                dataType: "JSON",
                url: $('#truckForm').attr('action'),
                data: {type: "simple", trucks: JSON.stringify( tovalidate )},
                success: function(data) {
                        var result = data;                        
                        var resmessage = '';                        

                        try{
                            console.log( result );
                            resmessage = 'Data Saved';
                            display_messages( result );                    
                        } catch(e) {
                            // NOT JSON == ERROR or SMTH
                            console.log( e );
                        }                   
                }
        });
}

$(document).ready(function() {
    var dialog = $('#dialog').dialog({
        autoOpen: false,
        height: 'auto',
        width: 'auto',
        resizable: false,
        autoResize: true,
        modal: true,
	title: '',
        overlay: { backgroundColor: "#000", opacity: 0.5 },
    });
    dialog.dialog('close');
	
    //Submit event with prevalidation
    $('#truckForm').validator('validate').on('submit', function (e) {
        
        event.preventDefault();
        var form = $('#truckForm');
        var url = $(form).attr("action");
        
        var tovalidate = new Array();
        
        $( 'textarea', this ).each(function(){
            var tagstr = $(this).tagEditor('getTags')[0].tags;
            var tagarr = tagstr.toString().split(',');
            var resultarr = new Array( this.id, tagarr );
            //var resultstr = JSON.strigify( resultarr );
            tovalidate.push( resultarr );            
        });
        
        $.ajax({
            url: url,
            type: 'POST',
            data: {type: "full", trucks: JSON.stringify( tovalidate )},
            complete: function(data){
                console.log(data.responseText);
                result = data.responseText;
                dialog.dialog('close');
                
                var resmessage = '';
                var messages = '';
                
                try{
                    messages = JSON.parse(result);
                    resmessage = 'Data Saved';
                    display_messages( messages );                    
                } catch(e) {
                    // NOT JSON == ERROR or SMTH
                    resmessage = result;
                }
  

                
                dialog.dialog('option', 'title', 'Result');
                $('#content').html(resmessage);
                dialog.dialog('open');
            },
            error: function (request, status, error) {
                console.log("Ajax request error: "+request.responseText);
            }
        });
    });
});
