<?php
session_start();
$path = $_SERVER['DOCUMENT_ROOT'];
require_once($path.'/db_connect/connect.php');
require_once($path.'/auth/auth.php');
 
$auth = new AuthClass();

function exec_query($query)
{
	try{
		$conn = Database::getInstance()->dbc;
		$result = $conn->prepare($query);
		$result->execute();
		$result = $result->fetchAll(PDO::FETCH_ASSOC);
		$conn = null;
		return $result;
	}
	catch(PDOException $e)
	{
		$conn = null;
		die($e->getMessage());
	}
}

if (isset($_POST["login"]) && isset($_POST["password"])) {
    if (!$auth->auth($_POST["login"], $_POST["password"])) {
        echo "<script>alert('Wrong login or password!')</script>";
    }
}
 
if (isset($_GET["is_exit"])) {
    if ($_GET["is_exit"] == 1) {
        $auth->out();
        header("Location: ?is_exit=0");
    }
}

if ($auth->isAuth()) {

if (isset($_GET['po']) && !empty($_GET['po']))
{
	$po = $_GET['po'];
	$query = "SELECT [TRUCK]
                       , SUM( CAST([QUANTITY] AS int) ) AS QTY
                    FROM [".DATABASE."].[dbo].[truck_preload]
                   WHERE [PONUMBER] = '".$po."'
                GROUP BY [TRUCK]
                ORDER BY [TRUCK]";        
	$results = exec_query($query);
        
	$query = "SELECT [TRUCK]
                       , [".DATABASE31."].[dbo].GROUP_CONCAT_S( [UPCs], 1 ) AS UPCs
                    FROM ( SELECT [ID]
                                , REPLICATE(([UPC]+','), [QUANTITY]) as UPCs
                                , [TRUCK]
                             FROM [".DATABASE."].[dbo].[truck_load]
                            WHERE [PONUMBER] = '".$po."'
                         ) a
                GROUP BY [TRUCK]
                ORDER BY [TRUCK]";        
        $data = exec_query($query);
        
	if ( (!isset($results) || empty($results)) && (!isset($data)) ) die('<br>Can not get data on this PO: '.$po.'<br>');
	
	
} else die('<br>Can not get PO.<br>');
?>
<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Truck Load</title>
  <link rel="stylesheet" href="Include/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="Include/bootstrap/css/bootstrap-theme.min.css">
  <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <link rel="stylesheet" href="Include/jquery-tageditor/jquery.tag-editor.css">
  <script src="Include/jQuery/jquery-1.11.3.min.js"></script>
  <script src="Include/bootstrap/js/bootstrap.min.js"></script>
  <script src="Include/jquery-ui-1.11.4.custom/jquery-ui.js"></script>
  <script src="Include/bootstrap-validator-master/js/validator.js"></script>
  
  <script src="Include/jquery-ui-1.11.4.custom/jquery-ui.js"></script>
  <script src="Include/jquery-tageditor/jquery.caret.min.js"></script>
  <script src="Include/jquery-tageditor/jquery.tag-editor.js"></script>
  
  <script src="js/script.js"></script>
  
</head>
<body>

	<?php 
	$user = $auth->getName();
	echo "<div style='position : absolute; left: 50%; margin-left: -160px; top: 0px; width: 500px; height: 20px;'>Hello, " . $user .". <a href='?is_exit=1'><button class='loginsubmit'>Exit</button></a></div>";
	?>
	<br>
        <br>
        <form data-toggle="validator" role="form" id="truckForm" action="handler.php?po=<?php echo $po; ?>" method="POST">
            
        <div style="padding: 10px;">
        <?php
        foreach( $results as $result ) {
            echo "<h3>Truck ".$result['TRUCK']." (<b>".$result['QTY']."</b> items in preload)</h3>";
            echo "<p style='color:red' id='truck_".$result['TRUCK']."_notes'></p>";
            echo "<textarea id='truck_".$result['TRUCK']."'>";
            foreach( $data as $row ) {
                if( $row['TRUCK'] == $result['TRUCK'] ) {
                    echo $row['UPCs'];
                }
            }
            echo "</textarea><br><br>";
            ?>
            <script>
                    $(document).ready(function(){
                            var truck = '<?php echo $result['TRUCK'] ?>';
                            
                            $('#truck_'+truck).tagEditor({
                                    removeDuplicates: false,
                                    onChange: simpleHandler,
                                    beforeTagDelete: function(field, editor, tags, val) {
                                            return confirm('Remove UPC ' + val + ' ?');
                                    }
                            });
                    })
            </script>
            <?php
        }
        
        
        
        ?>
        <button type="submit" style="float: left;" class="btn btn-success">Submit</button>
        </div>       
        </form>


<?php
}
else { //Если не авторизован, показываем форму ввода логина и пароля
?>
<div style="position: relative; top: 110px; width: 100%;">
<div style="
position: relative;
width: 330px;
height: 160px;
left: 50%;
margin-left: -160px;
padding: 10px;
background: #367FBB;
">
<form method="post" action="">
<fieldset>
    <p><label class="field" for="login">Login:</label><input class="logininput" type="text" name="login"
    value="<?php echo (isset($_POST["login"])) ? $_POST["login"] : null; // Заполняем поле по умолчанию ?>" /></p>
    <p><label class="field" for="password">Password:</label><input class="logininput" type="password" name="password" value="" /></p>
	<p><label class="field" for="password"> </label><input class="loginsubmit" type="submit" value="login" /></p>
</fieldset>
</form>
</div>
<div style="bottom: 2px; width: 100%; text-align:center; margin: 10px 0 0 0;"><p>Copyright © 2015 Dreamline</p></div>
</div>
<?php 
}
?>

<div id="dialog">
	<div id="content"></div>
</div>
</body>
</html>
<?php
function is_truck_exists($preload, $truck)
{
	$exists = false;
	foreach($preload as $value)
	{
		if($value['TRUCK'] == $truck) $exists = true;
	}
	return $exists;
}

function merge_same_groups($results)
{
	foreach($results as $key => $result)
	{
		$results[$key] = array(
			'PONumber' => $result['PONumber'],
			'RefNumber' => $result['RefNumber'],
			'UPC' => $result['UPC'],
			'Prod_desc' => $result['Prod_desc'],
			'Quantity' => $result['Quantity'],
			'Quantity_left' => $result['Quantity'],
			'preload' => array(array(
					'distributed_qty' => $result['distributed_qty'],
					'TRUCK' => $result['TRUCK']
				)
			)
		);
	}
	
	
	
	$final_results = array();
	foreach($results as $key => $result)
	{
		$temp_preload = array();
		foreach($results as $key2 => $result2)
		{
			if($result['UPC'] == $result2['UPC'])
			{
				if(isset($temp_preload[0]) && $temp_preload[0]['distributed_qty'] == 0)
				{
					$temp_preload = $temp_preload;
				} else{
					$temp_preload[]= $result2['preload'][0];
				}
				unset($results[$key2]);
			}
		}
		
		$distributed_qty = 0;
		foreach($temp_preload as $temp_pre)
		{
			$distributed_qty+=$temp_pre['distributed_qty'];
		}
		$Quantity_left = $result['Quantity'] - $distributed_qty;
		
		if (!empty($temp_preload))
		{
			$final_results[]= array(
				'PONumber' => $result['PONumber'],
				'RefNumber' => $result['RefNumber'],
				'UPC' => $result['UPC'],
				'Prod_desc' => $result['Prod_desc'],
				'Quantity' => $result['Quantity'],
				'Quantity_left' => $Quantity_left,
				'preload' => $temp_preload
			);
		}
	}
	unset($results);
	
	/*print_r("<pre>");
	print_r($final_results);
	print_r("</pre>");
	die();*/
	
	return $final_results;
}


