<?php
$header_name = "Duplicating Orders Report";
require_once( "../shipping_module/header.php" );
?>
<?php
if ($auth->isAuth()) {

$user_name = $auth->getName();
$conn = Database::getInstance()->dbc;

$settings = getSettings($conn);

$query = "SELECT [Duplicating_Orders_Report].[id]
      ,[Duplicating_Orders_Report].[PONUMBER]
      ,[Duplicating_Orders_Report].[TimeCreated]
      ,[Duplicating_Orders_Report].[ShipAddress_Addr1]
      ,[Duplicating_Orders_Report].[ShipAddress_Addr2]
      ,[Duplicating_Orders_Report].[ShipAddress_PostalCode]
      ,[Duplicating_Orders_Report].[CustomerRef_FullName]
      ,[Duplicating_Orders_Report].[Active_Duplicated]
      ,[Duplicating_Orders_Report].[CARRIERNAME]
      ,[Duplicating_Orders_Report].[Model]
      ,[Duplicating_Orders_Report].[sent]
      ,[Duplicating_Orders_Report].[RefNumber]
	  ,[Duplicating_Orders_Report].[check_sum]
      ,[Duplicating_Orders_Report].[investigated_user]
      ,[Duplicating_Orders_Report].[investigated_time]
      ,[Duplicating_Orders_Report].[fixed_user]
      ,[Duplicating_Orders_Report].[fixed_time]
  FROM [".DATABASE31."].[dbo].[Duplicating_Orders_Report] 
  LEFT JOIN [".DATABASE31."].[dbo].[invoice] ON [Duplicating_Orders_Report].[PONUMBER] = [invoice].[PONUMBER]
  WHERE [fixed_time] IS NULL
  AND [check_sum] NOT IN(SELECT [check_sum] FROM [".DATABASE31."].[dbo].[Duplicating_Orders_Report] where [fixed_time] IS NOT NULL)
  AND [invoice].[ShipMethodRef_FullName] != 'cancelled'
  AND [invoice].[ShipMethodRef_FullName] != 'void'
  ORDER BY [Model], [ShipAddress_Addr1], [ShipAddress_Addr2], [ShipAddress_PostalCode], [Active_Duplicated]";
$result = exec_query($conn, $query);
$result = $result->fetchAll();
/*print_r('<pre>');
print_r($query);
print_r('</pre>');
die();*/
?>

	<title><?php echo $header_name; ?></title>
	<link rel="stylesheet" href="Include/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="Include/bootstrap/css/bootstrap-theme.min.css">
	<link rel="stylesheet" href="Include/style/style.css">

	<script src="js/duplicate_order.js"></script>
	<script src="Include/bootstrap/js/bootstrap.min.js"></script>
	<style>

	</style>
</head>

<body>
<?php require_once($path.'/Include/header_section.php'); ?>
<!--		<div id="headerContainer">-->
<!--			<div class="logo"><img src="Include/pictures/DreamLineLogo_Color_final-01.png"></div>-->
<!--			<div class="product">-->
<!--				<i>Duplicating Orders Report</i>-->
<!--			</div>-->
<!--		</div>-->

<table data-toggle='table' class='table table-bordered display table-striped'>
<thead>
<tr class='info'>
	<th>PO</th>
	<th>Created (date)</th>
	<th>Created (time)</th>
	<th>Dealer</th>
	<th>Carrier</th>
	<th>Model</th>
	<th>Addr1</th>
	<th>Addr2</th>
	<th>Zip</th>
	<th>Active/Duplicated</th>
	<th>Reported first</th>
	<th>Investigated</th>
	<th>Fixed</th>
</tr>
</thead>
<tbody>
<?php
if($result && !empty($result))
{
	$check_sum = $result[0]['check_sum'];
	foreach ($result as $key => $res)
	{
		if ($res['check_sum'] != $check_sum) echo "<tr>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			</tr>";
			$ts = strtotime($res['TimeCreated']);
			$res['DateCreated'] = date("m-d-Y", $ts);
			$res['TimeCreated'] = date("H:i:s", $ts);
			echo "
				<tr>
					<td><a target='_blank' href='/shipping_module/order_status_history.php?po=".urlencode($res['PONUMBER'])."&id=".urlencode($res['RefNumber'])."'>".$res['PONUMBER']."</a></td>
					<td>".$res['DateCreated']."</td>
					<td>".$res['TimeCreated']."</td>
					<td>".$res['CustomerRef_FullName']."</td>
					<td>".$res['CARRIERNAME']."</td>
					<td>".$res['Model']."</td>
					<td>".$res['ShipAddress_Addr1']."</td>
					<td>".$res['ShipAddress_Addr2']."</td>
					<td>".$res['ShipAddress_PostalCode']."</td>
					<td>".$res['Active_Duplicated']."</td>
					<td>".$res['sent']."</td>";
			if (!empty($res['investigated_user'])) echo "<td>Under investigation by user: ".$res['investigated_user']."</td>"; else
			{
					echo "<td><button onclick='inv(".'"'.$res['check_sum'].'"'.", ".'"'.$user_name.'"'.")'>I will investigate it</button></td>";
			}
			echo "<td><button onclick='report_fix(".'"'.$res['check_sum'].'"'.")'>I fixed this problem</button></td>";
			echo "</tr>";
		$check_sum = $res['check_sum'];
	}
} else
{
	echo "
			<tr>
				<td>N/A</td>
				<td>N/A</td>
				<td>N/A</td>
				<td>N/A</td>
				<td>N/A</td>
				<td>N/A</td>
				<td>N/A</td>
				<td>N/A</td>
				<td>N/A</td>
				<td>N/A</td>
				<td>N/A</td>
				<td>N/A</td>
				<td>N/A</td>
			</tr>";
}
?>
</tbody>
</table>

<div id="duplicateBackground">
				<div id="duplicateMessageBox">
					<span id="messageSpan" onclick="duplicateCancel()">x</span>
					<label>Please choose duplicate reason:</label>
					<center>
						<select onchange="showDuplicateReasonNote()" id="reasonSelectDuplicate">
						  <?php
							$duplicate_reasons = '';
							foreach ($settings as $setting)
							{
								if ($setting['group_name'] == 'duplicate_reasons' && $setting['type_name'] == 'reasons')
								$duplicate_reasons = $setting['setting_value'];
							}
							$duplicateReasons = explode(",", $duplicate_reasons);
							foreach ($duplicateReasons as $duplicateReason)
							{
								echo '<option value="'.$duplicateReason.'">'.$duplicateReason.'</option>';
							}
							?>
						</select>
					</center>
					<label id="duplicateLabel" style="display: none;">Please type duplicate reason:</label>
					<textarea id="reasonNoteDuplicate" rows="7" cols="39" name="text" maxlength="255"></textarea>
					<br>
					<br>
					<button onclick="duplicateOrder(<?php echo "'".$auth->getName()."'"; ?>)">OK</button>
				</div>
			</div>
</body>
</html>

<?php
}

function exec_query($conn, $query)
{
	try{
		$result = $conn->prepare($query);
		$result->execute();
		return $result;
	}
	catch(PDOException $e)
	{
		print_r($e->getMessage());
	}
}

function getSettings($conn)
{
	$query = "SELECT * FROM ".GENERAL_SETTINGS." WHERE [app] = 'shipping_module' ORDER BY [group_name]";
	$result = $conn->prepare($query);
	$result->execute();
	$result = $result->fetchAll(PDO::FETCH_ASSOC);
	if (!isset($result) || empty($result)) die('Can not get settings.');
	return $result;
}
?>