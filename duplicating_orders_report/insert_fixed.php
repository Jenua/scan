<?php
$path = $_SERVER['DOCUMENT_ROOT'];
require_once($path.'/db_connect/connect.php');
date_default_timezone_set('America/New_York');


	if ($_REQUEST['check_sum'] && $_REQUEST['user'] && $_REQUEST['report'])
	{
		$check_sum = $_REQUEST['check_sum'];
		$user = $_REQUEST['user'];
		$report = $_REQUEST['report'];
		$note = $_REQUEST['note'];

		$conn = Database::getInstance()->dbc;
		
		$query_first = "select [fixed_user] from [".DATABASE31."].[dbo].[Duplicating_Orders_Report] oriv where oriv.[check_sum] = '".$check_sum."'";
		
		$query = "UPDATE [".DATABASE31."].[dbo].[Duplicating_Orders_Report]
		   SET [fixed_user] = '".$user."'
			  ,[fixed_time] = GETDATE()
			  ,[report] = '".$report."'
			  ,[note] = '".$note."'
		 WHERE [check_sum] = '".$check_sum."'";
		
		$res_first = exec_query($conn, $query_first);
		$res_first = $res_first->fetchAll();
		if (isset($res_first[0]['fixed_user']) && !empty($res_first[0]['fixed_user']))
		{
			print_r('This problem is already fixed by user: '.$res_first[0]['fixed_user'].'.');
		} else
		{
			$res = exec_query($conn, $query);
			if ($res)
			{
				print_r('You reported this problem as fixed');
			} else
			{
				print_r('Error. Can not update order status.');
			}
		}
		$conn = null;
	} else print_r('Error. No check_sum or user.');

function exec_query($conn, $query)
{
	try{
		$result = $conn->prepare($query);
		$result->execute();
		return $result;
	}
	catch(PDOException $e)
	{
		print_r($e->getMessage());
	}
}
?>