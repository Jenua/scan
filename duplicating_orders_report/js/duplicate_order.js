console.log('log started');

var check_sum = '';

function inv(check_sum, user)
{
	$.ajax({
			url: 'insert_investigated.php',
			type: 'POST',
			data: {
				check_sum: check_sum,
				user: user
			},
			complete: function(data){
				alert(data.responseText);
				window.location.reload();
			},
			error: function (request, status, error) {
				console.log("Ajax request error: "+request.responseText);
			}
		});
}

function duplicateCancel()
{
	$('#duplicateBackground').fadeOut(100);
	$("#reasonNoteDuplicate").val('');
	check_sum = '';
}

function report_fix(check_sum_in)
{
	check_sum = '';
	check_sum = check_sum_in;
	$('#duplicateBackground').fadeIn(100);
	setFocusToId('#reasonSelectDuplicate');
}

function duplicateOrder(userName)
{
	var reason = $("#reasonSelectDuplicate option:selected").val();
	var reasonNote = $("#reasonNoteDuplicate").val();
	
	$.ajax({
			url: 'insert_fixed.php',
			type: 'POST',
			data: {
				check_sum: check_sum,
				user: userName,
				report: reason,
				note: reasonNote
			},
			complete: function(data){
				alert(data.responseText);
				window.location.reload();
			},
			error: function (request, status, error) {
				console.log("Ajax request error: "+request.responseText);
			}
		});
		
	duplicateCancel();
}

function setFocusToId(id){
	setTimeout( function() { $(id).focus(); }, 100 );
}

function showDuplicateReasonNote()
{
	var lastValue = $('#reasonSelectDuplicate option:last-child').val();
	var selectedValue = $( "#reasonSelectDuplicate option:selected" ).text();
	if(lastValue == selectedValue)
	{
		$('#duplicateLabel').fadeIn(50);
		$('#reasonNoteDuplicate').fadeIn(50);
		setFocusToId('#reasonNoteDuplicate');
	} else
	{
		$('#duplicateLabel').fadeOut(50);
		$('#reasonNoteDuplicate').fadeOut(50);
	}
}