﻿function go()
{
	var inputField = $('#inputField').val();
	if (!validate(inputField))
	{
		alert('Please enter valid Purchase Order number!');
	}
	else
	{
		getProductCode(inputField);
	}
}

function loading()
{
	$('#errors').html("");
	$('#button').html("<img src='Include/gif/ajax_loader_gray_128.gif' width='128' height='128' alt='loading...'>");
}

function getProductCode(value)
{
	var productCode;
	$.ajax({
		url: 'phpFunctions/functions.php',
		type: 'POST',
		data: {
			value: value, 
		},
		beforeSend : function(){ loading() },
		//async:false,
		complete: function(data){
		productCode = data.responseText;
		if ((productCode.indexOf("Parse error") != -1) || 
		(productCode.indexOf("Fatal error") != -1) || 
		(productCode.indexOf("Warning") != -1) || 
		(productCode.indexOf("Notice") != -1))
		{
			$('#button').html("<b><p style='font-size: 16pt; color: red;'>An error occurred! Errors description below.</p></b>");
			$('#errors').html("<pre>"+productCode+"</pre>");
			//console.log(productCode);
		} else {
			productCode = JSON.parse(productCode);
			var path = productCode.path;
			if (path != 'false') $('#button').html("<button onclick="+'"'+"window.open('newTab.php?value="+path+"')"+'"'+">View PDF</button>"); else $('#button').html("<b><p style='font-size: 16pt; color: red;'>The barcode processing failed for this product!</p></b>");;
			$('#errors').html(productCode.er+productCode.DataEr);
		}
		},
			error: function (request, status, error) {
		alert("Ajax request error on query to database.");
        console.log("Ajax request error on writing to database: "+request.responseText);
    }
	});
}

function validate(value)
{
	var pattern=/^[a-zA-Z0-9- _/]+$/;
	if (value.length<5 || value.length>35 || pattern.test(value) === false) return false; else return true;
}