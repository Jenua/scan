<?php
//Functions to generate PDF depending on store name (look function name)

//replace bad symbols in PO with good :)
function repPo($po)
{
	$bad = array("/", " ");
	$good = "_";
	$repPo = str_replace($bad, $good, $po);
	return $repPo;
}

//Generate pdf files for each PO number with separate pages for each box number with barcodes and info
function lowesPdf($arr)
{
	try{
			$PONumbers = array();
			$result = array();
			
			foreach ($arr as $row)
			{
				$PONumbers[] = $row['PONumber'];
			}
			$c = count($PONumbers);
			
			for ($i=0; $i<$c; $i++)
			{
				$pdf_path = "../Pdf/".repPo($PONumbers[$i]).".pdf";
				$pdf_path2 = "Pdf/".repPo($PONumbers[$i]).".pdf";
				
				$pdf = new FPDF('P','mm','A4');
				
				foreach ($arr as $row)
				{
					if ($row['PONumber'] == $PONumbers[$i])
					{
						$logo = '../Include/logo/logo.png';
						$UPC = 'UPC: '.$row['UPC'];
						$Path = $row['Path'].'.png';
						$PONumber = 'PO Number: '.$row['PONumber'];
						$ProductCode = 'SKU: '.$row['ProductCode'];
						
						$boxes = (integer)$row['boxes'];
						$boxNumber = (integer)$row['boxNumber'];
						if ($boxes > 1)
						{
							$text = "This box is part of a group.";
							$text1 = "This is box:";
							$BoxCarton = $boxNumber.' of '.$boxes;
						} else
						{
							$text = "This is a single box.";
							$text1 = "This is box:";
							$BoxCarton = $boxNumber.' of '.$boxes;
						}
						
						$ShipAddress_Addr1 =  'Customer: '.$row['ShipAddress_Addr1'];
						$contact = 'For questions or concerns regarding this order';
						$contact1 = 'Please contact us at 866-731-8378';
						
						$pdf->AddPage();
						$pdf->SetFont('Arial','B',24);
								
								//UPC Barcode
								$pdf->Cell(190,20,"UPC CODE",0,1,'L');
								$pdf->Image($Path, NULL, NULL, -50);
								
								//UPC Code
								$pdf->Cell(190,20,$UPC,0,1,'L');
								
								//SKU Code
								$pdf->Cell(190,20,$ProductCode,0,1,'L');
								
								//PO #
								$pdf->Cell(190,20,$PONumber,0,1,'L');
								
								//Customer
								$pdf->SetFont('Arial','B',24);
								$pdf->Cell(190,20,$ShipAddress_Addr1,0,1,'L');
								
								//text
								$pdf->SetFont('Arial','',24);
								$pdf->Cell(190,10,$text,0,1,'C');
								$pdf->Cell(190,10,$text1,0,1,'C');
								//Box Number / Carton Number
								$pdf->SetFont('Arial','B',140);
								$pdf->Cell(190,50,$BoxCarton,0,1,'C');
								
								//line
								$pdf->SetFont('Arial','B',16);
								$pdf->Cell(190,2,'_________________________________________________',0,1,'C');
								//Dreamline Logo
								$pdf->Cell(60);
								$pdf->Image($logo, NULL, NULL, -800);
								$pdf->Ln();
								
								//contact
								$pdf->SetFont('Arial','',24);
								$pdf->Cell(190,10,$contact,0,1,'C');
								$pdf->SetFont('Arial','B',24);
								$pdf->Cell(190,10,$contact1,0,1,'C');
					}
				}
				$pdf->Output($pdf_path);
				$result[] = $pdf_path2;
			}
			if (!empty($result))
			{
				return $result;
			} else
			{
				error_msg("Can not generate any pdf!");
				send_error();
			}
	}catch (Exception $e) {
		error_msg($e->getMessage());
	}
}

//Generate pdf files for each PO number with separate pages for each box number with barcodes and info
function amazonPdf($arr)
{
	try{
			$PONumbers = array();
			$result = array();
			
			foreach ($arr as $row)
			{
				$PONumbers[] = $row['PONumber'];
			}
			$c = count($PONumbers);
			
			for ($i=0; $i<$c; $i++)
			{
				$pdf_path = "../Pdf/".repPo($PONumbers[$i]).".pdf";
				$pdf_path2 = "Pdf/".repPo($PONumbers[$i]).".pdf";
				
				$pdf = new FPDF('P','mm','A4');
				
				foreach ($arr as $row)
				{
					if ($row['PONumber'] == $PONumbers[$i])
					{
						$logo = '../Include/logo/logo.png';
						$UPC = 'UPC: '.$row['UPC'];
						$Path = $row['Path'].'.png';
						$PONumber = 'PO Number: '.$row['PONumber'];
						$ProductCode = 'SKU: '.$row['ProductCode'];
						
						$boxes = (integer)$row['boxes'];
						$boxNumber = (integer)$row['boxNumber'];
						if ($boxes > 1)
						{
							$text = "This box is part of a group.";
							$text1 = "This is box:";
							$BoxCarton = $boxNumber.' of '.$boxes;
						} else
						{
							$text = "This is a single box.";
							$text1 = "This is box:";
							$BoxCarton = $boxNumber.' of '.$boxes;
						}
						$contact = 'For questions or concerns regarding this order';
						$contact1 = 'Please contact us at 866-731-8378';
						
						$pdf->AddPage();
						$pdf->SetFont('Arial','B',28);
								
								//UPC Barcode
								$pdf->Cell(190,20,"Barcode Packing Slip",0,1,'L');
								$pdf->Image($Path, NULL, NULL, -50);
								$pdf->Ln();
								//UPC Code
								$pdf->Cell(190,20,$UPC,0,1,'L');
								$pdf->Ln();
								//SKU Code
								$pdf->Cell(190,20,$ProductCode,0,1,'L');
								$pdf->Ln();
//Commenting out the extra info that Aliona and Semeon did not want on the amazon label. 
/*								//PO #
								$pdf->Cell(190,20,$PONumber,0,1,'L');
								
								//text
								$pdf->SetFont('Arial','',24);
								$pdf->Cell(190,10,$text,0,1,'C');
								$pdf->Cell(190,10,$text1,0,1,'C');
								//Box Number / Carton Number
								$pdf->SetFont('Arial','B',140);
								$pdf->Cell(190,50,$BoxCarton,0,1,'C');
								
								//line
								$pdf->SetFont('Arial','B',16);
								$pdf->Cell(190,2,'_________________________________________________',0,1,'C');
								//Dreamline Logo
								$pdf->Cell(60);
								$pdf->Image($logo, NULL, NULL, -800);
								$pdf->Ln();
								
								//contact
								$pdf->SetFont('Arial','',24);
								$pdf->Cell(190,10,$contact,0,1,'C');
								$pdf->SetFont('Arial','B',24);
								$pdf->Cell(190,10,$contact1,0,1,'C');
*/								
					}
				}
				$pdf->Output($pdf_path);
				$result[] = $pdf_path2;
			}
			if (!empty($result))
			{
				return $result;
			} else
			{
				error_msg("Can not generate any pdf!");
				send_error();
			}
	}catch (Exception $e) {
		error_msg($e->getMessage());
	}
}

//Generate pdf files for each PO number with separate pages for each box number with barcodes and info
function homePdf($arr)
{
	try{
			$PONumbers = array();
			$result = array();
			
			foreach ($arr as $row)
			{
				$PONumbers[] = $row['PONumber'];
			}
			$c = count($PONumbers);
			
			for ($i=0; $i<$c; $i++)
			{
				$pdf_path = "../Pdf/".repPo($PONumbers[$i]).".pdf";
				$pdf_path2 = "Pdf/".repPo($PONumbers[$i]).".pdf";
				
				$pdf = new FPDF('P','mm','A4');
				
				foreach ($arr as $row)
				{
					if ($row['PONumber'] == $PONumbers[$i])
					{
						$logo = '../Include/logo/logo.png';
						$PONumber = 'PO Number: '.$row['PONumber'];
						$ProductCode = 'SKU: '.$row['ProductCode'];
						
						$boxes = (integer)$row['boxes'];
						$boxNumber = (integer)$row['boxNumber'];
						if ($boxes > 1)
						{
							$text = "This box is part of a group.";
							$text1 = "This is box:";
							$BoxCarton = $boxNumber.' of '.$boxes;
						} else
						{
							$text = "This is a single box.";
							$text1 = "This is box:";
							$BoxCarton = $boxNumber.' of '.$boxes;
						}
						$contact = 'For questions or concerns regarding this order';
						$contact1 = 'Please contact us at 866-731-8378';
						
						$pdf->AddPage();
						$pdf->SetFont('Arial','B',32);
								
								//SKU Code
								$pdf->Cell(190,20,$ProductCode,0,1,'L');
								$pdf->Ln();
								//PO #
								$pdf->Cell(190,20,$PONumber,0,1,'L');
								

								//text
								$pdf->SetFont('Arial','',24);
								$pdf->Ln();
								$pdf->Cell(190,10,$text,0,1,'C');
								$pdf->Cell(190,10,$text1,0,1,'C');

								//Box Number / Carton Number
								$pdf->SetFont('Arial','B',140);
								$pdf->Cell(190,50,$BoxCarton,0,1,'C');
								
								//line
								$pdf->SetFont('Arial','B',16);
								$pdf->Cell(190,2,'_________________________________________________',0,1,'C');
								//Dreamline Logo
								$pdf->Cell(60);
								$pdf->Image($logo, NULL, NULL, -800);
								
								//contact
								$pdf->SetFont('Arial','',24);
								$pdf->Ln(2);
								$pdf->Cell(190,10,$contact,0,1,'C');
								$pdf->SetFont('Arial','B',24);
								$pdf->Cell(190,10,$contact1,0,1,'C');
					}
				}
				$pdf->Output($pdf_path);
				$result[] = $pdf_path2;
			}
			if (!empty($result))
			{
				return $result;
			} else
			{
				error_msg("Can not generate any pdf!");
				send_error();
			}
	}catch (Exception $e) {
		error_msg($e->getMessage());
	}
}

//Generate pdf files for each PO number with separate pages for each box number with barcodes and info
function cnrPdf($arr)
{
	try{
			$PONumbers = array();
			$result = array();
			
			foreach ($arr as $row)
			{
				$PONumbers[] = $row['PONumber'];
			}
			$c = count($PONumbers);
			
			for ($i=0; $i<$c; $i++)
			{
				$pdf_path = "../Pdf/".repPo($PONumbers[$i]).".pdf";
				$pdf_path2 = "Pdf/".repPo($PONumbers[$i]).".pdf";
				
				$pdf = new FPDF('P','mm','A4');
				
				foreach ($arr as $row)
				{
					if ($row['PONumber'] == $PONumbers[$i])
					{
						$logo = '../Include/logo/logo.png';
						$PONumber = 'PO Number: '.$row['PONumber'];
						$ProductCode = 'SKU: '.$row['ProductCode'];
						$boxes = (integer)$row['boxes'];
						$boxNumber = (integer)$row['boxNumber'];
						if ($boxes > 1)
						{
							$text = "This box is part of a group.";
							$text1 = "This is box:";
							$BoxCarton = $boxNumber.' of '.$boxes;
						} else
						{
							$text = "This is a single box.";
							$text1 = "This is box:";
							$BoxCarton = $boxNumber.' of '.$boxes;
						}
						$contact = 'For questions or concerns regarding this order';
						$contact1 = 'Please contact us at 866-731-8378';
						
						$pdf->AddPage();
						$pdf->SetFont('Arial','B',32);
						
								//SKU Code
								$pdf->Cell(190,20,$ProductCode,0,1,'L');
								
								//PO #
								$pdf->Cell(190,20,$PONumber,0,1,'L');
								
								//text
								$pdf->SetFont('Arial','',24);
								$pdf->Cell(190,10,$text,0,1,'C');
								$pdf->Cell(190,10,$text1,0,1,'C');
								//Box Number / Carton Number
								$pdf->SetFont('Arial','B',140);
								$pdf->Cell(190,50,$BoxCarton,0,1,'C');
								
								//line
								$pdf->SetFont('Arial','B',16);
								$pdf->Cell(190,2,'_________________________________________________',0,1,'C');
								//Dreamline Logo
								$pdf->Cell(60);
								$pdf->Image($logo, NULL, NULL, -800);
								$pdf->Ln();
								
								//contact
								$pdf->SetFont('Arial','',24);
								$pdf->Cell(190,10,$contact,0,1,'C');
								$pdf->SetFont('Arial','B',32);
								$pdf->Cell(190,10,$contact1,0,1,'C');
					}
				}
				$pdf->Output($pdf_path);
				$result[] = $pdf_path2;
			}
			if (!empty($result))
			{
				return $result;
			} else
			{
				error_msg("Can not generate any pdf!");
				send_error();
			}
	}catch (Exception $e) {
		error_msg($e->getMessage());
	}
}

//Generate pdf files for each PO number with separate pages for each box number with barcodes and info
function menardsPdf($arr)
{
/*print_r("<pre>");
print_r($arr);
print_r("</pre>");*/
	try{
			$PONumbers = array();
			$result = array();
			
			foreach ($arr as $row)
			{
				$PONumbers[] = $row['PONumber'];
			}
			$c = count($PONumbers);
			
			for ($i=0; $i<$c; $i++)
			{
				$pdf_path = "../Pdf/".repPo($PONumbers[$i]).".pdf";
				$pdf_path2 = "Pdf/".repPo($PONumbers[$i]).".pdf";
				
				$pdf = new FPDF('L','mm','A4');
				
				foreach ($arr as $row)
				{
					if ($row['PONumber'] == $PONumbers[$i])
					{
						$logo = '../Include/logo/logo.png';
						$PONumber = 'PO Number: '.$row['PONumber'];
						$ProductCode = 'SKU: '.$row['ProductCode'].'         PONumber: '.$row['PONumber'];
						$boxes = (integer)$row['boxes'];
						$boxNumber = (integer)$row['boxNumber'];
						if ($boxes > 1)
						{
							$text = "This box is part of a group. This is box: ";
							$text1 = "";
							$BoxCarton = $boxNumber.' of '.$boxes;
						} else
						{
							$text = "This package has 1 box";
//							$text1 = "This is box:";
							$BoxCarton = $boxNumber.' of '.$boxes;
						}
						$contact = 'For questions or concerns regarding this order';
						$contact1 = 'Please contact us at 866-731-8378';
						$ShipAddress_Addr1 =  'Customer: '.$row['ShipAddress_Addr1'];
						$Date = 'Order date: '.$row['Date'].'              Store# '.$row['Store'];
						$Store = 'Store# '.$row['Store'];
						$Descr = 'Product description: '.$row['Descr'];
						
						$pdf->AddPage();
						$pdf->SetFont('Arial','B',20);
								
								//SKU Code
								$pdf->Cell(190,10,$ProductCode,0,1,'L');
								$pdf->Ln();
								//PO #
//								$pdf->Cell(190,10,$PONumber,0,1,'L');
								
								//Order date
								$pdf->Cell(190,10,$Date,0,1,'L');
//								$pdf->Ln();
								
								//Store#
//								$pdf->Cell(190,10,$Store,0,1,'L');
								
								//Descr
//								$pdf->Cell(190,10,$Descr,0,1,'L');
								$pdf->MultiCell(250,10,$Descr,0,'L');
								
								//Customer
								$pdf->SetFont('Arial','B',24);
								$pdf->Cell(190,10,$ShipAddress_Addr1,0,1,'L');
								$pdf->Ln();
						//text
								$pdf->SetFont('Arial','',20);
								$pdf->Cell(275,10,$text,0,1,'C');
//								$pdf->Cell(275,10,$text1,0,1,'C');
								//Box Number / Carton Number
								$pdf->SetFont('Arial','B',140);
								$pdf->Cell(275,50,$BoxCarton,0,1,'C');
								
								//line
								$pdf->SetFont('Arial','B',16);
								$pdf->Cell(275,2,'_________________________________________________',0,1,'C');
								//Dreamline Logo
								$pdf->Cell(105);
								$pdf->Image($logo, NULL, NULL, -800);
								$pdf->Ln();
								
								//contact
								$pdf->SetFont('Arial','',20);
								$pdf->Cell(275,10,$contact,0,1,'C');
								$pdf->SetFont('Arial','B',20);
								$pdf->Cell(275,10,$contact1,0,1,'C');
					}
				}
				$pdf->Output($pdf_path);
				$result[] = $pdf_path2;
			}
			if (!empty($result))
			{
				return $result;
			} else
			{
				error_msg("Can not generate any pdf!");
				send_error();
			}
	}catch (Exception $e) {
		error_msg($e->getMessage());
	}
}
?>