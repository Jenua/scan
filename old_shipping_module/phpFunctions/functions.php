<?php
//Include files
require_once('../Include/barcode/class/BCGFontFile.php');
require_once('../Include/barcode/class/BCGcode39.barcode.php');
require_once('../Include/barcode/class/BCGColor.php');
require_once('../Include/barcode/class/BCGDrawing.php');
require_once('../Include/barcode/class/BCGpdf417.barcode2d.php');
require_once('../Include/fpdf17/fpdf.php');
require_once('functionsPDF.php');
require_once('functionsDB.php');
require_once('functionsBar.php');
require_once('functionsExtr.php');
$path = $_SERVER['DOCUMENT_ROOT'];
require_once($path.'/db_connect/connect.php');

//Global variables
$error_messages = array();//Array to save all errors
$data_error_messages = array();//Array to save all errors in input data
$connect = Database::getInstance()->dbc;
$pdf_pathes = false;//path to pdf. False while not created

//Start if have PO number
if (isset($_REQUEST['value']) && !empty($_REQUEST['value']))
	{
		main($_REQUEST['value']);
		$connect = null;
	}

//Main function to execute functions kit depending on store name
function main($PONumber)
{
	global $error_messages;
	global $data_error_messages;
	global $connect;
	global $pdf_pathes;
	
	$array = getProductCode($connect, $PONumber);
	//$array = test1();//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	$storeName = extrStore($array);
	switch ($storeName)
	{
    case "Lowe's":
		$array = boxes($array);
		$array = dublic($array);
		//$array = extrPc($array);
        $array = getUPC($connect, $array);
		$array = gen_images($array);
		$pdf_pathes = lowesPdf($array);
		send_error();
        break;
    case "Amazon":
        $array = boxes($array);
		$array = dublic($array);
		//$array = extrPc($array);
        $array = getUPC($connect, $array);
		$array = gen_imagesBCGpdf417($array);
		$pdf_pathes = amazonPdf($array);
		send_error();
        break;
    case "Home":
        $array = boxes($array);
		$array = dublic($array);
		$pdf_pathes = homePdf($array);
		send_error();
        break;
	case "C & R":
        $array = boxes($array);
		$array = dublic($array);
		//$array = extrPc($array);
		$pdf_pathes = cnrPdf($array);
		send_error();
        break;
	case "MENARDS":
        $array = boxes($array);
		$array = dublic($array);
		//$array = extrPc($array);
		$array = extrDate($array);
		$array = extrStoreNum($array);
		//$array = extrDescr($array);
		$pdf_pathes = menardsPdf($array);
		send_error();
        break;
    case "Overstock":
        $array = boxes($array);
		$array = dublic($array);
		$pdf_pathes = homePdf($array);
		send_error();
        break;	
	case "Gordian Project LLC":
        $array = boxes($array);
		$array = dublic($array);
		$pdf_pathes = homePdf($array);
		send_error();
        break;
	case "ATG Stores/Fixture Universe":
        $array = boxes($array);
		$array = dublic($array);
		$pdf_pathes = homePdf($array);
		send_error();
        break;
	case "Wayfair, LLC":
        $array = boxes($array);
		$array = dublic($array);
		$pdf_pathes = homePdf($array);
		send_error();
        break;	
	case "FaucetDirect":
        $array = boxes($array);
		$array = dublic($array);
		$pdf_pathes = homePdf($array);
		send_error();
        break;		
	default: /*defaulted everyone else to standard home depot layout*/
	
		 $array = boxes($array);
		$array = dublic($array);
		$pdf_pathes = homePdf($array);
		send_error();
        break;		
	/*		data_error_msg("CustomerRef_FullName", $storeName, "Unknown store name!");
		send_error();
        break;
		
		*/
	}
	
	return send_error();
}

//Fills an array with error messages
function error_msg($msg)
{
	global $error_messages;
	$error_messages[] = $msg;
}

//Fills an array with errors in input data
function data_error_msg($type, $id, $msg)
{
	global $data_error_messages;
	$row = array('type' => $type, 'id' => $id, 'msg' => $msg);
	$data_error_messages[] = $row;
}

//Generate list of error messages
function formErrors($error_messages)
{
	if (isset($error_messages) && !empty($error_messages))
	{
		$mes_text = "<b><p style='font-size: 12pt; color: red;'>Errors:</p></b>";
		foreach($error_messages as $message){
			$mes_text.= "<b><p style='font-size: 10pt; color: black;'>".$message."</p></b>";
		}
		return $mes_text;
	} else return false;
}

//Generate table with errors in input data
function formDataErrors($data_error_messages)
{
	if (isset($data_error_messages) && !empty($data_error_messages))
	{
		$mes_text = "<b><p style='font-size: 12pt; color: red;'>Detailed errors:</p></b><p><table border='1' style='font-size: 12pt; color: black;'>
		<tr>
			<th>type</th>
			<th>data</th>
			<th>error</th>
		</tr>";
		foreach($data_error_messages as $message){
			$mes_text.= "<tr>
			<td>".$message['type']."</td>
			<td>".$message['id']."</td>
			<td>".$message['msg']."</td>
			</tr>";
		}
		$mes_text.= "</table></p>";
		return $mes_text;
	} else return false;
}

//Returns array to ajax query with errors and pathes to pdf files and terminates script
function send_error()
{
	global $error_messages;
	global $data_error_messages;
	global $pdf_pathes;
	try
	{
		$formErrors = (string)formErrors($error_messages);
		$formDataErrors = (string)formDataErrors($data_error_messages);
		if ($pdf_pathes && isset($pdf_pathes) && !empty($pdf_pathes))
		{
			$html = (string)$pdf_pathes[0];
		} else $html = 'false';
		$result = array(
		'path' => $html,
		'er' => $formErrors,
		'DataEr' => $formDataErrors
		);
		$result = json_encode($result);
		print_r($result);
	} catch (Exception $e){
		echo $e->getMessage();
	}
	die();
}
?>