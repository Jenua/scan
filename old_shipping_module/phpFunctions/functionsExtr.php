<?php
//Count boxes for each PO number
function boxes($arr)
{
	try{
		$c = count($arr);
		for($i=0; $i<$c; $i++)
		{
			$row = $arr[$i];
			$PONumber = $row['PONumber'];
			$boxes = 0;
			foreach ($arr as $row2)
			{
				if ($row2['PONumber'] == $PONumber)
				{
					$box = $row2['quantity'];
					$boxes += $box;
				}
			}
			$row['boxes'] = $boxes;
			$arr[$i] = $row;
		}
		return $arr;
	}catch (Exception $e) {
		error_msg($e->getMessage());
	}
}

//Divide records which has quantity more than 1 to separate records and add box numbers
function dublic($arr)
{
	try{
		$PONumbers = array();
		$result = array();
		$arr2 = $arr;
		
		foreach ($arr as $row)
		{
			$PONumbers[] = $row['PONumber'];
		}
		$PONumbers = array_unique($PONumbers);
		sort($PONumbers);
		$c = count($PONumbers);
		
		for ($i=0; $i<$c; $i++)
		{
			$last = 0;
			foreach ($arr2 as $row2)
			{
				if ($row2['PONumber'] == $PONumbers[$i])
				{	
					for ($j=$last; $j<((integer)$row2['quantity']+$last); $j++)
					{
						$res = array(
						'RefNumber' => $row2['RefNumber'],
						'PONumber' => $row2['PONumber'],
						'ProductCode' => $row2['ProductCode'],
						'Descr' => $row2['Descr'],
						'CustomerRef_FullName' => $row2['CustomerRef_FullName'],
						//'ItemRef_FullName' => $row2['ItemRef_FullName'],
						'quantity' => $row['quantity'],
						'ShipAddress_Addr1' => $row['ShipAddress_Addr1'],
						'TimeCreated' => $row['TimeCreated'],
						'boxes' => $row['boxes'],
						'boxNumber' => $j+1);
						$result[] = $res;
					}
					$last = $j;
				}
			}
		}
		return $result;
	}catch (Exception $e) {
		error_msg($e->getMessage());
	}
}


//Extract product code from ItemRef_FullName field
function extrPc($arr)
{
	try{
		$c = count($arr);
		for ($i=0; $i<$c; $i++)
		{
			$row = $arr[$i];
			$value = $row['ItemRef_FullName'];
			$pieces = explode(":", $value);
			$index = count($pieces)-1;
			$ProductCode = $pieces[$index];
			if (isset($ProductCode) && !empty($ProductCode))
			{
				$row['ProductCode'] = $ProductCode;
				$arr[$i] = $row;
			} else
			{
				data_error_msg("ItemRef_FullName", $row['ItemRef_FullName'], "Can not get product code!");
				unset($arr[$i]);
			}
		}
	if (!empty($arr))
	{
		sort($arr);
		return $arr;
	} else
	{
		error_msg("Can not get any product codes!");
		send_error();
	}
	}catch (Exception $e) {
		error_msg($e->getMessage());
	}
}

//Extract description from ItemRef_FullName field
function extrDescr($arr)
{
	try{
		$c = count($arr);
		for ($i=0; $i<$c; $i++)
		{
			$row = $arr[$i];
			$value = $row['ItemRef_FullName'];
			$pieces = explode(":", $value);
			$Descr = $pieces[0];
			if (isset($Descr) && !empty($Descr))
			{
				$row['Descr'] = $Descr;
				$arr[$i] = $row;
			} else
			{
				data_error_msg("ItemRef_FullName", $row['ItemRef_FullName'], "Can not get description!");
				unset($arr[$i]);
			}
		}
		if (!empty($arr))
		{
			sort($arr);
			return $arr;
		} else
		{
			error_msg("Can not extract any descriptions!");
			send_error();
		}
	}catch (Exception $e) {
		error_msg($e->getMessage());
	}
}

//Extract store number from CustomerRef_FullName field
function extrStoreNum($arr)
{
	try{
		$c = count($arr);
		for ($i=0; $i<$c; $i++)
		{
			$row = $arr[$i];
			$value = $row['CustomerRef_FullName'];
			$len = strlen($value);
			$first = $len - 4;
			$Store = substr($value, $first, $len);
			if (isset($Store) && !empty($Store))
			{
				$row['Store'] = $Store;
				$arr[$i] = $row;
			} else
			{
				data_error_msg("CustomerRef_FullName", $row['CustomerRef_FullName'], "Can not get store number!");
				unset($arr[$i]);
			}
		}
		if (!empty($arr))
		{
			sort($arr);
			return $arr;
		} else
		{
			error_msg("Can not extract any store numbers!");
			send_error();
		}
	}catch (Exception $e) {
		error_msg($e->getMessage());
	}
}

//Extract date from TimeCreated field
function extrDate($arr)
{
	try{
		$c = count($arr);
		for ($i=0; $i<$c; $i++)
		{
			$row = $arr[$i];
			$value = $row['TimeCreated'];
			$date = new DateTime($value);
			$Date = $date->format('Y-m-d');
			if (isset($Date) && !empty($Date))
			{
				$row['Date'] = $Date;
				$arr[$i] = $row;
			} else
			{
				data_error_msg("TimeCreated", $row['TimeCreated'], "Can not get date!");
				unset($arr[$i]);
			}
		}
		if (!empty($arr))
		{
			sort($arr);
			return $arr;
		} else
		{
			error_msg("Can not extract any dates!");
			send_error();
		}
	}catch (Exception $e) {
		error_msg($e->getMessage());
	}
}

//Extract store name from CustomerRef_FullName field
function extrStore($arr)
{
	$result = false;
	foreach ($arr as $row)
	{
	$str = $row['CustomerRef_FullName'];
		if (is_in_str($str, "Lowe's") != false)
		{
			$result = "Lowe's";
		} else if (is_in_str($str, "Amazon") != false)
		{
			$result = "Amazon";
		} else if (is_in_str($str, "Home") != false)
		{
			$result = "Home";
		} else if (is_in_str($str, "C & R") != false)
		{
			$result = "C & R";
		} else if (is_in_str($str, "MENARDS") != false)
		{
			$result = "MENARDS";
		} else $result = $str;
	}
	return $result;
}

//Is substring in string
function is_in_str($str,$substr)
{
	$result = strpos ($str, $substr);
	if ($result === FALSE) return false; else return true;
}
?>