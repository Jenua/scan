<?php
//Generate barcodes for each UPC
function gen_images($arr)
{
	try{
		$img_result = array();
		$c = count($arr);
		for ($i=0; $i<$c; $i++)
		{
				$row = $arr[$i];
				$image = gen_image($row['UPC']);
				if (isset($image) && !empty($image))
				{
					$row['Path'] = $image;
					$arr[$i] = $row;
				} else
				{
					unset($arr[$i]);
					data_error_msg("UPC", $row['UPC'], "Can not generate barcode!");
				}
		}
			if (!empty($arr))
		{
			sort($arr);
			return $arr;
		} else
		{
			error_msg("Can not generate any barcodes!");
			send_error();
		}
	} catch (Exception $e){
		error_msg($e->getMessage());
	}
}

//Generate barcodes for each UPC BCGgs1128
function gen_imagesBCGpdf417($arr)
{
	try{
		$img_result = array();
		$c = count($arr);
		for ($i=0; $i<$c; $i++)
		{
				$row = $arr[$i];
				$valueForGen = "AMZN,PO:".$row['PONumber'].",UPC:".$row['UPC'].",QTY:".$row['quantity'];
				$image = gen_imageBCGpdf417($valueForGen, $row['UPC']);
				if (isset($image) && !empty($image))
				{
					$row['Path'] = $image;
					$arr[$i] = $row;
				} else
				{
					unset($arr[$i]);
					data_error_msg("UPC", $row['UPC'], "Can not generate barcode!");
				}
		}
			if (!empty($arr))
		{
			sort($arr);
			return $arr;
		} else
		{
			error_msg("Can not generate any barcodes!");
			send_error();
		}
	} catch (Exception $e){
		error_msg($e->getMessage());
	}
}

//Generate barcode for input value BCGgs1128
function gen_imageBCGpdf417($value, $UPC)
{
global $error_messages;
	try{

	// The arguments are R, G, B for color.
	$color_black = new BCGColor(0, 0, 0);
	$color_white = new BCGColor(255, 255, 255); 

	$code = new BCGpdf417();
	$code->setScale(2); // Resolution
	$code->setColumn(3);
	$code->setForegroundColor($color_black); // Color of bars
	$code->setBackgroundColor($color_white); // Color of spaces
	$code->setErrorLevel(-1);
	$code->setCompact(false);
	$code->setQuietZone(true);
	$code->parse($value); // Text

	$path = '../Images/code_'.$UPC;
	$drawing = new BCGDrawing($path.'.png', $color_white);
	$drawing->setBarcode($code);
	$drawing->draw();

	// Header that says it is an image (remove it if you save the barcode to a file)
	//header('Content-Type: image/png');

	// Draw (or save) the image into PNG format.
	$drawing->finish(BCGDrawing::IMG_FORMAT_PNG);
	return $path;
	} catch (Exception $e){
		error_msg($e->getMessage());
	}
}

//Generate barcode for input value
function gen_image($value)
{
global $error_messages;
	try{
	// Loading Font
	$font = new BCGFontFile('../Include/barcode/font/Arial.ttf', 18);

	// The arguments are R, G, B for color.
	$color_black = new BCGColor(0, 0, 0);
	$color_white = new BCGColor(255, 255, 255); 

	$code = new BCGcode39();
	$code->setScale(2); // Resolution
	$code->setThickness(30); // Thickness
	$code->setForegroundColor($color_black); // Color of bars
	$code->setBackgroundColor($color_white); // Color of spaces
	$code->setFont($font); // Font (or 0)
	//$code->parse('815324013899'); // Text
	$code->parse($value); // Text
	/* Here is the list of the arguments
	1 - Filename (empty : display on screen)
	2 - Background color */
	//$drawing = new BCGDrawing('sandeep.png', $color_white);
	$path = '../Images/code_'.$value;
	$drawing = new BCGDrawing($path.'.png', $color_white);
	$drawing->setBarcode($code);
	$drawing->draw();

	// Header that says it is an image (remove it if you save the barcode to a file)
	//header('Content-Type: image/png');

	// Draw (or save) the image into PNG format.
	$drawing->finish(BCGDrawing::IMG_FORMAT_PNG);
	return $path;
	} catch (Exception $e){
		error_msg($e->getMessage());
	}
}

//Delete all files in input directory
function deletfile($directory) 
{
global $error_messages;
	try{
		// открываем директорию (получаем дескриптор директории) 
		$dir = opendir($directory); 
	   
		// считываем содержание директории 
		while(($file = readdir($dir))) 
		{ 
			  // Если это файл и он равен удаляемому ... 
			if((is_file("$directory/$file")))
			{
				// ...удаляем его. 
				unlink("$directory/$file"); 
			}
		}
		// Закрываем дескриптор директории. 
		closedir($dir); 
	} catch (Exception $e){
		error_msg($e->getMessage());
	}
}
?>