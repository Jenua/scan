<?php
$path = $_SERVER['DOCUMENT_ROOT'];
require_once($path.'/db_connect/connect.php');
//Connection to DB
function con()
{
	$conn = Database::getInstance()->dbc;
	return $conn;
}

//Execute input query
function execQuery($connect, $query)
{
	try{
		$result = $connect->prepare($query);
		$result->execute();
	}
	catch(PDOException $e){
		error_msg($e->getMessage());
	}
	return $result;
}

//Get all data from DB
function getProductCode($connect, $PONumber)
{

	$query = "SELECT 
	shiptrack.RefNumber,   
	shiptrack.PONumber,	   
	shiptrack.CustomerRef_FullName, 
	shiptrack.ShipAddress_Addr1, 
	shiptrack.TimeCreated, 
	groupdetail.ItemGroupRef_FullName, 
	groupdetail.Prod_desc, 
	linedetail.quantity
	FROM shiptrack
		INNER JOIN groupdetail ON shiptrack.TxnID = groupdetail.IDKEY
		INNER JOIN linedetail ON shiptrack.TxnID = linedetail.IDKEY
	WHERE 
		shiptrack.ShipMethodRef_FullName not like '%cancel%'  and 
		shiptrack.ShipMethodRef_FullName not like '%void%'and 
		shiptrack.ShipMethodRef_FullName not like '%cancel%'  and 
		shiptrack.ShipMethodRef_FullName not like '%void%'  and 
		linedetail.ItemRef_FullName not like '%Price-Adjustment%'   and 
		linedetail.ItemRef_FullName not like '%Freight%'            and
		linedetail.ItemRef_FullName not like '%warranty%'           and        
		linedetail.ItemRef_FullName is not NULL                     and
		linedetail.ItemRef_FullName not like '%Subtotal%'           and
		linedetail.ItemRef_FullName not like '%IDSC-10%'            and
		linedetail.ItemRef_FullName not like '%DISCOUNT%'           and
		linedetail.ItemRef_FullName not like '%SPECIAL ORDER%'    	and 	
		linedetail.CustomField9 is not NULL							and 
		linedetail.GroupIDKEY = groupdetail.TxnLineID  				and
		shiptrack.PONumber = '".$PONumber."'";
	
	try{
		$result = execQuery($connect, $query);
		$res = array();
		while ($row = $result->fetch()) {
			$arr = array(
			'RefNumber' => $row['RefNumber'],
			'PONumber' => $row['PONumber'],
			'ProductCode' => $row['ItemGroupRef_FullName'],
			'Descr' => $row['Prod_desc'],
			'CustomerRef_FullName' => $row['CustomerRef_FullName'],
			//'ItemRef_FullName' => $row['ItemRef_FullName'],
			'quantity' => $row['quantity'],
			'ShipAddress_Addr1' => $row['ShipAddress_Addr1'],
			'TimeCreated' => $row['TimeCreated']
			);
			$res[] = $arr;
		}
		if (isset($res) && !empty($res))
		{
			return $res;
		} else{
			error_msg("No such PO in database!");
			data_error_msg("PO", $PONumber, "No such PO found!");
			send_error();
		}
	}catch (Exception $e) {
		error_msg($e->getMessage());
	}	
}

//Get UPC for each relevant product code
function getUPC($connect, $res)
{
	try{
	$c = count($res);
	for ($i=0; $i<$c; $i++)
	{
		$row2 = $res[$i];
		$ProductCode = $row2['ProductCode'];
//Sandeep (Jan-01-2014): added this code to strip the product code of the leading 'G' which represents a group.	
		if (substr($ProductCode,0,1)=='G')
		{
			$codelen = strlen($ProductCode) -1; 
			$ProductCode = substr($ProductCode, 1, $codelen);	
		}
		$query = "SELECT upc FROM UPCcode WHERE sku = '".$ProductCode."'";
		$result = execQuery($connect, $query);
		$row3 = $result->fetch();
		$UPC = $row3['upc'];
		if (isset($UPC) && !empty($UPC))
		{
			$row2['UPC'] = $UPC;
			$res[$i] = $row2;
		} else
		{
			data_error_msg("ProductCode", $row2['ProductCode'], "Can not get UPC!");
			unset($res[$i]);
		}
	}
	if (!empty($res))
		{
			sort($res);
			return $res;
		} else
		{
			error_msg("Can not get any UPC!");
			send_error();
		}
	}catch (Exception $e) {
		error_msg($e->getMessage());
	}
}

//Replacement of getProductCode function to test script
function test1()
{
	return $arr = array(
						array(
						'RefNumber' => 'ref12345',
						'PONumber' => '301799865',
						'ProductCode' => 'DLVRB-102-WG',
						'Descr' => 'SOME-DOOR',
						'CustomerRef_FullName' => "Amazon",
						'TimeCreated' => '12-12-12 12:12:12',
						'quantity' => '3',
						'ShipAddress_Addr1' => 'Stan Stetsenko'),
						);// test data
}
?>