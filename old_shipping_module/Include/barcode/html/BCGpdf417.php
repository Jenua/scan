<?php
define('IN_CB', true);
include('include/header.php');

$default_value['column'] = '-1';
$column = isset($_POST['column']) ? $_POST['column'] : $default_value['column'];
registerImageKey('column', $column);

$default_value['errorlevel'] = '-1';
$errorlevel = isset($_POST['errorlevel']) ? $_POST['errorlevel'] : $default_value['errorlevel'];
registerImageKey('errorlevel', $errorlevel);

$default_value['compact'] = '0';
$compact = isset($_POST['compact']) ? $_POST['compact'] : $default_value['compact'];
registerImageKey('compact', $compact);

$default_value['quietzone'] = '1';
$quietzone = isset($_POST['quietzone']) ? $_POST['quietzone'] : ($_SERVER['REQUEST_METHOD'] === 'POST' ? '0' : $default_value['quietzone']);
registerImageKey('quietzone', $quietzone);

registerImageKey('code', 'BCGpdf417');

$vals = array();
for($i = 0; $i <= 127; $i++) {
    $vals[] = '%' . sprintf('%02X', $i);
}
$characters = array(
    'NUL', 'SOH', 'STX', 'ETX', 'EOT', 'ENQ', 'ACK', 'BEL', 'BS', 'TAB', 'LF', 'VT', 'FF', 'CR', 'SO', 'SI', 'DLE', 'DC1', 'DC2', 'DC3', 'DC4', 'NAK', 'SYN', 'ETB', 'CAN', 'EM', 'SUB', 'ESC', 'FS', 'GS', 'RS', 'US',
    '&nbsp;', '!', '"', '#', '$', '%', '&', '\'', '(', ')', '*', '+', ',', '-', '.', '/', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', ':', ';', '<', '=', '>', '?',
    '@', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '[', '\\', ']', '^', '_',
    '`', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '{', '|', '}', '~', 'DEL'
);
?>

<ul id="specificOptions">
    <li class="option">
        <div class="title">
            <label for="column">Columns</label>
        </div>
        <div class="value">
            <?php echo getInputTextHtml('column', $column, array('type' => 'number', 'min' => -1, 'max' => 30)); ?>
        </div>
    </li>
    <li class="option">
        <div class="title">
            <label for="errorlevel">Error Level</label>
        </div>
        <div class="value">
            <?php echo getInputTextHtml('errorlevel', $errorlevel, array('type' => 'number', 'min' => -1, 'max' => 8)); ?>
        </div>
    </li>
    <li class="option">
        <div class="title">
            <label for="compact">Compact</label>
        </div>
        <div class="value">
            <?php echo getCheckboxHtml('compact', $compact, array('value' => 1)); ?>
        </div>
    </li>
    <li class="option">
        <div class="title">
            <label for="quietzone">Quiet Zone</label>
        </div>
        <div class="value">
            <?php echo getCheckboxHtml('quietzone', $quietzone, array('value' => 1)); ?>
        </div>
    </li>
</ul>

<div id="validCharacters">
    <h3>Valid Characters</h3>
    <?php $c = count($characters); for ($i = 0; $i < $c; $i++) { echo getButton($characters[$i], $vals[$i]); } ?>
</div>

<div id="explanation">
    <h3>Explanation</h3>
    <ul>
        <li>Capable of containing large amounts of data in a variety of characters.</li>
        <li>Used mainly for identification cards, inventory, and transport.</li>
        <li>Dimensions of this barcode can be set; also contains a truncated version.</li>
        <li>Your browser may not be able to write the special characters (NUL, SOH, etc.) but you can write them with the code.</li>
    </ul>
</div>

<?php
include('include/footer.php');
?>