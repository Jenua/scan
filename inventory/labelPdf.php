<?php
define("globalWidth", 102);
define("globalHeight", 159);
define("globalMargins", 10);
define("globalMainFont", 22);
define("PDF_IMAGE_SCALE_RATIO", 1);
require_once '../shipping_module/Include/tcpdf/tcpdf.php';
require_once '../Include/barcode.php';
//Generate pdf files with label

function labelPdf($sku, $lot){
    try {

        //deletfile('Pdf/', 50000);

        if (!file_exists('Pdf'))
            mkdir('Pdf', 0777, true);
        $pdf_path = "Pdf/" . $sku . '-' . $lot . ".pdf";

        if (!file_exists('tmp'))
            mkdir('tmp', 0777, true);

        genBarcode_skulot($sku, $lot);
        $barcode_src = "tmp/" . $sku . "_" . $lot . ".png";

        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $pdf->SetPrintHeader(false);
        $pdf->SetPrintFooter(false);
        $pdf->SetMargins(globalMargins, globalMargins);
        $pdf->SetAutoPageBreak(FALSE, 0);
    	$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
        // set image scale factor
        //$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
        $pdf->AddPage('L', array(globalWidth, globalHeight, 'Rotate' => -90), false, false);
        //$pdf->AddPage('P', array(200, 300), true, true);
        $pdf->SetFont('helvetica', '', globalMainFont);

        //barcode size 4x2 inches => width="384" height="193" height="200" width="664"
        $tbl = '
            <table border="0" cellpadding="0" cellspacing="0" align="center" valign="bottom">         
              <tbody>
                <tr><td height="0"></td></tr>
                <tr><td height="60">' . $sku . ' ' . $lot . '</td></tr>
                <tr><td height="40"> <hr /> </td></tr>
                <tr><td><img height="80" src="' . $barcode_src . '" /></td></tr>
               ';
        $tbl .= '</tbody></table>';
        $pdf->writeHTML($tbl, true, false, true, false, '');

        //$pdf->IncludeJS("print();");
        $pdf->Output($pdf_path, 'F');

    }
    catch (Exception $e){
       echo 'Can\'t generate pdf:'.$e->getMessage();
    }
}


?>
<?php
    if(isset($_REQUEST['action']) && $_REQUEST['action'] == 'print'
            && isset($_REQUEST['SKU']) && !empty($_REQUEST['SKU']) && isset($_REQUEST['LOT']) && !empty($_REQUEST['LOT'])) {
        labelPdf($_REQUEST['SKU'], $_REQUEST['LOT']);

    } else print_r(json_encode('Error! No params included!'));
?>
