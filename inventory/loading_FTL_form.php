<?php
/**
 * Created by PhpStorm.
 * User: Yeuvheniia Melnykova
 * Date: 3/1/2017
 * Description: This file includes in the main index.php if $_SESSION['module']='rits'
 */
global
$user,
$module,
$lType;
?>
<div class="loading module-container row" id="<?= $lType ?>">
    <div class="col-md-12" id="module-header">
        <div id="date_block" class="text-right"><span
                    class="xx-large"><?= date('m-d-y'); ?></span></div>
        <h1 class="text-left">FTL Loading</h1>
    </div>
    <div class="col-md-12" id="scanForm">
        <form method="post" action="ftl_handler.php" class="form-vertical" id="ftlForm" role="form"
              data-toggle="validator">
            <fieldset>
                <div class="form-group">
                    <div class="col-md-12 text-center">
                        <div id="userAcc"></div>
                        <input type="hidden" id="scannedByUser" name="scannedByUser"
                               value="<?= ($module == 'shipping' or $user->data()->id == 2) ? $user->data()->id : 0 ?>">
                    </div>
                    <!--old ftl begin -->

                    <div class="col-md-12" id="message">
                        <div class="popup">
                            <div class="content"></div>
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <label class="control-label" for="scannedBarcode">Scanned barcode: </label>
                        <div class="form-group">
                            <input class="main-input form-control input-lg" type="text" id="scannedBarcode"
                                   name="scannedBarcode"
                                   required>
                        </div>
                        <div class="upc-set">
                            <h2 class="upc-title">Parsed barcode:</h2>
                        </div>
                        <div class="row">
                            <div class="col-xs-3">
                                <label class="control-label" for="TRU">
                                    Truck: </label>
                            </div>
                            <div class="col-xs-9">
                                <div class="form-group">
                                    <input class="form-control input" type="text" id="TRU" name="TruckID" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-3">
                                <label class="control-label" for="PLT"> Pallet: </label>
                            </div>
                            <div class="col-xs-9">
                                <div class="form-group">
                                    <input class="form-control input" type="text" id="PLT" name="PalletID" required>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-3">
                                <label class="control-label" for="PO">PO: </label>
                            </div>
                            <div class="col-xs-9">
                                <div class="form-group">
                                    <input class="form-control input" type="text" id="PO" name="poNumber" required>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <label class="control-label" for="scanned"> UPC: </label>
                        <div class="form-group">
                            <input class="form-control input-lg"
                                   type="text"
                                   id="scanned"
                                   name="scanned"
                                   placeholder="#UPC"
                                   maxlength="12"
                                   minlength="12"
                                   pattern="^8[0-9]{11}$">
                        </div>
                        <div class="upc-set">
                            <h2 class="upc-title">UPC code(s):</h2>
                            <div class="upc-row" data-ups="ups-numbers">
                            </div>
                        </div>

                    </div>
                    <div class="form-group col-md-12">
                        <button id="pallet-submit"
                                class="btn btn-lg btn-primary btn-block disabled">
                            Confirm Pallet
                        </button>
                    </div>


                    <!--old ftl end -->




                    <input type="hidden" id="scannedSystem" name="scannedSystem" value="<?= $module ?>">
                    <input type="hidden" id="moduleType" name="moduleType" value="<?= $lType ?>">

                </div>
            </fieldset>
        </form>
    </div>
    <!--        <audio src="../Include/snd/Siren_Noise.mp3" preload="auto"></audio>-->
    <?php
    $savedData = getCodeSet(4);
    ///var_dump($savedData);
    ?>
    <div class="accordion-pallet col-md-12 hidden" id="jsAppend">
        <h2 class="accordion-title">Last saved data: </h2>
        <div class="panel-group" id="justSaved">
            <!-- filling from by justSaved js-function -->
        </div>
    </div>

    <div class="accordion-pallet col-md-12">
        <h2 class="accordion-title">Saved Data:</h2>
        <div class="panel-group" id="accordion">
            <?php foreach ($savedData as $key => $code) { ?>
                <div class="panel panel-primary">
                    <?php if (isset($code['parent'])) { ?>
                        <div class="panel-heading">
                            <a data-toggle="collapse" data-parent="#accordion" href="#panel-<?= $key ?>"
                               aria-expanded="false">
                                Barcode #<?= $code['parent'] ?>
                            </a>
                        </div>
                    <?php }
                    if (!empty($code['children'])) { ?>
                        <div id="panel-<?= $key ?>" class="panel-collapse collapse">
                            <div class="panel-body">
                                <!--                            <h2 class="upc-title">UPC code(s):</h2>-->
                                <?php foreach ($code['children'] as $child) { ?>
                                    <p class="upc-number"><?= $child->Code . ' ( ' . $child->QTY . ' )'; ?></p>
                                <?php } ?>
                            </div>
                        </div>

                    <?php } ?>
                </div>
            <?php } ?>

        </div>
    </div>

</div>
