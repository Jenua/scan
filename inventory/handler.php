<?php
require_once('../Include/init.php');
?>
<?php
global $user;
$ENTERED_DATE = date('Y-m-d');
$conn = Database::getInstance()->dbc;
$response = new stdClass();
if ($user->isLoggedIn() && $conn) {
    $userName = $user->data()->name;
    $userId = $user->data()->id;

    if (Input::get('action'))
        $action = Input::get('action');
    else if (Input::get('oper'))        //case action is the edition of the column in the jqGrid table
        $action = Input::get('oper');
    else
        $action = 'save';

    $warehouseModules = Config::get('master_modules');
    $module = (Input::get('module')) ? Input::get('module') : Session::get('module');

    $scanStatus = ($module === 'scanning')? 'Print' : ucfirst(substr($module, 0, -3));
    $table = '';
    $comment = Input::get('comment') ? $conn->quote(Input::get('comment')) : 'NULL';
    if (in_array($module, $warehouseModules)) {
        $scannedBy = Input::get('scannedBy');
        //ORDERS_PRINTED_TABLE storing the data of scanning system
        $tableName = constant('ORDERS_' . strtoupper($scanStatus) . 'ED_TABLE');

        //use test db instead of orders31 for testing
        $scanSystemsDB =  (DATABASE == "Orders_Test")? DATABASE : DATABASE31;
        $table = "[" . $scanSystemsDB . "].[dbo].[" . $tableName . "]";

        $result = false;
        $mess = '';
        switch ($action) {
            case 'edit':
                $mess = 'UPD';
                if (empty($scannedBy))
                    $scannedBy = $userId;

                // for multiedit lowcase id
                $editId = (isset($_POST['ID'])) ? $_POST['ID'] : $_POST['id'];
                    $query = "UPDATE " . $table . "
                        SET 
                            [comment] = " . $comment . "
                            ,[UserID] = " . $scannedBy . "
                        WHERE [ID]=" . $editId;
                    if ($editId > 0)
                        $result = exec_query($conn,$query);
                if ($result) {
                    $mess = ' done';
                    print_r(json_encode($mess));
                } else
                    print_r(json_encode($mess . '_error'));

                break;

            case 'save':

                $mess = 'INS';
                $PONumber = (Input::get('PO')) ? Input::get('PO') : '';
                $trigger = Input::get('trigger');
                //case scanned save
                if (!empty($PONumber)) {

                    $PO = $conn->quote(Input::get('PO'));
                    $PROnumber = '';

                    if ($module === 'loading') {
                        // in the loading system we scanning PROnumber instead of PO
                        $PROnumber = $PO;
                        $query = "
                 INSERT INTO " .  $table . "
                       ([POnumber]
                       ,[PROnumber]
                       ,[comment]
                       ,[UserID])
                 VALUES(
                        (
                        SELECT TOP 1 CASE WHEN COUNT(1) > 0 THEN [PONUMBER] ELSE NULL END AS [PONUMBER] 
                        FROM " . SHIPTRACKWITHLABEL_TABLE . "                         
                        WHERE  PRO = " . $PROnumber . " 
                        GROUP BY [PONUMBER]
                        )," . $PROnumber . "
                       ," . $comment . "
                       ," . $scannedBy . "
                       
                  )";
                    } else {

                        $query =
                            "
                 INSERT INTO " . $table . "
                       ([POnumber],[comment],[UserID])
                 VALUES
                       (" . $PO . "                                              
                       ," . $comment . "
                       ," . $scannedBy . " )";
                    }

                    if ($result = exec_query($conn, $query)) {
                        $lastScannedID = $conn->lastInsertId();
                        $mess .= ' done';
                        $historyID = 0;

                        if ($trigger === "true") {

                            if($module === 'loading' )
                                $historyID = notFoundPo($module, $lastScannedID, null);
                            else
                                $historyID = notFoundPo($module, $lastScannedID, $PONumber);
                        }


                        if( Input::get('showHistoryStatus') === "true"){
                            $response->poStatus = '?Valid';
                            $response->poInfo = 'This product order number <b>' . $PONumber . '</b> is not valid.';
                            if ($module === 'loading')
                                if ($loadResult = exec_query($conn, 'SELECT PONumber FROM ' . $table . ' WHERE ID = ' . $lastScannedID))
                                    $PONumber = $loadResult->fetchColumn();
                                else
                                    $response->poInfo = 'This PROnumber: ' . $PROnumber . ' is not tied to any POnumber.';

                            if (!empty($PONumber)) {
                                if ($statusHistory = getPOHistory($PONumber, $module)) {
                                    $statusMessage = getPOStatus($statusHistory[0], $module);
                                    $response->poInfo = $statusMessage['message'];
                                    $response->poStatus = $statusMessage['status'];
                                }
                            }

                        }
                        $response->message = $mess;
                        $response->historyID = $historyID;

                        $mess .= ' data for module: ' . $module.', historyID = '.$historyID ;

                        print_r(json_encode($response));
                    } else
                        print_r(json_encode($mess . '_error'));
                }

                break;

            case 'scanned_today':
                //jqGrid Params
                $page = ($_REQUEST['page'] < 1) ? 1 : $_REQUEST['page']; // get the requested page
                $limit = $_REQUEST['rows']; // get how many rows we want to have into the grid
                $sidx = $_REQUEST['sidx']; // get index row - i.e. user click to sort
                $sord = $_REQUEST['sord']; // get the direction

                //Filters
                $userFilter = ' UserID is not null';

                //Owner or ALL
                if(Input::get('filters') === 'byUser')
                    $userFilter =  " UserID = ". Input::get('userFilter');


                if (!$sidx) $sidx = 1;

                $totalrows = isset($_REQUEST['totalrows']) ? $_REQUEST['totalrows'] : false;
                if ($totalrows)
                    $limit = $totalrows;

                //end of jqGrid params

                //custom systems names

                $dateCreatedColumn = (ORDERS_SHIPPED_TABLE === $tableName) ? "ShipDate" : "DateCreated";

                $where = " WHERE [".$dateCreatedColumn."] > '" . $ENTERED_DATE . "' AND ".$userFilter;

                $query = "SELECT COUNT(*) as 'count' FROM " . $table . " 
                  " . $where;

                if ($res = exec_query($conn, $query)) {
                    $count = $res->fetchColumn();
                    if ($count > 0) {
                        $total_pages = ceil($count / $limit);
                    } else {
                        $total_pages = 0;
                    }
                    if ($page > $total_pages) $page = $total_pages;
                    if ($limit < 0) $limit = 0;

                    //list of usersNames
                    $usersAll = fetchAllUsers();
                    $resUser = [];
                    foreach ($usersAll as $user)
                        $resUser[$user->id] = $user->name;

                    $start = $limit * $page - $limit; // do not put $limit*($page - 1)
                    if ($start < 0) $start = 0; //[POnumber], [refnumber], [DateCreated], [comment], [UserID]
                    $SQL = "SELECT * FROM (
                              SELECT ROW_NUMBER() OVER ( ORDER BY " . $sidx . " " . $sord . ") AS RowNum,
                              * FROM ".$table."
                                " . $where . ")  as mQuery
                   
                        WHERE RowNum > " . $start . " AND RowNum <= " . $limit * $page . "
                     ";

                    $result = exec_query($conn, $SQL);

                    $response->page = $page;
                    $response->total = $total_pages;
                    $response->records = $count;
                    $i = 0;
                    $result = $result->fetchAll(PDO::FETCH_ASSOC);
                    foreach ($result as $row) {
                        $response->rows[$i] = $row;
                        if (is_null($row['UserID']) or empty($row['UserID']))
                            $response->rows[$i]['UserName'] = '';
                        else
                            $response->rows[$i]['UserName'] = isset($resUser[$row['UserID']]) ? $resUser[$row['UserID']] : '<div class="alert-warning">Absent userID in the DB:"' . DATABASE . '".</div>';

                        $i++;
                    }
                    print_r(json_encode($response));
                }
                break;

            case 'isPoCanceled':
                $PO = Input::get('PO');
                $where = " AND [PONumber] = '" . $PO . "' ";
                $query = "----------------------@Actual_status
SELECT [ShipMethodRef_FullName], [TimeModified], [RefNumber],[PONumber]
    FROM [" . DATABASE31 . "].[dbo].[salesorder]
    WHERE (
        -- [ShipMethodRef_FullName] LIKE '%BACKORDER%' OR

            [ShipMethodRef_FullName] LIKE 'CANCEL%'

            OR [ShipMethodRef_FullName] = 'CC DECLINED'
            OR [ShipMethodRef_FullName] LIKE 'ON HOLD%'
            OR [ShipMethodRef_FullName] = 'PARTIAL_SHIP'
            OR [ShipMethodRef_FullName] = 'PRODUCTION'
            OR [ShipMethodRef_FullName] = 'VOID'
)
    AND (salesorder.IsFullyInvoiced = 'false'
                OR CONVERT(DATE, salesorder.TimeModified, 110) > GETDATE() - 100)
                " . $where;
                $result = exec_query($conn, $query);

                if ($result = $result->fetchAll(PDO::FETCH_ASSOC)) {
                    print_r(json_encode($result));
                } else
                    print_r(json_encode('' . $PO . '_absent'));

                break;
        }
    } else {

        $action = (Input::get('oper') && Input::get('oper') === 'del') ? Input::get('oper') : $action;
        $search = ($action === "data_window" && Input::get('_search') === "true");

        switch ($action) {
            case 'report':
                $date_from = (Input::get('date_from')) ? Input::get('date_from') : '1900-01-01';
                $date_to = (Input::get('date_to')) ? Input::get('date_to') : $ENTERED_DATE;
                $wh = (!empty(Input::get('date_from'))) ? " [LABEL_PRINT_DATE] >= '" . $date_from . "' " : " [LABEL_PRINT_DATE] IS NOT NULL ";
                $query = "SELECT * FROM [dbo].[TRN_CREATEMEMO] 
                  WHERE " . $wh . " AND [LABEL_PRINT_DATE] <= '" . $date_to . "' ";

                if ($res = exec_query($conn, $query)) {
                    //SuperUser delete all temporary files before the report creation
                    if (file_exists('tmp'))
                        array_map('unlink', glob("tmp/*.*"));//del all files
                    else
                        mkdir('tmp', 0777, true);

                    // Record result to csv
                    $list = array();
                    $filename = 'tmp/report' . $ENTERED_DATE . '.csv';

                    // Append results to array
                    array_push($list, array("## PRINTED LABEL TABLE  " . $date_from . " - " . $date_to . " )"));
                    if ($result = $res->fetchAll(PDO::FETCH_ASSOC)) {
                        $header = array_keys($result[0]);
                        array_push($list, $header);
                        foreach ($result as $row) {
                            array_push($list, array_values($row));
                        }
                        array_push($list, array("## END OF PRINTED LABEL TABLE ##"));

                        // Output array into CSV file
                        $fp = fopen($filename, 'w');
                        header('Content-Type: text/csv');
                        header('Content-Disposition: attachment; filename="' . $filename . '"');
                        foreach ($list as $ferow) {
                            fputcsv($fp, $ferow);
                        }

                        $query = "UPDATE [dbo].[TRN_CREATEMEMO]
                              SET [REPORTED_TO_ACCT_DATE] = '" . $ENTERED_DATE . "'
                                WHERE " . $wh . " AND [LABEL_PRINT_DATE] <= '" . $date_to . "' ";

                        print_r(($res = exec_query($conn, $query)) ? $filename : json_encode('Can\'t update table'));
                    } else {
                        print_r(json_encode('no_data'));
                    }
                } else {
                    print_r(json_encode('Error of SQL.'));
                }
                break;
            case 'save':
                if (isset($_POST['SKU']) && !empty($_POST['SKU'])
                    && isset($_POST['LOT']) && !empty($_POST['LOT'])
                ) {
                    $jqGridAction = isset($_POST['oper']) ? $_POST['oper'] : 'none';
                    $MODIFIED_BY = $userName;
                    $LOT = $_POST['LOT'];
                    $SKU = $_POST['SKU'];
                    $editId = isset($_POST['id']) && !empty($_POST['id']) ? $_POST['id'] : 0;

                    $query0 = "
            SELECT [ID] FROM [dbo].[TRN_CREATEMEMO]
             WHERE [SKU] = :SKU and [LOT] = :LOT 
               ";
                    $res = $conn->prepare($query0);
                    $res->execute(array(':SKU' => $SKU, ':LOT' => $LOT));

                    if ($id = $res->fetchColumn()) {
                        //if editable SKU+LOT already exists
                        if ($editId > 0) {
                            $mess = "This label was printed before. Inventory number is: " . $id;
                            print_r(json_encode('Err:' . $mess));
                            break;
                        } else {
                            $query = "
                UPDATE [dbo].[TRN_CREATEMEMO]
                       SET [MODIFIED_DATE]   =  '" . $ENTERED_DATE . "'   
                       ,[MODIFIED_BY]        =  '" . $MODIFIED_BY . "'
                       ,[LABEL_PRINT_DATE] = '" . $ENTERED_DATE . "'
                      
                WHERE [ID] = " . $id . " ";
                            $mess = "data updated";
                        }
                    } else if ($editId > 0) {
                        $query = "UPDATE [dbo].[TRN_CREATEMEMO]
                        SET [SKU] = '" . $SKU . "' 
                        ,[LOT] = '" . $LOT . "'
                        ,[MODIFIED_DATE]='" . $ENTERED_DATE . "'
                        ,[MODIFIED_BY]='" . $userName . "'
                         WHERE [ID]=" . $editId;
                        $mess = 'The SKU and LOT are edited.';
                    } else {
                        $query = "
                 INSERT INTO [dbo].[TRN_CREATEMEMO]
                       ([SKU]
                       ,[LOT]           
                       ,[MODIFIED_DATE]
                       ,[MODIFIED_BY]
                       ,[LABEL_PRINT_DATE]
                       )
                 VALUES
                       ('" . $SKU . "'
                       ,'" . $LOT . "'
                       ,'" . $ENTERED_DATE . "'  
                       ,'" . $MODIFIED_BY . "'
                       ,'" . $ENTERED_DATE . "'
                  )";
                        $mess = 'data saved';
                    }
                    $result = exec_query($conn, $query);

                    if ($result)
                        print_r(json_encode('' . $mess));
                    else
                        print_r(json_encode('' . $mess . '_error'));
                } else
                    print_r(json_encode('The are nothing to save'));

                break;
            case 'data_window':
                //jqGrid Params
                $page = $_REQUEST['page']; // get the requested page
                $limit = $_REQUEST['rows']; // get how many rows we want to have into the grid
                $sidx = $_REQUEST['sidx']; // get index row - i.e. user click to sort
                $sord = $_REQUEST['sord']; // get the direction
                if (!$sidx) $sidx = 1;

                $totalrows = isset($_REQUEST['totalrows']) ? $_REQUEST['totalrows'] : false;
                if ($totalrows) {
                    $limit = $totalrows;
                }
                //end of jqGrid params
                $where = ($search) ? " WHERE [" . $_POST['searchField'] . "] LIKE '%" . $_POST['searchString'] . "%' "
                    : " WHERE [MODIFIED_DATE] = '" . $ENTERED_DATE . "' ";

//hide deleted rows from userRole.user
//            if ($auth->getPermission() == "user")
//                $where .= " AND [MARK] != 1 ";
                $query = "SELECT COUNT(*) as 'count' FROM [dbo].[TRN_CREATEMEMO] 
                  " . $where;

                if ($res = exec_query($conn, $query)) {
                    $count = $res->fetchColumn();
                    if ($count > 0) {
                        $total_pages = ceil($count / $limit);
                    } else {
                        $total_pages = 0;
                    }
                    if ($page > $total_pages) $page = $total_pages;
                    if ($limit < 0) $limit = 0;
                    //$start = $limit*$page - $limit; // do not put $limit*($page - 1)
                    //if ($start<0) $start = 0;
                    $SQL = "SELECT TOP " . $limit . " [ID] as [id]
                  ,[SKU]
                  ,[LOT]
                  ,[ENTERED_DATE]
                  ,[MODIFIED_DATE]
                  ,[MODIFIED_BY]
                  ,[LABEL_PRINT_DATE]
                  ,[REPORTED_TO_ACCT_DATE]
                  ,[MARK]
                   FROM [dbo].[TRN_CREATEMEMO] 
                  " . $where . " ORDER BY " . $sidx . " " . $sord;
                    $result = exec_query($conn, $SQL);
                    $response = new stdClass();
                    $response->page = $page;
                    $response->total = $total_pages;
                    $response->records = $count;
                    $i = 0;
                    $result = $result->fetchAll(PDO::FETCH_ASSOC);
                    foreach ($result as $row) {
                        $response->rows[$i] = $row;
                        $i++;
                    }
                    print_r(json_encode($response));
                }
                break;
            case 'del':
                //user can't delete the record only mark it as deleted
                if (checkPermission(1))
                    $query = "DELETE FROM [dbo].[TRN_CREATEMEMO]                        
                         WHERE [ID] in(" . $_POST['id'] . ")";
                else//if ($user->data()->permission == 'user')
                    $query = "UPDATE [dbo].[TRN_CREATEMEMO]                        
                            SET [MARK] = 1
                            ,[MODIFIED_DATE] = '" . $ENTERED_DATE . "'
                            ,[MODIFIED_BY] = '" . $userName . "'
                         WHERE [ID] in(" . $_POST['id'] . ")";
                $result = exec_query($conn, $query);

                if ($result)
                    print_r(json_encode('Inventory number(s): ' . $_POST['id'] . ' is deleted.'));
                else
                    print_r(json_encode('update_data_error'));
                break;
            case 'set_printer':

                if (isset($_POST['params']) && !empty($_POST['params'])) {
                    $params = json_decode($_POST['params']);

                    $query = "UPDATE [" . USER_TABLE . "]                         
                            SET [rits_printer] = '" . $params->printerName . "'
                            ,[rits_print_copies] = " . $params->copies . "
                            WHERE [id] = '" . $user->data()->id . "'
                         ";
                    $result = exec_query($conn, $query);
                    if ($result)
                        print_r(json_encode('update_userPrinterSettings_in_DB'));
                    else
                        print_r(json_encode('update_userPrinterSettings_error'));
                }
                break;
        }
    }
    $conn = null;
} else {
    print_r(json_encode('DB_error'));
}

function exec_query($conn, $query)
{
    try {
        $result = $conn->prepare($query);
        $result->execute();
        return $result;
    } catch (PDOException $e) {
        print_r($e->getMessage());
    }
}

?>