/**
 * Inventory form
 * */
let inventoryForm = $('#iForm'),
    connectionFailed = "No connection detected. The system maybe offline. Please close and re-open the application and try again.";

inventoryForm.validator().on('submit', function (e) {
    //hide error messages if it was
    $('.content > .alert').fadeOut(1000).scrollTop();
    if (e.isDefaultPrevented()) {
        bootstrapMess('warning','Please fill-in the required data correctly.');
    } else {

        e.preventDefault();

        let form = $(this),
            url = form.attr('action'),
            formData;

        formData = $(form).serialize();
        $.ajax({
            url: url,
            method: 'POST',
            data: formData,
            dataType: 'json',
            beforeSend: function () {
                displayModal();
                displayLoading();
            },
            complete: function (request) {
                hideLoading();
                hideModal();

                if(~(request.responseText).indexOf('Can not connect to database.')===-1){
                    bootstrapMess('danger', connectionFailed);
                    window.location.reload();
                } else if(typeof request === 'object'){
                    justSavedInventory(request.responseText);
                    form[0].reset();
                    $('.main-input').focus();
                }  else {
                    bootstrapMess('warning', 'Saving with problems.' );
                    window.location.reload();
                }

            },
            error: function (request, error) {
                bootstrapMess('warning', 'Saving failed.' );
                console.log("Ajax request error: " + request.responseText);

            }
        });
    }
});

/**
 Activating next input field in form on enter, order by tabIndex attribute
 */
inventoryForm.on('keydown', 'input', function (event) {
    if (event.which == 13) {
        event.preventDefault();
        let $this = $(event.target);
        let index = parseFloat($this.attr('tabindex'));

        $('[tabindex="' + (index + 1).toString() + '"]').focus();
    }
});
$('body').on('keydown', function (e) {
	let maxTabIndex = 4;
	if(e.keyCode == 9 && e.shiftKey == false && $('[tabindex="'+maxTabIndex+'"]').is(':focus')) {
		e.preventDefault();
		$('[tabindex="1"]').focus();
	}
});