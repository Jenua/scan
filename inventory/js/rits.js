/**
 * Created by Yeuvheniia Melnykova on 7/20/2017.
 * JS Script for Rits Module Form
 */

/*
 Form validation
 */
$('#rForm').validator().on('submit', function (e) {
    if (e.isDefaultPrevented()) {
        bootstrapMess('warning','Please fill-in the required data correctly.');
    } else {
        //hide error messages if was
        $('.popup').fadeOut(1000);
        e.preventDefault();

        let form = $(this),
            url = form.attr('action'),
            formData = {};

        formData = $(form).serialize();
        $.ajax({
            url: url,
            method: 'POST',
            data: formData,
            beforeSend: function () {
                displayModal();
                displayLoading();
            },
            complete: function (request) {
                hideLoading();
                hideModal();
                let res = request.responseText;
                if(!(~res.indexOf('data '))){
                    bootstrapMess('danger', res + ' Saving failed.' );

                } else {
                    //bootstrapMess('success', res);
                    $(form).find("#print_pdf").trigger('click');
                }

            },
            error: function (request, error) {
                console.log("Ajax request error: " + request.responseText);
            }
        });
    }
});