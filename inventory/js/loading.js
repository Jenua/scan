/**
 * Created by PhpStorm.
 * User: Vitkovskiy Sergei
 * Date: 7/20/2017
 * Description: This file includes in the main index.php if $_GET['module']='loading' type = 'FTL'
 */


$(function () {
    inputNumsOnly();
    addUPSCode();
    // accordionOpen();
    parseBarcode();
    preventSubmit();

    function isEmpty() {
        let isEmpty = true;
        let btnClose = $("[data-ups='ups-numbers'] button.close");
        let btnSubmit = $("#pallet-submit");

        btnClose.click(function () {
            if ($("[data-upc-code]").length > 1) {
                $(btnSubmit).removeClass('disabled');
            } else {
                isEmpty = false;
                $(btnSubmit).addClass('disabled');
            }
        });

        if (!$("[data-upc-code]").length) {
            isEmpty = true;
            $(btnSubmit).addClass('disabled');
        } else {
            isEmpty = false;
            $(btnSubmit).removeClass('disabled');
        }
        return isEmpty;
    }


    function inputNumsOnly() {
        $('#scanned').bind("change keyup input click", function () {
            if (this.value.match(/[^0-9]/g)) {
                this.value = this.value.replace(/[^0-9]/g, '');
            }
        });
    }


    function addUPSCode() {
        $("#scanned").focus();

        $("#scanned").keypress(function (event) {
            let upsInput = $("#scanned");
            let characterReg = /^8[0-9]{11}$/;
            let keycode = (event.keyCode ? event.keyCode : event.which);

            if (keycode == '13') {
                event.preventDefault();
                let upsCode = upsInput.val();

                let templateHtml = [
                    '<div class="alert alert-info alert-dismissible alert-inline" role="alert">',
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>',
                    '<span class="upc-number" data-upc-code="' + upsCode + '">' + upsCode + '</span>',
                    '</div>'
                ].join('\n');

                if (upsCode && characterReg.test(upsCode)) {
                    $("[data-ups='ups-numbers']").append(templateHtml);
                    $(upsInput).val("");
                    $(upsInput).attr("placeholder", "#UPS");
                    isEmpty();
                }
            }
        });
    }


    function accordionOpen() {
        let accordion = $("#accordion");
        let toggle = $("[data-toggle='collapse']");

        accordion.ready(function () {
            accordion.find("[data-toggle='collapse']").first().attr("aria-expanded", "true").removeClass("collapsed");
            accordion.find(".panel-primary > .panel-collapse.collapse").first().addClass("collapse in");
        });
    }

    /* Loading parser for Barcode (test = PLT_P1 TRU_Truck1 PO_8692563) */
    function parseBarcode() {
        let lParams = {};
        $(document).on('input', '#scannedBarcode', function () {

            let pattern = /(PLT|PO|TRU)_([\w-_]+)/gi,
                scanned = $(this).val(),
                result = '';

            while (result = pattern.exec(scanned)) {

                lParams[result[1]] = result[2];
                $('#' + result[1] + '').val(result[2]).change();
            }
            //console.log(addParams.PLT);
        });
    }



    function preventSubmit() {
        let input = $("#scannedBarcode");
        input.keypress(function (event) {
            let keycode = (event.keyCode ? event.keyCode : event.which);

            if (keycode == '13') {
                $("#scanned").focus();
                event.preventDefault();
            }
        });
    }

    $("body").keypress(function (event) {
        let keycode = (event.keyCode ? event.keyCode : event.which);

        if (keycode == '13') {
            hideAlert();
        }
    });

    $("#pallet-submit").on("click", ()=> {
        hideAlert();
    });

});