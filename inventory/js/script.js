/**
 * Created by Melnykova on 12/13/2016.
 * Scanning systems
 */
$(function () {
    let userID = $('body').find('#userID').val(),
        superUser = (userID == 2),//permissions value
        selectedUserID = (superUser) ? userID : 0,
        connectionFailed = "No connection detected. The system maybe offline. Please close and re-open the application and try again.";

    let mainDiv, module, scannedBy, scanForm, dataWindow,
        PO = $('#PO'),
        comment = $('#comment'),
        savedMess = '';
    mainDiv = $('.module-container');
    module = $('#scannedSystem').val();
    scannedBy = $('#scannedByUser').val(selectedUserID);
    dataWindow = $(".jqGridScannedOrders");
    scanForm = $('#scanForm');


    //focus on main scanning field
    $('.main-input')
        .focus()
        //off the history of the input field as valueless
        .attr("autocomplete", "off");

    /**
     *  Scanning user popup dialog
     */
    let usersDialog;
    //Define the UserList Dialog window
    //noinspection JSUnresolvedFunction
    usersDialog = $("#module_accounts").dialog({
        //Select your name to personalize of scanning process.
        title: "Select your name to start scanning",
        autoOpen: false,
        width: 775,
        modal: true,
        position: {my: "center top", at: "center top", of: scanForm},
        //prevent to close the dialog
        closeOnEscape: (superUser),
        open: function (event, ui) {
            if (!superUser)
                $(".ui-dialog-titlebar-close", ui.dialog | ui).hide();
            selectedUserID = 0;
        }
    });
    //OFF the UserList popup by default for DreamLineAdmin
    if (!superUser)
        usersDialog.dialog("open");
    //OFF the User Selection for shipping SS(End-of-Day)
    if (module === 'shipping') {
        selectedUserID = userID;
    }


    //Button for UserPopup in the navigation panel
    let accPopupBtn = $("#module_accounts_dialog");
    accPopupBtn.button().on("click", function () {
        usersDialog.dialog("open");
    });
    accPopupBtn.removeClass('ui-button');
    //User selection
    usersDialog.find('button').on("click", function (event) {
        event.preventDefault();

        let formData = $(this);
        selectedUserID = formData[0].value;

        scannedBy.val(selectedUserID).change();
        $('#userAcc').text(formData[0].name).change();
        usersDialog.dialog("close");
        $('.main-input').focus();
    });
    // End of UserList Popup
    /**
     * Error effects, animation
     * @type {*}
     */
    let audio = scanForm.children('audio'),
        audioElement = audio.get(0);

    // Do action until
    mainDiv.on('keypress click', function () {

        if (typeof audioElement != 'undefined' && audioElement.currentTime > 0) {
            audioElement.pause();
            stopEffect();
            //if($('.popup').css('display') == 'block')
            $('.popup').effect('blind', 800);
            audioElement.currentTime = 0;
        }
    });


    $('#sForm').validator().on('submit', function (e) {

        if (e.isDefaultPrevented() || selectedUserID === 0) {

            bootstrapMess('danger', 'Please enter data before submitting.');
            //Error sound+highlight effect
            audioElement.play();
            runEffect('highlight');//puff

            return false;
        }
        else {
            e.preventDefault();
            e.stopPropagation();
            //in progress TIMER = 0 START
            // bootstrapMess('info', savedMess);

            let form = $(this),
                PONumber = PO.val(),
                showPOMessage = $('#poHistoryShow').prop('checked');

            showProgressbar(PONumber);

            $.ajax({
                url: form.attr('action'),
                method: 'POST',
                data: {
                    action: 'save',
                    module: module,
                    PO: PONumber,
                    comment: comment.val(),
                    scannedBy: selectedUserID,
                    trigger: $('#poNotFoundInfo').prop('checked'),
                    showHistoryStatus: showPOMessage
                },
                dataType: 'json',
                beforeSend: function () {
                    PO.val('');
                },
                complete: function (data) {
                    let res = data.responseJSON;

                    hideProgressbar(PONumber);

                    if (typeof res == 'undefined'){
                        bootstrapMess('warning', PONumber + ' - saving failed. ');

                        window.location.reload();
                    } else if (~(res['message'].indexOf('done') < -1)) {

                        savedMess = (module != 'loading') ? "Scanned code <b>" + PONumber + " </b> saved." :
                        "Scanned PRO number <b>" + PONumber + " </b> saved.";
                        savedMess += " <br> Expand the collapsed 'Scanned today' table to see changes.";

                        hideAlert();
                        if (showPOMessage === true)
                            if (~(res['poStatus']).indexOf('?'))
                                savedMess += '<br />'+ res['poInfo'];
                        //bootstrapMess('info', res['poInfo']);
                        bootstrapMess('success', savedMess);
                        $('.main-input').focus();
                    }

                    if (dataWindow.getGridParam("gridstate") == "visible")
                        $(".ui-jqgrid-titlebar-close", dataWindow[0].grid.cDiv).trigger("click");

                },
                error: function (request, error) {
                    console.log("Ajax request error: " + request.responseText);
                }
            });

        }
        hideLoading();
        hideModal();
        return false;  // Stop form submit
    });

    /* Show Progressbar */
    function showProgressbar(PONumber = 0) {
        let progressBars = ("#progress-holder");
        let html = `<div class="progress" data-ponumber=${PONumber}>
                        <div class="progress-bar progress-bar-striped active"
                             role="progressbar" aria-valuenow="45" aria-valuemin="0"
                             aria-valuemax="100" style="width: 100%">
                             <span>Scanned code: <strong>${PONumber}</strong> is saving... </span>
                         </div>
                      </div> `;
        $(progressBars).append(html);
    }


    /* Hide Progressbar */
    function hideProgressbar(PONumber = 0) {
        let progressBar = `[data-ponumber=${PONumber}]`;
        $(progressBar).fadeOut();
    }

    /* Loading FTL */

    $('#ftlForm').validator().on('submit', function (e) {
        let upcs = $('.upc-row').find('span.upc-number');

        if (e.isDefaultPrevented() || selectedUserID === 0 || upcs.length === 0) {

            bootstrapMess('danger', 'Please enter data before submitting.');
            //Error sound+highlight effect
            //audioElement.play();
            //runEffect('highlight');//puff

            return false;
        }
        else {
            e.preventDefault();
            e.stopPropagation();

            let form, url, barcode, moduleType, parentCode,PONumber,Truck;
            form = $(this);
            url = form.attr('action');
            moduleType = $('#moduleType').val();
            parentCode = $('#PLT').val(),
            PONumber = $('#PO').val(),
                Truck = $('#TRU').val(),
            barcode = $('#scannedBarcode');

            // Fill array by upc codes
            let countsUPC = {};
            jQuery.each(upcs, function() {
                let upc = $(this).data('upc-code');
                if (!countsUPC.hasOwnProperty(upc)) {
                    countsUPC[upc] = 1;
                } else {
                    countsUPC[upc]++;
                }
            });

            // Message after success saving
            let savedMess = "Scanned Barcode <b>" + barcode.val() + " </b> saved.";

            $.ajax({
                url: url,
                method: 'POST',
                data: {
                    action: 'save',
                    module: module, moduleType: moduleType,
                    PO: PONumber,
                    Truck: Truck,
                    parent: parentCode,
                    saveSet: countsUPC,
                    scannedBy: selectedUserID
                },
                dataType:'json',

                beforeSend: function () {
                    displayModal();
                    displayLoading();
                },
                complete: function (response) {
                    hideLoading();
                    hideModal();

                     if (typeof response === 'object') {

                        justSaved(response.responseText);
                        bootstrapMess('success', savedMess);
                     } else{
                         bootstrapMess('warning', connectionFailed);
                         window.location.reload();
                     }
                    form[0].reset();
                    $('.upc-row').html('');
                },
                error: function (request, error) {
                    console.log("Ajax request error: " + request.responseText);
                }
            });

        }
        return false;  // Stop form submit
    });
    /* Scanning modules system end */

    /**
     * Inventory_activity
     * Saved data table
     */
   // $(document).ready(function () {
    if(module === 'inventory_activity'){

        let transType = $('#mType').val(),
            url = $('form').attr('action');
        if(typeof transType != 'undefined')
            $.getJSON(url+'?action=data&transTypeID='+transType,
            function (json) {
                let tr;
                for (let i = 0; i < json.length; i++) {
                    tr = $('<tr/>');
                    tr.append("<td>" + json[i].BIN + "</td>");
                    tr.append("<td>" + json[i].SKU + "</td>");
                    tr.append("<td>" + json[i].PLT + "</td>");
                    tr.append("<td>" + json[i].QTY + "</td>");
                    tr.append("<td>" + json[i].DateCreated + "</td>");
                    $('table').append(tr);
                }
            });
    }
});

// Set adaptive width
$(window).on("resize", function () {
    let $grid = $('[class^="jqGrid"]'),
        newWidth = $grid.closest(".ui-jqgrid").parent().width(),
        userPopup = $("#module_accounts");
    $grid.jqGrid("setGridWidth", newWidth, true);
    // Setter for dialog window
    userPopup.dialog("option", "width", newWidth);
    if (newWidth >= 479) {
        userPopup.find("button").css({width: '100%'});
    }
});
// Set adaptive UserList popup
// $(window).on("resize", function () {
//     var $container = $('.module-container'),
//         newWidth = $container.closest("div").parent().width() + 30;
//     console.log($container.closest("div").parent().width());
//     // Setter for dialog window
//     $( "#module_accounts" ).dialog( "option", "width", newWidth );
// });

/**
 * Fill div#justSaved in the result table
 $.getJSON('/inventory/inventory_handler.php', function (json) {
*/
function justSavedInventory(jsonResponse){
   let row = jQuery.parseJSON(jsonResponse),
        tr;
    tr = '<td>'+ row.BIN + '</td>' +
        '<td>'+row.SKU + '</td>' +
        '<td>'+row.PLT + '</td>' +
        '<td>'+row.QTY + '</td>'+
    '<td>'+row.DateCreated + '</td>';

    $('table').prepend('<tr class="success">' + tr + '</tr>');

}
function justSaved(jsonResponse) {
    let accDiv = $('#justSaved'),
        resDiv = $('#jsAppend'),
        bodyAcc = '',
        children = [];

    let json = jQuery.parseJSON(jsonResponse);

    if (resDiv.hasClass('hidden'))
        resDiv.removeClass('hidden');

    jQuery.each(json.rows, function (i, row) {
        if (i === 'parent')
            bodyAcc =
                '<div class="panel-heading">' +
                '<a data-toggle="collapse" data-parent="#accordion" href="#panel-' + row.id + '" aria-expanded="false">Barcode # ' + json.barcode + '' +
                '</a></div>' +
                '<div id="panel-' + row.id + '" class="panel-collapse collapse">' +
                '<div class="panel-body">';

        if (i == 'children')

            jQuery.each(row, function (index, cell) {
                children[index] = '<p class="upc-number" id="' + cell.id + '">' + cell.Code  +' ( '+  cell.QTY + ' ) </p>';
            });


    });
    bodyAcc += children.join('\n');

    accDiv.prepend('<div class="panel panel-primary">' + bodyAcc + '</div></div></div>');

}