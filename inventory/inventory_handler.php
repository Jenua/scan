<?php
/**
 * Created by PhpStorm.
 * User: Yeuvheniia Melnykova
 * Date: 7/7/2017
 * Time: 4:37 PM
 */
require_once ('../Include/init.php');?>
<?php
global $user;
$ENTERED_DATE = date('Y-m-d h:i:s');

if ($user->isLoggedIn()) {

    $userId = $user->data()->id;
    $module = (Input::get('module')) ? Input::get('module') : Session::get('module');
    $transTypeID = intval(Input::get('transTypeID'));
    $inventoryModules = array('inventory_activity');

    $response = new stdClass();
    $conn = Database::getInstance()->dbc;
    $action = isset($_REQUEST['action']) ? $_REQUEST['action'] : 'save';


    if (in_array($module, $inventoryModules)) {
        $table = ($module == 'inventory_activity')? ORDERS_INVENTORY : '';

        switch ($action) {
            case 'save':
                $mess = 'Unable to save';

                if (empty($table)) break;
                $ENTERED_DATE = date('Y-m-d h:i:s');
                $sku = Input::get('SKU') ? Input::get('SKU') : 'NULL';
                $bin = Input::get('BIN') ? Input::get('BIN') : 'NULL';
                //[dbo].[BINWarehouse] replase by name, CustomField7
                $sku_preordained = ($sku === 'NULL' && $bin != 'NULL')?
                    ' (SELECT TOP 1 [name] FROM ['.DATABASE31.'].[dbo].['.ITEMINVENTORY.'] WHERE CustomField7='.$bin.') '
                    : $conn->quote($sku);

                $po  = Input::get('PO')  ? Input::get('PO') : 'NULL';
                $qty = intval(Input::get('QTY'));

                $plt = Input::get('PLT') ? intval(Input::get('PLT')) : 'NULL';
                //negative quantity can be only for RPL-
                $qty = ($transTypeID != 4 && $qty < 0)? abs($qty): $qty;

                //case scanned save
                //{action: "save", module: "iventory_activity", SKU: "", BIN: "", PO: ""}

                if ((Input::get('SKU') OR Input::get('BIN')) AND $transTypeID > 0 ) {

                    $query = "
                 INSERT INTO [" . DATABASE31 . "].[dbo].[" . $table . "]
                       ([SKU],[BIN],
                       --[POnumber],
                       [QTY],[TransTypeID]
                       ,[Pallet]
                       ,[UserID])
                 VALUES
                       (" . $sku_preordained . ", " . $conn->quote($bin) . "
                       --," . $conn->quote($po) . "
                       ,".$qty.", ".$transTypeID.",".$plt.", " . $userId . "
                  )";
                    $response->SKU = $sku;
                    $response->QTY = $qty;
                    $response->PLT = $plt;
                    $response->BIN = $bin;
                    $response->DateCreated = $ENTERED_DATE;

                $result = exec_query($conn, $query);

                if ($result)
                    print_r(json_encode($response));
                } else
                    print_r(json_encode($mess));

                break;
            case 'data':
                $i = 0;
                $q = 'SELECT TOP 100 [ID]
      ,[TransTypeID],[UserID]
      ,[BIN]
      ,[SKU]
      ,[Pallet] as PLT
      ,[QTY]
      ,CAST(DateCreated AS datetime2(0)) as DateCreated 
  FROM '.DATABASE31.'.dbo.'.$table
                    .' WHERE TransTypeID ='.$transTypeID
                .' ORDER BY DateCreated DESC';
                $result = exec_query($conn,$q);
                if($response = $result->fetchAll(PDO::FETCH_ASSOC))
                    print_r(json_encode($response));
                else
                    print_r(json_encode('result_null'));

                break;
            default:
                print_r(json_encode('action_null'));
                break;
        }
    }
} else {
    print_r(json_encode('no_auth'));
}

function exec_query($conn, $query)
{
    try {
        $result = $conn->prepare($query);
        $result->execute();
        return $result;
    } catch (PDOException $e) {
        print_r($e->getMessage());
    }
}
