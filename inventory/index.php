<?php require_once "../Include/init.php"; ?>
<?php
global $user,
       $us_url_root;
$master_modules = Config::get('master_modules');
$userSettings = getUserSettings();
//Session::flash('module');
if (Input::get('module'))
    Session::put('module', Input::get('module'));
else
    Session::put('module', '');
//loading ftl and ltl types
$lType = (Input::get('type')) ? Input::get('type') : '';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <!-- To ensure proper rendering and touch zooming for all devices -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php if (isset($expiryTime) && $expiryTime > 0) { ?>
        <meta http-equiv="refresh" content="<?= $expiryTime ?>">
    <?php } ?>
    <title><?php
        echo (Session::get('module')) ? module_title(Session::get('module'),$lType) : page_title(); ?></title>
    <!-- FORM: HEAD SECTION -->
    <link rel="stylesheet" href="../Include/frameworks/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../Include/frameworks/bootstrap/css/bootstrap-theme.min.css">

    <script src="../Include/frameworks/jquery/2.2.4/jquery.min.js"></script>
    <script type="text/javascript" src="../Include/frameworks/jquery/moment.min.js"></script>
    <!-- Custom Fonts/Animation/Styling-->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"
          integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

    <script src="../Include/frameworks/bootstrap/js/bootstrap.min.js"></script>
    <script src="../Include/frameworks/bootstrap-validator-master/js/validator.js"></script>
    <!--End of new-order styles end-->
    <!-- FORM: BODY SECTION -->
    <?php
    if ($user->isLoggedIn() && Session::get('module') == 'rits') { ?>

        <!-- QZ Print settings -->
        <script>
            let rits_printer = '<?=$user->data()->rits_printer ?>',
                rits_print_copies = '<?=$user->data()->rits_print_copies ?>';
        </script>
        <script type="text/javascript" src="../shipping_module/Include/qz/js/dependencies/rsvp-3.1.0.min.js"></script>
        <script type="text/javascript" src="../shipping_module/Include/qz/js/dependencies/sha-256.min.js"></script>
        <script type="text/javascript" src="../shipping_module/Include/qz/js/qz-tray.js"></script>
        <script type="text/javascript" src="../shipping_module/js/print.js"></script>
        <script type="text/javascript" src="../Include/js/rits.js"></script>
        <!-- QZ end-->
    <?php } ?>
    <script type="text/javascript" src="js/script.js"></script>
    <!-- jqGridFree -->
    <link rel="stylesheet" href="../Include/frameworks/jquery-ui-1.12.1.custom/themes/smoothness/jquery-ui.min.css">
    <link rel="stylesheet" href="../Include/frameworks/free-jqgrid/css/ui.jqgrid.min.css">

    <script src="../Include/frameworks/free-jqgrid/js/jquery.jqgrid.min.js"></script>
    <script src="js/jqGrid.js"></script>

    <script src="../Include/frameworks/jquery-ui-1.12.1.custom/jquery-ui.min.js"></script>
    <link rel="stylesheet" href="../Include/frameworks/jquery-ui-1.12.1.custom/jquery-ui.min.css">
    <link rel="stylesheet" href="../Include/frameworks/jquery-ui-1.12.1.custom/jquery-ui.structure.min.css">
    <link rel="stylesheet" href="../Include/frameworks/jquery-ui-1.12.1.custom/jquery-ui.theme.min.css">
    <!-- Include Date Range Picker -->
    <script type="text/javascript" src="../Include/frameworks/bootstrap.daterangepicker/daterangepicker.js"></script>
    <link rel="stylesheet" type="text/css" href="../Include/frameworks/bootstrap.daterangepicker/daterangepicker.css"/>
    <link rel="stylesheet" type="text/css" href="../Include/css/style.css"/>
    <?php
    $module = Input::get('module');
    if (isset($module) && $module != '')
        //custom scripts for any module with the same name(sample: module_name script: module_name.js)
        if (file_exists('css/' . $module . '.css'))
            echo '<link rel="stylesheet" type="text/css" href="css/' . $module . '.css" />';?>
    <script src="../Include/js/script.js"></script>
</head>
<body>
<div class="container">
    <?php if ((!$user->isLoggedIn())){
        Redirect::to('../Include/login.php');
    } else {
    Redirect::to();
    $inv_url = $us_url_root . "?module=";
    $module = Session::get('module');
    ?>

<?php // navigation menu
include "../Include/navigation.php";
//var_dump(getPOHistory('53581492'));
?>
    <div class="row">
        <div id="warehouse" class="col-xs-12 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2">
            <?php
            if (!empty($module)) {
                $module_title = module_title($module);
                $scanning_modules = ($user_modules) ? array_diff($user_modules, ['inventory_activity', 'rits']) : false;

                if (in_array($module, $user_modules)) {//user has access to the module
                    if (in_array($module, $scanning_modules) and $lType != 'FTL') { ?>
                        <div class="<?= $module ?> module-container row">
                        <div id="module-header" class="col-md-12">
                            <div id="date_block" class="text-right">
                                <span class="xx-large"> <?php echo date('m-d-y'); ?></span></div>
                            <h1><?= $module_title ?> </h1>
                        </div>

                        <div class="col-md-12" id="scanForm">

                            <form method="post" action="handler.php" class="form-vertical" id="sForm"
                                  data-toggle="validator">
                                <fieldset>
                                    <div class="form-group">
                                        <div class="col-md-12 text-center">
                                            <div id="userAcc"></div>
                                            <input type="hidden" id="scannedByUser" name="scannedByUser"
                                                   value="<?= ($module == 'shipping' or $user->data()->id == 2) ? $user->data()->id : 0 ?>">
                                        </div>
                                        <label class="col-md-12 control-label" for="PO">
                                            <?= ($module == 'loading') ? 'PRO ' : 'PO '; ?>
                                            Number: </label>
                                        <div class="col-md-12" id="message">
                                            <div class="popup">
                                                <div class="content"></div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div id="progress-holder" class="progress-bar-scanned"> </div>
                                        </div>
                                        <div class="col-md-12">
                                            <input class="form-control input-lg main-input" type="text" id="PO"
                                                   name="poNumber"
                                                   required>

                                            <div class="hidden">
                                                <?php
                                                foreach($userSettings as $settings) {
                                                    $checkedTrigger = ($settings->value === '1')? 'checked':'';
                                                    if($settings->name === 'ON_ORDERS_SCAN_HISTORY'){ ?>
                                                        <input type="checkbox" id="poNotFoundInfo" name="trigger" value="on" <?= $checkedTrigger ?> />
                                                        <label class="control-label" for="poNotFoundInfo" about="Save PO into NotFoundPo table">
                                                            Set the scanned value as not founded if it does not exist in logistics hub </label>
                                                   <?php } if($settings->name === 'ON_ORDERS_HISTORY_MESSAGES'){ ?>
                                                <input type="checkbox" id="poHistoryShow" name="showHistoryStatus" value="on" <?= $checkedTrigger ?> />
                                                <label class="control-label" for="poHistoryShow" about="Show messages about PONumber history scan process">
                                                </label>

                                                    <?php } ?>
                                                <?php } ?>
                                            </div>

                                        </div>
                                        <div class="col-md-12">
                                            <button type="submit" class="btn btn-lg btn-primary btn-block">Submit
                                            </button>
                                        </div>
                                        <input type="hidden" id="scannedSystem" name="scannedSystem"
                                               value="<?= $module ?>">

                                        <input type="hidden" id="comment" name="comment">
                                    </div>
                                </fieldset>
                            </form>
                            <audio id="xyz" src="../Include/snd/Siren_Noise.mp3" preload="auto"></audio>
                        </div>

                        <div class="col-md-12">
                            <table class="jqGridScannedOrders" id="jqGridScanedOrders"></table>
                            <div class="pagernavScannedOrders" id="pagernav"></div>
                        </div>
                        <?php
                    } else if (in_array($module, ['loading', 'rits', 'inventory_activity'])) {

                        $fName = (isset($lType) && $lType === 'FTL') ? $module . '_' . $lType : $module;
                        $fName .= '_form.php';

                        if (file_exists($fName)) {
                            // module's forms
                            include_once $fName . '';
                        }
                    }
                }
                ?>
                <input type="hidden" id="userID" name="userID" value="<?= $user->data()->id ?>">
                </div> <!-- module-container end of -->
                <?php
            } else { ?>
                <div class="scanning-modules">
                    <h2 class="gradient">Order Scanning Systems</h2>
                    <form id="sform" name="inventorySelect" method="post" action="index.php">
                        <ul class="system-btn">
                            <?php
                            foreach ($master_modules as $module_name) {
                                $moduleTitle = module_title($module_name,$lType);
                                if (in_array($module_name, $user_modules)) { ?>
                                    <li class="col-md-5" id="<?= $module_name ?>" title="<?= $module_name ?>">
                                        <a id="<?= $module_name . "_link" ?>"
                                           href="<?= $inv_url . $module_name ?>"><?= $moduleTitle ?></a>
                                    </li>
                                <?php } else { ?>
                                    <li class="disabled col-md-5" id="<?= $module_name ?>">
                                        <a href=""><?= $moduleTitle ?></a>
                                    </li>
                                <?php }

                            } ?>
                            <?php //module_type: loading_ftl
                                $href = (in_array('loading', $user_modules))? $inv_url.'loading&type=FTL': '';
                                ?>
                                <li class="<?= !empty($href)? '': 'disabled '; ?>col-md-5" id="FTL"><a id="loading_ftl_link" href="<?= $href ?>"> FTL
                                        Loading</a></li>

                        </ul>
                    </form>
                </div>
                <?php //inventory activity menu if user has an access to the inventory_activity module
                if (in_array('inventory_activity', $user_modules)) {
                    ?>
                    <?php
                    $transTypes = fetchAllTransTypes();
                    $inv_url .= 'inventory_activity';
                    ?>
                    <div id="inventory_activity">
                        <h2 class="gradient"> Inventory Activity Systems</h2>
                        <ul class="system-btn">
                            <?php foreach ($transTypes as $id => $type) { ?>
                                <li class="col-md-5" id="<?= $type['abr'] ?>" title="<?= $type['desc'] ?>">
                                    <a href="<?= $inv_url . '&mType=' . $id ?>"><?= $type['title'] ?></a></li>
                                <?php
                            } ?>
                        </ul>
                    </div>

                <?php } ?>
            <?php }?>


            <?php
            }
            ?>
        </div>
        <div class="col-lg-2 col-md-1"></div>
    </div>

    <div class="darkBack"></div>
    <div class="page-loading"><img alt="loading..." src="../new_order/Include/img/294.GIF"></div>

    <?php include_once '../Include/footer.php' ?>
