<?php
/**
 * Created by PhpStorm.
 * User: Yeuvheniia Melnykova
 * Date: 7/26/2017
 * Time: 2:37 PM
 */
require_once ('../Include/init.php');?>
<?php
global $user;

if ($user->isLoggedIn()) {

    $userId = $user->data()->id;
    $module = (Input::get('module')) ? Input::get('module') : '';
    $type = (Input::get('moduleType')) ? Input::get('moduleType') : '';
    $response = new stdClass();

    $db = Database::getInstance();
    $action = isset($_REQUEST['action']) ? $_REQUEST['action'] : 'save';


    if ($module === 'loading' && $type= 'FTL' ) {
        $table = ($type === 'FTL')?  ORDERS_LOADED_FULL_TABLE : ORDERS_LOADED_TABLE ;

        switch ($action) {
            case 'save':

                $mess = 'Unable to save';

                $PO = Input::get('PO') ? Input::get('PO') : false;
                $Truck = Input::get('Truck') ? Input::get('Truck') : false;

                $parent = Input::get('parent') ? Input::get('parent') : false;
                $savedSet = !empty($_POST['saveSet']) && is_array($_POST['saveSet']) ? $_POST['saveSet'] : false;
                $scannedBy = Input::get('scannedBy')? Input::get('scannedBy') : false;

                if (empty($table) or !$scannedBy or !($parent && $savedSet))
                    break;

                $res = addCodeSet($parent,$savedSet,4);
                $upcQTY = count($savedSet) - 1;

                if (count($res)>0) {
                    $i=0;
                    foreach ($res as $id => $row){
                        if ($row['ParentID'] == 0){
                            $response->rows['parent'] = array( 'id'=>$id ,'Code'=> $row['Code'],'ParentID'=>$row['ParentID']);
                            $lastID = $id;
                        }

                        else
                            $response->rows['children'][$i] = array( 'id'=>$id ,'Code'=> $row['Code'], 'ParentID'=>$row['ParentID']
                            , 'QTY'=>$row['QTY']);
                        $i++;
                    }

                    $response->mess = 'Pallet: #' . $parent . ' with UPc(s)_saved';

                }
                if (isset($lastID) && $lastID>0){
                    //insert into ftl_loading
                    $query = "INSERT INTO " . $table . "
                       ([PalletUPCID],[POnumber],[Truck],[Pallet],[UserID])
                 VALUES
                       (" . $lastID .", '" . $PO . "', '" . $Truck . "','" . $parent . "'
                       ," . $scannedBy . " )";
                    $res = $db->query($query);
                    if (!$res)
                        $response->mess = 'Orders loaded full insert failed.';
                    else
                        $response->barcode = 'PO #'.$PO.' Truck #'.$Truck.' Pallet #'.$parent;
                }

                print_r(json_encode($response));
                break;

            default:
                print_r(json_encode('action_null'));
                break;
        }
    }
} else {
    print_r(json_encode('no_auth'));
}
