<?php
/**
 * Created by PhpStorm.
 * User: Yeuvheniia Melnykova
 * Date: 7/7/2017
 * Description: This file includes in the main index.php if $_GET['module']='inventory_activity'
 */
global $module,
       $transTypes,
       $abs_us_root;

//TransType of Inventory such as module type
$transType = (Input::get('mType') && isset($transTypes[Input::get('mType')]))? intval(Input::get('mType')): 1;

$backPath = isset($transTypes[$transType]['abr'])? '/Include/img/'.$transTypes[$transType]['abr'].'128.png': '';
$backFile = file_exists($abs_us_root.$backPath);
$typeBack = $backFile ? "background: url('".$backPath."') left center/100px auto  no-repeat;": "";

?>
<div class="module-container <?= $module ?>">
    <div class="col-xs-12 text-right inventory-title" style="<?=$typeBack?>">
        <h1>Inventory Activity</h1>
        <h2>
            <?php echo (isset($transType) and key_exists($transType,$transTypes)) ? $transTypes[$transType]['title'] : "Undefined Type"; ?>
        </h2>
        <h3> Date: <?php echo date('m-d-y'); ?></h3>
    </div>

    <div class="col-xs-12" id="scanForm">
        <hr>
    <form method="post" action="inventory_handler.php" class="form-vertical" id="iForm" role="form"
          data-toggle="validator">
        <fieldset>
            <div class="form-group row">

                <div class="col-xs-12" id="message">
                    <div class="popup">
                        <div class="content"></div>
                    </div>
                </div>
<!--BIN QTY row -->
                <label class="main-input col-xs-4 col-sm-2 control-label" for="BIN">Shelf: </label>
                <div class="col-xs-8 col-sm-5">
                    <input class="form-control input-lg" type="text" id="BIN" name="BIN"
                           autocomplete="off" tabindex="1" required />
                </div>
                <label class="col-xs-5 col-sm-3 control-label" for="QTY">Quantity: </label>
                <div class="col-xs-7 col-sm-2">
                    <input class="form-control input-lg" min="0" type="number" id="QTY" name="QTY" value="0"
                        autocomplete="off" tabindex="3" required />
<!--                           pattern="^[1-9]{1}[0-9]{0,2}$">-->
                </div>

                <?php // A pallet count for Inventory Scanning: Receiving
                    $SKUclass = ($transType === 1)? "col-xs-8 col-sm-5" : "col-xs-8 col-sm-10"; ?>
<!--SKU row -->
                    <label class="col-xs-4 col-sm-2 control-label" for="SKU">SKU: </label>
                    <div class="<?= $SKUclass ?> control-label">
                        <input class="form-control input-lg" type="text" id="SKU" name="SKU"
                               autocomplete="off" tabindex="2" required />
                    </div>
                <?php if($transType === 1){ ?>
<!--Pallet row only for RCSV Inventory type -->

                    <label class="col-xs-5 col-sm-3 control-label" for="PLT">Pallet: </label>
                    <div class="col-xs-7 col-sm-2">
                        <input class="form-control input-lg" type="number" id="PLT" name="PLT"
                               autocomplete="off" tabindex="4" />
                    </div>
                <?php } ?>
<!--PO row now not used-->
<!--                <label class="col-sm-3 control-label" for="PO">PO: </label>-->
<!--                <div class="col-sm-9">-->
<!--                    <input class="form-control input-lg" type="text" id="PO" name="PO">-->
<!--                </div>-->
                <div class="col-xs-12">
                    <button type="submit" class="btn btn-lg btn-primary btn-block" tabindex="4">Submit</button>
                </div>
                <input type="hidden" name="transTypeID" id="mType" value="<?= $transType ?>" />
                <input type="hidden" id="scannedSystem" name="scannedSystem" value="<?= $module ?>">
                <input type="hidden" id="comment" name="comment">
            </div>
        </fieldset>
    </form>
    </div>
    <div class="col-md-12">

            <h2>Scanned items</h2>
<!--            <p>Contextual classes can be used to color table rows or table cells. The classes that can be used are: .active, .success, .info, .warning, and .danger.</p>-->
            <table class="table">
                <thead>
                <tr>
                    <th>SHELF</th>
                    <th>SKU</th>
                    <th>Pallet</th>
                    <th>QTY</th>
                    <th>Timestamp</th>
                </tr>
                </thead>
                <tbody>
                <!-- here data from response -->
                </tbody>
            </table>
        </div>

</div>
