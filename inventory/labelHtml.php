<?php
/**
 * Created by PhpStorm.
 * User: Yeuvheniia Melnykova
 * Date: 2/13/2017
 * Time: 12:50 PM
 */

require_once('../shipping_module/Include/barcode/class/BCGFontFile.php');
require_once('../shipping_module/Include/barcode/class/BCGColor.php');
require_once('../shipping_module/Include/barcode/class/BCGDrawing.php');
require_once('../shipping_module/Include/barcode/class/BCGupca.barcode.php');
require_once('../shipping_module/Include/barcode/class/BCGcode128.barcode.php');
//Twig template engine
require_once('../shipping_module/Include/Twig/Autoloader.php');

if (isset($_REQUEST['SKU']) && !empty($_REQUEST['SKU']) && isset($_REQUEST['LOT']) && !empty($_REQUEST['LOT'])) {
    $sku = $_REQUEST['SKU'];
    $lot = $_REQUEST['LOT'];
    //
    if (!file_exists('Pdf'))
        mkdir('tmp', 0777, true);
    $html_path = "tmp/" . $sku . '_' . $lot . ".html";

    //Initialize Twig
    Twig_Autoloader::register();
    $loader = new Twig_Loader_Filesystem('templates/');
    $twig = new Twig_Environment($loader, array(
        'cache' => 'compilation_cache',
        'auto_reload' => true
    ));
    //
    $sku_lot_img = get_skulot_code($sku . "   " . $lot);
    //generate files and save it to disk, and save file name to array
    $html = $twig->render('barcode_content.html', array('data' => $sku_lot_img, 'code' => $sku . "  " . $lot));
    file_put_contents($html_path, $html);

    if (file_exists($html_path))
        $result = $html_path;
    else
        $result = "Error: file hasn't been generated.";

    //Return JSON
    print_r($result);    //uncomment this!!!

}
else print_r(json_encode('Error! No params included!'));

function get_skulot_code($value)
{
    try{
        // Loading Font
        $font = new BCGFontFile('../shipping_module/Include/barcode/font/Arial.ttf', 14);

        // The arguments are R, G, B for color.
        $color_black = new BCGColor(0, 0, 0);
        $color_white = new BCGColor(255, 255, 255);

        $code = new BCGcode128();
        $code->setScale(1); // Resolution
        $code->setThickness(50); // Thickness
        $code->setForegroundColor($color_black); // Color of bars
        $code->setBackgroundColor($color_white); // Color of spaces
        $code->setFont($font); // Font (or 0)
        $code->parse($value); // Text
        $drawing = new BCGDrawing('', $color_white);
        $drawing->setBarcode($code);
        $drawing->draw();


        ob_start();
        $drawing->finish(BCGDrawing::IMG_FORMAT_PNG);
        $result = base64_encode( ob_get_contents() );
        ob_end_clean();

        unset($color_black);
        unset($color_white);

        return $result;
    } catch (Exception $e){
        $result = "Error. Can not generate SKU LOT barcode. ".$e->getMessage();
        $result = json_encode($result);
        print_r($result);
        die();
    }
}