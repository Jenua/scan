<?php
/**
 * Created by PhpStorm.
 * User: Yeuvheniia Melnykova
 * Date: 3/1/2017
 * Description: This file includes in the main index.php if $_SESSION['module']='rits'
 */
?>
<div class="<?= $module ?> module-container">
    <div class="col-md-12">
        <div id="date_block" class="text-right">Date: <span
                    class="xx-large"><?php echo date('m-d-y'); ?></span></div>
        <h1 class="text-left"><?= ucfirst($module) ?> system</h1>
    </div>
    <div class="col-md-12">
    <form method="post" action="handler.php" class="form-vertical" id="sForm" role="form"
          data-toggle="validator">
        <fieldset>
            <div class="form-group">
                <label class="col-md-12 control-label" for="scannedPO">Scanned barcode: </label>
                <div class="col-md-12" id="message">
                    <div class="popup">
                        <div class="content"></div>
                    </div>
                </div>
                <div class="col-md-12">
                    <input class="form-control input-lg" type="text" id="scanned" name="scanned"
                           required></div>
                <label class="col-sm-2 text-right control-label" for="TRU">
                    Truck: </label>
                <div class="col-sm-2">
                    <input class="form-control input" type="text" id="TRU" name="TruckID"
                           required></div>
                <label class="col-sm-2 text-right control-label" for="PLT">
                    Pallet: </label>
                <div class="col-sm-2">
                    <input class="form-control input" type="text" id="PLT" name="PalletID"
                           required></div>
                <label class="col-sm-1 control-label" for="PO">
                    PO: </label>
                <div class="col-sm-3">
                    <input class="form-control input" type="text" id="PO" name="poNumber"
                           required></div>

                <div class="col-md-12">
                    <button type="submit" class="btn btn-lg btn-primary btn-block">Submit</button>
                </div>
                <input type="hidden" id="scannedSystem" name="scannedSystem" value="<?= $module ?>">
                <input type="hidden" id="comment" name="comment">
            </div>
        </fieldset>
    </form>
    </div>
    <div class="col-md-12">
        <table class="jqGridScannedOrders" id="jqGridScanedOrders"></table>
        <div class="pagernavScannedOrders" id="pagernav"></div>
    </div>