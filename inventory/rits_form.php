<?php
/**
 * Created by PhpStorm.
 * User: Yeuvheniia Melnykova
 * Date: 3/1/2017
 * Description: This file includes in the main index.php if $_SESSION['module']='rits'
 */
global $user;
?>
<div class="col-md-12">
    <h3>Inventory tracking</h3>
    <hr/>
</div>
<div class="col-md-12">
    <form method="post" action="handler.php" class="form-horizontal well" id="rForm" role="form"
          data-toggle="validator">
        <fieldset>
            <legend align="left">Scanned SKU and LOT</legend>
            <div class="form-group">

                <label class="col-sm-3 col-md-4 col-lg-3 control-label" for="scanned">Scanned Code: </label>
                <div class="col-sm-6 col-md-5 col-lg-5">
                    <input class="form-control" type="text" id="scanned"
                           pattern="^(\s)*([A-Z0-9-]+)(\s)*([A-Z0-9]+)(\s)*$"></div>

            </div>
        </fieldset>
        <fieldset>
            <legend align="left">The followed information will be saved</legend>

            <div class="popup">
                <div class="content"></div>
            </div>

            <div class="form-group has-feedback">

                <label class="col-sm-3 col-md-4 col-lg-3 control-label" for="SKU">SKU: </label>
                <div class="col-sm-6 col-md-5 col-lg-5">
                    <input class="form-control" name="SKU" data-minlength="10" data-maxlength="25" type="text" id="SKU"
                           pattern="^[A-Z0-9-]{10,25}$" required>
                    <div class="help-block with-errors"></div>
                </div>

            </div>

            <div class="form-group has-feedback">

                <label class="col-sm-3 col-md-4 col-lg-3 control-label" for="LOT">LOT: </label>
                <div class="col-sm-6 col-md-5 col-lg-5">
                    <input class="form-control" name="LOT" data-minlength="6" data-maxlength="10" type="text" id="LOT"
                           pattern="^[{A-z}0-9-]{6,10}$" required>
                    <div class="help-block with-errors"></div>
                    <div class="checkbox" data-toggle="tooltip" title="Replace with RPMMYYYY">
                        <label><input type="checkbox" id="defaultLot">Invalid or missing LOT information</label></div>
                </div>

            </div>
        </fieldset>

        <div class="form-group">
            <div class="row">
                <div class="col-xs-9 col-xs-offset-3 col-sm-6 col-sm-offset-3 col-md-offset-4 col-lg-9 col-lg-offset-3">
                    <button type="submit" class="btn btn-primary">Save and Print</button>
                    <input type="button" id="test-scan" class="hidden btn btn-info" value="Scan">

                    <a href="#" id="print_pdf" class="hidden btn btn-default" rel="nofollow">
                        <span class="glyphicon glyphicon-print"></span> Print PDF</a>
                    <a href="#" id="print_html" class="hidden btn btn-default" rel="nofollow">
                        <span class="glyphicon glyphicon-print"></span> Print Label</a>
                    <input type="hidden" value="<?= $user->data()->name ?>" name="userName">
                </div>
            </div>
        </div>
        <input type="hidden" id="scannedSystem" name="scannedSystem" value="rits">
    </form>

    <hr>

    <table class="jqGridTable" id="jqGrid"></table>
    <div id="pagernav"></div>
    <hr/>
    <?php if ($user->data()->permission == 'superuser'): ?>
        <fieldset class="report-section">
            <legend align="left">Report to accounting the fixed records/labels</legend>
            <div class="form-group">
                <div class="row">
                    <label for="daterange" class="col-xs-4 col-lg-2 control-label">Report range: </label>
                    <div class="col-xs-4 col-lg-4">
                        <input type="text" class="form-control" name="daterange" id="daterange">
                    </div>
                    <div class="col-xs-2">
                        <a href="#" id="report" class="btn btn-default" rel="nofollow" title="Printed labels list">
                            <span class="glyphicon glyphicon-download-alt"></span> Report </a>
                    </div>
                </div>
            </div>
        </fieldset>

    <?php endif; ?>
</div>