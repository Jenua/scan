<?php
error_reporting(E_ERROR);

$path = $_SERVER['DOCUMENT_ROOT'];
require_once($path.'/shipsingle/shipsingleBigFunc.php');
require_once($path.'/db_connect/connect.php');

if(isset($_POST) && !empty($_POST) && count($_POST) > 2)
{
	$data = array();
	$class = $_POST['class'];
	$lift = $_POST['lift'];
	$resi = $_POST['resi'];
	foreach($_POST as $key => $row)
	{
		if ($key != 'user' && $key != 'carrier' && $key != 'quote' && $key != 'time' && $key != 'cost' && $key != 'class' && $key != 'lift' && $key != 'resi') $data[]= $row;
	}
	
	$conn = Database::getInstance()->dbc;
	
	$query = "SELECT [DEALER], [WEIGHT], [TOZIP], [COST] FROM [dbo].[ShipTrackWithLabel] WHERE [ID] = '".$data[0]."'";
	foreach($data as $key => $ref)
	{
		if($key != 0) $query.=" OR [ID] = '".$ref."'";
	}
	
	$shiptrackData = runQueryCalc($conn, $query);
	$total_old_cost = 0;
	foreach($shiptrackData as $value)
	{
		$total_old_cost+=$value['COST'];
	}
	$tozip = $shiptrackData[0]['TOZIP'];
	$dealer = $shiptrackData[0]['DEALER'];
	$weight = 0;
	foreach($shiptrackData as $ship)
	{
		$weight+=$ship['WEIGHT'] - 70;
	}
	$weight+=70;
	
	$settings = getSettings($conn);

	$originZip = false;
	$originStatecode = false;
	$originCountry = false;
	$originCity = false;
	$ziphead_not_allowed_RL = array();
	$rl_pal_addon_cost_liftgate = 0;
	$rl_pal_addon_cost_residential = 0;
	$fedex_addon_cost_liftgate = 0;
	$rate_ups = false;
	
foreach ($settings as $setting)
{
	if ($setting['group_name'] == 'origin_address' && $setting['type_name'] == 'zip')
	{
		$originZip=$setting['setting_value'];
	} else if ($setting['group_name'] == 'UPS' && $setting['type_name'] == 'rate')
	{
		$rate_ups=$setting['setting_value'];
	} else if ($setting['group_name'] == 'origin_address' && $setting['type_name'] == 'statecode')
	{
		$originStatecode=$setting['setting_value'];
	} else if ($setting['group_name'] == 'origin_address' && $setting['type_name'] == 'country')
	{
		$originCountry=$setting['setting_value'];
	} else if ($setting['group_name'] == 'origin_address' && $setting['type_name'] == 'city')
	{
		$originCity=$setting['setting_value'];
	} else if ($setting['group_name'] == 'rl_pal_addon_cost' && $setting['type_name'] == 'liftgate')
	{
		$rl_pal_addon_cost_liftgate=$setting['setting_value'];
	} else if ($setting['group_name'] == 'rl_pal_addon_cost' && $setting['type_name'] == 'residential')
	{
		$rl_pal_addon_cost_residential=$setting['setting_value'];
	} else if ($setting['group_name'] == 'ziphead_not_allowed' && $setting['type_name'] == 'RL')
	{
		$ziphead_not_allowed_RL[]=$setting['setting_value'];
	} else if ($setting['group_name'] == 'fedex_addon_cost' && $setting['type_name'] == 'liftgate')
	{
		$fedex_addon_cost_liftgate=$setting['setting_value'];
	}
}

	$order = getData($dealer, $tozip, $weight, $class, $lift, $resi);
	/*print_r('<pre>');
	print_r($order);
	print_r('</pre>');
	die();*/
	if ($order != false) startShipsingle($order, $total_old_cost, $weight); else die('Can not get data!');
	
} else
{
	die('Error. Missing data. Make sure at least two orders are chosen.');
}

function getData($dealer, $tozip, $weight, $class, $lift, $resi)
{
		$lowes_mode = getLowesMode($dealer, $resi);
		$order = prepareData($dealer, $tozip, $weight, $class, $lift, $resi, $lowes_mode);
	return $order;
}

function getLowesMode($dealerName, $resi)
{
	$returnVal = 'no';
	if (extrStore($dealerName) == 'Lowe' && $resi == '1')
	{
		$returnVal = 'residential';
	} else if (extrStore($dealerName) == 'Lowe' && $resi == '0')
	{
		$returnVal = 'commercial';
	}
	return $returnVal;
}

function prepareData($dealerName, $toZip, $weight, $class, $lift, $resi, $lowes_mode)
{
	$order = Array
	(
		'ID' => 'QuickShippingQuote',
		'RefNumber' => 'QuickShippingQuote',
		'PONumber' => 'QuickShippingQuote',
		'dealerName' => $dealerName,
		'weight' => $weight,
		'liftgate' => $lift,
		'FreightClass' => $class,
		'ShipPostalCode' => $toZip,
		'residentialFedex' => $resi,
		'residential' => $resi,
		'lowes_mode' => $lowes_mode
	);
	return $order;
}

function filterResults($otherResults, $usedCarriers)
{
	/*print_r('<pre>');
	print_r($otherResults);
	print_r($usedCarriers);
	print_r('</pre>');
	die();*/
	$filteredResults = Array();
	foreach ($otherResults as $otherResult)
	{
		$extrCarrier = extrCarrier($otherResult['carrier']);
		if (in_array($extrCarrier, $usedCarriers) && $otherResult['cost'] >= 20)
		{
			$filteredResults[]=$otherResult;
		}
	}
	if (!empty($filteredResults)) return $filteredResults; else return false;
}

function chooseBest($filteredResults)
{
	$min['cost'] = $filteredResults[0];
	foreach ($filteredResults as $filteredResult)
	{
		if (($filteredResult['cost'] < $min['cost']) /*|| ($filteredResult['cost'] == $min['cost'] && $filteredResult['time'] < $min['time'])*/)
		{
			$min = $filteredResult;
		}
	}
	return $min;
}

function startShipsingle($order, $total_old_cost, $weight)
{
	global $originZip, $originStatecode, $originCountry, $originCity, $ziphead_not_allowed_RL, $rl_pal_addon_cost_liftgate, $rl_pal_addon_cost_residential, $fedex_addon_cost_liftgate, $rate_ups;
	$conn = getConnection();
	 
	 //print_r($order);
	
	$allFinalResults = bigFunc($order, $conn, $originZip, $originStatecode, $originCountry, $originCity, $order['FreightClass'], $rl_pal_addon_cost_liftgate, $rl_pal_addon_cost_residential, $fedex_addon_cost_liftgate, $ziphead_not_allowed_RL, $rate_ups);
	
	
	$usedCarriers = Array('YRC', 'RL', 'FEDEX');
	if (!empty($_GET['yrc'])) $usedCarriers[]=$_GET['yrc'];
	//if (!empty($_GET['yrc_nonpal'])) $usedCarriers[]=$_GET['yrc_nonpal'];
	if (!empty($_GET['odfl'])) $usedCarriers[]=$_GET['odfl'];
	//if (!empty($_GET['odfl'])) $usedCarriers[]=$_GET['odfl'];
	if (!empty($_GET['rl'])) $usedCarriers[]=$_GET['rl'];
	//if (!empty($_GET['rl_pal'])) $usedCarriers[]=$_GET['rl_pal'];
	if (!empty($_GET['fedex'])) $usedCarriers[]=$_GET['fedex'];
	//if (!empty($_GET['fedex_economy'])) $usedCarriers[]=$_GET['fedex_economy'];
	
	/*print_r('<pre>');
	print_r($allFinalResults['otherResults']);
	print_r('</pre>');*/
	
	$cheaporder = $allFinalResults['cheaporder'];
	$otherResults = $allFinalResults['otherResults'];
	$filteredResults = filterResults($otherResults, $usedCarriers);
	/*print_r('<pre>');
	print_r($filteredResults);
	print_r('</pre>');*/
	if ($filteredResults) $min = chooseBest($filteredResults); else $min = false;

		if ($min)
		{
			$min['logo'] = getCarrierLogo($min['carrier']);
			$min['date'] = date('m.d.Y');
			$min['total_old_cost'] = $total_old_cost;
			$min['weight'] = $weight;
			$min = json_encode($min);
			print_r($min);
		} else
		{
			$min = 'Can not get rate for this order.';
			$min = json_encode($min);
			print_r($min);
		}
}

function runQueryCalc($conn, $query)
{
	try
	{
		//echo "<br>".$query."<br>";
		$stmt = $conn->prepare($query);
		$result = $stmt->execute();
		$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}catch (Exception $e) {
		die("<br>".$e."<br>");
		return false;
	}
}

function saveToValidationLog($conn, $messages)
{
	echo "<br>Error.<br>";
	//print_r($messages);
}

function display_message($value)
{
	//$date = date("h:i:s");
	$date = '';
	echo '<span style="color:red;">'.$date.': </span><span style="color:black; font-weight: bold;">'.$value.'</span><br>';
}
?>