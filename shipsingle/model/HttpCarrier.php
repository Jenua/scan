<?php

require_once("AbstractCarrier.php");

class HttpCarrier extends AbstractCarrier
{
	protected $_needed;
	protected $_error;
	protected $_i;

	public function __construct($name, $url, $timeout, $needed, $mapper, $error)
	{
		parent::__construct($name, $url, $timeout, $mapper);
		$this->_needed = $needed;
		$this->_error = $error;
	}

	public function getNeeded()
	{
		return (array) $this->_needed;
	}

	public function getErrorMessage(){
		return $this->_error;
	} 
        public function getResponse($url, $xmlParser) {
    	  $response = Transfer::getHTTPResponse($url);
          $result = array('result' => $xmlParser->parse($response, $this->_needed));
          /*if ($result["result"]["ERROR"] == "Cannot process your request at this time" && $i < 2) {
          	$this->_i++;
        	return $this->getResponse($url, $xmlParser);
          } else { */
            return $result;
          //}
        }
	function setInUrl($placeHolder, $value)
	{
		$this->_url = str_replace($placeHolder, $value, $this->_url);
	}
}

?>