<?php

require_once("AbstractShipper.php");

class NonPalletShipper extends AbstractShipper
{
	protected $_carrier;
	protected $_carrierResponse;
	protected $_carrierResponseMsg;

	public function __construct($name, $carrier, $fromZip, $destZip, $weight, $class, $options = array())
	{
		parent::__construct($name, 'Standard', $fromZip, $destZip, $weight, $class, $options);
		$this->_carrier = $carrier;
		$this->_carrierResponse = array();
		$this->_carrierResponseMsg = "";
	}

	public function getCarrier()
	{
		return $this->_carrier;
	}

	public function parseCarrierResponce($carrierResponse)
	{
        if($carrierResponse['status'] != 'ok') {
            $this->_carrierResponseMsg = $carrierResponse['status'];
        }
        $result = $carrierResponse['result'];
		if(!empty($result)) {
			if($mapper = $this->_carrier->getMapper()) {
				$result = $mapper->convert($result);
			}
		} else {
			$carrierName = $this->_carrier->getName();
            $this->_carrierResponseMsg = "The response from `$carrierName` server is empty.";

			$mapper = new NAMapper();
			$result = $mapper->convert(array());
		}
		$this->_carrierResponse = $result;
	}

	public function setFromZip($fromZip)
	{
		$this->_fromZip = $fromZip;
	}

	public function setFromState($fromState)
	{
		$this->_fromState = $fromState;
	}

	public function setLogo($logo)
	{
		$this->_logo = $logo;
	}

	public function setPreference($preference)
	{
		$this->_preference = $preference;
	}

	public function setTargetMet($targetMet)
	{
		$this->_targetMet = $targetMet;
	}

	public function setMultiplier($multiplier)
	{
		$this->_multiplier = $multiplier;
	}
	
	public function getResult()
	{
		return array_merge(
			$this->_carrierResponse,
			array('ERROR_MESSAGE' => $this->_carrierResponseMsg),
			array(
				'NAME'        => $this->_name,
				'CARRIERNAME' => $this->_carrier->getName(),
				'TYPE'        => $this->_type,
				'FROMZIP'     => $this->_fromZip['zip'],
				'FROMSTATE'   => $this->_fromZip['statecode'],
				'LOGO'        => $this->_logo,
				'COST'        => Calculate::Cost(
					$this->_carrier->getName(),
					$this->_carrierResponse['CHARGE'],
					($this->_multiplier) ? $this->_multiplier : 1,
					($this->_isCostWeb) ? $this->_multiplierWeb : 1.1
				),
			)
		);
	}
}

?>