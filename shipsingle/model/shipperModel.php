<?php
require_once('DBConnectionModel.php');

class ShippingModel
{
	private $_db;
	private $_db1;
	protected $_name;
	protected $_url;
	protected $_file_name;

	public function __construct() 
	{
		$this->_db = new DBConnectionModel();
	}

	public function getInProcessOrders($query) 
	{
		$result = $this->_db->query($query, 'SELECT');
		$result = $result->fetchAll(PDO::FETCH_ASSOC);
		$countRefNumbers = $this->countRefNumbers($result);
		$countRefNumbers = $this->addSKUs($result, $countRefNumbers);
		$countRefNumbers = $this->selectRate($countRefNumbers);
		$result = $this->uniqueOrders($result, $countRefNumbers);
		$result = $this->addRatesToOrders($result, $countRefNumbers);
		$result = $this->addSkusToOrders($result, $countRefNumbers);
		$result = $this->shipPostal($result);
		$result = $this->formatArray($result);
		/*print_r('<pre>');
		print_r($result);
		print_r('</pre>');
		die('stop');*/

			if (isset($result) && !empty($result))
			{
				return $result;
			}else
			{
				return false;
			}
	}
	
	public function addRatesToOrders($orders, $countRefNumbers)
	{
		foreach($countRefNumbers as $key1 => $value1)
		{
			foreach($orders as $key2 => $value2)
			{
				if($value1['RefNumber'] == $value2['RefNumber'])
				{
					$orders[$key2]['rate'] = $value1['rate'];
					unset($orders[$key2]['Rate']);
				}
			}
		}
		return $orders;
	}
	
	public function selectRate($countRefNumbers){
		foreach($countRefNumbers as $key1 => $value1)
		{
			$bestRate = 0;
			foreach($value1['rates'] as $rate)
			{
				if ($rate > $bestRate) $bestRate = $rate;
			}
			$countRefNumbers[$key1]['rate'] = $bestRate;
			unset($countRefNumbers[$key1]['rates']);
		}
		return $countRefNumbers;
	}
	
	public function formatArray($orders){
		foreach ($orders as $key => $value)
		{
			$orders[$value['RefNumber']] = $value;
			$orders[$value['RefNumber']]['ID'] = $value['RefNumber'];
			unset($orders[$key]);
		}
		return $orders;
	}
	
	public function addSkusToOrders($orders, $countRefNumbers){
		foreach($countRefNumbers as $kay1 => $value1)
		{
			foreach($orders as $key2 => $value2)
			{
				if($value1['RefNumber'] == $value2['RefNumber'])
				{
					$orders[$key2]['countRefNumber'] = $value1['countRefNumber'];
					$orders[$key2]['skus'] = $value1['skus'];
				}
			}
		}
		return $orders;
	}
	
	public function shipPostal($orders)
	{
		foreach($orders as $key => $value)
		{
			$orders[$key]['ShipPostalCode'] = trim(str_replace(' ', '', $value["ShipAddress_PostalCode"]));
			//unset($orders[$key]["ShipAddress_PostalCode"]);
		}
		return $orders;
	}
	
	public function uniqueOrders($orders, $countRefNumbers){
		foreach($countRefNumbers as $countRefNumber)
		{
			$i = 0;
			foreach($orders as $key => $value)
			{
				unset($orders[$key]['ItemGroupRef_FullName']);
				if($countRefNumber['RefNumber'] == $value['RefNumber'])
				{
					$i++;
					if ($i>1)
					{
						unset($orders[$key]);
					}
				}
			}
		}
		return $orders;
	}
	
	public function countRefNumbers($orders){
		$countRefNumbers = array();
		foreach($orders as $order)
		{
			$i = 0;
			foreach($orders as $order1)
			{
				if(in_array($order['RefNumber'], $order1))
				{
					$i++;
				}
			}
			$countRefNumbers[] = array('RefNumber' => $order['RefNumber'], 'countRefNumber' => $i);
		}
		$countRefNumbers= array_map("unserialize", array_unique( array_map("serialize", $countRefNumbers) ));
		return $countRefNumbers;
	}
	
	public function addSKUs($orders, $countRefNumbers)
	{
		foreach($countRefNumbers as $key => $value)
		{
		$skus = array();
		$rates = array();
			foreach($orders as $key2 => $value2)
			{
				if($value2['RefNumber'] == $value['RefNumber'])
				{
					$skus[] = $value2['ItemGroupRef_FullName'];
					$rates[] = $value2['Rate'];
				}
			}
		$countRefNumbers[$key]['skus'] = $skus;
		$countRefNumbers[$key]['rates'] = $rates;
		}
		return $countRefNumbers;
	}

	public function getCarieer() {
		  		
		$query = "SELECT id, IsPallet, Contact, Multiplier, MaxWeight, preference, MultiplierWeb
  				  FROM Carrier_Profile
  				  WHERE IsActive = 1";

  		$result = $this->_db->query($query, 'SELECT');

  		while ($row = $result->fetch()) {
  			$rows[$row['Contact']] = $row;
  		}
  		return $rows;
	} 
	
	public function getRate($carrier_id, $sate, $zip){
		$zip = str_split($zip, 3);
		$query = "SELECT Rate FROM PalletRates WHERE CarierProfileId = " . $carrier_id . " AND state = '" . $sate . "' AND ziphead = '" . $zip[0]  . "'";
		$result = $this->_db->query($query, 'SELECT');

		$rate = $result->fetch();

		return $rate[0];
	}
//Sandeep Gopal [June 25, 2013]: For approximately 1400 New York Zip codes, RL Pallet charges more money than the stimate returned by the API (Tolls and other surcharges)
//The sales team asked us to filter out RL Pallet estimates for these zip codes with a look up against a table  zip_code_for_ny in the shipping DB. 
	public function getZipCodeNY($sate, $zip){

		$query = "SELECT State_Code FROM zip_code_for_ny WHERE State_Code LIKE '%" . $sate . "%' AND Postal = '" . $zip  . "'";

		$result = $this->_db->query($query, 'SELECT');

		$zipNY = $result->fetch();

		if (!empty($zipNY)) {
		  return TRUE;
		} else {
		  return FALSE;
		}
	}

	public function export($result) 
	{
	    $this->_file_name = 'Orders/BestShipper_' . date("m_d_Y_H_i") . '.csv';
	    $fp = fopen('Orders/BestShipper_' . date("m_d_Y_H_i") . '.csv', 'w+');
	    $head[0][0] = array("ID", "FROMZIP", "TOZIP", "CARRIERNAME", "QUOTENUMBER", "TRANSITTIME", "COST", "PONUMBER", "WEIGHT");
	    $bestOforders = array_merge($head, $result);

	    for ($j=0; $j<count($bestOforders); $j++) {
        	foreach ($bestOforders[$j] as $fields) {
            	fputcsv($fp, $fields);
        	}
        }

        fclose($fp);
	}

	
	/*public function exportAllShipers($result) 
	{
	    $fp = fopen('Orders/AllShipers_' . date("m_d_Y_H_i_s") . '.csv', 'w+');
	    $head[0][0] = array("ID", "FROMZIP", "TOZIP", "CARRIERNAME", "QUOTENUMBER", "TRANSITTIME", "COST", "PONUMBER", "WEIGHT");
	    $bestOforders = array_merge($head, $result);

	    for ($j=0; $j<count($bestOforders); $j++) {
        	foreach ($bestOforders[$j] as $fields) {
            	fputcsv($fp, $fields);
        	}
        }

        fclose($fp);
	}*/



	public function sendErrorMsg($errors) {
		require_once "Mail/Mail.php";
		require_once "Mail/mime.php";
		require_once "Net_SMTP/SMTP.php";
		
                $from = EMAIL_FROM;
		$to = EMAIL_TO;
		$port = SMTP_PORT;
		$subject = "Daily quotes for best shipper";

                $headers = array ('From' => $from,'To' => $to, 'Subject' => $subject);

                $text = '';

		$html = '<html>';
		$html .= '<head>';
		$html .= '</head><body><h4>The attached spreadsheet has the best shipping estimates for all LTL orders - webrole</h4><table border="1">';
		$html .= '<td>ID</td>';
		$html .= '<td>Carrier name</td>';
		$html .= '<td>Error</td>';

		foreach ($errors as $key => $value) {
			foreach ($value as $name => $carrier) {
				$html .= '<tr>';
				$html .= '<td>' . $key . '</td>';
				$html .= '<td>' . $name. '</td>';
				$html .= '<td>' . $carrier['ERROR'] . '</td>';
				$html .= '</tr>';
			}
		}
		$html .= '</table></body></html>';

                $file = $this->_file_name; // attachment
		$crlf = "\n";
 
		$mime = new Mail_mime($crlf);
		$mime->setTXTBody($text);
		$mime->setHTMLBody($html);
		$mime->addAttachment($file, 'text/plain');

		$body = $mime->get();
		$headers = $mime->headers($headers);

		$host = SMTP_SERVER;
		$username = SMTP_USER;
		$password = SMTP_PASSWORD;

		$smtp = Mail::factory('smtp', 
				array ('host' => $host,
                                       'port' => $port, 
				       'auth' => true,
		 		       'username' => $username,
				       'password' => $password));
 
		$mail = $smtp->send($to, $headers, $body);
 
		if (PEAR::isError($mail)) {
		  echo("<p>" . $mail->getMessage() . "</p>");
		}
		else {
		  echo("<p>Message successfully sent!</p>");
		  $hour = date('G');
		  $day  = date('N'); // 1..7 for Monday to Sunday
		  $minute = date('i');
		  echo $day." ".$hour." ".$minute;	
		}
	}	
}
