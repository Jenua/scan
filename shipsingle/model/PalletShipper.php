<?php

require_once("AbstractShipper.php");

class PalletShipper extends AbstractShipper
{
	protected $_carrierName;
	protected $_maxWeight;
	protected $_pallets;
	protected $_rate;
	protected $_transitTime;

	public function __construct($name, $carrierName, $fromZip, $destZip, $weight, $class, $options = array())
	{
		parent::__construct($name, 'Pallet', $fromZip, $destZip, $weight, $class, $options);
		$this->_carrierName = $carrierName;
	}

	public function setMaxWeight($maxWeight)
	{
		$this->_maxWeight = $maxWeight;
	}

	public function setPallets($pallets)
	{
		$this->_pallets = $pallets;
	}
	
	public function setRate($rate)
	{
		$this->_rate = $rate;
	}

	public function setTransitTime($transitTime)
	{
		$this->_transitTime = $transitTime;
	}

	public function getResult()
	{
        $requiredWeight = floor($this->_weight / $this->_maxWeight) + ceil($this->_weight % $this->_maxWeight / $this->_maxWeight);
		return array(
			'ERROR_MESSAGE' => '',
			'NAME'          => $this->_name,
			'CARRIERNAME'   => $this->_carrierName,
			'TYPE'          => $this->_type,
			'FROMZIP'       => $this->_fromZip['zip'],
			'FROMSTATE'     => $this->_fromZip['statecode'],
			'LOGO'          => $this->_logo,
            'QUOTENUMBER'   => ($this->_pallets == 1) ? $this->_pallets.' pallet' : $this->_pallets.' pallets',
            'TRANSITTIME'   => 1,
			'COST'          => Calculate::PalletCost(
				$requiredWeight, $this->_rate, $this->_multiplier,
				($this->_isCostWeb) ? $this->_multiplierWeb : 1
			),
		);
	}
	public function getFedexResult($time, $quotenum, $cost)
	{
		return array(
			'ERROR_MESSAGE' => '',
			'NAME'          => $this->_name,
			'CARRIERNAME'   => $this->_carrierName,
			'TYPE'          => $this->_type,
			'FROMZIP'       => $this->_fromZip['zip'],
			'FROMSTATE'     => $this->_fromZip['statecode'],
			'LOGO'          => $this->_logo,
            'QUOTENUMBER'   => $quotenum,
//            'QUOTENUMBER'   => ($this->_pallets == 1) ? $this->_pallets.' pallet' : $this->_pallets.' pallets',
//            'TRANSITTIME'   => 1,
            'TRANSITTIME'   => $time,
			'COST'          => $cost
		);
	}
	
}

?>