<?php

require_once("AbstractCarrier.php");

class SoapCarrier extends AbstractCarrier
{
	protected $_namespace;

	public function __construct($name, $url, $timeout, $namespace, $mapper)
	{
		parent::__construct($name, $url, $timeout, $mapper);
		$this->_namespace = $namespace;
	}

	public function getNamespace()
	{
		return $this->_namespace;
	}
}

?>