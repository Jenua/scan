<?php
$path = $_SERVER['DOCUMENT_ROOT'];
require_once($path.'/db_connect/connect.php');

class DBConnectionModel
{
	protected $_connect;

	public function __construct() {
		  $this->_connect = Database::getInstance()->dbc;
	}

	public function query($query, $flag){

		$result = $this->_connect->prepare($query);
		$result->execute();

	    return $result;
	}	
	
	public function query1($query, $flag){

		$result = $this->_connect->prepare($query);
		$result->execute();

	    return $result;
	}			  
}
