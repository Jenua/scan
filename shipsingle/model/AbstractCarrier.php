<?php

abstract class AbstractCarrier
{
	protected $_name;
	protected $_url;
	protected $_timeout;
	protected $_mapper;

	public function __construct($name, $url, $timeout, $mapper)
	{
		$this->_name = $name;
		$this->_url = $url;
		$this->_timeout = $timeout;
		$this->_mapper = $mapper;
	}

	public function getName()
	{
		return $this->_name;
	}

	public function getUrl()
	{
		return $this->_url;
	}

	public function getTimeout()
	{
		return $this->_timeout;
	}

	public function getMapper()
	{
		return $this->_mapper;
	}
}

?>