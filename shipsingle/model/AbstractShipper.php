<?php

abstract class AbstractShipper
{
	protected $_name;
	protected $_type;
	protected $_weight;
	protected $_class;
	protected $_nmfc;
	protected $_fromZip;
	protected $_destZip;
	protected $_options;
	protected $_logo;
	protected $_multiplier;
	protected $_multiplierWeb;
	protected $_isCostWeb;

	public function __construct($name, $type, $fromZip, $destZip, $weight, $class, $options)
	{
		$this->_name = $name;
		$this->_type = $type;
		$this->_fromZip = $fromZip;
		$this->_destZip = $destZip;
		$this->_weight = $weight;
		$this->_class = $class;
		$this->_options = $options;
		$this->_isCostWeb = false;
	}

	public abstract function getResult();

	public function getWeight()
	{
		return $this->_weight;
	}

	public function getClass()
	{
		return $this->_class;
	}

	public function setNmfc($nmfc)
	{
		$this->_nmfc = $nmfc;
	}

	public function getNmfc()
	{
		return $this->_nmfc;
	}

	public function getOptions()
	{
		return $this->_options;
	}

	public function getFromZip()
	{
		return $this->_fromZip;
	}

	public function getDestZip()
	{
		return $this->_destZip;
	}

	public function setLogo($logo)
	{
		$this->_logo = $logo;
	}

	public function setMultiplier($multiplier)
	{
		$this->_multiplier = $multiplier;
	}

	public function setMultiplierWeb($multiplierWeb)
	{
		$this->_multiplierWeb = $multiplierWeb;
	}

	public function setCostWebTrue()
	{
		$this->_isCostWeb = true;
	}
}

?>