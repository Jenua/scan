<?php
function getAddrLine($lat, $long)
{
	$url  = "http://maps.googleapis.com/maps/api/geocode/json?latlng=".$lat.",".$long."&sensor=false";
	$json = @file_get_contents($url);
	$data = json_decode($json);
	$status = $data->status;
	$address = false;
	if($status == "OK")
	{
		$address = $data->results[0]->formatted_address;
	}
	return $address;
}

echo getAddrLine('40.2180742', '-75.0727956');
?>