<?php
echo "<br><strong>SHIPSINGLE</strong><br><a target='_blank' href='monitor.php'>Click here to monitor shipsingle.</a><br><br>";
$max_exec_time = 1200; //max execution time 20 minutes. Set in seconds
$delay = $max_exec_time + 10;
ini_set('max_execution_time', $max_exec_time);
error_reporting(E_ERROR); //show only errors. No warnings. No notices.
date_default_timezone_set('America/New_York');
require_once('model/shipperModel.php');
require_once('helpers/fedexAddressValidation.php');
require_once('shipsingleBigFunc.php');
require_once('functions.php');
$path = $_SERVER['DOCUMENT_ROOT'];
require_once($path.'/db_connect/connect.php');

$conn = Database::getInstance()->dbc;

//if there is parameter id in request, then calculate only this order
if (isset($_GET['id']) && !empty($_GET['id']))
{
	$RefNumber = " and [shiptrack].[RefNumber] = '".$_GET['id']."'";
        $RefNumber_manually = " and [manually_shiptrack].[RefNumber] = '".$_GET['id']."'";
} else if(isset($_GET['po']) && !empty($_GET['po']))
{
	$RefNumber = " and [shiptrack].[PONumber] = '".$_GET['po']."'";
        $RefNumber_manually = " and [manually_shiptrack].[PONumber] = '".$_GET['po']."'";
} else
{
	$RefNumber = '';
        $RefNumber_manually = '';
}

//read settings from DB
$settings = getSettings($conn);

$authorized_dealers = array();
$FreightClass = false;
$originZip = false;
$originStatecode = false;
$originCountry = false;
$originCity = false;
$pro_numbers_estes_min = false;
$pro_numbers_estes_max = false;
$pro_numbers_estes_fd = false;
$pro_numbers_fedex_min = false;
$pro_numbers_fedex_max = false;
$pro_numbers_rl_min = false;
$pro_numbers_rl_max = false;
$pro_numbers_ups_min = false;
$pro_numbers_ups_max = false;
$pro_numbers_nemf_min = false;
$pro_numbers_nemf_max = false;
$pro_numbers_abf_min = false;
$pro_numbers_abf_max = false;
$pro_numbers_abf_first_digit = false;
$pro_numbers_yrc_min = false;
$pro_numbers_yrc_max = false;
$pro_numbers_amazonds_min = false;
$pro_numbers_amazonds_max = false;
$pro_numbers_yrc_before_hyphen = false;
$pro_numbers_odfl_min = false;
$pro_numbers_odfl_max = false;
$ziphead_not_allowed_RL = array();
$rl_pal_addon_cost_liftgate = 0;
$fedex_addon_cost_liftgate = 0;
$ORDER_PROCESSING_MODE_FEDEX = false;
$rl_pal_addon_cost_residential = 0;
$query_mode_dealer_setting = false;
$remote_orders = array();
$remote_orders_dealer_name = array();
$shipping_ground = array();
$PrintCarrierForAmazonMode = false;
$shipping_ground_weight_max = false;
$pro_numbers_pyle_min = false;
$pro_numbers_pyle_max = false;
$rate_ups = false;

//get settings
foreach ($settings as $setting)
{
	if ($setting['group_name'] == 'authorized_dealers' && $setting['type_name'] == 'dealer_name')
	{
		$authorized_dealers[]=$setting['setting_value'];
	} else if ($setting['group_name'] == 'freight_setting' && $setting['type_name'] == 'FreightClass')
	{
		$FreightClass=$setting['setting_value'];
	} else if ($setting['group_name'] == 'UPS' && $setting['type_name'] == 'rate')
	{
		$rate_ups=$setting['setting_value'];
	} else if ($setting['group_name'] == 'origin_address' && $setting['type_name'] == 'zip')
	{
		$originZip=$setting['setting_value'];
	} else if ($setting['group_name'] == 'origin_address' && $setting['type_name'] == 'statecode')
	{
		$originStatecode=$setting['setting_value'];
	} else if ($setting['group_name'] == 'origin_address' && $setting['type_name'] == 'country')
	{
		$originCountry=$setting['setting_value'];
	} else if ($setting['group_name'] == 'origin_address' && $setting['type_name'] == 'city')
	{
		$originCity=$setting['setting_value'];
	} else if ($setting['group_name'] == 'pro_numbers_estes' && $setting['type_name'] == 'min')
	{
		$pro_numbers_estes_min=$setting['setting_value'];
	} else if ($setting['group_name'] == 'pro_numbers_estes' && $setting['type_name'] == 'max')
	{
		$pro_numbers_estes_max=$setting['setting_value'];
	} else if ($setting['group_name'] == 'pro_numbers_estes' && $setting['type_name'] == 'first digit')
	{
		$pro_numbers_estes_fd=$setting['setting_value'];
	} else if ($setting['group_name'] == 'pro_numbers_fedex' && $setting['type_name'] == 'min')
	{
		$pro_numbers_fedex_min=$setting['setting_value'];
	} else if ($setting['group_name'] == 'pro_numbers_fedex' && $setting['type_name'] == 'max')
	{
		$pro_numbers_fedex_max=$setting['setting_value'];
	} else if ($setting['group_name'] == 'pro_numbers_rl' && $setting['type_name'] == 'min')
	{
		$pro_numbers_rl_min=$setting['setting_value'];
	} else if ($setting['group_name'] == 'pro_numbers_rl' && $setting['type_name'] == 'max')
	{
		$pro_numbers_rl_max=$setting['setting_value'];
	} else if ($setting['group_name'] == 'pro_numbers_ups' && $setting['type_name'] == 'min')
	{
		$pro_numbers_ups_min=$setting['setting_value'];
	} else if ($setting['group_name'] == 'pro_numbers_ups' && $setting['type_name'] == 'max')
	{
		$pro_numbers_ups_max=$setting['setting_value'];
	} else if ($setting['group_name'] == 'pro_numbers_nemf' && $setting['type_name'] == 'min')
	{
		$pro_numbers_nemf_min=$setting['setting_value'];
	} else if ($setting['group_name'] == 'pro_numbers_nemf' && $setting['type_name'] == 'max')
	{
		$pro_numbers_nemf_max=$setting['setting_value'];
	} else if ($setting['group_name'] == 'pro_numbers_abf' && $setting['type_name'] == 'min')
	{
		$pro_numbers_abf_min=$setting['setting_value'];
	} else if ($setting['group_name'] == 'pro_numbers_abf' && $setting['type_name'] == 'max')
	{
		$pro_numbers_abf_max=$setting['setting_value'];
	} else if ($setting['group_name'] == 'pro_numbers_abf' && $setting['type_name'] == 'first digit')
	{
		$pro_numbers_abf_first_digit=$setting['setting_value'];
	} else if ($setting['group_name'] == 'pro_numbers_yrc' && $setting['type_name'] == 'min')
	{
		$pro_numbers_yrc_min=$setting['setting_value'];
	} else if ($setting['group_name'] == 'pro_numbers_yrc' && $setting['type_name'] == 'max')
	{
		$pro_numbers_yrc_max=$setting['setting_value'];
	} else if ($setting['group_name'] == 'pro_numbers_amazonds' && $setting['type_name'] == 'min')
	{
		$pro_numbers_amazonds_min=$setting['setting_value'];
	} else if ($setting['group_name'] == 'pro_numbers_amazonds' && $setting['type_name'] == 'max')
	{
		$pro_numbers_amazonds_max=$setting['setting_value'];
	} else if ($setting['group_name'] == 'pro_numbers_yrc' && $setting['type_name'] == 'before_hyphen')
	{
		$pro_numbers_yrc_before_hyphen=$setting['setting_value'];
	} else if ($setting['group_name'] == 'pro_numbers_odfl' && $setting['type_name'] == 'min')
	{
		$pro_numbers_odfl_min=$setting['setting_value'];
	} else if ($setting['group_name'] == 'pro_numbers_odfl' && $setting['type_name'] == 'max')
	{
		$pro_numbers_odfl_max=$setting['setting_value'];
	} else if ($setting['group_name'] == 'pro_numbers_pyle' && $setting['type_name'] == 'min')
	{
		$pro_numbers_pyle_min=$setting['setting_value'];
	} else if ($setting['group_name'] == 'pro_numbers_pyle' && $setting['type_name'] == 'max')
	{
		$pro_numbers_pyle_max=$setting['setting_value'];
	} else if ($setting['group_name'] == 'rl_pal_addon_cost' && $setting['type_name'] == 'liftgate')
	{
		$rl_pal_addon_cost_liftgate=$setting['setting_value'];
	} else if ($setting['group_name'] == 'fedex_addon_cost' && $setting['type_name'] == 'liftgate')
	{
		$fedex_addon_cost_liftgate=$setting['setting_value'];
	} else if ($setting['group_name'] == 'order_processing_mode' && $setting['type_name'] == 'fedex')
	{
		$ORDER_PROCESSING_MODE_FEDEX=$setting['setting_value'];
	} else if ($setting['group_name'] == 'PrintCarrierForAmazon' && $setting['type_name'] == 'PrintCarrierForAmazonMode')
	{
		$PrintCarrierForAmazonMode=$setting['setting_value'];
	} else if ($setting['group_name'] == 'rl_pal_addon_cost' && $setting['type_name'] == 'residential')
	{
		$rl_pal_addon_cost_residential=$setting['setting_value'];
	} else if ($setting['group_name'] == 'query_mode' && $setting['type_name'] == 'dealer_setting')
	{
		$query_mode_dealer_setting=$setting['setting_value'];
	} else if ($setting['group_name'] == 'remote_orders' && $setting['type_name'] == 'state')
	{
		$remote_orders[]=$setting['setting_value'];
	} else if ($setting['group_name'] == 'ziphead_not_allowed' && $setting['type_name'] == 'RL')
	{
		$ziphead_not_allowed_RL[]=$setting['setting_value'];
	} else if ($setting['group_name'] == 'remote_orders' && $setting['type_name'] == 'dealer_name')
	{
		$remote_orders_dealer_name[]=$setting['setting_value'];
	} else if ($setting['group_name'] == 'shipping_ground' && $setting['type_name'] == 'sku')
	{
		$shipping_ground[]=$setting['setting_value'];
	} else if ($setting['group_name'] == 'shipping_ground_weight' && $setting['type_name'] == 'max')
	{
		$shipping_ground_weight_max=$setting['setting_value'];
	}
}

//Store assigned PROs in arrays to not assign duplicates
$FEDEXassignedPROs = Array();
$RLassignedPROs = Array();
$ESTESassignedPROs = Array();
$UPSassignedPROs = Array();
$NEWENGLassignedPROs = Array();
$YRCassignedPROs = Array();
$ODFLassignedPROs = Array();

//Change main query based on settings
$query_dealers = query_mode_dealer($query_mode_dealer_setting, $authorized_dealers);

//There are constant values from config file in query
$query1 = "SET NOCOUNT ON
BEGIN
DECLARE @Orders_to_restore TABLE
(
   po varchar(255)
);

--Temporary table to store orders to rate
DECLARE @Orders_to_process TABLE
(
	[TxnID] varchar(255),
	[PONumber] varchar(255),
	[RefNumber] varchar(255),
	[ShipAddress_PostalCode] varchar(255),
	[ShipAddress_State] varchar(255),
	[CustomerRef_FullName] varchar(255),
	[ShipMethodRef_FullName] varchar(255),
	[ShipAddress_City] varchar(255),
	[ShipAddress_Country] varchar(255),
	[ShipAddress_Addr1] varchar(255),
	[ShipAddress_Addr2] varchar(255),
	[memo] varchar(255)
);

--Temporary table to store manually created orders to rate
DECLARE @Orders_to_process_manually_created TABLE
(
	[TxnID] varchar(255),
	[PONumber] varchar(255),
	[RefNumber] varchar(255),
	[ShipAddress_PostalCode] varchar(255),
	[ShipAddress_State] varchar(255),
	[CustomerRef_FullName] varchar(255),
	[ShipMethodRef_FullName] varchar(255),
	[ShipAddress_City] varchar(255),
	[ShipAddress_Country] varchar(255),
	[ShipAddress_Addr1] varchar(255),
	[ShipAddress_Addr2] varchar(255),
	[memo] varchar(255)
);

--Special_Order temporary table
DECLARE @Special_Order TABLE
(
   PO varchar(255)
);

--Direct_Ship temporary table
DECLARE @Direct_Ship TABLE
(
   PO varchar(255)
);

--Temporary table to store full data of orders to rate
DECLARE @Orders_to_process_full_data TABLE
(
	[TxnID] varchar(255),
	[PONumber] varchar(255),
	[RefNumber] varchar(255),
	[ShipAddress_PostalCode] varchar(255),
	[ShipAddress_State] varchar(255),
	[dealerName] varchar(255),
	[ShipMethodRef_FullName] varchar(255),
	[ShipAddress_City] varchar(255),
	[ShipAddress_Country] varchar(255),
	[ShipAddress_Addr1] varchar(255),
	[ShipAddress_Addr2] varchar(255),
	[memo] varchar(255),
	[ItemGroupRef_FullName] varchar(255),
	[Rate] varchar(255),
	[quantity] varchar(255),					
	[liftgate] int,
	[lowes_mode] varchar(255),
	[residential] varchar(255),
	[weight] varchar(255),
	[weight_inventory] varchar(255),
	[force_carrier] varchar(255)
);

INSERT INTO
	@Orders_to_restore
SELECT po FROM [dbo].[restore_order]

--Select orders to rate
INSERT INTO
	@Orders_to_process
SELECT  TOP 1000
	[shiptrack].[TxnID],
	[shiptrack].[PONumber],
	[shiptrack].[RefNumber],
	[shiptrack].[ShipAddress_PostalCode],
	[shiptrack].[ShipAddress_State],
	[shiptrack].[CustomerRef_FullName],
	[shiptrack].[ShipMethodRef_FullName],
	[shiptrack].[ShipAddress_City],
	[shiptrack].[ShipAddress_Country],
	[shiptrack].[ShipAddress_Addr1],
	[shiptrack].[ShipAddress_Addr2],
	[shiptrack].[memo]
FROM [dbo].[shiptrack]
	LEFT JOIN [dbo].[ShipTrackWithLabel] ON [shiptrack].[PONumber] = [ShipTrackWithLabel].[PONUMBER] AND [shiptrack].[RefNumber] = [ShipTrackWithLabel].[ID]
WHERE
	[shiptrack].[PONumber] IS NOT NULL
	AND [shiptrack].[RefNumber] IS NOT NULL
	AND 
        (
            [shiptrack].[IsFullyInvoiced] = 'false' 
            OR [shiptrack].[PONumber] IN (SELECT po FROM @Orders_to_restore)
        )
	AND
        (
            [shiptrack].[IsManuallyClosed] = 'false'
            OR [shiptrack].[PONumber] IN (SELECT po FROM @Orders_to_restore)
        )
	AND [shiptrack].[SalesRepRef_FullName] != 'HDDC'
        AND [shiptrack].[SalesRepRef_FullName] != 'HOSP'
	AND [ShipTrackWithLabel].[CARRIERNAME] IS NULL
        ".$RefNumber.";

--Select manually created orders to rate
INSERT INTO
	@Orders_to_process_manually_created
SELECT 
	[manually_shiptrack].[TxnID],
	[manually_shiptrack].[PONumber],
	[manually_shiptrack].[RefNumber],
	[manually_shiptrack].[ShipAddress_PostalCode],
	[manually_shiptrack].[ShipAddress_State],
	[manually_shiptrack].[CustomerRef_FullName],
	[manually_shiptrack].[ShipMethodRef_FullName],
	[manually_shiptrack].[ShipAddress_City],
	[manually_shiptrack].[ShipAddress_Country],
	[manually_shiptrack].[ShipAddress_Addr1],
	[manually_shiptrack].[ShipAddress_Addr2],
	[manually_shiptrack].[memo]
FROM [dbo].[manually_shiptrack]
	LEFT JOIN [dbo].[ShipTrackWithLabel] ON [manually_shiptrack].[PONumber] = [ShipTrackWithLabel].[PONUMBER] AND [manually_shiptrack].[RefNumber] = [ShipTrackWithLabel].[ID]
WHERE
	[manually_shiptrack].[PONumber] IS NOT NULL
	AND [manually_shiptrack].[RefNumber] IS NOT NULL
	AND 
        (
            [manually_shiptrack].[IsFullyInvoiced] = 'false' 
            OR [manually_shiptrack].[PONumber] IN (SELECT po FROM @Orders_to_restore)
        )
	AND
        (
            [manually_shiptrack].[IsManuallyClosed] = 'false'
            OR [manually_shiptrack].[PONumber] IN (SELECT po FROM @Orders_to_restore)
        )
	AND [manually_shiptrack].[SalesRepRef_FullName] != 'HDDC'
	AND [ShipTrackWithLabel].[CARRIERNAME] IS NULL
        ".$RefNumber_manually.";

--Get residential/comercial flags
INSERT INTO 
    @Special_Order 
select case when altdocid = '' then accountid else altdocid end  as [PO] from [SQLSERVER2].[integrator].[dbo].[ttcTxnTransactions] where partnerid='14' and TxnType='Special Order';

INSERT INTO 
    @Direct_Ship 
select case when altdocid = '' then accountid else altdocid end  as [PO] from [SQLSERVER2].[integrator].[dbo].[ttcTxnTransactions] where partnerid='14' and TxnType='Direct Ship';

--Get full data about each order
INSERT INTO
	@Orders_to_process_full_data
SELECT
	[otp].[TxnID],
	[otp].[PONumber],
	[otp].[RefNumber],
	case
		when len([otp].[ShipAddress_PostalCode])=4 then '0'+[otp].[ShipAddress_PostalCode] 
		else [otp].[ShipAddress_PostalCode]
	end as [ShipAddress_PostalCode],
	[otp].[ShipAddress_State],
	[otp].[CustomerRef_FullName] as [dealerName],
	[otp].[ShipMethodRef_FullName],
	[otp].[ShipAddress_City],
	[otp].[ShipAddress_Country],
	[otp].[ShipAddress_Addr1],
	[otp].[ShipAddress_Addr2],
	[otp].[memo],
	[groupdetail].[ItemGroupRef_FullName],
	[linedetail].[Rate] As [Rate],
	[linedetail].[quantity],					
	case
 		when cast(dbo.udf_GetNumeric([linedetail].[CustomField9]) as int)  > 150 then 1
 		when cast(dbo.udf_GetNumeric([linedetail].[CustomField9]) as int)  >= 132 and [linedetail].[ItemRef_FullName] like '%SHDR-19%' then 1
 		else 0 
	end as [liftgate],
	case
		when [Special_Order].[PO] IS NOT NULL then 'commercial'
		when [Direct_Ship].[PO] IS NOT NULL then 'residential'
		when [otp].[memo] like '%SPECIAL ORDER%' then 'commercial'
		when [otp].[memo] like '%DIRECT SHIP%' then 'residential'
 		else 'no'
	end as [lowes_mode],
	case
		when [otp].[CustomerRef_FullName] like 'lowe%' and [Special_Order].[PO] IS NOT NULL then '0'
		when LOWER([otp].[CustomerRef_FullName]) like 'lowe%' and [Direct_Ship].[PO] IS NOT NULL then '1'
		when LOWER([otp].[CustomerRef_FullName]) like 'lowe%' and [otp].[memo] like '%SPECIAL ORDER%' then '0'
		when LOWER([otp].[CustomerRef_FullName]) like 'lowe%' and [otp].[memo] like '%DIRECT SHIP%' then '1'
		when LOWER([otp].[CustomerRef_FullName]) like 'menards%' and ([otp].[ShipAddress_Addr1] like 'Menards store%') THEN '0'
		when LOWER([otp].[CustomerRef_FullName]) like 'amazon%' and ([otp].[ShipAddress_Addr1] like 'Amazon.com%') THEN '0'
		when LOWER([otp].[CustomerRef_FullName]) like 'your other warehouse%' and ([otp].[ShipAddress_Addr1] like 'HOME DEPOT%' or [otp].[ShipAddress_Addr1] like 'Home Depot%') THEN '0'
		when LOWER([otp].[CustomerRef_FullName]) like 'The Home Depot%' and ([otp].[ShipAddress_Addr2] like 'C/O THD Ship to Store%') THEN '0'
 		else 'no'
	end as [residential],
	SUM
	(
		case when [linedetail].[Quantity] is NULL then 1*cast(dbo.udf_GetNumeric([linedetail].[CustomField9]) as int) 
 			else  [linedetail].[Quantity]*cast(dbo.udf_GetNumeric([linedetail].[CustomField9]) as int) 
		end
	) over  (partition by [RefNumber])  +70 as [weight],
	SUM
	(
		case when [linedetail].[Quantity] is NULL then 1*cast(dbo.udf_GetNumeric([linedetail].[CustomField9]) as int) 
 			else  [linedetail].[Quantity]*cast(dbo.udf_GetNumeric([linedetail].[CustomField9]) as int) 
		end
	) over  (partition by [RefNumber])  +70 as [weight_inventory],
	'0' as [force_carrier]
FROM @Orders_to_process [otp]
	INNER JOIN [groupdetail] ON [otp].[TxnID] = [groupdetail].[IDKEY]
	INNER JOIN [linedetail] ON [groupdetail].[TxnLineID] = [linedetail].[GroupIDKEY]
	LEFT JOIN [orders31].[dbo].[iteminventory] ON [linedetail].[ItemRef_FullName] = [iteminventory].[FullName]
	LEFT JOIN @Special_Order [Special_Order] ON [Special_Order].[PO] = [otp].[PONumber]
	LEFT JOIN @Direct_Ship [Direct_Ship] ON [Direct_Ship].[PO] = [otp].[PONumber]
UNION
SELECT
	[otpmc].[TxnID],
	[otpmc].[PONumber],
	[otpmc].[RefNumber],
	case
		when len([otpmc].[ShipAddress_PostalCode])=4 then '0'+[otpmc].[ShipAddress_PostalCode] 
		else [otpmc].[ShipAddress_PostalCode]
	end as [ShipAddress_PostalCode],
	[otpmc].[ShipAddress_State],
	[otpmc].[CustomerRef_FullName] as [dealerName],
	[otpmc].[ShipMethodRef_FullName],
	[otpmc].[ShipAddress_City],
	[otpmc].[ShipAddress_Country],
	[otpmc].[ShipAddress_Addr1],
	[otpmc].[ShipAddress_Addr2],
	[otpmc].[memo],
	[manually_groupdetail].[ItemGroupRef_FullName],
	[manually_linedetail].[Rate] As [Rate],
	[manually_linedetail].[quantity],					
	case
 		when cast(dbo.udf_GetNumeric([manually_linedetail].[CustomField9]) as int)  > 150 then 1
 		when cast(dbo.udf_GetNumeric([manually_linedetail].[CustomField9]) as int)  >= 132 and [manually_linedetail].[ItemRef_FullName] like '%SHDR-19%' then 1
 		else 0 
	end as [liftgate],
	case
		when [Special_Order].[PO] IS NOT NULL then 'commercial'
		when [Direct_Ship].[PO] IS NOT NULL then 'residential'
		when [otpmc].[memo] like '%SPECIAL ORDER%' then 'commercial'
		when [otpmc].[memo] like '%DIRECT SHIP%' then 'residential'
 		else 'no'
	end as [lowes_mode],
	case
		when [otpmc].[CustomerRef_FullName] like 'lowe%' and [Special_Order].[PO] IS NOT NULL then '0'
		when LOWER([otpmc].[CustomerRef_FullName]) like 'lowe%' and [Direct_Ship].[PO] IS NOT NULL then '1'
		when LOWER([otpmc].[CustomerRef_FullName]) like 'lowe%' and [otpmc].[memo] like '%SPECIAL ORDER%' then '0'
		when LOWER([otpmc].[CustomerRef_FullName]) like 'lowe%' and [otpmc].[memo] like '%DIRECT SHIP%' then '1'
		when LOWER([otpmc].[CustomerRef_FullName]) like 'menards%' and ([otpmc].[ShipAddress_Addr1] like 'Menards store%') THEN '0'
		when LOWER([otpmc].[CustomerRef_FullName]) like 'amazon%' and ([otpmc].[ShipAddress_Addr1] like 'Amazon.com%') THEN '0'
		when LOWER([otpmc].[CustomerRef_FullName]) like 'your other warehouse%' and ([otpmc].[ShipAddress_Addr1] like 'HOME DEPOT%' or [otpmc].[ShipAddress_Addr1] like 'Home Depot%') THEN '0'
		when LOWER([otpmc].[CustomerRef_FullName]) like 'The Home Depot%' and ([otpmc].[ShipAddress_Addr2] like 'C/O THD Ship to Store%') THEN '0'
 		else 'no'
	end as [residential],
	SUM(
		case when [manually_linedetail].[Quantity] is NULL then 1*cast(dbo.udf_GetNumeric([manually_linedetail].[CustomField9]) as int) 
 			else  [manually_linedetail].[Quantity]*cast(dbo.udf_GetNumeric([manually_linedetail].[CustomField9]) as int) 
		end
	) over  (partition by [RefNumber])  +70 as [weight],
	SUM(
		case when [manually_linedetail].[Quantity] is NULL then 1*cast(dbo.udf_GetNumeric([manually_linedetail].[CustomField9]) as int) 
 			else  [manually_linedetail].[Quantity]*cast(dbo.udf_GetNumeric([manually_linedetail].[CustomField9]) as int) 
		end
	) over  (partition by [RefNumber])  +70 as [weight_inventory],
	0 as [force_carrier]
FROM @Orders_to_process_manually_created [otpmc]
	INNER JOIN [dbo].[manually_groupdetail] ON [otpmc].[TxnID] = [manually_groupdetail].[IDKEY]
	INNER JOIN [dbo].[manually_linedetail] ON [manually_groupdetail].[TxnLineID] = [manually_linedetail].[GroupIDKEY]
	LEFT JOIN [orders31].[dbo].[iteminventory] ON [manually_linedetail].[ItemRef_FullName] = [iteminventory].[FullName]
	LEFT JOIN @Special_Order [Special_Order] ON [Special_Order].[PO] = [otpmc].[PONumber]
	LEFT JOIN @Direct_Ship [Direct_Ship] ON [Direct_Ship].[PO] = [otpmc].[PONumber]
END
--Get final result
select 
	[RefNumber],
	[PONumber],
	[dealerName],
	[ShipAddress_State],
	[ShipAddress_City],
	[ShipAddress_Country],
	[ShipAddress_Addr1],
	[ShipAddress_Addr2],
	[ItemGroupRef_FullName],
	[ShipMethodRef_FullName],
	[ShipAddress_PostalCode], 
	case 
		when [weight] IS NULL AND [weight_inventory] IS NOT NULL then [weight_inventory]
		when [weight] IS NULL AND [weight_inventory] IS NULL then NULL
		else [weight]
	end
	as [weight],
	case
		when sum(liftgate) over  (partition by refnumber) > 0 then '1'
		else '0' 
	end as [liftgate],
	[Rate],
	[residential],
	[lowes_mode],
	[force_carrier]
FROM @Orders_to_process_full_data;";
/*print_r('<pre>');
print_r($query1);
print_r('</pre>');
die();*/

//Make sure that only one instance is running
$lastRun = getTime();

$startTime = $lastRun['start'];
$stopTime = $lastRun['stop'];
$nowTime = getNowTime();
$diffStartNow = $nowTime - $startTime;

if (empty($stopTime) && empty($startTime))
{
	$newStartTime = getNowTime();
	setTime($newStartTime, 0);
	$newStopTime = startMainScript();
	if ($newStopTime > $newStartTime)
	{
		setTime($newStartTime, $newStopTime);
	}
} else if ($stopTime != 0 && $stopTime > $startTime)
{
	$newStartTime = getNowTime();
	setTime($newStartTime, 0);
	$newStopTime = startMainScript();
	if ($newStopTime > $newStartTime)
	{
		setTime($newStartTime, $newStopTime);
	}
} else if ($stopTime == 0 && $diffStartNow > $delay)
{
	$newStartTime = getNowTime();
	setTime($newStartTime, 0);
	$newStopTime = startMainScript();
	if ($newStopTime > $newStartTime)
	{
		setTime($newStartTime, $newStopTime);
	}
} else if ($stopTime == 0 && $diffStartNow <= $delay)
{
	$waitForDelayMinutes = ceil(($delay - $diffStartNow));
	display_message('Please try again later (in 20 minutes).');
}

function validateUSAZip($zip_code)
{
  if(preg_match("/^([0-9]{5})(-[0-9]{4})?$/i",$zip_code))
    return true;
  else
    return false;
}

function validateCanadaZip($zip_code)
{
	$zip_code = str_replace(' ', '', $zip_code);
	$zip_code = str_replace('-', '', $zip_code);
 if(preg_match("/^([a-ceghj-npr-tv-z]){1}[0-9]{1}[a-ceghj-npr-tv-z]{1}[0-9]{1}[a-ceghj-npr-tv-z]{1}[0-9]{1}$/i",$zip_code))
    return true;
 else
    return false;
}

//Validate one order data
function validateInputData($conn, $PO, $ref, $related_products)
{
    display_message('Validating order PO: <a target="_blank" href="../shipping_module/order_status_history.php?po='.$PO.'">'.$PO.'</a>');
	$returnValue = false;
	$messages = Array();
	$warnings = Array();
	if (isset($PO) && !empty($PO))
	{
		$query = "SELECT 
		id,
		case 
			when item_weight IS NULL AND item_weight_inventory IS NOT NULL then item_weight_inventory
			when item_weight IS NULL AND item_weight_inventory IS NULL then NULL
			else item_weight
		end
		as item_weight,
		quantity,
		ShipAddress_PostalCode,
		FOB,
                [ShipAddress_Addr2]
FROM
(
SELECT 
		linedetail.id,
		linedetail.CustomField9 as item_weight,
		[iteminventory].CustomField9 as item_weight_inventory,
		linedetail.quantity,
		shiptrack.ShipAddress_PostalCode,
		shiptrack.FOB,
                [shiptrack].[ShipAddress_Addr2]
	 FROM [dbo].[linedetail]
	LEFT JOIN groupdetail ON linedetail.GroupIDKEY = groupdetail.TxnLineID
	LEFT JOIN shiptrack ON groupdetail.IDKEY = shiptrack.TxnID
	LEFT JOIN [".DATABASE31."].[dbo].[iteminventory] ON linedetail.[ItemRef_FullName] = [iteminventory].[FullName]
	  WHERE
			linedetail.ItemRef_FullName not like '%Price-Adjustment%'   and 
			linedetail.ItemRef_FullName not like '%Freight%'            and
			linedetail.ItemRef_FullName not like '%warranty%'           and        
			linedetail.ItemRef_FullName is not NULL                     and
			linedetail.ItemRef_FullName not like '%Subtotal%'           and
			linedetail.ItemRef_FullName not like '%IDSC-10%'            and
			linedetail.ItemRef_FullName not like '%DISCOUNT%'           and
			linedetail.ItemRef_FullName not like '%SPECIAL ORDER%'    	and 	
			linedetail.ItemRef_FullName not like '%Manual%'    	and 	
			linedetail.GroupIDKEY = groupdetail.TxnLineID  				and
			shiptrack.PONumber = '".$PO."'
			UNION ALL
			SELECT 
		manually_linedetail.id,
		manually_linedetail.CustomField9 as item_weight,
		[iteminventory].CustomField9 as item_weight_inventory,
		manually_linedetail.quantity,
		manually_shiptrack.ShipAddress_PostalCode,
		manually_shiptrack.FOB,
                [manually_shiptrack].[ShipAddress_Addr2]
	 FROM [dbo].[manually_linedetail]
	LEFT JOIN manually_groupdetail ON manually_linedetail.GroupIDKEY = manually_groupdetail.TxnLineID
	LEFT JOIN manually_shiptrack ON manually_groupdetail.IDKEY = manually_shiptrack.TxnID
	LEFT JOIN [".DATABASE31."].[dbo].[iteminventory] ON manually_linedetail.[ItemRef_FullName] = [iteminventory].[FullName]
	  WHERE
			manually_linedetail.ItemRef_FullName not like '%Price-Adjustment%'   and 
			manually_linedetail.ItemRef_FullName not like '%Freight%'            and
			manually_linedetail.ItemRef_FullName not like '%warranty%'           and        
			manually_linedetail.ItemRef_FullName is not NULL                     and
			manually_linedetail.ItemRef_FullName not like '%Subtotal%'           and
			manually_linedetail.ItemRef_FullName not like '%IDSC-10%'            and
			manually_linedetail.ItemRef_FullName not like '%DISCOUNT%'           and
			manually_linedetail.ItemRef_FullName not like '%SPECIAL ORDER%'    	and 	
			manually_linedetail.ItemRef_FullName not like '%Manual%'    	and 	
			manually_linedetail.GroupIDKEY = manually_groupdetail.TxnLineID  				and
			manually_shiptrack.PONumber = '".$PO."'
) result";
			
		/*print_r('<pre>');
		print_r($query);
		print_r('</pre>');*/
		
		$result = $conn->prepare($query);
		$result->execute();
		$result = $result->fetchAll(PDO::FETCH_ASSOC);
                
		
		$query_gr = "SELECT 
				[groupdetail].[ItemGroupRef_FullName],
				[groupdetail].[Quantity],
				[DL_valid_SKU].[SHIP_METHOD],
				[DL_valid_SKU].[SKU],
                                [DL_valid_SKU].[UPC]
			FROM [dbo].[groupdetail]
			LEFT JOIN [shiptrack] ON [groupdetail].[IDKEY] = [shiptrack].[TxnID]
			LEFT JOIN [DL_valid_SKU] ON [groupdetail].[ItemGroupRef_FullName] = [DL_valid_SKU].[QB_SKU]
			WHERE [shiptrack].[RefNumber] = '".$ref."'
			UNION ALL
			SELECT 
				[manually_groupdetail].[ItemGroupRef_FullName],
				[manually_groupdetail].[Quantity],
				[DL_valid_SKU].[SHIP_METHOD],
				[DL_valid_SKU].[SKU],
                                [DL_valid_SKU].[UPC]
			FROM [dbo].[manually_groupdetail]
			LEFT JOIN [manually_shiptrack] ON [manually_groupdetail].[IDKEY] = [manually_shiptrack].[TxnID]
			LEFT JOIN [DL_valid_SKU] ON [manually_groupdetail].[ItemGroupRef_FullName] = [DL_valid_SKU].[QB_SKU]
			WHERE [manually_shiptrack].[RefNumber] = '".$ref."'";
		$result_gr = $conn->prepare($query_gr);
		$result_gr->execute();
		$result_gr = $result_gr->fetchAll(PDO::FETCH_ASSOC);
		
		/*print_r('<pre>');
		print_r($result);
		print_r('</pre>');*/
		
		if (!isset($result) || empty($result) || !isset($result_gr) || empty($result_gr))
		{
			$order_status = false;
			display_message('Can not get data for validation. PO: '.$PO.'. <a target="_blank" href="../analyzer/index.php?error=10832&po='. urlencode($PO).'"><button>Validation error: 10832</button></a>');
			$messages[] = Array(
					'po' => $PO,
					'missing_field' => 'N/A',
					'reference_name' => 'shiptrack.RefNumber',
					'reference_value' => $ref,
					'message' => 'Can not get data for validation',
                                        'error_code' => '10832'
				);
		} else
		{
			$order_status = true;
			foreach($result as $item_weight)
			{
				if(!isset($item_weight['item_weight']) || (empty($item_weight['item_weight']) && $item_weight['item_weight'] != 0))
				{
					$messages[] = Array(
						'po' => $PO,
						'missing_field' => 'linedetail.CustomField9',
						'reference_name' => 'linedetail.id',
						'reference_value' => $item_weight['id'],
						'message' => 'Missing item weight'
					);
					$order_status = false;
				} else update_linedetail_weight($item_weight['id'], $item_weight['item_weight']);
				
				if(!isset($item_weight['quantity']) || empty($item_weight['quantity']) || $item_weight['quantity'] == 0)
				{
					$messages[] = Array(
						'po' => $PO,
						'missing_field' => 'linedetail.quantity',
						'reference_name' => 'linedetail.id',
						'reference_value' => $item_weight['id'],
						'message' => 'Missing item quantity or quantity is 0'
					);
					$order_status = false;
				}
			}
                        
                        if(empty($result['0']['ShipAddress_Addr2']))
				{
                                    /*print_r('<pre>');
                                    print_r($result);
                                    print_r($result['0']['ShipAddress_Addr2']);
                                    print_r('</pre>');*/
					$messages[] = Array(
						'po' => $PO,
						'missing_field' => 'shiptrack.ShipAddress_Addr2',
						'reference_name' => 'shiptrack.PONumber',
						'reference_value' => $PO,
						'message' => 'Missing ShipAddress_Addr2'
					);
					$order_status = false;
				}
                        
			if(!isset($result['0']['ShipAddress_PostalCode']) || empty($result['0']['ShipAddress_PostalCode']))
				{
					$messages[] = Array(
						'po' => $PO,
						'missing_field' => 'shiptrack.ShipAddress_PostalCode',
						'reference_name' => 'shiptrack.PONumber',
						'reference_value' => $PO,
						'message' => 'Missing postal (zip) code'
					);
					$order_status = false;
				} else{
					if(!validateUSAZip($result['0']['ShipAddress_PostalCode']) && !validateCanadaZip($result['0']['ShipAddress_PostalCode']))
					{
						$messages[] = Array(
							'po' => $PO,
							'missing_field' => 'shiptrack.ShipAddress_PostalCode',
							'reference_name' => 'shiptrack.PONumber',
							'reference_value' => $PO,
							'message' => 'Postal code is not valid'
						);
					}
				}
			
			if(!isset($result['0']['FOB']) || empty($result['0']['FOB']))
				{
					$messages[] = Array(
						'po' => $PO,
						'missing_field' => 'shiptrack.FOB',
						'reference_name' => 'shiptrack.PONumber',
						'reference_value' => $PO,
						'message' => 'Missing phone number'
					);
					$order_status = false;
				} else{
					$FOB = $result['0']['FOB'];
					$FOB = preg_replace("/[^0-9]/","",$FOB);
					if (strlen($FOB) != 10)
					{
						$messages[] = Array(
							'po' => $PO,
							'missing_field' => 'shiptrack.FOB',
							'reference_name' => 'shiptrack.PONumber',
							'reference_value' => $PO,
							'message' => 'Phone number is not valid'
						);
					}
				}
			
                                /*print_r('<pre>');
                                print_r($result_gr);
                                print_r('</pre>');*/
			foreach ($result_gr as $res)
			{
				if (empty($res['SHIP_METHOD']))
				{
					$messages[] = Array(
						'po' => $PO,
						'missing_field' => 'DL_valid_SKU.SHIP_METHOD',
						'reference_name' => 'DL_valid_SKU.QB_SKU',
						'reference_value' => $res['ItemGroupRef_FullName'],
						'message' => 'Missing SHIP_METHOD'
					);
					$order_status = false;
				}
                                
                                if (empty($res['Quantity']))
				{
					$messages[] = Array(
						'po' => $PO,
						'missing_field' => 'DL_valid_SKU.Quantity',
						'reference_name' => 'DL_valid_SKU.QB_SKU',
						'reference_value' => $res['ItemGroupRef_FullName'],
						'message' => 'Missing group Quantity'
					);
					$order_status = false;
				}
                                
                                if (empty($res['UPC']))
				{
					$messages[] = Array(
						'po' => $PO,
						'missing_field' => 'DL_valid_SKU.Quantity',
						'reference_name' => 'DL_valid_SKU.QB_SKU',
						'reference_value' => $res['ItemGroupRef_FullName'],
						'message' => 'Missing UPC'
					);
					$order_status = false;
				}
			}
			
			$SKUs_in_order = Array();
			foreach($result_gr as $gr)
			{
				if (!empty($gr['SKU']))
				{
					$SKUs_in_order[]= $gr['SKU'];
				}
			}
			if(count($SKUs_in_order) > 1)
			{
				$product_status = true;
				foreach($SKUs_in_order as $key_sku => $sku)
				{
					foreach($related_products as $rel)
					{
						if($sku == $rel['product'])
						{
							$SKUs_in_order_rel = array_diff($SKUs_in_order, array($sku));
							$diff = array_diff($SKUs_in_order_rel, $rel['related']);
							if(!empty($diff))
							{
								$product_status = false;
								//$diff = implode(',', $diff);
							}
						}
					}
				}
				if(!$product_status)
				$warnings[] = Array(
									'PONumber' => $PO,
									'RefNumber' => $ref,
									'Warning' => 'This order may contain incompatible products!'
								);
			}
		}
	} else
	{
		$messages[] = Array(
					'po' => 'N/A',
					'missing_field' => 'shiptrack.PONumber',
					'reference_name' => 'shiptrack.RefNumber',
					'reference_value' => $ref,
					'message' => 'Missing PO number'
				);
	}
	
	if ($order_status) $returnValue = true; else
	{
		saveToValidationLog($conn, $messages);
	}
	
	if (!empty($warnings))
	{
		saveToWarningsLog($conn, $warnings);
	}
	
	return $returnValue;
}

//Save warnings
function saveToWarningsLog($conn, $warnings)
{
	foreach($warnings as $warning)
	{
		$query = "EXECUTE shipsingle_insert_to_warnings_log @PONumber = '".$warning['PONumber']."', @RefNumber = '".$warning['RefNumber']."', @Warning = '".$warning['Warning']."'";
		$result = runQuery($conn, $query);
		if (!$result)
		{
			display_message('Can not save warning to log.');
			print_r($warning);
		}
	}
}

//Save to DB message about invalid data
function saveToValidationLog($conn, $messages)
{
	foreach($messages as $message)
	{
            display_message($message['message']);
            if(!isset($message['error_code'])) $message['error_code'] = '';
            
		$query = "EXECUTE shipsingle_insert_to_validation_log @po = '".$message['po']."', @missing_field = '".$message['missing_field']."', @reference_name = '".$message['reference_name']."', @reference_value = '".$message['reference_value']."', @message = '".$message['message']."', @error_code = '".$message['error_code']."'";
		$result = runQuery($conn, $query);
		if (!$result)
		{
			display_message('Can not save message to log.');
			print_r($message);
		}
	}
}

function get_related_products()
{
	$related_products = Array();
	$array = parseCsvFile('files/FPTCSV_V2.csv');
	
	foreach($array as $key => $row)
	{
		if ($key != 0 && !empty($row[0]) && !empty($row[1]))
		{
			$product = $row[0];
			$related = explode('|', $row[1]);
			$related_products[]= array('product' => $product, 'related' => $related);
		}
	}
	
	/*print_r('<pre>');
	print_r($related_products);
	print_r('</pre>');*/
	
	return $related_products;
}

function parseCsvFile($fileName)
{
	$csvData = file_get_contents($fileName);
	$lines = explode("\r\n", $csvData);
	$array = array();
	foreach ($lines as $line) {
		$array[] = str_getcsv($line, ',');
	}
	return $array;
}

function display_message($value)
{
	$date = date("h:i:s");
	echo '<span style="color:red;">'.$date.': </span><span style="color:black; font-weight: bold;">'.$value.'</span><br>';
	$query = "
	INSERT INTO [dbo].[shipsingle_message_log]
           ([message]
           ,[Date])
     VALUES
           ('".$value."'
           ,GETDATE())";
	//print_r($query);
	$conn = getConnection();
	runQuery($conn, $query);
	ob_flush();
	flush();
}

//Main function
function startShipsingle()
{
	display_message('Script started.');
	$conn = getConnection();
	global $query1, $startTime, $remote_orders, $remote_orders_dealer_name, $originZip, $originStatecode, $originCountry, $originCity, $FreightClass, $rl_pal_addon_cost_liftgate, $fedex_addon_cost_liftgate, $rl_pal_addon_cost_residential, $ziphead_not_allowed_RL, $PrintCarrierForAmazonMode, $shipping_ground_weight_max, $rate_ups; //make variables visible for this func
	 $shipperModel = new ShippingModel(); //create shipper object and get orders data
	 $ordersSM = $shipperModel->getInProcessOrders($query1);
	 
	/*print_r('<pre>');
	print_r($ordersSM);
	print_r('</pre>');
	die('stop');*/
	 
	 //divide orders on groups
	 $ordersSM_HD_M_YOW = array();
	 $ordersSears = array();
	 $ordersWayfair = array();
	 $ordersAmazonDS = array();
	 $ordersAmazon = array();
	 $ordersFerguson = array();
	 $manual_orders = array();
	 
	 if ($ordersSM != false && isset($ordersSM) && !empty($ordersSM))
	 {
		display_message("Total orders to be processed : ".count($ordersSM));
		 foreach ($ordersSM as $key => $value)
		 {
			if (isset($value['force_carrier']) && !empty($value['force_carrier']) && $value['force_carrier'] == 1)
			{
				$manual_orders[$key] = $value;
				unset($ordersSM[$key]);
			} else if (extrStore($value['dealerName']) == 'Depot' || extrStore($value['dealerName']) == 'YOW')
			{
				$ordersSM_HD_M_YOW[$key] = $value;
				unset($ordersSM[$key]);
			} else if (extrStore($value['dealerName']) == 'Sears')
			{
				$ordersSears[$key] = $value;
				unset($ordersSM[$key]);
			} else if (extrStore($value['dealerName']) == 'Wayfair')
			{
				$ordersWayfair[$key] = $value;
				unset($ordersSM[$key]);
			} else if ($value['dealerName'] == 'AmazonDS')
			{
				$ordersAmazonDS[$key] = $value;
				unset($ordersSM[$key]);
			} else if ($value['dealerName'] == 'Amazon' && ($PrintCarrierForAmazonMode == '1' || $PrintCarrierForAmazonMode == '2'))
			{
				$ordersAmazon[$key] = $value;
				unset($ordersSM[$key]);
			} else if (extrStore($value['dealerName']) == 'Ferguson')
			{
				$ordersFerguson[$key] = $value;
				unset($ordersSM[$key]);
			}
		 }
	 } else 
	 {
		setTime($startTime, getNowTime());
		die("<br>Total orders to be processed : 0<br>");
	 }
	 
	 $related_products = get_related_products();
	 /*print_r('<pre>');
	 print_r($related_products);
	 print_r('</pre>');*/
	 
	 $numWrite = 0;
	 $numUpd = 0;
	 $numCalc = 0;
	 
	 //process each group of orders
	if ($manual_orders != false && isset($manual_orders) && !empty($manual_orders))
	{
		
		foreach ($manual_orders as $key => $oSM)
		{
			$validation_result = validateInputData($conn, $oSM['PONumber'], $oSM['RefNumber'], $related_products);
			if ($validation_result == false)
			{
				display_message('Order will not be processed due to validation issue. PO: <a target="_blank" href="../shipping_module/order_status_history.php?po='.$oSM['PONumber'].'">'.$oSM['PONumber'].'</a>');
			} else
			{
				display_message('<br>Processing order PO: <a target="_blank" href="../shipping_module/order_status_history.php?po='.$oSM['PONumber'].'">'.$oSM['PONumber'].'</a>');
				$oSM=getResidentialOneOrder($conn, $oSM, $key);
                                if($oSM != false)
                                {
                                    writeToShipTrackWithLabel($conn, $oSM);
                                    $numWrite++;
                                    $ShipMethodRef_FullName = str_replace(' - PROC', '', $oSM['ShipMethodRef_FullName']);
                                    updateToShipTrackWithLabel($conn, $oSM['RefNumber'], $ShipMethodRef_FullName, 'N/A', 'N/A', 'N/A', $oSM['dealerName'], true);
                                    $numUpd++;
                                    display_message("Done");
                                } else
                                {
                                    display_message('Fedex Address Validation Service Failed.');
                                }
			}
			
		}
	}
	
	 if ($ordersWayfair != false && isset($ordersWayfair) && !empty($ordersWayfair))
	{	
		
		foreach ($ordersWayfair as $key => $oSM)
		{
			$validation_result = validateInputData($conn, $oSM['PONumber'], $oSM['RefNumber'], $related_products);
			if ($validation_result == false)
			{
				display_message('Order will not be processed due to validation issue. PO: <a target="_blank" href="../shipping_module/order_status_history.php?po='.$oSM['PONumber'].'">'.$oSM['PONumber'].'</a>');
			} else
			{
				display_message('Processing order PO: <a target="_blank" href="../shipping_module/order_status_history.php?po='.$oSM['PONumber'].'">'.$oSM['PONumber'].'</a>');
				$oSM=getResidentialOneOrder($conn, $oSM, $key);
				if($oSM != false)
                                {
                                    writeToShipTrackWithLabel($conn, $oSM);
                                    $numWrite++;
                                    $ShipMethodRef_FullName = str_replace(' - PROC', '', $oSM['ShipMethodRef_FullName']);
                                    updateToShipTrackWithLabel($conn, $oSM['RefNumber'], $ShipMethodRef_FullName, 'N/A', 'N/A', '0', $oSM['dealerName'], true);
                                    $numUpd++;
                                    display_message("Done");
                                } else
                                {
                                    display_message('Fedex Address Validation Service Failed.');
                                }
			}
		}
	}
	
	if ($ordersFerguson != false && isset($ordersFerguson) && !empty($ordersFerguson))
	{	
		
		foreach ($ordersFerguson as $key => $oSM)
		{
			$validation_result = validateInputData($conn, $oSM['PONumber'], $oSM['RefNumber'], $related_products);
			if ($validation_result == false)
			{
				display_message('Order will not be processed due to validation issue. PO: <a target="_blank" href="../shipping_module/order_status_history.php?po='.$oSM['PONumber'].'">'.$oSM['PONumber'].'</a>');
			} else
			{
				display_message('Processing order PO: <a target="_blank" href="../shipping_module/order_status_history.php?po='.$oSM['PONumber'].'">'.$oSM['PONumber'].'</a>');
				$oSM=getResidentialOneOrder($conn, $oSM, $key);
                                if($oSM != false)
                                {
                                    writeToShipTrackWithLabel($conn, $oSM);
                                    $numWrite++;
                                    $ShipMethodRef_FullName = str_replace(' - PROC', '', $oSM['ShipMethodRef_FullName']);
                                    updateToShipTrackWithLabel($conn, $oSM['RefNumber'], /*$ShipMethodRef_FullName*/'FEDEX-ECONOMY', 'N/A', 'N/A', '0', $oSM['dealerName'], true);
                                    $numUpd++;
                                    display_message("Done");
                                } else
                                {
                                    display_message('Fedex Address Validation Service Failed.');
                                }
			}
		}
	}
	 
	 if ($ordersAmazon != false && isset($ordersAmazon) && !empty($ordersAmazon))
	{	
		
		foreach ($ordersAmazon as $key => $oSM)
		{
			$validation_result = validateInputData($conn, $oSM['PONumber'], $oSM['RefNumber'], $related_products);
			if ($validation_result == false)
			{
				display_message('Order will not be processed due to validation issue. PO: <a target="_blank" href="../shipping_module/order_status_history.php?po='.$oSM['PONumber'].'">'.$oSM['PONumber'].'</a>');
			} else
			{
				display_message('Processing order PO: <a target="_blank" href="../shipping_module/order_status_history.php?po='.$oSM['PONumber'].'">'.$oSM['PONumber'].'</a>');
				$oSM=getResidentialOneOrder($conn, $oSM, $key);
                                if($oSM != false)
                                {
                                    writeToShipTrackWithLabel($conn, $oSM);
                                    $numWrite++;
                                    $ShipMethodRef_FullName = str_replace(' - PROC', '', $oSM['ShipMethodRef_FullName']);
                                    updateToShipTrackWithLabel($conn, $oSM['RefNumber'], $ShipMethodRef_FullName, 'N/A', 'N/A', '0', $oSM['dealerName'], true);
                                    $numUpd++;
                                    display_message("Done");
                                } else
                                {
                                    display_message('Fedex Address Validation Service Failed.');
                                }
			}
		}
	}
	 
	  if ($ordersAmazonDS != false && isset($ordersAmazonDS) && !empty($ordersAmazonDS))
	{	
		
		foreach ($ordersAmazonDS as $key => $oSM)
		{
			$validation_result = validateInputData($conn, $oSM['PONumber'], $oSM['RefNumber'], $related_products);
			if ($validation_result == false)
			{
				display_message('Order will not be processed due to validation issue. PO: <a target="_blank" href="../shipping_module/order_status_history.php?po='.$oSM['PONumber'].'">'.$oSM['PONumber'].'</a>');
			} else
			{
				display_message('Processing order PO: <a target="_blank" href="../shipping_module/order_status_history.php?po='.$oSM['PONumber'].'">'.$oSM['PONumber'].'</a>');
				$oSM=getResidentialOneOrder($conn, $oSM, $key);
                                if($oSM != false)
                                {
                                    writeToShipTrackWithLabel($conn, $oSM);
                                    $numWrite++;
                                    $ShipMethodRef_FullName = str_replace(' - PROC', '', $oSM['ShipMethodRef_FullName']);
                                    updateToShipTrackWithLabel($conn, $oSM['RefNumber'], $ShipMethodRef_FullName, 'N/A', 'N/A', '0', $oSM['dealerName'], true);
                                    $numUpd++;
                                    display_message("Done");
                                } else
                                {
                                    display_message('Fedex Address Validation Service Failed.');
                                }
			}
		}
	}
	 
	 if ($ordersSM_HD_M_YOW != false && isset($ordersSM_HD_M_YOW) && !empty($ordersSM_HD_M_YOW))
	{	
		
		foreach ($ordersSM_HD_M_YOW as $key => $oSM)
		{
			$validation_result = validateInputData($conn, $oSM['PONumber'], $oSM['RefNumber'], $related_products);
			if ($validation_result == false)
			{
				display_message('Order will not be processed due to validation issue. PO: <a target="_blank" href="../shipping_module/order_status_history.php?po='.$oSM['PONumber'].'">'.$oSM['PONumber'].'</a>');
			} else
			{
				display_message('Processing order PO: <a target="_blank" href="../shipping_module/order_status_history.php?po='.$oSM['PONumber'].'">'.$oSM['PONumber'].'</a>');
				$oSM=getResidentialOneOrder($conn, $oSM, $key);
                                if($oSM != false)
                                {
                                    writeToShipTrackWithLabel($conn, $oSM);
                                    $numWrite++;
                                    $ShipMethodRef_FullName = str_replace(' - PROC', '', $oSM['ShipMethodRef_FullName']);
                                    updateToShipTrackWithLabel($conn, $oSM['RefNumber'], $ShipMethodRef_FullName, 'N/A', 'N/A', '0', $oSM['dealerName'], true);
                                    $numUpd++;
                                    display_message("Done");
                                } else
                                {
                                    display_message('Fedex Address Validation Service Failed.');
                                }
			}
		}
	}

	if ($ordersSears != false && isset($ordersSears) && !empty($ordersSears))
	{	
		
		foreach ($ordersSears as $key => $oSM)
		{
			$validation_result = validateInputData($conn, $oSM['PONumber'], $oSM['RefNumber'], $related_products);
			if ($validation_result == false)
			{
				display_message('Order will not be processed due to validation issue. PO: <a target="_blank" href="../shipping_module/order_status_history.php?po='.$oSM['PONumber'].'">'.$oSM['PONumber'].'</a>');
			} else
			{
				display_message('Processing order PO: <a target="_blank" href="../shipping_module/order_status_history.php?po='.$oSM['PONumber'].'">'.$oSM['PONumber'].'</a>');
				$oSM=getResidentialOneOrder($conn, $oSM, $key);
                                if($oSM != false)
                                {
                                    writeToShipTrackWithLabel($conn, $oSM);
                                    $numWrite++;
                                    $is_ground_shippable = isOrderGroundShippable($conn, $oSM, extrStore($oSM['dealerName']), $shipping_ground_weight_max);
                                    if ($is_ground_shippable == 'ltl')
                                    {
                                                    updateToShipTrackWithLabel($conn, $oSM['RefNumber'], 'LTL', 'N/A', 'N/A', '0', $oSM['dealerName'], true);
                                                    $numUpd++;
                                    } else if ($is_ground_shippable == 'gr') {
                                            updateToShipTrackWithLabel($conn, $oSM['RefNumber'], 'Shipping Ground', 'N/A', 'N/A', '0', $oSM['dealerName'], true);
                                            $numUpd++;
                                    } else if ($is_ground_shippable == 'error')
                                    {
                                            display_message('Can not detect Shipping Ground. PO: <a target="_blank" href="../shipping_module/order_status_history.php?po='.$oSM['PONumber'].'">'.$oSM['PONumber'].'</a>');
                                    }
                                    display_message("Done");
                                } else
                                {
                                    display_message('Fedex Address Validation Service Failed.');
                                }
			}
		}
	}
	
	if ($ordersSM != false && isset($ordersSM) && !empty($ordersSM))
	{
		foreach ($ordersSM as $key => $oSM)
		{
			$validation_result = validateInputData($conn, $oSM['PONumber'], $oSM['RefNumber'], $related_products);
			if ($validation_result == false)
			{
				display_message('Order will not be processed due to validation issue. PO: <a target="_blank" href="../shipping_module/order_status_history.php?po='.$oSM['PONumber'].'">'.$oSM['PONumber'].'</a>');
			} else
			{
				display_message('Processing order PO: <a target="_blank" href="../shipping_module/order_status_history.php?po='.$oSM['PONumber'].'">'.$oSM['PONumber'].'</a>');
				$oSM=getResidentialOneOrder($conn, $oSM, $key);
				if($oSM != false)
                                {
                                    $is_ground_shippable = isOrderGroundShippable($conn, $oSM, extrStore($oSM['dealerName']), $shipping_ground_weight_max);
                                    /*if(strpos(strtoupper($oSM['dealerName']), 'MENARDS')  !== false && $is_ground_shippable == 'ltl')
                                    {
                                        $oSM['liftgate'] = '1';
                                    }*/

                                    writeToShipTrackWithLabel($conn, $oSM);
                                    $numWrite++;
                                    if ($is_ground_shippable == 'ltl')
                                    {
                                            if (isDistantOrder($oSM, $remote_orders, $remote_orders_dealer_name))
                                            {
                                                    updateToShipTrackWithLabel($conn, $oSM['RefNumber'], '3rd party Conway', '3rd party', 'N/A', '0', $oSM['dealerName'], false);
                                                    $numUpd++;
                                            } else if (isDistantOrderUnknown($oSM, $remote_orders, $remote_orders_dealer_name))
                                            {
                                                    updateToShipTrackWithLabel($conn, $oSM['RefNumber'], 'N/A*('.$oSM['ShipAddress_State'].')', 'N/A*', 'N/A*', 'N/A*', $oSM['dealerName'], false);
                                                    $numUpd++;
                                            } else if (isCanadaState($oSM['ShipAddress_State'], $oSM['ShipAddress_Country']) || isHI_AL($oSM['ShipAddress_State']))
                                            {
                                                    updateToShipTrackWithLabel($conn, $oSM['RefNumber'], 'N/A', 'N/A', 'N/A', '0', $oSM['dealerName'], false);
                                                    $numUpd++;
                                            } else
                                            {
                                                    $cheaporder = bigFunc($oSM, $conn, $originZip, $originStatecode, $originCountry, $originCity, $FreightClass, $rl_pal_addon_cost_liftgate, $rl_pal_addon_cost_residential, $fedex_addon_cost_liftgate, $ziphead_not_allowed_RL, $rate_ups);

                                                    $cheaporder = $cheaporder['cheaporder'];

                                                    if (isset($cheaporder) && !empty($cheaporder))
                                                    {
                                                            $numCalc++;
                                                            $retVal = parseCheaporder($conn, $cheaporder);
                                                            if ($retVal) $numUpd++; else display_message("Can not get rate for this order");
                                                    }
                                            }
                                    } else if ($is_ground_shippable == 'gr'){
                                            updateToShipTrackWithLabel($conn, $oSM['RefNumber'], 'Shipping Ground', 'N/A', 'N/A', '0', $oSM['dealerName'], true);
                                            $numUpd++;
                                    } else if ($is_ground_shippable == 'error')
                                    {
                                            display_message('Can not detect Shipping Ground. PO: <a target="_blank" href="../shipping_module/order_status_history.php?po='.$oSM['PONumber'].'">'.$oSM['PONumber'].'</a>');
                                    }
                                    display_message("Done");
                                } else
                                {
                                    display_message('Fedex Address Validation Service Failed.');
                                }
			}
		}
	}

	display_message("Orders created: ".$numWrite);
	display_message("Orders updated: ".$numUpd);
	display_message('Script finished.');
	setTime($startTime, getNowTime());
}

?>