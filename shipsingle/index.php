<?php
$header_name = "Quick Shipping Quote";
require_once( "../shipping_module/header.php" );
?>
<?php
if ($auth->isAuth()) {    ?>
  <title><?php echo $header_name; ?> </title>
  <link rel="stylesheet" href="Include/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="Include/bootstrap/css/bootstrap-theme.min.css">

  <script src="Include/bootstrap-validator-master/js/validator.js"></script>
  <script>
	$(document).ready(function() {
		$('#weight').change(function()
		{
			var weightChanged = $('#weight').val();
			if (weightChanged > 140)
			{
				$('#lift').prop('checked', true);
			} else
			{
				$('#lift').prop('checked', false);
			}
		});
	   $('#orderForm').validator().on('submit', function (e) {
	  if (e.isDefaultPrevented()) {
		console.log('Invalid form.');
	  } else {
		e.preventDefault();
		console.log('Valid form.');
		var $form = $(e.target);

            // Use Ajax to submit form data
            $.ajax({
                url: $form.attr('action'),
                type: 'GET',
                data: $form.serialize(),
				beforeSend : function(){ showModal(); loading(); },
                complete: function(result) {
                    console.log(result.responseText);
					try{
						result_array = JSON.parse(result.responseText);
						
						if( Object.prototype.toString.call(result_array) === "[object Array]" && Object.prototype.toString.call(result_array[0]) === "[object Object]" ) {
							make_report(result_array);
						} else
						{
							show_error(result.responseText);
						}

					}catch(e){
						show_error(result.responseText);
					}
                }
            });
	  }
	});
});

function compare_results( a, b ) {
  if ( a.cost < b.cost || (a.cost == b.cost && a.time < b.time) ) {
    return -1;
  }
  if ( a.cost > b.cost || (a.cost == b.cost && a.time > b.time) ) {
    return 1;
  }
  return 0;
}

function make_report(result_array)
{       
        result_array.sort( compare_results );
        
	var html = "<center><h3>Best result</h3><table class='table table-bordered'><tr><th>LOGO</th><th>CARRIER</th><th>QUOTE #</th><th>TRANSIT TIME</th><th>COST</th><th>DATE</th></tr>";
        
        for( var i=0; i < result_array.length; i++ ) {
                
                var robj = result_array[i];                
                html += "<tr class='info' "+(i==0?"id='tr-best'":'')+"><td><img src='"+(robj.logo)+"' height='40px'></td><td>"+(robj.carrier)+"</td><td>"+(robj.quote)+"</td><td>"+(robj.time)+"</td><td>"+(robj.cost)+"</td><td>"+(robj.date)+"</td></tr>";
        }
        
        html += "</table><button class='btn btn-primary' onclick='closePopup()'>Get another quote</button></center>";
	$('#result').html(html);
}

function show_error(error)
{
	$('#result').html(error);
}

function loading()
{
	$('#result').html("<img id='loading' src='Include/Images/loading.gif' alt='loading...'>");
}

function closePopup()
{
	$('#modalBackground').fadeOut();
}

function showModal()
{
	$('#modalBackground').fadeIn();
}
  </script>
  <style>
	body{
		height: 100%;
		width: 100%;
	}
	#popup{
		position: fixed;
		width: 800px;
		min-height: 300px;
		/*overflow-y: auto;*/
		left: 50%;
		top: 20%;
		background: #fff;
		margin-left: -400px;
		z-index: 1000;
		padding: 20px;
		border-radius: 10px;
	}
	
	#close{
		position: absolute;
		right: 10px;
		top: 10px;
		font-weight: bold;
		cursor: pointer;
	}
	
	#result
	{
		width: 100%;
		height: 100%;
	}
	
	#modalBackground{
		display: none;
		position: fixed;
		left: 0px;
		top: 0px;
		width: 100%;
		height: 100%;
		background: rgba(0, 0, 0, 0.7);
		z-index: 999;
	}
	
	#loading
	{
		position: absolute;
		left: 50%;
		top: 50%;
		margin-left: -32px;
		margin-top: -32px;
	}

	#tr-best>td
	{
		background-color: #dfe !important;
	}
  </style>
</head>
<body>
<?php require_once($path.'/Include/header_section.php');?>
<div class="container">
	<form data-toggle="validator" role="form" id="orderForm" action="ShippingQ.php" method="GET">
	
	<div class="group">
		<h2>Quick Shipping Quote</h2>
		
		<div class="form-group">
			<label for="dealerName">Ship To Customer Name</label>
			<input type="text" class="form-control" id="dealerName" name="dealerName" placeholder="John Smith" data-minlength="3"  required>
			<span class="help-block with-errors">Person or Dealer name to ship to</span>
		</div>
		
		<div class="form-group">
			<label for="fromZip">Origin Zip</label>
			<input type="text" class="form-control" id="fromZip" name="fromZip" placeholder="18974" pattern="^(\d{5}(-\d{4})?|[A-CEGHJ-NPRSTVXY]\d[A-CEGHJ-NPRSTV-Z] ?\d[A-CEGHJ-NPRSTV-Z]\d)$" value="18974" required>
			<span class="help-block with-errors">USA or Canada postal code</span>
		</div>
		
		<div class="form-group">
			<label for="toZip">Destination Zip</label>
			<input type="text" class="form-control" id="toZip" name="toZip" placeholder="42906" pattern="^(\d{5}(-\d{4})?|[A-CEGHJ-NPRSTVXY]\d[A-CEGHJ-NPRSTV-Z] ?\d[A-CEGHJ-NPRSTV-Z]\d)$" required>
			<span class="help-block with-errors">USA or Canada postal code</span>
		</div>
		
		<div class="form-group">
			<label for="weight">Weight</label>
			<input type="number" class="form-control" id="weight" name="weight" placeholder="170" min="1" max="9999" required>
			<span class="help-block with-errors">Full order weight. Number from 1 to 9999</span>
		</div>
		
		<div class="form-group">
			<label for="class">Item Class</label>
			<select id="class" name="class" class="form-control">
				<option value="85">85</option>
				<option value="100">100</option>
				<option value="125">125</option>
				<option value="250">250</option>
				<option value="77">77</option>
				<option value="70">70</option>
				<option value="60">60</option>
				<option value="55">55</option>
			</select>
			<span class="help-block with-errors">Choose class from the list</span>
		</div>
		
		<div class="form-group">
			<label class="checkbox-inline"><input type="checkbox" id="lift" name="lift" value="1">Liftgate</label>
			<span class="help-block with-errors">Check if Liftgate</span>
		</div>
		
		<div class="form-group">
			<label class="checkbox-inline"><input type="checkbox" name="resi" value="1">Residential</label>
			<span class="help-block with-errors">Check if Residential</span>
		</div>
		
		<div class="form-group">
			<label>Carriers to use</label><br>
			<!--<label class="checkbox-inline"><input type="checkbox" name="yrc_pal" value="YELLOW-PAL" checked>YELLOW - PAL</label><br>-->
			<!--<label class="checkbox-inline"><input type="checkbox" name="yrc" value="YRC" checked>YELLOW</label><br>-->
			<!--<label class="checkbox-inline"><input type="checkbox" name="odfl_local" value="ODFL-Local" checked>ODFL-Local</label><br>-->
			<!--<label class="checkbox-inline"><input type="checkbox" name="odfl" value="ODFL" checked>ODFL</label><br>-->
			<!--<label class="checkbox-inline"><input type="checkbox" name="rl" value="RL" checked>RL</label><br>-->
			<label class="checkbox-inline"><input type="checkbox" name="rl" value="RL" checked>RL</label><br>
			<!--<label class="checkbox-inline"><input type="checkbox" name="fedex_priority" value="FEDEX-PRIORITY" checked>FEDEX-PRIORITY</label><br>-->
			<label class="checkbox-inline"><input type="checkbox" name="fedex" value="FEDEX" checked>FEDEX</label><br>
			<label class="checkbox-inline"><input type="checkbox" name="ups" value="UPS" checked>UPS</label><br>
			<span class="help-block with-errors">Deselect carriers if needed</span>
		</div>

	</div>
	
	<button type="submit" class="btn btn-success">Submit</button>
	<button type="reset" class="btn btn-default">Reset</button><br><br>
	</form><br>
</div>

<div id="modalBackground">
	<div id="popup">
		<span id="close" onclick="closePopup()">X</span>
		<div id="result">
		</div>
	</div>
</div>


<?php
}
?>
<?php require_once($path.'/Include/footer_section.php');?>
