<?php
error_reporting(E_ERROR);

require_once('helpers/fedexAddressValidation.php');
require_once('shipsingleBigFunc.php');
require_once('functions.php');
$path = $_SERVER['DOCUMENT_ROOT'];
require_once($path.'/db_connect/connect.php');

$conn = Database::getInstance()->dbc;

$settings = getSettings($conn);

$originZip = false;
$originStatecode = false;
$originCountry = false;
$originCity = false;
$ziphead_not_allowed_RL = array();
$rl_pal_addon_cost_liftgate = 0;
$rl_pal_addon_cost_residential = 0;
$fedex_addon_cost_liftgate = 0;
$rate_ups = false;

foreach ($settings as $setting)
{
	if ($setting['group_name'] == 'origin_address' && $setting['type_name'] == 'zip')
	{
		$originZip=$setting['setting_value'];
	} else if ($setting['group_name'] == 'UPS' && $setting['type_name'] == 'rate')
	{
		$rate_ups=$setting['setting_value'];
	} else if ($setting['group_name'] == 'origin_address' && $setting['type_name'] == 'statecode')
	{
		$originStatecode=$setting['setting_value'];
	} else if ($setting['group_name'] == 'origin_address' && $setting['type_name'] == 'country')
	{
		$originCountry=$setting['setting_value'];
	} else if ($setting['group_name'] == 'origin_address' && $setting['type_name'] == 'city')
	{
		$originCity=$setting['setting_value'];
	} else if ($setting['group_name'] == 'rl_pal_addon_cost' && $setting['type_name'] == 'liftgate')
	{
		$rl_pal_addon_cost_liftgate=$setting['setting_value'];
	} else if ($setting['group_name'] == 'rl_pal_addon_cost' && $setting['type_name'] == 'residential')
	{
		$rl_pal_addon_cost_residential=$setting['setting_value'];
	} else if ($setting['group_name'] == 'ziphead_not_allowed' && $setting['type_name'] == 'RL')
	{
		$ziphead_not_allowed_RL[]=$setting['setting_value'];
	} else if ($setting['group_name'] == 'fedex_addon_cost' && $setting['type_name'] == 'liftgate')
	{
		$fedex_addon_cost_liftgate=$setting['setting_value'];
	}
}

$order = getData();
/*
print_r('<pre>');
print_r($order);
print_r('</pre>');
die();
*/
if ($order != false) startShipsingle($order); else die('Can not get data!');

function getData()
{
	$order = false;
	if (
		isset($_GET['dealerName'])
		&& isset($_GET['toZip'])
		&& isset($_GET['weight'])
		&& isset($_GET['class'])
	)
	{
		if (empty($_GET['lift'])) $lift = '0'; else $lift = '1';
		if (empty($_GET['resi'])) $resi = '0'; else $resi = '1';
		
		$lowes_mode = getLowesMode($_GET['dealerName'], $resi);
		$order = prepareData($_GET['dealerName'], $_GET['toZip'], $_GET['weight'], $_GET['class'], $lift, $resi, $lowes_mode);
		//print_r($order);
	}
	return $order;
}

function getLowesMode($dealerName, $resi)
{
	$returnVal = 'no';
	if (extrStore($dealerName) == 'Lowe' && $resi == '1')
	{
		$returnVal = 'residential';
	} else if (extrStore($dealerName) == 'Lowe' && $resi == '0')
	{
		$returnVal = 'commercial';
	}
	return $returnVal;
}

function prepareData($dealerName, $toZip, $weight, $class, $lift, $resi, $lowes_mode)
{
	$order = Array
	(
		'ID' => 'QuickShippingQuote',
		'RefNumber' => 'QuickShippingQuote',
		'PONumber' => 'QuickShippingQuote',
		'dealerName' => $dealerName,
		'weight' => $weight,
		'liftgate' => $lift,
		'FreightClass' => $class,
		'ShipPostalCode' => $toZip,
		'residentialFedex' => $resi,
		'residential' => $resi,
		'lowes_mode' => $lowes_mode
	);
	return $order;
}

function filterResults($otherResults, $usedCarriers)
{
	/*print_r('<pre>');
	print_r($otherResults);
	print_r($usedCarriers);
	print_r('</pre>');
	die();*/
	$filteredResults = Array();
	foreach ($otherResults as $otherResult)
	{
		$extrCarrier = extrCarrier($otherResult['carrier']);
		if (in_array($extrCarrier, $usedCarriers) && $otherResult['cost'] >= 20)
		{
			$otherResult['logo'] = getCarrierLogo($otherResult['carrier']);
			$otherResult['date'] = date('m.d.Y');
			$filteredResults[]=$otherResult;
		}
	}
	if (!empty($filteredResults)) return $filteredResults; else return false;
}

function chooseBest($filteredResults)
{
	$min['cost'] = $filteredResults[0];
	foreach ($filteredResults as $filteredResult)
	{
		if (($filteredResult['cost'] < $min['cost']) /*|| ($filteredResult['cost'] == $min['cost'] && $filteredResult['time'] < $min['time'])*/)
		{
			$min = $filteredResult;
		}
	}
	return $min;
}

function startShipsingle($order)
{
	global $originZip, $originStatecode, $originCountry, $originCity, $ziphead_not_allowed_RL, $rl_pal_addon_cost_liftgate, $rl_pal_addon_cost_residential, $fedex_addon_cost_liftgate, $rate_ups;
	$conn = getConnection();
	 
	 //print_r($order);
	
	$allFinalResults = bigFunc($order, $conn, $_GET['fromZip'], $originStatecode, $originCountry, $originCity, $order['FreightClass'], $rl_pal_addon_cost_liftgate, $rl_pal_addon_cost_residential, $fedex_addon_cost_liftgate, $ziphead_not_allowed_RL, $rate_ups);
	
	
	$usedCarriers = Array();
	if (!empty($_GET['yrc'])) $usedCarriers[]=$_GET['yrc'];
	//if (!empty($_GET['yrc_nonpal'])) $usedCarriers[]=$_GET['yrc_nonpal'];
	if (!empty($_GET['odfl'])) $usedCarriers[]=$_GET['odfl'];
	//if (!empty($_GET['odfl'])) $usedCarriers[]=$_GET['odfl'];
	if (!empty($_GET['rl'])) $usedCarriers[]=$_GET['rl'];
	//if (!empty($_GET['rl_pal'])) $usedCarriers[]=$_GET['rl_pal'];
	if (!empty($_GET['fedex'])) $usedCarriers[]=$_GET['fedex'];
	if (!empty($_GET['ups'])) $usedCarriers[]=$_GET['ups'];
	//if (!empty($_GET['fedex_economy'])) $usedCarriers[]=$_GET['fedex_economy'];
	
	/*print_r('<pre>');
	print_r($allFinalResults['otherResults']);
	print_r('</pre>');*/
	
	$cheaporder = $allFinalResults['cheaporder'];
	$otherResults = $allFinalResults['otherResults'];
	$filteredResults = filterResults($otherResults, $usedCarriers);
	/*print_r('<pre>');
	print_r($filteredResults);
	print_r('</pre>');*/
	
        print_r( json_encode($filteredResults) );
        
        /*
        if ($filteredResults) $min = chooseBest($filteredResults); else $min = false;

		if ($min)
		{
			$min['logo'] = getCarrierLogo($min['carrier']);
			$min['date'] = date('m.d.Y');
			$min = json_encode($min);
			print_r($min);
		} else
		{
			$min = 'Can not get rate for this order.';
			$min = json_encode($min);
			print_r($min);
		}
        //*/
}

function saveToValidationLog($conn, $messages)
{
	echo "<br>Error.<br>";
	//print_r($messages);
}

function display_message($value)
{
	$date = date("h:i:s");
	echo '<span style="color:red;">'.$date.': </span><span style="color:black; font-weight: bold;">'.$value.'</span><br>';
}
?>