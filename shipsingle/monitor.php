<?php
echo "<br><strong>MONITOR</strong><br>Refresh the page to display shipsingle log for last 20 minutes.<br><br>";
$max_exec_time = 1200; //max execution time 20 minutes. Set in seconds
$delay = $max_exec_time + 10;
ini_set('max_execution_time', $max_exec_time);
date_default_timezone_set('America/New_York');
$path = $_SERVER['DOCUMENT_ROOT'];
require_once($path.'/db_connect/connect.php');

$messages = runQuery("SELECT [id]
      ,[message]
      ,[Date]
  FROM [dbo].[shipsingle_message_log]
WHERE 
[Date] < GetDate()
AND [Date] > DateADD(mi, -20, GetDate())");

if($messages)
{
	foreach($messages as $message)
	{
		echo '<span style="color:red;">'.$message['Date'].': </span><span style="color:black; font-weight: bold;">'.$message['message'].'</span><br>';
	}
} else 
{
	$date = date("h:i:s");
	echo '<span style="color:red;">'.$date.': </span><span style="color:black; font-weight: bold;">There are no messages for last 20 minutes.</span><br>';
}

function runQuery($query)
{
	try
	{
		$conn = Database::getInstance()->dbc;
		$stmt = $conn->prepare($query);
		$result = $stmt->execute();
		$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
		$conn = null;
		return $result;
	}catch (Exception $e) {
		die($e);
		return false;
	}
}
?>