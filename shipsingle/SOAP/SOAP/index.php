<?php
    ini_set('max_execution_time', 1000); 
    require_once('model/shipperModel_bamodified.php');
    require_once('helpers/carrierHelper.php');

    //Autoloader for mappers classes
    require_once("mappers/autoloader.php");

    //models
    require_once("model/HttpCarrier.php");
    require_once("model/SoapCarrier.php");
    require_once("model/NonPalletShipper.php");
    require_once("model/PalletShipper.php");

    //Helpers used for shipping
    require_once("helpers/Calculate.php");

    //Helpers used for requests
    require_once("helpers/UrlParser.php");
    require_once("helpers/MultiRequest.php");
    require_once("helpers/Transfer.php");
    require_once("helpers/ShippingAdvisor.php");

    //mapper error
    require_once("mappers/ErrorMapper.php");


    $shipperModel = new ShippingModel();

    //Helper objects:
    $multiRequester = new MultiRequest();

    $xmlParser = new XMLparser();
    $advisor = new ShippingAdvisor();
    $erroMapper = new ErrorMapper(); 

    $originLocations = json_decode(file_get_contents('config/config.originLocations.json'), true);
    $noPalletCarriers = json_decode(file_get_contents('config/config.noPalletCarriers.json'), true);
    $PalletCarriers = json_decode(file_get_contents('config/config.PalletCarriers.json'), true);


    $zipCodeOrigin = array(
        "zip"      => '18974',
        "statecode"    => 'PA',
        "country"  => 'US',
        "city"     => 'Warminster'
    );

    $orders = $shipperModel->getInProcessOrders();   
    $carrierOnDb = $shipperModel->getCarieer();

    $i = 0;
    $t = 0;
    $order = array();
    $ship = array();
    $errors = array();
    foreach ($orders as $key => $value) {
        if (!$value['PostalCode'] || !$value['weight']) {
            $errors[$value['ID']][$t++] = array(
                            'ID' => $value['ID'],
                            'ERROR' => 'Order has not Postal code or weight'
            );
            //echo "Order Id " . $value["ID"] . " has not Postal code or weight \n";
        } else if(!preg_match('/^([0-9]+)(-)*([0-9])*$/', $value['PostalCode'])) {
                    
                    $errors[$value['ID']][$t++] = array(
                        'ID' => $value['ID'],
                        'ERROR' => 'Zip index of order has wrong format',
                    );
            //echo "Order ".$value["ID"]." has invalid Postal Code";        

                } else {
                        $zipCodeDest = getZipInfo($value['PostalCode']);
                        if (isset($zipCodeDest['error_message'])) {
                            $errors[$value['ID']][$t++] = array(
                                'ID' => $value['ID'],
                                'ERROR' => $zipCodeDest['error_message'],
                            );
                        } else {    
                                foreach ($carrierOnDb as $name => $shipper) {
                            
                                    $options = array(
                                        'liftgate'       => FALSE,
                                        'residential'    => FALSE,
                                        'respickup'      => FALSE,
                                        'callondelivery' => FALSE,
                                        'inside'         => FALSE,
                                    );

                                    //Shipper initialization
                    		        $carrier = createCarrier($name, $noPalletCarriers, $zipCodeOrigin['zip'], $originLocations);
                    		        $shipp = new NonPalletShipper($name.$i, $carrier, $zipCodeOrigin, $zipCodeDest, $value['weight'], 85, $options);
                                    $shipp->setMultiplier(isset($carrierOnDb['Multiplier']) ? $carrierOnDb['Multiplier'] : 1);
                            	    
                                    if($carrier instanceof HttpCarrier)	{
                   	
                                        UrlParser::makeUrlComplete($shipp, $originLocations);
                                    	$url = $carrier->getUrl();
                                        $response = Transfer::getHTTPResponse($url);
                                        $result = array('result' => $xmlParser->parse($response, $carrier->getNeeded()));
                                        if (count($result['result'])<2) {
                                                $status = $erroMapper->convert($result, $carrier->getErrorMessage());
                                                $result['status'] = $status['ERROR'];
                                                
                                                $errors[$value['ID']][$name] = array(
                                                        'ID' => $value['ID'],
                                                        'ERROR' => $status['ERROR']
                                                );

                                        } else {
                                                    $result['status'] = 'ok';
                                                }

                    	               if (count($result['result']) != 0) {
                    	                       $shipp->parseCarrierResponce($result);
                    	               }

                                    } else if($carrier instanceof SoapCarrier) {
                                        
                                                $rateRequest = UrlParser::makeSoapRequest($shipp);
                                                $soapResponse = Transfer::getSOAPResponse($carrier->getUrl(), $carrier->getTimeout(), $carrier->getNamespace(), $rateRequest);

                                                if($soapResponse['status'] != 'ok') {
                                                        print("Error for `$name$i`: ".$soapResponse['message']."\n");
                                                }
                        
                                                //Don't call xmlParser. Parse soap response in the Shipper using carrier's mapper
                                                $shipp->parseCarrierResponce($soapResponse);
                                    }
                                    if (count($result['result']) != 0) {
        			                     $ship[$name.$i] = $shipp->getResult();
                                    }
	                           }
                        

            
                        /*foreach ($PalletCarriers as $name => $shipper) {

                                $shipp = new PalletShipper($name.$i, $name, $zipCodeOrigin, $zipCodeOrigin, $value['weight'], NULL);
                                $shipp->setMultiplier(isset($shipper['Multiplier']) ? $shipper['Multiplier'] : 1);
                                $shipp->setMaxWeight($shipper['MaxWeight']);
                                $pallets = intval(ceil($value['weight'] / $shipper['MaxWeight']));
          
                                $shipp->setPallets($pallets);

                                if($pallets == 1) $rate = floatval($value['rate']);
                                if($pallets == 2) $rate = floatval($value['rate']);
                                if($pallets >= 3) $rate = floatval($value['rate']);
                                $shipp->setRate($rate);
                       
                                $ship[$name.$i] = $shipp->getResult();
                        }*/


                        $order[][$value['ID']] = $ship;
                        unset($ship);
                    }
                    }
		
                    $i++;
	}
for ($j=0; $j<count($order); $j++) {
   foreach ($order[$j] as $id => $shipers) {
        $bestShipper = $advisor->choose_best($shipers);
        $bestShipperResults = $shipers[$bestShipper];
        
            $bestOforders[][$id] = array(
                $id, 
                $bestShipperResults['FROMZIP'],
                $bestShipperResults['CARRIERNAME'],
                $bestShipperResults['QUOTENUMBER'],
                $bestShipperResults['TRANSITTIME'],
                $bestShipperResults['COST']
            );
    }
}

    $shipperModel->export($bestOforders);
    $shipperModel->sendErrorMsg($errors);
