<?php
ini_set('max_execution_time', 7200);
date_default_timezone_set('America/New_York');
require_once('helpers/fedexAddressValidation.php');
require_once('functions.php');
$path = $_SERVER['DOCUMENT_ROOT'];
require_once($path.'/db_connect/connect.php');

function getResidentialOneOrderTEST($conn, $value, $key)
{
    $final_liftgate = '0';
    $final_residential = '0';
    $display_popup = 'False';
    $lowes_mode = 'no';
    if($value['residential'] == 'no')
    {
        $fedex_residential = getResidentialOneOrderHandler($conn, $value, $key);
        if
        (
            $fedex_residential == 'RESIDENTIAL' ||
            $fedex_residential == 'MIXED' ||
            $fedex_residential == 'UNKNOWN'
        )
        {
            $final_residential = '1';
        } else if($fedex_residential == 'BUSINESS')
        {
            $final_residential = '0';
        } else
        {
            $final_residential = false;
        }
    } else
    {
        $final_residential = $value['residential'];
        $fedex_residential = 'not calculated';
    }
    
    if($final_residential == '1' && is_in_str($value['dealerName'], "MENARDS") != false)
    {
        $final_liftgate = '1';
    } else if($final_residential == '1')
    {
        $final_liftgate = $value['liftgate'];
    } else if($final_residential == '0')
    {
        $final_liftgate = '0';
    }
    
    if($value['lowes_mode'] == 'no')
    {
        if($final_residential == '1')
        {
            $lowes_mode = 'residential';
        } else
        {
            $lowes_mode = 'commercial';
        }
    } else
    {
        $lowes_mode = $value['lowes_mode'];
    }
    
    if($final_residential == '1')
    {
        $display_popup = 'True';
    }
    
    if($final_residential)
    {
        $value['residential'] = $final_residential;
        $value['residentialFedex'] = $final_residential;
        $value['liftgate'] = $final_liftgate;
        $value['display_popup'] = $display_popup;
        $value['lowes_mode'] = $lowes_mode;
        $value['fedex_api_residential'] = $fedex_residential;
    } else
    {
        $value = false;
    }
    
    return $value;
}

$conn = Database::getInstance()->dbc;

$query = "SELECT TOP 1000
    [memo],
        [PONumber],
      [ShipAddress_Addr1],
	  [ShipAddress_Addr2],
	  [ShipAddress_PostalCode],
	  [CustomerRef_FullName] as [dealerName],
          '1' as [liftgate],
          case
            when shiptrack.ponumber in (select case when altdocid like '' then accountid else altdocid end  as [PO] from [SQLSERVER2].[integrator].[dbo].[ttcTxnTransactions] where partnerid='14' and TxnType='Special Order') then 'commercial'
            when shiptrack.ponumber in (select case when altdocid like '' then accountid else altdocid end  as [PO] from [SQLSERVER2].[integrator].[dbo].[ttcTxnTransactions] where partnerid='14' and TxnType='Direct Ship') then 'residential'
            when shiptrack.memo like '%SPECIAL ORDER%' then 'commercial'
            when shiptrack.memo like '%DIRECT SHIP%' then 'residential'
            else 'no'
            end as lowes_mode,
            case
                  when LOWER(shiptrack.CustomerRef_FullName) like 'lowe%' and shiptrack.ponumber in (select case when altdocid like '' then accountid else altdocid end  as [PO] from [SQLSERVER2].[integrator].[dbo].[ttcTxnTransactions] where partnerid='14' and TxnType='Special Order') then '0'
                  when LOWER(shiptrack.CustomerRef_FullName) like 'lowe%' and shiptrack.ponumber in (select case when altdocid like '' then accountid else altdocid end  as [PO] from [SQLSERVER2].[integrator].[dbo].[ttcTxnTransactions] where partnerid='14' and TxnType='Direct Ship') then '1'
                  when LOWER(shiptrack.CustomerRef_FullName) like 'lowe%' and shiptrack.memo like '%SPECIAL ORDER%' then '0'
                  when LOWER(shiptrack.CustomerRef_FullName) like 'lowe%' and shiptrack.memo like '%DIRECT SHIP%' then '1'
                  when LOWER(shiptrack.CustomerRef_FullName) like 'menards%' and (shiptrack.ShipAddress_Addr1 like 'Menards store%') THEN '0'
                  when LOWER(shiptrack.CustomerRef_FullName) like 'amazon%' and (shiptrack.ShipAddress_Addr1 like 'Amazon.com%') THEN '0'
                  when LOWER(shiptrack.CustomerRef_FullName) like 'your other warehouse%' and (shiptrack.ShipAddress_Addr1 like 'HOME DEPOT%' or shiptrack.ShipAddress_Addr1 like 'Home Depot%') THEN '0'
                  when LOWER(shiptrack.CustomerRef_FullName) like 'The Home Depot%' and (shiptrack.ShipAddress_Addr2 like 'C/O THD Ship to Store%') THEN '0'
            else 'no'
            end as residential
  FROM [dbo].[shiptrack] where [PONumber] in ('60796805', '60814635', '60765673')";

$steps = 30;

for($i=0; $i<$steps; $i++)
{
    $result = $conn->prepare($query);
    $result->execute();
    $result = $result->fetchAll(PDO::FETCH_ASSOC);

    foreach($result as $key => $value)
    {
            $resi = getResidentialOneOrderTEST($conn, $value, $key);
            /*echo 'PO: '.$value['PONumber'].'<br>';
            echo 'CustomerRef_FullName: '.$value['dealerName'].'<br>';
            echo 'ShipAddress_Addr1: '.$value['ShipAddress_Addr1'].'<br>';
            echo 'ShipAddress_Addr2: '.$value['ShipAddress_Addr2'].'<br>';
            echo 'ShipAddress_PostalCode: '.$value['ShipAddress_PostalCode'].'<br>';
            echo 'ShipAddress_PostalCode: '.$value['memo'].'<br>';

            echo 'fedex_api_residential: '.$resi['fedex_api_residential'].'<br>';
            echo 'database_residential: '.$value['residential'].'<br>';
            echo 'final_residential: '.$resi['residential'].'<br>';*/

            //die();
            $query2 = "INSERT INTO [Orders_Test].[dbo].[test_stores]
               ([PONumber]
               ,[CustomerRef_FullName]
               ,[ShipAddress_Addr1]
               ,[ShipAddress_Addr2]
               ,[ShipAddress_PostalCode]
               ,[fedex_api_residential]
               ,[database_residential]
               ,[final_residential]
               ,[memo])
         VALUES
               (".strRepl($value['PONumber'])."
               ,".strRepl($value['dealerName'])."
               ,".strRepl($value['ShipAddress_Addr1'])."
               ,".strRepl($value['ShipAddress_Addr2'])."
               ,".strRepl($value['ShipAddress_PostalCode'])."
               ,'".$resi['fedex_api_residential']."'
               ,'".$value['residential']."'
               ,'".$resi['residential']."'
               ,".strRepl($value['memo']).")";
            $result = $conn->prepare($query2);
            $result->execute();
    }

    echo 'finished loop '.$i.' of '.$steps.'<br>';
}