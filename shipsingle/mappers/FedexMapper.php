<?php

class FedexMapper
{
    public function convert($result)
    {
    	if (!isset($result['TITLE'])) {
        	$result['CHARGE'] = str_replace(array('$',','), '', $result['NET-FREIGHT-CHARGES']);
        	$result['QUOTENUMBER'] = $result['RATE-NUMBER'];
        	$result['TRANSITTIME'] = preg_replace('/[\D]/', '', $result['TRANSIT-DAYS']);
    	}

        return $result;
    }
}

?>