<?php

class ErrorMapper
{
	public function convert($result, $error)
    {
    	if (isset($result['result'][$error])) {
    		$status['ERROR'] = $result['result'][$error];
    	} else {
    		$status['ERROR'] = NULL;
    	}
    
        return $status;
    }
}