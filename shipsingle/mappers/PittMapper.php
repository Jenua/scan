<?php

class PittMapper
{
    public function convert($result)
    {
        $result['CHARGE'] = $result['TOTALCHARGES'];
        $result['QUOTENUMBER'] = $result['QUOTEID'];
        $result['TRANSITTIME'] = $result['ADVERTISEDTRANSIT'];

        return $result;
    }
}

?>