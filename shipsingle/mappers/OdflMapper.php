<?php

class OdflMapper
{
    public function convert($response)
    {
        $result = array();
        $result['CHARGE'] = $response->rateEstimate->netFreightCharge;
        $result['QUOTENUMBER'] = $response->referenceNumber == 0 ? 'n/a' : $response->referenceNumber;
        $result['TRANSITTIME'] = $response->destinationCitiesArray->serviceDays == 0 ? 'n/a' : $response->destinationCitiesArray->serviceDays;

    	return $result;
    }
}

?>