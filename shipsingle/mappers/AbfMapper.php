<?php

class AbfMapper
{
    public function convert($result)
    {
        $result['QUOTENUMBER'] = $result['QUOTEID'];
        $result['TRANSITTIME'] = preg_replace('/[\D]/', '', $result['ADVERTISEDTRANSIT']);

        return $result;
	}
}

?>