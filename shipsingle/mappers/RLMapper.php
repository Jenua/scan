<?php

class RLMapper
{
    public function convert($result)
    {
        $result['CHARGE'] = preg_replace('/[\D]/', '', $result['NETCHARGES']);
        $result['QUOTENUMBER'] = $result['QUOTE'];
        $result['TRANSITTIME'] = $result['DAYS'];

        return $result;
    }
}

?>