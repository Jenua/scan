<?php

class GoogleMapper
{
    public function convert($response)
    {
	      $place = $response->results[0];
	      $place = get_object_vars($place);
        
        for ($i = 0; $i < count($place["address_components"]); $i++) {
          switch ($place["address_components"][$i]->types[0]) {
            case 'postal_code' : $result['POSTAL'] = $place["address_components"][$i]->long_name;
               break;
            case 'administrative_area_level_1' : $result['STATE'] = $place["address_components"][$i]->short_name;
               break;
            case 'country' : $result['COUNTRY'] = $place["address_components"][$i]->short_name;
              break;
          }
        }

        $city = explode(", ", $place["formatted_address"]);
        if ($city[0] == "La Cañada Flintridge") {
          $city[0] = str_replace('Cañada', ' Canada', $city[0]);
        }
        if($city[0] == 'Warminster Twp') {
          $city[0] = 'Warminster';
        }
        if($city[0] == 'Charter Township of Clinton') {
          $city[0] = 'Clinton Twp';
        }
        $result['CITY'] = str_replace('-', '%20', str_replace(' ', '%20', str_replace('. ', '%20', $city[0])));
        $result['CITY'] = str_replace('Sainte', 'St%20', $result['CITY']);
		
		$lat = $place['geometry']->location->lat;
		$lng = $place['geometry']->location->lng;
		
		$result['lat'] = $lat;
		$result['lng'] = $lng;
        
        return $result;
    }
}

?>