<?php

function autoloadMapper($className)
{
	if (!defined('PHP_VERSION_ID') || PHP_VERSION_ID < 50120)
	{
		die('spl_autoload_register requires PHP 5.1.2 or higher');
	}

	if (preg_match('%([A-z]+)Mapper%', $className) && file_exists("mappers/$className.php"))
	{
		include_once("$className.php");
		return true;
	}

	return false;
}
spl_autoload_register('autoloadMapper');

?>