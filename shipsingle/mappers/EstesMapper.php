<?php

class EstesMapper
{
    public function convert($result)
    {   
		$charge = $result['TOTALFREIGHTCHARGES'];
        if($charge != 0)
        {
            $result['CHARGE'] = $charge - $result["DISCOUNT"] + $result["TOTALACCESSORIALCHARGES"] + $result["FUELSURCHARGE"];
        } else {
            $result['CHARGE'] = $result["NETFREIGHTCHARGES"] + $result["FUELSURCHARGE"];
        }

        return $result;
    }
}

?>