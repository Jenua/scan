<?php

class XMLparser
{
	private $_insideitem;
	private $_tagName;
	private $_result;
	private $_hash;
	
	public function __construct()
	{
		$this->_insideitem = false;
		$this->_tagName = "";
		$this->_result = array();
		$this->_hash = array();
	}
	
	private function startElement($parser, $name, $attrs) 
	{
		//var_dump($name);
		$this->_tagName = $name;
		$this->_insideitem = true;
	}
	
	private function endElement($parser, $name) 
	{
    	$this->_insideitem = false;
	}
	
	private function characterData($parser, $data)
	{
		if ($this->_insideitem) {
			if(in_array($this->_tagName, $this->_hash))
			{
				@$this->_result[$this->_tagName] .= strip_tags($data);
			}
		}
		//$this->_insideitem = false;
	}
	
	/*
	* Parses XML to reqiured array
	*
	* array $neededItems is hash for resulting array
	*/
	public function parse($response, $neededItems)
	{
		$this->_result = array();
		$xml_parser = xml_parser_create("");
		xml_parser_set_option($xml_parser, XML_OPTION_TARGET_ENCODING, 'utf-8');
		xml_set_object($xml_parser, $this);
		xml_set_element_handler($xml_parser, "startElement", "endElement");
		xml_set_character_data_handler($xml_parser, "characterData");

		$this->_hash = $neededItems;
		if (!xml_parse($xml_parser, $response)) {
			$error = xml_error_string(xml_get_error_code($xml_parser));
			$line = xml_get_current_line_number($xml_parser);

			throw new Exception("XML Parser error: $error at line $line.\n");
		}
		xml_parser_free($xml_parser);
		$this->_hash = array();
		
		return $this->_result;
	}
}

?>