<?php
ini_set('soap.wsdl_cache_enabled',0);
ini_set('soap.wsdl_cache_ttl',0);
require_once('SOAP/Client.php');

class Transfer
{
	//gets xml document from given url by HTTP
	public static function getHTTPResponse($url)
	{
		//print_r('<br>URL: '.$url);
		return file_get_contents($url);
	}
	
	//gets xml document from given url by SOAP
	public static function getSOAPResponse($url, $timeout, $namespace, $rateRequest)
	{
			$WSDL = new SOAP_WSDL($url, array('timeout'=> $timeout));
			$client = $WSDL->getProxy();
			if (method_exists($client,'setTrace'))
			{
			$client->setTrace(true);
			$client->setDefaultNamespace($namespace);

			$soapResult = $client->call(
				'getRateEstimate',
				array('rateRequest' => $rateRequest),
				array(
					'namespace'  => $namespace,
					'soapaction' => '',
					'style'      => 'document',
					'use'        => 'literal'
				)
			);


        if($client->fault != null) {
            return array(
                'status'  => 'fail',
                'message' => $client->fault->message,
                'result'  => array()
            );
        }
			/*print_r('<pre>');
			print_r($soapResult);
			print_r('</pre>');*/
			die();
        $ret = array(
            'status'  => 'ok',
            'result'  => $soapResult //stdClass
        );
		return $ret;
		} else return null;
		/*}*/
	}
	
}

?>