<?php
// Copyright 2009, FedEx Corporation. All rights reserved.
// Version 2.0.1   
/*
$fakeAddress = array(
				'ClientReferenceId' => 'fakeID',
				'Address' => array(
					'StreetLines' => array('75 Hawk Rd'),
					'PostalCode' => '18974',
					'CompanyName' => 'Dreamline'
				)
			);
$realAddress = array(
				'ClientReferenceId' => 'realID',
				'Address' => array(
					'StreetLines' => array('54 Morning Dove Ct'),
					'PostalCode' => '60548',
					'CompanyName' => 'Schwerdle, Richard'
				)
			);
$realAddress1 = array(
				'ClientReferenceId' => 'realID1',
				'Address' => array(
					'StreetLines' => array('172 Neptune Ave'),
					'PostalCode' => '11235',
					'CompanyName' => 'Sola Home Design Center'
				)
			);
$realAddress2 = array(
				'ClientReferenceId' => 'realID2',
				'Address' => array(
					'StreetLines' => array('4325 Greenhill Way'),
					'PostalCode' => '46012',
					'CompanyName' => 'Martin, Cedric'
				)
			);

$AddressesToValidate = array();
$AddressesToValidate[] = $fakeAddress;
$AddressesToValidate[] = $realAddress;
$AddressesToValidate[] = $realAddress1;
$AddressesToValidate[] = $realAddress2;
getValidatedAddress($AddressesToValidate);
*/
function getValidatedAddress($AddressesToValidate)
{
	//require_once('../fedex/fedex-common.php5');
	require_once('fedex/fedex-common.php5');

	//The WSDL is not included with the sample code.
	//Please include and reference in $path_to_wsdl variable.
	//$path_to_wsdl = "../fedex/AddressValidationService_v3.wsdl";
	$path_to_wsdl = "fedex/AddressValidationService_v3.wsdl";

	ini_set("soap.wsdl_cache_enabled", "0");

	$client = new SoapClient($path_to_wsdl, array('trace' => 1)); // Refer to http://us3.php.net/manual/en/ref.soap.php for more information

	$request['WebAuthenticationDetail'] = array(
		'UserCredential' => array(
			'Key' => getProperty('key'), 
			'Password' => getProperty('password')
		)
	);
	$request['ClientDetail'] = array(
		'AccountNumber' => getProperty('shipaccount'), 
		'MeterNumber' => getProperty('meter')
	);
	$request['TransactionDetail'] = array('CustomerTransactionId' => ' *** Address Validation Request using PHP ***');
	$request['Version'] = array(
		'ServiceId' => 'aval', 
		'Major' => '3', 
		'Intermediate' => '0', 
		'Minor' => '0'
	);
	$request['InEffectAsOfTimestamp'] = date('c');

	/*$request['AddressesToValidate'] = array(
		0 => array(
			'ClientReferenceId' => 'ClientReferenceId1',
			'Address' => array(
				'StreetLines' => array('75 Hawk Rd'),
				'PostalCode' => '18974',
				'CompanyName' => 'Dreamline'
			)
		),
		1 => array(
			'ClientReferenceId' => 'ClientReferenceId2',
			'Address' => array(
				'StreetLines' => array('50 N Front St'),
				'PostalCode' => '38103',
				'CompanyName' => 'FedEx Office'
			)
		),
		2 => array(
			'ClientReferenceId' => 'ClientReferenceId3',
			'Address' => array(
				'StreetLines' => array('18R High Street'),
				'PostalCode' => '01966',
				'CompanyName' => 'Cynthia Carroll'
			)
		)
	);*/
	$request['AddressesToValidate'] = $AddressesToValidate;


	try {
		if(setEndpoint('changeEndpoint')){
			$newLocation = $client->__setLocation(setEndpoint('endpoint'));
		}

		$response = $client ->addressValidation($request);
		/*print_r($response);
		die();*/

		$returnArray = Array();
		
		if ($response -> HighestSeverity != 'FAILURE' && $response -> HighestSeverity != 'ERROR'){
			foreach($response -> AddressResults as $addressResult){
				/*echo 'Client Reference Id: ' . $addressResult->ClientReferenceId . Newline;
				echo 'State: ' . $addressResult->State . Newline;
				echo 'Classification: ' . $addressResult->Classification . Newline;*/
				$returnArray[$addressResult->ClientReferenceId] = $addressResult->Classification;
				/*echo 'Proposed Address:' . Newline;
				foreach($addressResult->EffectiveAddress as $addressKey => $addressValue){
					echo '&nbsp;&nbsp;' . $addressValue . Newline;
				}
				if(array_key_exists("Attributes", $addressResult)){
					echo Newline . 'Address Attributes' . Newline;
					foreach($addressResult->Attributes as $attribute){
						echo '&nbsp;&nbsp;' . $attribute -> Name . ': ' . $attribute -> Value . Newline; 
					}
				}
				echo Newline;*/
			}
			
			//printSuccess($client, $response);
			/*print_r('<pre>');
			print_r($returnArray);
			print_r('</pre>');*/
			return $returnArray;
		}else{
			printError($client, $response);
			return false;
		} 
		
		writeToLog($client);    // Write to log file   
	} catch (SoapFault $exception) {
		printFault($exception, $client);
	}
}

/*$AddressesToValidate = array(
		0 => array(
			'ClientReferenceId' => 'ClientReferenceId1',
			'Address' => array(
				'StreetLines' => array('75 Hawk Rd'),
				'PostalCode' => '18974',
				'CompanyName' => 'Dreamline'
			)
		),
		1 => array(
			'ClientReferenceId' => 'ClientReferenceId2',
			'Address' => array(
				'StreetLines' => array('50 N Front St'),
				'PostalCode' => '38103',
				'CompanyName' => 'FedEx Office'
			)
		),
		2 => array(
			'ClientReferenceId' => 'ClientReferenceId3',
			'Address' => array(
				'StreetLines' => array('18R High Street'),
				'PostalCode' => '01966',
				'CompanyName' => 'Cynthia Carroll'
			)
		)
	);
	
	$result = getValidatedAddress($AddressesToValidate);
	if ($result)
	{
		print_r('<pre>');
		print_r($result);
		print_r('</pre>');
	}*/
?>