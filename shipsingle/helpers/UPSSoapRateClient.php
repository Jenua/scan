<?php
  
    //Configuration
  $access = "DCF7FCB1FEA8AD66";
  $userid = "Marina_Luxaris";
  $passwd = "dora1234";
  $wsdl = "../wsdl/ups/RateWS.wsdl";
  $operation = "ProcessRate";
  $endpointurl = 'https://wwwcie.ups.com/webservices/Rate';
  $outputFileName = "XOLTResult.xml";

  function processRate()
  {
      //create soap request
      $option['RequestOption'] = 'Rate';
      $request['Request'] = $option;

      $pickuptype['Code'] = '01';
      $pickuptype['Description'] = 'Daily Pickup';
      $request['PickupType'] = $pickuptype;

      $customerclassification['Code'] = '01';
      $customerclassification['Description'] = 'Classfication';
      $request['CustomerClassification'] = $customerclassification;

      $shipper['Name'] = 'Bathauthority';
      $shipper['ShipperNumber'] = 'Marina_Luxaris';
      $address['AddressLine'] = array
      (
          '75 Hawk Rd'
      );
      $address['City'] = 'Warminster';
      $address['StateProvinceCode'] = 'PA';
      $address['PostalCode'] = '18974';
      $address['CountryCode'] = 'US';
      $shipper['Address'] = $address;
      $shipment['Shipper'] = $shipper;

      $shipto['Name'] = 'Imani Imaginarium';
      $addressTo['AddressLine'] = '7111 Westlake Terrace';
      $addressTo['City'] = 'Bethesda';
      $addressTo['StateProvinceCode'] = 'MD';
      $addressTo['PostalCode'] = '20817';
      $addressTo['CountryCode'] = 'US';
      $addressTo['ResidentialAddressIndicator'] = '1';
      $shipto['Address'] = $addressTo;
      $shipment['ShipTo'] = $shipto;

      $shipfrom['Name'] = 'Imani Imaginarium';
      $addressFrom['AddressLine'] = array
      (
          'Southam Rd',
          '4 Case Court',
          'Apt 3B'
      );
      $addressFrom['City'] = 'Warminster';
      $addressFrom['StateProvinceCode'] = 'PA';
      $addressFrom['PostalCode'] = '18974';
      $addressFrom['CountryCode'] = 'US';
      $shipfrom['Address'] = $addressFrom;
      $shipment['ShipFrom'] = $shipfrom;

      $service['Code'] = '03';
      $service['Description'] = 'Service Code';
      $shipment['Service'] = $service;

      $packaging1['Code'] = '02';
      $packaging1['Description'] = 'Rate';
      $package1['PackagingType'] = $packaging1;
      $dunit1['Code'] = 'IN';
      $dunit1['Description'] = 'inches';
      $dimensions1['Length'] = '5';
      $dimensions1['Width'] = '4';
      $dimensions1['Height'] = '10';
      $dimensions1['UnitOfMeasurement'] = $dunit1;
      $package1['Dimensions'] = $dimensions1;
      $punit1['Code'] = 'LBS';
      $punit1['Description'] = 'Pounds';
      $packageweight1['Weight'] = '150';
      $packageweight1['UnitOfMeasurement'] = $punit1;
      $package1['PackageWeight'] = $packageweight1;

      $shipment['Package'] = array(	$package1 );
      $shipment['ShipmentServiceOptions'] = '';
      $shipment['LargePackageIndicator'] = '';
      $request['Shipment'] = $shipment;
      /*echo "Request.......\n";
      print_r($request);
      echo "\n\n";*/
      return $request;
  }

  try
  {

    $mode = array
    (
         'soap_version' => 'SOAP_1_1',  // use soap 1.1 client
         'trace' => 1
    );

    // initialize soap client
  	$client = new SoapClient($wsdl , $mode);

  	//set endpoint url
  	$client->__setLocation($endpointurl);


    //create soap header
    $usernameToken['Username'] = $userid;
    $usernameToken['Password'] = $passwd;
    $serviceAccessLicense['AccessLicenseNumber'] = $access;
    $upss['UsernameToken'] = $usernameToken;
    $upss['ServiceAccessToken'] = $serviceAccessLicense;

    $header = new SoapHeader('http://www.ups.com/XMLSchema/XOLTWS/UPSS/v1.0','UPSSecurity',$upss);
    $client->__setSoapHeaders($header);


    //get response
  	$resp = $client->__soapCall($operation ,array(processRate()));
	print_r('<pre>');
	print_r($resp);
	print_r('</pre>');
	

    //get status
    //echo "Response Status: " . $resp->Response->ResponseStatus->Description ."\n";

    //save soap request and response to file
   /* $fw = fopen($outputFileName , 'w');
    fwrite($fw , "Request: \n" . $client->__getLastRequest() . "\n");
    fwrite($fw , "Response: \n" . $client->__getLastResponse() . "\n");
    fclose($fw);*/

  }
  catch(Exception $ex)
  {
  	print_r ($ex);
  }

?>
