<?php

class UrlParser
{
	public static function makeUrlComplete($shipper, $originLocations)
	{
        $carrier = $shipper->getCarrier();
        $name = $carrier->getName();
        $zipCodeOrigin = $shipper->getFromZip();
        $zipCodeDest = $shipper->getDestZip();
        $class = $shipper->getClass();
        $weight = $shipper->getWeight();
        $options = $shipper->getOptions();

        if($name == 'ABF-NONPALLET') {
            //Fedex uses different account and regKey for bill-to shipments
            $carrier->setInUrl("{SHIP_MONTH}", date('n'));
            $carrier->setInUrl("{SHIP_DAY}", date('j'));
            $carrier->setInUrl("{SHIP_YEAR}", date('Y'));
        }

        if($name == 'YELLOW-NONPAL') {
            if ($zipCodeDest['state'] == 'NL') {
                $zipCodeDest['state'] = 'NF';
            } elseif ($zipCodeDest['state'] == 'QC') {
                $zipCodeDest['state'] = 'PQ';
            }
            //Fedex uses different account and regKey for bill-to shipments
            $carrier->setInUrl("{ORIGCITYNAME}", $zipCodeOrigin['city']);
            $carrier->setInUrl("{ORIGCODE}", $zipCodeOrigin['statecode']);
            $carrier->setInUrl("{DESTCITYNAME}", $zipCodeDest['city']);
            $carrier->setInUrl("{DESTCODE}", $zipCodeDest['state']);
        }

        //In case of Friday, Sunday or Saturday we will use Monday as nearest business day
        if(date('l') == 'Sunday' || date('l') == 'Saturday' || date('l') == 'Friday') {
            $dateToUse = date('Ymd', strtotime('next Monday'));
        } else {
            $dateToUse = date('Ymd', strtotime('+1 day'));
        }
        $carrier->setInUrl("{DATE}", $dateToUse);

        //Shipping class flips
        if($name=='FEDEX FREIGHT' && $class=='77.5') { 
            $class = '77'; //As fedex can't accept more than 3 symbols, we need to change 77.5 to 77
        }
        if(($name=='ESTES' || $name=='YELLOW-NONPAL' || $name == 'ABF-NONPALLET') && $class == '77') { 
            $class = '77.5';
        }
        if($name == 'YELLOW-NONPAL' && $class == '92') {
            $class = '92.5';
        }
        if($name=='FEDEX FREIGHT' || $name=='FEDEX-PRIORITY') {
            if (strlen($class) < 3) {
                settype($class, "string");
                $class = "0".$class;
            }
        }
        $carrier->setInUrl("{CLASS1}", $class);
        //--- end Shipping class flips

        $carrier->setInUrl("{ORIG}", $zipCodeOrigin['zip']);
        $carrier->setInUrl("{DEST}", $zipCodeDest['zip']);

        //Deal with canada and usa
        if($zipCodeDest['country'] == 'CA') {
            $carrier->setInUrl("{DESTCOUNTRY3}", 'CAN');
            $carrier->setInUrl("{DESTCOUNTRY2}", 'CA');
        } else {
            $carrier->setInUrl("{DESTCOUNTRY3}", 'USA');
            $carrier->setInUrl("{DESTCOUNTRY2}", 'US');
        }
        if($zipCodeOrigin['country'] == 'CA') {
            $carrier->setInUrl("{ORIGCOUNTRY3}", 'CAN');
            $carrier->setInUrl("{ORIGCOUNTRY2}", 'CA');
        } else {
            $carrier->setInUrl("{ORIGCOUNTRY3}", 'USA');
            $carrier->setInUrl("{ORIGCOUNTRY2}", 'US');
        }
        //Options section
        if($options['liftgate']) {
            $carrier->setInUrl("{LIFTGATE}", 'Y');
            $carrier->setInUrl("{RL-LIFTGATE}", 'X');
        } else {
            $carrier->setInUrl("{LIFTGATE}", 'N');
            $carrier->setInUrl("{RL-LIFTGATE}", '');
        }

        if($options['inside']) {
            $carrier->setInUrl("{IDEL}", 'Y');
            $carrier->setInUrl("{RL-IDEL}", 'X');
            $carrier->setInUrl("{FEDEX-IDEL}", 'PREPAID');
            $carrier->setInUrl("{YRC-IDEL}", 'on');
        } else {
            $carrier->setInUrl("{IDEL}", 'N');
            $carrier->setInUrl("{RL-IDEL}", '');
            $carrier->setInUrl("{FEDEX-IDEL}", 'COLLECT');
            $carrier->setInUrl("{YRC-IDEL}", 'off');
        }

        if($options['residential']) {
            $carrier->setInUrl("{RDEL}", 'Y');
            $carrier->setInUrl("{RL-RDEL}", 'X');
            $carrier->setInUrl("{HOMD}", 'HOMD');
        } else {
            $carrier->setInUrl("{RDEL}", 'N');
            $carrier->setInUrl("{RL-RDEL}", '');
            $carrier->setInUrl("{HOMD}", '');
        }

        if($options['respickup']) {
            $carrier->setInUrl("{RL-PICKUP}", 'X');
            $carrier->setInUrl("{FED-RPICKUP}", 'Y');
        } else {
            $carrier->setInUrl("{RL-PICKUP}", '');
            $carrier->setInUrl("{FED-RPICKUP}", 'N');
        }

        if($options['callondelivery']) {
            $carrier->setInUrl("{CALL}", 'Y');
        } else {
            $carrier->setInUrl("{CALL}", 'N');
        }
        //end of options

        //$carrier->setInUrl("{CLASS1}", $class);
        $carrier->setInUrl("{WEIGHT1}", $weight);
        $carrier->setInUrl("{SHIP_CITY}", $zipCodeDest['city']);
        $carrier->setInUrl("{SHIP_STATE}", $zipCodeDest['state']);
        $carrier->setInUrl("{ORIG_STATE}", $zipCodeOrigin['statecode']);
        $carrier->setInUrl("{ORIG_CITY}", $zipCodeOrigin['city']);
        $carrier->setInUrl("{ORIG_ZIP}", $zipCodeOrigin['zip']);
	}

	public static function makeSoapRequest($shipper)
	{
        $zipCodeOrigin = $shipper->getFromZip();
        $zipCodeDest = $shipper->getDestZip();
        $class = $shipper->getClass();
        $weight = $shipper->getWeight();
        $options = $shipper->getOptions();

        //---do the shipping class flips
        $frCountry = $toCountry = 'USA';
        if($zipCodeDest['country'] == 'CA') {
            $toCountry = 'CAN';
        }
        if($zipCodeOrigin['zip'] == 'CA') {
            $frCountry = 'CAN';
        }
        $class = $class == 77.5 ? 77 : $class;
        //---end do the shipping class flips

        $accessorials = array();
        if($options['liftgate']) {
            $accessorials[] = new SOAP_Value('code', false, 'HYD');
            $accessorials[] = new SOAP_Value('value', false, null);
        }
        if($options['inside']) {
            $accessorials[] = new SOAP_Value('code', false, 'IDC');
            $accessorials[] = new SOAP_Value('value', false, null);
        }
        if($options['residential'] || $options['respickup']) {
            $accessorials[] = new SOAP_Value('code', false, 'RDC');
            $accessorials[] = new SOAP_Value('value', false, null);
        }
        $params = array(
            'originPostalCode' => $zipCodeOrigin['zip'],
            'originCountry'    => $frCountry,
            'destinationPostalCode' => $zipCodeDest['zip'],
            'destinationCountry'    => $toCountry,
            'requestorType' => 'S',
            'odfl4meUser'   => 'dreamline',
            'odfl4mePassword' => 'dora1234',
            'odflCustomerAccount' => '12548094',
            'freightArray'  => array(
                'nmfcClass' => $class,
                'weight' => $weight,
                'length' => null,
                'width'  => null,
                'height' => null,
                'handlingUnits' => null,
                'density' => null
            ),
            'mexicoServiceCenter' => null,
            'currencyFormat' => null,
            'requestReferenceNumber' => 'Y'
        );
        if(!empty($accessorials)) {
            $params['accessorialArray'] = $accessorials;
        }

        $namespace = $shipper->getCarrier()->getNamespace();
        $rateRequest = new SOAP_Value("{".$namespace."}rateRequest", false, $params);
        return $rateRequest;
	}
}

?>