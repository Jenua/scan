<?php

  //Configuration
  $access = "DCF7FCB1FEA8AD66";
  $userid = "Marina_Luxaris";
  $passwd = "dora1234";
  $wsdl = "../wsdl/ups/FreightRate.wsdl";
  $operation = "ProcessFreightRate";
  //$endpointurl = 'https://wwwcie.ups.com/webservices/FreightRate';
  $endpointurl = 'https://onlinetools.ups.com/webservices/FreightRate';
  $outputFileName = "XOLTResult.xml";

  function processFreightRate()
  {
      //create soap request
      $option['RequestOption'] = '1';
      $request['Request'] = $option;
      $shipfrom['Name'] = 'Bathauthority';
      $addressFrom['AddressLine'] = '75 Hawk Rd';
      $addressFrom['City'] = 'Warminster';
      $addressFrom['StateProvinceCode'] = 'PA';
      $addressFrom['PostalCode'] = '18974';
      $addressFrom['CountryCode'] = 'US';
      $shipfrom['Address'] = $addressFrom;
      $request['ShipFrom'] = $shipfrom;

      $shipto['Name'] = 'Lowe`s CUS:2440';
      $addressTo['AddressLine'] = '3000 SH 121';
      $addressTo['City'] = 'Euless';
      $addressTo['StateProvinceCode'] = 'TX';
      $addressTo['PostalCode'] = '76039';
      $addressTo['CountryCode'] = 'US';
      $shipto['Address'] = $addressTo;
      $request['ShipTo'] = $shipto;

      $payer['Name'] = 'Bathauthority';
      $addressPayer['AddressLine'] = '75 Hawk Rd';
      $addressPayer['City'] = 'Warminster';
      $addressPayer['StateProvinceCode'] = 'PA';
      $addressPayer['PostalCode'] = '18974';
      $addressPayer['CountryCode'] = 'US';
      $payer['Address'] = $addressPayer;
      $shipmentbillingoption['Code'] = '10';
      $shipmentbillingoption['Description'] = 'Prepaid';
      $paymentinformation['Payer'] = $payer;
      $paymentinformation['ShipmentBillingOption'] = $shipmentbillingoption;
      $request['PaymentInformation'] = $paymentinformation;

      $service['Code'] = '308';
      $service['Description'] = 'UPS Freight LTL';
      $request['Service'] = $service;

      $handlingunitone['Quantity'] = '4';
      $handlingunitone['Type'] = array
      (
          'Code' => 'PLT',
          'Description' => 'Pallet'
      );
      $request['HandlingUnitOne'] = $handlingunitone;

     // $commodity['CommodityID'] = '';
      $commodity['Description'] = 'Samples';
      $commodity['Weight'] = array
      (
         'UnitOfMeasurement' => array
         (
             'Code' => 'LBS',
             'Description' => 'Pounds'
         ),
         'Value' => '168'
      );
      $commodity['NumberOfPieces'] = '4';
      $commodity['PackagingType'] = array
      (
           'Code' => 'PLT',
           'Description' => 'Pallet'
      );
      $commodity['FreightClass'] = '85';
      //$commodity['NMFCCommodityCode'] = '1160301';
      $request['Commodity'] = $commodity;

      $shipmentserviceoptions['PickupOptions'] = array
      (
            'HolidayPickupIndicator' => '',
	  	    'InsidePickupIndicator' => '',
	  		'ResidentialPickupIndicator' => '',
	  		'WeekendPickupIndicator' => '',
	  		'LiftGateRequiredIndicator' => ''
	  );

	  $request['ShipmentServiceOptions'] = $shipmentserviceoptions;

     /* echo "Request.......\n";
      print_r($request);
      echo "\n\n";*/
      return $request;
  }

  try
  {

    $mode = array
    (
         'soap_version' => 'SOAP_1_1',  // use soap 1.1 client
         'trace' => 1
    );

    // initialize soap client
  	$client = new SoapClient($wsdl , $mode);

  	//set endpoint url
  	$client->__setLocation($endpointurl);


    //create soap header
    $usernameToken['Username'] = $userid;
    $usernameToken['Password'] = $passwd;
    $serviceAccessLicense['AccessLicenseNumber'] = $access;
    $upss['UsernameToken'] = $usernameToken;
    $upss['ServiceAccessToken'] = $serviceAccessLicense;

    $header = new SoapHeader('http://www.ups.com/XMLSchema/XOLTWS/UPSS/v1.0','UPSSecurity',$upss);
    $client->__setSoapHeaders($header);


    //get response
  	$resp = $client->__soapCall($operation ,array(processFreightRate()));

    //get status
    echo "Response Status: " . $resp->Response->ResponseStatus->Description ."\n";
	print_r('<pre>');
	print_r($resp);
	print_r('</pre>');

    //save soap request and response to file
   /* $fw = fopen($outputFileName , 'w');
    fwrite($fw , "Request: \n" . $client->__getLastRequest() . "\n");
    fwrite($fw , "Response: \n" . $client->__getLastResponse() . "\n");
    fclose($fw);*/

  }
  catch(Exception $ex)
  {
  	print_r ($ex);
  }

?>
