<?php

class MultiRequest
{
    private $_multiHandle;
    private $_curls; //for connections resourses
    private $_result;

    public function __construct()
    {
        $this->_multiHandle = curl_multi_init();
        $this->_curls = array();
        $this->_result = array();
    }

    public function __destruct()
    {
        curl_multi_close($this->_multiHandle);
    }

    public function addMultiRequest($name, $url, $timeout, $callback, $callbackData = array())
    {
        $curlHandle = curl_init();
        curl_setopt($curlHandle, CURLOPT_URL, $url);
        curl_setopt($curlHandle, CURLOPT_HEADER, false);
        curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curlHandle, CURLOPT_TIMEOUT, $timeout);
        curl_setopt($curlHandle, CURLOPT_FAILONERROR, true);

        if(parse_url($url, PHP_URL_SCHEME) == "https") {
            curl_setopt($curlHandle, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curlHandle, CURLOPT_SSLVERSION, 3);
            curl_setopt($curlHandle, CURLOPT_SSL_VERIFYHOST, 2);
        }

        $this->_curls[$name] = array(
            'curlHandle' => $curlHandle,
            'callback'   => $callback,
            'callbackData' => $callbackData
        );

        curl_multi_add_handle($this->_multiHandle, $curlHandle);
    }

    public function getResponses()
    {
        //Do requests untill there is a connection
        $running = null;
        do {
            curl_multi_exec($this->_multiHandle, $running);
            usleep(10000);
        } while($running > 0);

        //Get content and pase it by callback function
        foreach($this->_curls as $name => $curlInfo)
        {
            $content = curl_multi_getcontent($curlInfo['curlHandle']);

            if($content && !empty($content)) {
                try {
                    $this->_result[$name] = array(
                        'status'  => 'ok',
                        'result'  => call_user_func_array($curlInfo['callback'], array($content, $curlInfo['callbackData']))
                    );
                } catch (Exception $e) {
                    $this->_result[$name] = array(
                        'status'  => 'fail',
                        'message' => $e->getMessage(),
                        'result'  => array()
                    );
                }
            } else {
                $this->_result[$name] = array(
                    'status'  => 'fail',
                    'message' => curl_error($curlInfo['curlHandle']),
                    'result'  => array()
                );
            }
            curl_multi_remove_handle($this->_multiHandle, $curlInfo['curlHandle']);
        }

        return $this->_result;
    }
}

?>