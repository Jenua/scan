<?php
function getLTL_ODFL_Rate($fromZip, $toZip, $ratedClass, $weight, $lift, $resi)
{
	if ($lift == '1') $lift = 'HYO'; else $lift = '';
	if ($resi == '1') $resi = 'RDC'; else $resi = '';
	/*print_r($fromZip.'<br>');
	print_r($toZip.'<br>');
	print_r($ratedClass.'<br>');
	print_r($weight.'<br>');*/
		$soapClient = new SoapClient("wsdl/odfl/RateService.wsdl");
		
		$freightarray=array(array('ratedClass'=>$ratedClass,'weight'=>$weight));
		$params = Array(
			'arg0' => Array(
				'accessorials' => $lift,
				'accessorials' => $resi,
				'odfl4MeUser' => 'dreamline',
				'odfl4MePassword' => 'dora1234',
				'odflCustomerAccount' => '12548094',
				'originPostalCode' => $fromZip,
				'destinationPostalCode' => $toZip,
				'freightItems' => $freightarray,
				'requestReferenceNumber' => true
			)
		);

		$results = $soapClient->getLTLRateEstimate($params);

		if($results->return->success){
			$ODFLresult = array();
			$ODFLresult['transitTime']=	 $results->return->destinationCities->serviceDays;
			$ODFLresult['Quotenumber']=	 $results->return->referenceNumber;
			$ODFLresult['totalcharge'] = $results->return->rateEstimate->netFreightCharge;
			
			return $ODFLresult;
		} else {
			$errors=$results->return->errorMessages;
			print_r($errors);
			return false;
		}
}
?>