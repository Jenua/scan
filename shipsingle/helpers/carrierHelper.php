<?php

//Helpers used for requests
require_once("helpers/Transfer.php");
require_once("helpers/XMLparser.php");
require_once("mappers/GoogleMapper.php");

function getZipInfo($country, $zipcode)	{
    	$googleInfo = json_decode(file_get_contents('config/config.google.json'), true);
    	$urlGoogle = str_replace('zipcode', $zipcode, $googleInfo['url']);
		$urlGoogle = str_replace('countrycode', $country, $urlGoogle);
    	$response = json_decode(Transfer::getHTTPResponse($urlGoogle));
		
    	$mapper = new GoogleMapper();
    	$zipInfo = $mapper->convert($response);
		//print_r($zipInfo);
		$addrLine = getAddrLine($zipInfo['lat'], $zipInfo['lng']);
    	return array(
			"zip"     => $zipcode,
       	 	//"zip"     => str_replace(' ', '', $zipInfo["POSTAL"]),
       	 	"state"   => $zipInfo['STATE'],
       	 	"country" => $zipInfo['COUNTRY'],
       	 	"city"    => $zipInfo['CITY'],
			"lat" => $zipInfo['lat'],
			"lng" => $zipInfo['lng'],
			"addr" => $addrLine
    	);
}

function getAddrLine($lat, $long)
{
	$url  = "http://maps.googleapis.com/maps/api/geocode/json?latlng=".$lat.",".$long."&sensor=false";
	$json = @file_get_contents($url);
	$data = json_decode($json);
	$status = $data->status;
	$address = false;
	if($status == "OK")
	{
		$address = $data->results[0]->formatted_address;
		$address = substr($address, 0, strpos($address, ','));
	}
	return $address;
}

function createCarrier($name, $nonPalletCarriersInfo, $zipcode, $originLocations)
{
    $carrier = null;

    $shiper = $nonPalletCarriersInfo[$name];
    if(!$shiper) {
        die("Can't identify not pallet carrier.");
    }

    $mapper = isset($shiper['mapper']) ? new $shiper['mapper'] : null;
    $timeout = isset($shiper['timeout']) ? intval($shiper['timeout']) : 20;
    $error = isset($shiper['error']) ? $shiper['error'] : null;
    if(!isset($shiper['protocolType'])) {
        die("Protocol type for not pallet carrier `$name` is undifined in config.");
    }
    if($shiper['protocolType'] == 'http') {
        if(in_array($zipcode, $originLocations)) {
            $url = $shiper['url'];
        } else {
            $url = isset($shiper['url-notOrigins']) ? $shiper['url-notOrigins'] : $shiper['url'];
        }
        $carrier = new HttpCarrier($name, $url, $timeout, $shiper['needed'], $mapper, $error);

    } else if($shiper['protocolType'] == 'soap') {
        $carrier = new SoapCarrier($name, $shiper['url'], $timeout, $shiper['namespace'], $mapper);

    } else {
        die("Protocol type `".$shiper['protocolType']."` for not pallet carrier `$name` is incorrect.");
    }

    if(!$carrier) {
        die("Can't cteate Carrier `$name`.");
    }
    
    return $carrier;

}