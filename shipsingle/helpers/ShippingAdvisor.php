<?php

//TODO: strip invalid carreers form array (like which have 0 price etc)
//!!! check first assignment for zero or empty value
class ShippingAdvisor 
{
	private $_internalShippers;
	private $_includePallets;
	
	function __construct($includePallets = false)
	{
		$this->_includePallets = $includePallets;
		$this->_internalShippers = array();
	}
	
	function choose_best($shippers)
	{
		/*print_r('<pre>');
		print_r($shippers);
		print_r('</pre>');
		die();*/
		if(!$shippers || empty($shippers))
			return null;

		$this->_internalShippers = $shippers;

		$shipper = null;
		$price = 0; //choose price of the best preferred
		
		foreach($this->_internalShippers as $key => $internalShipper)
		{
			if ($internalShipper['COST'] > 20)
			{
				$name = $internalShipper['CARRIERNAME'];

				$pricetocompare = round($internalShipper['COST']);
				//var_dump($name.$pricetocompare);
		            
				if($price == 0 && $pricetocompare != 0) {
					$price = $pricetocompare;
					$shipper = $key;
					continue;
				}
				if($pricetocompare < $price && $pricetocompare != 0) {
					$shipper = $key;
					$price = $pricetocompare;
				} 
				else //if values are the same then we need them both to check transit time
				if($pricetocompare == $price && is_numeric($internalShipper['TRANSITTIME'])) {
					//if next carreer has the same price, but less transit time then use it
					$currentTT = round($internalShipper['TRANSITTIME']);
					if($currentTT != 0 && $currentTT < round($this->_internalShippers[$shipper]['TRANSITTIME'])) {
						$shipper = $key;
						$price = $pricetocompare;
					}
				}
			}
		};

		//Now we have the best preferred carreer with lowest price

		if(!$shipper)
			return null;
		/*print_r('<pre>');
		print_r($this);
		print_r('</pre>');
		die();*/
		return $this->_internalShippers[$shipper]['NAME'];
	}
}
?>