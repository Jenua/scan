<?php
// Copyright 2009, FedEx Corporation. All rights reserved.
// Version 12.0.0
/*$city = 'Stuart';
$state = 'FL';
$postalCode = '34997';
$countryCode = 'US';
$weightValue = '260';
$resi = '1';
$lift = '1';
$fedex_addon_cost_liftgate = 65.08;
print_r(getFedExRate($city, $state, $postalCode, $countryCode, $weightValue, $resi, $lift, $fedex_addon_cost_liftgate));*/

function getFedExRate($city, $state, $postalCode, $countryCode, $weightValue, $resi, $lift, $fedex_addon_cost_liftgate, $origin_addr, $origin_city, $origin_statecode, $origin_zip, $origin_country){
	require_once('fedex/fedex-common.php5');
	//require_once('../fedex/fedex-common.php5');
	//if ($lift == '1') $lift = 'LIFTGATE_PICKUP'; else $lift = '';
	if ($lift == '1') $lift_text = 'LIFTGATE_DELIVERY'; else $lift_text = '';
	$newline = "<br />";
	//The WSDL is not included with the sample code.
	//Please include and reference in $path_to_wsdl variable.
	$path_to_wsdl = "fedex/RateService_v16.wsdl";
	//$path_to_wsdl = "../fedex/RateService_v16.wsdl";

	ini_set("soap.wsdl_cache_enabled", "0");
	 
	$client = new SoapClient($path_to_wsdl, array('trace' => 1)); // Refer to http://us3.php.net/manual/en/ref.soap.php for more information

	$request['WebAuthenticationDetail'] = array(
		'UserCredential' =>array(
			'Key' => getProperty('key'), 
			'Password' => getProperty('password')
		)
	); 
	if($origin_zip == '18974') $shipaccount = getProperty('shipaccount'); else $shipaccount = getProperty('shipaccount');
	$request['ClientDetail'] = array(
		'AccountNumber' => $shipaccount, 
		'MeterNumber' => getProperty('meter')
	);
	$request['TransactionDetail'] = array('CustomerTransactionId' => ' *** Rate Request using PHP ***');
	$request['Version'] = array(
		'ServiceId' => 'crs', 
		'Major' => '16', 
		'Intermediate' => '0', 
		'Minor' => '0'
	);
	$request['ReturnTransitAndCommit'] = true;
	$request['RequestedShipment']['DropoffType'] = 'REGULAR_PICKUP'; // valid values REGULAR_PICKUP, REQUEST_COURIER, ...
	$request['RequestedShipment']['ShipTimestamp'] = date('c');
	$request['RequestedShipment']['ServiceType'] = 'FEDEX_FREIGHT_ECONOMY'; // valid values STANDARD_OVERNIGHT, PRIORITY_OVERNIGHT, FEDEX_GROUND, ...
	
	if ($lift_text == 'LIFTGATE_DELIVERY')
	{
		$request['RequestedShipment']['SpecialServicesRequested'] = array(
			'SpecialServiceTypes' => $lift_text
		);
	}
	$request['RequestedShipment']['PackagingType'] = 'YOUR_PACKAGING'; // valid values FEDEX_BOX, FEDEX_PAK, FEDEX_TUBE, YOUR_PACKAGING, ...
	
	if($origin_zip == '18974') $freightbilling = getProperty('freightbilling'); else $freightbilling = array(
		'Contact'=>array(
			'ContactId' => 'freight1',
			'PersonName' => 'Big Shipper',
			'Title' => 'Manager',
			'CompanyName' => 'Freight Shipper Co',
			'PhoneNumber' => '1234567890'
		),
		'Address'=>array(
			'StreetLines'=>array(
				$origin_addr, 
				
			),
			'City' =>$origin_city,
			'StateOrProvinceCode' => $origin_statecode,
			'PostalCode' => $origin_zip,
			'CountryCode' => $origin_country
			)
	);
	
	$request['RequestedShipment']['Shipper'] = $freightbilling;
	$request['RequestedShipment']['Recipient'] = array(
		'Address' => array(
			'City' => $city,
			'StateOrProvinceCode' => $state,
			'PostalCode' => $postalCode,
			'CountryCode' => $countryCode,
			'Residential' => $resi
		)
	);
	
	if($origin_zip == '18974') $PaymentType = 'SENDER'; else $PaymentType = 'RECIPIENT';
	
	$request['RequestedShipment']['ShippingChargesPayment'] = array(
		'PaymentType' => $PaymentType, // valid values RECIPIENT, SENDER and THIRD_PARTY
		'Payor' => array(
			'ResponsibleParty' => array(
				'AccountNumber' => $shipaccount,
				'CountryCode' => 'US')
		)
	);
	
	if($origin_zip == '18974') $Role = 'SHIPPER'; else $Role = 'CONSIGNEE';
	if($origin_zip == '18974') $CollectTermsType = 'PREPAID'; else $CollectTermsType = 'COLLECT';
	
	$request['RequestedShipment']['FreightShipmentDetail'] = array(
		'FedExFreightAccountNumber' => $shipaccount,
		'FedExFreightBillingContactAndAddress' => getProperty('freightbilling'),
		'Role' => $Role,
		'PaymentType' => $CollectTermsType,
		'CollectTermsType' => 'STANDARD',
		'DeclaredValuePerUnit' => array(
			'Currency' => 'USD',
			'Amount' => 50
		),
		'LiabilityCoverageDetail' => array(
			'CoverageType' => 'NEW',
			'CoverageAmount' => array(
				'Currency' => 'USD',
				'Amount' => '50'
			)
		),
		'ShipmentDimensions' => array(
			'Length' => 85,
			'Width' => 43,
			'Height' => 15,
			'Units' => 'IN'
		),
		'ClientDiscountPercent' => 0,
		'PalletWeight' => array(
			'Units' => 'LB',
			'Value' => 20
		),
		'LineItems' => array(
			'FreightClass' => 'CLASS_085',
			'ClassProvidedByCustomer' => false,
			'Packaging' => 'PALLET',
			'Pieces' => 1,
			'Weight' => array(
				'Value' => $weightValue,
				'Units' => 'LB'
			)
		)
	);



	try{
		if(setEndpoint('changeEndpoint')){
			$newLocation = $client->__setLocation(setEndpoint('endpoint'));
		}
		/*print_r("<pre>");
		print_r($request);
		print_r("</pre>");*/
		$response = $client -> getRates($request);
		/*print_r("<pre>");
		print_r($response);
		print_r("</pre>");*/
			
		if ($response -> HighestSeverity != 'FAILURE' && $response -> HighestSeverity != 'ERROR'){
			
			$rateReply = $response -> RateReplyDetails;
			
			
			$TotalNetCharge = $response->RateReplyDetails->RatedShipmentDetails->ShipmentRateDetail->TotalNetCharge->Amount;
			
			$transitTime = $response->RateReplyDetails->CommitDetails->TransitTime;
			
			$Quotenumber = $response->RateReplyDetails->RatedShipmentDetails->ShipmentRateDetail->FreightRateDetail->QuoteNumber;

			if ($lift == '1')
			{
				$liftgate_reply = $response->RateReplyDetails->RatedShipmentDetails->ShipmentRateDetail->Surcharges;
				
				$lift_check_flag = false;
				
				foreach ($liftgate_reply as $liftgate_rep)
				{
					if ($liftgate_rep->SurchargeType == 'LIFTGATE_DELIVERY')
					{
						$lift_surcharge = $liftgate_rep->Amount->Amount;
						$message = "<br>Shipping Module message: Fedex returned liftgate surcharge: ".$liftgate_rep->Amount->Amount."<br>";
						//echo $message;
						$lift_check_flag = true;
					}
				}
				
				if (!$lift_check_flag)
				{
					$TotalNetCharge = $TotalNetCharge + $fedex_addon_cost_liftgate;
				}
			}
			
			/*print_r("<pre>");
			print_r($liftgate_reply);
			print_r("</pre>");*/
			
			 switch ($transitTime) {
			   	case 'ONE_DAY':
			   		$transitTime = '1';
			   		break;
			   	case 'TWO_DAYS':
			   		$transitTime = '2';
			   		break;
			   	case 'THREE_DAYS':
			   		$transitTime = '3';
			   		break;
			   	case 'FOUR_DAYS':
			   		$transitTime = '4';
			   		break;
			   	case 'FIVE_DAYS':
			   		$transitTime = '5';
			   		break;
			   	case 'SIX_DAYS':
			   		$transitTime = '6';
			   	default:
			   		$transitTime = 'N/A';
			   		break;
			   }
				$fedpal = array();
				$fedpal['transitTime']=	 $transitTime;		 	
				$fedpal['Quotenumber']=	 $Quotenumber;		 	
				$fedpal['totalcharge'] = $TotalNetCharge;
				return $fedpal;
		   
			//printSuccess($client, $response);
		}else{
			//printError($client, $response);
			return false;
		} 
		
		writeToLog($client);    // Write to log file   
	} catch (SoapFault $exception) {
	   printFault($exception, $client);        
	}
}
?>