<?php

class Calculate
{
	public static function Cost($name, $charge, $multiplier, $multiplierForWeb)
	{
	$cost = -1;
        switch($name)
        {
            case 'RL':
                $cost = $charge /100;
                break;

            case 'YELLOW-NONPAL':
                $cost = round($charge * $multiplier, 2);
                $numLen = strlen(strval($cost));
                if($numLen > 2){ //add . to cost
                    $numLen = $numLen - 2;
                    $cost = substr($cost, 0, $numLen).'.'.substr($cost, $numLen);
                }
                break;
            default:
                $cost = $charge * $multiplier;
            	break;
        }
        //$cost *= $multiplierForWeb;
		return $cost;
	}

    public static function PalletCost($weight, $rate, $multiplier, $multiplierForWeb)
    {
        $cost = $weight * $rate * $multiplier * $multiplierForWeb;
        return $cost;
    }
}

?>