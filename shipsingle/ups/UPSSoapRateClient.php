<?php
//GROUND
function getUPSRate($shipfromName, $addressFromAddressLine, $addressFromCity, $addressFromStateProvinceCode, $addressFromPostalCode, $addressFromCountryCode, $shiptoName, $addressToAddressLine, $addressToCity, $addressToStateProvinceCode, $addressToPostalCode, $addressToCountryCode, $totalWeight, $FreightClass, $lift, $resi)
{
//print_r('<br>UPS GROUND<br>');
    //Configuration
  $access = "DCF7FCB1FEA8AD66";
  $userid = "Marina_Luxaris";
  $passwd = "dora1234";
  $wsdl = "wsdl/ups/RateWS.wsdl";
  $operation = "ProcessRate";
  $endpointurl = 'https://wwwcie.ups.com/webservices/Rate';
  $outputFileName = "XOLTResult.xml";

    try
  {

    $mode = array
    (
         'soap_version' => 'SOAP_1_1',  // use soap 1.1 client
         'trace' => 1
    );

    // initialize soap client
  	$client = new SoapClient($wsdl , $mode);

  	//set endpoint url
  	$client->__setLocation($endpointurl);


    //create soap header
    $usernameToken['Username'] = $userid;
    $usernameToken['Password'] = $passwd;
    $serviceAccessLicense['AccessLicenseNumber'] = $access;
    $upss['UsernameToken'] = $usernameToken;
    $upss['ServiceAccessToken'] = $serviceAccessLicense;

    $header = new SoapHeader('http://www.ups.com/XMLSchema/XOLTWS/UPSS/v1.0','UPSSecurity',$upss);
    $client->__setSoapHeaders($header);


    //get response
  	$resp = $client->__soapCall($operation ,array(processRateUPS($shipfromName, $addressFromAddressLine, $addressFromCity, $addressFromStateProvinceCode, $addressFromPostalCode, $addressFromCountryCode, $shiptoName, $addressToAddressLine, $addressToCity, $addressToStateProvinceCode, $addressToPostalCode, $addressToCountryCode, $totalWeight, $FreightClass, $lift, $resi)));
	
	/*print_r('<pre>');
	print_r($resp);
	print_r('</pre>');*/
	
	if ($resp->Response->ResponseStatus->Description == 'Success')
	{
		return $resp->RatedShipment->TotalCharges->MonetaryValue;
	} else return false;
	

    //get status
    //echo "Response Status: " . $resp->Response->ResponseStatus->Description ."\n";

    //save soap request and response to file
   /* $fw = fopen($outputFileName , 'w');
    fwrite($fw , "Request: \n" . $client->__getLastRequest() . "\n");
    fwrite($fw , "Response: \n" . $client->__getLastResponse() . "\n");
    fclose($fw);*/

  }
  catch(Exception $ex)
  {
  	print_r ($ex);
  }
}

  function processRateUPS($shipfromName, $addressFromAddressLine, $addressFromCity, $addressFromStateProvinceCode, $addressFromPostalCode, $addressFromCountryCode, $shiptoName, $addressToAddressLine, $addressToCity, $addressToStateProvinceCode, $addressToPostalCode, $addressToCountryCode, $totalWeight, $FreightClass, $lift, $resi)
  {
      //create soap request
      $option['RequestOption'] = 'Rate';
      $request['Request'] = $option;

      $pickuptype['Code'] = '01';
      $pickuptype['Description'] = 'Daily Pickup';
      $request['PickupType'] = $pickuptype;

      $customerclassification['Code'] = '01';
      $customerclassification['Description'] = 'Classfication';
      $request['CustomerClassification'] = $customerclassification;

      $shipper['Name'] = $shipfromName;
      $shipper['ShipperNumber'] = 'DCF7FCB1FEA8AD66';
      $address['AddressLine'] = array
      (
          $addressFromAddressLine
      );
      $address['City'] = $addressFromCity;
      $address['StateProvinceCode'] = $addressFromStateProvinceCode;
      $address['PostalCode'] = $addressFromPostalCode;
      $address['CountryCode'] = $addressFromCountryCode;
      $shipper['Address'] = $address;
      $shipment['Shipper'] = $shipper;

      $shipto['Name'] = $shiptoName;
      $addressTo['AddressLine'] = $addressToAddressLine;
      $addressTo['City'] = $addressToCity;
      $addressTo['StateProvinceCode'] = $addressToStateProvinceCode;
      $addressTo['PostalCode'] = $addressToPostalCode;
      $addressTo['CountryCode'] = $addressToCountryCode;
	  if (isset($resi) && $resi == '1') $addressTo['ResidentialAddressIndicator'] = '1'; else $addressTo['ResidentialAddressIndicator'] = '0';
      $shipto['Address'] = $addressTo;
      $shipment['ShipTo'] = $shipto;

      $shipfrom['Name'] = $shipfromName;
      $addressFrom['AddressLine'] = array
      (
          $addressFromAddressLine
      );
      $addressFrom['City'] = $addressFromCity;
      $addressFrom['StateProvinceCode'] = $addressFromStateProvinceCode;
      $addressFrom['PostalCode'] = $addressFromPostalCode;
      $addressFrom['CountryCode'] = $addressFromCountryCode;
      $shipfrom['Address'] = $addressFrom;
      $shipment['ShipFrom'] = $shipfrom;

      $service['Code'] = '03';
      $service['Description'] = 'Service Code';
      $shipment['Service'] = $service;

      $packaging1['Code'] = '02';
      $packaging1['Description'] = 'Rate';
      $package1['PackagingType'] = $packaging1;
     /* $dunit1['Code'] = 'IN';
      $dunit1['Description'] = 'inches';
      $dimensions1['Length'] = '5';
      $dimensions1['Width'] = '4';
      $dimensions1['Height'] = '10';
      $dimensions1['UnitOfMeasurement'] = $dunit1;
      $package1['Dimensions'] = $dimensions1;*/
      $punit1['Code'] = 'LBS';
      $punit1['Description'] = 'Pounds';
      $packageweight1['Weight'] = $totalWeight;
      $packageweight1['UnitOfMeasurement'] = $punit1;
      $package1['PackageWeight'] = $packageweight1;

      $shipment['Package'] = array(	$package1 );
      $shipment['ShipmentServiceOptions'] = '';
      $shipment['LargePackageIndicator'] = '';
      $request['Shipment'] = $shipment;
      /*echo "Request.......\n";
      print_r($request);
      echo "\n\n";*/
      return $request;
  }
?>
