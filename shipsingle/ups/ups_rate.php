<?php
require_once('ups/UPSSoapRateClient.php');
require_once('ups/SoapGroundFreightRateClientSample.php');

function ups_rate($rn, $po, $shipfromName, $addressFromAddressLine, $addressFromCity, $addressFromStateProvinceCode, $addressFromPostalCode, $addressFromCountryCode, $shiptoName, $addressToAddressLine, $addressToCity, $addressToStateProvinceCode, $addressToPostalCode, $addressToCountryCode, $totalWeight, $FreightClass, $lift, $resi)
{
	
	$retVal = false;
	
	$shiptoName = strRepl($shiptoName);
	$addressToAddressLine = strRepl($addressToAddressLine);
	if ($totalWeight <= 150)
	{
		/*$retVal = getUPSRate($shipfromName, $addressFromAddressLine, $addressFromCity, $addressFromStateProvinceCode, $addressFromPostalCode, $addressFromCountryCode, $shiptoName, $addressToAddressLine, $addressToCity, $addressToStateProvinceCode, $addressToPostalCode, $addressToCountryCode, $totalWeight, $FreightClass, $lift, $resi);
		if ($retVal) $retVal = array('cost' => $retVal, 'type' => 'Ground');*/
		$retVal = false;
	} else 
	{
		//$totalWeight = $totalWeight + 70;
		//$NumberOfPieces = count_boxes($rn);
		$NumberOfPieces = ceil($totalWeight / 2500);
		//print_r('<br>boxes: '.$NumberOfPieces.'<br>');
		if (!empty($NumberOfPieces) && $NumberOfPieces > 0) $retVal = getUPSRateGround($shipfromName, $addressFromAddressLine, $addressFromCity, $addressFromStateProvinceCode, $addressFromPostalCode, $addressFromCountryCode, $shiptoName, $addressToAddressLine, $addressToCity, $addressToStateProvinceCode, $addressToPostalCode, $addressToCountryCode, $totalWeight, $NumberOfPieces, $FreightClass, $lift, $resi); else $retVal = false;
		if ($retVal) $retVal = array('cost' => $retVal, 'type' => 'LTL');
	}
	//print_r('UPS RESULT: '.$retVal);
	return $retVal;
}
?>