<?php
//LTL
function getUPSRateGround($shipfromName, $addressFromAddressLine, $addressFromCity, $addressFromStateProvinceCode, $addressFromPostalCode, $addressFromCountryCode, $shiptoName, $addressToAddressLine, $addressToCity, $addressToStateProvinceCode, $addressToPostalCode, $addressToCountryCode, $totalWeight, $NumberOfPieces, $FreightClass, $lift, $resi)
{
//print_r('<br>UPS LTL<br>');
/*echo $shipfromName."<br>";
echo $addressFromAddressLine."<br>";*/
/*echo $addressFromCity."<br>";
echo $addressFromStateProvinceCode."<br>";
echo $addressFromPostalCode."<br>";
echo $addressFromCountryCode."<br>";*/
/*echo $shiptoName."<br>";
echo $addressToAddressLine."<br>";*/
/*echo $addressToCity."<br>";
echo $addressToStateProvinceCode."<br>";
echo $addressToPostalCode."<br>";
echo $addressToCountryCode."<br>";
echo $totalWeight."<br>";
echo $NumberOfPieces."<br>";
echo $FreightClass."<br>";
echo $lift."<br>";
echo $resi."<br>";*/

$handlingunitoneQuantity = ceil($totalWeight / 2500);

//Configuration
  $access = "DCF7FCB1FEA8AD66";
  $userid = "Marina_Luxaris";
  $passwd = "dora1234";
  $wsdl = "wsdl/ups/FreightRate.wsdl";
  $operation = "ProcessFreightRate";
  $endpointurl = 'https://onlinetools.ups.com/webservices/FreightRate';
  $outputFileName = "XOLTResult.xml";
  
  try
  {

    $mode = array
    (
         'soap_version' => 'SOAP_1_1',  // use soap 1.1 client
         'trace' => 1
    );

    // initialize soap client
  	$client = new SoapClient($wsdl , $mode);

  	//set endpoint url
  	$client->__setLocation($endpointurl);


    //create soap header
    $usernameToken['Username'] = $userid;
    $usernameToken['Password'] = $passwd;
    $serviceAccessLicense['AccessLicenseNumber'] = $access;
    $upss['UsernameToken'] = $usernameToken;
    $upss['ServiceAccessToken'] = $serviceAccessLicense;

    $header = new SoapHeader('http://www.ups.com/XMLSchema/XOLTWS/UPSS/v1.0','UPSSecurity',$upss);
    $client->__setSoapHeaders($header);


    //get response
  	$resp = $client->__soapCall($operation ,array(processFreightRateUPS($shipfromName, $addressFromAddressLine, $addressFromCity, $addressFromStateProvinceCode, $addressFromPostalCode, $addressFromCountryCode, $shiptoName, $addressToAddressLine, $addressToCity, $addressToStateProvinceCode, $addressToPostalCode, $addressToCountryCode, $totalWeight, $NumberOfPieces, $FreightClass, $lift, $resi, $handlingunitoneQuantity)));

	/*print_r('<pre>');
	print_r($client->__getLastResponse());
	print_r('</pre>');*/
	
	if ($resp->Response->ResponseStatus->Description == 'Success')
	{
		/*print_r('<pre>');
		print_r($resp);
		print_r('</pre>');*/
		return $resp->TotalShipmentCharge->MonetaryValue;
	} else return false;
	
    //get status
    //echo "Response Status: " . $resp->Response->ResponseStatus->Description ."\n";

    //save soap request and response to file
   /* $fw = fopen($outputFileName , 'w');
    fwrite($fw , "Request: \n" . $client->__getLastRequest() . "\n");
    fwrite($fw , "Response: \n" . $client->__getLastResponse() . "\n");
    fclose($fw);*/

  }
  catch(Exception $ex)
  {
  	//print_r ($ex);
  }
}

  function processFreightRateUPS($shipfromName, $addressFromAddressLine, $addressFromCity, $addressFromStateProvinceCode, $addressFromPostalCode, $addressFromCountryCode, $shiptoName, $addressToAddressLine, $addressToCity, $addressToStateProvinceCode, $addressToPostalCode, $addressToCountryCode, $totalWeight, $NumberOfPieces, $FreightClass, $lift, $resi, $handlingunitoneQuantity)
  {

      //create soap request
      $option['RequestOption'] = '1';
      $request['Request'] = $option;
      $shipfrom['Name'] = $shipfromName;
      $addressFrom['AddressLine'] = $addressFromAddressLine;
      $addressFrom['City'] = $addressFromCity;
      $addressFrom['StateProvinceCode'] = $addressFromStateProvinceCode;
      $addressFrom['PostalCode'] = $addressFromPostalCode;
      $addressFrom['CountryCode'] = $addressFromCountryCode;
      $shipfrom['Address'] = $addressFrom;
      $request['ShipFrom'] = $shipfrom;

      $shipto['Name'] = $shiptoName;
      $addressTo['AddressLine'] = $addressToAddressLine;
      $addressTo['City'] = $addressToCity;
      $addressTo['StateProvinceCode'] = $addressToStateProvinceCode;
      $addressTo['PostalCode'] = $addressToPostalCode;
      $addressTo['CountryCode'] = $addressToCountryCode;
      $shipto['Address'] = $addressTo;
      $request['ShipTo'] = $shipto;

      $payer['Name'] = $shipfromName;
      $addressPayer['AddressLine'] = $addressFromAddressLine;
      $addressPayer['City'] = $addressFromCity;
      $addressPayer['StateProvinceCode'] = $addressFromStateProvinceCode;
      $addressPayer['PostalCode'] = $addressFromPostalCode;
      $addressPayer['CountryCode'] = $addressFromCountryCode;
      $payer['Address'] = $addressPayer;
      $shipmentbillingoption['Code'] = '10';
      $shipmentbillingoption['Description'] = 'PREPAID';
      $paymentinformation['Payer'] = $payer;
      $paymentinformation['ShipmentBillingOption'] = $shipmentbillingoption;
      $request['PaymentInformation'] = $paymentinformation;

      $service['Code'] = '308';
      $service['Description'] = 'UPS Freight LTL';
      $request['Service'] = $service;

      $handlingunitone['Quantity'] = $handlingunitoneQuantity;
      $handlingunitone['Type'] = array
      (
          'Code' => 'PLT',
          'Description' => 'PALLET'
      );
      $request['HandlingUnitOne'] = $handlingunitone;

      $commodity['CommodityID'] = '';
      $commodity['Description'] = 'No Description';
      $commodity['Weight'] = array
      (
         'UnitOfMeasurement' => array
         (
             'Code' => 'LBS',
             'Description' => 'Pounds'
         ),
         'Value' => $totalWeight
      );
      /*$commodity['Dimensions'] = array
      (
          'UnitOfMeasurement' => array
          (
              'Code' => 'IN',
              'Description' => 'Inches'
          ),
          'Length' => '23',
          'Width' => '17',
          'Height' => '45'
      );*/
      $commodity['NumberOfPieces'] = $NumberOfPieces;
      $commodity['PackagingType'] = array
      (
           'Code' => 'PLT',
           'Description' => 'Pallet'
      );
      /*$commodity['DangerousGoodsIndicator'] = '';
      $commodity['CommodityValue'] = array
      (
           'CurrencyCode' => 'USD',
           'MonetaryValue' => '5670'
      );*/
      $commodity['FreightClass'] = $FreightClass;
      //$commodity['NMFCCommodityCode'] = '';
      $request['Commodity'] = $commodity;
	
	  $shipmentserviceoptions['PickupOptions'] = array();
	  
	  if ($lift == '1')
	  {
		//echo "<br><br>lift: 1<br>";
		$shipmentserviceoptions['PickupOptions']['LiftGateRequiredIndicator'] = '';
	  } //else echo "lift: 0<br>";
	  
	   if ($resi == '1')
	  {
		//echo "resi: 1<br>";
		$shipmentserviceoptions['PickupOptions']['ResidentialPickupIndicator'] = '';
	  } //else echo "resi: 0<br>";
	  
      /*$shipmentserviceoptions['PickupOptions'] = array
      (
	  		'ResidentialPickupIndicator' => '',
	  		'LiftGateRequiredIndicator' => ''
	  );*/
	  /*$shipmentserviceoptions['OverSeasLeg'] = array
	  (
	         'Dimensions' => array
	         (
	              'Volume' => '20',
	              'UnitOfMeasurement' => array
	              (
	                  'Code' => 'CF',
	                  'Description' => 'String'
	              )
	         ),
	         'Value' => array
	         (
	              'Cube' => array
	              (
	                   'CurrencyCode' => 'USD',
	                   'MonetaryValue' => '5670'
	              )
	         ),
	         'COD' => array
	         (
	              'CODValue' => array
	              (
	                   'CurrencyCode' => 'USD',
	                   'MonetaryValue' => '363'
	              ),
	              'CODPaymentMethod' => array
	              (
	                   'Code' => 'M',
	                   'Description' => 'For Company Check'
	              ),
	              'CODBillingOption' => array
	              (
	                   'Code' => '01',
	                   'Description' => 'Prepaid'
	              ),
	              'RemitTo' => array
	              (
	                   'Name' => 'RemitToSomebody',
	                   'Address' => array
	                   (
	                         'AddressLine' => '640 WINTERS AVE',
	  						 'City' => 'PARAMUS',
	  					     'StateProvinceCode' => 'NJ',
	  					     'PostalCode' => '07652',
	  					     'CountryCode' => 'US'
	  				   ),
	  				   'AttentionName' => 'C J Parker'
	  			  )
	  		  ),
	  		  'DangerousGoods' => array
	  		  (
					'Name' => 'Very Safe',
					'Phone' => array
					(
						'Number' => '453563321',
						'Extension' => '1111'
					),
					'TransportationMode' => array
					(
						'Code' => 'CARGO',
						'Description' => 'Cargo Mode'
					)
	  		  ),
	  		  'SortingAndSegregating' => array
	  		  (
	  				'Quantity' => '23452'
	  		  ),
	  		  'CustomsValue' => array
	  		  (
	  				'CurrencyCode' => 'USD',
	  				'MonetaryValue' => '23457923'
	  		  ),
	  		  'HandlingCharge' => array
	  		  (
					'Amount' => array
					(
						'CurrencyCode' => 'USD',
						'MonetaryValue' => '450'
					)
	  		  )
	   );*/
	  $request['ShipmentServiceOptions'] = $shipmentserviceoptions;

      /*echo "Request.......\n";
      print_r($request);
      echo "\n\n";*/
      return $request;
  }
?>
