<?php
$path = $_SERVER['DOCUMENT_ROOT'];
require_once($path.'/shipsingle/functions.php');
function bigFunc($orders, $conn, $originZip, $originStatecode, $originCountry, $originCity, $FreightClass, $rl_pal_addon_cost_liftgate, $rl_pal_addon_cost_residential, $fedex_addon_cost_liftgate, $ziphead_not_allowed_RL, $rate_ups)
{
	/*print_r('<pre>');
	print_r($orders);
	print_r('<br>');
	print_r($originZip);
	print_r('<br>');
	print_r($originStatecode);
	print_r('<br>');
	print_r($originCountry);
	print_r('<br>');
	print_r($originCity);
	print_r('<br>');
	print_r($FreightClass);
	print_r('<br>');
	print_r($rl_pal_addon_cost_liftgate);
	print_r('<br>');
	print_r($rl_pal_addon_cost_residential);
	print_r('<br>');
	print_r($fedex_addon_cost_liftgate);
	print_r('<br>');
	print_r($ziphead_not_allowed_RL);
	print_r('<br>');
	print_r($rate_ups);
	print_r('</pre>');*/
	
$orders = array($orders['RefNumber'] => $orders);

    $zipCodeOrigin = array(
        "zip"      => $originZip,
        "statecode"    => $originStatecode,
        "country"  => $originCountry,
        "city"     => $originCity,
		"addr" => '75 Hawk Rd'
    );
	
	//print_r($zipCodeOrigin);
	
 //-------------------------------------------------------------	
    require_once('helpers/carrierHelper.php');
    require_once("mappers/autoloader.php");     //Autoloader for mappers classes
    //models
    require_once("model/HttpCarrier.php");
    require_once("model/SoapCarrier.php");
    require_once("model/NonPalletShipper.php");
    require_once("model/PalletShipper.php");
    
//Helpers used for shipping
    require_once("helpers/Calculate.php");     
    require_once('helpers/fedex.php');
	require_once('ups/ups_rate.php');
	 require_once('helpers/ratingLTLODFL.php');
    require_once('fedex/fedex-common.php5');

    //Helpers used for requests
    require_once("helpers/UrlParser.php");
    require_once("helpers/MultiRequest.php");
    require_once("helpers/Transfer.php");
    require_once("helpers/ShippingAdvisor.php");
    //mapper error
	require_once("mappers/ErrorMapper.php");
    //Helper objects:
        
    $multiRequester = new MultiRequest();
    $xmlParser = new XMLparser();
    $advisor = new ShippingAdvisor();
    $erroMapper = new ErrorMapper(); 
    $originLocations = json_decode(file_get_contents('config/config.originLocations.json'), true);
    $noPalletCarriers = json_decode(file_get_contents('config/config.noPalletCarriers.json'), true);
    $PalletCarriers = json_decode(file_get_contents('config/config.PalletCarriers.json'), true);

	$query = "SELECT id, IsPallet, Contact, Multiplier, MaxWeight, preference, MultiplierWeb
		  FROM ".CARRIER_PROFILE."
		  WHERE IsActive = 1";
    $stmt = $conn->prepare($query); $stmt->execute();
    $result = $stmt->fetch(PDO::FETCH_ASSOC); 

    while ($result) 
    {
		$carrierOnDb[$result['Contact']] = $result;
		$result = $stmt->fetch(PDO::FETCH_ASSOC); 
	}
    
    $i = 0;
    $t = 0;
    $order = array();
    $ship = array();
    $errors = array();
    foreach ($orders as $key => $value) {
		if(extrCountryCode($value['ShipPostalCode']) == 'US') $value['ShipPostalCode'] = substr($value['ShipPostalCode'], 0, 5);
		//print_r($value['ShipPostalCode']);
		
		if ($originZip != '18974')
		{
			$zipCodeOrigin = getZipInfo('US', $originZip);
			$zipCodeOrigin['country'] = validate_state_code($zipCodeOrigin['country']);
			$zipCodeOrigin['statecode'] = $zipCodeOrigin['state'];
			$zipCodeOrigin['city'] = urldecode($zipCodeOrigin['city']);
			/*print_r('<pre>');
			print_r($zipCodeOrigin);
			print_r('</pre>');*/
			if (!isset($zipCodeOrigin['zip']) || empty($zipCodeOrigin['zip'])
			|| !isset($zipCodeOrigin['statecode']) || empty($zipCodeOrigin['statecode'])
			|| !isset($zipCodeOrigin['country']) || empty($zipCodeOrigin['country'])
			|| !isset($zipCodeOrigin['city']) || empty($zipCodeOrigin['city']))
			{
					$messages = array(array(
						'po' => $value['PONumber'],
						'missing_field' => 'N/A',
						'reference_name' => 'shiptrack.RefNumber',
						'reference_value' => $value['RefNumber'],
						'message' => 'Google does not recognize this as zip code: '.$originZip
					));
					saveToValidationLog($conn, $messages);
					display_message("Google does not recognize this as zip code: ".$originZip);
			}
		}
		
        if (!$value['ShipPostalCode'] || !$value['weight']) 
        {
            $errors[$value['ID']][$t++] = array(
                            'ID' => $value['ID'],
                            'ERROR' => 'Missing zip code or weight'
            		);
			display_message('Missing zip code or weight!');
        } 
        else 
        {
            $dealer = substr($value["dealerName"],0,4);
			
			/*print_r($value['ShipPostalCode']);
			print_r($value['ShipAddress_City']);
			print_r($value['ShipAddress_State']);
			print_r($value['ShipAddress_Country']);*/
			
			if (isset($value['ShipPostalCode']) && !empty($value['ShipPostalCode'])
			&& isset($value['ShipAddress_City']) && !empty($value['ShipAddress_City'])
			&& isset($value['ShipAddress_State']) && !empty($value['ShipAddress_State'])
			&& isset($value['ShipAddress_Country']) && !empty($value['ShipAddress_Country']))
			{
				//echo '<br>zip ok<br>';
				$zipCodeDest = Array(
				'zip' => $value['ShipPostalCode'],
				'state' => $value['ShipAddress_State'],
				'country' => $value['ShipAddress_Country'],
				'city' => $value['ShipAddress_City']
				);
				$zipCodeDest['country'] = validate_state_code($zipCodeDest['country']);
			} else
			{
				//echo '<br>zip not set<br>';
				$tempCountryCode = extrCountryCode($value['ShipPostalCode']);
				$zipCodeDest = getZipInfo($tempCountryCode, $value['ShipPostalCode']);
				$zipCodeDest['country'] = validate_state_code($zipCodeDest['country']);
			}

			 if (!isset($zipCodeDest['zip']) || empty($zipCodeDest['zip'])
			|| !isset($zipCodeDest['state']) || empty($zipCodeDest['state'])
			|| !isset($zipCodeDest['country']) || empty($zipCodeDest['country'])
			|| !isset($zipCodeDest['city']) || empty($zipCodeDest['city']))
			{
				$messages = array(array(
					'po' => $value['PONumber'],
					'missing_field' => 'N/A',
					'reference_name' => 'shiptrack.RefNumber',
					'reference_value' => $value['RefNumber'],
					'message' => 'Google does not recognize this as zip code: '.$value['ShipPostalCode']
				));
				saveToValidationLog($conn, $messages);
				display_message("Google does not recognize this as zip code: ".$value['ShipPostalCode']);
				/*print_r('<pre>');
				print_r($zipCodeDest);
				print_r('</pre>');*/
				
			}else 
                        {
							//print_r($zipCodeDest);
							if (((extrStore($value["dealerName"]) == 'Lowe' && $value['lowes_mode'] == 'residential') || extrStore($value["dealerName"]) != 'Lowe') && $rate_ups != 'False')
							{
								if ($originZip == '18974')
								{
									$ship_from = 'Bathauthority';
								} else 
								{
									$ship_from = 'Dealer';
								}
								
								if (!isset($value['ShipAddress_Addr1']) || empty($value['ShipAddress_Addr1'])) $value['ShipAddress_Addr1'] = 'Dealer';
								if (!isset($value['ShipAddress_Addr2']) || empty($value['ShipAddress_Addr2'])) $value['ShipAddress_Addr2'] = $zipCodeDest['addr'];

									
								$UPSresult = ups_rate($value['RefNumber'], $value['PONumber'], $ship_from, $zipCodeOrigin['addr'], $zipCodeOrigin['city'], $zipCodeOrigin['statecode'], $zipCodeOrigin['zip'], $zipCodeOrigin['country'], $value['ShipAddress_Addr1'], $value['ShipAddress_Addr2'], $zipCodeDest['city'], $zipCodeDest['state'], $zipCodeDest['zip'], $zipCodeDest['country'], $value['weight'], '70', $value['liftgate'],$value['residential']);
                                                                /*print_r('<pre>');
                                                                print_r($value);
                                                                print_r($zipCodeOrigin);
                                                                print_r($zipCodeDest);
                                                                print_r($UPSresult);
                                                                print_r('</pre>');*/
											if ($UPSresult != false)
											{
												//print_r('<br>UPS result cost: '.$UPSresult['cost'].'<br>');
												//print_r('<br>UPS result type: '.$UPSresult['type'].'<br>');
												$carrier_ups = 'UPS';
												if($UPSresult['type'] == 'Ground') $carrier_ups = 'UPS Ground';
												$ship['UPS0'] = Array(
														'QUOTE' => 'N/A',
														'DAYS' => 'N/A',
														'NAME' => 'UPS0',
														'TYPE' => 'Standard',
														'FROMZIP' => $zipCodeOrigin['zip'],
														'FROMSTATE' => $zipCodeOrigin['statecode'],
														'ERROR_MESSAGE' => '',
														'LOGO' => '',
														'CARRIERNAME' => $carrier_ups,
														'QUOTENUMBER' => 'N/A',
														'TRANSITTIME' => '1',
														'COST' => $UPSresult['cost']
													);
											}
							}
						
                          foreach ($carrierOnDb as $name => $shipper) {
						  if ($name == 'ODFL' && $value["dealerName"] != 'Amazon' && ((extrStore($value["dealerName"]) == 'Lowe' && $value['lowes_mode'] == 'residential') || extrStore($value["dealerName"]) != 'Lowe'))
									{
										$ODFLresult = getLTL_ODFL_Rate($zipCodeOrigin['zip'], $zipCodeDest['zip'], $FreightClass, $value['weight'], $value['liftgate'], $value['residential']);
										if ($ODFLresult != false)
										{
											//print_r($ODFLresult);
											$ship['ODFL0'] = Array(
													'QUOTE' => $ODFLresult['Quotenumber'],
													'DAYS' => $ODFLresult['transitTime'],
													'NAME' => 'ODFL0',
													'TYPE' => 'Standard',
													'FROMZIP' => $zipCodeOrigin['zip'],
													'FROMSTATE' => $zipCodeOrigin['statecode'],
													'ERROR_MESSAGE' => '',
													'LOGO' => '',
													'CARRIERNAME' => 'ODFL',
													'QUOTENUMBER' => $ODFLresult['Quotenumber'],
													'TRANSITTIME' => $ODFLresult['transitTime'],
													'COST' => $ODFLresult['totalcharge']
												);
										}
									} else
									{
						  $ziphead = substr($zipCodeDest['zip'], 0, 3);

						   if ((extrCarrier($name) != 'FEDEX' && $value["dealerName"] == 'Amazon') || 
						   (extrCarrier($name) == 'RL' && in_array($ziphead, $ziphead_not_allowed_RL)))
							{

							} else
							{
                            if ($shipper["IsPallet"] == 0) 
                            {
								if (extrCarrier($name) == 'RL' && isset($value['residentialFedex']) && ($value['residentialFedex'] == '1' || $value['residentialFedex'] == '0'))
								{
									$residential = $value['residentialFedex'];
								} else
								{
									$residential = $value['residential'];
								}
                                $options = array(
                                        'liftgate'       => $value['liftgate'],
                                        'residential'    => $residential,
                                        'respickup'      => FALSE,
                                        'callondelivery' => FALSE,
                                        'inside'         => FALSE,
                                );
									
                            		$carrier = createCarrier($name, $noPalletCarriers, $zipCodeOrigin['zip'], $originLocations);
                            		$shipp = new NonPalletShipper($name.$i, $carrier, $zipCodeOrigin, $zipCodeDest, $value['weight'], $FreightClass, $options);
									
                                        $shipp->setMultiplier(isset($carrierOnDb['Multiplier']) ? $carrierOnDb['Multiplier'] : 1); 
										
                                        if($carrier instanceof HttpCarrier)	
                                        {
                                           UrlParser::makeUrlComplete($shipp, $originLocations);
                                           $url = $carrier->getUrl();
										   
										  /* print_r('<pre>');
										   print_r($url);
										   print_r('</pre>');*/
										   
                                           $result = $carrier->getResponse($url, $xmlParser);
											
                                           if (count($result['result'])<2) 
                                           {
                                              $status = $erroMapper->convert($result, $carrier->getErrorMessage());
                                              $result['status'] = $status['ERROR'];
                                              $errors[$value['ID']][$name] = array(
                                                      'ID' => $value['ID'],
                                                      'ERROR' => $status['ERROR']
                                              );
                                            } 
                                            else 
                                            {
                                              $result['status'] = 'ok';
                                            }
                    	                      if (count($result['result']) != 0) 
                                            {
                    	                       $shipp->parseCarrierResponce($result);
                    	                      }
                                        } 
                                        else if($carrier instanceof SoapCarrier) 
                                        {
                                           $rateRequest = UrlParser::makeSoapRequest($shipp);
                                           $soapResponse = Transfer::getSOAPResponse($carrier->getUrl(), $carrier->getTimeout(), $carrier->getNamespace(), $rateRequest);

                                           if($soapResponse['status'] != 'ok') {

                                           }
                                           $shipp->parseCarrierResponce($soapResponse);
                                        }
                                       if ( $dealer == "Lowe") {
                                        if ($value['lowes_mode'] == 'residential')
											{
												$ship[$name.$i] = $shipp->getResult();
											} else if ( $value['lowes_mode'] == 'commercial' && (($name == "FEDEX FREIGHT") || ($name == "FEDEX-PRIORITY") || ($name == "FEDEX-ECONOMY") || ($name == "RL")))
											{
												$ship[$name.$i] = $shipp->getResult();
											}
                                       }
                                     else {
                                         $ship[$name.$i] = $shipp->getResult();
                                     }
                                    
                                    } else 
                                    {
                                      if ($name != 'RL-PAL') 
                                      {
                      									  $stmt =  $conn->prepare("SELECT State_Code FROM ".ZIP_CODE_FOR_NY." WHERE State_Code LIKE ? and Postal = ?");
                      									  $stmt->execute(array($zipCodeDest['state'], $zipCodeDest['zip']));
                      								      $result = $stmt->fetch(PDO::FETCH_ASSOC); 
                      									  if (!empty($result)){
                      										  	$zipNY = TRUE;
                      										} else {
                      											$zipNY = FALSE;
                      										} 									  
                                    }
                                    if ((!$zipNY) || (($zipNY) && ($name != 'RL-PAL'))) 
                                    {
                                       $shipp = new PalletShipper($name.$i, $name, $zipCodeOrigin, $zipCodeDest, $value['weight'], NULL);
                                       $shipp->setMultiplier(isset($shipper['Multiplier']) ? $shipper['Multiplier'] : 1);
                                       $shipp->setMaxWeight($shipper['MaxWeight']);
                                       $pallets = intval(ceil($value['weight'] / $shipper['MaxWeight']));
                                       $shipp->setPallets($pallets);
									   
									   

                                      if ($name == 'FEDEX-ECONOMY'/* && $originZip == '18974'*/)
                                        {
                                          $zipCodeDest['city'] = str_replace('%20', ' ', $zipCodeDest['city']);
										  
                                         $fedpaldata = getFedExRate($zipCodeDest['city'], $zipCodeDest['state'], $zipCodeDest['zip'], $zipCodeDest['country'], $value['weight'], $value['residential'], $value['liftgate'], $fedex_addon_cost_liftgate, $zipCodeOrigin['addr'], $zipCodeOrigin['city'], $zipCodeOrigin['statecode'], $zipCodeOrigin['zip'], $zipCodeOrigin['country']);
										 
										 
                                          $cost = $shipp->getFedexResult($fedpaldata['transitTime'], $fedpaldata['Quotenumber'], $fedpaldata['totalcharge']);
										  
                                        }
                                      else if ($name != 'FEDEX-ECONOMY' && $originZip == '18974')
                                      {
                                          $zip = str_split($zipCodeDest['zip'], 3);
										  if ($name == 'YELLOW - PAL')
										  {
											  $stmt =  $conn->prepare("SELECT Rate FROM ".PALLETRATES." WHERE CarierProfileId = ? AND state = ?");
											  $stmt->execute(array($shipper['id'],$zipCodeDest['state']));
 
											  $result = $stmt->fetch(PDO::FETCH_ASSOC);
										  } else
										  {
											  // LEADING ZERO CONVERTION
											  $intZip = $zip[0];
											  try { $intZip = intval($zip[0]); }
											  catch( Exception $e ) { $intZip = $zip[0]; }
											  // SQL DOES NOT PROCESS ZIPHEAD WITH LEADING ZEROS 
											  // I.E. 014 != 14 IN SQL
											  
											  $stmt =  $conn->prepare("SELECT Rate FROM ".PALLETRATES." WHERE CarierProfileId = ? AND state = ? AND ziphead = ?");
											  $stmt->execute(array( $shipper['id'], $zipCodeDest['state'], $intZip ));
											  $result = $stmt->fetch(PDO::FETCH_ASSOC);
										  }

                                          $rate = $result['Rate'];
										  
										  if ($name == 'RL-PAL')
										  {
											if ($value['liftgate']) $rate = $rate + $rl_pal_addon_cost_liftgate;
											if ($value['residential']) $rate = $rate + $rl_pal_addon_cost_residential;
										  }
										  
										 
                                           if (is_null($rate)) { $rate = 1; }
                                           if($pallets == 1) $rate = floatval($rate);
                                           if($pallets == 2) $rate = floatval($rate);
                                           if($pallets >= 3) $rate = floatval($rate);
                                           $shipp->setRate($rate);
                                           $cost = $shipp->getResult();
                                      }  
										/*print_r('<pre>');
										print_r($cost["COST"]);
										print_r('<br>');
										print_r($dealer);
										print_r('<br>');
										print_r($value['lowes_mode']);
										print_r('<br>');
										print_r($name);
										print_r('</pre>');*/
                                     if ($cost["COST"] > 1.5) 
                                     {
                                         if ( $dealer == "Lowe"){
                                            if ($value['lowes_mode'] == 'residential')
											{
												if ($name == "FEDEX-ECONOMY")
												{
													$ship[$name.$i] = $cost;
												}	else
												{
													$ship[$name.$i] = $shipp->getResult();
												}
											} else if ( $value['lowes_mode'] == 'commercial')
											{
												if  ($name == "FEDEX FREIGHT" || $name == "FEDEX-PRIORITY" || $name == "RL" || $name == "RL-PAL")
												{
													$ship[$name.$i] = $shipp->getResult();
												}else if ($name == "FEDEX-ECONOMY")
												{
													$ship[$name.$i] = $cost; 
												}
											}
                                         } else {
                                              $ship[$name.$i] = $cost; 
                                            }
                                      }
									  //print_r($ship);
                                    }
                               }
							   }
							 }
                           }
                        $order[][$value['ID']] = array($ship, "PONumber" => $value["PONumber"], "weight" => $value["weight"], 'tozip' => $value["ShipPostalCode"], 'dealerName' => $value["dealerName"]);
						/*print_r('<pre>');
						print_r($ship);
						print_r('</pre>');*/
                        unset($ship);
                      }
                    }
		
                    $i++;
	}
	
	
	
$allShipers = array();
for ($j=0; $j<count($order); $j++) {
   foreach ($order[$j] as $id => $shipers) {
        foreach ($shipers[0] as $shiperinfo) {
            $allShipers[][$id] = array(
                $id, 
                $shiperinfo['FROMZIP'],
                $shipers['tozip'],
                $shiperinfo['CARRIERNAME'],
                $shiperinfo['QUOTENUMBER'],
                $shiperinfo['TRANSITTIME'],
                $shiperinfo['COST'],
                $shipers['PONumber'],
                $shipers['weight'],
				$shipers['dealerName']
            );
        }
        $bestShipper = $advisor->choose_best($shipers[0]);
        $bestShipperResults = $shipers[0][$bestShipper];

		$bestOforders[][$id] = array(
			$id, 
			$bestShipperResults['FROMZIP'],
			$shipers['tozip'],
			$bestShipperResults['CARRIERNAME'],
			$bestShipperResults['QUOTENUMBER'],
			$bestShipperResults['TRANSITTIME'],
			$bestShipperResults['COST'],
			$shipers['PONumber'],
			$shipers['weight'],
			$shipers['dealerName']
            );
	
		$cheaporder[] = array('id'=> $id, 'origin'=> $bestShipperResults['FROMZIP'], 'dest'=> $shipers['tozip'], 'carrier'=> $bestShipperResults['CARRIERNAME'], 'quote'=> $bestShipperResults['QUOTENUMBER'], 'time'=> $bestShipperResults['TRANSITTIME'], 'cost'=> $bestShipperResults['COST'], 'po'=> $shipers['PONumber'], 'weight'=> $shipers['weight'], 'dealerName'=> $shipers['dealerName']);        
			
    }
}
$otherResults = extrOtherResults($allShipers);
writeOtherResults($conn, $otherResults);
$allFinalResults = Array
(
	'cheaporder' => $cheaporder,
	'otherResults' => $otherResults
);
/*print_r('<pre>');
print_r($allFinalResults);
print_r('</pre>');*/
return $allFinalResults;
}


?>