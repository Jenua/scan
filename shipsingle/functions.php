<?php
$path = $_SERVER['DOCUMENT_ROOT'];
require_once($path.'/db_connect/connect.php');

function change_carrier_log($conn, $Ref, $po, $old_CARRIERNAME, $old_COST, $old_TRANSITTIME, $old_PRO, $Carrier, $Time, $Cost, $user)
{
	$query = "INSERT INTO [dbo].[change_carrier_log]
           ([POnumber]
           ,[RefNumber]
           ,[old_carriername]
           ,[old_cost]
           ,[old_transittime]
           ,[old_pro]
           ,[new_carriername]
           ,[new_cost]
           ,[new_transittime]
           ,[change_carrier_date]
           ,[userName])
     VALUES
           (".$conn->quote($po)."
           ,".$conn->quote($Ref)."
           ,".$conn->quote($old_CARRIERNAME)."
           ,".$conn->quote($old_COST)."
           ,".$conn->quote($old_TRANSITTIME)."
           ,".$conn->quote($old_PRO)."
           ,".$conn->quote($Carrier)."
           ,".$conn->quote($Cost)."
           ,".$conn->quote($Time)."
           ,GETDATE()
           ,".$conn->quote($user).")";
		   
	$result = $conn->prepare($query);
	$result->execute();
	return $result;
}

function update_linedetail_weight($item_id, $item_weight)
{
	$conn = getConnection();
	$query = "UPDATE [dbo].[linedetail]
   SET [CustomField9] = ".$conn->quote($item_weight)."
 WHERE [id] = ".$conn->quote($item_id)."";
	$result = $conn->prepare($query);
	$result->execute();
}

function count_boxes($rn)
{
	//print_r('count_boxes');
	$conn = getConnection();
	$query = "SELECT PARSENAME(REPLACE(linedetail.ItemRef_FullName, ':', '.'), 1) AS ProductCode
 ,linedetail.quantity
FROM linedetail
INNER JOIN groupdetail ON linedetail.GroupIDKEY = groupdetail.TxnLineID
INNER JOIN shiptrack ON shiptrack.TxnID = linedetail.IDKEY
WHERE linedetail.ItemRef_FullName NOT LIKE '%Price-Adjustment%'
 AND linedetail.ItemRef_FullName NOT LIKE '%Freight%'
 AND linedetail.ItemRef_FullName NOT LIKE '%warranty%'
 AND linedetail.ItemRef_FullName IS NOT NULL
 AND linedetail.ItemRef_FullName NOT LIKE '%Subtotal%'
 AND linedetail.ItemRef_FullName NOT LIKE '%IDSC-10%'
 AND linedetail.ItemRef_FullName NOT LIKE '%DISCOUNT%'
 AND linedetail.ItemRef_FullName NOT LIKE '%SPECIAL ORDER%'
 AND linedetail.CustomField9 IS NOT NULL
 AND shiptrack.RefNumber = '".$rn."'
UNION ALL
SELECT PARSENAME(REPLACE(manually_linedetail.ItemRef_FullName, ':', '.'), 1) AS ProductCode
 ,manually_linedetail.quantity
FROM manually_linedetail
INNER JOIN manually_groupdetail ON manually_linedetail.GroupIDKEY = manually_groupdetail.TxnLineID
INNER JOIN manually_shiptrack ON manually_shiptrack.TxnID = manually_linedetail.IDKEY
WHERE manually_linedetail.ItemRef_FullName NOT LIKE '%Price-Adjustment%'
 AND manually_linedetail.ItemRef_FullName NOT LIKE '%Freight%'
 AND manually_linedetail.ItemRef_FullName NOT LIKE '%warranty%'
 AND manually_linedetail.ItemRef_FullName IS NOT NULL
 AND manually_linedetail.ItemRef_FullName NOT LIKE '%Subtotal%'
 AND manually_linedetail.ItemRef_FullName NOT LIKE '%IDSC-10%'
 AND manually_linedetail.ItemRef_FullName NOT LIKE '%DISCOUNT%'
 AND manually_linedetail.ItemRef_FullName NOT LIKE '%SPECIAL ORDER%'
 AND manually_linedetail.CustomField9 IS NOT NULL
 AND manually_shiptrack.RefNumber = '".$rn."'";
	$result = $conn->prepare($query);
	$result->execute();
	$result = $result->fetchAll(PDO::FETCH_ASSOC);
	//print_r($result);
	if (!empty($result))
	{
		$boxes = 0;
		foreach ($result as $res)
		{
			if ($res['ProductCode']=='SHST-01-TK' || $res['ProductCode']=='SHST-01-PL')
			{
				$boxes = $boxes + ceil((integer)$res['quantity'] / 5);
			} else 
			{
				$boxes = $boxes + ($res['quantity']*$res['quantity']);
			}
		}
		return $boxes;
	} else return false;
}

function getPallets($weight)
{
	if (isset($weight) && !empty($weight))
	{
		$pallets = ceil($weight / 2500);
	} else
	{
		$pallets = 1;
	}
	return $pallets;
}

function runQuery($conn, $query)
{
	try
	{
		$stmt = $conn->prepare($query);
		$result = $stmt->execute();
		return $result;
	}catch (Exception $e) {
		print_r($query);
		display_message($e);
		return false;
	}
}

function strRepl($value)
{
	$value = (string)$value;
	$value = str_replace("'", "`", $value);
	$value = "'".$value."'";
	return $value;
}

function writeOtherResults($conn, $otherResults)
{
	/*print_r('<pre>');
	print_r($otherResults);
	print_r('</pre>');*/
	foreach($otherResults as $otherResult)
	{
		$otherResult['id'] = strRepl($otherResult['id']);
		$otherResult['po'] = strRepl($otherResult['po']);
		$otherResult['carrier'] = strRepl($otherResult['carrier']);
		$otherResult['cost'] = strRepl($otherResult['cost']);
		$otherResult['time'] = strRepl($otherResult['time']);
		$otherResult['quote'] = strRepl($otherResult['quote']);
		
		$query = "INSERT INTO [dbo].[".ALLCOSTS."]
			   ([ID]
			   ,[CARRIERNAME]
			   ,[PONUMBER]
			   ,[TRANSITTIME]
			   ,[COST]
			   ,[QUOTE]
			   ,[DATE])
		 VALUES
			   (".$otherResult['id']."
			   ,".$otherResult['carrier']."
			   ,".$otherResult['po']."
			   ,".$otherResult['time']."
			   ,".$otherResult['cost']."
			   ,".$otherResult['quote']."
			   ,GETDATE())";

		runQuery($conn, $query);
	}
}

//Extract store name from CustomerRef_FullName field
function extrStore($value)
{
		if (is_in_str($value, "Lowe") != false)
		{
			$result = "Lowe";
		} else if (is_in_str($value, "Amazon") != false && $value != "AmazonDS" && $value != "Amazon.Canada" && $value != "AmazonDS Canada")
		{
			$result = "Amazon";
		} else if (is_in_str($value, "AmazonDS") != false)
		{
			$result = "AmazonDS";
		} else if (is_in_str($value, "FEDEX") != false)
		{
			$result = "FEDEX";
		} else if (is_in_str($value, "The Home Depot") != false)
		{
			$result = "Depot";
		} else if (is_in_str($value, "C & R") != false)
		{
			$result = "C & R";
		} else if (is_in_str($value, "MENARDS") != false)
		{
			$result = "MENARDS";
		} else if (is_in_str($value, "HomeClick") != false)
		{
			$result = "HomeClick";
		} else if (is_in_str($value, "Sears") != false)
		{
			$result = "Sears";
		} else if (is_in_str($value, "Your other Warehouse") != false)
		{
			$result = "YOW";
		} else if (is_in_str($value, "Wayfair") != false)
		{
			$result = "Wayfair";
		} else if (is_in_str($value, "Ferguson") != false)
		{
			$result = "Ferguson";
		} else $result = $value;
	return $result;
}

//Extract carrier name from value
function extrCarrier($value)
{
		if (is_in_str($value, "FEDEX") != false)
		{
			$result = "FEDEX";
		} else if (is_in_str($value, "RL") != false)
		{
			$result = "RL";
		} else if (is_in_str($value, "ABF") != false)
		{
			$result = "ABF";
		} else if (is_in_str($value, "CEVA") != false)
		{
			$result = "CEVA";
		} else if (is_in_str($value, "ESTES") != false)
		{
			$result = "ESTES";
		} else if (is_in_str($value, "NEW ENGL") != false)
		{
			$result = "NEW ENGL";
		} else if (is_in_str($value, "ODFL") != false)
		{
			$result = "ODFL";
		} else if (is_in_str($value, "SEKO") != false)
		{
			$result = "SEKO";
		} else if (is_in_str($value, "UPS") != false)
		{
			$result = "UPS";
		} else if (is_in_str($value, "YELLOW") != false || is_in_str($value, "YRC") != false)
		{
			$result = "YRC";
		} else if (is_in_str($value, "EMPIRE") != false)
		{
			$result = "EMPIRE";
		} else if (is_in_str($value, "PYLE") != false)
		{
			$result = "PYLE";
		} else $result = $value;
	return $result;
}

function getConnection()
{
	$conn = Database::getInstance()->dbc;
	return $conn;
}

function validate_state_code($country)
{
	$returnValue = $country;
	switch ($country)
	{
		case 'USA':
		case 'US':
			$returnValue = 'US';
			break;
		case 'CAN':
		case 'CA':
			$returnValue = 'CA';
			break;
	}
	return $returnValue;
}

function extrCountryCode($zip)
{
	if(preg_match( "/[0-9]{5}\-[0-9]{4}/", $zip ) || preg_match( "/[0-9]{5}/", $zip ))
	{
		return 'US';
	} else
	{
		return 'CA';
	}
}

function extrOtherResults($allShipers)
{
	$otherResults = array();
	foreach($allShipers as $allShiper)
	{
		foreach($allShiper as $key => $value)
		{
			$otherResults[] = array('id' => $key, 'po' => $value[7], 'carrier' => $value[3], 'quote' => $value[4], 'cost' => $value[6], 'time' => $value[5]);
		}
	}
	//print_r($otherResults);
	return $otherResults;
}

function parseCheaporder($conn, $cheaporder)
{
	$retVal = true;
	foreach ($cheaporder as $cheap)
	{
		if(!array_search('', $cheap))
		{
			updateToShipTrackWithLabel($conn, $cheap['id'], $cheap['carrier'], $cheap['quote'], $cheap['time'], $cheap['cost'], $cheap['dealerName'], true);
			$retVal = true;
		} else $retVal = false;
	}
	return $retVal;
}


//Is substring in string
function is_in_str($str,$substr)
{
	$result = strpos ($str, $substr);
	if ($result === FALSE) return false; else return true;
}

function getSettings($conn)
{
		$query = "SELECT * FROM ".GENERAL_SETTINGS." WHERE [app] = 'shipping_module' ORDER BY [group_name]";
		$result = $conn->prepare($query);
		$result->execute();
		$result = $result->fetchAll(PDO::FETCH_ASSOC);
		if (!isset($result) || empty($result)) die('Can not get settings.');
		return $result;
}

function getResidentialOneOrder($conn, $value, $key)
{
    /*print_r('<pre>');
    print_r($value);
    print_r('</pre>');*/
    
    $final_liftgate = '0';
    $final_residential = '0';
    $display_popup = 'False';
    $lowes_mode = 'no';
    if($value['residential'] == 'no')
    {
        $fedex_residential = getResidentialOneOrderHandler($conn, $value, $key);
        /*print_r('FEDEX RESIDENTIAL: '.$fedex_residential.'<br>');*/
        if
        (
            $fedex_residential == 'RESIDENTIAL' ||
            $fedex_residential == 'MIXED' ||
            $fedex_residential == 'UNKNOWN'
        )
        {
            $final_residential = '1';
        } else if($fedex_residential == 'BUSINESS')
        {
            $final_residential = '0';
        } else
        {
            $final_residential = 'false';
        }
    } else
    {
        $final_residential = $value['residential'];
    }
    /*print_r('LOGIC RESIDENTIAL: '.$final_residential.'<br>');*/
    
    if($final_residential == '1' && is_in_str($value['dealerName'], "MENARDS") != false)
    {
        $final_liftgate = '1';
    } else if($final_residential == '1')
    {
        $final_liftgate = $value['liftgate'];
    } else if($final_residential == '0')
    {
        $final_liftgate = '0';
    }
    
    if($value['lowes_mode'] == 'no')
    {
        if($final_residential == '1')
        {
            $lowes_mode = 'residential';
        } else
        {
            $lowes_mode = 'commercial';
        }
    } else
    {
        $lowes_mode = $value['lowes_mode'];
    }
    
    if($final_residential == '1')
    {
        $display_popup = 'True';
    }
    
    /*print_r('FINAL RESIDENTIAL: '.$final_residential.'<br>');
    print_r('FINAL LIFTGATE: '.$final_liftgate.'<br>');*/
    
    if($final_residential == '1' || $final_residential == '0')
    {
        $value['residential'] = $final_residential;
        $value['residentialFedex'] = $final_residential;
        $value['liftgate'] = $final_liftgate;
        $value['display_popup'] = $display_popup;
        $value['lowes_mode'] = $lowes_mode;
        $value['fedex_api_residential'] = $fedex_residential;
    } else
    {
        $value = false;
    }
    
    /*print_r('<pre>');
    print_r($value);
    print_r('</pre>');*/
    
    return $value;
}

function getResidentialOneOrderHandler($conn, $value, $key)
{
	$return_residential = false;
		
		if(isset($value['ShipAddress_Addr2']) && isset($value['ShipAddress_PostalCode']) && isset($value['dealerName']) && !empty($value['ShipAddress_Addr2']) && !empty($value['ShipAddress_PostalCode']) && !empty($value['dealerName']))
		{
			$AddressesToValidate = Array();
			$AddressesToValidate[] = array(
				'ClientReferenceId' => 'fakeID',
				'Address' => array(
					'StreetLines' => array('75 Hawk Rd'),
					'PostalCode' => '18974',
					'CompanyName' => 'Dreamline'
				)
			);
			$AddressesToValidate[] = array(
				'ClientReferenceId' => $key,
				'Address' => array(
					'StreetLines' => array($value['ShipAddress_Addr2']),
					'PostalCode' => $value['ShipAddress_PostalCode'],
					'CompanyName' => $value['dealerName']
				)
			);
			$validatedAddresses = getValidatedAddress($AddressesToValidate);
                        
                        /*print_r('<pre>');
                        print_r($validatedAddresses);
                        print_r($value);
                        print_r('</pre>');*/
                        
			if ($validatedAddresses != false)
			{
				if (isset($validatedAddresses[$key]))
				{
                                    $return_residential = $validatedAddresses[$key];
				}
			}
		}
	return $return_residential;
}

function getResidential($conn, $ordersSM)
{
	if (!empty($ordersSM))
	{
		$fakeAddress = array(
				'ClientReferenceId' => 'fakeID',
				'Address' => array(
					'StreetLines' => array('75 Hawk Rd'),
					'PostalCode' => '18974',
					'CompanyName' => 'Dreamline'
				)
			);
		$AddressesToValidate = Array();
		foreach($ordersSM as $key => $value)
		{
			if(isset($value['ShipAddress_Addr2']) && isset($value['ShipAddress_PostalCode']) && isset($value['dealerName']) && !empty($value['ShipAddress_Addr2']) && !empty($value['ShipAddress_PostalCode']) && !empty($value['dealerName']))
			{
				$AddressToValidate = array(
					'ClientReferenceId' => $key,
					'Address' => array(
						'StreetLines' => array($value['ShipAddress_Addr2']),
						'PostalCode' => $value['ShipAddress_PostalCode'],
						'CompanyName' => $value['dealerName']
					)
				);
				$AddressesToValidate[] = $AddressToValidate;
			}
		}
		$AddressesToValidate[] = $fakeAddress;
		$validatedAddresses = getValidatedAddress($AddressesToValidate);
		//print_r($validatedAddresses);

			foreach($ordersSM as $key => $value)
			{
				$ordersSM[$key]['residentialFedex'] = '1';
				if ($validatedAddresses != false)
				{
					if (isset($validatedAddresses[$key]))
					{
						if ($validatedAddresses[$key] != 'RESIDENTIAL')
						{
							$ordersSM[$key]['residentialFedex'] = '0';
						}
					}
				}
			}
		
		/*print_r('<pre>');
		print_r($validatedAddresses);
		print_r('</pre>');
		die('stop');*/
	}
	return $ordersSM;
}

function query_mode_dealer_unauthorized($authorized_dealers)
{
	$query_dealers = 'and (';
	foreach ($authorized_dealers as $key => $value)
	{
		if ($key == 0)
		{
			$query_dealers.=" ".SHIPTRACK_TABLE.".CustomerRef_FullName not like '%".$value."%'";
		} else {
			$query_dealers.="and  ".SHIPTRACK_TABLE.".CustomerRef_FullName not like '%".$value."%'";
		}
	}
	$query_dealers.= ')';
	return $query_dealers;
}

function query_mode_dealer_authorized($authorized_dealers)
{
	$query_dealers = 'and (';
	foreach ($authorized_dealers as $key => $value)
	{
		if ($key == 0)
		{
			$query_dealers.=" ".SHIPTRACK_TABLE.".CustomerRef_FullName like '%".$value."%'";
		} else {
			$query_dealers.="or  ".SHIPTRACK_TABLE.".CustomerRef_FullName like '%".$value."%'";
		}
	}
	$query_dealers.= ')';
	return $query_dealers;
}

 function query_mode_dealer($query_mode_dealer_setting, $authorized_dealers)
{
	
		
	switch($query_mode_dealer_setting)
	{
	case 'authorized':
		$query_dealers = query_mode_dealer_authorized($authorized_dealers);
		break;
	case 'unauthorized':
		$query_dealers = query_mode_dealer_unauthorized($authorized_dealers);
		break;
	case 'all':
		$query_dealers = '';
		break;
	default:
		$query_dealers = query_mode_dealer_authorized($authorized_dealers);
		break;
	}

	return $query_dealers;
}

 function isDistantOrderUnknown($oSM, $remote_orders, $remote_orders_dealer_name)
 {
	$returnVal = false;
	if (isset($oSM['ShipAddress_State']) && !empty($oSM['ShipAddress_State']))
	{
		if (in_array($oSM['ShipAddress_State'], $remote_orders) && !in_array($oSM['dealerName'], $remote_orders_dealer_name)) $returnVal = true;
	}
	return $returnVal;
 }
 
 function isDistantOrder($oSM, $remote_orders, $remote_orders_dealer_name)
 {
	$returnVal = false;
	if (isset($oSM['ShipAddress_State']) && !empty($oSM['ShipAddress_State']))
	{
		if (in_array($oSM['ShipAddress_State'], $remote_orders) && in_array($oSM['dealerName'], $remote_orders_dealer_name)) $returnVal = true;
	}
	return $returnVal;
 }
 
 function isSKU($shipping_ground, $oSM)
 {
	$isGround = true;
	foreach ($oSM['skus'] as $sku)
	{
		if(!isGroundShippable($shipping_ground, $sku)) $isGround = false;
	}
	return $isGround;
 }
 
 function isGroundShippable($shipping_ground, $sku)
 {
	if (in_array($sku, $shipping_ground) == true) return true; else return false;
 }
 
 function isOrderGroundShippable($conn, $oSM, $dealer, $shipping_ground_weight_max)
{
	/*print_r('<pre>');
	print_r($oSM);
	print_r('</pre>');
        die();*/
	$returnValue = 'ltl';
	if ($oSM['dealerName'] != 'Amazon' && extrStore($oSM['dealerName']) != 'Sears')
	{
		//print_r($oSM['weight']);
		//print_r($shipping_ground_weight_max);
		if($oSM['weight'] > $shipping_ground_weight_max)
		{
			$returnValue = 'ltl';
		} else
		{
			$query = "exec [dbo].[shipping_get_GR_LTL] @RefNumber = '".$oSM['RefNumber']."'";
				$result = $conn->prepare($query);
				$result->execute();
				$result = $result->fetchAll(PDO::FETCH_ASSOC);
				/*print_r('<pre>');
				print_r($result);
				print_r('</pre>');
                                die();*/
				//!!!TESTING
				/*$result = array(
				'0' => array(
					'ItemGroupRef_FullName' => 'DL-6195C-01',
					'Quantity' => '1',
					'SHIP_METHOD' => 'LTL')
				);*/
				//!!!TESTING
			
			$isOneOfSKUs = false;
			if($dealer == 'Overstock')
			{
				foreach($result as $res)
				{
					if ($res['ItemGroupRef_FullName'] == 'GSHDR-0960720-01' || $res['ItemGroupRef_FullName'] == 'GSHDR-0960720-04') $isOneOfSKUs = true;
				}
			}
			$westCoastStates = array('AZ', 'CA', 'HI', 'IA', 'MO', 'NE', 'NM', 'OR', 'SD', 'TX', 'WA', 'AL', 'AR', 'CO', 'ID', 'KS', 'MT', 'NV', 'ND', 'OK', 'UT', 'WY');
			/*
			print_r('<pre>');
			print_r($result);
			print_r('</pre>');
			*/
			if (in_array($oSM['ShipAddress_State'], $westCoastStates) && $isOneOfSKUs == true) $returnValue = 'ltl'; else
			{
				$is_result_full = true;
				foreach ($result as $key => $res)
				{
					if (empty($res['SHIP_METHOD'])) $is_result_full = false;
					if (empty($res['Quantity'])) $result[$key]['Quantity'] = '1';
				}
				if ($is_result_full)
				{
					$skusCount = count($result);
					$countGR = 0;
					$countGR1 = 0;
					$is_GR_flag = true;
					foreach ($result as $res)
					{
						if ($res['SHIP_METHOD'] != 'GR')
						{
							$is_GR_flag = false;
							//echo "not GR<br>";
						} else
						{
							if ($res['Quantity'] < 3 && $dealer != 'Amazon' && $dealer != 'Depot' && $dealer != 'YOW')
							{
								$countGR++;
								//echo "GR<br>";
							} else
							{
								$is_GR_flag = false;
							}
						}
					} 
					
					if ($is_GR_flag == false)
					{
						$is_GR_flag = true;
						$is_GR1_flag = true;
						foreach ($result as $res)
						{
							if ($res['SHIP_METHOD'] == 'GR:if not exceed 1')
							{
								if ($res['Quantity'] != '1')
								{
									$is_GR1_flag = false;
									//echo "not GR1<br>";
								} else
								{
									$countGR1++;
									//echo "GR1<br>";
								}
							}
						}
					}
					
					$GR_GR1_counter = $countGR + $countGR1;
					
					if ($is_GR_flag == true && $GR_GR1_counter == $skusCount)
					{
						$returnValue = 'gr';
					} else 
					{
						$returnValue = 'ltl';
					}
				} else
				{
					display_message("Can not get SHIP_METHOD for each SKU to detect Shipping Ground for this order.");
					/*print_r('<pre>');
					print_r($result);
					print_r('</pre>');*/
					$returnValue = 'error';
				}
			}
		}
	} else if (extrStore($oSM['dealerName']) == 'Sears')
	{
		$returnValue = 'gr';
		foreach($oSM['skus'] as $sku)
		{
			$query = "SELECT [SKU], [Length], [Width], [Height] FROM ".SHIPPING_MEASUREMENTS." WHERE [SKU] = '".$sku."'";
			$result = $conn->prepare($query);
			$result->execute();
			$result = $result->fetch(PDO::FETCH_ASSOC);
			
			
			if (isset($result['SKU']) && !empty($result['SKU']))
			{
				$formulaResult = (int)$result['Width'] + (int)$result['Height'];
				$formulaResult = $formulaResult*2;
				$formulaResult = $formulaResult + (int)$result['Length'];
				if ($formulaResult >= 108 || $oSM['weight'] >= 130)
				{
					$returnValue = 'ltl';
				}
			} else $returnValue = 'ltl';
		}
	}
	//print_r('<br>'.$returnValue.'<br>');
	return $returnValue;
}

function writeToShipTrackWithLabel($conn, $oSM)
{
$oSM['RefNumber'] = strRepl($oSM['RefNumber']);
$oSM['ShipPostalCode'] = strRepl($oSM['ShipPostalCode']);
$oSM['dealerName'] = strRepl($oSM['dealerName']);
$oSM['PONumber'] = strRepl($oSM['PONumber']);
$oSM['weight'] = strRepl($oSM['weight']);
$oSM['liftgate'] = strRepl($oSM['liftgate']);
$oSM['display_popup'] = strRepl($oSM['display_popup']);
	$query = "INSERT INTO [dbo].[".SHIPTRACKWITHLABEL_TABLE."]
           ([ID]
           ,[FROMZIP]
           ,[TOZIP]
           ,[DEALER]
           ,[PONUMBER]
           ,[WEIGHT]
		   ,[LIFTGATE]
		   ,[RESIDENTIAL]
		   ,[PALLETS]
                   ,[display_popup])
     VALUES
           (".$oSM['RefNumber'].",
           '18974',
           ".$oSM['ShipPostalCode'].",
           ".$oSM['dealerName'].",
           ".$oSM['PONumber'].",
           ".$oSM['weight'].",
            ".$oSM['liftgate'].",
            ".$oSM['residentialFedex'].",
            ".getPallets($oSM['weight']).",
            ".$oSM['display_popup'].")";

	runQuery($conn, $query);
	//print_r($query);
}

function logToFile($filename, $msg)
{
	$fd = fopen($filename, "a");
	fwrite($fd, $msg . "\n");
	fclose($fd);
}

function isCanadaState($state, $country)
{
	$returnVal = false;
	$canada_states = array('AB', 'BC', 'MB', 'NB', 'NL', 'NS', 'NT', 'NU', 'ON', 'PE', 'QC', 'SK', 'YT');
	if (isset($country) && !empty($country))
	{
		if ($country == 'CA' || $country == 'CAN') $returnVal = true;
	}
	if (isset($state) && !empty($state))
	{
		if (in_array($state, $canada_states)) $returnVal = true;
	}
	return $returnVal;
}

function isHI_AL($state)
{
	$returnVal = false;
	if (isset($state) && !empty($state))
	{
		if ($state == 'AK' || $state == 'HI') $returnVal = true;
	}
	return $returnVal;
}

function clearProFromShipTrackWithLabel( $conn, $Ref, $Dealer ) {
	$result = false;
	if( $Ref && $Dealer ) {
		$query = "UPDATE [".DATABASE."].[dbo].[ShipTrackWithLabel]
					 SET [PRO] = NULL
					   , [CARRIERNAME_PRO] = NULL
				   WHERE [ID] = ".$conn->quote($Ref)."
				     AND [DEALER] = ".$conn->quote($Dealer).";";		
		$result = runQuery($conn, $query);
	}
	return $result;
}

function updateToShipTrackWithLabel($conn, $id, $carrier, $quote, $time, $cost, $dealerName, $assignPro)
{
	/*print_r('<pre>');
	print_r($id.'<br>');
	print_r($carrier.'<br>');
	print_r($quote.'<br>');
	print_r($time.'<br>');
	print_r($cost.'<br>');
	print_r($dealerName.'<br>');
	print_r($assignPro.'<br>');
	print_r('</pre>');
	die();*/
	if ($assignPro) $proNumberResult = carrierByPRO($conn, $carrier, $dealerName); else $proNumberResult = false;
	if (extrCarrier($carrier) == 'YRC')
	{
		$proNumber = $proNumberResult['proCheck'];
		$proNumberWoCheckDidit = $proNumberResult['pro'];
		$extrCarrier = extrCarrier($carrier);
		$CARRIERNAME_PRO_WO_CHECK = $extrCarrier.'_'.$proNumberWoCheckDidit;
	} else 
	{
		$proNumber = $proNumberResult;
	}
        //print_r($proNumber);
	//$fedexPro = fedexPro($conn, $carrier);

	if ($proNumber != false && $proNumber != 'error')
	{
		$extrCarrier = extrCarrier($carrier);
		$CARRIERNAME_PRO = $extrCarrier.'_'.$proNumber;
		
		$id = strRepl($id);
		$carrier = strRepl($carrier);
		$quote = strRepl($quote);
		$time = strRepl($time);
		$cost = strRepl($cost);
		$proNumber = strRepl($proNumber);
		
		
		$query = "UPDATE [dbo].[".SHIPTRACKWITHLABEL_TABLE."] SET [CARRIERNAME_PRO] = '".$CARRIERNAME_PRO."' WHERE [ID] = ".$id." AND NOT EXISTS (SELECT [CARRIERNAME],[PRO] FROM [dbo].[".SHIPTRACKWITHLABEL_TABLE."] WHERE [CARRIERNAME] = ".$carrier." and [PRO] = ".$proNumber.")";
		
		if ($extrCarrier == 'YRC')
		{
			$result = runQuery($conn, "UPDATE [dbo].[".SHIPTRACKWITHLABEL_TABLE."] SET [CARRIERNAME_PRO] = '".$CARRIERNAME_PRO_WO_CHECK."' WHERE [ID] = ".$id." AND NOT EXISTS (SELECT [CARRIERNAME],[PRO] FROM [dbo].[".SHIPTRACKWITHLABEL_TABLE."] WHERE [CARRIERNAME] = ".$carrier." and [PRO] = ".$proNumber.")");
			if ($result) $result = runQuery($conn, $query);
		} else 
		{
			$result = runQuery($conn, $query);
		}
		
		
		if ($result)
		{
			$query = "UPDATE [dbo].[".SHIPTRACKWITHLABEL_TABLE."]
			SET [CARRIERNAME] = ".$carrier.",
			[QUOTENUMBER] = ".$quote.",
			[TRANSITTIME] = ".$time.",
			[COST] = ".$cost.",
			[PRO] = ".$proNumber.",
			[DATE] = GETDATE() WHERE [ID] = ".$id." AND NOT EXISTS (SELECT [CARRIERNAME],[PRO] FROM [dbo].[".SHIPTRACKWITHLABEL_TABLE."] WHERE [CARRIERNAME] = ".$carrier." and [PRO] = ".$proNumber.")";
		
		$result = runQuery($conn, $query);
		} else 
		{
			$msg = "id: ".$id." pro: ".$proNumber." duplicates";
			$filename = "duplicate_log.txt";
			logToFile($filename, $msg);
			display_message("This PRO# is already in use.<br><br>Error while assigning PRO#. This order will be processed on next run.");
		}
	}
	
	if ($proNumber == false && $proNumber != 'error') {
		$id = strRepl($id);
		$carrier = strRepl($carrier);
		$quote = strRepl($quote);
		$time = strRepl($time);
		$cost = strRepl($cost);
		
		$query = "UPDATE [dbo].[".SHIPTRACKWITHLABEL_TABLE."]
		SET [CARRIERNAME] = ".$carrier.",
		[QUOTENUMBER] = ".$quote.",
		[TRANSITTIME] = ".$time.",
		[COST] = ".$cost.",
		[DATE] = GETDATE() WHERE [ID] = ".$id;
		
		runQuery($conn, $query);
	}
	
	if ($proNumber == 'error' && $proNumber != false)
	{
		/*$id = strRepl($id);
		$carrier = strRepl($carrier);
		$quote = strRepl($quote);
		$time = strRepl($time);
		$cost = strRepl($cost);
		
		$query = "UPDATE [dbo].[".SHIPTRACKWITHLABEL_TABLE."]
		SET [CARRIERNAME] = ".$carrier.",
		[QUOTENUMBER] = ".$quote.",
		[TRANSITTIME] = ".$time.",
		[COST] = ".$cost.",
		[DATE] = GETDATE() WHERE [ID] = ".$id;
		
		runQuery($conn, $query);*/
		$msg = "id: ".$id." pro: ".$proNumber." duplicates";
		$filename = "duplicate_log.txt";
		logToFile($filename, $msg);
		display_message("Error while assigning PRO#. This order will be processed on next run.");
	}
	
	if($proNumberResult != 'error' && $proNumberResult != false)
	{
		return $proNumberResult;
	} else return false;
}

function fedexPro($conn, $carrier)
{
	global $pro_numbers_fedex_min, $pro_numbers_fedex_max;
	$min = (int)$pro_numbers_fedex_min;
	$max = (int)$pro_numbers_fedex_max;
	$query = "SELECT CONVERT(bigint, SUBSTRING([PRO] ,0, CHARINDEX('-', [PRO] ))) AS myPro  FROM [dbo].[".SHIPTRACKWITHLABEL_TABLE."] WHERE [CARRIERNAME] LIKE '%".$carrier."%' AND ISNUMERIC(SUBSTRING([PRO] ,0, CHARINDEX('-', [PRO] ))) = 1 AND CONVERT(bigint, SUBSTRING([PRO] ,0, CHARINDEX('-', [PRO] ))) <= ".$max." AND ".$min." <= CONVERT(bigint, SUBSTRING([PRO] ,0, CHARINDEX('-', [PRO] ))) AND GETDATE() != [DATE] ORDER BY myPro";
	$retValue = getPRO($conn, 'FEDEX', $min, $max, $query);
	return $retValue;
}

function upsPro($conn, $carrier)
{
	global $pro_numbers_ups_min, $pro_numbers_ups_max;
	$min = (int)$pro_numbers_ups_min;
	$max = (int)$pro_numbers_ups_max;
	$query = "SELECT CONVERT(bigint, SUBSTRING([PRO] ,0, CHARINDEX('-', [PRO] ))) AS myPro  FROM [dbo].[".SHIPTRACKWITHLABEL_TABLE."] WHERE [CARRIERNAME] LIKE '%".$carrier."%' AND ISNUMERIC(SUBSTRING([PRO] ,0, CHARINDEX('-', [PRO] ))) = 1 AND CONVERT(bigint, SUBSTRING([PRO] ,0, CHARINDEX('-', [PRO] ))) <= ".$max." AND ".$min." <= CONVERT(bigint, SUBSTRING([PRO] ,0, CHARINDEX('-', [PRO] ))) AND GETDATE() != [DATE] ORDER BY myPro";
	$retValue = getPRO($conn, 'UPS', $min, $max, $query);
	return $retValue;
}

function rlPro($conn, $carrier)
{
	global $pro_numbers_rl_min, $pro_numbers_rl_max;
	$min = (int)$pro_numbers_rl_min;
	$max = (int)$pro_numbers_rl_max;
	$query = "SELECT CONVERT(bigint, SUBSTRING([PRO] ,0, CHARINDEX('-', [PRO] ))) AS myPro  FROM [dbo].[".SHIPTRACKWITHLABEL_TABLE."] WHERE [CARRIERNAME] LIKE '%".$carrier."%' AND ISNUMERIC(SUBSTRING([PRO] ,0, CHARINDEX('-', [PRO] ))) = 1 AND CONVERT(bigint, SUBSTRING([PRO] ,0, CHARINDEX('-', [PRO] ))) <= ".$max." AND ".$min." <= CONVERT(bigint, SUBSTRING([PRO] ,0, CHARINDEX('-', [PRO] ))) AND GETDATE() != [DATE] ORDER BY myPro";
	$retValue = getPRO($conn, 'RL', $min, $max, $query);
	return $retValue;
}

function estesPro($conn, $carrier)
{
	global $pro_numbers_estes_min, $pro_numbers_estes_max, $pro_numbers_estes_fd;
	$min = (int)$pro_numbers_estes_min;
	$max = (int)$pro_numbers_estes_max;
	$query = "SELECT CONVERT(bigint, [PRO]) AS myPro  FROM [dbo].[".SHIPTRACKWITHLABEL_TABLE."] WHERE [CARRIERNAME] LIKE '%".$carrier."%' AND ISNUMERIC([PRO]) = 1 AND CONVERT(bigint, [PRO]) <= ".$max." AND ".$min." <= CONVERT(bigint, [PRO]) AND GETDATE() != [DATE] ORDER BY myPro";
	//print_r($query);
	$retValue = getPRO($conn, 'ESTES', $min, $max, $query);
	$retValue = $pro_numbers_estes_fd.$retValue;
	return $retValue;
}

function nemfPro($conn, $carrier)
{
	global $pro_numbers_nemf_min, $pro_numbers_nemf_max;
	$min = (int)$pro_numbers_nemf_min;
	$max = (int)$pro_numbers_nemf_max;
	$query = "SELECT CONVERT(bigint, [PRO]) AS myPro  FROM [dbo].[".SHIPTRACKWITHLABEL_TABLE."] WHERE [CARRIERNAME] LIKE '%".$carrier."%' AND ISNUMERIC([PRO]) = 1 AND CONVERT(bigint, [PRO]) <= ".$max." AND ".$min." <= CONVERT(bigint, [PRO]) AND GETDATE() != [DATE] ORDER BY myPro";
	$retValue = getPRO($conn, 'NEW ENGL', $min, $max, $query);
	return $retValue;
}

function abfPro($conn, $carrier)
{
	global $pro_numbers_abf_min, $pro_numbers_abf_max, $pro_numbers_abf_first_digit;
	$min = (int)$pro_numbers_abf_min;
	$max = (int)$pro_numbers_abf_max;
	$query = "SELECT CONVERT(bigint, [PRO]) AS myPro  FROM [dbo].[".SHIPTRACKWITHLABEL_TABLE."] WHERE [CARRIERNAME] LIKE '%".$carrier."%' AND ISNUMERIC([PRO]) = 1 AND CONVERT(bigint, [PRO]) <= ".$max." AND ".$min." <= CONVERT(bigint, [PRO]) AND GETDATE() != [DATE] ORDER BY myPro";
	//print_r($query);
	$retValue = getPRO($conn, 'ABF', $min, $max, $query);
	$retValue = $pro_numbers_abf_first_digit.$retValue;
	return $retValue;
}

function yrcPro($conn, $carrier)
{
	global $pro_numbers_yrc_min, $pro_numbers_yrc_max, $pro_numbers_yrc_before_hyphen;
	$min = (int)$pro_numbers_yrc_min;
	$max = (int)$pro_numbers_yrc_max;
	$query = "SELECT CONVERT(bigint, SUBSTRING([PRO] ,5, 6)) AS myPro  FROM [dbo].[".SHIPTRACKWITHLABEL_TABLE."] WHERE ([CARRIERNAME] LIKE '%YELLOW%' OR [CARRIERNAME] LIKE '%YRC%') AND ISNUMERIC(SUBSTRING([PRO] ,5, 6)) = 1 AND CONVERT(bigint, SUBSTRING([PRO] ,5, 6)) <= ".$max." AND ".$min." <= CONVERT(bigint, SUBSTRING([PRO] ,5, 6)) AND SUBSTRING([PRO] ,1, 3) = '".$pro_numbers_yrc_before_hyphen."' AND GETDATE() != [DATE] ORDER BY myPro";
	$retValue = getPRO($conn, 'YRC', $min, $max, $query);
	return $retValue;
}

function odflPro($conn, $carrier)
{
	global $pro_numbers_odfl_min, $pro_numbers_odfl_max;
	$min = $pro_numbers_odfl_min;
	$max = $pro_numbers_odfl_max;
	$query = "SELECT CONVERT(bigint, [PRO]) AS myPro  FROM [dbo].[".SHIPTRACKWITHLABEL_TABLE."] WHERE [CARRIERNAME] LIKE '%".$carrier."%' AND ISNUMERIC([PRO]) = 1 AND CONVERT(bigint, [PRO]) <= ".$max." AND ".$min." <= CONVERT(bigint, [PRO]) AND GETDATE() != [DATE] ORDER BY myPro";
	$retValue = getPRO($conn, 'ODFL', $min, $max, $query);
	return $retValue;
}

function pylePro($conn, $carrier)
{
	global $pro_numbers_pyle_min, $pro_numbers_pyle_max;
	$min = $pro_numbers_pyle_min;
	$max = $pro_numbers_pyle_max;
	$query = "SELECT CONVERT(bigint, SUBSTRING([PRO], 0, LEN([PRO]))) AS myPro  FROM [dbo].[".SHIPTRACKWITHLABEL_TABLE."] WHERE [CARRIERNAME] LIKE '%".$carrier."%' AND ISNUMERIC([PRO]) = 1 AND CONVERT(bigint, SUBSTRING([PRO], 0, LEN([PRO]))) <= ".$max." AND ".$min." <= CONVERT(bigint, SUBSTRING([PRO], 0, LEN([PRO]))) AND GETDATE() != [DATE] ORDER BY myPro";
	$retValue = getPRO($conn, 'PYLE', $min, $max, $query);
	return $retValue;
}

function amazonDS_CEVAPro($conn, $carrier)
{
	global $pro_numbers_amazonds_min, $pro_numbers_amazonds_max;
	$min = $pro_numbers_amazonds_min;
	$max = $pro_numbers_amazonds_max;
	$query = "SELECT CONVERT(bigint, [PRO]) AS myPro  FROM [dbo].[".SHIPTRACKWITHLABEL_TABLE."] WHERE [DEALER] LIKE '%".$carrier."%' AND [CARRIERNAME] LIKE 'CEVA' AND ISNUMERIC([PRO]) = 1 AND CONVERT(bigint, [PRO]) <= ".$max." AND ".$min." <= CONVERT(bigint, [PRO]) AND GETDATE() != [DATE] ORDER BY myPro";
	$retValue = getPRO($conn, 'CEVA', $min, $max, $query);
	return $retValue;
}

function getPRO($conn, $carrier, $min, $max, $query)
{
	try{
		$conn=null;
		$conn = getConnection();
		$retValue = false;
		$result = $conn->prepare($query);
		$result->execute();
		$result = $result->fetchAll(PDO::FETCH_COLUMN, 'myPro');
		
		$result = array_unique($result);
		$rangedArray = range($min,$max);
		$missing = array_diff($rangedArray, $result);
	}
	catch(PDOException $e)
	{
            //print_r($query);
		display_message("Can not get range.");
		$missing = false;
	}
	$rangeCount = 0;
	
	if(isset($missing) && !empty($missing) && $missing != false)
	{
		if (count($result) >= count($rangedArray)) $rangeCount = 0; else $rangeCount = count($missing);
		if($rangeCount <= 500) display_message("PRO# range for ".$carrier." carrier will expire in ".$rangeCount." orders! Extend the range please!");
		$maxPro = min($missing);
		if($min <= $maxPro && $maxPro <= $max)
		{
			$retValue = $maxPro;
		} else 
		{
			display_message("Error! PRO# FOR ".$carrier." IS OUT OF RANGE!");
			$retValue = 'error';
		}
	} else 
	{
		print_r("Can not generate PRO# FOR '".$carrier."' in range:<br>
		MIN: ".$min."<br>
		MAX: ".$max."<br>
		Extend the range here: <a href='/general_settings/'>GENERAL SETTINGS PAGE</a>!<br>");
	}
	$LeftInRangeQuery = "UPDATE ".LEFTINRANGE." SET [LEFT] = '".$rangeCount."' WHERE [CARRIERNAME] = '".$carrier."'";
	runQuery($conn, $LeftInRangeQuery);
	
	return $retValue;
}

function ifProNotAssigned($carrier, $pro)
{
	global $FEDEXassignedPROs, $RLassignedPROs, $ESTESassignedPROs, $UPSassignedPROs, $NEWENGLassignedPROs, $YRCassignedPROs, $ODFLassignedPROs;

	switch ($carrier)
	{
		case 'FEDEX':
			if (in_array($pro, $FEDEXassignedPROs))
			{
				return false;
			} else 
			{
				$FEDEXassignedPROs[] = $pro;
				return true;
			}
			break;
		case 'RL':
			if (in_array($pro, $RLassignedPROs))
			{
				return false;
			} else 
			{
				$RLassignedPROs[] = $pro;
				return true;
			}
			break;
		case 'ESTES':
			if (in_array($pro, $ESTESassignedPROs))
			{
				return false;
			} else 
			{
				$ESTESassignedPROs[] = $pro;
				return true;
			}
			break;
		case 'UPS':
			if (in_array($pro, $UPSassignedPROs))
			{
				return false;
			} else 
			{
				$UPSassignedPROs[] = $pro;
				return true;
			}
			break;
		case 'NEW ENGL':
			if (in_array($pro, $NEWENGLassignedPROs))
			{
				return false;
			} else 
			{
				$NEWENGLassignedPROs[] = $pro;
				return true;
			}
			break;
		case 'YRC':
			if (in_array($pro, $YRCassignedPROs))
			{
				return false;
			} else 
			{
				$YRCassignedPROs[] = $pro;
				return true;
			}
			break;
		case 'ODFL':
			if (in_array($pro, $ODFLassignedPROs))
			{
				return false;
			} else 
			{
				$ODFLassignedPROs[] = $pro;
				return true;
			}
			break;
		default:
			return false;
			break;
	}
}

function carrierByPRO($conn, $carrierOriginal, $dealerName)
{
	global $ORDER_PROCESSING_MODE_FEDEX;
	$carrier = extrCarrier($carrierOriginal);
	if ($dealerName != 'AmazonDS')
	{
		switch ($carrier)
		{
			case 'FEDEX':
				if ($ORDER_PROCESSING_MODE_FEDEX && ($ORDER_PROCESSING_MODE_FEDEX == 'mixed' || $ORDER_PROCESSING_MODE_FEDEX == 'electronic'))
				{
					display_message($ORDER_PROCESSING_MODE_FEDEX." mode: PRO# will not be assigned to this order.");
					return false;
				} else
				{
					$fedexPro = fedexPro($conn, $carrier);
					if ($fedexPro != 'error' && $fedexPro != false) $fedexPro = add10digitFedex($fedexPro);
					return $fedexPro;
				}
				break;
			case 'RL':
				$rlPro = rlPro($conn, $carrier);
				if ($rlPro != 'error' && $rlPro != false) $rlPro = add9digitRL($rlPro);
				return $rlPro;
				break;
			case 'ESTES':
				$estesPro = estesPro($conn, $carrier);
				return $estesPro;
				break;
			case 'UPS':
				if ($carrierOriginal == 'UPS GR')
				{
					return false;
				} else
				{
					$upsPro = upsPro($conn, $carrier);
					if ($upsPro != 'error' && $upsPro != false) $upsPro = add9digitUPS($upsPro);
					return $upsPro;
				}
				break;
			case 'NEW ENGL':
				$nemfPro = nemfPro($conn, $carrier);
				return $nemfPro;
				break;
			case 'ABF':
				$abfPro = abfPro($conn, $carrier);
				return $abfPro;
				break;
			case 'YRC':
				$yrcPro = yrcPro($conn, $carrier);
				if ($yrcPro != 'error' && $yrcPro != false) 
				{
					$yrcProWoCheckDigit = add_before_hyphen($yrcPro);
					$yrcPro = addCheckdigitYRC($yrcPro);
					$yrcPro = add_before_hyphen($yrcPro);
					
					$yrcPro = Array('pro' => $yrcProWoCheckDigit, 'proCheck' => $yrcPro);
				}
				return $yrcPro;
				break;
			case 'ODFL':
				$odflPro = odflPro($conn, $carrier);
				return $odflPro;
				break;
			case 'PYLE':
				$pylePro = pylePro($conn, $carrier);
				if ($pylePro != 'error' && $pylePro != false)
				{
					$pylePro = luhn_process($pylePro);
					return $pylePro;
				} else
				return false;
				break;
			default:
				return false;
				break;
		}
	} else {
		if ($carrier == 'ABF')
		{
			$abfPro = abfPro($conn, $carrier);
			return $abfPro;
		} else if ($carrier == 'CEVA')
		{
			$amazonDSPro = amazonDS_CEVAPro($conn, 'AmazonDS');
		return $amazonDSPro;
		} else if ($carrier == 'ESTES')
		{
			$amazonDSPro = estesPro($conn, 'ESTES');
		return $amazonDSPro;
		} else
		{
			return false;
		}
	}
}

//MOD-10 (Modulus 10 Check Digit Calculation)
function luhn_process($number1) {
	$number = $number1 . '0';
	$number_length=strlen($number);
	$parity=$number_length % 2;
	$total=0;
	for ($i=0; $i<$number_length; $i++) {
		$digit=$number[$i];
		if ($i % 2 == $parity) {
			$digit*=2;
			if ($digit > 9) {
				$digit-=9;
			}
		}
		$total+=$digit;
	}
	$temp_res = $total % 10;
	if($temp_res != 0) $chckdgt = 10 - ($temp_res); else $chckdgt = 0;
	$result = $number1.$chckdgt;
	return $result;
}

//Add 10s digit to Fedex barcode
function add10digitFedex($pro)
{
	if(isset($pro) && !empty($pro) && is_numeric($pro))
	{
		$numOfDigits = preg_match_all( "/[0-9]/", $pro );
		if ($numOfDigits == 9)
		{
			$proDb7 = $pro/7;
			$proInt = intval($proDb7);
			$proMb7 = $proInt*7;
			$digit10 = $pro-$proMb7;
			$pro10 = $pro.'-'.$digit10;
			//$pro10 =  $pro.($pro-intval($pro/7)*7);
		} else {
			print_r("PRO# should have 9 digits for this order!");
			$pro10 = false;
		}
	} else {
		print_r("PRO# should be integer number for this order!");
		$pro10 = false;
	}
	return $pro10;
}

//Add 9s digit to RL barcode
function add9digitRL($pro)
{
	if(isset($pro) && !empty($pro) && is_numeric($pro))
	{
		$numOfDigits = preg_match_all( "/[0-9]/", $pro );
		if ($numOfDigits == 8)
		{
			$digit9 = 9 - ($pro - (intval($pro / 9) * 9));
			$pro9 = $pro.'-'.$digit9;
		} else {
			print_r("PRO# should have 8 digits for this order!");
			$pro9 = false;
		}
	} else {
		print_r("PRO# should be integer number for this order!");
		$pro9 = false;
	}
	return $pro9;
}

//Add 9s digit to UPS barcode. FORMULA OF MOD-7 CHECK DIGIT
function add9digitUPS($pro)
{
	if(isset($pro) && !empty($pro) && is_numeric($pro))
	{
		$numOfDigits = preg_match_all( "/[0-9]/", $pro );
		if ($numOfDigits == 8)
		{
			$value = $pro / 7;
			$value = floor($value*10)/10;
			$value = $value - intval($value);
			$value = $value * 7;
			$value  = ceil($value);
			$digit9 = $value;
			$pro9 = $pro.'-'.$digit9;
		} else {
			print_r("PRO# should have 8 digits for this order!");
			$pro9 = false;
		}
	} else {
		print_r("PRO# should be integer number for this order!");
		$pro9 = false;
	}
	return $pro9;
}

//Add check digit to YRC barcode.
function addCheckdigitYRC($pro)
{
	if(isset($pro) && !empty($pro) && is_numeric($pro))
	{
		$numOfDigits = preg_match_all( "/[0-9]/", $pro );
		if ($numOfDigits == 6)
		{
			$valueD11 = $pro/11;
			$value = floor($valueD11);
			$left = $valueD11-$value;
			if ($left != 0)
			{
				$value = $value*11;
				$value = $pro-$value;
				$value = 11-$value;
				if ($value == 10) $digit9 = 'X'; else $digit9 = $value;
			} else
			{
				$digit9 = '0';
			}
			$pro9 = $pro.'-'.$digit9;
		} else {
			print_r("PRO# should have 6 digits for this order!");
			$pro9 = false;
		}
	} else {
		print_r("PRO# should be integer number for this order!");
		$pro9 = false;
	}
	return $pro9;
}

//Add before_hyphen number to YRC barcode
function add_before_hyphen($pro)
{
	global $pro_numbers_yrc_before_hyphen;
	if(isset($pro) && !empty($pro))
	{
		$pro = $pro_numbers_yrc_before_hyphen.'-'.$pro;
	} else {
		print_r("PRO# should be integer number for this order!");
		$pro = false;
	}
	return $pro;
}

function remrlamazon($order)
{
	foreach ($order as $key1 => $value1)
	{
		foreach ($value1 as $key2 => $value2)
		{
			$dealerName = $value2['dealerName'];
			if ($dealerName == 'Amazon'/* || $dealerName == 'AmazonDS' || $dealerName == 'Amazon.Canada'*/)
			{
				foreach ($value2[0] as $key3 => $value3)
				{
					if (extrCarrier($value3['CARRIERNAME']) != 'FEDEX')
					{
						unset($order[$key1][$key2][0][$key3]);
					}
				}
			}
		}
	}
	return $order;
}

function startMainScript()
{
	startShipsingle();
	return getNowTime();
}

function getTime()
{
	$conn = getConnection();
		$query = "SELECT [start],[stop] FROM [dbo].[shipsingleTime] WHERE [shipsingleTimeID] = 0";
		$result = $conn->prepare($query);
		$result->execute();
		$result = $result->fetch(PDO::FETCH_ASSOC);
		if (!isset($result) || empty($result)) die('Can not get time.');
	$conn=null;
		return $result;
}

function getNowTime()
{
	$conn = getConnection();
		$query = "SELECT GETDATE() as myDate FROM [dbo].[shipsingleTime]";
		$result = $conn->prepare($query);
		$result->execute();
		$result = $result->fetch(PDO::FETCH_ASSOC);
		if (!isset($result) || empty($result)) die('Can not get time.');
		$datetime = new DateTime($result['myDate']);
	$conn=null;
		return $datetime->format('U');
}

function setTime($start, $stop)
{
	$conn = getConnection();
		$query = "UPDATE [dbo].[shipsingleTime] SET [start] = ".$start.",[stop] = ".$stop." WHERE [shipsingleTimeID] = 0";
		$result = $conn->prepare($query);
		$result->execute();
	$conn=null;
}

//get carrier logo
function getCarrierLogo($carrier)
{
	$carrier = extrCarrier($carrier);
	switch ($carrier)
	{
	case "FEDEX":
		$src = 'Include/Images/fedex.png';
        break;
	case "RL":
		$src = 'Include/Images/rl.png';
        break;
	case "YRC":
		$src = 'Include/Images/yrc.png';
        break;
	case "ESTES":
		$src = 'Include/Images/estes.png';
        break;
	case "ABF":
		$src = 'Include/Images/abf.png';
        break;
	case "CEVA":
		$src = 'Include/Images/ceva.png';
        break;
	case "EMPIRE":
		$src = 'Include/Images/empire.PNG';
        break;
	case "NEW ENGL":
		$src = 'Include/Images/newengl.png';
        break;
	case "ODFL":
		$src = 'Include/Images/odfl.png';
        break;
	case "SEKO":
		$src = 'Include/Images/seko.png';
        break;
	case "UPS":
		$src = 'Include/Images/ups.png';
        break;
	default:
		$src = 'Include/pictures/noImage.png';
		break;
	}
	return $src;
}
?>