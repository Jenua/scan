<?php

$message_type = [
    '1' => [
        'type' => 'error',
    ],
    '2' => [
        'type' => 'warning',
    ],
    '3' => [
        'type' => 'notification',
    ],
    '4' => [
        'type' => 'service message',
    ]
];

$rule_type = [
    '00' => [
        'rule' => 'empty value',
    ],
    '01' => [
        'rule' => 'not numeric value',
    ],
    '02' => [
        'rule' => 'invalid value (in list of invalid)',
    ],
    '03' => [
        'rule' => 'invalid value (not in list of valid)',
    ],
    '04' => [
        'rule' => 'invalid value (does not match regex)',
    ],
    '05' => [
        'rule' => 'invalid value (not boolean)',
    ],
    '06' => [
        'rule' => 'duplicated data',
    ],
    '07' => [
        'rule' => 'empty row',
    ],
    '08' => [
        'rule' => 'empty result',
    ],
    '09' => [
        'rule' => 'invalid data',
    ],
    '10' => [
        'rule' => 'not exists',
    ]
];

$field_type = [
    '00' => [
        'field' => 'order data block',
    ],
    '01' => [
        'field' => 'total weight',
    ],
    '02' => [
        'field' => 'total number of pallets',
    ],
    '03' => [
        'field' => 'freight class',
    ],
    '04' => [
        'field' => 'liftgate',
    ],
    '05' => [
        'field' => 'residential',
    ],
    '06' => [
        'field' => 'ship from data block',
    ],
    '07' => [
        'field' => 'name from',
    ],
    '08' => [
        'field' => 'address from',
    ],
    '09' => [
        'field' => 'city from',
    ],
    '10' => [
        'field' => 'state from',
    ],
    '11' => [
        'field' => 'zip from',
    ],
    '12' => [
        'field' => 'country from',
    ],
    '13' => [
        'field' => 'ship to data block',
    ],
    '14' => [
        'field' => 'dealer',
    ],
    '15' => [
        'field' => 'customer',
    ],
    '16' => [
        'field' => 'address to',
    ],
    '17' => [
        'field' => 'city to',
    ],
    '18' => [
        'field' => 'state to',
    ],
    '19' => [
        'field' => 'zip to',
    ],
    '20' => [
        'field' => 'country to',
    ],
    '21' => [
        'field' => 'groupdetail data',
    ],
    '22' => [
        'field' => 'product quantity',
    ],
    '23' => [
        'field' => 'linedetail data',
    ],
    '24' => [
        'field' => 'item weight',
    ],
    '25' => [
        'field' => 'item quantity',
    ],
    '26' => [
        'field' => 'carriers block',
    ],
    '27' => [
        'field' => 'carrier name',
    ],
    '28' => [
        'field' => 'quote number',
    ],
    '29' => [
        'field' => 'cost',
    ],
    '30' => [
        'field' => 'transit time',
    ],
    '31' => [
        'field' => 'cost details',
    ],
    '32' => [
        'field' => 'shipsingle validation data'
    ],
    '33' => [
        'field' => '[shitrack] table'
    ],
    '34' => [
        'field' => '[shitrack].[TxnID]'
    ],
    '35' => [
        'field' => '[groupdetail].[ItemGroupRef_FullName]'
    ],
    '36' => [
        'field' => '[groupdetail].[TxnLineID]'
    ],
    '37' => [
        'field' => '[DL_valid_SKU] table'
    ],
    '38' => [
        'field' => '[DL_valid_SKU].[UPC]'
    ],
    '39' => [
        'field' => 'request'
    ],
    '40' => [
        'field' => 'request type'
    ],
    '41' => [
        'field' => 'JSON data'
    ],
    '42' => [
        'field' => 'error code'
    ],
    '43' => [
        'field' => 'input values'
    ],
    '44' => [
        'field' => 'ponumber'
    ],
    '45' => [
        'field' => 'refnumber'
    ],
    '46' => [
        'field' => 'combined flag'
    ],
    '47' => [
        'field' => 'barcode'
    ],
    '48' => [
        'field' => 'number of boxes'
    ],
    '49' => [
        'field' => 'file'
    ]
];