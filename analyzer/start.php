<?php
if (!empty($_REQUEST['data']))
{
    //Get project root path
    $path = $_SERVER['DOCUMENT_ROOT'];
    
    //Include basic actions for controller
    require_once($path.'/analyzer/helpers.php');
    
    //Get input data from request as array
    //GET, POST request type supported
    $data = get_array_from_json();
    
    //Get array of error codes to analyse
    $error_codes = get_error_codes($data);
    
    //Get array of supported input values
    $input_values = get_input_values($data);
    
    //Global array with validation messages
    $messages = [];
    
    //Include database connection class
    require_once($path.'/db_connect/connect.php');
    
    //Include predefined database requests
    require_once($path.'/analyzer/database/db_requests.php');
    
    //Include error codes with description
    require_once($path.'/analyzer/messages/messages.php');
    
    //Include actions to log activities
    //require_once($path.'/analyzer/reporting/loging.php');
   
    //Basic validation of typical values and advanced validation scripts for each error code
    //require_once($path.'/analyzer/validation/basic_validation.php');
    require_once($path.'/analyzer/validation/advanced_validation.php');
    
    //Loop through each error code
    foreach($error_codes as $error_code)
    {
        //Find pattern to analyse error code
        switch ($error_code) {
            case '10832':
                validate_10832($input_values);
                break;
            default:
                add_message('40342', 'error code = '.$error_code);
                finish_validation();
                break;
        }
    }
    
    finish_validation();
} else
{
    add_message('40039', '');
    finish_validation();
}