<?php
//10832 error code
function validate_10832($input_values)
{
    global $shiptrack, $groupdetail, $linedetail, $DL_valid_SKU;
    
    if(empty($input_values['po']))
    {
        add_message('40039', 'value name = po');
        finish_validation();
    }
    
    $conn = Database::getInstance()->dbc;
    
    $obj = $conn->prepare($shiptrack);
    $obj->execute(array($input_values['po'], $input_values['po']));
    $shiptrack_datas = $obj->fetchAll(PDO::FETCH_ASSOC);
    
    if(count($shiptrack_datas) > 1) 
    {
        add_message('20633', '[shiptrack].[PONumber] = '.$input_values['po']);
    }
    if(empty($shiptrack_datas[0]))
    {
        add_message('10733', '[shiptrack].[PONumber] = '.$input_values['po']);
        finish_validation();
    }
    if(empty($shiptrack_datas[0]['TxnID']))
    {
        add_message('10034', '[shiptrack].[PONumber] = '.$input_values['po']);
        finish_validation();
    }
    
    $obj = $conn->prepare($groupdetail);
    $obj->execute(array($shiptrack_datas[0]['TxnID'], $shiptrack_datas[0]['TxnID']));
    $groupdetail_datas = $obj->fetchAll(PDO::FETCH_ASSOC);
    
    if(empty($groupdetail_datas))
    {
        add_message('10721', '[groupdetail].[IDKEY] = '.$shiptrack_datas[0]['TxnID']);
        finish_validation();
    }
    
    foreach($groupdetail_datas as $groupdetail_data)
    {
        if(empty($groupdetail_data['ItemGroupRef_FullName']))
        {
            add_message('10035', '[groupdetail].[IDKEY] = '.$shiptrack_datas[0]['TxnID']);
            finish_validation();
        }
        
        if(empty($groupdetail_data['TxnLineID']))
        {
            add_message('10036', '[groupdetail].[IDKEY] = '.$shiptrack_datas[0]['TxnID']);
            finish_validation();
        }
    }
    
    foreach($groupdetail_datas as $groupdetail_data)
    {
        $obj = $conn->prepare($DL_valid_SKU);
        $obj->execute(array($groupdetail_data['ItemGroupRef_FullName']));
        $DL_valid_SKU_datas = $obj->fetchAll(PDO::FETCH_ASSOC);
        
        if(empty($DL_valid_SKU_datas))
        {
            add_message('10737', '[DL_valid_SKU].[QB_SKU] = '.$groupdetail_data['ItemGroupRef_FullName']);
            finish_validation();
        }
        
        if(empty($DL_valid_SKU_datas[0]['UPC']))
        {
            add_message('20038', '[DL_valid_SKU].[QB_SKU] = '.$groupdetail_data['ItemGroupRef_FullName']);
            finish_validation();
        }
    }
    
    foreach($groupdetail_datas as $groupdetail_data)
    {
        $obj = $conn->prepare($linedetail);
        $obj->execute(array($groupdetail_data['TxnLineID'], $groupdetail_data['TxnLineID']));
        $linedetail_datas = $obj->fetchAll(PDO::FETCH_ASSOC);
        
        if(empty($linedetail_datas))
        {
            add_message('10723', '[linedetail].[GroupIDKEY] = '.$groupdetail_data['TxnLineID']);
        }
    }
    
}