<?php
function get_array_from_json()
{
    $response = false;
    try{
        if ($_SERVER['REQUEST_METHOD'] === 'GET')
        {
            $object = json_decode(urldecode($_REQUEST['data']));
            $response = (array)$object;
        } else if ($_SERVER['REQUEST_METHOD'] === 'POST')
        {
            $object = json_decode(urldecode($_REQUEST['data']));
            $response = (array)$object;
        } else
        {
            add_message('40340', 'request type = '.$_SERVER['REQUEST_METHOD']);
            finish_validation();
        }
    } catch (Exception $ex) {
        add_message('40941', 'json data = '.urldecode($_REQUEST['data']));
        finish_validation();
    }
    
    if ($response)
    {
        return $response;
    } else
    {
        add_message('40039', '');
        finish_validation();
    }
}

function get_error_codes($data)
{
    if (!empty($data['error_codes']))
    {
        return $data['error_codes'];
    } else
    {
        add_message('40042', '');
        finish_validation();
    }
}

function get_input_values($data)
{
    $input_values = [];
    
    if(!empty($data['po']))
    {
        $input_values['po'] = $data['po'];
    } else $input_values['po'] = '';
    
    if (!empty($input_values))
    {
        return $input_values;
    } else
    {
        add_message('40043', '');
        finish_validation();
    }
}

function add_message($error, $reference)
{
    global $messages, $message_type, $rule_type, $field_type;
    $messages[]= [
            'code' => $error,
            'type' => $message_type[substr($error, 0, 1)]['type'],
            'rule' => $rule_type[substr($error, 1, 2)]['rule'],
            'field' => $field_type[substr($error, 3, 2)]['field'],
            'reference' => $reference
        ];
}

function finish_validation()
{
    global $messages;
    $json_messages = json_encode($messages);
    print_r($json_messages);
    die();
}

function debug($value)
{
    print_r('<pre>');
    print_r($value);
    print_r('</pre>');
    die('debug stopped');
}