<?php

$array = [
        'error_codes' => [urldecode($_REQUEST['error'])],
        'po' => urldecode($_REQUEST['po'])
    ];


$response = analyzerCurlRequest($array);

get_readable_error($response);

function analyzerCurlRequest($array)
{
    $json_array = urlencode(json_encode($array));
    
    $actual_link = "http://$_SERVER[HTTP_HOST]";
    $myCurl = curl_init();
    curl_setopt($myCurl, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($myCurl, CURLOPT_HEADER, FALSE);
    curl_setopt($myCurl, CURLOPT_FOLLOWLOCATION, TRUE);
    curl_setopt($myCurl, CURLOPT_MAXREDIRS, 10);
    curl_setopt($myCurl, CURLOPT_ENCODING, "");
    curl_setopt($myCurl, CURLOPT_USERAGENT, "test");
    curl_setopt($myCurl, CURLOPT_AUTOREFERER, TRUE);
    curl_setopt($myCurl, CURLOPT_CONNECTTIMEOUT, 120);
    curl_setopt($myCurl, CURLOPT_TIMEOUT, 120);

    curl_setopt($myCurl, CURLOPT_URL, $actual_link.'/analyzer/start.php');
    curl_setopt($myCurl, CURLOPT_POST, TRUE);
    curl_setopt($myCurl, CURLOPT_POSTFIELDS, http_build_query(array('data' => $json_array)));

    $response = curl_exec($myCurl);
    curl_close($myCurl);

    return $response;
}

function get_readable_error($response)
{
    $object = json_decode($response);
    $messages = (array)$object;
    if (!empty($messages))
    {
        foreach($messages as $message_obj)
        {
            $message = (array)$message_obj;
            echo $message['type'].' '.$message['code'].': '.$message['rule'].' in '.$message['field'].'<br> reference value: '.$message['reference'].'<br><br>';
        }
    } else
    {
        echo 'Can`t find the cause of this error. No validation errors found.';
    }
}