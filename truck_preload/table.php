<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title><?php echo $dealerType['name']; ?> Truck Preload</title>
    <link rel="stylesheet" href="Include/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="Include/bootstrap/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <link rel="stylesheet" href="Include/css/style.css">

    <script src="Include/jQuery/jquery-1.11.3.min.js"></script>
    <script src="Include/bootstrap/js/bootstrap.min.js"></script>
    <script src="Include/jquery-ui-1.11.4.custom/jquery-ui.js"></script>
    <script src="Include/bootstrap-validator-master/js/validator.js"></script>
    <script src="Include/jquery-serializeForm/jquery-serializeForm.min.js"></script>
    <script src="js/script.js"></script>
</head>
<body style="padding:10px;">

<?php
$user = $auth->getName();
echo "<div style='position: absolute; left: 50%; margin-left: -160px; top: 0px; width: 500px; height: 20px;'>Hello, " . $user .". <a href='?is_exit=1'><button class='loginsubmit'>Exit</button></a></div>";
?>
<script>
    var js_results = <?php echo json_encode($results); ?>;
</script>
<br>
<br>
<button class="btn btn-success" style="float: left;" onclick="printPalletBarcodes(js_results)">Generate Pallet Label</button>
<button class="btn btn-success" style="float: right;" onclick="addColumn(js_results)">Add Truck</button>
<br>
<br>
<form data-toggle="validator" role="form" id="truckForm" action="handler.php?t=<?php echo $dealerType['type']; ?>&po=<?php echo $results[0]['PONUMBER']; ?>&ref=<?php echo $results[0]['REFNUMBER']; ?>&u=<?php echo $auth->getId() ?>" method="POST">

    <div class="group">
        <h4><?php echo $dealerType['name']; ?> Products</h4>
        <table data-toggle='table' id='truck_table' class='table table-bordered display table-striped'>
            <thead>
            <tr>
                <th class='info'>PALLET</th>
                <th class='info'>No Truck</th>
                <?php
                foreach($trucks as $truck)
                {
                    echo "<th class='danger truck_name' data-truck='".$truck."'>Truck ".$truck." 
                    <input type=\"button\" onclick=\"assignToTruck('".$truck."', true);\" value='' alt='Select all' />
                    <input type=\"button\" onclick=\"assignToTruck('".$truck."', false);\" value='' alt='Unselect all' />
                    </th>";
                }
                ?>
            </tr>
            </thead>
            <tbody>
            <?php
            $i = 0;
            foreach ($results as $result)
            {
                echo
                    "
				<tr class='truck_pallet ".$result['PALLET']."' data-pallet='".$result['PALLET']."'>
					<td><b>".$result['PALLET']."</b> <b class=\"".$result['PALLET_SIZE']."\">".$result['PALLET_SIZE']."</b> </td>
					<td><input type='radio' name='".$result['PALLET']."' value='NULL' ".($result['TRUCK']==null ? 'checked' : '' )." ></td>";


                foreach($trucks as $truck)
                {
                    if($result['TRUCK'] == $truck)
                    {
                        echo "<td class='success'><input onchange='validate_radio(this, event)' type='radio' name='".$result['PALLET']."' value='".$truck."' checked='checked'></td>";
                    } else
                    {
                        echo "<td class='success'><input onchange='validate_radio(this, event)' type='radio' name='".$result['PALLET']."' value='".$truck."'></td>";
                    }
                }
                echo "</tr>";
                $i++;
            }
            ?>
            </tbody>
        </table>
    </div>

    <br>
    <button type="submit" style="float: right;" class="btn btn-success">Save Changes</button>
    <button type="button" onclick="printLabelsGo('<?php echo $po; ?>', '<?php echo $dealerType['type']; ?>')" style="float: right; margin-right: 20px;" class="btn btn-success">Print Labels</button>
    <button type="button" onclick="removePallets('<?php echo $po; ?>', '<?php echo $auth->getId() ?>')" style="float: left;" class="btn btn-success">Remove all trucks</button>
    <br>
    <br>
</form><br>

<div id="dialog">
    <div id="content"></div>
</div>
<script>
    $(document).ready(function(e){
        truckNum = <?php echo ($trucksCount+1) ?>;
    });
</script>
</body>
</html>
