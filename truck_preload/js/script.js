var truckNum = 1;
$(document).ready(function() {
    preventInput();	
    var dialog = $('#dialog').dialog({
        autoOpen: false,
        height: 'auto',
        width: 'auto',
        resizable: false,
        autoResize: true,
        modal: true,
		title: '',
        overlay: { backgroundColor: "#000", opacity: 0.5 },
    });
    dialog.dialog('close');

    //Submit event with prevalidation
    $('#truckForm').on('submit', function (e) {
        if (e.isDefaultPrevented()) {
            alert('Form is not valid.');
        } else {
            dialog.dialog('option', 'title', 'Saving data...');
            $('#content').html("<p>Saving data to DB...</p><img src='Include/gif/294.GIF' height='150px' width='150px'>");
            dialog.dialog('open');
            console.log('Form is valid.');
            event.preventDefault();
            
            var url = $(this).attr("action");
            var formData = {};
            formData = $(this).serializeForm();
            var formDataStr = JSON.stringify( formData );
            
            $.ajax({
                url: url,
                type: 'POST',
                data: { truckStr: formDataStr },
                complete: function(data){
                        console.log(data.responseText);
                        result = data.responseText;
                        dialog.dialog('close');

                        dialog.dialog('option', 'title', 'Result');
                        $('#content').html(result);
                        dialog.dialog('open');
                },
                error: function (request, status, error) {
                        console.log("Ajax request error: "+request.responseText);
                }
            });
        }
    });
});

function assignToTruck( truckname, isAdd ) {
    let max_pallets = 28,
        radios = [],
        filled = $( "input:radio[value='"+truckname+"']" ).filter(':checked').length;
    $( 'tr.truck_pallet' ).each(function(){
        let pallet  = $(this).attr('data-pallet');
        let radio   = $( 'input:radio[name='+pallet+']' ).filter(':checked').val();
        let compare = isAdd ? 'NULL' : truckname;
        if( radio == compare ) {
            if( (isAdd && radios.length < (max_pallets-filled)) || !isAdd ) {                
                radios.push(pallet);
            }
        }
    });
    $(radios).each( function(){
        let compare = isAdd ? truckname : 'NULL';
        $( "input:radio[name="+this+"][value!='"+compare+"']" ).prop('checked',false);
        $( "input:radio[name="+this+"][value='"+compare+"']" ).prop('checked',true);
    });
}

function validate_radio(obj, event)
{
    event.stopPropagation();
    var max_pallets = 28;
    var value = $(obj).val();
    console.log(value);
    
    var pallet_count = 0;
    
    $('input[type = radio]:checked').each(function(){
	if ($(this).val() == value)
        {
            pallet_count++;
        }
    });
    if (pallet_count > max_pallets)
    {
        $(obj).prop('checked', false);
        alert("Truck maximum pallet count reached.");
    }
}

function preventInput()
{
	$('input[type="number"]').keydown(function(e){
	   var ingnore_key_codes = [8,46,48,49,50,51,52,53,54,55,56,57,96,97,98,99,100,101,102,103,104,105];
	   if ($.inArray(e.keyCode, ingnore_key_codes) >= 0){
		  e.preventDefault();
	   }
	});
}

function addColumn(products)
{
	var dialog = $('#dialog').dialog({
        autoOpen: false,
        height: 'auto',
        width: 'auto',
        resizable: false,
        autoResize: true,
        modal: true,
		title: 'Add Truck',
        overlay: { backgroundColor: "#000", opacity: 0.5 },
    });
	dialog.dialog('close');
	
	$('#content').html( "Truck ID:<br><input id='truck_name' value='Truck"+truckNum+"'><br><br><button style='cursor: pointer;' id='save_truck_name'>Add Truck</button>");
        
        $('#truck_name').focus(function() {
            setTimeout((function(el) {
                var strLength = el.value.length;
                return function() {
                    if(el.setSelectionRange !== undefined) {
                        el.setSelectionRange(strLength, strLength);
                    } else {
                        $(el).val(el.value);
                    }
            }}(this)), 5);
        });
        
	dialog.dialog('open');
	
	$('#save_truck_name').click(function() {
                var name = $('#truck_name').val();
                name = $.trim(name);
                var truck_not_exist = true;

                var old_names = new Array();
                $('.truck_name').each(function(){
                        var old_name = $(this).attr('data-truck');
                        old_name = old_name.toLowerCase();
                        old_names.push(old_name);
                });
                if(old_names.includes(name.toString().toLowerCase())){
                    truck_not_exist = false;
                }
                if( name == 'NULL' ){
                    truck_not_exist = false;
                }
                var regex = /^[a-z0-9\-_]{2,32}$/i;
                
                if( !truck_not_exist ) {
                    dialog.dialog('close');
                    dialog.dialog('option', 'title', 'Validation Error');
                    $('#content').html("<b style='color:red;'>Error:</b> Truck already exists");
                    dialog.dialog('open');
                } else if( !name || !regex.test(name) ) {
                    dialog.dialog('close');
                    dialog.dialog('option', 'title', 'Validation Error');
                    $('#content').html("<b style='color:red;'>Error:</b> Invalid truck name<br/>Truck name should be 2-32 characters long and should contain only:<ul><li>Letters ( <b>A-z</b> )</li><li>Numbers ( <b>0-9</b> )</li><li>Dash ( <b>-</b> )</li><li>Underscore ( <b>_</b> )</li></ul>");
                    dialog.dialog('open');
                } else {
                    truckNum++;
                    $('#truck_table').find('th').eq(-1).after('<th class="danger truck_name" data-truck="'+name+'">Truck '+name+' <input type="button" onclick="assignToTruck(\''+name+'\', true)" value="+"><input type="button" onclick="assignToTruck(\''+name+'\', false)" value="–"></th>');
                    $('#truck_table tr').each(function(n){
                            if(n != 0)
                            {
                                    i = n-1;
                                    $(this).append("<td  class='success'><input onchange='validate_radio(this, event)' type='radio' name='"+$(this).attr('data-pallet')+"' value='"+name+"'></td>");
                            }
                    });
                    preventInput();
                    dialog.dialog('close');
                }
        });
			
	
}

function csv_report(po)
{
		var form = $('#truckForm');
		var formData = {};
		formData = $(form).serialize();
		
		$.ajax({
			url: 'generate_report.php?po='+po,
			type: 'POST',
			data: formData,
			complete: function(data){
				console.log(data.responseText);
				var new_url = 'download.php?file='+data.responseText;
				OpenInNewTab(new_url);
			},
			error: function (request, status, error) {
				console.log("Ajax request error: "+request.responseText);
			}
		});
}

function OpenInNewTab(url) {
  var win = window.open(url, '_blank');
  win.focus();
}

function removePallets(po, u)
{
	$.ajax({
			url: 'removePallets.php?po='+po+'&u='+u,
			type: 'POST',
			complete: function(data){
				alert(data.responseText);
                                truckNum = 1;
				location.reload();
			},
			error: function (request, status, error) {
				console.log("Ajax request error: "+request.responseText);
			}
		});
}

function truckPreloadGo(po){
	var url = '/truck_preload/index.php?po='+po;
	OpenInNewTab(url);
}

function printPalletBarcodes(data){
    var po = data[0].PONUMBER;
    var ref = data[0].REFNUMBER;
    if(confirm('Are you sure, you saved changes?'))
    {
        console.log(po);
        console.log(ref);
        $.ajax({
            url: '/shipping_module/phpFunctions/functionsPalletBarcode.php?po='+po+'&ref='+ref,
            type: 'POST',
            complete: function(data){
                    console.log(data.responseText);
                    productCode = JSON.parse(data.responseText);
                                    if (Object.prototype.toString.call(productCode) === "[object Object]") {
                                            var path = productCode.path;
                                            if (path != 'false')
                                            {
                                                var time = generate_unique_string();
                                                window.open('newTab.php?value='+path+'&time='+time);
                                            } else
                                            {
                                                alert('Error: '+productCode.er);
                                            }
                                    } else
                                    {
                                        alert('Error: '+data.responseText);
                                    }
            },
            error: function (request, status, error) {
                    console.log("Ajax request error: "+request.responseText);
            }
        });
    } else
    {
        console.log('cancelled.');
    }
}

function generate_unique_string()
{
	var timestamp = new Date().getUTCMilliseconds();
	return timestamp;
}

function printLabelsGo(po, type){
        var url = '/shipping_module/hddc_print_labels.php?t='+type+'&po='+po;
        OpenInNewTab(url);
}