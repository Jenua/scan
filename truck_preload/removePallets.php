<?php
ini_set('max_execution_time', 1200);
$path = $_SERVER['DOCUMENT_ROOT'];
require_once($path.'/db_connect/connect.php');
if(isset($_REQUEST['po']) && !empty($_REQUEST['po']) && isset($_REQUEST['u']) && !empty($_REQUEST['u']))
{
	$po = $_REQUEST['po'];
	$user = $_REQUEST['u'];
	$query="DELETE FROM [dbo].[truck_preload]
      WHERE [PONUMBER] = '".$po."'";
	$result = runQuery_empty_result($query);
	if ($result)
	{
        print_r('Trucks removed for this order.');
		saveDiff( $user, $po, 2, json_encode([ 'message' => 'All trucks removed' ]) );		
	} else print_r('Error. Can not remove trucks.');
} else
{
	echo "Can not get po.";
}

function saveDiff( $user, $po, $type, $diff ) {
	$conn = Database::getInstance()->dbc;	
	$query = "	INSERT INTO [dc_log] (
					  [user]
					, [po]
					, [type]
					, [diff]
					, [date])
				VALUES (
					  ".$conn->quote( $user )."
					, ".$conn->quote( $po )."
					, ".$conn->quote( $type )."
					, ".$conn->quote( $diff )."
					, GETDATE()
				);";
	runQuery_empty_result($query);
}

function runQuery_empty_result($query)
{
	try
	{
		$conn = Database::getInstance()->dbc;
		$stmt = $conn->prepare($query);
		$result = $stmt->execute();
		return $result;
	}catch (Exception $e) {
		die("<br>".$e."<br>");
		return false;
	}
}
?>