<?php
/*print_r("<pre>");
print_r($_REQUEST);
print_r("</pre>");
die();*/
ini_set('max_execution_time', 1200);
$path = $_SERVER['DOCUMENT_ROOT'];
require_once($path.'/db_connect/connect.php');

if(isset($_REQUEST['po']) && !empty($_REQUEST['po']) && isset($_REQUEST['ref']) && !empty($_REQUEST['ref']) && isset($_REQUEST['u']) && !empty($_REQUEST['u']))
{
	$po = $_REQUEST['po'];
	$ref = $_REQUEST['ref'];
	$type = $_REQUEST['t'];
	$user = $_REQUEST['u'];
	$palletsStr = $_REQUEST['truckStr'];
	$pallets = json_decode( $palletsStr, true );
	
	$dataBefore = getDataArr($po);
	
	$queries = '';
	$queries = generate_query($pallets, $po, $ref);	
	
	$result = beginTransaction();
	if ($result)
	{
		try {
			runQuery_empty_result($queries);
		}
		catch (PDOException $e)
		{
			rollbackTransaction();
			die('Error. Can not run queries: '.$queries.'');
		}
		
		$result = commitTransaction();
		if (!$result)
		{
			rollbackTransaction();
			die('Error. Can not commit transaction.');
		} else 
		{
			echo 'Truck preload was successfully saved.';
		}
		
		$dataAfter = getDataArr($po);
		$dataDiff  = compareDataArr( $dataBefore, $dataAfter );
		if( !empty($dataDiff) && $dataDiff != '[]' ) {
			// 1 = PALLET Changelog | 2 = TRUCK Changelog
			saveDiff( $user, $po, 2, json_encode($dataDiff) );
		}
	} else die('Error. Can not begin transaction.');
} else
{
	echo "Can not get data.";
}

function getDataArr( $po ) {
	$query = "	SELECT
					  DISTINCT p.[Pallet]
					, t.[Truck]	
				FROM [pallet_preload] AS p
				LEFT JOIN [truck_preload] AS t ON t.[Pallet] = p.[Pallet] AND t.[PONumber] = p.[PONumber]
				where p.[PONumber] = '".addquote($po)."'
				;";
	$result = runQuery($query);	
	$resArr = [];
	foreach( $result as $res ) {
		$TRK = $res['Truck'];
		$PLT  = $res['Pallet'];
		
		if( empty($resArr[$PLT]) ) {
			$resArr[$PLT] = [];
		}
		if( empty($resArr[$PLT][$TRK]) ) {
			$resArr[$PLT][$TRK] = 'removed';
		}
		$resArr[$PLT][$TRK] = 'added';
	}	
	return $resArr;
}
function compareDataArr( $before, $after ) {
	$diff = [];
	foreach( $before as $PLT => $B ){
		if(!empty($after[$PLT])) {
			$A = $after[$PLT];
		} else {
			$A = [];
		}			
		$diff_before = array_diff_assoc( $B, $A );
		$diff_after  = array_diff_assoc( $A, $B );
		foreach( $diff_before as $dBk => $dB ) {
			if( $dBk ) {
				$diff[$PLT][$dBk]['before'] = $dB;
				$diff[$PLT][$dBk]['after'] = 'removed';
			}
		}
		foreach( $diff_after as $dAk => $dA ) {
			if( $dAk ) {
				$diff[$PLT][$dAk]['after'] = $dA;
				if( empty($diff[$PLT][$dAk]['before']) ) {
					$diff[$PLT][$dAk]['before'] = 'removed';	
				}
			}
		}		
	}
	return $diff;
}
function saveDiff( $user, $po, $type, $diff ) {
	$conn = Database::getInstance()->dbc;	
	$query = "	INSERT INTO [dc_log] (
					  [user]
					, [po]
					, [type]
					, [diff]
					, [date])
				VALUES (
					  ".$conn->quote( $user )."
					, ".$conn->quote( $po )."
					, ".$conn->quote( $type )."
					, ".$conn->quote( $diff )."
					, GETDATE()
				);";
	runQuery_empty_result($query);
}

function generate_query($pallets, $po, $ref)
{
	$queries = '';
	foreach($pallets as $pallet => $truck)
	{
			if( $truck == 'NULL' ) { continue; }
		
			$queries.= "
			if exists (SELECT * FROM [dbo].[truck_preload] with (updlock,serializable) where [REFNUMBER] = '".addquote($ref)."' AND [PONUMBER] = '".addquote($po)."' AND [PALLET] = '".addquote($pallet)."')
			begin
			   UPDATE [dbo].[truck_preload]
			   SET [PONUMBER] = '".addquote($po)."'
				  ,[REFNUMBER] = '".addquote($ref)."'
                                  ,[PALLET] = '".addquote($pallet)."'
				  ,[TRUCK] = '".addquote($truck)."'
				  ,[DISTRIBUTE_DATE] = GETDATE()
			   WHERE [REFNUMBER] = '".addquote($ref)."' AND [PONUMBER] = '".addquote($po)."' AND [PALLET] = '".addquote($pallet)."'
			end
			else
			begin
			   INSERT INTO [dbo].[truck_preload]
			   ([PONUMBER]
			   ,[REFNUMBER]
			   ,[PALLET]
                           ,[TRUCK]
			   ,[DISTRIBUTE_DATE])
		 VALUES
			   ('".addquote($po)."'
			   ,'".addquote($ref)."'
			   ,'".addquote($pallet)."'
			   ,'".addquote($truck)."'
			   ,GETDATE())
			end;";
	}
	$palletNames="";
	foreach($pallets as $pallet => $truck)
	{
		if( $truck != 'NULL' ) {
			if( $palletNames != '' ) {
				$palletNames.=", ";
			}
			$palletNames.="'".$pallet."'";
		}
	}
	if( $palletNames != '' ) {
		$notInCondition = "AND [PALLET] NOT IN (".$palletNames.")";
	} else {
		$notInCondition = ';';
	}
	
	$queries.= "
		   DELETE FROM [dbo].[truck_preload]
		   WHERE [REFNUMBER] = '".addquote($ref)."' AND [PONUMBER] = '".addquote($po)."'".$notInCondition;
	return $queries;
}


function beginTransaction()
{
	try
	{
		$conn = Database::getInstance()->dbc;
		$result = $conn->beginTransaction();
		return $result;
	}catch (Exception $e) {
		die("<br>".$e."<br>");
		return false;
	}
}

function commitTransaction()
{
	try
	{
		$conn = Database::getInstance()->dbc;
		$result = $conn->commit();
		return $result;
	}catch (Exception $e) {
		die("<br>".$e."<br>");
		return false;
	}
}

function rollbackTransaction()
{
	try
	{
		$conn = Database::getInstance()->dbc;
		$result = $conn->rollback();
		return $result;
	}catch (Exception $e) {
		die("<br>".$e."<br>");
		return false;
	}
}

function runQuery_empty_result($query)
{
	try
	{
		$conn = Database::getInstance()->dbc;
		$stmt = $conn->prepare($query);
		$result = $stmt->execute();
		return $result;
	}catch (Exception $e) {
		die("<br>".$query."<br>");
		return false;
	}
}
function runQuery($query)
{
	try
	{
		$conn = Database::getInstance()->dbc;
		return exec_query($conn, $query);
	}catch (Exception $e) {
		die("<br>".$e->getMessage()."<br><br>".$query);
		return false;
	}
}
function exec_query($conn, $query) {
	try {
		$result = $conn->prepare($query);
		$result->execute();
		return $result;
	}
	catch(PDOException $e) {
		//print_r($e->getMessage());
	}
}
function addquote($str)
{
	$str = str_replace("'", "`", $str);
	return $str;
}
?>