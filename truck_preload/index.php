<?php
session_start();
$path = $_SERVER['DOCUMENT_ROOT'];
require_once($path.'/db_connect/connect.php');
require_once($path.'/auth/auth.php');
require_once($path.'/pallet_preload/Include/Twig/Autoloader.php');
 
function getDealerType( $conn, $type ) {
	
	$query   = "SELECT * FROM [dc_type] WHERE id = ".$conn->quote( $type );
	$results = exec_query($conn, $query);
	return $results;
}

Twig_Autoloader::register();
$loader = new Twig_Loader_Filesystem('templates/Pages');
$twig = new Twig_Environment($loader, array(
	'cache'       => 'compilation_cache',
	'auto_reload' => true
));

$auth = new AuthClass();
$auth = new AuthClass();
if( isset($_POST['login']) && isset($_POST["password"]) ) {
	if( !$auth->auth($_POST["login"], $_POST["password"]) ) {		
		$loginName = $_POST["login"] ? $_POST["login"] : '';
		$wrongCredentials = $twig->render('loginPage.html', array('login' => $loginName, 'errmessage' => 'Error: wrong login or password'));
		die($wrongCredentials);		
	}
}
if( isset($_GET["is_exit"]) ) {
    if ($_GET["is_exit"] == 1) {
        $auth->out();
        header("Location: ?is_exit=0");
    }
}
if( !$auth->isAuth() ) {	
	$noAuth = $twig->render('loginPage.html', array('login' => '', 'errmessage' => ''));
	die($noAuth);
}
if ( empty($_GET['po']) )
{
	$noPO = $twig->render('fatalErorr.html', array('error' => 'Error: No PONumber'));
	die($noPO);
}

$conn = Database::getInstance()->dbc;

$type = empty($_REQUEST['t']) ? '1' : $_REQUEST['t'];
$dealer = getDealerType($conn,$type);

$po = $_GET['po'];
$po = trim($po);
$query = "SELECT 
		[pallet_preload].[PONUMBER],
		[pallet_preload].[REFNUMBER],
		[pallet_preload].[PALLET],				
		[truck_preload].[TRUCK],
		ISNULL([pallet_preload].[PALLET_SIZE], 'long') as [PALLET_SIZE]
	FROM [dbo].[pallet_preload]
	LEFT JOIN [dbo].[truck_preload] ON [truck_preload].[PONUMBER] = [pallet_preload].[PONUMBER] AND
		[truck_preload].[REFNUMBER] = [pallet_preload].[REFNUMBER] AND
		[truck_preload].[PALLET] = [pallet_preload].[PALLET]
	WHERE [pallet_preload].[PONUMBER] = '" . $po . "'
	GROUP BY [pallet_preload].[PONUMBER],
		[pallet_preload].[REFNUMBER],
		[pallet_preload].[PALLET],				
		[truck_preload].[TRUCK],
		[pallet_preload].[PALLET_SIZE]
	ORDER BY LEN([pallet_preload].[PALLET]), [pallet_preload].[PALLET]";
$results = exec_query($conn,$query);
if (!isset($results) || empty($results)) die('<br>Can not get data on this PO: ' . $po . '<br>');

$trucks = get_trucks($results);
$trucksCount = count($trucks);

$truckPreload = $twig->render('truckPreload.html', array(
	'results'		=> $results,
	'jsonResults'	=> json_encode( $results ),
	'trucks'		=> $trucks,
	'dealer'		=> $dealer,
	'po'			=> $po,
	'ref'			=> $results[0]['REFNUMBER'],
	'trucksCount'	=> $trucksCount,
	'username'		=> $auth->getName(),
	'userid'		=> $auth->getId(),
));
die($truckPreload);

require 'table.php';

function exec_query($conn,$query)
{
    try {
        $result = $conn->prepare($query);
        $result->execute();
        $result = $result->fetchAll(PDO::FETCH_ASSOC);
        $conn = null;
        return $result;
    } catch (PDOException $e) {
        $conn = null;
        die($e->getMessage());
    }
}

function get_trucks($results)
{
    $trucks = array();
    foreach ($results as $result) {
        if (!empty($result['TRUCK']) && !in_array($result['TRUCK'], $trucks)) {
            $trucks[] = $result['TRUCK'];
        }
    }
    sort($trucks);
    return $trucks;
}