<?php
$path = $_SERVER['DOCUMENT_ROOT'];
require_once($path.'/db_connect/connect.php');

function getRequestURL()
{
        $result = '';
        $default_port = 80;

        if (isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS']=='on')) {
                $result .= 'https://';
                $default_port = 443;
        } else {
                $result .= 'http://';
        }
        $result .= $_SERVER['SERVER_NAME'];
        if ($_SERVER['SERVER_PORT'] != $default_port) {
                $result .= ':'.$_SERVER['SERVER_PORT'];
        }
        $result .= $_SERVER['REQUEST_URI'];
        return $result;
}


class AuthClass {
	private $_loginPassword = array();
	
	public function __construct() {
		$conn = Database::getInstance()->dbc;
		
		$query = "SELECT [ID]
			  ,[login]
			  ,[password]
			  ,[permission]
			  ,[name]
			  ,[printer_label]
			  ,[printer_bol]
			  ,[printer_hddc1]
			  ,[printer_hddc2]
			  ,[rits_printer]
			  ,[rits_print_copies]
		  FROM [".USER_TABLE."]";
		$result = $conn->prepare($query);
		$result->execute();
		$result = $result->fetchAll(PDO::FETCH_ASSOC);
		$conn = null;
		if (!isset($result) || empty($result)) die('<br>Authorization failed. Can`t get user data.<br>'); else
		{
			foreach ($result as $res)
			{
				$this->_loginPassword[] = array("login" => $res['login'], "password" => $res['password'], "permission" => $res['permission'], "id" => $res['ID'], "name" => $res['name'], "printer_label" => $res['printer_label'], "printer_bol" => $res['printer_bol'], "printer_hddc1" => $res['printer_hddc1'], "printer_hddc2" => $res['printer_hddc2'],
                    "rits_printer" => array("name" => $res['rits_printer'], "copies" => $res['rits_print_copies']));
			}
		}
    }
	
    public function isAuth() {
        if (isset($_SESSION["is_auth"])) {
            return $_SESSION["is_auth"];
        }
        else return false;
    }
     
    public function auth($login, $passwors) {
		$is_auth = false;
		$key = false;
		foreach ($this->_loginPassword as $key_cur => $loginPassword)
		{
			if ($login == $loginPassword["login"] && $passwors == $loginPassword["password"]) 
			{
				$is_auth = true;
				$key = $key_cur;
			}
		}
        if ($is_auth)
		{
            $_SESSION["is_auth"] = true;
            $_SESSION["login"] = $login;
			$_SESSION["id"] = $this->_loginPassword[$key]['id'];
			$_SESSION["permission"] = $this->_loginPassword[$key]['permission'];
			$_SESSION["name"] = $this->_loginPassword[$key]['name'];
			if(isset($this->_loginPassword[$key]['printer_label']) && !empty($this->_loginPassword[$key]['printer_label']))
			{
				$_SESSION["printer_label"] = $this->_loginPassword[$key]['printer_label'];
			} else
			{
				$_SESSION["printer_label"] = 'false';
			}
			
			if(isset($this->_loginPassword[$key]['printer_bol']) && !empty($this->_loginPassword[$key]['printer_bol']))
			{
				$_SESSION["printer_bol"] = $this->_loginPassword[$key]['printer_bol'];
			} else
			{
				$_SESSION["printer_bol"] = 'false';
			}

			if(isset($this->_loginPassword[$key]['printer_hddc1']) && !empty($this->_loginPassword[$key]['printer_hddc1']))
			{
				$_SESSION["printer_hddc1"] = $this->_loginPassword[$key]['printer_hddc1'];
			} else
			{
				$_SESSION["printer_hddc1"] = 'false';
			}

			if(isset($this->_loginPassword[$key]['printer_hddc2']) && !empty($this->_loginPassword[$key]['printer_hddc2']))
			{
				$_SESSION["printer_hddc2"] = $this->_loginPassword[$key]['printer_hddc2'];
			} else
			{
				$_SESSION["printer_hddc2"] = 'false';
			}
			if(isset($this->_loginPassword[$key]['rits_printer']) && is_array($this->_loginPassword[$key]['rits_printer']))
			{
				$_SESSION["rits_printer"]["name"] = empty($this->_loginPassword[$key]['rits_printer']['name']) ?
                    $_SESSION["printer_hddc2"] :
                    $this->_loginPassword[$key]['rits_printer']['name'];
				$_SESSION["rits_printer"]["copies"] = empty($this->_loginPassword[$key]['rits_printer']['copies']) ?
                    1 :
                    $this->_loginPassword[$key]['rits_printer']['copies'];
			} else
			{
				$_SESSION["rits_printer"] = 'false';
			}
            return true;
        }
        else {
            $_SESSION["is_auth"] = false;
            return false; 
        }
    }
     
    public function getLogin() {
        if ($this->isAuth()) {
            return $_SESSION["login"];
        }
    }
	
	public function getId() {
        if ($this->isAuth()) {
            return $_SESSION["id"];
        }
    }
	
	public function getPermission() {
        if ($this->isAuth()) {
            return $_SESSION["permission"];
        }
    }
	
	public function getName() {
        if ($this->isAuth()) {
            return $_SESSION["name"];
        }
    }
	
	public function getPrinter_label() {
        if ($this->isAuth()) {
            return $_SESSION["printer_label"];
        }
    }
	
	public function getPrinter_bol() {
        if ($this->isAuth()) {
            return $_SESSION["printer_bol"];
        }
    }
    
	public function getPrinter_hddc1() {
        if ($this->isAuth()) {
            return $_SESSION["printer_hddc1"];
        }
    }

	public function getPrinter_hddc2() {
        if ($this->isAuth()) {
            return $_SESSION["printer_hddc2"];
        }
    }

    public function getRitsPrinter() {
        if ($this->isAuth()) {
            return $_SESSION["rits_printer"];
        }
    }
     
     
    public function out() {
        $_SESSION = array();
        session_destroy();
    }
}