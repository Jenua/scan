<?php
ini_set('max_execution_time', 7200);
ini_set("memory_limit", "2000M");
//print_r($_REQUEST);
if(isset($_REQUEST['value']) && !empty($_REQUEST['value']) && isset($_REQUEST['carrier']) && !empty($_REQUEST['carrier']) && isset($_REQUEST['pro']) && !empty($_REQUEST['pro']))
{
	$array = json_decode($_REQUEST['value']);
	$carrier = json_decode($_REQUEST['carrier']);
	$pro = json_decode($_REQUEST['pro']);
	
	$results = array();
	
	foreach($array as $key=>$value)
	{
		$value = (array)$value;
		$result_spb = saveProBol($value['id'], $value['bol'], $carrier);
		if ($result_spb == 'Order unrated.')
		{
			$resultTemp= doRequest($value['po'], $value['id'], $pro, $value['bol']);
                        $results[]= $resultTemp;
		} else $results = 'Error. Can not get data to generate Labels.';
	}
	//print_r($results);
	$results = json_encode($results);
	print_r($results);
} else
{
	$results = 'Error. Can not get data to generate Labels.';
	$results = json_encode($results);
	print_r($results);
}

function get_uid()
{
	$uid = uniqid('', true);
	return $uid;
}

function doRequest($po, $id, $pro, $bol)
{
	$myCurl = curl_init();
	$actual_link = "http://$_SERVER[HTTP_HOST]";
	curl_setopt($myCurl, CURLOPT_URL, $actual_link.'/shipping_module/phpFunctions/functions.php');
	curl_setopt($myCurl, CURLOPT_POST, TRUE);
	curl_setopt($myCurl, CURLOPT_POSTFIELDS, http_build_query(array('value' => $po, 'id' => $id, 'pro' => $pro, 'bol' => $bol, 'combine' => 'combine')));
	
	ob_start();
	$response = curl_exec($myCurl);
	$data = ob_get_clean();
	curl_close($myCurl);

	return $data;
}

function saveProBol($id, $bol, $carrier)
{
	$myCurl = curl_init();
	$actual_link = "http://$_SERVER[HTTP_HOST]";
	curl_setopt($myCurl, CURLOPT_URL, $actual_link.'/shipping_module/phpFunctions/unrateOrder.php');
	curl_setopt($myCurl, CURLOPT_POST, TRUE);
	curl_setopt($myCurl, CURLOPT_POSTFIELDS, http_build_query(array('value' => $id, 'bol' => $bol, 'carrier' => $carrier)));
	
	ob_start();
	$response = curl_exec($myCurl);
	$data = ob_get_clean();
	curl_close($myCurl);

	return $data;
}