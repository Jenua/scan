<?php
ini_set('max_execution_time', 1200);

$header_name = "Combine Orders";
require_once( "../shipping_module/header.php" );

require_once('functions.php');
?>
<?php
if ($auth->isAuth()) {

?>

  <title><?=$header_name?></title>
  <link rel="stylesheet" href="Include/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="Include/bootstrap/css/bootstrap-theme.min.css">
  <link href="Include/jquery-ui-1.11.4.custom/themes/black-tie/jquery-ui.css" rel="stylesheet" type="text/css" />

  <script src="Include/bootstrap/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="Include/jquery-ui-1.11.4.custom/jquery-ui.min.js"></script>

  <script src="js/script.js?rndmid=<?php echo uniqid(); ?>"></script>
  <script type="text/javascript" src="js/clipboard.min.js"></script>
  <script type="text/javascript" src="js/lgr_clipboard.min.js"></script>
  <style>
			.ui-dialog-titlebar-close { 
				outline: none; border: none;
			}
			/*:link { color: #0000EE !important; }
			:visited { color: #551A8B !important; }
			.ui-autocomplete {
				max-height: 600px;
				overflow-y: auto;
				overflow-x: hidden;
			}*/
			#content
			{
				max-height: 600px;
				overflow-y: auto;
				overflow-x: hidden;
			}
			.ui-autocomplete { position: fixed; cursor: default;z-index:30 !important;}
			.calculateCost
			{
				float: right;
				margin-right: 20px;
				margin-top: 20px;
			}
			.typeCost
			{
				float: right;
				margin-right: 60px;
				margin-top: 20px;
			}
			.combineOrders
			{
				margin-top: 20px;
			}
                        .copy_icon
                        {
                            cursor: pointer;
                        }
                        .order-supply{
                            background: url('Include/pictures/supply.png');
                        }
                        .order-supply2{
                            background: url('Include/pictures/negative.png');
                        }
                        .order-supply-ok{
                            background: url('Include/pictures/check.png');
                        }
                        .order-supply-no{
                            background: url('Include/pictures/not_calculated.png');
                        }
                        .status {
                            text-align: left;
                            padding: 0;
                            white-space: nowrap;
                       }
                       .status i {
                            margin-left: 2px;
                            margin-right: 2px;
                            display: inline-block;
                            width: 32px;
                            height: 32px;
                            vertical-align: middle;
                            background-size: 100%;
                            border-radius: 5px;
                        }
		</style>
</head>
<body>
<?php require_once($path.'/Include/header_section.php');?>
<div class="container">
<?php 
	$user = $auth->getName();
	echo "<div style='position: absolute; left: 50%; margin-left: -160px; top: 0px; width: 500px; height: 20px;'>Hello, " . $user .". <a href='?is_exit=1'><button class='loginsubmit'>Exit</button></a></div>";
	?>
<br><h4>Orders that go to same location and can be combined: </h4>
<form data-toggle="validator" role="form" id="orderForm" action="handler.php" method="POST">
<input type="hidden" name="user" value="<?php echo $user; ?>">
<div class="bs-example">
    <div class="panel-group" id="accordion">
<?php
$conn = Database::getInstance()->dbc;
$query = "EXEC [dbo].[combine_orders2]";
$results = runQuery2($conn, $query);
$results = sort_results($results);
/*print_r('<pre>');
print_r($results);
print_r('</pre>');
die('stop');*/

if ($results)
{
	$href = 1;
	foreach ($results as $result)
	{
            $result = sort_date($result);
?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" onclick="clearCheckboxes()" href="#<?php echo $href; ?>">
					<?php 
						$ponumbersInTitle = "PO: ";
						$comma_separated_po = "";
						foreach ($result as $res)
						{
							$ponumbersInTitle.= $res['PONumber'].", ";
						}
						$ponumbersInTitle = rtrim($ponumbersInTitle, ", ")."";
						$countTitle = "orders: ".count($result)."";
						echo "<strong>".$result[0]['CustomerRef_FullName'].", ".$result[0]['ShipAddress_Addr2'].", - ".$result[0]['ShipAddress_City'].", ".$result[0]['ShipAddress_State']."</strong>, ".$countTitle.", ".$ponumbersInTitle.". Date: <strong>".$result[0]['TimeCreated']."</strong>"; 
					?>
					</a>
                </h4>
            </div>
            <div id="<?php echo $href; ?>" class="panel-collapse collapse">
		<table data-toggle='table' class='table table-bordered display'>
			<thead>
				<tr class='info'>
                                        <th>TimeCreated</th>
					<th>PO</th>
					<th>RefNumber</th>
					<th>Customer</th>
					<th>Zip</th>
					<th>Weight</th>
					<th>Cost</th>
                                        <th>Shipping Method</th>
					<th>Carrier</th>
                                        <th>Inventory</th>
					<th>Combine</th>
				</tr>
			</thead>
			<tbody>
				<?php
				foreach ($result as $res)
				{
                                    if ($res['inventory_flag'] == 'negative')
                                    {
                                        $supply2 = '<a target="_blank" href="/shipping_module/inventory_allocation.php?po='.urlencode($res['PONumber']).'&ref='.urlencode($res['RefNumber']).'"><i class="order-supply2" title="Not enough items for this order! Click for details. Calculated date: '.$res['inventory'].'"></i></a>';
                                    } else if ($res['inventory_flag'] == 'positive')
                                    {
                                        $supply2 = '<a target="_blank" href="/shipping_module/inventory_allocation.php?po='.urlencode($res['PONumber']).'&ref='.urlencode($res['RefNumber']).'"><i class="order-supply-ok" title="Inventory - OK. Calculated date: '.$res['inventory'].'"></i></a>';
                                    } else
                                    {
                                        $supply2 = '<i class="order-supply-no" title="Inventory was not calculated for this order."></i>';
                                    }
					echo '
					<tr>
                                                <td>'.$res['TimeCreated'].'</td>
						<td><a target="_blank" href="/shipping_module/order_status_history.php?po='.$res['PONumber'].'">'.$res['PONumber'].'</a></td>
						<td>'.$res['RefNumber'].'</td>
						<td>'.$res['ShipAddress_Addr1'].'</td>
						<td>'.$res['ShipAddress_PostalCode'].'</td>
						<td>'.$res['WEIGHT'].'</td>
						<td>'.$res['COST'].'</td>
                                                <td>'.$res['ShipMethodRef_FullName'].'</td>
						<td>'.$res['CARRIERNAME'].'</td>
                                                <td class="status">'.$supply2.'</td>
						<td>
							<input class="orderCheckBox" type="checkbox" id="'.$res['RefNumber'].'" name="'.$res['PONumber'].'" value="'.$res['RefNumber'].'" data-po="'.$res['PONumber'].'" data-lift="'.$res['LIFTGATE'].'" data-resi="'.$res['RESIDENTIAL'].'" data-weight="'.$res['WEIGHT'].'" data-special="'.$res['special_order'].'">
						</td>
					</tr>';
				}
				?>
			</tbody>
		</table>
			<button type="button" class="btn btn-info calculateCost">Calculate cost</button>
			<button type="button" class="btn btn-info typeCost">Custom carrier</button>
			<!--<center><button type="submit" class="btn btn-success combineOrders">Combine</button></center>-->
			<br><br><br>
            </div>
        </div>
<?php
		$href++;
	}
} else echo "There are no orders for combining.";
?>
    </div>
</div>
</form>
</div>
<div id="dialog" title="Combine Orders">
			<div id="content"></div>
		</div>
<div id="dialog2" title="Loading...">
			<div id="content2"><img src='Include/gif/294.GIF' height='150px' width='150px'></div>
		</div>
<div id="dialog3" title="Cost">
			<div id="content3"></div>
		</div>
<div id="dialogError" title="Error">
			<div id="contentError"></div>
		</div>
<?php
}
?>
<?php require_once($path.'/Include/footer_section.php');?>
