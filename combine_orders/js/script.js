var cur_resi = '';
var cur_lift = '';
var cur_weight = '';
var pathes = new Array();

var completed = false;
var dialog = false;
var dialog2 = false;
var dialog3 = false;
var dialogError = false;
var comma_separated_po = '';

$(document).ready(function() {
	completed = false;
	console.log('Combine Orders');
        
        dialog3 = $("#dialog3").dialog({
	autoOpen: false,
        closeOnEscape: false,
        height: 'auto',
        width: 'auto',
        resizable: false,
        autoResize: true,
        modal: true,
        overlay: { backgroundColor: "#000", opacity: 0.5 },
        open: function(event, ui) {
                $(".ui-dialog-titlebar-close", ui.dialog | ui).hide();
            },
	});
	
	dialog = $('#dialog').dialog({
            autoOpen: false,
            closeOnEscape: false,
            height: 'auto',
            width: 'auto',
            resizable: false,
            autoResize: true,
            modal: true,
            overlay: { backgroundColor: "#000", opacity: 0.5 },
            open: function(event, ui) {
                $(".ui-dialog-titlebar-close", ui.dialog | ui).hide();
            },
        });
	
	dialog2 = $("#dialog2").dialog({
            autoOpen: false,
            closeOnEscape: false,
            height: 'auto',
            width: 'auto',
            resizable: false,
            autoResize: true,
            modal: true,
            overlay: { backgroundColor: "#000", opacity: 0.5 },
            open: function(event, ui) {
                $(".ui-dialog-titlebar-close", ui.dialog | ui).hide();
            },
	});
        
        dialogError = $("#dialogError").dialog({
        autoOpen: false,
        closeOnEscape: false,
        height: 'auto',
        width: 'auto',
        resizable: false,
        autoResize: true,
        modal: true,
        overlay: { backgroundColor: "#000", opacity: 0.5 },
	open: function(event, ui) {
                $(".ui-dialog-titlebar-close", ui.dialog | ui).hide();
            },
	});
	
	dialog.dialog('close');
	dialog2.dialog('close');
	dialog3.dialog('close');
        dialogError.dialog('close');
	
	$('#dialog').on('dialogclose', function(event) {
		 if (completed) refreshThepage();
	 });
	
	window.submit_combine_call = function submit_combine(carrier, quote, time, cost) {
            if($("#need_to_lock_combine").length != 0) {
                $("#need_to_lock_combine").attr("disabled", true);
            }
		//event.preventDefault();
		console.log(carrier);
		console.log(quote);
		console.log(time);
		console.log(cost);
		var form = $('#orderForm');
		var url = $(form).attr("action");
		var formData = {};
		formData = $(form).serialize();
		formDataArray = $(form).serializeArray();
		
		
		$('.orderCheckBox:checked').each(function() {
		   comma_separated_po+=$(this).data('po')+'_';
		});
		comma_separated_po = comma_separated_po.substring(0, comma_separated_po.length - 1);
		
		if(formDataArray.length >= 3)
		{
			/*$('#content').html("<p>Please enter PO</p><input type='text' id='po' name='po' value='"+comma_separated_po+"'> PO<br><br><button id='closeButton'>OK</button>");
			*/
			/*$("#ship_method").autocomplete({
				source: "phpFunctions/autocomplete/carrier.php",
				minLength: 1
			});*/
			
			/*dialog.dialog('open');
			
			$('#closeButton').on('click', function(){*/
				if (/*$('#po').val()*/comma_separated_po)
				{
					var po = /*encodeURIComponent($('#po').val())*/comma_separated_po;
					formData = formData+'&carrier='+carrier+'&quote='+quote+'&time='+time+'&cost='+cost+'&po='+po+'&resi='+cur_resi+'&lift='+cur_lift+'&weight='+cur_weight;
					console.log('serialized data: '+formData);
					dialog.dialog('close');
					dialog3.dialog('close');
					
					dialog2.dialog('open');
					$.ajax({
						url: url,
						type: 'POST',
						data: formData,
						complete: function(data){
							console.log(data.responseText);
							responseText = data.responseText;
								response = JSON.parse(responseText);
								if( Object.prototype.toString.call(response) === "[object Object]" )
								{
									var origin_po = response.po;
									var origin_id = response.id;
									var origin_pro = response.pro;
									dialog2.dialog('close');
									
									getProInvoiceSuborders(origin_po, origin_id, origin_pro, pathes, carrier);
									
								} else{
									dialog2.dialog('close');
									alert(data.responseText);
								}
							
						},
						error: function (request, status, error) {
							console.log("Ajax request error: "+request.responseText);
						}
					});
				} else alert('Can not generate combined PO.');
			/*});*/
		} else alert('Must be chosen at least two orders');
	}
	
	$('.calculateCost').on('click', function (e) {
		cur_resi = '';
		cur_lift = '';
		cur_weight = '';
		var form = $('#orderForm');
		var url = "/shipsingle/calculate_cost.php";
		var formData = {};
		formData = $(form).serialize();
		formDataArray = $(form).serializeArray();
		
		var lift_new = '0';
		var resi_new = '0';
                var special_order_flag = true;
                
                $('input:checked').each(function() {
		   if($(this).data('special') == 'commercial') 
                   {
                        special_order_flag = false;
                   }
		});
                
                
                if(special_order_flag)
                {
                    $('input:checked').each(function() {
                        if($(this).data('lift') == '1') lift_new = '1';
                        if($(this).data('resi') == '1' || $(this).data('special_order') == 'residential') resi_new = '1';
                    });
                }
		
		if(formDataArray.length >= 3)
		{
		/*$('#content3').html('<div class="form-group"><label for="class">Item Class</label><select id="class" name="class" class="form-control"><option value="85">85</option><option value="100">100</option><option value="125">125</option><option value="250">250</option><option value="77">77</option><option value="70">70</option><option value="60">60</option><option value="55">55</option></select><span class="help-block with-errors">Choose class from the list</span></div><div class="form-group"><label class="checkbox-inline"><input type="checkbox" id="lift" name="lift" value="1" '+$lift_new+'>Liftgate</label><span class="help-block with-errors">Check if Liftgate</span></div><div class="form-group"><label class="checkbox-inline"><input type="checkbox" name="resi" id="resi" value="1" '+$resi_new+'>Residential</label><span class="help-block with-errors">Check if Residential</span></div><br><br><button id="calculateOk">OK</button>');
		dialog3.dialog('open');
		
		$('#calculateOk').on('click', function(){
		dialog3.dialog('close');*/
		var s_class = encodeURIComponent(/*$('#class').val()*/'85');
		/*if($('#lift').is(':checked')) var lift = '1'; else lift = '0';
		if($('#resi').is(':checked')) var resi = '1'; else resi = '0';*/
		formData = formData+'&class='+s_class+'&lift='+lift_new+'&resi='+resi_new;
		cur_resi = resi_new;
		cur_lift = lift_new;
		
				dialog2.dialog('open');
				$.ajax({
					url: url,
					type: 'POST',
					data: formData,
					complete: function(data){
						console.log(data.responseText);
						dialog2.dialog('close');
						var final_result = '';
						try{
							result_array = JSON.parse(data.responseText);
							if( Object.prototype.toString.call(result_array) === "[object Object]" ) {
								console.log('weight: '+result_array.weight);
								cur_weight = result_array.weight;
								final_result = make_report(result_array);
							} else
							{
								final_result = data.responseText;
							}
						}catch(e){
							final_result = data.responseText;
						}
						
						$('#content3').html(final_result);
						dialog3.dialog('open');
					},
					error: function (request, status, error) {
						console.log("Ajax request error: "+request.responseText);
					}
				});
		/*});*/
		} else alert('Must be chosen at least two orders');
	});
	
	$('.typeCost').on('click', function (e) {
		cur_resi = '';
		cur_lift = '';
		cur_weight = '';
		var form = $('#orderForm');
		var url = "/shipsingle/calculate_cost.php";
		var formData = {};
		formData = $(form).serialize();
		formDataArray = $(form).serializeArray();
		
		var lift_new = '0';
		var resi_new = '0';
                var special_order_flag = true;
                
                $('input:checked').each(function() {
		   if($(this).data('special') == 'commercial') 
                   {
                        console.log('commercial');
                        special_order_flag = false;
                   }
		});
                
                
                if(special_order_flag)
                {
                    console.log('residential');
                    $('input:checked').each(function() {
                        if($(this).data('lift') == '1') lift_new = '1';
                        if($(this).data('resi') == '1' || $(this).data('special_order') == 'residential') resi_new = '1';
                    });
                }
		
		if(formDataArray.length >= 3)
		{
		$('#content3').html('<div class="form-group"><label for="Carrier">Carrier</label><input type="text" class="form-control" id="Carrier" name="Carrier" placeholder="UPS" required></div><br><br><button type="button" class="btn btn-success" style="float:left" id="calculateOkCustom">OK</button><button onclick="window.cancel_combine_call()" type="button" class="btn btn-warning" style="float:right">Cancel</button>');
		$("#Carrier").autocomplete({
					source: "phpFunctions/autocomplete/carrier.php",
					minLength: 1
				});
		dialog3.dialog('open');
		
		$('#calculateOkCustom').on('click', function(){
                    $('#calculateOkCustom').attr("disabled", true);
			var s_class = encodeURIComponent(/*$('#class').val()*/'85');
			if($('#Carrier').val()) var carr = $('#Carrier').val(); else var carr = false;
			formData = formData+'&class='+s_class+'&lift='+lift_new+'&resi='+resi_new;
			
			cur_weight = 0;
			$('input:checked').each(function() {
				temp_weight = $(this).data('weight') - 70;
				cur_weight+= temp_weight;
			 });
			 
			 cur_weight+= 70;
			 console.log("full weight: "+cur_weight);
			
			cur_resi = resi_new;
			cur_lift = lift_new;
			if(carr) 
			{
				dialog3.dialog('close');
				submit_combine_call(carr, 'N/A', 'N/A', '0');
			}else 
                        {
                            alert('Type carrier name please');
                            $('#calculateOkCustom').attr("disabled", false);
                        }
		});
		} else alert('Must be chosen at least two orders');
	});
});

function cancel_combine_call(){
    dialog3.dialog('close');
}

function close_dialog(){
    dialog.dialog('close');
}

function IsJsonString(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}

function proBolBillDialog(origin_po, origin_id, origin_pro, carrier, pathes, combined_invoices)
{
    console.log('pro, invoices:');
    console.log(origin_pro);
    console.log(combined_invoices);
        if(origin_pro && combined_invoices)
        {
            generateBillPopupHandler(origin_po, origin_id, origin_pro, combined_invoices, carrier, pathes, combined_invoices);
        } else
        {
            var en_dis = '';
            if(origin_pro) en_dis = 'disabled';
            $('#content').html("<p>Please enter PRO# and INVOICE# for combined order</p><input type='text' id='pro_number' name='pro_number' value='"+origin_pro+"' "+en_dis+"> PRO#<br><br><input type='text' id='invoice_number' name='invoice_number' value='"+combined_invoices+"'> INVOICE#<br><br><button id='generateBill'>OK</button>");
            $('#generateBill').on('click', function (e) {

                    origin_pro = $('#pro_number').val();
                    var origin_bol = $('#invoice_number').val();
                    generateBillPopupHandler(origin_po, origin_id, origin_pro, origin_bol, carrier, pathes, combined_invoices);
            });
            dialog.dialog('open');
        }
}

function generateBillPopupHandler(origin_po, origin_id, origin_pro, origin_bol, carrier, pathes, combined_invoices)
{
    if(origin_pro && origin_bol)
                {
                        dialog.dialog('close');
                        dialog2.dialog('open');
                        $.ajax({
                                url: '/shipping_module/phpFunctions/functionsSaveProBol.php',
                                type: 'POST',
                                data: {
                                        value: origin_id,
                                        pro: origin_pro,
                                        bol: origin_bol,
                                        carrier: carrier
                                },
                                complete: function(data){
                                        console.log(data.responseText);
                                        dialog2.dialog('close');
                                        console.log(carrier);
                                        pdfBill = generatePDF(origin_po, origin_id, origin_pro, origin_bol, "/shipping_module/phpFunctions/functionsBill.php", carrier, pathes);
                                },
                                error: function (request, status, error) {
                                        dialog2.dialog('close');
                                        console.log("Ajax request error: "+request.responseText);
                                }
                        });
                } else alert('Please fill all fields.');
}

function generatePDF(origin_po, origin_id, origin_pro, origin_bol, url, carrier, pathes) {
        //pathes = [];
        dialog2.dialog('open');
        $.ajax({
                url: url,
                type: 'POST',
                data: 
                {
                        value: origin_po,
                        id: origin_id,
                        pro: origin_pro, 
                        bol: origin_bol
                },
                complete: function(data){
                        console.log(data.responseText);
                        responseText = data.responseText;
                    if(IsJsonString(responseText))
                    {
                        response = JSON.parse(responseText);
                        if( Object.prototype.toString.call(response) === "[object Object]" ) {
                                var path = response.path;
                                if (path != 'false' && !response.er && !response.DataEr)
                                {
                                        var tempBol = {
                                            'po' : origin_po,
                                            'path' : path
                                        };
                                        pathes.push(tempBol);
                                        dialog2.dialog('close');
                                        //getProInvoiceSuborders(origin_po, origin_id, pathes, carrier);
                                        console.log(carrier);
                                        generateSuccessButtons(pathes);
                                }else
                                {
                                    if(confirm('Error generating documents. Retry? '+response.er+' '+response.DataEr))
                                    {
                                        generatePDF(origin_po, origin_id, origin_pro, origin_bol, url, carrier, pathes);
                                    } else
                                    {
                                        rollback('Error. Can not generate documents.');
                                    }
                                }
                        } else
                        {
                                dialog2.dialog('close');
                                if(confirm('Error generating documents. Retry? '+responseText))
                                {
                                    generatePDF(origin_po, origin_id, origin_pro, origin_bol, url, carrier, pathes);
                                } else
                                {
                                    rollback('Error. Can not generate documents.');
                                }
                        }
                    } else
                    {
                            dialog2.dialog('close');
                            if(confirm('Error generating documents. Retry? '+responseText))
                            {
                                generatePDF(origin_po, origin_id, origin_pro, origin_bol, url, carrier, pathes);
                            } else
                            {
                                rollback('Error. Can not generate documents.');
                            }
                    }
                },
                error: function (request, status, error) {
                        dialog2.dialog('close');
                        rollback("Ajax request error: "+request.responseText);
                }
        });
}

function generateSuccessButtons(pathes)
{
    //console.log(pathes);
        var temp_content = '';
        for (i=0;i<pathes.length;i++)
        {
                time = generate_unique_string();
                if (pathes[i]['path'].includes("Bill") && pathes[i]['path'] != 'false')
                {
                    temp_content+="<button class='resultButton' onclick="+'"'+"window.open('/shipping_module/newTab.php?value="+pathes[i]['path']+"&time="+time+"')"+'"'+">BOL (PO: "+pathes[i]['po']+")</button><br><br>";
                } else if (pathes[i]['path'].includes("flyer") && pathes[i]['path'] != 'false')
                {
                    temp_content+="<button class='resultButton' onclick="+'"'+"window.open('/shipping_module/newTab.php?value="+pathes[i]['path']+"&time="+time+"')"+'"'+">Flyer (PO: "+pathes[i]['po']+")</button><br><br>";
                } else if(pathes[i]['path'] != 'false')
                {
                    temp_content+="<button class='resultButton' onclick="+'"'+"window.open('/shipping_module/newTab.php?value="+pathes[i]['path']+"&time="+time+"')"+'"'+">Label (PO: "+pathes[i]['po']+")</button><br><br>";
                }
        }
        
        temp_content+="<button onclick='window.close_dialog()' type='button' class='btn btn-success' style='float:left'>OK</button>";
        
        completed = true;
        $('#content').html(temp_content);
        $('.resultButton').click(function(){
            $(this).css({"background-color":"#b3ffb3"});
        });
        dialog.dialog('open');
}

function doSmth() {
    console.log($('.copy_icon'));
    var clip = new Clipboard('.copy_icon');
    console.log(clip);
}

function proBolLabelDialog(origin_po, origin_id, origin_pro, array, pathes, carrier)
{
    console.log('PRO: '+origin_pro);
    if(!origin_pro)
    {
        alert('Can not generate PRO# for this carrier.');
        rollback('Can not generate PRO# for this carrier.');
    }
        pathes = [];

        var allBolFlag = true;
        for(i=0; i<array.length; i++)
        {
                if(!array[i].BOL || array[i].BOL == 'NULL') allBolFlag = false;
        }

        var proInvoiceLabel = new Array();

        var combined_invoices = '';

        if(true/*!allBolFlag*/)
        {
            var temp_content = '<p>Please enter INVOICE# for suborders</p>';
            //console.log(array);
            for(i=0; i<array.length; i++)
            {
                    if(!array[i].PRO || array[i].PRO == 'NULL') array[i].PRO = '';
                    if(!array[i].BOL || array[i].BOL == 'NULL') array[i].BOL = '';
                    temp_content+= "<p>PO: "+array[i].PONumber+"</p><!--<input type='text' id='"+array[i].RefNumber+"PRO' name='"+array[i].RefNumber+"PRO' value='"+array[i].PRO+"'> PRO#<br><br>--><input type='text' id='"+array[i].RefNumber+"BOL' name='"+array[i].RefNumber+"BOL' value='"+array[i].BOL+"'> INVOICE#<br><br><br>";
            }
            temp_content+="<button id='generateLabel'>OK</button><button id='CancelAllAndRollback' style='float: right;'>Cancel combination</button>";
            temp_content+='<br><br><p>Carrier name: '+carrier+' <img data-clipboard-text=\''+carrier+'\' class="copy_icon" id="copy1" src="Include/pictures/copy.png" onclick="clipboard.copy(\'1234567890\')" title="Copy PRO# to clipboard."></p><p>PRO#: '+origin_pro+' <img data-clipboard-text="'+origin_pro+'" class="copy_icon" src="Include/pictures/copy.png" title="Copy PRO# to clipboard."></p>';
            $('#content').html(temp_content);
            $('.copy_icon').click(function(e){
                e.stopPropagation();
                var text = $(this).attr('data-clipboard-text');
                $(this).attr( 'src', 'Include/pictures/copy_visited.png' );
                console.log( text );
                clipboard.copy( text );
            });

            $('#generateLabel').on('click', function (e) {
                var success_flag = true;
                    for(i=0; i<array.length; i++)
                    {
                            //label_pro = $('#'+array[i].RefNumber+'PRO').val();
                            var label_bol = $('#'+array[i].RefNumber+'BOL').val();
                            if (/*label_pro && */label_bol) 
                            {
                                    proInvoiceLabelTemp = new Object();
                                    proInvoiceLabelTemp.id = array[i].RefNumber;
                                    proInvoiceLabelTemp.po = array[i].PONumber;
                                    //proInvoiceLabelTemp.pro = label_pro;
                                    proInvoiceLabelTemp.bol = label_bol;

                                    proInvoiceLabel.push(proInvoiceLabelTemp);
                                    combined_invoices+=label_bol;
                                    if (i != array.length - 1) combined_invoices+=',';
                            } else success_flag = false;
                    }
                    proBolLabelDialogHandler(origin_po, origin_id, origin_pro, array, pathes, carrier, success_flag, proInvoiceLabel, combined_invoices);
            });
            
            $('#CancelAllAndRollback').on('click', function (e) {
                if(confirm('Are you sure to cancel orders combination?'))
                {
                    rollback('User cancel');
                }
            });
            
            dialog.dialog('open');
        } else
        {
            for(i=0; i<array.length; i++)
                    {

                        proInvoiceLabelTemp = new Object();
                        proInvoiceLabelTemp.id = array[i].RefNumber;
                        proInvoiceLabelTemp.po = array[i].PONumber;
                        proInvoiceLabelTemp.bol = array[i].BOL;

                        proInvoiceLabel.push(proInvoiceLabelTemp);
                        combined_invoices+=array[i].BOL;
                        if (i != array.length - 1) combined_invoices+=',';
                        success_flag = true;
                    }
            proBolLabelDialogHandler(origin_po, origin_id, origin_pro, array, pathes, carrier, success_flag, proInvoiceLabel, combined_invoices);
        }
}

function proBolLabelDialogHandler(origin_po, origin_id, origin_pro, array, pathes, carrier, success_flag, proInvoiceLabel, combined_invoices)
{

                if(success_flag)
                {
                        dialog.dialog('close');
                        dialog2.dialog('open');

                        //console.log(proInvoiceLabel);
                        console.log(combined_invoices);

                        proInvoiceLabelString = JSON.stringify(proInvoiceLabel);
                        carrier_json = JSON.stringify(carrier);
                        var origin_pro_temp = JSON.stringify(origin_pro);

                        console.log(proInvoiceLabelString);
                        console.log(carrier_json);

                        $.ajax({
                                url: '/combine_orders/generateLabels.php',
                                type: 'POST',
                                data: {
                                        value: proInvoiceLabelString,
                                        carrier: carrier_json,
                                        pro: origin_pro_temp
                                },
                                complete: function(data){
                                    console.log(data.responseText);
                                    responseText = data.responseText;
                                    if(IsJsonString(responseText))
                                    {
                                        response = JSON.parse(responseText);
                                        if( Object.prototype.toString.call(response) === "[object Array]" ) {
                                                var success = true;
                                                for (i=0;i<response.length;i++)
                                                {
                                                    response_next = JSON.parse(response[i]);

                                                        if (response_next.path == 'false') 
                                                        {
                                                                success = false;
                                                                display_error_html(response_next.er, response_next.DataEr);
                                                        }
                                                }
                                                if (success) 
                                                {
                                                        for (i=0;i<response.length;i++)
                                                        {
                                                                response[i] = JSON.parse(response[i]);
                                                                var tempLabel = {
                                                                  'po' : response[i].po,
                                                                  'path' : response[i].path
                                                                };
                                                                var tempFlyer = {
                                                                  'po' : response[i].po,
                                                                  'path' : response[i].pathFlyer
                                                                };
                                                                pathes.push(tempLabel);
                                                                pathes.push(tempFlyer);
                                                        }
                                                        //console.log(pathes);
                                                        dialog2.dialog('close');
                                                        console.log(carrier);
                                                        proBolBillDialog(origin_po, origin_id, origin_pro, carrier, pathes, combined_invoices);
                                                        //generateSuccessButtons(pathes);
                                                } else 
                                                {
                                                    if(confirm('Error generating documents. Retry? '+responseText))
                                                    {
                                                        proBolLabelDialogHandler(origin_po, origin_id, origin_pro, array, pathes, carrier, success_flag, proInvoiceLabel, combined_invoices);
                                                    } else
                                                    {
                                                        rollback('Error. Can not generate documents.');
                                                    }
                                                }
                                        } else
                                        {
                                                dialog2.dialog('close');
                                                if(confirm('Error generating documents. Retry? '+responseText))
                                                {
                                                    proBolLabelDialogHandler(origin_po, origin_id, origin_pro, array, pathes, carrier, success_flag, proInvoiceLabel, combined_invoices);
                                                } else
                                                {
                                                    rollback('Error. Can not generate documents.');
                                                }
                                        }
                                    } else
                                    {
                                            dialog2.dialog('close');
                                            if(confirm('Error generating documents. Retry? '+responseText))
                                            {
                                                proBolLabelDialogHandler(origin_po, origin_id, origin_pro, array, pathes, carrier, success_flag, proInvoiceLabel, combined_invoices);
                                            } else
                                            {
                                                rollback('Error. Can not generate documents.');
                                            }
                                    }
                                },
                                error: function (request, status, error) {
                                        dialog2.dialog('close');
                                        console.log("Ajax request error: "+request.responseText);
                                        rollback("Ajax request error: "+request.responseText);
                                }
                        });
                } else alert('Please fill all fields.');
}

function getProInvoiceSuborders(origin_po, origin_id, origin_pro, pathes, carrier)
{
        dialog2.dialog('open');
        $.ajax({
                        url: '/shipping_module/phpFunctions/getCombinedProInvoice.php',
                        type: 'POST',
                        data: 
                        {
                                id: origin_id
                        },
                        complete: function(data){
                                console.log(data.responseText);
                                responseText = data.responseText;
                                response = JSON.parse(responseText);
                                if( Object.prototype.toString.call(response) === "[object Array]" ) {
                                        dialog2.dialog('close');
                                        console.log(carrier);
                                        proBolLabelDialog(origin_po, origin_id, origin_pro, response, pathes, carrier);
                                } else
                                {
                                        alert(responseText);
                                        dialog2.dialog('close');
                                }
                        },
                        error: function (request, status, error) {
                                dialog2.dialog('close');
                                console.log("Ajax request error: "+request.responseText);
                        }
                });
}

function display_error_html(er, DataEr)
{
    $('#contentError').html("<p>Error occurred generating the document.</p><p>"+er+"</p><p>"+DataEr+"</p><br><button id='error_html_ok'>Cancel combination</button><br>");
    dialogError.dialog('open');
    $('#error_html_ok').on('click', function (e) {
            rollback('Error occurred generating the document.'+er);
        });
}


function generate_unique_string()
{
	var timestamp = new Date().getUTCMilliseconds();
	return timestamp;
}

function make_report(result_array)
{
	console.log('OLD: '+result_array.total_old_cost);
	console.log('NEW: '+result_array.cost);
	if(result_array.cost < result_array.total_old_cost)
	{
		var diff = result_array.total_old_cost - result_array.cost;
		var advice = '<strong style="color: green;">Benefit: '+diff.toFixed(2)+'</strong>';
	} else if(result_array.cost > result_array.total_old_cost){
		var diff = result_array.cost - result_array.total_old_cost;
		var advice = '<strong style="color: red;">Loss: '+diff.toFixed(2)+'</strong>';
	} else 
	{
		var advice = '<strong style="color: brown;">Cost is equal</strong>';
	}
	var html = "<h3>Best result</h3><table class='table table-bordered'><tr><th>LOGO</th><th>CARRIER</th><th>QUOTE #</th><th>TRANSIT TIME</th><th>COST</th><th>DATE</th><th>DIFFERENCE</th></tr><tr class='info'><td><img src='/shipsingle/"+result_array.logo+"' height='40px'></td><td>"+result_array.carrier+"</td><td>"+result_array.quote+"</td><td>"+result_array.time+"</td><td>"+result_array.cost+"</td><td>"+result_array.date+"</td><td>"+advice+"</td></tr></table><button id='need_to_lock_combine' onclick='window.submit_combine_call("+'"'+result_array.carrier+'"'+", "+'"'+result_array.quote+'"'+", "+'"'+result_array.time+'"'+", "+'"'+result_array.cost+'"'+");' style='float:left' type='button' class='btn btn-success'>Combine</button><button onclick='window.cancel_combine_call()' type='button' class='btn btn-warning' style='float:right'>Cancel</button>";
	return html;
}

function runShipsingle()
{
	console.log('shipsingle started.');
	$.ajax({
		url: '/shipsingle/shipsingle.php',
		type: 'POST',
		complete: function(data){
			console.log(data.responseText);
			console.log('shipsingle finished.');
		},
		error: function (request, status, error) {
			console.log("Ajax request error: "+request.responseText);
		}
	});
}

function rollback(message)
{
    console.log('rollback started.');
    document.body.innerHTML = '';
    document.body.innerHTML += '<div>Error. Rollback procedure started.</div>';
	$.ajax({
		url: '/shipping_module/phpFunctions/uncombineOrder.php',
		type: 'POST',
                data: 
                {
                    po: comma_separated_po,
                    reason: 'Order combination was not completed',
                    reasonNote: 'Please uninvoice orders to combine again, if not on combine list',
                    userName: 'Logistics Hub'
                },
		complete: function(data){
			console.log(data.responseText);
			console.log('rollback finished.');
                        alert('Orders where uncombined because of error: '+message);
                        refreshThepage();
		},
		error: function (request, status, error) {
			console.log("Ajax request error: "+request.responseText);
		}
	});
}

function refreshThepage()
{
    document.body.innerHTML = '';
    document.body.innerHTML += '<div>Page is going to be updated. Please wait.</div>';
    window.location.href = 'index.php';
}

function clearCheckboxes()
{
	$('input:checkbox').removeAttr('checked');
}