<?php
$path = $_SERVER['DOCUMENT_ROOT'];
require_once($path.'/db_connect/connect.php');
require_once($path.'/shipsingle/functions.php');

function display_message($value)
{
	print_r('<pre>');
	print_r($value);
	print_r('</pre>');
}

function get_uid()
{
	$uid = uniqid('combine_', true);
	return $uid;
}

function addquote($str)
{
	$str = str_replace("'", "`", $str);
	return $str;
}

function generateInsertQueries($array, $shiptrackData)
{
	$queries = array();
        
        foreach($shiptrackData as $shiptrack)
	{
		$queries[]="EXEC [dbo].[backup_order] @PO = '".addquote($shiptrack['PONumber'])."'";
	}
        
	$queries[]= "INSERT INTO [dbo].[".MANUALLY_SHIPTRACK_TABLE."]
           ([TxnID]
           ,[TimeCreated]
           ,[TimeModified]
           ,[CustomerRef_FullName]
           ,[RefNumber]
           ,[ShipAddress_Addr1]
           ,[ShipAddress_Addr2]
           ,[ShipAddress_Addr3]
           ,[ShipAddress_City]
           ,[ShipAddress_State]
           ,[ShipAddress_PostalCode]
           ,[ShipAddress_Country]
           ,[PONumber]
           ,[FOB]
		   ,[ShipMethodRef_FullName]
           ,[IsManuallyClosed]
           ,[IsFullyInvoiced]
		   ,[CustomField10])
     VALUES
           ('".addquote($array['TxnID'])."'
           ,GETDATE()
           ,GETDATE()
		   ,'".addquote($array['CustomerRef_FullName'])."'
		   ,'".addquote($array['RefNumber'])."'
		   ,'".addquote($array['ShipAddress_Addr1'])."'
		   ,'".addquote($array['ShipAddress_Addr2'])."'
		   ,'".addquote($array['ShipAddress_Addr3'])."'
		   ,'".addquote($array['ShipAddress_City'])."'
		   ,'".addquote($array['ShipAddress_State'])."'
		   ,'".addquote($array['ShipAddress_PostalCode'])."'
		   ,'".addquote($array['ShipAddress_Country'])."'
		   ,'".addquote($array['PONumber'])."'
		   ,'".addquote($array['FOB'])."'
		   ,'".addquote($array['ShipMethodRef_FullName'])."'
		   ,0
		   ,0
		   ,'".addquote($array['CustomField10'])."'
          )";
		  
	foreach($array['products'] as $product)
	{
		$queries[]= "INSERT INTO [dbo].[".MANUALLY_GROUPDETAIL_TABLE."]
			   ([IDKEY]
			   ,[TxnLineID]
			   ,[ItemGroupRef_FullName]
			   ,[Prod_desc]
			   ,[Quantity])
		 VALUES
			   ('".addquote($product['IDKEY'])."'
			   ,'".addquote($product['TxnLineID'])."'
			   ,'".addquote($product['ItemGroupRef_FullName'])."'
			   ,'".addquote($product['Prod_desc'])."'
			   ,'".addquote($product['Quantity'])."')";
	}
	
	foreach($array['products'] as $product)
	{
		foreach($product['items'] as $item)
		{
			$queries[]="INSERT INTO [dbo].[".MANUALLY_LINEDETAIL_TABLE."]
				   ([IDKEY]
				   ,[TxnLineID]
				   ,[CustomField8]
				   ,[CustomField9]
				   ,[ItemRef_FullName]
				   ,[Rate]
				   ,[quantity]
				   ,[GroupIDKEY]
				   ,[original_po])
			 VALUES
				   ('".addquote($item['IDKEY'])."'
				   ,'".addquote($item['TxnLineID'])."'
				   ,'".addquote($item['CustomField8'])."'
				   ,'".addquote($item['CustomField9'])."'
				   ,'".addquote($item['ItemRef_FullName'])."'
				   ,'".addquote($item['Rate'])."'
				   ,'".addquote($item['quantity'])."'
				   ,'".addquote($item['GroupIDKEY'])."'
				   ,'".addquote($item['original_po'])."')";
		}
	}
	foreach($shiptrackData as $shiptrack)
	{
		$queries[]="INSERT INTO [dbo].[".ORDERS_COMBINED."]
			   ([POnumber]
			   ,[RefNumber]
			   ,[Combined_into_POnumber]
			   ,[Combined_into_RefNumber]
			   ,[CombineDate]
			   ,[userName])
		 VALUES
			   ('".$shiptrack['PONumber']."'
			   ,'".$shiptrack['RefNumber']."'
			   ,'".$array['PONumber']."'
			   ,'".$array['RefNumber']."'
			   ,GETDATE()
			   ,'".$array['CustomField10']."')";
	}
	
	return $queries;
}

function chooseCustormer($shiptrackData, $ShipAddress_Addr1)
{
    
    $lastCustomer = $shiptrackData[0]['CustomerRef_FullName'];
    $isCustomerSame = true;
    $customersList = ['Lowe', 'Menards'];
    $isCustomerInList = false;
    
    foreach($shiptrackData as $data)
    {
        if($data['CustomerRef_FullName'] != $lastCustomer) $isCustomerSame = false;
        $isCustomerSame = $data['CustomerRef_FullName'];
    }

    if($isCustomerSame)
    {
        foreach($customersList as $customer)
        {
            if (strpos($lastCustomer, $customer) !== false) $isCustomerInList = true;
        }
        
    }
    if ($isCustomerSame && $isCustomerInList)
    {
        /*print_r('Customer: ');
        print_r($lastCustomer);
        die();*/
        return $lastCustomer;
    } else 
    {
        /*print_r('Customer: ');
        print_r($ShipAddress_Addr1);
        die();*/
        return $ShipAddress_Addr1;
    }
}

function prepare_array($shiptrackData, $groupdetailData, $linedetailData, $user, $ship_method, $po)
{
	$array = array();
	$array['TxnID'] = get_uid();
	$array['CustomerRef_FullName'] = $shiptrackData[0]['CustomerRef_FullName'];
	$array['RefNumber'] = get_uid();
	$array['ShipAddress_Addr1'] = chooseCustormer($shiptrackData, $shiptrackData[0]['ShipAddress_Addr1']);
	$array['ShipAddress_Addr2'] = $shiptrackData[0]['ShipAddress_Addr2'];
	$array['ShipAddress_Addr3'] = $shiptrackData[0]['ShipAddress_Addr3'];
	$array['ShipAddress_City'] = $shiptrackData[0]['ShipAddress_City'];
	$array['ShipAddress_State'] = $shiptrackData[0]['ShipAddress_State'];
	$array['ShipAddress_PostalCode'] = $shiptrackData[0]['ShipAddress_PostalCode'];
	$array['ShipAddress_Country'] = $shiptrackData[0]['ShipAddress_Country'];
	$array['PONumber'] = $po;
	$array['FOB'] = $shiptrackData[0]['FOB'];
	$array['ShipMethodRef_FullName'] = $ship_method;
	$array['CustomField10'] = $user;
	foreach($groupdetailData as $keyG => $groupdetail)
	{
		$groupdetail['IDKEY'] = $array['TxnID'];
		$array['products'][get_uid()] = $groupdetail;
	}
	
	foreach($array['products'] as $keyP => $product)
	{
		foreach($linedetailData as $linedetail)
		{
			if($product['TxnLineID'] == $linedetail['GroupIDKEY'])
			{
				$linedetail['IDKEY'] = $product['IDKEY'];
				$array['products'][$keyP]['items'][get_uid()] = $linedetail;
			}
		}
	}
	
	foreach($array['products'] as $keyA => $arrA)
	{
		foreach($arrA['items'] as $keyI => $arrI)
		{
			$array['products'][$keyA]['items'][$keyI]['TxnLineID'] = $keyI;
			$array['products'][$keyA]['items'][$keyI]['GroupIDKEY'] = $keyA;
		}
		$array['products'][$keyA]['TxnLineID'] = $keyA;
	}
	return $array;
}

function generateSelectQueries($data)
{
	$queries = array();
	$queryShiptrack = "SELECT * FROM [dbo].[".SHIPTRACK_TABLE."]";
	$first1 = true;
	foreach($data as $ref)
	{
		if($first1 == true)
		{
			$queryShiptrack.=" WHERE [RefNumber] = '".$ref."'";
			$first1 = false;
		} else
		{
			$queryShiptrack.=" OR [RefNumber] = '".$ref."'";
		}
	}
	
	$queryGroupdetail = "SELECT [".GROUPDETAIL_TABLE."].*
		FROM [dbo].[".GROUPDETAIL_TABLE."]
		LEFT JOIN [".SHIPTRACK_TABLE."] ON [".GROUPDETAIL_TABLE."].[IDKEY] = [".SHIPTRACK_TABLE."].[TxnID]";
	$first2 = true;
	foreach($data as $ref)
	{
		if($first2 == true)
		{
			$queryGroupdetail.=" WHERE [".SHIPTRACK_TABLE."].[RefNumber] = '".$ref."'";
			$first2 = false;
		} else
		{
			$queryGroupdetail.=" OR [".SHIPTRACK_TABLE."].[RefNumber] = '".$ref."'";
		}
	}
	
	$queryLinedetail = "SELECT [".LINEDETAIL_TABLE."].*, [".SHIPTRACK_TABLE."].[PONumber] as [original_po]
		FROM [dbo].[".LINEDETAIL_TABLE."]
		LEFT JOIN [".GROUPDETAIL_TABLE."] ON [".GROUPDETAIL_TABLE."].[TxnLineID] = [".LINEDETAIL_TABLE."].[GroupIDKEY]
		LEFT JOIN [".SHIPTRACK_TABLE."] ON [".GROUPDETAIL_TABLE."].[IDKEY] = [".SHIPTRACK_TABLE."].[TxnID]
		";
	$first3 = true;
	// Get array keys
	$arrayKeys = array_keys($data);
	// Fetch last array key
	$lastArrayKey = array_pop($arrayKeys);
	foreach($data as $key => $ref)
	{
		if($first3 == true)
		{
			$queryLinedetail.=" WHERE ([".SHIPTRACK_TABLE."].[RefNumber] = '".$ref."'";
			$first3 = false;
		} else
		{
			$queryLinedetail.=" OR [".SHIPTRACK_TABLE."].[RefNumber] = '".$ref."'";
		}
		if($key == $lastArrayKey)
		{
			$queryLinedetail.=")";
		}
	}
	$queryLinedetail.=" and
						 (
						  [".LINEDETAIL_TABLE."].ItemRef_FullName not like '%Price-Adjustment%' 
						  and [".LINEDETAIL_TABLE."].ItemRef_FullName not like '%Freight%'
						  and [".LINEDETAIL_TABLE."].ItemRef_FullName not like '%warranty%' 
						  and [".LINEDETAIL_TABLE."].ItemRef_FullName is not NULL
						  and [".LINEDETAIL_TABLE."].ItemRef_FullName not like '%Subtotal%'
						  and [".LINEDETAIL_TABLE."].ItemRef_FullName not like '%IDSC-10%'
						  and [".LINEDETAIL_TABLE."].ItemRef_FullName not like '%DISCOUNT%'
						  and [".LINEDETAIL_TABLE."].ItemRef_FullName not like '%SPECIAL ORDER4%'
						  and [".LINEDETAIL_TABLE."].ItemRef_FullName not like '%Manual%'
						  )";
	$queries['shiptrack']=$queryShiptrack;
	$queries['groupdetail']=$queryGroupdetail;
	$queries['linedetail']=$queryLinedetail;
	return $queries;
}

function sort_results($results)
{
	$sorted_results = array();
	foreach ($results as $key1 => $value1)
	{
		foreach ($results as $key2 => $value2)
		{
			if ($value1['my_sum'] == $value2['my_sum'] 
				&& $key1 != $key2 
				&& !alreadySet($value2, $sorted_results, $value1['my_sum']))
			{
				$sorted_results[$value1['my_sum']][]= $value2;
			}
		}
	}
	return $sorted_results;
}

function alreadySet($value, $results, $key)
{
	$returnValue = false;
	if (isset($results[$key]) && !empty($results[$key]))
	{
		foreach($results[$key] as $result)
		{
			if($result == $value) $returnValue = true;
		}
	}
	return $returnValue;
}

function sort_date($array)
{
    usort($array, "sortFunctionDate");
    return $array;
}

function sortFunctionDate( $a, $b ) {
    return strtotime($a["TimeCreated"]) - strtotime($b["TimeCreated"]);
}

/*function getConnection()
{
	$conn = Database::getInstance()->dbc;
	return $conn;
}*/

function beginTransaction($conn)
{
	try
	{
		$result = $conn->beginTransaction();
		return $result;
	}catch (Exception $e) {
		die("<br>".$e."<br>");
		return false;
	}
}

function commitTransaction($conn)
{
	try
	{
		$result = $conn->commit();
		return $result;
	}catch (Exception $e) {
		die("<br>".$e."<br>");
		return false;
	}
}

function rollbackTransaction($conn)
{
	try
	{
		$result = $conn->rollback();
		return $result;
	}catch (Exception $e) {
		die("<br>".$e."<br>");
		return false;
	}
}

function runQuery2($conn, $query)
{
	try
	{
		//echo "<br>".$query."<br>";
		$stmt = $conn->prepare($query);
		$result = $stmt->execute();
		$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}catch (Exception $e) {
		die("<br>".$e."<br>");
		return false;
	}
}

function runQuery_empty_result($conn, $query)
{
	try
	{
		//echo "<br>".$query."<br>";
		$stmt = $conn->prepare($query);
		$result = $stmt->execute();
		return $result;
	}catch (Exception $e) {
		die("<br>".$e."<br>");
		return false;
	}
}
?>