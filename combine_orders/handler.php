<?php
ini_set('max_execution_time', 7200);
ini_set("memory_limit", "2000M");
require_once('functions.php');
$path = $_SERVER['DOCUMENT_ROOT'];
require_once($path.'/db_connect/connect.php');



/*print_r(count($_POST));*/

if(isset($_POST) && !empty($_POST) 
	&& count($_POST) > 10 
	&& isset($_POST['user']) && !empty($_POST['user']) 
	&& isset($_POST['carrier']) && !empty($_POST['carrier'])
	&& isset($_POST['quote']) && !empty($_POST['quote'])
	&& isset($_POST['time']) && !empty($_POST['time'])
	&& isset($_POST['cost']) && (!empty($_POST['cost']) || $_POST['cost'] == 0)
	&& isset($_POST['po']) && !empty($_POST['po'])
	&& isset($_POST['resi'])
	&& isset($_POST['lift'])
	&& isset($_POST['weight']) && !empty($_POST['weight']))
{
	/*print_r($_POST);
	die();*/
	
	$user = $_POST['user'];
	$ship_method = $_POST['carrier'];
	$po = $_POST['po'];
	$data = array();
	foreach($_POST as $key => $row)
	{
		if ($key != 'user' && $key != 'carrier' && $key != 'quote' && $key != 'time' && $key != 'cost' && $key != 'resi' && $key != 'lift' && $key != 'weight') $data[]= $row;
	}
	$queries = generateSelectQueries($data);
	
	$conn = Database::getInstance()->dbc;
	
	
	//read settings from DB
$settings = getSettings($conn);

$authorized_dealers = array();
$FreightClass = false;
$originZip = false;
$originStatecode = false;
$originCountry = false;
$originCity = false;
$pro_numbers_estes_min = false;
$pro_numbers_estes_max = false;
$pro_numbers_estes_fd = false;
$pro_numbers_fedex_min = false;
$pro_numbers_fedex_max = false;
$pro_numbers_rl_min = false;
$pro_numbers_rl_max = false;
$pro_numbers_ups_min = false;
$pro_numbers_ups_max = false;
$pro_numbers_nemf_min = false;
$pro_numbers_nemf_max = false;
$pro_numbers_yrc_min = false;
$pro_numbers_yrc_max = false;
$pro_numbers_amazonds_min = false;
$pro_numbers_amazonds_max = false;
$pro_numbers_yrc_before_hyphen = false;
$pro_numbers_odfl_min = false;
$pro_numbers_odfl_max = false;
$ziphead_not_allowed_RL = array();
$rl_pal_addon_cost_liftgate = 0;
$fedex_addon_cost_liftgate = 0;
$ORDER_PROCESSING_MODE_FEDEX = false;
$rl_pal_addon_cost_residential = 0;
$query_mode_dealer_setting = false;
$remote_orders = array();
$remote_orders_dealer_name = array();
$shipping_ground = array();
$PrintCarrierForAmazonMode = false;
$shipping_ground_weight_max = false;
$pro_numbers_pyle_min = false;
$pro_numbers_pyle_max = false;
$rate_ups = false;

foreach ($settings as $setting)
{
	if ($setting['group_name'] == 'authorized_dealers' && $setting['type_name'] == 'dealer_name')
	{
		$authorized_dealers[]=$setting['setting_value'];
	} else if ($setting['group_name'] == 'freight_setting' && $setting['type_name'] == 'FreightClass')
	{
		$FreightClass=$setting['setting_value'];
	} else if ($setting['group_name'] == 'UPS' && $setting['type_name'] == 'rate')
	{
		$rate_ups=$setting['setting_value'];
	} else if ($setting['group_name'] == 'origin_address' && $setting['type_name'] == 'zip')
	{
		$originZip=$setting['setting_value'];
	} else if ($setting['group_name'] == 'origin_address' && $setting['type_name'] == 'statecode')
	{
		$originStatecode=$setting['setting_value'];
	} else if ($setting['group_name'] == 'origin_address' && $setting['type_name'] == 'country')
	{
		$originCountry=$setting['setting_value'];
	} else if ($setting['group_name'] == 'origin_address' && $setting['type_name'] == 'city')
	{
		$originCity=$setting['setting_value'];
	} else if ($setting['group_name'] == 'pro_numbers_estes' && $setting['type_name'] == 'min')
	{
		$pro_numbers_estes_min=$setting['setting_value'];
	} else if ($setting['group_name'] == 'pro_numbers_estes' && $setting['type_name'] == 'max')
	{
		$pro_numbers_estes_max=$setting['setting_value'];
	} else if ($setting['group_name'] == 'pro_numbers_estes' && $setting['type_name'] == 'first digit')
	{
		$pro_numbers_estes_fd=$setting['setting_value'];
	} else if ($setting['group_name'] == 'pro_numbers_fedex' && $setting['type_name'] == 'min')
	{
		$pro_numbers_fedex_min=$setting['setting_value'];
	} else if ($setting['group_name'] == 'pro_numbers_fedex' && $setting['type_name'] == 'max')
	{
		$pro_numbers_fedex_max=$setting['setting_value'];
	} else if ($setting['group_name'] == 'pro_numbers_rl' && $setting['type_name'] == 'min')
	{
		$pro_numbers_rl_min=$setting['setting_value'];
	} else if ($setting['group_name'] == 'pro_numbers_rl' && $setting['type_name'] == 'max')
	{
		$pro_numbers_rl_max=$setting['setting_value'];
	} else if ($setting['group_name'] == 'pro_numbers_ups' && $setting['type_name'] == 'min')
	{
		$pro_numbers_ups_min=$setting['setting_value'];
	} else if ($setting['group_name'] == 'pro_numbers_ups' && $setting['type_name'] == 'max')
	{
		$pro_numbers_ups_max=$setting['setting_value'];
	} else if ($setting['group_name'] == 'pro_numbers_nemf' && $setting['type_name'] == 'min')
	{
		$pro_numbers_nemf_min=$setting['setting_value'];
	} else if ($setting['group_name'] == 'pro_numbers_nemf' && $setting['type_name'] == 'max')
	{
		$pro_numbers_nemf_max=$setting['setting_value'];
	} else if ($setting['group_name'] == 'pro_numbers_yrc' && $setting['type_name'] == 'min')
	{
		$pro_numbers_yrc_min=$setting['setting_value'];
	} else if ($setting['group_name'] == 'pro_numbers_yrc' && $setting['type_name'] == 'max')
	{
		$pro_numbers_yrc_max=$setting['setting_value'];
	} else if ($setting['group_name'] == 'pro_numbers_amazonds' && $setting['type_name'] == 'min')
	{
		$pro_numbers_amazonds_min=$setting['setting_value'];
	} else if ($setting['group_name'] == 'pro_numbers_amazonds' && $setting['type_name'] == 'max')
	{
		$pro_numbers_amazonds_max=$setting['setting_value'];
	} else if ($setting['group_name'] == 'pro_numbers_yrc' && $setting['type_name'] == 'before_hyphen')
	{
		$pro_numbers_yrc_before_hyphen=$setting['setting_value'];
	} else if ($setting['group_name'] == 'pro_numbers_odfl' && $setting['type_name'] == 'min')
	{
		$pro_numbers_odfl_min=$setting['setting_value'];
	} else if ($setting['group_name'] == 'pro_numbers_odfl' && $setting['type_name'] == 'max')
	{
		$pro_numbers_odfl_max=$setting['setting_value'];
	} else if ($setting['group_name'] == 'pro_numbers_pyle' && $setting['type_name'] == 'min')
	{
		$pro_numbers_pyle_min=$setting['setting_value'];
	} else if ($setting['group_name'] == 'pro_numbers_pyle' && $setting['type_name'] == 'max')
	{
		$pro_numbers_pyle_max=$setting['setting_value'];
	} else if ($setting['group_name'] == 'rl_pal_addon_cost' && $setting['type_name'] == 'liftgate')
	{
		$rl_pal_addon_cost_liftgate=$setting['setting_value'];
	} else if ($setting['group_name'] == 'fedex_addon_cost' && $setting['type_name'] == 'liftgate')
	{
		$fedex_addon_cost_liftgate=$setting['setting_value'];
	} else if ($setting['group_name'] == 'order_processing_mode' && $setting['type_name'] == 'fedex')
	{
		$ORDER_PROCESSING_MODE_FEDEX=$setting['setting_value'];
	} else if ($setting['group_name'] == 'PrintCarrierForAmazon' && $setting['type_name'] == 'PrintCarrierForAmazonMode')
	{
		$PrintCarrierForAmazonMode=$setting['setting_value'];
	} else if ($setting['group_name'] == 'rl_pal_addon_cost' && $setting['type_name'] == 'residential')
	{
		$rl_pal_addon_cost_residential=$setting['setting_value'];
	} else if ($setting['group_name'] == 'query_mode' && $setting['type_name'] == 'dealer_setting')
	{
		$query_mode_dealer_setting=$setting['setting_value'];
	} else if ($setting['group_name'] == 'remote_orders' && $setting['type_name'] == 'state')
	{
		$remote_orders[]=$setting['setting_value'];
	} else if ($setting['group_name'] == 'ziphead_not_allowed' && $setting['type_name'] == 'RL')
	{
		$ziphead_not_allowed_RL[]=$setting['setting_value'];
	} else if ($setting['group_name'] == 'remote_orders' && $setting['type_name'] == 'dealer_name')
	{
		$remote_orders_dealer_name[]=$setting['setting_value'];
	} else if ($setting['group_name'] == 'shipping_ground' && $setting['type_name'] == 'sku')
	{
		$shipping_ground[]=$setting['setting_value'];
	} else if ($setting['group_name'] == 'shipping_ground_weight' && $setting['type_name'] == 'max')
	{
		$shipping_ground_weight_max=$setting['setting_value'];
	}
}
	
	
	$shiptrackData = runQuery2($conn, $queries['shiptrack']);
	$groupdetailData = runQuery2($conn, $queries['groupdetail']);
	$linedetailData = runQuery2($conn, $queries['linedetail']);
	
	$array = prepare_array($shiptrackData, $groupdetailData, $linedetailData, $user, $ship_method, $po);
	$all_queries = generateInsertQueries($array, $shiptrackData);
	
	$oSM = array(
				'RefNumber' => $array['RefNumber'],
				'ShipPostalCode' => $array['ShipAddress_PostalCode'],
				'dealerName' => $array['CustomerRef_FullName'],
				'PONumber' => $array['PONumber'],
				'weight' => $_POST['weight'],
				'liftgate' => $_POST['lift'],
				'residentialFedex' => $_POST['resi']);
	
	/*print_r("<pre>");
	print_r($oSM);
	print_r("</pre>");
	die();*/
	
	$result = beginTransaction($conn);
	if ($result)
	{
		foreach ($all_queries as $query)
		{
			$result = runQuery_empty_result($conn, $query);
			if (!$result)
			{
				rollbackTransaction($conn);
				die(json_encode('Error. Can not run query: '.$query.''));
			}
		}
		$result = commitTransaction($conn);
		if (!$result)
		{
			rollbackTransaction($conn);
			die(json_encode('Error. Can not commit transaction.'));
		} else 
		{
			ob_start();
			writeToShipTrackWithLabel($conn, $oSM);
			$proNumber = updateToShipTrackWithLabel($conn, $array['RefNumber'], $_POST['carrier'], $_POST['quote'], $_POST['time'], $_POST['cost'], $array['CustomerRef_FullName'], true);
			$err = ob_get_clean();
			$message = array('po' => $array['PONumber'], 'id' => $array['RefNumber'], 'pro' => $proNumber);
			$message = json_encode($message);
			print_r($message);
		}
	} else die('Error. Can not begin transaction.');
	
	/*print_r("<pre>");
	print_r($all_queries);
	print_r("</pre>");*/
	$conn = null;
} else
{
	//print_r($_POST);
	die(json_encode('Error. Missing data. Make sure at least two orders are chosen.'));
}
?>