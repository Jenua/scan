<?php
$path = $_SERVER['DOCUMENT_ROOT'];
require_once($path.'/db_connect/connect.php');

	$returnValue = false;
		
	$term = trim(strip_tags($_GET['term']));
	$term = str_replace(array("'", "`"), "''", $term);
		
	$conn = Database::getInstance()->dbc;
	$res = getValues($conn, $term);
	
	if(isset($res) && !empty($res))
	{
		$array = array();
		foreach($res as $key => $value)
		{
			$array[]= $value['ShipMethodRef_FullName'];
		}
		$returnValue = $array;
	}
	$returnValue = json_encode($returnValue);
	$conn = null;
	print_r($returnValue);

function getValues($conn, $term)
{
	$query = "SELECT TOP(15) * FROM (	
	SELECT 
	[shiptrack].[ShipMethodRef_FullName]
	FROM [".DATABASE."].[dbo].[shiptrack]
	WHERE [shiptrack].[ShipMethodRef_FullName] LIKE '%".$term."%' UNION ALL
	SELECT 
	[invoice].[ShipMethodRef_FullName]
	FROM [".DATABASE31."].[dbo].[invoice]
	WHERE [invoice].[ShipMethodRef_FullName] LIKE '%".$term."%' UNION ALL
	SELECT 
	[salesorder].[ShipMethodRef_FullName]
	FROM [".DATABASE31."].[dbo].[salesorder]
	WHERE [salesorder].[ShipMethodRef_FullName] LIKE '%".$term."%' UNION ALL
	SELECT 
	[manually_shiptrack].[ShipMethodRef_FullName]
	FROM [".DATABASE."].[dbo].[manually_shiptrack]
	WHERE [manually_shiptrack].[ShipMethodRef_FullName] LIKE '%".$term."%' UNION ALL
	SELECT 
	[ShipTrackWithLabel].[CARRIERNAME] as [ShipMethodRef_FullName]
	FROM [".DATABASE."].[dbo].[ShipTrackWithLabel]
	WHERE [ShipTrackWithLabel].[CARRIERNAME] LIKE '%".$term."%') A
	GROUP BY [ShipMethodRef_FullName]
	ORDER BY [ShipMethodRef_FullName]";
	//print_r($query);
	try{
		$result = $conn->prepare($query);
		$result->execute();
		$result = $result->fetchAll(PDO::FETCH_ASSOC); 
		return $result;
	}
	catch(PDOException $e)
	{
		return false;
	}
}
?>